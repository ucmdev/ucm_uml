/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_environment.ucm_resources.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import ucm_base.ucm_contracts.Ucm_contractsPackage;

import ucm_base.ucm_contracts.provider.IConfigurationParameterItemProvider;

import ucm_environment.ucm_resources.ResourceAllocationAndConfigurationParameter;
import ucm_environment.ucm_resources.Ucm_resourcesPackage;

/**
 * This is the item provider adapter for a {@link ucm_environment.ucm_resources.ResourceAllocationAndConfigurationParameter} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ResourceAllocationAndConfigurationParameterItemProvider extends IConfigurationParameterItemProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceAllocationAndConfigurationParameterItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addTypePropertyDescriptor(object);
			addDefaultValuePropertyDescriptor(object);
			addAllowedResourceDefinitionPropertyDescriptor(object);
			addMinPropertyDescriptor(object);
			addMaxPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_IHasDatatype_type_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_IHasDatatype_type_feature", "_UI_IHasDatatype_type"),
				 Ucm_contractsPackage.Literals.IHAS_DATATYPE__TYPE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Default Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDefaultValuePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_IHasDefaultValue_defaultValue_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_IHasDefaultValue_defaultValue_feature", "_UI_IHasDefaultValue_type"),
				 Ucm_contractsPackage.Literals.IHAS_DEFAULT_VALUE__DEFAULT_VALUE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Allowed Resource Definition feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAllowedResourceDefinitionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ResourceAllocationAndConfigurationParameter_allowedResourceDefinition_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ResourceAllocationAndConfigurationParameter_allowedResourceDefinition_feature", "_UI_ResourceAllocationAndConfigurationParameter_type"),
				 Ucm_resourcesPackage.Literals.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__ALLOWED_RESOURCE_DEFINITION,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Min feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMinPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ResourceAllocationAndConfigurationParameter_min_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ResourceAllocationAndConfigurationParameter_min_feature", "_UI_ResourceAllocationAndConfigurationParameter_type"),
				 Ucm_resourcesPackage.Literals.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__MIN,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Max feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMaxPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ResourceAllocationAndConfigurationParameter_max_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ResourceAllocationAndConfigurationParameter_max_feature", "_UI_ResourceAllocationAndConfigurationParameter_type"),
				 Ucm_resourcesPackage.Literals.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__MAX,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns ResourceAllocationAndConfigurationParameter.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ResourceAllocationAndConfigurationParameter"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((ResourceAllocationAndConfigurationParameter)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_ResourceAllocationAndConfigurationParameter_type") :
			getString("_UI_ResourceAllocationAndConfigurationParameter_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ResourceAllocationAndConfigurationParameter.class)) {
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__DEFAULT_VALUE:
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__MIN:
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__MAX:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
