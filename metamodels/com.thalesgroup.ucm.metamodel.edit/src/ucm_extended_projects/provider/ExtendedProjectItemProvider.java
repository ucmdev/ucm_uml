/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_extended_projects.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;

import ucm_basic_projects.Ucm_basic_projectsPackage;

import ucm_basic_projects.provider.BasicProjectItemProvider;

import ucm_environment.ucm_resources.Ucm_resourcesFactory;

import ucm_supplement.ucm_deployments.Ucm_deploymentsFactory;

import ucm_supplement.ucm_test_cases.Ucm_test_casesFactory;

/**
 * This is the item provider adapter for a {@link ucm_extended_projects.ExtendedProject} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ExtendedProjectItemProvider extends BasicProjectItemProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExtendedProjectItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This returns ExtendedProject.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("ucm/Project.png"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_ExtendedProject_type");
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(Ucm_basic_projectsPackage.Literals.IPROJECT__MODULE,
				 Ucm_deploymentsFactory.eINSTANCE.createDeploymentModule()));

		newChildDescriptors.add
			(createChildParameter
				(Ucm_basic_projectsPackage.Literals.IPROJECT__MODULE,
				 Ucm_deploymentsFactory.eINSTANCE.createAllocationPlan()));

		newChildDescriptors.add
			(createChildParameter
				(Ucm_basic_projectsPackage.Literals.IPROJECT__MODULE,
				 Ucm_test_casesFactory.eINSTANCE.createTestCaseGroup()));

		newChildDescriptors.add
			(createChildParameter
				(Ucm_basic_projectsPackage.Literals.IPROJECT__MODULE,
				 Ucm_resourcesFactory.eINSTANCE.createResourceDefinitionModule()));

		newChildDescriptors.add
			(createChildParameter
				(Ucm_basic_projectsPackage.Literals.IPROJECT__MODULE,
				 Ucm_resourcesFactory.eINSTANCE.createEnvironmentModule()));
	}

}
