/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_enhanced_configuration.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import ucm_base.ucm_contracts.Ucm_contractsPackage;

import ucm_base.ucm_contracts.provider.AnnotationDefinitionItemProvider;

import ucm_environment.ucm_resources.Ucm_resourcesFactory;
import ucm_supplement.ucm_enhanced_configuration.RestrictiveAnnotationDefinition;
import ucm_supplement.ucm_enhanced_configuration.Ucm_enhanced_configurationFactory;
import ucm_supplement.ucm_enhanced_configuration.Ucm_enhanced_configurationPackage;

/**
 * This is the item provider adapter for a {@link ucm_supplement.ucm_enhanced_configuration.RestrictiveAnnotationDefinition} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class RestrictiveAnnotationDefinitionItemProvider extends AnnotationDefinitionItemProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RestrictiveAnnotationDefinitionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addAnnotableEntitiesPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Annotable Entities feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAnnotableEntitiesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RestrictiveAnnotationDefinition_annotableEntities_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RestrictiveAnnotationDefinition_annotableEntities_feature", "_UI_RestrictiveAnnotationDefinition_type"),
				 Ucm_enhanced_configurationPackage.Literals.RESTRICTIVE_ANNOTATION_DEFINITION__ANNOTABLE_ENTITIES,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns RestrictiveAnnotationDefinition.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/RestrictiveAnnotationDefinition"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((RestrictiveAnnotationDefinition)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_RestrictiveAnnotationDefinition_type") :
			getString("_UI_RestrictiveAnnotationDefinition_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(RestrictiveAnnotationDefinition.class)) {
			case Ucm_enhanced_configurationPackage.RESTRICTIVE_ANNOTATION_DEFINITION__ANNOTABLE_ENTITIES:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(Ucm_contractsPackage.Literals.ICONFIGURABLE__CONFIGURATION_PARAMETER,
				 Ucm_enhanced_configurationFactory.eINSTANCE.createPolicyDependencyParameter()));

		newChildDescriptors.add
			(createChildParameter
				(Ucm_contractsPackage.Literals.ICONFIGURABLE__CONFIGURATION_PARAMETER,
				 Ucm_resourcesFactory.eINSTANCE.createResourceAllocationParameter()));

		newChildDescriptors.add
			(createChildParameter
				(Ucm_contractsPackage.Literals.ICONFIGURABLE__CONFIGURATION_PARAMETER,
				 Ucm_resourcesFactory.eINSTANCE.createResourceAllocationAndConfigurationParameter()));
	}

}
