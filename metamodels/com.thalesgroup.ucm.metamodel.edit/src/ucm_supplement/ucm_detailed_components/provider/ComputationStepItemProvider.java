/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_detailed_components.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import ucm_supplement.ucm_detailed_components.ComputationStep;
import ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage;

/**
 * This is the item provider adapter for a {@link ucm_supplement.ucm_detailed_components.ComputationStep} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ComputationStepItemProvider extends IExecutionStepItemProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComputationStepItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addSpecifiedWCETPropertyDescriptor(object);
			addSpecifiedBCETPropertyDescriptor(object);
			addMeasuredWCETPropertyDescriptor(object);
			addMeasuredBCETPropertyDescriptor(object);
			addEvaluatedWCETPropertyDescriptor(object);
			addEvaluatedBCETPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Specified WCET feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSpecifiedWCETPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ComputationStep_specifiedWCET_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ComputationStep_specifiedWCET_feature", "_UI_ComputationStep_type"),
				 Ucm_detailed_componentsPackage.Literals.COMPUTATION_STEP__SPECIFIED_WCET,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Specified BCET feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSpecifiedBCETPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ComputationStep_specifiedBCET_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ComputationStep_specifiedBCET_feature", "_UI_ComputationStep_type"),
				 Ucm_detailed_componentsPackage.Literals.COMPUTATION_STEP__SPECIFIED_BCET,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Measured WCET feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMeasuredWCETPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ComputationStep_measuredWCET_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ComputationStep_measuredWCET_feature", "_UI_ComputationStep_type"),
				 Ucm_detailed_componentsPackage.Literals.COMPUTATION_STEP__MEASURED_WCET,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Measured BCET feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMeasuredBCETPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ComputationStep_measuredBCET_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ComputationStep_measuredBCET_feature", "_UI_ComputationStep_type"),
				 Ucm_detailed_componentsPackage.Literals.COMPUTATION_STEP__MEASURED_BCET,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Evaluated WCET feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEvaluatedWCETPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ComputationStep_evaluatedWCET_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ComputationStep_evaluatedWCET_feature", "_UI_ComputationStep_type"),
				 Ucm_detailed_componentsPackage.Literals.COMPUTATION_STEP__EVALUATED_WCET,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Evaluated BCET feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEvaluatedBCETPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ComputationStep_evaluatedBCET_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ComputationStep_evaluatedBCET_feature", "_UI_ComputationStep_type"),
				 Ucm_detailed_componentsPackage.Literals.COMPUTATION_STEP__EVALUATED_BCET,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns ComputationStep.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ComputationStep"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((ComputationStep)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_ComputationStep_type") :
			getString("_UI_ComputationStep_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ComputationStep.class)) {
			case Ucm_detailed_componentsPackage.COMPUTATION_STEP__SPECIFIED_WCET:
			case Ucm_detailed_componentsPackage.COMPUTATION_STEP__SPECIFIED_BCET:
			case Ucm_detailed_componentsPackage.COMPUTATION_STEP__MEASURED_WCET:
			case Ucm_detailed_componentsPackage.COMPUTATION_STEP__MEASURED_BCET:
			case Ucm_detailed_componentsPackage.COMPUTATION_STEP__EVALUATED_WCET:
			case Ucm_detailed_componentsPackage.COMPUTATION_STEP__EVALUATED_BCET:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
