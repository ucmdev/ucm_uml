/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_detailed_components.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import ucm_base.ucm_commons.provider.INamedItemProvider;
import ucm_supplement.ucm_detailed_components.ExecutionScenario;
import ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsFactory;
import ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage;

/**
 * This is the item provider adapter for a {@link ucm_supplement.ucm_detailed_components.ExecutionScenario} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ExecutionScenarioItemProvider extends INamedItemProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExecutionScenarioItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(Ucm_detailed_componentsPackage.Literals.EXECUTION_SCENARIO__PORT_AND_METHOD);
			childrenFeatures.add(Ucm_detailed_componentsPackage.Literals.EXECUTION_SCENARIO__ASSOCIATED_STEP);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns ExecutionScenario.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ExecutionScenario"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((ExecutionScenario)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_ExecutionScenario_type") :
			getString("_UI_ExecutionScenario_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ExecutionScenario.class)) {
			case Ucm_detailed_componentsPackage.EXECUTION_SCENARIO__PORT_AND_METHOD:
			case Ucm_detailed_componentsPackage.EXECUTION_SCENARIO__ASSOCIATED_STEP:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(Ucm_detailed_componentsPackage.Literals.EXECUTION_SCENARIO__PORT_AND_METHOD,
				 Ucm_detailed_componentsFactory.eINSTANCE.createPortAndMethod()));

		newChildDescriptors.add
			(createChildParameter
				(Ucm_detailed_componentsPackage.Literals.EXECUTION_SCENARIO__ASSOCIATED_STEP,
				 Ucm_detailed_componentsFactory.eINSTANCE.createComputationStep()));

		newChildDescriptors.add
			(createChildParameter
				(Ucm_detailed_componentsPackage.Literals.EXECUTION_SCENARIO__ASSOCIATED_STEP,
				 Ucm_detailed_componentsFactory.eINSTANCE.createComputationWithEndingCommunicationStep()));

		newChildDescriptors.add
			(createChildParameter
				(Ucm_detailed_componentsPackage.Literals.EXECUTION_SCENARIO__ASSOCIATED_STEP,
				 Ucm_detailed_componentsFactory.eINSTANCE.createLoopStep()));

		newChildDescriptors.add
			(createChildParameter
				(Ucm_detailed_componentsPackage.Literals.EXECUTION_SCENARIO__ASSOCIATED_STEP,
				 Ucm_detailed_componentsFactory.eINSTANCE.createAlternativeStep()));

		newChildDescriptors.add
			(createChildParameter
				(Ucm_detailed_componentsPackage.Literals.EXECUTION_SCENARIO__ASSOCIATED_STEP,
				 Ucm_detailed_componentsFactory.eINSTANCE.createSequenceStep()));
	}

}
