/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_deployments.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import ucm_base.ucm_commons.Ucm_commonsFactory;
import ucm_base.ucm_commons.Ucm_commonsPackage;

import ucm_base.ucm_contracts.Ucm_contractsFactory;
import ucm_base.ucm_contracts.Ucm_contractsPackage;

import ucm_supplement.ucm_deployments.ComponentInstanceInitialization;
import ucm_supplement.ucm_deployments.Ucm_deploymentsFactory;
import ucm_supplement.ucm_deployments.Ucm_deploymentsPackage;

/**
 * This is the item provider adapter for a {@link ucm_supplement.ucm_deployments.ComponentInstanceInitialization} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ComponentInstanceInitializationItemProvider extends IHasAttributeInitializationItemProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstanceInitializationItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addNamePropertyDescriptor(object);
			addComponentInstancePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_INamed_name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_INamed_name_feature", "_UI_INamed_type"),
				 Ucm_commonsPackage.Literals.INAMED__NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Component Instance feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addComponentInstancePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ComponentInstanceInitialization_componentInstance_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ComponentInstanceInitialization_componentInstance_feature", "_UI_ComponentInstanceInitialization_type"),
				 Ucm_deploymentsPackage.Literals.COMPONENT_INSTANCE_INITIALIZATION__COMPONENT_INSTANCE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(Ucm_commonsPackage.Literals.INAMED__COMMENT);
			childrenFeatures.add(Ucm_contractsPackage.Literals.IANNOTABLE__ANNOTATION);
			childrenFeatures.add(Ucm_deploymentsPackage.Literals.COMPONENT_INSTANCE_INITIALIZATION__PORT_INITIALIZATION);
			childrenFeatures.add(Ucm_deploymentsPackage.Literals.COMPONENT_INSTANCE_INITIALIZATION__POLICY_INITIALIZATION);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns ComponentInstanceInitialization.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("ext_ucm/ComponentInstance.png"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((ComponentInstanceInitialization)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_ComponentInstanceInitialization_type") :
			getString("_UI_ComponentInstanceInitialization_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ComponentInstanceInitialization.class)) {
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__NAME:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__COMMENT:
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__ANNOTATION:
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__PORT_INITIALIZATION:
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__POLICY_INITIALIZATION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(Ucm_commonsPackage.Literals.INAMED__COMMENT,
				 Ucm_commonsFactory.eINSTANCE.createSimpleComment()));

		newChildDescriptors.add
			(createChildParameter
				(Ucm_contractsPackage.Literals.IANNOTABLE__ANNOTATION,
				 Ucm_contractsFactory.eINSTANCE.createAnnotation()));

		newChildDescriptors.add
			(createChildParameter
				(Ucm_deploymentsPackage.Literals.COMPONENT_INSTANCE_INITIALIZATION__PORT_INITIALIZATION,
				 Ucm_deploymentsFactory.eINSTANCE.createComponentInstancePortInitialization()));

		newChildDescriptors.add
			(createChildParameter
				(Ucm_deploymentsPackage.Literals.COMPONENT_INSTANCE_INITIALIZATION__POLICY_INITIALIZATION,
				 Ucm_deploymentsFactory.eINSTANCE.createComponentInstancePolicyInitialization()));
	}

}
