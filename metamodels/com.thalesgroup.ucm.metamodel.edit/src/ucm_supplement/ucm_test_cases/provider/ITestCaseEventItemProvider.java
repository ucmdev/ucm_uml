/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_test_cases.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import ucm_base.ucm_commons.provider.INamedItemProvider;

import ucm_supplement.ucm_deployments.Ucm_deploymentsFactory;

import ucm_supplement.ucm_test_cases.ITestCaseEvent;
import ucm_supplement.ucm_test_cases.Ucm_test_casesFactory;
import ucm_supplement.ucm_test_cases.Ucm_test_casesPackage;

/**
 * This is the item provider adapter for a {@link ucm_supplement.ucm_test_cases.ITestCaseEvent} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ITestCaseEventItemProvider extends INamedItemProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ITestCaseEventItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addPortElementPropertyDescriptor(object);
			addMethodPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Port Element feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPortElementPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ITestCaseEvent_portElement_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ITestCaseEvent_portElement_feature", "_UI_ITestCaseEvent_type"),
				 Ucm_test_casesPackage.Literals.ITEST_CASE_EVENT__PORT_ELEMENT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Method feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMethodPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ITestCaseEvent_method_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ITestCaseEvent_method_feature", "_UI_ITestCaseEvent_type"),
				 Ucm_test_casesPackage.Literals.ITEST_CASE_EVENT__METHOD,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(Ucm_test_casesPackage.Literals.ITEST_CASE_EVENT__COMPONENT_INSTANCE);
			childrenFeatures.add(Ucm_test_casesPackage.Literals.ITEST_CASE_EVENT__INTERACTION_POINT);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((ITestCaseEvent)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_ITestCaseEvent_type") :
			getString("_UI_ITestCaseEvent_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ITestCaseEvent.class)) {
			case Ucm_test_casesPackage.ITEST_CASE_EVENT__COMPONENT_INSTANCE:
			case Ucm_test_casesPackage.ITEST_CASE_EVENT__INTERACTION_POINT:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(Ucm_test_casesPackage.Literals.ITEST_CASE_EVENT__COMPONENT_INSTANCE,
				 Ucm_deploymentsFactory.eINSTANCE.createComponentInstance()));

		newChildDescriptors.add
			(createChildParameter
				(Ucm_test_casesPackage.Literals.ITEST_CASE_EVENT__INTERACTION_POINT,
				 Ucm_test_casesFactory.eINSTANCE.createComponentPortReference()));

		newChildDescriptors.add
			(createChildParameter
				(Ucm_test_casesPackage.Literals.ITEST_CASE_EVENT__INTERACTION_POINT,
				 Ucm_test_casesFactory.eINSTANCE.createComponentMultiplexPortReference()));

		newChildDescriptors.add
			(createChildParameter
				(Ucm_test_casesPackage.Literals.ITEST_CASE_EVENT__INTERACTION_POINT,
				 Ucm_test_casesFactory.eINSTANCE.createTechnicalPolicyReference()));
	}

}
