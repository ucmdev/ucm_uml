package org.omg.ucm.metamodel.validation;

import java.util.HashSet;

import ucm_base.ucm_contracts.IAbstractTypeDeclaration;

class AbstractTypeList {
	private HashSet<IAbstractTypeDeclaration> abstractTypes;

	public AbstractTypeList() {
		this.abstractTypes = new HashSet<IAbstractTypeDeclaration>();
	}

	public AbstractTypeList(AbstractTypeList otherList) {
		if (otherList != null) {
			this.abstractTypes = new HashSet<IAbstractTypeDeclaration>(otherList.abstractTypes);
		} else {
			this.abstractTypes = new HashSet<IAbstractTypeDeclaration>();
		}
	}

	public AbstractTypeList(IAbstractTypeDeclaration abstractType) {
		this.abstractTypes = new HashSet<IAbstractTypeDeclaration>();
		this.abstractTypes.add(abstractType);
	}

	public void add(IAbstractTypeDeclaration abstractType) {
		this.abstractTypes.add(abstractType);
	}

	public void add(AbstractTypeList otherList) {
		if (otherList != null) {
			this.abstractTypes.addAll(abstractTypes);
		}
	}

	public HashSet<IAbstractTypeDeclaration> getList() {
		return this.abstractTypes;
	}
}
