package org.omg.ucm.metamodel.validation;

public class UcmBaseValidationFactory {
	public static UcmBaseValidationFactory INSTANCE = new UcmBaseValidationFactory();

	protected UcmComponentValidation componentValidator = new UcmComponentValidation();

	protected UcmContractValidation contractValidator = new UcmContractValidation();

	protected UcmTechnicalPolicyValidation policyValidator = new UcmTechnicalPolicyValidation();

	protected UcmInteractionValidation interactionValidator = new UcmInteractionValidation();

	public UcmComponentValidation getComponentValidator() {
		return this.componentValidator;
	}

	public UcmContractValidation getContractValidator() {
		return this.contractValidator;
	}

	public UcmTechnicalPolicyValidation getPolicyValidator() {
		return this.policyValidator;
	}

	public UcmInteractionValidation getInteractionValidator() {
		return this.interactionValidator;
	}
}
