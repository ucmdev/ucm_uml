package org.omg.ucm.metamodel.validation;

import ucm_base.ucm_contracts.ucm_datatypes.Array;
import ucm_base.ucm_contracts.ucm_datatypes.Sequence;
import ucm_base.ucm_contracts.ucm_datatypes.Structure;
import ucm_base.ucm_contracts.ucm_datatypes.StructureField;
import ucm_base.ucm_contracts.ucm_datatypes.Union;
import ucm_base.ucm_contracts.ucm_datatypes.UnionCase;
import ucm_base.ucm_contracts.ucm_datatypes.util.Ucm_datatypesSwitch;

class AbstractTypeDataVisitor extends Ucm_datatypesSwitch<AbstractTypeList> {
	private AbstractTypeVisitor visitor;

	public AbstractTypeDataVisitor(AbstractTypeVisitor visitor) {
		this.visitor = visitor;
	}

	public AbstractTypeList caseStructure(Structure object) {
		AbstractTypeList list = new AbstractTypeList();
		for (StructureField field : object.getField()) {
			list.add(this.visitor.doSwitch(field.getType()));
		}
		return list;
	}

	public AbstractTypeList caseUnion(Union object) {
		AbstractTypeList list = new AbstractTypeList();
		for (UnionCase field : object.getCase()) {
			list.add(this.visitor.doSwitch(field.getType()));
		}
		return list;
	}

	public AbstractTypeList caseArray(Array object) {
		return this.visitor.doSwitch(object.getType());
	}

	public AbstractTypeList caseSequence(Sequence object) {
		return this.visitor.doSwitch(object.getType());
	}

}
