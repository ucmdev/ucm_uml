package org.omg.ucm.metamodel.validation;

import java.util.LinkedList;

public class ComponentFeatureListManager<Comp, Feat> {
	private class CompFeat {
		public Comp comp;
		public Feat feat;
	}

	private LinkedList<CompFeat> compFeats = new LinkedList<CompFeat>();

	public void register(Comp component, Feat feature) {
		CompFeat pp = new CompFeat();
		pp.comp = component;
		pp.feat = feature;
		this.compFeats.add(pp);
	}

	public boolean alreadyExists(Comp component, Feat feature) {
		boolean found = false;
		for (CompFeat pp : this.compFeats) {
			if (pp.comp == component && pp.feat == feature) {
				found = true;
				break;
			}
		}
		return found;
	}
}
