package org.omg.ucm.metamodel.validation;

import java.util.LinkedList;

import ucm_base.ucm_contracts.IConfigurable;
import ucm_base.ucm_contracts.IConfigured;

public interface IConfiguredManager {
	public LinkedList<IConfigurable> getConfigurables(IConfigured configured);
}
