package org.omg.ucm.metamodel.validation;

import java.util.HashSet;
import java.util.Map;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;

import ucm_base.ucm_technicalpolicies.TechnicalPolicyDefinition;
import ucm_base.ucm_technicalpolicies.util.Ucm_technicalpoliciesValidator;

public class UcmTechnicalPolicyValidation {
	public boolean noExtensionLoop(TechnicalPolicyDefinition policyDef, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		boolean success = true;
		boolean found = false;
		/*
		 * Hash set that stores all visited policy definitions, to avoid endless
		 * loops.
		 */
		HashSet<TechnicalPolicyDefinition> policyDefs = new HashSet<TechnicalPolicyDefinition>();
		policyDefs.add(policyDef);
		TechnicalPolicyDefinition refined = policyDef.getExtends();

		if (refined != null) {
			if (policyDef.getTechnicalAspect() != refined.getTechnicalAspect()) {
				if (diagnostics != null) {
					diagnostics
							.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_technicalpoliciesValidator.DIAGNOSTIC_SOURCE,
									Ucm_technicalpoliciesValidator.TECHNICAL_POLICY_DEFINITION__NO_EXTENSION_LOOP,
									"Technical policy definitions " + policyDef.getName() + " and " + refined.getName()
											+ " correspond to different aspects",
									new Object[] { policyDef, refined }));
				}
				success = false;
			}
			if (policyDef.getApplicability() != refined.getApplicability()) {
				if (diagnostics != null) {
					diagnostics
							.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_technicalpoliciesValidator.DIAGNOSTIC_SOURCE,
									Ucm_technicalpoliciesValidator.TECHNICAL_POLICY_DEFINITION__NO_EXTENSION_LOOP,
									"Technical policy definitions " + policyDef.getName() + " and " + refined.getName()
											+ " have different applicability settings",
									new Object[] { policyDef, refined }));
				}
				success = false;
			}
		}

		while (refined != null && !found && !policyDefs.contains(policyDef)) {
			policyDefs.add(policyDef);
			if (refined == policyDef) {
				found = true;
			}
			refined = refined.getExtends();
		}
		if (found) {
			if (diagnostics != null) {
				diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_technicalpoliciesValidator.DIAGNOSTIC_SOURCE,
						Ucm_technicalpoliciesValidator.TECHNICAL_POLICY_DEFINITION__NO_EXTENSION_LOOP,
						"Technical policy definition " + policyDef.getName() + " refines itself",
						new Object[] { policyDef }));
			}
			success = false;
		}
		return success;
	}
}
