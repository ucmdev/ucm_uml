package org.omg.ucm.metamodel.validation;

import org.eclipse.emf.ecore.EObject;

class AbstractTypeVisitor {
	private AbstractTypeContractVisitor contractVisitor;
	private AbstractTypeDataVisitor dataVisitor;
	
	public AbstractTypeVisitor()
	{
		this.contractVisitor = new AbstractTypeContractVisitor(this);
		this.dataVisitor = new AbstractTypeDataVisitor(this);
	}
	
	public AbstractTypeList doSwitch(EObject object)
	{
		AbstractTypeList list; 
		list = this.contractVisitor.doSwitch(object);
		if (list == null)
		{
			list = this.dataVisitor.doSwitch(object);
		}
		return list;
	}
}
