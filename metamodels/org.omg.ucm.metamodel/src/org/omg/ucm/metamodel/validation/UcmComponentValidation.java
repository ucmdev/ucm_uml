package org.omg.ucm.metamodel.validation;

import java.util.HashSet;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Map;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;

import ucm_base.ucm_components.AbstractTypeBinding;
import ucm_base.ucm_components.AssemblyPart;
import ucm_base.ucm_components.AtomicComponentImplementation;
import ucm_base.ucm_components.AttributeDelegation;
import ucm_base.ucm_components.ComponentPortTechnicalPolicy;
import ucm_base.ucm_components.ComponentTechnicalPolicy;
import ucm_base.ucm_components.ComponentType;
import ucm_base.ucm_components.CompositeComponentImplementation;
import ucm_base.ucm_components.Connection;
import ucm_base.ucm_components.ConnectionEnd;
import ucm_base.ucm_components.IComponent;
import ucm_base.ucm_components.IComponentImplementation;
import ucm_base.ucm_components.IPortSpec;
import ucm_base.ucm_components.InteractionItemBinding;
import ucm_base.ucm_components.Port;
import ucm_base.ucm_components.PortDelegation;
import ucm_base.ucm_components.PortRoleSpec;
import ucm_base.ucm_components.PortTypeSpec;
import ucm_base.ucm_components.util.Ucm_componentsValidator;
import ucm_base.ucm_contracts.Attribute;
import ucm_base.ucm_contracts.IAbstractTypeDeclaration;
import ucm_base.ucm_contracts.IConcreteTypeDeclaration;
import ucm_base.ucm_contracts.ITypeDeclaration;
import ucm_base.ucm_contracts.util.Ucm_contractsValidator;
import ucm_base.ucm_interactions.ConnectorDefinition;
import ucm_base.ucm_interactions.ConnectorPort;
import ucm_base.ucm_interactions.IITemBinding;
import ucm_base.ucm_interactions.IInteractionDefinition;
import ucm_base.ucm_interactions.InteractionItem;
import ucm_base.ucm_interactions.InteractionPattern;
import ucm_base.ucm_interactions.InteractionRole;
import ucm_base.ucm_interactions.ItemBinding;
import ucm_base.ucm_interactions.PortElement;
import ucm_base.ucm_interactions.PortType;
import ucm_base.ucm_technicalpolicies.TechnicalAspect;
import ucm_base.ucm_technicalpolicies.TechnicalPolicyApplicability;
import ucm_base.ucm_technicalpolicies.TechnicalPolicyDefinition;

public class UcmComponentValidation {

	private final class TechnicalAspectMultiplicity {
		private Hashtable<TechnicalAspect, Integer> multiplicities = new Hashtable<TechnicalAspect, Integer>();
		private HashSet<EObject> technicalAspectsAreFound = new HashSet<EObject>();

		public TechnicalAspectMultiplicity(EObject object) {
			if (object != null) {
				this.findTechnicalAspects(object);
			}
		}

		/**
		 * Scans all objects from the top-level root of eObject, looking for
		 * technical aspects. The rationale for this is to be able to raise
		 * errors for technical aspects that are mandatory (exactly_one or
		 * at_least_one).
		 * 
		 * @param eObject
		 *            the object to consider
		 */
		private void findTechnicalAspects(EObject eObject) {
			if (eObject == null) {
				return;
			}
			if (this.technicalAspectsAreFound.contains(eObject)) {
				return;
			} else {
				this.technicalAspectsAreFound.add(eObject);
			}
			EObject root = eObject;
			/*
			 * We first reach the model root
			 */
			while (root.eContainer() != null) {
				root = root.eContainer();
			}
			if (this.technicalAspectsAreFound.contains(root)) {
				return;
			} else {
				this.technicalAspectsAreFound.add(root);
			}

			/*
			 * Then retrieve all technical aspects. If we find an atomic
			 * component implementation, we scan its technical policies, and
			 * find the associated technical aspects.
			 */
			TreeIterator<EObject> iterator = root.eAllContents();
			while (iterator.hasNext()) {
				EObject object = iterator.next();
				if (object instanceof TechnicalAspect) {
					this.touchMultiplicity((TechnicalAspect) object);
				} else if (object instanceof AtomicComponentImplementation) {
					for (ComponentTechnicalPolicy policy : ((AtomicComponentImplementation) object).getPolicy()) {
						TechnicalPolicyDefinition def = policy.getDefinition();
						this.findTechnicalAspects(def);
						this.findTechnicalAspects(policy);
						this.findTechnicalAspects(def.getTechnicalAspect());
					}
				} else if (object instanceof AssemblyPart) {
					this.findTechnicalAspects(((AssemblyPart) object).getComponentDefinition());
				}
			}
		}

		private void touchMultiplicity(TechnicalAspect aspect) {
			this.multiplicities.putIfAbsent(aspect, new Integer(0));
		}

		public void incrementMultiplicity(TechnicalAspect aspect) {
			this.findTechnicalAspects(aspect);
			if (multiplicities.containsKey(aspect)) {
				multiplicities.put(aspect, (multiplicities.get(aspect)) + 1);
			} else {
				multiplicities.put(aspect, new Integer(1));
			}
		}

		public int getMultiplicity(TechnicalAspect aspect) {
			if (this.multiplicities.containsKey(aspect)) {
				return this.multiplicities.get(aspect).intValue();
			} else {
				return 0;
			}
		}

		public boolean isConsistent(TechnicalAspect aspect) {
			boolean multiplicityIsOk;
			int multiplicity = this.getMultiplicity(aspect);
			switch (aspect.getMultiplicity()) {
			case EXACTLY_ONE:
				multiplicityIsOk = (multiplicity == 1);
				break;
			case AT_MOST_ONE:
				multiplicityIsOk = (multiplicity <= 1);
				break;
			case AT_LEAST_ONE:
				multiplicityIsOk = (multiplicity >= 1);
				break;
			case ANY_NUMBER:
			default:
				multiplicityIsOk = true;
			}
			return multiplicityIsOk;
		}

		public LinkedList<TechnicalAspect> getTechnicalAspects() {
			return new LinkedList<TechnicalAspect>(this.multiplicities.keySet());
		}
	}

	public boolean extensionConsistency(Connection connection, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		boolean success = true;
		if (connection.getRefines() != null) {
			IInteractionDefinition connectionDefinition = connection.getConnectionDefinition();
			IInteractionDefinition ancestorConnectionDefinition = connection.getRefines().getConnectionDefinition();
			if (connectionDefinition instanceof ConnectorDefinition
					&& ancestorConnectionDefinition instanceof ConnectorDefinition) {
				ConnectorDefinition def = (ConnectorDefinition) connectionDefinition;
				boolean found = false;
				while (def != null & !found) {
					if (def == ancestorConnectionDefinition) {
						found = true;
					}
					def = def.getExtends();
				}
				if (!found) {
					if (diagnostics != null) {
						diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_componentsValidator.DIAGNOSTIC_SOURCE,
								Ucm_componentsValidator.CONNECTION__EXTENSION_CONSISTENCY,
								"Connection " + connection.getName()
										+ " extends another connection with an incompatible definition",
								new Object[] { connection }));
					}
					success = false;
				}
			} else {
				if (diagnostics != null) {
					diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_componentsValidator.DIAGNOSTIC_SOURCE,
							Ucm_componentsValidator.CONNECTION__EXTENSION_CONSISTENCY,
							"Connection " + connection.getName()
									+ " extends another connection with an incompatible definition",
							new Object[] { connection }));
				}
				success = false;
			}
		}

		return success;
	}

	public boolean noParamsIfPattern(Connection connection, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean success = true;
		if (connection.getConnectionDefinition() instanceof InteractionPattern
				&& !connection.getConfigurationValue().isEmpty()) {
			if (diagnostics != null) {
				diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_componentsValidator.DIAGNOSTIC_SOURCE,
						Ucm_componentsValidator.CONNECTION__NO_PARAMS_IF_PATTERN,
						"Connection " + connection.getName()
								+ " is an Interaction Pattern and therefore cannot have configuration parameters",
						new Object[] { connection }));
			}
			success = false;
		}
		return success;
	}

	private LinkedList<Port> getAllDelegatedPorts(Port port, AssemblyPart part, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		LinkedList<Port> ports = new LinkedList<Port>();
		IComponent compDef = part.getComponentDefinition();
		/*
		 * get the port of an atomic implementation, or the delegated ports of a
		 * composite implementation. If we only have a component type, we cannot
		 * tell how many ports are delegated, since the architecture is
		 * incomplete. So, we add the port itself because there will be at least
		 * one port.
		 */
		if (compDef instanceof AtomicComponentImplementation) {
			ports.add(port);
		} else if (compDef instanceof CompositeComponentImplementation) {
			for (PortDelegation portDelegation : ((CompositeComponentImplementation) compDef).getPortDelegation()) {
				if (portDelegation.getExternalPort() == port) {
					ports.addAll(this.getAllDelegatedPorts(portDelegation.getPort(), portDelegation.getPart(),
							diagnostics, context));
				}
			}
		} else if (compDef instanceof ComponentType) {
			ports.add(port);
			if (diagnostics != null) {
				diagnostics.add(new BasicDiagnostic(Diagnostic.WARNING, Ucm_componentsValidator.DIAGNOSTIC_SOURCE,
						Ucm_componentsValidator.CONNECTION__MULTIPLICITY_CONSISTENCY,
						"Port " + port.getName() + " of part " + part.getName() + " is not delegated",
						new Object[] { part, port }));
			}
		}
		return ports;
	}

	private InteractionRole getRoleOfPort(ConnectorDefinition connector, PortTypeSpec portTypeSpec) {
		InteractionRole role = null;
		PortType portType = portTypeSpec.getType();
		ConnectorPort connectorPort = null;
		for (ConnectorPort cp : this.getConnectorPorts(connector)) {
			if (cp.getType() == portType) {
				connectorPort = cp;
			}
		}
		if (connectorPort != null) {
			role = connectorPort.getImplements();
		}
		return role;
	}

	public boolean multiplicityConsistency(Connection connection, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		boolean success = true;
		/*
		 * Compute the multiplicity of each role
		 */
		Hashtable<InteractionRole, Integer> roleMultiplicity = new Hashtable<InteractionRole, Integer>();
		IInteractionDefinition connectionDefinition = connection.getConnectionDefinition();
		for (ConnectionEnd connectionEnd : connection.getEndpoint()) {
			Port port = connectionEnd.getPort();
			AssemblyPart part = connectionEnd.getPart();
			LinkedList<Port> ports = this.getAllDelegatedPorts(port, part, diagnostics, context);

			for (Port p : ports) {
				IPortSpec portSpec = p.getSpec();
				if (connectionDefinition instanceof InteractionPattern && portSpec instanceof PortRoleSpec) {
					InteractionRole role = ((PortRoleSpec) portSpec).getRole();
					if (roleMultiplicity.containsKey(role)) {
						roleMultiplicity.put(role, roleMultiplicity.get(role) + 1);
					} else {
						roleMultiplicity.put(role, 1);
					}
				} else if (connectionDefinition instanceof ConnectorDefinition && portSpec instanceof PortTypeSpec) {
					InteractionRole role = this.getRoleOfPort((ConnectorDefinition) connectionDefinition,
							(PortTypeSpec) portSpec);
					if (role != null) {
						if (roleMultiplicity.containsKey(role)) {
							roleMultiplicity.put(role, roleMultiplicity.get(role) + 1);
						} else {
							roleMultiplicity.put(role, 1);
						}
					} else {
						if (diagnostics != null) {
							diagnostics.add(
									new BasicDiagnostic(Diagnostic.ERROR, Ucm_componentsValidator.DIAGNOSTIC_SOURCE,
											Ucm_componentsValidator.CONNECTION__MULTIPLICITY_CONSISTENCY,
											"Connection " + connection.getName() + " connects an inconsistent port: "
													+ port.getName(),
											new Object[] { connection, port }));
						}
					}

				} else {
					if (diagnostics != null) {
						diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_componentsValidator.DIAGNOSTIC_SOURCE,
								Ucm_componentsValidator.CONNECTION__MULTIPLICITY_CONSISTENCY, "Connection "
										+ connection.getName() + " connects an inconsistent port: " + port.getName(),
								new Object[] { connection, port }));
					}
					success = false;
				}
			}
		}

		/*
		 * then compare with the specifications
		 */
		for (InteractionRole role : roleMultiplicity.keySet()) {
			if (roleMultiplicity.get(role).intValue() < role.getLowerMultiplicity()
					|| (roleMultiplicity.get(role).intValue() > role.getUpperMultiplicity()
							&& role.getUpperMultiplicity() > 0)) {
				if (diagnostics != null) {
					diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_componentsValidator.DIAGNOSTIC_SOURCE,
							Ucm_componentsValidator.CONNECTION__MULTIPLICITY_CONSISTENCY,
							"Connection " + connection.getName() + " has multiplicity errors for role " + role.getName()
									+ " (" + roleMultiplicity.get(role).toString() + ")",
							new Object[] { connection, role }));

				}
				success = false;
			}
		}
		return success;
	}

	public boolean bindingConsistency(Connection connection, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean success = true;
		LinkedList<Port> ports = new LinkedList<Port>();
		/*
		 * We first retrieve all the ports involved in the connection
		 */
		for (ConnectionEnd connectionEnd : connection.getEndpoint()) {
			Port port = connectionEnd.getPort();
			AssemblyPart part = connectionEnd.getPart();
			ports.addAll(this.getAllDelegatedPorts(port, part, diagnostics, context));
		}

		/*
		 * If we have an interaction pattern, we get the items that must be
		 * bound, get the item binding of the first port and then check if all
		 * other ports have the same binding.
		 */
		if (connection.getConnectionDefinition() instanceof InteractionPattern) {
			InteractionPattern pattern = (InteractionPattern) connection.getConnectionDefinition();
			for (InteractionItem item : pattern.getItem()) {
				IConcreteTypeDeclaration firstBinding = null;
				for (Port port : ports) {
					boolean bindingIsConsistent = false;
					PortRoleSpec spec = (PortRoleSpec) port.getSpec();
					if (spec != null) {
						for (InteractionItemBinding binding : spec.getBinding()) {
							if (binding.getItem() == item) {
								if (firstBinding == null) {
									firstBinding = binding.getActualType();
									bindingIsConsistent = true;
								} else {
									bindingIsConsistent = (firstBinding == binding.getActualType());
								}
							}
						}
					}
					if (!bindingIsConsistent) {
						if (diagnostics != null) {
							diagnostics.add(
									new BasicDiagnostic(Diagnostic.ERROR, Ucm_componentsValidator.DIAGNOSTIC_SOURCE,
											Ucm_componentsValidator.CONNECTION__BINDING_CONSISTENCY,
											"bindings of item " + item.getName() + " in connection ends of "
													+ connection.getName() + " are inconsistent",
											new Object[] { connection }));
						}
						success = false;
					}
				}
			}
		}
		/*
		 * If we have a connector definition, there may be several possible
		 * bindings for a given interaction item.
		 */
		else if (connection.getConnectionDefinition() instanceof ConnectorDefinition) {
			ConnectorDefinition connector = (ConnectorDefinition) connection.getConnectionDefinition();
			InteractionPattern pattern = connector.getPattern();
			for (InteractionItem item : pattern.getItem()) {
				for (IITemBinding itemBinding : connector.getItemBinding()) {
					if (itemBinding.getInteractionItem() == item && itemBinding instanceof ItemBinding) {
						ITypeDeclaration itemType = ((ItemBinding) itemBinding).getConnectorItem();
						/*
						 * if the interaction item is directly bound to a
						 * concrete type, we are done.
						 */
						if (itemType instanceof IConcreteTypeDeclaration) {
							continue;
						}
						IConcreteTypeDeclaration firstBinding = null;
						for (Port port : ports) {
							boolean bindingIsConsistent = false;
							PortTypeSpec spec = (PortTypeSpec) port.getSpec();
							if (spec != null) {
								for (AbstractTypeBinding binding : spec.getBinding()) {
									if (binding.getAbstractType() == itemType) {
										if (firstBinding == null) {
											firstBinding = binding.getActualType();
											bindingIsConsistent = true;
										} else {
											bindingIsConsistent = (firstBinding == binding.getActualType());
										}
									}
								}
							}
							if (!bindingIsConsistent) {
								if (diagnostics != null) {
									diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR,
											Ucm_componentsValidator.DIAGNOSTIC_SOURCE,
											Ucm_componentsValidator.CONNECTION__BINDING_CONSISTENCY,
											"bindings of item " + item.getName() + " in connection ends of "
													+ connection.getName() + " are inconsistent",
											new Object[] { connection }));
								}
								success = false;
							}
						}
					}
				}
			}
		}

		return success;
	}

	public boolean noRefinementLoop(CompositeComponentImplementation compositeImpl, DiagnosticChain diagnostic,
			Map<Object, Object> context) {
		boolean success = true;
		boolean found = false;
		CompositeComponentImplementation refined = compositeImpl.getRefines();
		while (refined != null && !found) {
			if (refined == compositeImpl) {
				found = true;
			}
			refined = refined.getRefines();
		}
		if (found) {
			if (diagnostic != null) {
				diagnostic.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_componentsValidator.DIAGNOSTIC_SOURCE,
						Ucm_componentsValidator.COMPOSITE_COMPONENT_IMPLEMENTATION__NO_REFINEMENT_LOOP,
						"Composite component implementation " + compositeImpl.getName() + " refines itself",
						new Object[] { compositeImpl }));
			}
			success = false;
		}

		return success;
	}

	public boolean componentTypeConsistency(CompositeComponentImplementation compositeImpl, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		boolean success = true;
		CompositeComponentImplementation refined = compositeImpl.getRefines();
		if (refined != null) {
			ComponentType compType = compositeImpl.getType();
			ComponentType ancestorCompType = refined.getType();

			if (compType != ancestorCompType) {
				if (diagnostics != null) {
					diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_componentsValidator.DIAGNOSTIC_SOURCE,
							Ucm_componentsValidator.COMPOSITE_COMPONENT_IMPLEMENTATION__COMPONENT_TYPE_CONSISTENCY,
							"Composite component implementation " + compositeImpl.getName()
									+ " refines a composite that has a different component type",
							new Object[] { compositeImpl }));
				}
				success = false;
			}
		}
		return success;
	}

	public boolean noRefinementLoop(ComponentType compType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean success = true;
		boolean found = false;
		ComponentType refined = compType.getRefines();
		while (refined != null && !found) {
			if (refined == compType) {
				found = true;
			}
			refined = refined.getRefines();
		}
		if (found) {
			if (diagnostics != null) {
				diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_componentsValidator.DIAGNOSTIC_SOURCE,
						Ucm_componentsValidator.COMPONENT_TYPE__NO_REFINEMENT_LOOP,
						"Component type " + compType.getName() + " refines itself", new Object[] { compType }));
			}
			success = false;
		}

		return success;
	}

	public boolean policyApplicability(ComponentTechnicalPolicy policy, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		boolean success = true;
		TechnicalPolicyDefinition def = policy.getDefinition();
		if (policy instanceof ComponentPortTechnicalPolicy) {
			if (def.getApplicability() == TechnicalPolicyApplicability.ON_COMPONENT) {
				if (diagnostics != null) {
					diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_componentsValidator.DIAGNOSTIC_SOURCE,
							Ucm_componentsValidator.COMPONENT_PORT_TECHNICAL_POLICY__POLICY_APPLICABILITY,
							"policy " + policy.getName() + " should be a component policy ", new Object[] { policy }));
				}
				success = false;
			}
			if (((ComponentPortTechnicalPolicy) policy).getManagedPort().isEmpty()) {
				if (diagnostics != null) {
					diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_componentsValidator.DIAGNOSTIC_SOURCE,
							Ucm_componentsValidator.COMPONENT_PORT_TECHNICAL_POLICY__POLICY_APPLICABILITY,
							"policy " + policy.getName() + " manages no port ", new Object[] { policy }));
				}
			}

			for (Port port : ((ComponentPortTechnicalPolicy) policy).getManagedPort()) {
				ComponentType componentType = (ComponentType) port.eContainer();
				if (!policy.getManagedComponent().contains(componentType)) {
					if (diagnostics != null) {
						diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_componentsValidator.DIAGNOSTIC_SOURCE,
								Ucm_componentsValidator.COMPONENT_PORT_TECHNICAL_POLICY__POLICY_APPLICABILITY,
								"port " + port.getName() + " does not belong to a component managed by policy "
										+ policy.getName(),
								new Object[] { policy, port }));
					}
					success = false;
				}
			}
		} else if (policy instanceof ComponentTechnicalPolicy) {
			if (def.getApplicability() == TechnicalPolicyApplicability.ON_PORT) {
				if (diagnostics != null) {
					diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_componentsValidator.DIAGNOSTIC_SOURCE,
							Ucm_componentsValidator.COMPONENT_TECHNICAL_POLICY__POLICY_APPLICABILITY,
							"policy " + policy.getName() + " should be a component port policy ",
							new Object[] { policy }));
				}
				success = false;
			}
		}
		return success;
	}

	public boolean policyMultiplicity(AtomicComponentImplementation compImpl, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		TechnicalAspectMultiplicity multiplicities = new TechnicalAspectMultiplicity(compImpl);
		return this.policyMultiplicity(compImpl, multiplicities, diagnostics, context);
	}

	public boolean policyMultiplicity(AssemblyPart part, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (part.getComponentDefinition() instanceof AtomicComponentImplementation) {
			AtomicComponentImplementation compImpl = (AtomicComponentImplementation) part.getComponentDefinition();
			if (compImpl != null) {
				TechnicalAspectMultiplicity multiplicities = new TechnicalAspectMultiplicity(part);
				return this.policyMultiplicity(compImpl, multiplicities, diagnostics, context);
			}
		}
		return true;
	}

	public boolean policyMultiplicity(AtomicComponentImplementation compImpl,
			TechnicalAspectMultiplicity multiplicities, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean success = true;
		/*
		 * Compute the number of technical aspects related to the component
		 * implementation
		 */
		for (ComponentTechnicalPolicy policy : compImpl.getPolicy()) {
			TechnicalPolicyDefinition def = policy.getDefinition();
			TechnicalAspect aspect = def.getTechnicalAspect();
			multiplicities.incrementMultiplicity(aspect);
		}
		/*
		 * Compare with the multiplicity of each technical aspect. If the
		 * multiplicity does not match the specification, then generate an error
		 * message.
		 */
		for (TechnicalAspect aspect : multiplicities.getTechnicalAspects()) {
			boolean multiplicityIsOk = multiplicities.isConsistent(aspect);

			if (!multiplicityIsOk) {
				if (diagnostics != null) {
					diagnostics
							.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_contractsValidator.DIAGNOSTIC_SOURCE,
									Ucm_componentsValidator.ATOMIC_COMPONENT_IMPLEMENTATION__POLICY_MULTIPLICITY,
									"Atomic component " + compImpl.getName()
											+ " has multiplicity errors with technical aspect " + aspect.getName(),
									new Object[] { compImpl }));
				}
				success = false;
			}
		}

		return success;
	}

	public boolean completeTypeBinding(ComponentTechnicalPolicy policy, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		boolean success = true;
		AbstractTypeVisitor visitor = new AbstractTypeVisitor();
		TechnicalPolicyDefinition policyDefinition = policy.getDefinition();
		/*
		 * compute all the abstract types involved in the port elements of the
		 * policy
		 */
		AbstractTypeList abstractTypeList = new AbstractTypeList();
		for (PortElement pe : policyDefinition.getPortElement()) {
			AbstractTypeList abstractTypes = visitor.doSwitch(pe.getInterface());
			abstractTypeList.add(abstractTypes);
		}

		/*
		 * then check if there is a binding for these abstract types. If not,
		 * raise an error.
		 */
		for (IAbstractTypeDeclaration abstractType : abstractTypeList.getList()) {
			boolean abstractTypeIsBound = false;
			for (AbstractTypeBinding binding : policy.getBinding()) {
				if (binding.getAbstractType() == abstractType) {
					abstractTypeIsBound = true;
				}
			}
			if (!abstractTypeIsBound) {
				if (diagnostics != null) {
					diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_componentsValidator.DIAGNOSTIC_SOURCE,
							Ucm_componentsValidator.COMPONENT_TECHNICAL_POLICY__COMPLETE_TYPE_BINDING, "Policy "
									+ policy.getName() + " has no binding for abstract type " + abstractType.getName(),
							new Object[] { policy }));
				}
				success = false;
			}
		}
		return success;
	}

	public boolean attributeDelegationConsistency(AttributeDelegation attributeDelegation, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		boolean success = true;

		/*
		 * Check the data type consistency between the two attributes
		 */
		Attribute internalAttribute = attributeDelegation.getAttribute();
		Attribute externalAttribute = attributeDelegation.getExternalAttribute();
		if (internalAttribute.getType() != externalAttribute.getType()) {
			if (diagnostics != null) {
				diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_componentsValidator.DIAGNOSTIC_SOURCE,
						Ucm_componentsValidator.ATTRIBUTE_DELEGATION__ATTRIBUTE_DELEGATION_CONSISTENCY,
						"Attribute delegation " + attributeDelegation.getName() + " is wrong because "
								+ internalAttribute.getName() + " and attribute " + externalAttribute.getName()
								+ " has different data types",
						new Object[] { attributeDelegation }));
			}
			success = false;
		}

		/*
		 * Check if the internal attribute belongs to the internal part
		 */
		boolean internalAttributeIsOk = false;
		AssemblyPart part = attributeDelegation.getPart();
		ComponentType def = null;
		if (part.getComponentDefinition() instanceof ComponentType) {
			def = (ComponentType) part.getComponentDefinition();
		} else if (part.getComponentDefinition() instanceof IComponentImplementation) {
			def = ((IComponentImplementation) part.getComponentDefinition()).getType();
		}
		while (def != null) {
			if (def.getAttribute().contains(internalAttribute)) {
				internalAttributeIsOk = true;
			}
			def = def.getRefines();
		}
		if (!internalAttributeIsOk) {
			if (diagnostics != null) {
				diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_componentsValidator.DIAGNOSTIC_SOURCE,
						Ucm_componentsValidator.ATTRIBUTE_DELEGATION__ATTRIBUTE_DELEGATION_CONSISTENCY,
						"Attribute delegation " + attributeDelegation.getName() + " is wrong because "
								+ internalAttribute.getName() + " does not belong to " + part.getName(),
						new Object[] { attributeDelegation }));
			}
		}
		success = success && internalAttributeIsOk;

		/*
		 * Check if the external attribute belongs to the container of the
		 * attribute delegation
		 */
		boolean externalAttributeIsOk = false;
		CompositeComponentImplementation implem = (CompositeComponentImplementation) attributeDelegation.eContainer();
		def = null;
		if (implem != null) {
			def = implem.getType();

			while (def != null) {
				if (def.getAttribute().contains(externalAttribute)) {
					externalAttributeIsOk = true;
				}
				def = def.getRefines();
			}
			if (!externalAttributeIsOk) {
				if (diagnostics != null) {
					diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_componentsValidator.DIAGNOSTIC_SOURCE,
							Ucm_componentsValidator.ATTRIBUTE_DELEGATION__ATTRIBUTE_DELEGATION_CONSISTENCY,
							"Attribute delegation " + attributeDelegation.getName() + " is wrong because "
									+ externalAttribute.getName() + " does not belong to " + implem.getName(),
							new Object[] { attributeDelegation }));
				}
			}
			success = success && externalAttributeIsOk;
		}

		return success;
	}

	private boolean areSimilarlPortSpec(IPortSpec spec1, IPortSpec spec2) {
		/*
		 * We compare the two roles or port types, then check if the bindings
		 * match
		 */
		boolean specificationsAreSimilar = true;
		if (spec1 instanceof PortRoleSpec && spec2 instanceof PortRoleSpec) {
			PortRoleSpec roleSpec1 = (PortRoleSpec) spec1;
			PortRoleSpec roleSpec2 = (PortRoleSpec) spec2;
			if (roleSpec1.getRole() == roleSpec2.getRole()) {
				Hashtable<InteractionItem, ITypeDeclaration> table = new Hashtable<InteractionItem, ITypeDeclaration>();
				for (InteractionItemBinding binding : roleSpec1.getBinding()) {
					table.put(binding.getItem(), binding.getActualType());
				}
				for (InteractionItemBinding binding : roleSpec2.getBinding()) {
					if (!table.containsKey(binding.getItem())
							|| table.get(binding.getItem()) != binding.getActualType()) {
						specificationsAreSimilar = false;
					}
				}
			} else {
				specificationsAreSimilar = false;
			}
		} else if (spec1 instanceof PortRoleSpec && spec2 instanceof PortRoleSpec) {
			PortTypeSpec typeSpec1 = (PortTypeSpec) spec1;
			PortTypeSpec typeSpec2 = (PortTypeSpec) spec2;
			if (typeSpec1.getType() == typeSpec2.getType()) {
				Hashtable<IAbstractTypeDeclaration, ITypeDeclaration> table = new Hashtable<IAbstractTypeDeclaration, ITypeDeclaration>();
				for (AbstractTypeBinding binding : typeSpec1.getBinding()) {
					table.put(binding.getAbstractType(), binding.getActualType());
				}
				for (AbstractTypeBinding binding : typeSpec2.getBinding()) {
					if (!table.containsKey(binding.getAbstractType())
							|| table.get(binding.getAbstractType()) != binding.getActualType()) {
						specificationsAreSimilar = false;
					}
				}
			} else {
				specificationsAreSimilar = false;
			}
		} else {
			specificationsAreSimilar = false;
		}
		return specificationsAreSimilar;
	}

	public boolean portDelegationConsistency(PortDelegation portDelegation, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		boolean success = true;
		/*
		 * Check the consistency between the two port specifications
		 */
		Port internalPort = portDelegation.getPort();
		Port externalPort = portDelegation.getExternalPort();
		if (!this.areSimilarlPortSpec(internalPort.getSpec(), externalPort.getSpec())) {
			if (diagnostics != null) {
				diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_componentsValidator.DIAGNOSTIC_SOURCE,
						Ucm_componentsValidator.PORT_DELEGATION__PORT_DELEGATION_CONSISTENCY,
						"Port delegation " + portDelegation.getName()
								+ " is wrong because ports do not have compatible port specifications",
						new Object[] { portDelegation }));
			}
			success = false;
		}

		/*
		 * Check if the internal port belongs to the internal part
		 */
		boolean internalPortIsOk = false;
		AssemblyPart part = portDelegation.getPart();
		internalPortIsOk = (this.getComponentPorts(part.getComponentDefinition()).contains(internalPort));
		if (!internalPortIsOk) {
			if (diagnostics != null) {
				diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_componentsValidator.DIAGNOSTIC_SOURCE,
						Ucm_componentsValidator.PORT_DELEGATION__PORT_DELEGATION_CONSISTENCY,
						"Attribute delegation " + portDelegation.getName() + " is wrong because "
								+ internalPort.getName() + " does not belong to " + part.getName(),
						new Object[] { portDelegation }));
			}
		}
		success = success && internalPortIsOk;

		/*
		 * Check if the external attribute belongs to the container of the
		 * attribute delegation
		 */
		boolean externalPortIsOk = false;
		CompositeComponentImplementation implem = (CompositeComponentImplementation) portDelegation.eContainer();
		externalPortIsOk = (this.getComponentPorts(implem).contains(externalPort));
		if (!externalPortIsOk) {
			if (diagnostics != null) {
				diagnostics
						.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_componentsValidator.DIAGNOSTIC_SOURCE,
								Ucm_componentsValidator.PORT_DELEGATION__PORT_DELEGATION_CONSISTENCY,
								"Port delegation " + portDelegation.getName() + " is wrong because "
										+ externalPort.getName() + " does not belong to " + implem.getName(),
								new Object[] { portDelegation }));
			}
			success = success && externalPortIsOk;
		}

		return success;
	}

	public boolean oneConnectionForEachSubport(CompositeComponentImplementation compImpl, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		boolean success = true;
		ComponentFeatureListManager<AssemblyPart, Port> partPortList = new ComponentFeatureListManager<AssemblyPart, Port>();
		ComponentFeatureListManager<AssemblyPart, Attribute> partAttrList = new ComponentFeatureListManager<AssemblyPart, Attribute>();
		for (PortDelegation portDelegation : compImpl.getPortDelegation()) {
			if (partPortList.alreadyExists(portDelegation.getPart(), portDelegation.getPort())) {
				if (diagnostics != null) {
					diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_componentsValidator.DIAGNOSTIC_SOURCE,
							Ucm_componentsValidator.COMPOSITE_COMPONENT_IMPLEMENTATION__ONE_CONNECTION_FOR_EACH_SUBPORT,
							"In " + compImpl.getName() + ": Port " + portDelegation.getPort().getName() + " of part "
									+ portDelegation.getPart().getName() + " is connected several times",
							new Object[] { compImpl, portDelegation.getPart(), portDelegation.getPort() }));
				}
				success = false;
			}
			partPortList.register(portDelegation.getPart(), portDelegation.getPort());
		}
		for (Connection connection : compImpl.getInternalConnection()) {
			for (ConnectionEnd connectionEnd : connection.getEndpoint()) {
				if (partPortList.alreadyExists(connectionEnd.getPart(), connectionEnd.getPort())) {
					if (diagnostics != null) {
						diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_componentsValidator.DIAGNOSTIC_SOURCE,
								Ucm_componentsValidator.COMPOSITE_COMPONENT_IMPLEMENTATION__ONE_CONNECTION_FOR_EACH_SUBPORT,
								"In " + compImpl.getName() + ": Port " + connectionEnd.getPort().getName() + " of part "
										+ connectionEnd.getPart().getName() + " is connected several times",
								new Object[] { compImpl, connectionEnd.getPart(), connectionEnd.getPort() }));
					}
					success = false;
				}
				partPortList.register(connectionEnd.getPart(), connectionEnd.getPort());
			}
		}
		for (AttributeDelegation attributeDelegation : compImpl.getAttributeDelegation()) {
			if (partAttrList.alreadyExists(attributeDelegation.getPart(), attributeDelegation.getAttribute())) {
				if (diagnostics != null) {
					diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_componentsValidator.DIAGNOSTIC_SOURCE,
							Ucm_componentsValidator.COMPOSITE_COMPONENT_IMPLEMENTATION__ONE_CONNECTION_FOR_EACH_SUBPORT,
							"In " + compImpl.getName() + ": Attribute " + attributeDelegation.getAttribute().getName()
									+ " of part " + attributeDelegation.getPart().getName()
									+ " is connected several times",
							new Object[] { compImpl, attributeDelegation.getPart(),
									attributeDelegation.getAttribute() }));
				}
				success = false;
			}
			partAttrList.register(attributeDelegation.getPart(), attributeDelegation.getAttribute());
		}

		return success;
	}

	private LinkedList<Port> getComponentPorts(IComponent component) {
		LinkedList<Port> ports = new LinkedList<Port>();
		ComponentType componentType = null;
		if (component instanceof ComponentType) {
			componentType = (ComponentType) component;
		} else if (component instanceof IComponentImplementation) {
			componentType = ((IComponentImplementation) component).getType();
		}
		if (componentType != null) {
			if (componentType.getRefines() != null) {
				ports = this.getComponentPorts(componentType.getRefines());
			}
			ports.addAll(componentType.getPort());

			/*
			 * We remove all the ports that are refined
			 */
			for (Port port : componentType.getPort()) {
				if (port.getRefines() != null) {
					ports.remove(port.getRefines());
				}
			}

		}
		return ports;
	}

	private LinkedList<InteractionRole> getRoles(InteractionPattern pattern) {
		LinkedList<InteractionRole> roles;
		if (pattern.getExtends() != null) {
			roles = this.getRoles(pattern.getExtends());
		} else {
			roles = new LinkedList<InteractionRole>();
		}
		roles.addAll(pattern.getRole());
		return roles;
	}

	private LinkedList<ConnectorPort> getConnectorPorts(ConnectorDefinition connector) {
		LinkedList<ConnectorPort> ports;
		if (connector.getExtends() != null) {
			ports = this.getConnectorPorts(connector.getExtends());
		} else {
			ports = new LinkedList<ConnectorPort>();
		}
		ports.addAll(connector.getPort());
		return ports;
	}

	private LinkedList<PortType> getTypesOfConnectorPorts(ConnectorDefinition connector) {
		LinkedList<ConnectorPort> ports = this.getConnectorPorts(connector);
		LinkedList<PortType> portTypes = new LinkedList<PortType>();
		for (ConnectorPort port : ports) {
			portTypes.add((PortType) port.getType());
		}
		return portTypes;
	}

	public boolean portDefinitionCheck(ConnectionEnd connectionEnd, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		boolean success = true;
		Port port = connectionEnd.getPort();
		IPortSpec portSpec = port.getSpec();
		Connection connection = (Connection) connectionEnd.eContainer();
		IInteractionDefinition connectionDefinition = connection.getConnectionDefinition();

		/*
		 * check if the port belongs to the part
		 */
		AssemblyPart part = connectionEnd.getPart();
		success = (this.getComponentPorts(part.getComponentDefinition()).contains(port));
		if (!success) {
			if (diagnostics != null) {
				diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_componentsValidator.DIAGNOSTIC_SOURCE,
						Ucm_componentsValidator.CONNECTION_END__PORT_DEFINITION_CHECK,
						"Connection end " + connectionEnd.getName() + " is wrong because " + port.getName()
								+ " does not belong to " + part.getName(),
						new Object[] { connectionEnd }));
			}
		}

		/*
		 * check if the connection definition defines a role or a connector port
		 * that matches the port specification
		 */
		boolean specificationConsistency = false;
		if (connectionDefinition instanceof InteractionPattern && portSpec instanceof PortRoleSpec) {
			InteractionRole role = ((PortRoleSpec) portSpec).getRole();
			specificationConsistency = this.getRoles((InteractionPattern) connectionDefinition).contains(role);
		} else if (connectionDefinition instanceof ConnectorDefinition && portSpec instanceof PortTypeSpec) {
			PortType portType = ((PortTypeSpec) portSpec).getType();
			specificationConsistency = this.getTypesOfConnectorPorts((ConnectorDefinition) connectionDefinition)
					.contains(portType);
		}
		if (!specificationConsistency) {
			if (diagnostics != null) {
				diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_componentsValidator.DIAGNOSTIC_SOURCE,
						Ucm_componentsValidator.CONNECTION_END__PORT_DEFINITION_CHECK,
						"The definition of connection " + connectionEnd.getName()
								+ " does not provide any port definition that matches the port specification of "
								+ connectionEnd.getPort().getName(),
						new Object[] { connectionEnd }));
			}
			success = false;
		}
		return success;
	}

	public boolean specConsistency(Port port, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean success = true;
		IPortSpec portSpec = port.getSpec();
		if (portSpec instanceof PortRoleSpec) {
			InteractionRole role = ((PortRoleSpec) portSpec).getRole();
			for (InteractionItem item : role.getInvolvedItem()) {
				boolean itemIsBound = false;
				for (InteractionItemBinding binding : ((PortRoleSpec) portSpec).getBinding()) {
					if (binding.getItem() == item) {
						itemIsBound = true;
					}
				}
				if (!itemIsBound) {
					if (diagnostics != null) {
						diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_componentsValidator.DIAGNOSTIC_SOURCE,
								Ucm_componentsValidator.PORT__SPEC_CONSISTENCY,
								"port " + port.getName() + " has no binding for item " + item.getName(),
								new Object[] { port }));
					}
				}
			}
		} else if (portSpec instanceof PortTypeSpec) {
			PortType portType = ((PortTypeSpec) portSpec).getType();
			AbstractTypeVisitor visitor = new AbstractTypeVisitor();
			/*
			 * compute all the abstract types involved in the port elements of
			 * the port type
			 */
			AbstractTypeList abstractTypeList = new AbstractTypeList();
			for (PortElement pe : portType.getPortElement()) {
				AbstractTypeList abstractTypes = visitor.doSwitch(pe.getInterface());
				abstractTypeList.add(abstractTypes);
			}

			/*
			 * then check if there is a binding for these abstract types. If
			 * not, raise an error.
			 */
			for (IAbstractTypeDeclaration abstractType : abstractTypeList.getList()) {
				boolean abstractTypeIsBound = false;
				for (AbstractTypeBinding binding : ((PortTypeSpec) portSpec).getBinding()) {
					if (binding.getAbstractType() == abstractType) {
						abstractTypeIsBound = true;
					}
				}
				if (!abstractTypeIsBound) {
					if (diagnostics != null) {
						diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_componentsValidator.DIAGNOSTIC_SOURCE,
								Ucm_componentsValidator.COMPONENT_TECHNICAL_POLICY__COMPLETE_TYPE_BINDING,
								"Policy " + port.getName() + " has no binding for abstract type "
										+ abstractType.getName(),
								new Object[] { port }));
					}
					success = false;
				}
			}

		}

		return success;
	}
}
