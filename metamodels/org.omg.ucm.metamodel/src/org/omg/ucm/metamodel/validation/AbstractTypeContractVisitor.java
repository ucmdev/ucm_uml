package org.omg.ucm.metamodel.validation;

import org.eclipse.emf.ecore.EObject;

import ucm_base.ucm_contracts.AbstractDataType;
import ucm_base.ucm_contracts.AbstractInterface;
import ucm_base.ucm_contracts.Attribute;
import ucm_base.ucm_contracts.ExceptionField;
import ucm_base.ucm_contracts.Interface;
import ucm_base.ucm_contracts.Method;
import ucm_base.ucm_contracts.Parameter;
import ucm_base.ucm_contracts.util.Ucm_contractsSwitch;

class AbstractTypeContractVisitor extends Ucm_contractsSwitch<AbstractTypeList> {

	private AbstractTypeVisitor visitor;

	public AbstractTypeContractVisitor(AbstractTypeVisitor visitor) {
		this.visitor = visitor;
	}

	public AbstractTypeList caseAbstractInterface(AbstractInterface object) {
		AbstractTypeList list = new AbstractTypeList(object);
		return list;
	}

	public AbstractTypeList caseAbstractDataType(AbstractDataType object) {
		AbstractTypeList list = new AbstractTypeList(object);
		return list;
	}

	public AbstractTypeList caseInterface(Interface object) {
		AbstractTypeList list = new AbstractTypeList();
		for (Interface intf : object.getExtends()) {
			list.add(this.visitor.doSwitch(intf));
		}
		for (Method method : object.getMethod()) {
			list.add(this.visitor.doSwitch(method));
		}
		for (Attribute attribute : object.getAttribute()) {
			list.add(this.visitor.doSwitch(attribute));
		}
		return list;
	}

	public AbstractTypeList caseException(ucm_base.ucm_contracts.Exception object) {
		AbstractTypeList list = new AbstractTypeList();
		for (ExceptionField field : object.getField()) {
			list.add(this.visitor.doSwitch(field.getType()));
		}
		return list;
	}

	public AbstractTypeList caseMethod(Method object) {
		AbstractTypeList list = new AbstractTypeList();
		for (Parameter param : object.getParameter()) {
			list.add(this.visitor.doSwitch(param.getType()));
		}
		for (ucm_base.ucm_contracts.Exception except : object.getRaisedException()) {
			list.add(this.visitor.doSwitch(except));
		}
		return list;
	}

	public AbstractTypeList caseAttribute(Attribute object) {
		AbstractTypeList list = this.visitor.doSwitch(object.getType());
		return list;
	}

	public AbstractTypeList defaultCase(EObject object) {
		return null;
	}
}
