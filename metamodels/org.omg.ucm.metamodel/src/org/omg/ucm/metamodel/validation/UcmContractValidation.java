package org.omg.ucm.metamodel.validation;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;

import ucm_base.ucm_components.ComponentTechnicalPolicy;
import ucm_base.ucm_components.Connection;
import ucm_base.ucm_components.ConnectionEnd;
import ucm_base.ucm_components.Port;
import ucm_base.ucm_components.PortTypeSpec;
import ucm_base.ucm_contracts.Annotation;
import ucm_base.ucm_contracts.AnnotationDefinition;
import ucm_base.ucm_contracts.ConfigurationParameter;
import ucm_base.ucm_contracts.ConfigurationParameterValue;
import ucm_base.ucm_contracts.IConfigurable;
import ucm_base.ucm_contracts.IConfigured;
import ucm_base.ucm_contracts.Interface;
import ucm_base.ucm_contracts.util.Ucm_contractsValidator;
import ucm_base.ucm_interactions.ConnectorDefinition;
import ucm_base.ucm_interactions.ConnectorPort;
import ucm_base.ucm_interactions.ConnectorPortConfiguration;
import ucm_base.ucm_interactions.IInteractionDefinition;
import ucm_base.ucm_interactions.PortType;
import ucm_base.ucm_technicalpolicies.TechnicalPolicyDefinition;

public class UcmContractValidation {

	public LinkedList<IConfigurable> getConfigurables(IConfigured configured) {
		LinkedList<IConfigurable> list = new LinkedList<IConfigurable>();

		if (configured instanceof ComponentTechnicalPolicy) {
			TechnicalPolicyDefinition def = ((ComponentTechnicalPolicy) configured).getDefinition();
			while (def != null) {
				list.add(def);
				def = def.getExtends();
			}
		} else if (configured instanceof Connection) {
			IInteractionDefinition def = ((Connection) configured).getConnectionDefinition();
			if (def instanceof ConnectorDefinition) {
				ConnectorDefinition def2 = (ConnectorDefinition) def;
				while (def2 != null) {
					list.add(def2);
					def2 = def2.getExtends();
				}
			}
		} else if (configured instanceof ConnectionEnd) {
			Connection connection = (Connection) ((ConnectionEnd) configured).eContainer();
			Port port = ((ConnectionEnd) configured).getPort();
			list.addAll(this.getConnectorPortConfigurations(connection, port));
		} else if (configured instanceof Annotation) {
			AnnotationDefinition def = ((Annotation) configured).getAnnotationDefinition();
			list.add(def);
		}
		return list;
	}

	protected LinkedList<IConfigurable> getConnectorPortConfigurations(Connection connection, Port port) {
		LinkedList<IConfigurable> list = new LinkedList<IConfigurable>();
		PortTypeSpec spec = (PortTypeSpec) port.getSpec();
		ConnectorPort connectorPort = null;
		/*
		 * connection should normally be non-null. Spec is non-null if we are
		 * actually dealing with a port type spec.
		 */
		if (connection != null && spec != null) {
			PortType portType = spec.getType();
			ConnectorDefinition def = (ConnectorDefinition) connection.getConnectionDefinition();
			/*
			 * retrieve the connector port handled by the connection end
			 */
			if (def != null) {
				for (ConnectorPort cp : def.getPort()) {
					if (cp.getType() == portType) {
						connectorPort = cp;
					}
				}
			}
			/*
			 * retrieve all the port configurations of the connector and its
			 * ancestors.
			 */
			while (def != null && connectorPort != null) {
				for (ConnectorPortConfiguration cpc : def.getPortConfiguration()) {
					if (cpc.getPort() == connectorPort) {
						list.add(cpc);
					}
				}
				def = def.getExtends();
			}
		}
		return list;
	}

	public boolean configParamConsistency(ConfigurationParameterValue paramValue, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		boolean success = false;
		IConfigured configured = (IConfigured) paramValue.eContainer();
		ConfigurationParameter configParam = paramValue.getConfigurationParameter();
		if (configured != null) {
			LinkedList<IConfigurable> configurables = this.getConfigurables(configured);
			for (IConfigurable configurable : configurables) {
				if (configurable.getConfigurationParameter().contains(configParam)) {
					success = true;
				}
			}
		}
		if (!success) {
			if (diagnostics != null) {
				diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_contractsValidator.DIAGNOSTIC_SOURCE,
						Ucm_contractsValidator.CONFIGURATION_PARAMETER_VALUE__CONFIG_PARAM_CONSISTENCY,
						"The type of configuration parameter value " + paramValue.getValue()
								+ " cannot be found at the proper location",
						new Object[] { paramValue }));
			}
		}
		return success;
	}

	public boolean noExtensionLoop(Interface intf, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean success = true;
		boolean found = false;
		/*
		 * Hash set that stores all visited interfaces, to avoid endless loops.
		 */
		HashSet<Interface> interfaces = new HashSet<Interface>();
		interfaces.add(intf);

		HashSet<Interface> refined = new HashSet<Interface>(intf.getExtends());
		while (!refined.isEmpty() && !found) {
			/*
			 * for each ancestor interface, if this interface has not been
			 * visited yet, we get its ancestors. Then if the initial interface
			 * intf is found among these ancestor interfaces, we stop the
			 * search.
			 */
			HashSet<Interface> ancestorRefined = new HashSet<Interface>();
			for (Interface rIntf : refined) {
				if (!interfaces.contains(rIntf)) {
					ancestorRefined.addAll(rIntf.getExtends());
				}
			}
			if (ancestorRefined.contains(intf)) {
				found = true;
			}
			interfaces.addAll(ancestorRefined);
			refined = ancestorRefined;
		}
		if (found) {
			if (diagnostics != null) {
				diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_contractsValidator.DIAGNOSTIC_SOURCE,
						Ucm_contractsValidator.INTERFACE__NO_EXTENSION_LOOP,
						"Interface " + intf.getName() + " extends itself", new Object[] { this }));
			}
			success = false;
		}
		return success;
	}
}
