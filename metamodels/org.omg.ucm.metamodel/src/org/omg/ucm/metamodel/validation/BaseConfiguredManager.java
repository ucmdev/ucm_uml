package org.omg.ucm.metamodel.validation;

import java.util.LinkedList;

import ucm_base.ucm_components.ComponentTechnicalPolicy;
import ucm_base.ucm_components.Connection;
import ucm_base.ucm_components.ConnectionEnd;
import ucm_base.ucm_components.Port;
import ucm_base.ucm_components.PortTypeSpec;
import ucm_base.ucm_contracts.Annotation;
import ucm_base.ucm_contracts.AnnotationDefinition;
import ucm_base.ucm_contracts.IConfigurable;
import ucm_base.ucm_contracts.IConfigured;
import ucm_base.ucm_interactions.ConnectorDefinition;
import ucm_base.ucm_interactions.ConnectorPort;
import ucm_base.ucm_interactions.ConnectorPortConfiguration;
import ucm_base.ucm_interactions.IInteractionDefinition;
import ucm_base.ucm_interactions.PortType;
import ucm_base.ucm_technicalpolicies.TechnicalPolicyDefinition;

public class BaseConfiguredManager implements IConfiguredManager {

	public LinkedList<IConfigurable> getConfigurables(IConfigured configured) {
		LinkedList<IConfigurable> list = new LinkedList<IConfigurable>();

		if (configured instanceof ComponentTechnicalPolicy) {
			TechnicalPolicyDefinition def = ((ComponentTechnicalPolicy) configured).getDefinition();
			while (def != null) {
				list.add(def);
				def = def.getExtends();
			}
		} else if (configured instanceof Connection) {
			IInteractionDefinition def = ((Connection) configured).getConnectionDefinition();
			if (def instanceof ConnectorDefinition) {
				ConnectorDefinition def2 = (ConnectorDefinition) def;
				while (def2 != null) {
					list.add(def2);
					def2 = def2.getExtends();
				}
			}
		} else if (configured instanceof ConnectionEnd) {
			ConnectorPort connectorPort = null;
			Connection connection = (Connection) ((ConnectionEnd) configured).eContainer();
			Port port = ((ConnectionEnd) configured).getPort();
			PortTypeSpec spec = (PortTypeSpec) port.getSpec();
			/*
			 * connection should normally be non-null. Spec is non-null if we
			 * are actually dealing with a port type spec.
			 */
			if (connection != null && spec != null) {
				PortType portType = spec.getType();
				ConnectorDefinition def = (ConnectorDefinition) connection.getConnectionDefinition();
				/*
				 * retrieve the connector port handled by the connection end
				 */
				if (def != null) {
					for (ConnectorPort cp : def.getPort()) {
						if (cp.getType() == portType) {
							connectorPort = cp;
						}
					}
				}
				/*
				 * retrieve all the port configurations of the connector and its ancestors.
				 */
				while (def != null && connectorPort != null) {
					for (ConnectorPortConfiguration cpc : def.getPortConfiguration()) {
						if (cpc.getPort() == connectorPort) {
							list.add(cpc);
						}
					}
					def = def.getExtends();
				}
			}
		} else if (configured instanceof Annotation) {
			AnnotationDefinition def = ((Annotation) configured).getAnnotationDefinition();
			list.add(def);
		}
		return list;
	}
}
