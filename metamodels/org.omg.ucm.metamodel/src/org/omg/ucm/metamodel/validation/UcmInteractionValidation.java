package org.omg.ucm.metamodel.validation;

import java.util.HashSet;
import java.util.Map;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;

import ucm_base.ucm_interactions.ConnectorDefinition;
import ucm_base.ucm_interactions.ConnectorPort;
import ucm_base.ucm_interactions.IITemBinding;
import ucm_base.ucm_interactions.InteractionItem;
import ucm_base.ucm_interactions.InteractionPattern;
import ucm_base.ucm_interactions.InteractionRole;
import ucm_base.ucm_interactions.util.Ucm_interactionsValidator;

public class UcmInteractionValidation {

	public boolean noExtensionLoop(InteractionPattern pattern, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		boolean success = true;
		boolean found = false;
		/*
		 * Hash set that stores all visited connectors, to avoid endless loops.
		 */
		HashSet<InteractionPattern> patterns = new HashSet<InteractionPattern>();
		patterns.add(pattern);
		InteractionPattern refined = pattern.getExtends();
		while (refined != null && !found && !patterns.contains(pattern)) {
			patterns.add(pattern);
			if (refined == pattern) {
				found = true;
			}
			refined = refined.getExtends();
		}
		if (found) {
			if (diagnostics != null) {
				diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_interactionsValidator.DIAGNOSTIC_SOURCE,
						Ucm_interactionsValidator.INTERACTION_PATTERN__NO_EXTENSION_LOOP,
						"Interaction pattern " + pattern.getName() + " refines itself", new Object[] { pattern }));
			}
			success = false;
		}
		return success;
	}

	public boolean noExtensionLoop(ConnectorDefinition connector, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		boolean success = true;
		boolean found = false;
		/*
		 * Hash set that stores all visited connectors, to avoid endless loops.
		 */
		HashSet<ConnectorDefinition> connectors = new HashSet<ConnectorDefinition>();
		connectors.add(connector);
		ConnectorDefinition refined = connector.getExtends();
		
		if (refined != null)
		{
			if (connector.getPattern() != refined.getPattern()) {
				if (diagnostics != null) {
					diagnostics
							.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_interactionsValidator.DIAGNOSTIC_SOURCE,
									Ucm_interactionsValidator.CONNECTOR_DEFINITION__NO_EXTENSION_LOOP,
									"Connector definitions " + connector.getName() + " and " + refined.getName()
											+ " correspond to different interaction patterns",
									new Object[] { connector, refined }));
				}
				success = false;
			}
		}
		
		while (refined != null && !found && !connectors.contains(refined)) {
			connectors.add(refined);
			if (refined == connector) {
				found = true;
			}
			refined = refined.getExtends();
		}
		if (found) {
			if (diagnostics != null) {
				diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_interactionsValidator.DIAGNOSTIC_SOURCE,
						Ucm_interactionsValidator.CONNECTOR_DEFINITION__NO_EXTENSION_LOOP,
						"Connector definition " + connector.getName() + " refines itself", new Object[] { connector }));
			}
			success = false;
		}
		return success;
	}

	/**
	 * Check that all items of the interaction pattern are bound to something in
	 * the connector
	 * 
	 * @param connector
	 * @param diagnostics
	 * @param context
	 * @return
	 */
	public boolean completeItemBinding(ConnectorDefinition connector, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		boolean success = true;
		InteractionPattern pattern = connector.getPattern();
		for (InteractionItem item : pattern.getItem()) {
			ConnectorDefinition co = connector;
			boolean itemIsBound = false;
			while (co != null) {
				for (IITemBinding itemBinding : co.getItemBinding()) {
					if (itemBinding.getInteractionItem() == item) {
						itemIsBound = true;
					}
				}
				co = co.getExtends();
			}
			if (!itemIsBound) {
				if (diagnostics != null) {
					diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_interactionsValidator.DIAGNOSTIC_SOURCE,
							Ucm_interactionsValidator.CONNECTOR_DEFINITION__COMPLETE_ITEM_BINDING, "connector "
									+ connector.getName() + " has no binding for interaction item " + item.getName(),
							new Object[] { connector, item }));
				}
				success = false;
			}
		}

		return success;
	}

	public boolean completeRoleImplementation(ConnectorDefinition connector, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		boolean success = true;
		InteractionPattern pattern = connector.getPattern();
		for (InteractionRole role : pattern.getRole()) {
			ConnectorDefinition co = connector;
			boolean roleIsImplemented = false;
			while (co != null) {
				for (ConnectorPort port : co.getPort()) {
					if (port.getImplements() == role) {
						roleIsImplemented = true;
					}
				}
				co = co.getExtends();
			}
			if (!roleIsImplemented) {
				if (diagnostics != null) {
					diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_interactionsValidator.DIAGNOSTIC_SOURCE,
							Ucm_interactionsValidator.CONNECTOR_DEFINITION__COMPLETE_ROLE_IMPLEMENTATION,
							"connector " + connector.getName() + " defines no port for role " + role.getName(),
							new Object[] { connector, role }));
				}
				success = false;
			}
		}

		return success;
	}
}
