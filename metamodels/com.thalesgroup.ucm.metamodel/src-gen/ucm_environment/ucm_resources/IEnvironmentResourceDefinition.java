/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_environment.ucm_resources;

import ucm_base.ucm_commons.INamed;
import ucm_base.ucm_contracts.IConfigurable;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IEnvironment Resource Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getIEnvironmentResourceDefinition()
 * @model abstract="true"
 * @generated
 */
public interface IEnvironmentResourceDefinition extends IConfigurable, INamed {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

} // IEnvironmentResourceDefinition
