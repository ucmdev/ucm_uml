/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_environment.ucm_resources;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Computation Resource Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_environment.ucm_resources.ComputationResourceDefinition#getComputationPort <em>Computation Port</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.ComputationResourceDefinition#getPerformanceFactor <em>Performance Factor</em>}</li>
 * </ul>
 *
 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getComputationResourceDefinition()
 * @model
 * @generated
 */
public interface ComputationResourceDefinition extends IEnvironmentResourceDefinition {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Computation Port</b></em>' containment reference list.
	 * The list contents are of type {@link ucm_environment.ucm_resources.ComputationResourcePort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Computation Port</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Computation Port</em>' containment reference list.
	 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getComputationResourceDefinition_ComputationPort()
	 * @model containment="true"
	 * @generated
	 */
	EList<ComputationResourcePort> getComputationPort();

	/**
	 * Returns the value of the '<em><b>Performance Factor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Performance Factor</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Performance Factor</em>' attribute.
	 * @see #setPerformanceFactor(double)
	 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getComputationResourceDefinition_PerformanceFactor()
	 * @model required="true"
	 * @generated
	 */
	double getPerformanceFactor();

	/**
	 * Sets the value of the '{@link ucm_environment.ucm_resources.ComputationResourceDefinition#getPerformanceFactor <em>Performance Factor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Performance Factor</em>' attribute.
	 * @see #getPerformanceFactor()
	 * @generated
	 */
	void setPerformanceFactor(double value);

} // ComputationResourceDefinition
