/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_environment.ucm_resources;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage
 * @generated
 */
public interface Ucm_resourcesFactory extends EFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Ucm_resourcesFactory eINSTANCE = ucm_environment.ucm_resources.impl.Ucm_resourcesFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Resource Definition Module</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Resource Definition Module</em>'.
	 * @generated
	 */
	ResourceDefinitionModule createResourceDefinitionModule();

	/**
	 * Returns a new object of class '<em>Environment Module</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Environment Module</em>'.
	 * @generated
	 */
	EnvironmentModule createEnvironmentModule();

	/**
	 * Returns a new object of class '<em>IEnvironment Resource</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>IEnvironment Resource</em>'.
	 * @generated
	 */
	IEnvironmentResource createIEnvironmentResource();

	/**
	 * Returns a new object of class '<em>Computation Resource Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Computation Resource Definition</em>'.
	 * @generated
	 */
	ComputationResourceDefinition createComputationResourceDefinition();

	/**
	 * Returns a new object of class '<em>Computation Resource Port</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Computation Resource Port</em>'.
	 * @generated
	 */
	ComputationResourcePort createComputationResourcePort();

	/**
	 * Returns a new object of class '<em>Communication Resource Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Communication Resource Definition</em>'.
	 * @generated
	 */
	CommunicationResourceDefinition createCommunicationResourceDefinition();

	/**
	 * Returns a new object of class '<em>Communication Resource Port</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Communication Resource Port</em>'.
	 * @generated
	 */
	CommunicationResourcePort createCommunicationResourcePort();

	/**
	 * Returns a new object of class '<em>Communication Port Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Communication Port Definition</em>'.
	 * @generated
	 */
	CommunicationPortDefinition createCommunicationPortDefinition();

	/**
	 * Returns a new object of class '<em>Memory Partition Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Memory Partition Definition</em>'.
	 * @generated
	 */
	MemoryPartitionDefinition createMemoryPartitionDefinition();

	/**
	 * Returns a new object of class '<em>Execution Resource Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Execution Resource Definition</em>'.
	 * @generated
	 */
	ExecutionResourceDefinition createExecutionResourceDefinition();

	/**
	 * Returns a new object of class '<em>Computation Resource</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Computation Resource</em>'.
	 * @generated
	 */
	ComputationResource createComputationResource();

	/**
	 * Returns a new object of class '<em>Communication Resource</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Communication Resource</em>'.
	 * @generated
	 */
	CommunicationResource createCommunicationResource();

	/**
	 * Returns a new object of class '<em>Communication Port</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Communication Port</em>'.
	 * @generated
	 */
	CommunicationPort createCommunicationPort();

	/**
	 * Returns a new object of class '<em>Execution Resource</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Execution Resource</em>'.
	 * @generated
	 */
	ExecutionResource createExecutionResource();

	/**
	 * Returns a new object of class '<em>Resource Allocation Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Resource Allocation Parameter</em>'.
	 * @generated
	 */
	ResourceAllocationParameter createResourceAllocationParameter();

	/**
	 * Returns a new object of class '<em>Resource Allocation Parameter Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Resource Allocation Parameter Value</em>'.
	 * @generated
	 */
	ResourceAllocationParameterValue createResourceAllocationParameterValue();

	/**
	 * Returns a new object of class '<em>Memory Partition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Memory Partition</em>'.
	 * @generated
	 */
	MemoryPartition createMemoryPartition();

	/**
	 * Returns a new object of class '<em>Resource Allocation And Configuration Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Resource Allocation And Configuration Parameter</em>'.
	 * @generated
	 */
	ResourceAllocationAndConfigurationParameter createResourceAllocationAndConfigurationParameter();

	/**
	 * Returns a new object of class '<em>Resource Allocation And Configuration Parameter Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Resource Allocation And Configuration Parameter Value</em>'.
	 * @generated
	 */
	ResourceAllocationAndConfigurationParameterValue createResourceAllocationAndConfigurationParameterValue();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	Ucm_resourcesPackage getUcm_resourcesPackage();

} //Ucm_resourcesFactory
