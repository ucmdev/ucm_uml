/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_environment.ucm_resources.util;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import ucm_base.ucm_commons.IApplicationModule;
import ucm_base.ucm_commons.IModule;
import ucm_base.ucm_commons.INamed;
import ucm_base.ucm_commons.IPlatformModule;

import ucm_base.ucm_contracts.IAnnotable;
import ucm_base.ucm_contracts.IConfigurable;
import ucm_base.ucm_contracts.IConfigurationParameter;
import ucm_base.ucm_contracts.IConfigurationParameterValue;
import ucm_base.ucm_contracts.IConfigured;
import ucm_base.ucm_contracts.IHasDatatype;
import ucm_base.ucm_contracts.IHasDefaultValue;
import ucm_base.ucm_contracts.IValued;

import ucm_environment.ucm_resources.*;

import ucm_supplement.ucm_deployments.IComponentNode;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage
 * @generated
 */
public class Ucm_resourcesSwitch<T> {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static Ucm_resourcesPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ucm_resourcesSwitch() {
		if (modelPackage == null) {
			modelPackage = Ucm_resourcesPackage.eINSTANCE;
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	public T doSwitch(EObject theEObject) {
		return doSwitch(theEObject.eClass(), theEObject);
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(EClass theEClass, EObject theEObject) {
		if (theEClass.eContainer() == modelPackage) {
			return doSwitch(theEClass.getClassifierID(), theEObject);
		}
		else {
			List<EClass> eSuperTypes = theEClass.getESuperTypes();
			return
				eSuperTypes.isEmpty() ?
					defaultCase(theEObject) :
					doSwitch(eSuperTypes.get(0), theEObject);
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE: {
				ResourceDefinitionModule resourceDefinitionModule = (ResourceDefinitionModule)theEObject;
				T result = caseResourceDefinitionModule(resourceDefinitionModule);
				if (result == null) result = caseIPlatformModule(resourceDefinitionModule);
				if (result == null) result = caseIModule(resourceDefinitionModule);
				if (result == null) result = caseINamed(resourceDefinitionModule);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_resourcesPackage.ENVIRONMENT_MODULE: {
				EnvironmentModule environmentModule = (EnvironmentModule)theEObject;
				T result = caseEnvironmentModule(environmentModule);
				if (result == null) result = caseIApplicationModule(environmentModule);
				if (result == null) result = caseIModule(environmentModule);
				if (result == null) result = caseINamed(environmentModule);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_resourcesPackage.IENVIRONMENT_RESOURCE_DEFINITION: {
				IEnvironmentResourceDefinition iEnvironmentResourceDefinition = (IEnvironmentResourceDefinition)theEObject;
				T result = caseIEnvironmentResourceDefinition(iEnvironmentResourceDefinition);
				if (result == null) result = caseIConfigurable(iEnvironmentResourceDefinition);
				if (result == null) result = caseINamed(iEnvironmentResourceDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_resourcesPackage.IENVIRONMENT_RESOURCE: {
				IEnvironmentResource iEnvironmentResource = (IEnvironmentResource)theEObject;
				T result = caseIEnvironmentResource(iEnvironmentResource);
				if (result == null) result = caseIConfigured(iEnvironmentResource);
				if (result == null) result = caseINamed(iEnvironmentResource);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_resourcesPackage.COMPUTATION_RESOURCE_DEFINITION: {
				ComputationResourceDefinition computationResourceDefinition = (ComputationResourceDefinition)theEObject;
				T result = caseComputationResourceDefinition(computationResourceDefinition);
				if (result == null) result = caseIEnvironmentResourceDefinition(computationResourceDefinition);
				if (result == null) result = caseIConfigurable(computationResourceDefinition);
				if (result == null) result = caseINamed(computationResourceDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_resourcesPackage.COMPUTATION_RESOURCE_PORT: {
				ComputationResourcePort computationResourcePort = (ComputationResourcePort)theEObject;
				T result = caseComputationResourcePort(computationResourcePort);
				if (result == null) result = caseIConfigurable(computationResourcePort);
				if (result == null) result = caseINamed(computationResourcePort);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE_DEFINITION: {
				CommunicationResourceDefinition communicationResourceDefinition = (CommunicationResourceDefinition)theEObject;
				T result = caseCommunicationResourceDefinition(communicationResourceDefinition);
				if (result == null) result = caseIEnvironmentResourceDefinition(communicationResourceDefinition);
				if (result == null) result = caseIConfigurable(communicationResourceDefinition);
				if (result == null) result = caseINamed(communicationResourceDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE_PORT: {
				CommunicationResourcePort communicationResourcePort = (CommunicationResourcePort)theEObject;
				T result = caseCommunicationResourcePort(communicationResourcePort);
				if (result == null) result = caseIConfigurable(communicationResourcePort);
				if (result == null) result = caseINamed(communicationResourcePort);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_resourcesPackage.COMMUNICATION_PORT_DEFINITION: {
				CommunicationPortDefinition communicationPortDefinition = (CommunicationPortDefinition)theEObject;
				T result = caseCommunicationPortDefinition(communicationPortDefinition);
				if (result == null) result = caseIEnvironmentResourceDefinition(communicationPortDefinition);
				if (result == null) result = caseIConfigurable(communicationPortDefinition);
				if (result == null) result = caseINamed(communicationPortDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_resourcesPackage.MEMORY_PARTITION_DEFINITION: {
				MemoryPartitionDefinition memoryPartitionDefinition = (MemoryPartitionDefinition)theEObject;
				T result = caseMemoryPartitionDefinition(memoryPartitionDefinition);
				if (result == null) result = caseIEnvironmentResourceDefinition(memoryPartitionDefinition);
				if (result == null) result = caseIConfigurable(memoryPartitionDefinition);
				if (result == null) result = caseINamed(memoryPartitionDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_resourcesPackage.EXECUTION_RESOURCE_DEFINITION: {
				ExecutionResourceDefinition executionResourceDefinition = (ExecutionResourceDefinition)theEObject;
				T result = caseExecutionResourceDefinition(executionResourceDefinition);
				if (result == null) result = caseIEnvironmentResourceDefinition(executionResourceDefinition);
				if (result == null) result = caseIConfigurable(executionResourceDefinition);
				if (result == null) result = caseINamed(executionResourceDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_resourcesPackage.COMPUTATION_RESOURCE: {
				ComputationResource computationResource = (ComputationResource)theEObject;
				T result = caseComputationResource(computationResource);
				if (result == null) result = caseIEnvironmentResource(computationResource);
				if (result == null) result = caseIConfigured(computationResource);
				if (result == null) result = caseINamed(computationResource);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE: {
				CommunicationResource communicationResource = (CommunicationResource)theEObject;
				T result = caseCommunicationResource(communicationResource);
				if (result == null) result = caseIEnvironmentResource(communicationResource);
				if (result == null) result = caseIConfigured(communicationResource);
				if (result == null) result = caseINamed(communicationResource);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_resourcesPackage.COMMUNICATION_PORT: {
				CommunicationPort communicationPort = (CommunicationPort)theEObject;
				T result = caseCommunicationPort(communicationPort);
				if (result == null) result = caseIEnvironmentResource(communicationPort);
				if (result == null) result = caseIConfigured(communicationPort);
				if (result == null) result = caseINamed(communicationPort);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_resourcesPackage.EXECUTION_RESOURCE: {
				ExecutionResource executionResource = (ExecutionResource)theEObject;
				T result = caseExecutionResource(executionResource);
				if (result == null) result = caseIEnvironmentResource(executionResource);
				if (result == null) result = caseIConfigured(executionResource);
				if (result == null) result = caseINamed(executionResource);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_PARAMETER: {
				ResourceAllocationParameter resourceAllocationParameter = (ResourceAllocationParameter)theEObject;
				T result = caseResourceAllocationParameter(resourceAllocationParameter);
				if (result == null) result = caseIConfigurationParameter(resourceAllocationParameter);
				if (result == null) result = caseINamed(resourceAllocationParameter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_PARAMETER_VALUE: {
				ResourceAllocationParameterValue resourceAllocationParameterValue = (ResourceAllocationParameterValue)theEObject;
				T result = caseResourceAllocationParameterValue(resourceAllocationParameterValue);
				if (result == null) result = caseIConfigurationParameterValue(resourceAllocationParameterValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_resourcesPackage.MEMORY_PARTITION: {
				MemoryPartition memoryPartition = (MemoryPartition)theEObject;
				T result = caseMemoryPartition(memoryPartition);
				if (result == null) result = caseIComponentNode(memoryPartition);
				if (result == null) result = caseINamed(memoryPartition);
				if (result == null) result = caseIAnnotable(memoryPartition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER: {
				ResourceAllocationAndConfigurationParameter resourceAllocationAndConfigurationParameter = (ResourceAllocationAndConfigurationParameter)theEObject;
				T result = caseResourceAllocationAndConfigurationParameter(resourceAllocationAndConfigurationParameter);
				if (result == null) result = caseIConfigurationParameter(resourceAllocationAndConfigurationParameter);
				if (result == null) result = caseIHasDatatype(resourceAllocationAndConfigurationParameter);
				if (result == null) result = caseIHasDefaultValue(resourceAllocationAndConfigurationParameter);
				if (result == null) result = caseINamed(resourceAllocationAndConfigurationParameter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER_VALUE: {
				ResourceAllocationAndConfigurationParameterValue resourceAllocationAndConfigurationParameterValue = (ResourceAllocationAndConfigurationParameterValue)theEObject;
				T result = caseResourceAllocationAndConfigurationParameterValue(resourceAllocationAndConfigurationParameterValue);
				if (result == null) result = caseIConfigurationParameterValue(resourceAllocationAndConfigurationParameterValue);
				if (result == null) result = caseIValued(resourceAllocationAndConfigurationParameterValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Resource Definition Module</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Resource Definition Module</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResourceDefinitionModule(ResourceDefinitionModule object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Environment Module</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Environment Module</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEnvironmentModule(EnvironmentModule object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IEnvironment Resource Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IEnvironment Resource Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIEnvironmentResourceDefinition(IEnvironmentResourceDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IEnvironment Resource</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IEnvironment Resource</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIEnvironmentResource(IEnvironmentResource object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Computation Resource Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Computation Resource Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComputationResourceDefinition(ComputationResourceDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Computation Resource Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Computation Resource Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComputationResourcePort(ComputationResourcePort object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Communication Resource Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Communication Resource Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCommunicationResourceDefinition(CommunicationResourceDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Communication Resource Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Communication Resource Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCommunicationResourcePort(CommunicationResourcePort object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Communication Port Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Communication Port Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCommunicationPortDefinition(CommunicationPortDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Memory Partition Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Memory Partition Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMemoryPartitionDefinition(MemoryPartitionDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Execution Resource Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Execution Resource Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExecutionResourceDefinition(ExecutionResourceDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Computation Resource</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Computation Resource</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComputationResource(ComputationResource object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Communication Resource</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Communication Resource</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCommunicationResource(CommunicationResource object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Communication Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Communication Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCommunicationPort(CommunicationPort object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Execution Resource</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Execution Resource</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExecutionResource(ExecutionResource object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Resource Allocation Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Resource Allocation Parameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResourceAllocationParameter(ResourceAllocationParameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Resource Allocation Parameter Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Resource Allocation Parameter Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResourceAllocationParameterValue(ResourceAllocationParameterValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Memory Partition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Memory Partition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMemoryPartition(MemoryPartition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Resource Allocation And Configuration Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Resource Allocation And Configuration Parameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResourceAllocationAndConfigurationParameter(ResourceAllocationAndConfigurationParameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Resource Allocation And Configuration Parameter Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Resource Allocation And Configuration Parameter Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResourceAllocationAndConfigurationParameterValue(ResourceAllocationAndConfigurationParameterValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>INamed</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>INamed</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseINamed(INamed object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IModule</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IModule</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIModule(IModule object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IPlatform Module</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IPlatform Module</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIPlatformModule(IPlatformModule object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IApplication Module</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IApplication Module</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIApplicationModule(IApplicationModule object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IConfigurable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IConfigurable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIConfigurable(IConfigurable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IConfigured</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IConfigured</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIConfigured(IConfigured object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IConfiguration Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IConfiguration Parameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIConfigurationParameter(IConfigurationParameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IConfiguration Parameter Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IConfiguration Parameter Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIConfigurationParameterValue(IConfigurationParameterValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IAnnotable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IAnnotable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIAnnotable(IAnnotable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IComponent Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IComponent Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIComponentNode(IComponentNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IHas Datatype</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IHas Datatype</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIHasDatatype(IHasDatatype object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IHas Default Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IHas Default Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIHasDefaultValue(IHasDefaultValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IValued</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IValued</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIValued(IValued object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public T defaultCase(EObject object) {
		return null;
	}

} //Ucm_resourcesSwitch
