/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_environment.ucm_resources.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import ucm_base.ucm_commons.IApplicationModule;
import ucm_base.ucm_commons.IModule;
import ucm_base.ucm_commons.INamed;
import ucm_base.ucm_commons.IPlatformModule;

import ucm_base.ucm_contracts.IAnnotable;
import ucm_base.ucm_contracts.IConfigurable;
import ucm_base.ucm_contracts.IConfigurationParameter;
import ucm_base.ucm_contracts.IConfigurationParameterValue;
import ucm_base.ucm_contracts.IConfigured;
import ucm_base.ucm_contracts.IHasDatatype;
import ucm_base.ucm_contracts.IHasDefaultValue;
import ucm_base.ucm_contracts.IValued;

import ucm_environment.ucm_resources.*;

import ucm_supplement.ucm_deployments.IComponentNode;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage
 * @generated
 */
public class Ucm_resourcesAdapterFactory extends AdapterFactoryImpl {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static Ucm_resourcesPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ucm_resourcesAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = Ucm_resourcesPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Ucm_resourcesSwitch<Adapter> modelSwitch =
		new Ucm_resourcesSwitch<Adapter>() {
			@Override
			public Adapter caseResourceDefinitionModule(ResourceDefinitionModule object) {
				return createResourceDefinitionModuleAdapter();
			}
			@Override
			public Adapter caseEnvironmentModule(EnvironmentModule object) {
				return createEnvironmentModuleAdapter();
			}
			@Override
			public Adapter caseIEnvironmentResourceDefinition(IEnvironmentResourceDefinition object) {
				return createIEnvironmentResourceDefinitionAdapter();
			}
			@Override
			public Adapter caseIEnvironmentResource(IEnvironmentResource object) {
				return createIEnvironmentResourceAdapter();
			}
			@Override
			public Adapter caseComputationResourceDefinition(ComputationResourceDefinition object) {
				return createComputationResourceDefinitionAdapter();
			}
			@Override
			public Adapter caseComputationResourcePort(ComputationResourcePort object) {
				return createComputationResourcePortAdapter();
			}
			@Override
			public Adapter caseCommunicationResourceDefinition(CommunicationResourceDefinition object) {
				return createCommunicationResourceDefinitionAdapter();
			}
			@Override
			public Adapter caseCommunicationResourcePort(CommunicationResourcePort object) {
				return createCommunicationResourcePortAdapter();
			}
			@Override
			public Adapter caseCommunicationPortDefinition(CommunicationPortDefinition object) {
				return createCommunicationPortDefinitionAdapter();
			}
			@Override
			public Adapter caseMemoryPartitionDefinition(MemoryPartitionDefinition object) {
				return createMemoryPartitionDefinitionAdapter();
			}
			@Override
			public Adapter caseExecutionResourceDefinition(ExecutionResourceDefinition object) {
				return createExecutionResourceDefinitionAdapter();
			}
			@Override
			public Adapter caseComputationResource(ComputationResource object) {
				return createComputationResourceAdapter();
			}
			@Override
			public Adapter caseCommunicationResource(CommunicationResource object) {
				return createCommunicationResourceAdapter();
			}
			@Override
			public Adapter caseCommunicationPort(CommunicationPort object) {
				return createCommunicationPortAdapter();
			}
			@Override
			public Adapter caseExecutionResource(ExecutionResource object) {
				return createExecutionResourceAdapter();
			}
			@Override
			public Adapter caseResourceAllocationParameter(ResourceAllocationParameter object) {
				return createResourceAllocationParameterAdapter();
			}
			@Override
			public Adapter caseResourceAllocationParameterValue(ResourceAllocationParameterValue object) {
				return createResourceAllocationParameterValueAdapter();
			}
			@Override
			public Adapter caseMemoryPartition(MemoryPartition object) {
				return createMemoryPartitionAdapter();
			}
			@Override
			public Adapter caseResourceAllocationAndConfigurationParameter(ResourceAllocationAndConfigurationParameter object) {
				return createResourceAllocationAndConfigurationParameterAdapter();
			}
			@Override
			public Adapter caseResourceAllocationAndConfigurationParameterValue(ResourceAllocationAndConfigurationParameterValue object) {
				return createResourceAllocationAndConfigurationParameterValueAdapter();
			}
			@Override
			public Adapter caseINamed(INamed object) {
				return createINamedAdapter();
			}
			@Override
			public Adapter caseIModule(IModule object) {
				return createIModuleAdapter();
			}
			@Override
			public Adapter caseIPlatformModule(IPlatformModule object) {
				return createIPlatformModuleAdapter();
			}
			@Override
			public Adapter caseIApplicationModule(IApplicationModule object) {
				return createIApplicationModuleAdapter();
			}
			@Override
			public Adapter caseIConfigurable(IConfigurable object) {
				return createIConfigurableAdapter();
			}
			@Override
			public Adapter caseIConfigured(IConfigured object) {
				return createIConfiguredAdapter();
			}
			@Override
			public Adapter caseIConfigurationParameter(IConfigurationParameter object) {
				return createIConfigurationParameterAdapter();
			}
			@Override
			public Adapter caseIConfigurationParameterValue(IConfigurationParameterValue object) {
				return createIConfigurationParameterValueAdapter();
			}
			@Override
			public Adapter caseIAnnotable(IAnnotable object) {
				return createIAnnotableAdapter();
			}
			@Override
			public Adapter caseIComponentNode(IComponentNode object) {
				return createIComponentNodeAdapter();
			}
			@Override
			public Adapter caseIHasDatatype(IHasDatatype object) {
				return createIHasDatatypeAdapter();
			}
			@Override
			public Adapter caseIHasDefaultValue(IHasDefaultValue object) {
				return createIHasDefaultValueAdapter();
			}
			@Override
			public Adapter caseIValued(IValued object) {
				return createIValuedAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link ucm_environment.ucm_resources.ResourceDefinitionModule <em>Resource Definition Module</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_environment.ucm_resources.ResourceDefinitionModule
	 * @generated
	 */
	public Adapter createResourceDefinitionModuleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_environment.ucm_resources.EnvironmentModule <em>Environment Module</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_environment.ucm_resources.EnvironmentModule
	 * @generated
	 */
	public Adapter createEnvironmentModuleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_environment.ucm_resources.IEnvironmentResourceDefinition <em>IEnvironment Resource Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_environment.ucm_resources.IEnvironmentResourceDefinition
	 * @generated
	 */
	public Adapter createIEnvironmentResourceDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_environment.ucm_resources.IEnvironmentResource <em>IEnvironment Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_environment.ucm_resources.IEnvironmentResource
	 * @generated
	 */
	public Adapter createIEnvironmentResourceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_environment.ucm_resources.ComputationResourceDefinition <em>Computation Resource Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_environment.ucm_resources.ComputationResourceDefinition
	 * @generated
	 */
	public Adapter createComputationResourceDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_environment.ucm_resources.ComputationResourcePort <em>Computation Resource Port</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_environment.ucm_resources.ComputationResourcePort
	 * @generated
	 */
	public Adapter createComputationResourcePortAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_environment.ucm_resources.CommunicationResourceDefinition <em>Communication Resource Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_environment.ucm_resources.CommunicationResourceDefinition
	 * @generated
	 */
	public Adapter createCommunicationResourceDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_environment.ucm_resources.CommunicationResourcePort <em>Communication Resource Port</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_environment.ucm_resources.CommunicationResourcePort
	 * @generated
	 */
	public Adapter createCommunicationResourcePortAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_environment.ucm_resources.CommunicationPortDefinition <em>Communication Port Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_environment.ucm_resources.CommunicationPortDefinition
	 * @generated
	 */
	public Adapter createCommunicationPortDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_environment.ucm_resources.MemoryPartitionDefinition <em>Memory Partition Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_environment.ucm_resources.MemoryPartitionDefinition
	 * @generated
	 */
	public Adapter createMemoryPartitionDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_environment.ucm_resources.ExecutionResourceDefinition <em>Execution Resource Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_environment.ucm_resources.ExecutionResourceDefinition
	 * @generated
	 */
	public Adapter createExecutionResourceDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_environment.ucm_resources.ComputationResource <em>Computation Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_environment.ucm_resources.ComputationResource
	 * @generated
	 */
	public Adapter createComputationResourceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_environment.ucm_resources.CommunicationResource <em>Communication Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_environment.ucm_resources.CommunicationResource
	 * @generated
	 */
	public Adapter createCommunicationResourceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_environment.ucm_resources.CommunicationPort <em>Communication Port</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_environment.ucm_resources.CommunicationPort
	 * @generated
	 */
	public Adapter createCommunicationPortAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_environment.ucm_resources.ExecutionResource <em>Execution Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_environment.ucm_resources.ExecutionResource
	 * @generated
	 */
	public Adapter createExecutionResourceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_environment.ucm_resources.ResourceAllocationParameter <em>Resource Allocation Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_environment.ucm_resources.ResourceAllocationParameter
	 * @generated
	 */
	public Adapter createResourceAllocationParameterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_environment.ucm_resources.ResourceAllocationParameterValue <em>Resource Allocation Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_environment.ucm_resources.ResourceAllocationParameterValue
	 * @generated
	 */
	public Adapter createResourceAllocationParameterValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_environment.ucm_resources.MemoryPartition <em>Memory Partition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_environment.ucm_resources.MemoryPartition
	 * @generated
	 */
	public Adapter createMemoryPartitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_environment.ucm_resources.ResourceAllocationAndConfigurationParameter <em>Resource Allocation And Configuration Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_environment.ucm_resources.ResourceAllocationAndConfigurationParameter
	 * @generated
	 */
	public Adapter createResourceAllocationAndConfigurationParameterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_environment.ucm_resources.ResourceAllocationAndConfigurationParameterValue <em>Resource Allocation And Configuration Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_environment.ucm_resources.ResourceAllocationAndConfigurationParameterValue
	 * @generated
	 */
	public Adapter createResourceAllocationAndConfigurationParameterValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_base.ucm_commons.INamed <em>INamed</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_base.ucm_commons.INamed
	 * @generated
	 */
	public Adapter createINamedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_base.ucm_commons.IModule <em>IModule</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_base.ucm_commons.IModule
	 * @generated
	 */
	public Adapter createIModuleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_base.ucm_commons.IPlatformModule <em>IPlatform Module</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_base.ucm_commons.IPlatformModule
	 * @generated
	 */
	public Adapter createIPlatformModuleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_base.ucm_commons.IApplicationModule <em>IApplication Module</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_base.ucm_commons.IApplicationModule
	 * @generated
	 */
	public Adapter createIApplicationModuleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_base.ucm_contracts.IConfigurable <em>IConfigurable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_base.ucm_contracts.IConfigurable
	 * @generated
	 */
	public Adapter createIConfigurableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_base.ucm_contracts.IConfigured <em>IConfigured</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_base.ucm_contracts.IConfigured
	 * @generated
	 */
	public Adapter createIConfiguredAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_base.ucm_contracts.IConfigurationParameter <em>IConfiguration Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_base.ucm_contracts.IConfigurationParameter
	 * @generated
	 */
	public Adapter createIConfigurationParameterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_base.ucm_contracts.IConfigurationParameterValue <em>IConfiguration Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_base.ucm_contracts.IConfigurationParameterValue
	 * @generated
	 */
	public Adapter createIConfigurationParameterValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_base.ucm_contracts.IAnnotable <em>IAnnotable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_base.ucm_contracts.IAnnotable
	 * @generated
	 */
	public Adapter createIAnnotableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_supplement.ucm_deployments.IComponentNode <em>IComponent Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_supplement.ucm_deployments.IComponentNode
	 * @generated
	 */
	public Adapter createIComponentNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_base.ucm_contracts.IHasDatatype <em>IHas Datatype</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_base.ucm_contracts.IHasDatatype
	 * @generated
	 */
	public Adapter createIHasDatatypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_base.ucm_contracts.IHasDefaultValue <em>IHas Default Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_base.ucm_contracts.IHasDefaultValue
	 * @generated
	 */
	public Adapter createIHasDefaultValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_base.ucm_contracts.IValued <em>IValued</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_base.ucm_contracts.IValued
	 * @generated
	 */
	public Adapter createIValuedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //Ucm_resourcesAdapterFactory
