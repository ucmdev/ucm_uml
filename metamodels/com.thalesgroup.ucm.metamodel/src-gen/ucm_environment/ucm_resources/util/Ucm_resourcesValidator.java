/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_environment.ucm_resources.util;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

import ucm_environment.ucm_resources.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage
 * @generated
 */
public class Ucm_resourcesValidator extends EObjectValidator {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final Ucm_resourcesValidator INSTANCE = new Ucm_resourcesValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "ucm_environment.ucm_resources";

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'One Connection Per Port' of 'Environment Module'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int ENVIRONMENT_MODULE__ONE_CONNECTION_PER_PORT = 1;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Initialization Is Complete' of 'Environment Module'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int ENVIRONMENT_MODULE__INITIALIZATION_IS_COMPLETE = 2;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Port Cardinality' of 'Communication Resource'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int COMMUNICATION_RESOURCE__PORT_CARDINALITY = 3;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Parameter Definition Consistency' of 'Resource Allocation Parameter Value'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int RESOURCE_ALLOCATION_PARAMETER_VALUE__PARAMETER_DEFINITION_CONSISTENCY = 4;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 4;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ucm_resourcesValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return Ucm_resourcesPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE:
				return validateResourceDefinitionModule((ResourceDefinitionModule)value, diagnostics, context);
			case Ucm_resourcesPackage.ENVIRONMENT_MODULE:
				return validateEnvironmentModule((EnvironmentModule)value, diagnostics, context);
			case Ucm_resourcesPackage.IENVIRONMENT_RESOURCE_DEFINITION:
				return validateIEnvironmentResourceDefinition((IEnvironmentResourceDefinition)value, diagnostics, context);
			case Ucm_resourcesPackage.IENVIRONMENT_RESOURCE:
				return validateIEnvironmentResource((IEnvironmentResource)value, diagnostics, context);
			case Ucm_resourcesPackage.COMPUTATION_RESOURCE_DEFINITION:
				return validateComputationResourceDefinition((ComputationResourceDefinition)value, diagnostics, context);
			case Ucm_resourcesPackage.COMPUTATION_RESOURCE_PORT:
				return validateComputationResourcePort((ComputationResourcePort)value, diagnostics, context);
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE_DEFINITION:
				return validateCommunicationResourceDefinition((CommunicationResourceDefinition)value, diagnostics, context);
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE_PORT:
				return validateCommunicationResourcePort((CommunicationResourcePort)value, diagnostics, context);
			case Ucm_resourcesPackage.COMMUNICATION_PORT_DEFINITION:
				return validateCommunicationPortDefinition((CommunicationPortDefinition)value, diagnostics, context);
			case Ucm_resourcesPackage.MEMORY_PARTITION_DEFINITION:
				return validateMemoryPartitionDefinition((MemoryPartitionDefinition)value, diagnostics, context);
			case Ucm_resourcesPackage.EXECUTION_RESOURCE_DEFINITION:
				return validateExecutionResourceDefinition((ExecutionResourceDefinition)value, diagnostics, context);
			case Ucm_resourcesPackage.COMPUTATION_RESOURCE:
				return validateComputationResource((ComputationResource)value, diagnostics, context);
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE:
				return validateCommunicationResource((CommunicationResource)value, diagnostics, context);
			case Ucm_resourcesPackage.COMMUNICATION_PORT:
				return validateCommunicationPort((CommunicationPort)value, diagnostics, context);
			case Ucm_resourcesPackage.EXECUTION_RESOURCE:
				return validateExecutionResource((ExecutionResource)value, diagnostics, context);
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_PARAMETER:
				return validateResourceAllocationParameter((ResourceAllocationParameter)value, diagnostics, context);
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_PARAMETER_VALUE:
				return validateResourceAllocationParameterValue((ResourceAllocationParameterValue)value, diagnostics, context);
			case Ucm_resourcesPackage.MEMORY_PARTITION:
				return validateMemoryPartition((MemoryPartition)value, diagnostics, context);
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER:
				return validateResourceAllocationAndConfigurationParameter((ResourceAllocationAndConfigurationParameter)value, diagnostics, context);
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER_VALUE:
				return validateResourceAllocationAndConfigurationParameterValue((ResourceAllocationAndConfigurationParameterValue)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateResourceDefinitionModule(ResourceDefinitionModule resourceDefinitionModule, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(resourceDefinitionModule, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEnvironmentModule(EnvironmentModule environmentModule, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(environmentModule, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(environmentModule, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(environmentModule, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(environmentModule, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(environmentModule, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(environmentModule, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(environmentModule, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(environmentModule, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(environmentModule, diagnostics, context);
		if (result || diagnostics != null) result &= validateEnvironmentModule_oneConnectionPerPort(environmentModule, diagnostics, context);
		if (result || diagnostics != null) result &= validateEnvironmentModule_initializationIsComplete(environmentModule, diagnostics, context);
		return result;
	}

	/**
	 * Validates the oneConnectionPerPort constraint of '<em>Environment Module</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEnvironmentModule_oneConnectionPerPort(EnvironmentModule environmentModule, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return environmentModule.oneConnectionPerPort(diagnostics, context);
	}

	/**
	 * Validates the initializationIsComplete constraint of '<em>Environment Module</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEnvironmentModule_initializationIsComplete(EnvironmentModule environmentModule, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return environmentModule.initializationIsComplete(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIEnvironmentResourceDefinition(IEnvironmentResourceDefinition iEnvironmentResourceDefinition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(iEnvironmentResourceDefinition, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIEnvironmentResource(IEnvironmentResource iEnvironmentResource, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(iEnvironmentResource, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComputationResourceDefinition(ComputationResourceDefinition computationResourceDefinition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(computationResourceDefinition, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComputationResourcePort(ComputationResourcePort computationResourcePort, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(computationResourcePort, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCommunicationResourceDefinition(CommunicationResourceDefinition communicationResourceDefinition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(communicationResourceDefinition, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCommunicationResourcePort(CommunicationResourcePort communicationResourcePort, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(communicationResourcePort, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCommunicationPortDefinition(CommunicationPortDefinition communicationPortDefinition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(communicationPortDefinition, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMemoryPartitionDefinition(MemoryPartitionDefinition memoryPartitionDefinition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(memoryPartitionDefinition, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateExecutionResourceDefinition(ExecutionResourceDefinition executionResourceDefinition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(executionResourceDefinition, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComputationResource(ComputationResource computationResource, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(computationResource, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCommunicationResource(CommunicationResource communicationResource, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(communicationResource, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(communicationResource, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(communicationResource, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(communicationResource, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(communicationResource, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(communicationResource, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(communicationResource, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(communicationResource, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(communicationResource, diagnostics, context);
		if (result || diagnostics != null) result &= validateCommunicationResource_portCardinality(communicationResource, diagnostics, context);
		return result;
	}

	/**
	 * Validates the portCardinality constraint of '<em>Communication Resource</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCommunicationResource_portCardinality(CommunicationResource communicationResource, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return communicationResource.portCardinality(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCommunicationPort(CommunicationPort communicationPort, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(communicationPort, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateExecutionResource(ExecutionResource executionResource, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(executionResource, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateResourceAllocationParameter(ResourceAllocationParameter resourceAllocationParameter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(resourceAllocationParameter, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateResourceAllocationParameterValue(ResourceAllocationParameterValue resourceAllocationParameterValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(resourceAllocationParameterValue, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(resourceAllocationParameterValue, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(resourceAllocationParameterValue, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(resourceAllocationParameterValue, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(resourceAllocationParameterValue, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(resourceAllocationParameterValue, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(resourceAllocationParameterValue, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(resourceAllocationParameterValue, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(resourceAllocationParameterValue, diagnostics, context);
		if (result || diagnostics != null) result &= validateResourceAllocationParameterValue_parameterDefinitionConsistency(resourceAllocationParameterValue, diagnostics, context);
		return result;
	}

	/**
	 * Validates the parameterDefinitionConsistency constraint of '<em>Resource Allocation Parameter Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateResourceAllocationParameterValue_parameterDefinitionConsistency(ResourceAllocationParameterValue resourceAllocationParameterValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return resourceAllocationParameterValue.parameterDefinitionConsistency(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMemoryPartition(MemoryPartition memoryPartition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(memoryPartition, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateResourceAllocationAndConfigurationParameter(ResourceAllocationAndConfigurationParameter resourceAllocationAndConfigurationParameter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(resourceAllocationAndConfigurationParameter, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateResourceAllocationAndConfigurationParameterValue(ResourceAllocationAndConfigurationParameterValue resourceAllocationAndConfigurationParameterValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(resourceAllocationAndConfigurationParameterValue, diagnostics, context);
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //Ucm_resourcesValidator
