/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_environment.ucm_resources;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Execution Resource</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_environment.ucm_resources.ExecutionResource#getDefinition <em>Definition</em>}</li>
 * </ul>
 *
 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getExecutionResource()
 * @model
 * @generated
 */
public interface ExecutionResource extends IEnvironmentResource {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Definition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Definition</em>' reference.
	 * @see #setDefinition(ExecutionResourceDefinition)
	 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getExecutionResource_Definition()
	 * @model required="true"
	 * @generated
	 */
	ExecutionResourceDefinition getDefinition();

	/**
	 * Sets the value of the '{@link ucm_environment.ucm_resources.ExecutionResource#getDefinition <em>Definition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Definition</em>' reference.
	 * @see #getDefinition()
	 * @generated
	 */
	void setDefinition(ExecutionResourceDefinition value);

} // ExecutionResource
