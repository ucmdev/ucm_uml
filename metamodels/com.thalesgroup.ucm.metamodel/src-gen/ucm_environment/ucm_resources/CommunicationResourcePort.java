/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_environment.ucm_resources;

import ucm_base.ucm_commons.INamed;
import ucm_base.ucm_contracts.IConfigurable;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Communication Resource Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_environment.ucm_resources.CommunicationResourcePort#getDefinition <em>Definition</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.CommunicationResourcePort#getMin <em>Min</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.CommunicationResourcePort#getMax <em>Max</em>}</li>
 * </ul>
 *
 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getCommunicationResourcePort()
 * @model
 * @generated
 */
public interface CommunicationResourcePort extends IConfigurable, INamed {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Definition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Definition</em>' reference.
	 * @see #setDefinition(CommunicationPortDefinition)
	 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getCommunicationResourcePort_Definition()
	 * @model required="true"
	 * @generated
	 */
	CommunicationPortDefinition getDefinition();

	/**
	 * Sets the value of the '{@link ucm_environment.ucm_resources.CommunicationResourcePort#getDefinition <em>Definition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Definition</em>' reference.
	 * @see #getDefinition()
	 * @generated
	 */
	void setDefinition(CommunicationPortDefinition value);

	/**
	 * Returns the value of the '<em><b>Min</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min</em>' attribute.
	 * @see #setMin(long)
	 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getCommunicationResourcePort_Min()
	 * @model default="1" required="true"
	 * @generated
	 */
	long getMin();

	/**
	 * Sets the value of the '{@link ucm_environment.ucm_resources.CommunicationResourcePort#getMin <em>Min</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min</em>' attribute.
	 * @see #getMin()
	 * @generated
	 */
	void setMin(long value);

	/**
	 * Returns the value of the '<em><b>Max</b></em>' attribute.
	 * The default value is <code>"-1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max</em>' attribute.
	 * @see #setMax(long)
	 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getCommunicationResourcePort_Max()
	 * @model default="-1" required="true"
	 * @generated
	 */
	long getMax();

	/**
	 * Sets the value of the '{@link ucm_environment.ucm_resources.CommunicationResourcePort#getMax <em>Max</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max</em>' attribute.
	 * @see #getMax()
	 * @generated
	 */
	void setMax(long value);

} // CommunicationResourcePort
