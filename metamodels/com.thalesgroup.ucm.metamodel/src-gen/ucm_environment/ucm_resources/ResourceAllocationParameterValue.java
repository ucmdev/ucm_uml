/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_environment.ucm_resources;

import java.util.Map;
import org.eclipse.emf.common.util.DiagnosticChain;
import ucm_base.ucm_contracts.IConfigurationParameterValue;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Resource Allocation Parameter Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_environment.ucm_resources.ResourceAllocationParameterValue#getParameterDefinition <em>Parameter Definition</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.ResourceAllocationParameterValue#getResource <em>Resource</em>}</li>
 * </ul>
 *
 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getResourceAllocationParameterValue()
 * @model
 * @generated
 */
public interface ResourceAllocationParameterValue extends IConfigurationParameterValue {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Parameter Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Definition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Definition</em>' reference.
	 * @see #setParameterDefinition(ResourceAllocationParameter)
	 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getResourceAllocationParameterValue_ParameterDefinition()
	 * @model required="true"
	 * @generated
	 */
	ResourceAllocationParameter getParameterDefinition();

	/**
	 * Sets the value of the '{@link ucm_environment.ucm_resources.ResourceAllocationParameterValue#getParameterDefinition <em>Parameter Definition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter Definition</em>' reference.
	 * @see #getParameterDefinition()
	 * @generated
	 */
	void setParameterDefinition(ResourceAllocationParameter value);

	/**
	 * Returns the value of the '<em><b>Resource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource</em>' reference.
	 * @see #setResource(IEnvironmentResource)
	 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getResourceAllocationParameterValue_Resource()
	 * @model required="true"
	 * @generated
	 */
	IEnvironmentResource getResource();

	/**
	 * Sets the value of the '{@link ucm_environment.ucm_resources.ResourceAllocationParameterValue#getResource <em>Resource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Resource</em>' reference.
	 * @see #getResource()
	 * @generated
	 */
	void setResource(IEnvironmentResource value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean parameterDefinitionConsistency(DiagnosticChain diagnostics, Map<Object, Object> context);

} // ResourceAllocationParameterValue
