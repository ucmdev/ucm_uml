/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_environment.ucm_resources;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Computation Resource</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_environment.ucm_resources.ComputationResource#getDefinition <em>Definition</em>}</li>
 * </ul>
 *
 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getComputationResource()
 * @model
 * @generated
 */
public interface ComputationResource extends IEnvironmentResource {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Definition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Definition</em>' reference.
	 * @see #setDefinition(ComputationResourceDefinition)
	 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getComputationResource_Definition()
	 * @model required="true"
	 * @generated
	 */
	ComputationResourceDefinition getDefinition();

	/**
	 * Sets the value of the '{@link ucm_environment.ucm_resources.ComputationResource#getDefinition <em>Definition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Definition</em>' reference.
	 * @see #getDefinition()
	 * @generated
	 */
	void setDefinition(ComputationResourceDefinition value);

} // ComputationResource
