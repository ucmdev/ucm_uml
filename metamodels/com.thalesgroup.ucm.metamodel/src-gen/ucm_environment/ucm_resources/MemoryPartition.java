/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_environment.ucm_resources;

import org.eclipse.emf.common.util.EList;

import ucm_base.ucm_contracts.IConfigurationParameterValue;

import ucm_supplement.ucm_deployments.IComponentNode;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Memory Partition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_environment.ucm_resources.MemoryPartition#getMemoryPartitionDefinition <em>Memory Partition Definition</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.MemoryPartition#getConfigurationParameterValue <em>Configuration Parameter Value</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.MemoryPartition#getExecutionResource <em>Execution Resource</em>}</li>
 * </ul>
 *
 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getMemoryPartition()
 * @model
 * @generated
 */
public interface MemoryPartition extends IComponentNode {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Memory Partition Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Memory Partition Definition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Memory Partition Definition</em>' reference.
	 * @see #setMemoryPartitionDefinition(MemoryPartitionDefinition)
	 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getMemoryPartition_MemoryPartitionDefinition()
	 * @model required="true"
	 * @generated
	 */
	MemoryPartitionDefinition getMemoryPartitionDefinition();

	/**
	 * Sets the value of the '{@link ucm_environment.ucm_resources.MemoryPartition#getMemoryPartitionDefinition <em>Memory Partition Definition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Memory Partition Definition</em>' reference.
	 * @see #getMemoryPartitionDefinition()
	 * @generated
	 */
	void setMemoryPartitionDefinition(MemoryPartitionDefinition value);

	/**
	 * Returns the value of the '<em><b>Configuration Parameter Value</b></em>' containment reference list.
	 * The list contents are of type {@link ucm_base.ucm_contracts.IConfigurationParameterValue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Configuration Parameter Value</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Configuration Parameter Value</em>' containment reference list.
	 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getMemoryPartition_ConfigurationParameterValue()
	 * @model containment="true"
	 * @generated
	 */
	EList<IConfigurationParameterValue> getConfigurationParameterValue();

	/**
	 * Returns the value of the '<em><b>Execution Resource</b></em>' containment reference list.
	 * The list contents are of type {@link ucm_environment.ucm_resources.ExecutionResource}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Execution Resource</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Execution Resource</em>' containment reference list.
	 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getMemoryPartition_ExecutionResource()
	 * @model containment="true"
	 * @generated
	 */
	EList<ExecutionResource> getExecutionResource();

} // MemoryPartition
