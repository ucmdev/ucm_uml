/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_environment.ucm_resources;

import java.util.Map;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Communication Resource</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_environment.ucm_resources.CommunicationResource#getDefinition <em>Definition</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.CommunicationResource#getCommunicationPort <em>Communication Port</em>}</li>
 * </ul>
 *
 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getCommunicationResource()
 * @model
 * @generated
 */
public interface CommunicationResource extends IEnvironmentResource {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Definition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Definition</em>' reference.
	 * @see #setDefinition(CommunicationResourceDefinition)
	 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getCommunicationResource_Definition()
	 * @model required="true"
	 * @generated
	 */
	CommunicationResourceDefinition getDefinition();

	/**
	 * Sets the value of the '{@link ucm_environment.ucm_resources.CommunicationResource#getDefinition <em>Definition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Definition</em>' reference.
	 * @see #getDefinition()
	 * @generated
	 */
	void setDefinition(CommunicationResourceDefinition value);

	/**
	 * Returns the value of the '<em><b>Communication Port</b></em>' containment reference list.
	 * The list contents are of type {@link ucm_environment.ucm_resources.CommunicationPort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Communication Port</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Communication Port</em>' containment reference list.
	 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getCommunicationResource_CommunicationPort()
	 * @model containment="true"
	 * @generated
	 */
	EList<CommunicationPort> getCommunicationPort();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean portCardinality(DiagnosticChain diagnostics, Map<Object, Object> context);

} // CommunicationResource
