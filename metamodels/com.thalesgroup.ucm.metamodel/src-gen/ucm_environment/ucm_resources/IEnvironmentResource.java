/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_environment.ucm_resources;

import ucm_base.ucm_commons.INamed;
import ucm_base.ucm_contracts.IConfigured;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IEnvironment Resource</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getIEnvironmentResource()
 * @model
 * @generated
 */
public interface IEnvironmentResource extends IConfigured, INamed {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

} // IEnvironmentResource
