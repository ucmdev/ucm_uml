/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_environment.ucm_resources;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import ucm_base.ucm_commons.Ucm_commonsPackage;

import ucm_base.ucm_contracts.Ucm_contractsPackage;

import ucm_supplement.ucm_deployments.Ucm_deploymentsPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ucm_environment.ucm_resources.Ucm_resourcesFactory
 * @model kind="package"
 * @generated
 */
public interface Ucm_resourcesPackage extends EPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ucm_resources";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://com.thalesgroup.ucm/1.0/metamodels/environment/resources";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ucm_resources";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Ucm_resourcesPackage eINSTANCE = ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl.init();

	/**
	 * The meta object id for the '{@link ucm_environment.ucm_resources.impl.ResourceDefinitionModuleImpl <em>Resource Definition Module</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_environment.ucm_resources.impl.ResourceDefinitionModuleImpl
	 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getResourceDefinitionModule()
	 * @generated
	 */
	int RESOURCE_DEFINITION_MODULE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_DEFINITION_MODULE__NAME = Ucm_commonsPackage.IPLATFORM_MODULE__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_DEFINITION_MODULE__COMMENT = Ucm_commonsPackage.IPLATFORM_MODULE__COMMENT;

	/**
	 * The feature id for the '<em><b>Computation Resource Definition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_DEFINITION_MODULE__COMPUTATION_RESOURCE_DEFINITION = Ucm_commonsPackage.IPLATFORM_MODULE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Communication Resource Definition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_DEFINITION_MODULE__COMMUNICATION_RESOURCE_DEFINITION = Ucm_commonsPackage.IPLATFORM_MODULE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Execution Resource Definition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_DEFINITION_MODULE__EXECUTION_RESOURCE_DEFINITION = Ucm_commonsPackage.IPLATFORM_MODULE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Memory Partition Definition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_DEFINITION_MODULE__MEMORY_PARTITION_DEFINITION = Ucm_commonsPackage.IPLATFORM_MODULE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Contract Module</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_DEFINITION_MODULE__CONTRACT_MODULE = Ucm_commonsPackage.IPLATFORM_MODULE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Communication Port Definition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_DEFINITION_MODULE__COMMUNICATION_PORT_DEFINITION = Ucm_commonsPackage.IPLATFORM_MODULE_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Resource Definition Module</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_DEFINITION_MODULE_FEATURE_COUNT = Ucm_commonsPackage.IPLATFORM_MODULE_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link ucm_environment.ucm_resources.impl.EnvironmentModuleImpl <em>Environment Module</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_environment.ucm_resources.impl.EnvironmentModuleImpl
	 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getEnvironmentModule()
	 * @generated
	 */
	int ENVIRONMENT_MODULE = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_MODULE__NAME = Ucm_commonsPackage.IAPPLICATION_MODULE__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_MODULE__COMMENT = Ucm_commonsPackage.IAPPLICATION_MODULE__COMMENT;

	/**
	 * The feature id for the '<em><b>Computation Resource</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_MODULE__COMPUTATION_RESOURCE = Ucm_commonsPackage.IAPPLICATION_MODULE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Communication Resource</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_MODULE__COMMUNICATION_RESOURCE = Ucm_commonsPackage.IAPPLICATION_MODULE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Environment Module</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_MODULE_FEATURE_COUNT = Ucm_commonsPackage.IAPPLICATION_MODULE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link ucm_environment.ucm_resources.impl.IEnvironmentResourceDefinitionImpl <em>IEnvironment Resource Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_environment.ucm_resources.impl.IEnvironmentResourceDefinitionImpl
	 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getIEnvironmentResourceDefinition()
	 * @generated
	 */
	int IENVIRONMENT_RESOURCE_DEFINITION = 2;

	/**
	 * The feature id for the '<em><b>Configuration Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IENVIRONMENT_RESOURCE_DEFINITION__CONFIGURATION_PARAMETER = Ucm_contractsPackage.ICONFIGURABLE__CONFIGURATION_PARAMETER;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IENVIRONMENT_RESOURCE_DEFINITION__NAME = Ucm_contractsPackage.ICONFIGURABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IENVIRONMENT_RESOURCE_DEFINITION__COMMENT = Ucm_contractsPackage.ICONFIGURABLE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>IEnvironment Resource Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IENVIRONMENT_RESOURCE_DEFINITION_FEATURE_COUNT = Ucm_contractsPackage.ICONFIGURABLE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link ucm_environment.ucm_resources.impl.IEnvironmentResourceImpl <em>IEnvironment Resource</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_environment.ucm_resources.impl.IEnvironmentResourceImpl
	 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getIEnvironmentResource()
	 * @generated
	 */
	int IENVIRONMENT_RESOURCE = 3;

	/**
	 * The feature id for the '<em><b>Configuration Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IENVIRONMENT_RESOURCE__CONFIGURATION_VALUE = Ucm_contractsPackage.ICONFIGURED__CONFIGURATION_VALUE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IENVIRONMENT_RESOURCE__NAME = Ucm_contractsPackage.ICONFIGURED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IENVIRONMENT_RESOURCE__COMMENT = Ucm_contractsPackage.ICONFIGURED_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>IEnvironment Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IENVIRONMENT_RESOURCE_FEATURE_COUNT = Ucm_contractsPackage.ICONFIGURED_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link ucm_environment.ucm_resources.impl.ComputationResourceDefinitionImpl <em>Computation Resource Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_environment.ucm_resources.impl.ComputationResourceDefinitionImpl
	 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getComputationResourceDefinition()
	 * @generated
	 */
	int COMPUTATION_RESOURCE_DEFINITION = 4;

	/**
	 * The feature id for the '<em><b>Configuration Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_RESOURCE_DEFINITION__CONFIGURATION_PARAMETER = IENVIRONMENT_RESOURCE_DEFINITION__CONFIGURATION_PARAMETER;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_RESOURCE_DEFINITION__NAME = IENVIRONMENT_RESOURCE_DEFINITION__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_RESOURCE_DEFINITION__COMMENT = IENVIRONMENT_RESOURCE_DEFINITION__COMMENT;

	/**
	 * The feature id for the '<em><b>Computation Port</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_RESOURCE_DEFINITION__COMPUTATION_PORT = IENVIRONMENT_RESOURCE_DEFINITION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Performance Factor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_RESOURCE_DEFINITION__PERFORMANCE_FACTOR = IENVIRONMENT_RESOURCE_DEFINITION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Computation Resource Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_RESOURCE_DEFINITION_FEATURE_COUNT = IENVIRONMENT_RESOURCE_DEFINITION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link ucm_environment.ucm_resources.impl.ComputationResourcePortImpl <em>Computation Resource Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_environment.ucm_resources.impl.ComputationResourcePortImpl
	 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getComputationResourcePort()
	 * @generated
	 */
	int COMPUTATION_RESOURCE_PORT = 5;

	/**
	 * The feature id for the '<em><b>Configuration Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_RESOURCE_PORT__CONFIGURATION_PARAMETER = Ucm_contractsPackage.ICONFIGURABLE__CONFIGURATION_PARAMETER;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_RESOURCE_PORT__NAME = Ucm_contractsPackage.ICONFIGURABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_RESOURCE_PORT__COMMENT = Ucm_contractsPackage.ICONFIGURABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_RESOURCE_PORT__DEFINITION = Ucm_contractsPackage.ICONFIGURABLE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Computation Resource Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_RESOURCE_PORT_FEATURE_COUNT = Ucm_contractsPackage.ICONFIGURABLE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link ucm_environment.ucm_resources.impl.CommunicationResourceDefinitionImpl <em>Communication Resource Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_environment.ucm_resources.impl.CommunicationResourceDefinitionImpl
	 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getCommunicationResourceDefinition()
	 * @generated
	 */
	int COMMUNICATION_RESOURCE_DEFINITION = 6;

	/**
	 * The feature id for the '<em><b>Configuration Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_RESOURCE_DEFINITION__CONFIGURATION_PARAMETER = IENVIRONMENT_RESOURCE_DEFINITION__CONFIGURATION_PARAMETER;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_RESOURCE_DEFINITION__NAME = IENVIRONMENT_RESOURCE_DEFINITION__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_RESOURCE_DEFINITION__COMMENT = IENVIRONMENT_RESOURCE_DEFINITION__COMMENT;

	/**
	 * The feature id for the '<em><b>Communication Port</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_RESOURCE_DEFINITION__COMMUNICATION_PORT = IENVIRONMENT_RESOURCE_DEFINITION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Communication Resource Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_RESOURCE_DEFINITION_FEATURE_COUNT = IENVIRONMENT_RESOURCE_DEFINITION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link ucm_environment.ucm_resources.impl.CommunicationResourcePortImpl <em>Communication Resource Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_environment.ucm_resources.impl.CommunicationResourcePortImpl
	 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getCommunicationResourcePort()
	 * @generated
	 */
	int COMMUNICATION_RESOURCE_PORT = 7;

	/**
	 * The feature id for the '<em><b>Configuration Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_RESOURCE_PORT__CONFIGURATION_PARAMETER = Ucm_contractsPackage.ICONFIGURABLE__CONFIGURATION_PARAMETER;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_RESOURCE_PORT__NAME = Ucm_contractsPackage.ICONFIGURABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_RESOURCE_PORT__COMMENT = Ucm_contractsPackage.ICONFIGURABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_RESOURCE_PORT__DEFINITION = Ucm_contractsPackage.ICONFIGURABLE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Min</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_RESOURCE_PORT__MIN = Ucm_contractsPackage.ICONFIGURABLE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Max</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_RESOURCE_PORT__MAX = Ucm_contractsPackage.ICONFIGURABLE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Communication Resource Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_RESOURCE_PORT_FEATURE_COUNT = Ucm_contractsPackage.ICONFIGURABLE_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link ucm_environment.ucm_resources.impl.CommunicationPortDefinitionImpl <em>Communication Port Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_environment.ucm_resources.impl.CommunicationPortDefinitionImpl
	 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getCommunicationPortDefinition()
	 * @generated
	 */
	int COMMUNICATION_PORT_DEFINITION = 8;

	/**
	 * The feature id for the '<em><b>Configuration Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT_DEFINITION__CONFIGURATION_PARAMETER = IENVIRONMENT_RESOURCE_DEFINITION__CONFIGURATION_PARAMETER;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT_DEFINITION__NAME = IENVIRONMENT_RESOURCE_DEFINITION__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT_DEFINITION__COMMENT = IENVIRONMENT_RESOURCE_DEFINITION__COMMENT;

	/**
	 * The number of structural features of the '<em>Communication Port Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT_DEFINITION_FEATURE_COUNT = IENVIRONMENT_RESOURCE_DEFINITION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link ucm_environment.ucm_resources.impl.MemoryPartitionDefinitionImpl <em>Memory Partition Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_environment.ucm_resources.impl.MemoryPartitionDefinitionImpl
	 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getMemoryPartitionDefinition()
	 * @generated
	 */
	int MEMORY_PARTITION_DEFINITION = 9;

	/**
	 * The feature id for the '<em><b>Configuration Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMORY_PARTITION_DEFINITION__CONFIGURATION_PARAMETER = IENVIRONMENT_RESOURCE_DEFINITION__CONFIGURATION_PARAMETER;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMORY_PARTITION_DEFINITION__NAME = IENVIRONMENT_RESOURCE_DEFINITION__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMORY_PARTITION_DEFINITION__COMMENT = IENVIRONMENT_RESOURCE_DEFINITION__COMMENT;

	/**
	 * The number of structural features of the '<em>Memory Partition Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMORY_PARTITION_DEFINITION_FEATURE_COUNT = IENVIRONMENT_RESOURCE_DEFINITION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link ucm_environment.ucm_resources.impl.ExecutionResourceDefinitionImpl <em>Execution Resource Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_environment.ucm_resources.impl.ExecutionResourceDefinitionImpl
	 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getExecutionResourceDefinition()
	 * @generated
	 */
	int EXECUTION_RESOURCE_DEFINITION = 10;

	/**
	 * The feature id for the '<em><b>Configuration Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_RESOURCE_DEFINITION__CONFIGURATION_PARAMETER = IENVIRONMENT_RESOURCE_DEFINITION__CONFIGURATION_PARAMETER;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_RESOURCE_DEFINITION__NAME = IENVIRONMENT_RESOURCE_DEFINITION__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_RESOURCE_DEFINITION__COMMENT = IENVIRONMENT_RESOURCE_DEFINITION__COMMENT;

	/**
	 * The number of structural features of the '<em>Execution Resource Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_RESOURCE_DEFINITION_FEATURE_COUNT = IENVIRONMENT_RESOURCE_DEFINITION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link ucm_environment.ucm_resources.impl.ComputationResourceImpl <em>Computation Resource</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_environment.ucm_resources.impl.ComputationResourceImpl
	 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getComputationResource()
	 * @generated
	 */
	int COMPUTATION_RESOURCE = 11;

	/**
	 * The feature id for the '<em><b>Configuration Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_RESOURCE__CONFIGURATION_VALUE = IENVIRONMENT_RESOURCE__CONFIGURATION_VALUE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_RESOURCE__NAME = IENVIRONMENT_RESOURCE__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_RESOURCE__COMMENT = IENVIRONMENT_RESOURCE__COMMENT;

	/**
	 * The feature id for the '<em><b>Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_RESOURCE__DEFINITION = IENVIRONMENT_RESOURCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Computation Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_RESOURCE_FEATURE_COUNT = IENVIRONMENT_RESOURCE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link ucm_environment.ucm_resources.impl.CommunicationResourceImpl <em>Communication Resource</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_environment.ucm_resources.impl.CommunicationResourceImpl
	 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getCommunicationResource()
	 * @generated
	 */
	int COMMUNICATION_RESOURCE = 12;

	/**
	 * The feature id for the '<em><b>Configuration Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_RESOURCE__CONFIGURATION_VALUE = IENVIRONMENT_RESOURCE__CONFIGURATION_VALUE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_RESOURCE__NAME = IENVIRONMENT_RESOURCE__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_RESOURCE__COMMENT = IENVIRONMENT_RESOURCE__COMMENT;

	/**
	 * The feature id for the '<em><b>Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_RESOURCE__DEFINITION = IENVIRONMENT_RESOURCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Communication Port</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_RESOURCE__COMMUNICATION_PORT = IENVIRONMENT_RESOURCE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Communication Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_RESOURCE_FEATURE_COUNT = IENVIRONMENT_RESOURCE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link ucm_environment.ucm_resources.impl.CommunicationPortImpl <em>Communication Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_environment.ucm_resources.impl.CommunicationPortImpl
	 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getCommunicationPort()
	 * @generated
	 */
	int COMMUNICATION_PORT = 13;

	/**
	 * The feature id for the '<em><b>Configuration Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT__CONFIGURATION_VALUE = IENVIRONMENT_RESOURCE__CONFIGURATION_VALUE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT__NAME = IENVIRONMENT_RESOURCE__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT__COMMENT = IENVIRONMENT_RESOURCE__COMMENT;

	/**
	 * The feature id for the '<em><b>Computation Resource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT__COMPUTATION_RESOURCE = IENVIRONMENT_RESOURCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Computation Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT__COMPUTATION_PORT = IENVIRONMENT_RESOURCE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Communication Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT_FEATURE_COUNT = IENVIRONMENT_RESOURCE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link ucm_environment.ucm_resources.impl.ExecutionResourceImpl <em>Execution Resource</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_environment.ucm_resources.impl.ExecutionResourceImpl
	 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getExecutionResource()
	 * @generated
	 */
	int EXECUTION_RESOURCE = 14;

	/**
	 * The feature id for the '<em><b>Configuration Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_RESOURCE__CONFIGURATION_VALUE = IENVIRONMENT_RESOURCE__CONFIGURATION_VALUE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_RESOURCE__NAME = IENVIRONMENT_RESOURCE__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_RESOURCE__COMMENT = IENVIRONMENT_RESOURCE__COMMENT;

	/**
	 * The feature id for the '<em><b>Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_RESOURCE__DEFINITION = IENVIRONMENT_RESOURCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Execution Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_RESOURCE_FEATURE_COUNT = IENVIRONMENT_RESOURCE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link ucm_environment.ucm_resources.impl.ResourceAllocationParameterImpl <em>Resource Allocation Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_environment.ucm_resources.impl.ResourceAllocationParameterImpl
	 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getResourceAllocationParameter()
	 * @generated
	 */
	int RESOURCE_ALLOCATION_PARAMETER = 15;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_ALLOCATION_PARAMETER__NAME = Ucm_contractsPackage.ICONFIGURATION_PARAMETER__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_ALLOCATION_PARAMETER__COMMENT = Ucm_contractsPackage.ICONFIGURATION_PARAMETER__COMMENT;

	/**
	 * The feature id for the '<em><b>Allowed Resource Definition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_ALLOCATION_PARAMETER__ALLOWED_RESOURCE_DEFINITION = Ucm_contractsPackage.ICONFIGURATION_PARAMETER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Min</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_ALLOCATION_PARAMETER__MIN = Ucm_contractsPackage.ICONFIGURATION_PARAMETER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Max</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_ALLOCATION_PARAMETER__MAX = Ucm_contractsPackage.ICONFIGURATION_PARAMETER_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Resource Allocation Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_ALLOCATION_PARAMETER_FEATURE_COUNT = Ucm_contractsPackage.ICONFIGURATION_PARAMETER_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link ucm_environment.ucm_resources.impl.ResourceAllocationParameterValueImpl <em>Resource Allocation Parameter Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_environment.ucm_resources.impl.ResourceAllocationParameterValueImpl
	 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getResourceAllocationParameterValue()
	 * @generated
	 */
	int RESOURCE_ALLOCATION_PARAMETER_VALUE = 16;

	/**
	 * The feature id for the '<em><b>Parameter Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_ALLOCATION_PARAMETER_VALUE__PARAMETER_DEFINITION = Ucm_contractsPackage.ICONFIGURATION_PARAMETER_VALUE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Resource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_ALLOCATION_PARAMETER_VALUE__RESOURCE = Ucm_contractsPackage.ICONFIGURATION_PARAMETER_VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Resource Allocation Parameter Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_ALLOCATION_PARAMETER_VALUE_FEATURE_COUNT = Ucm_contractsPackage.ICONFIGURATION_PARAMETER_VALUE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link ucm_environment.ucm_resources.impl.MemoryPartitionImpl <em>Memory Partition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_environment.ucm_resources.impl.MemoryPartitionImpl
	 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getMemoryPartition()
	 * @generated
	 */
	int MEMORY_PARTITION = 17;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMORY_PARTITION__NAME = Ucm_deploymentsPackage.ICOMPONENT_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMORY_PARTITION__COMMENT = Ucm_deploymentsPackage.ICOMPONENT_NODE__COMMENT;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMORY_PARTITION__ANNOTATION = Ucm_deploymentsPackage.ICOMPONENT_NODE__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Language</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMORY_PARTITION__LANGUAGE = Ucm_deploymentsPackage.ICOMPONENT_NODE__LANGUAGE;

	/**
	 * The feature id for the '<em><b>Component Instance</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMORY_PARTITION__COMPONENT_INSTANCE = Ucm_deploymentsPackage.ICOMPONENT_NODE__COMPONENT_INSTANCE;

	/**
	 * The feature id for the '<em><b>Memory Partition Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMORY_PARTITION__MEMORY_PARTITION_DEFINITION = Ucm_deploymentsPackage.ICOMPONENT_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Configuration Parameter Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMORY_PARTITION__CONFIGURATION_PARAMETER_VALUE = Ucm_deploymentsPackage.ICOMPONENT_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Execution Resource</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMORY_PARTITION__EXECUTION_RESOURCE = Ucm_deploymentsPackage.ICOMPONENT_NODE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Memory Partition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMORY_PARTITION_FEATURE_COUNT = Ucm_deploymentsPackage.ICOMPONENT_NODE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link ucm_environment.ucm_resources.impl.ResourceAllocationAndConfigurationParameterImpl <em>Resource Allocation And Configuration Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_environment.ucm_resources.impl.ResourceAllocationAndConfigurationParameterImpl
	 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getResourceAllocationAndConfigurationParameter()
	 * @generated
	 */
	int RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER = 18;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__NAME = Ucm_contractsPackage.ICONFIGURATION_PARAMETER__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__COMMENT = Ucm_contractsPackage.ICONFIGURATION_PARAMETER__COMMENT;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__TYPE = Ucm_contractsPackage.ICONFIGURATION_PARAMETER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Default Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__DEFAULT_VALUE = Ucm_contractsPackage.ICONFIGURATION_PARAMETER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Allowed Resource Definition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__ALLOWED_RESOURCE_DEFINITION = Ucm_contractsPackage.ICONFIGURATION_PARAMETER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Min</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__MIN = Ucm_contractsPackage.ICONFIGURATION_PARAMETER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Max</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__MAX = Ucm_contractsPackage.ICONFIGURATION_PARAMETER_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Resource Allocation And Configuration Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER_FEATURE_COUNT = Ucm_contractsPackage.ICONFIGURATION_PARAMETER_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link ucm_environment.ucm_resources.impl.ResourceAllocationAndConfigurationParameterValueImpl <em>Resource Allocation And Configuration Parameter Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_environment.ucm_resources.impl.ResourceAllocationAndConfigurationParameterValueImpl
	 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getResourceAllocationAndConfigurationParameterValue()
	 * @generated
	 */
	int RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER_VALUE = 19;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER_VALUE__VALUE = Ucm_contractsPackage.ICONFIGURATION_PARAMETER_VALUE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Parameter Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER_VALUE__PARAMETER_DEFINITION = Ucm_contractsPackage.ICONFIGURATION_PARAMETER_VALUE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Resource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER_VALUE__RESOURCE = Ucm_contractsPackage.ICONFIGURATION_PARAMETER_VALUE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Resource Allocation And Configuration Parameter Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER_VALUE_FEATURE_COUNT = Ucm_contractsPackage.ICONFIGURATION_PARAMETER_VALUE_FEATURE_COUNT + 3;


	/**
	 * Returns the meta object for class '{@link ucm_environment.ucm_resources.ResourceDefinitionModule <em>Resource Definition Module</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Resource Definition Module</em>'.
	 * @see ucm_environment.ucm_resources.ResourceDefinitionModule
	 * @generated
	 */
	EClass getResourceDefinitionModule();

	/**
	 * Returns the meta object for the containment reference list '{@link ucm_environment.ucm_resources.ResourceDefinitionModule#getComputationResourceDefinition <em>Computation Resource Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Computation Resource Definition</em>'.
	 * @see ucm_environment.ucm_resources.ResourceDefinitionModule#getComputationResourceDefinition()
	 * @see #getResourceDefinitionModule()
	 * @generated
	 */
	EReference getResourceDefinitionModule_ComputationResourceDefinition();

	/**
	 * Returns the meta object for the containment reference list '{@link ucm_environment.ucm_resources.ResourceDefinitionModule#getCommunicationResourceDefinition <em>Communication Resource Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Communication Resource Definition</em>'.
	 * @see ucm_environment.ucm_resources.ResourceDefinitionModule#getCommunicationResourceDefinition()
	 * @see #getResourceDefinitionModule()
	 * @generated
	 */
	EReference getResourceDefinitionModule_CommunicationResourceDefinition();

	/**
	 * Returns the meta object for the containment reference list '{@link ucm_environment.ucm_resources.ResourceDefinitionModule#getExecutionResourceDefinition <em>Execution Resource Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Execution Resource Definition</em>'.
	 * @see ucm_environment.ucm_resources.ResourceDefinitionModule#getExecutionResourceDefinition()
	 * @see #getResourceDefinitionModule()
	 * @generated
	 */
	EReference getResourceDefinitionModule_ExecutionResourceDefinition();

	/**
	 * Returns the meta object for the containment reference list '{@link ucm_environment.ucm_resources.ResourceDefinitionModule#getMemoryPartitionDefinition <em>Memory Partition Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Memory Partition Definition</em>'.
	 * @see ucm_environment.ucm_resources.ResourceDefinitionModule#getMemoryPartitionDefinition()
	 * @see #getResourceDefinitionModule()
	 * @generated
	 */
	EReference getResourceDefinitionModule_MemoryPartitionDefinition();

	/**
	 * Returns the meta object for the containment reference list '{@link ucm_environment.ucm_resources.ResourceDefinitionModule#getContractModule <em>Contract Module</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Contract Module</em>'.
	 * @see ucm_environment.ucm_resources.ResourceDefinitionModule#getContractModule()
	 * @see #getResourceDefinitionModule()
	 * @generated
	 */
	EReference getResourceDefinitionModule_ContractModule();

	/**
	 * Returns the meta object for the containment reference list '{@link ucm_environment.ucm_resources.ResourceDefinitionModule#getCommunicationPortDefinition <em>Communication Port Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Communication Port Definition</em>'.
	 * @see ucm_environment.ucm_resources.ResourceDefinitionModule#getCommunicationPortDefinition()
	 * @see #getResourceDefinitionModule()
	 * @generated
	 */
	EReference getResourceDefinitionModule_CommunicationPortDefinition();

	/**
	 * Returns the meta object for class '{@link ucm_environment.ucm_resources.EnvironmentModule <em>Environment Module</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Environment Module</em>'.
	 * @see ucm_environment.ucm_resources.EnvironmentModule
	 * @generated
	 */
	EClass getEnvironmentModule();

	/**
	 * Returns the meta object for the containment reference list '{@link ucm_environment.ucm_resources.EnvironmentModule#getComputationResource <em>Computation Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Computation Resource</em>'.
	 * @see ucm_environment.ucm_resources.EnvironmentModule#getComputationResource()
	 * @see #getEnvironmentModule()
	 * @generated
	 */
	EReference getEnvironmentModule_ComputationResource();

	/**
	 * Returns the meta object for the containment reference list '{@link ucm_environment.ucm_resources.EnvironmentModule#getCommunicationResource <em>Communication Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Communication Resource</em>'.
	 * @see ucm_environment.ucm_resources.EnvironmentModule#getCommunicationResource()
	 * @see #getEnvironmentModule()
	 * @generated
	 */
	EReference getEnvironmentModule_CommunicationResource();

	/**
	 * Returns the meta object for class '{@link ucm_environment.ucm_resources.IEnvironmentResourceDefinition <em>IEnvironment Resource Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IEnvironment Resource Definition</em>'.
	 * @see ucm_environment.ucm_resources.IEnvironmentResourceDefinition
	 * @generated
	 */
	EClass getIEnvironmentResourceDefinition();

	/**
	 * Returns the meta object for class '{@link ucm_environment.ucm_resources.IEnvironmentResource <em>IEnvironment Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IEnvironment Resource</em>'.
	 * @see ucm_environment.ucm_resources.IEnvironmentResource
	 * @generated
	 */
	EClass getIEnvironmentResource();

	/**
	 * Returns the meta object for class '{@link ucm_environment.ucm_resources.ComputationResourceDefinition <em>Computation Resource Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Computation Resource Definition</em>'.
	 * @see ucm_environment.ucm_resources.ComputationResourceDefinition
	 * @generated
	 */
	EClass getComputationResourceDefinition();

	/**
	 * Returns the meta object for the containment reference list '{@link ucm_environment.ucm_resources.ComputationResourceDefinition#getComputationPort <em>Computation Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Computation Port</em>'.
	 * @see ucm_environment.ucm_resources.ComputationResourceDefinition#getComputationPort()
	 * @see #getComputationResourceDefinition()
	 * @generated
	 */
	EReference getComputationResourceDefinition_ComputationPort();

	/**
	 * Returns the meta object for the attribute '{@link ucm_environment.ucm_resources.ComputationResourceDefinition#getPerformanceFactor <em>Performance Factor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Performance Factor</em>'.
	 * @see ucm_environment.ucm_resources.ComputationResourceDefinition#getPerformanceFactor()
	 * @see #getComputationResourceDefinition()
	 * @generated
	 */
	EAttribute getComputationResourceDefinition_PerformanceFactor();

	/**
	 * Returns the meta object for class '{@link ucm_environment.ucm_resources.ComputationResourcePort <em>Computation Resource Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Computation Resource Port</em>'.
	 * @see ucm_environment.ucm_resources.ComputationResourcePort
	 * @generated
	 */
	EClass getComputationResourcePort();

	/**
	 * Returns the meta object for the reference '{@link ucm_environment.ucm_resources.ComputationResourcePort#getDefinition <em>Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Definition</em>'.
	 * @see ucm_environment.ucm_resources.ComputationResourcePort#getDefinition()
	 * @see #getComputationResourcePort()
	 * @generated
	 */
	EReference getComputationResourcePort_Definition();

	/**
	 * Returns the meta object for class '{@link ucm_environment.ucm_resources.CommunicationResourceDefinition <em>Communication Resource Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Communication Resource Definition</em>'.
	 * @see ucm_environment.ucm_resources.CommunicationResourceDefinition
	 * @generated
	 */
	EClass getCommunicationResourceDefinition();

	/**
	 * Returns the meta object for the containment reference list '{@link ucm_environment.ucm_resources.CommunicationResourceDefinition#getCommunicationPort <em>Communication Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Communication Port</em>'.
	 * @see ucm_environment.ucm_resources.CommunicationResourceDefinition#getCommunicationPort()
	 * @see #getCommunicationResourceDefinition()
	 * @generated
	 */
	EReference getCommunicationResourceDefinition_CommunicationPort();

	/**
	 * Returns the meta object for class '{@link ucm_environment.ucm_resources.CommunicationResourcePort <em>Communication Resource Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Communication Resource Port</em>'.
	 * @see ucm_environment.ucm_resources.CommunicationResourcePort
	 * @generated
	 */
	EClass getCommunicationResourcePort();

	/**
	 * Returns the meta object for the reference '{@link ucm_environment.ucm_resources.CommunicationResourcePort#getDefinition <em>Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Definition</em>'.
	 * @see ucm_environment.ucm_resources.CommunicationResourcePort#getDefinition()
	 * @see #getCommunicationResourcePort()
	 * @generated
	 */
	EReference getCommunicationResourcePort_Definition();

	/**
	 * Returns the meta object for the attribute '{@link ucm_environment.ucm_resources.CommunicationResourcePort#getMin <em>Min</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min</em>'.
	 * @see ucm_environment.ucm_resources.CommunicationResourcePort#getMin()
	 * @see #getCommunicationResourcePort()
	 * @generated
	 */
	EAttribute getCommunicationResourcePort_Min();

	/**
	 * Returns the meta object for the attribute '{@link ucm_environment.ucm_resources.CommunicationResourcePort#getMax <em>Max</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max</em>'.
	 * @see ucm_environment.ucm_resources.CommunicationResourcePort#getMax()
	 * @see #getCommunicationResourcePort()
	 * @generated
	 */
	EAttribute getCommunicationResourcePort_Max();

	/**
	 * Returns the meta object for class '{@link ucm_environment.ucm_resources.CommunicationPortDefinition <em>Communication Port Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Communication Port Definition</em>'.
	 * @see ucm_environment.ucm_resources.CommunicationPortDefinition
	 * @generated
	 */
	EClass getCommunicationPortDefinition();

	/**
	 * Returns the meta object for class '{@link ucm_environment.ucm_resources.MemoryPartitionDefinition <em>Memory Partition Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Memory Partition Definition</em>'.
	 * @see ucm_environment.ucm_resources.MemoryPartitionDefinition
	 * @generated
	 */
	EClass getMemoryPartitionDefinition();

	/**
	 * Returns the meta object for class '{@link ucm_environment.ucm_resources.ExecutionResourceDefinition <em>Execution Resource Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Execution Resource Definition</em>'.
	 * @see ucm_environment.ucm_resources.ExecutionResourceDefinition
	 * @generated
	 */
	EClass getExecutionResourceDefinition();

	/**
	 * Returns the meta object for class '{@link ucm_environment.ucm_resources.ComputationResource <em>Computation Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Computation Resource</em>'.
	 * @see ucm_environment.ucm_resources.ComputationResource
	 * @generated
	 */
	EClass getComputationResource();

	/**
	 * Returns the meta object for the reference '{@link ucm_environment.ucm_resources.ComputationResource#getDefinition <em>Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Definition</em>'.
	 * @see ucm_environment.ucm_resources.ComputationResource#getDefinition()
	 * @see #getComputationResource()
	 * @generated
	 */
	EReference getComputationResource_Definition();

	/**
	 * Returns the meta object for class '{@link ucm_environment.ucm_resources.CommunicationResource <em>Communication Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Communication Resource</em>'.
	 * @see ucm_environment.ucm_resources.CommunicationResource
	 * @generated
	 */
	EClass getCommunicationResource();

	/**
	 * Returns the meta object for the reference '{@link ucm_environment.ucm_resources.CommunicationResource#getDefinition <em>Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Definition</em>'.
	 * @see ucm_environment.ucm_resources.CommunicationResource#getDefinition()
	 * @see #getCommunicationResource()
	 * @generated
	 */
	EReference getCommunicationResource_Definition();

	/**
	 * Returns the meta object for the containment reference list '{@link ucm_environment.ucm_resources.CommunicationResource#getCommunicationPort <em>Communication Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Communication Port</em>'.
	 * @see ucm_environment.ucm_resources.CommunicationResource#getCommunicationPort()
	 * @see #getCommunicationResource()
	 * @generated
	 */
	EReference getCommunicationResource_CommunicationPort();

	/**
	 * Returns the meta object for class '{@link ucm_environment.ucm_resources.CommunicationPort <em>Communication Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Communication Port</em>'.
	 * @see ucm_environment.ucm_resources.CommunicationPort
	 * @generated
	 */
	EClass getCommunicationPort();

	/**
	 * Returns the meta object for the reference '{@link ucm_environment.ucm_resources.CommunicationPort#getComputationResource <em>Computation Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Computation Resource</em>'.
	 * @see ucm_environment.ucm_resources.CommunicationPort#getComputationResource()
	 * @see #getCommunicationPort()
	 * @generated
	 */
	EReference getCommunicationPort_ComputationResource();

	/**
	 * Returns the meta object for the reference '{@link ucm_environment.ucm_resources.CommunicationPort#getComputationPort <em>Computation Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Computation Port</em>'.
	 * @see ucm_environment.ucm_resources.CommunicationPort#getComputationPort()
	 * @see #getCommunicationPort()
	 * @generated
	 */
	EReference getCommunicationPort_ComputationPort();

	/**
	 * Returns the meta object for class '{@link ucm_environment.ucm_resources.ExecutionResource <em>Execution Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Execution Resource</em>'.
	 * @see ucm_environment.ucm_resources.ExecutionResource
	 * @generated
	 */
	EClass getExecutionResource();

	/**
	 * Returns the meta object for the reference '{@link ucm_environment.ucm_resources.ExecutionResource#getDefinition <em>Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Definition</em>'.
	 * @see ucm_environment.ucm_resources.ExecutionResource#getDefinition()
	 * @see #getExecutionResource()
	 * @generated
	 */
	EReference getExecutionResource_Definition();

	/**
	 * Returns the meta object for class '{@link ucm_environment.ucm_resources.ResourceAllocationParameter <em>Resource Allocation Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Resource Allocation Parameter</em>'.
	 * @see ucm_environment.ucm_resources.ResourceAllocationParameter
	 * @generated
	 */
	EClass getResourceAllocationParameter();

	/**
	 * Returns the meta object for the reference list '{@link ucm_environment.ucm_resources.ResourceAllocationParameter#getAllowedResourceDefinition <em>Allowed Resource Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Allowed Resource Definition</em>'.
	 * @see ucm_environment.ucm_resources.ResourceAllocationParameter#getAllowedResourceDefinition()
	 * @see #getResourceAllocationParameter()
	 * @generated
	 */
	EReference getResourceAllocationParameter_AllowedResourceDefinition();

	/**
	 * Returns the meta object for the attribute '{@link ucm_environment.ucm_resources.ResourceAllocationParameter#getMin <em>Min</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min</em>'.
	 * @see ucm_environment.ucm_resources.ResourceAllocationParameter#getMin()
	 * @see #getResourceAllocationParameter()
	 * @generated
	 */
	EAttribute getResourceAllocationParameter_Min();

	/**
	 * Returns the meta object for the attribute '{@link ucm_environment.ucm_resources.ResourceAllocationParameter#getMax <em>Max</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max</em>'.
	 * @see ucm_environment.ucm_resources.ResourceAllocationParameter#getMax()
	 * @see #getResourceAllocationParameter()
	 * @generated
	 */
	EAttribute getResourceAllocationParameter_Max();

	/**
	 * Returns the meta object for class '{@link ucm_environment.ucm_resources.ResourceAllocationParameterValue <em>Resource Allocation Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Resource Allocation Parameter Value</em>'.
	 * @see ucm_environment.ucm_resources.ResourceAllocationParameterValue
	 * @generated
	 */
	EClass getResourceAllocationParameterValue();

	/**
	 * Returns the meta object for the reference '{@link ucm_environment.ucm_resources.ResourceAllocationParameterValue#getParameterDefinition <em>Parameter Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parameter Definition</em>'.
	 * @see ucm_environment.ucm_resources.ResourceAllocationParameterValue#getParameterDefinition()
	 * @see #getResourceAllocationParameterValue()
	 * @generated
	 */
	EReference getResourceAllocationParameterValue_ParameterDefinition();

	/**
	 * Returns the meta object for the reference '{@link ucm_environment.ucm_resources.ResourceAllocationParameterValue#getResource <em>Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Resource</em>'.
	 * @see ucm_environment.ucm_resources.ResourceAllocationParameterValue#getResource()
	 * @see #getResourceAllocationParameterValue()
	 * @generated
	 */
	EReference getResourceAllocationParameterValue_Resource();

	/**
	 * Returns the meta object for class '{@link ucm_environment.ucm_resources.MemoryPartition <em>Memory Partition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Memory Partition</em>'.
	 * @see ucm_environment.ucm_resources.MemoryPartition
	 * @generated
	 */
	EClass getMemoryPartition();

	/**
	 * Returns the meta object for the reference '{@link ucm_environment.ucm_resources.MemoryPartition#getMemoryPartitionDefinition <em>Memory Partition Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Memory Partition Definition</em>'.
	 * @see ucm_environment.ucm_resources.MemoryPartition#getMemoryPartitionDefinition()
	 * @see #getMemoryPartition()
	 * @generated
	 */
	EReference getMemoryPartition_MemoryPartitionDefinition();

	/**
	 * Returns the meta object for the containment reference list '{@link ucm_environment.ucm_resources.MemoryPartition#getConfigurationParameterValue <em>Configuration Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Configuration Parameter Value</em>'.
	 * @see ucm_environment.ucm_resources.MemoryPartition#getConfigurationParameterValue()
	 * @see #getMemoryPartition()
	 * @generated
	 */
	EReference getMemoryPartition_ConfigurationParameterValue();

	/**
	 * Returns the meta object for the containment reference list '{@link ucm_environment.ucm_resources.MemoryPartition#getExecutionResource <em>Execution Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Execution Resource</em>'.
	 * @see ucm_environment.ucm_resources.MemoryPartition#getExecutionResource()
	 * @see #getMemoryPartition()
	 * @generated
	 */
	EReference getMemoryPartition_ExecutionResource();

	/**
	 * Returns the meta object for class '{@link ucm_environment.ucm_resources.ResourceAllocationAndConfigurationParameter <em>Resource Allocation And Configuration Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Resource Allocation And Configuration Parameter</em>'.
	 * @see ucm_environment.ucm_resources.ResourceAllocationAndConfigurationParameter
	 * @generated
	 */
	EClass getResourceAllocationAndConfigurationParameter();

	/**
	 * Returns the meta object for the reference list '{@link ucm_environment.ucm_resources.ResourceAllocationAndConfigurationParameter#getAllowedResourceDefinition <em>Allowed Resource Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Allowed Resource Definition</em>'.
	 * @see ucm_environment.ucm_resources.ResourceAllocationAndConfigurationParameter#getAllowedResourceDefinition()
	 * @see #getResourceAllocationAndConfigurationParameter()
	 * @generated
	 */
	EReference getResourceAllocationAndConfigurationParameter_AllowedResourceDefinition();

	/**
	 * Returns the meta object for the attribute '{@link ucm_environment.ucm_resources.ResourceAllocationAndConfigurationParameter#getMin <em>Min</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min</em>'.
	 * @see ucm_environment.ucm_resources.ResourceAllocationAndConfigurationParameter#getMin()
	 * @see #getResourceAllocationAndConfigurationParameter()
	 * @generated
	 */
	EAttribute getResourceAllocationAndConfigurationParameter_Min();

	/**
	 * Returns the meta object for the attribute '{@link ucm_environment.ucm_resources.ResourceAllocationAndConfigurationParameter#getMax <em>Max</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max</em>'.
	 * @see ucm_environment.ucm_resources.ResourceAllocationAndConfigurationParameter#getMax()
	 * @see #getResourceAllocationAndConfigurationParameter()
	 * @generated
	 */
	EAttribute getResourceAllocationAndConfigurationParameter_Max();

	/**
	 * Returns the meta object for class '{@link ucm_environment.ucm_resources.ResourceAllocationAndConfigurationParameterValue <em>Resource Allocation And Configuration Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Resource Allocation And Configuration Parameter Value</em>'.
	 * @see ucm_environment.ucm_resources.ResourceAllocationAndConfigurationParameterValue
	 * @generated
	 */
	EClass getResourceAllocationAndConfigurationParameterValue();

	/**
	 * Returns the meta object for the reference '{@link ucm_environment.ucm_resources.ResourceAllocationAndConfigurationParameterValue#getParameterDefinition <em>Parameter Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parameter Definition</em>'.
	 * @see ucm_environment.ucm_resources.ResourceAllocationAndConfigurationParameterValue#getParameterDefinition()
	 * @see #getResourceAllocationAndConfigurationParameterValue()
	 * @generated
	 */
	EReference getResourceAllocationAndConfigurationParameterValue_ParameterDefinition();

	/**
	 * Returns the meta object for the reference '{@link ucm_environment.ucm_resources.ResourceAllocationAndConfigurationParameterValue#getResource <em>Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Resource</em>'.
	 * @see ucm_environment.ucm_resources.ResourceAllocationAndConfigurationParameterValue#getResource()
	 * @see #getResourceAllocationAndConfigurationParameterValue()
	 * @generated
	 */
	EReference getResourceAllocationAndConfigurationParameterValue_Resource();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	Ucm_resourcesFactory getUcm_resourcesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ucm_environment.ucm_resources.impl.ResourceDefinitionModuleImpl <em>Resource Definition Module</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_environment.ucm_resources.impl.ResourceDefinitionModuleImpl
		 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getResourceDefinitionModule()
		 * @generated
		 */
		EClass RESOURCE_DEFINITION_MODULE = eINSTANCE.getResourceDefinitionModule();

		/**
		 * The meta object literal for the '<em><b>Computation Resource Definition</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESOURCE_DEFINITION_MODULE__COMPUTATION_RESOURCE_DEFINITION = eINSTANCE.getResourceDefinitionModule_ComputationResourceDefinition();

		/**
		 * The meta object literal for the '<em><b>Communication Resource Definition</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESOURCE_DEFINITION_MODULE__COMMUNICATION_RESOURCE_DEFINITION = eINSTANCE.getResourceDefinitionModule_CommunicationResourceDefinition();

		/**
		 * The meta object literal for the '<em><b>Execution Resource Definition</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESOURCE_DEFINITION_MODULE__EXECUTION_RESOURCE_DEFINITION = eINSTANCE.getResourceDefinitionModule_ExecutionResourceDefinition();

		/**
		 * The meta object literal for the '<em><b>Memory Partition Definition</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESOURCE_DEFINITION_MODULE__MEMORY_PARTITION_DEFINITION = eINSTANCE.getResourceDefinitionModule_MemoryPartitionDefinition();

		/**
		 * The meta object literal for the '<em><b>Contract Module</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESOURCE_DEFINITION_MODULE__CONTRACT_MODULE = eINSTANCE.getResourceDefinitionModule_ContractModule();

		/**
		 * The meta object literal for the '<em><b>Communication Port Definition</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESOURCE_DEFINITION_MODULE__COMMUNICATION_PORT_DEFINITION = eINSTANCE.getResourceDefinitionModule_CommunicationPortDefinition();

		/**
		 * The meta object literal for the '{@link ucm_environment.ucm_resources.impl.EnvironmentModuleImpl <em>Environment Module</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_environment.ucm_resources.impl.EnvironmentModuleImpl
		 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getEnvironmentModule()
		 * @generated
		 */
		EClass ENVIRONMENT_MODULE = eINSTANCE.getEnvironmentModule();

		/**
		 * The meta object literal for the '<em><b>Computation Resource</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENVIRONMENT_MODULE__COMPUTATION_RESOURCE = eINSTANCE.getEnvironmentModule_ComputationResource();

		/**
		 * The meta object literal for the '<em><b>Communication Resource</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENVIRONMENT_MODULE__COMMUNICATION_RESOURCE = eINSTANCE.getEnvironmentModule_CommunicationResource();

		/**
		 * The meta object literal for the '{@link ucm_environment.ucm_resources.impl.IEnvironmentResourceDefinitionImpl <em>IEnvironment Resource Definition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_environment.ucm_resources.impl.IEnvironmentResourceDefinitionImpl
		 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getIEnvironmentResourceDefinition()
		 * @generated
		 */
		EClass IENVIRONMENT_RESOURCE_DEFINITION = eINSTANCE.getIEnvironmentResourceDefinition();

		/**
		 * The meta object literal for the '{@link ucm_environment.ucm_resources.impl.IEnvironmentResourceImpl <em>IEnvironment Resource</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_environment.ucm_resources.impl.IEnvironmentResourceImpl
		 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getIEnvironmentResource()
		 * @generated
		 */
		EClass IENVIRONMENT_RESOURCE = eINSTANCE.getIEnvironmentResource();

		/**
		 * The meta object literal for the '{@link ucm_environment.ucm_resources.impl.ComputationResourceDefinitionImpl <em>Computation Resource Definition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_environment.ucm_resources.impl.ComputationResourceDefinitionImpl
		 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getComputationResourceDefinition()
		 * @generated
		 */
		EClass COMPUTATION_RESOURCE_DEFINITION = eINSTANCE.getComputationResourceDefinition();

		/**
		 * The meta object literal for the '<em><b>Computation Port</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPUTATION_RESOURCE_DEFINITION__COMPUTATION_PORT = eINSTANCE.getComputationResourceDefinition_ComputationPort();

		/**
		 * The meta object literal for the '<em><b>Performance Factor</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPUTATION_RESOURCE_DEFINITION__PERFORMANCE_FACTOR = eINSTANCE.getComputationResourceDefinition_PerformanceFactor();

		/**
		 * The meta object literal for the '{@link ucm_environment.ucm_resources.impl.ComputationResourcePortImpl <em>Computation Resource Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_environment.ucm_resources.impl.ComputationResourcePortImpl
		 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getComputationResourcePort()
		 * @generated
		 */
		EClass COMPUTATION_RESOURCE_PORT = eINSTANCE.getComputationResourcePort();

		/**
		 * The meta object literal for the '<em><b>Definition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPUTATION_RESOURCE_PORT__DEFINITION = eINSTANCE.getComputationResourcePort_Definition();

		/**
		 * The meta object literal for the '{@link ucm_environment.ucm_resources.impl.CommunicationResourceDefinitionImpl <em>Communication Resource Definition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_environment.ucm_resources.impl.CommunicationResourceDefinitionImpl
		 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getCommunicationResourceDefinition()
		 * @generated
		 */
		EClass COMMUNICATION_RESOURCE_DEFINITION = eINSTANCE.getCommunicationResourceDefinition();

		/**
		 * The meta object literal for the '<em><b>Communication Port</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATION_RESOURCE_DEFINITION__COMMUNICATION_PORT = eINSTANCE.getCommunicationResourceDefinition_CommunicationPort();

		/**
		 * The meta object literal for the '{@link ucm_environment.ucm_resources.impl.CommunicationResourcePortImpl <em>Communication Resource Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_environment.ucm_resources.impl.CommunicationResourcePortImpl
		 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getCommunicationResourcePort()
		 * @generated
		 */
		EClass COMMUNICATION_RESOURCE_PORT = eINSTANCE.getCommunicationResourcePort();

		/**
		 * The meta object literal for the '<em><b>Definition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATION_RESOURCE_PORT__DEFINITION = eINSTANCE.getCommunicationResourcePort_Definition();

		/**
		 * The meta object literal for the '<em><b>Min</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMMUNICATION_RESOURCE_PORT__MIN = eINSTANCE.getCommunicationResourcePort_Min();

		/**
		 * The meta object literal for the '<em><b>Max</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMMUNICATION_RESOURCE_PORT__MAX = eINSTANCE.getCommunicationResourcePort_Max();

		/**
		 * The meta object literal for the '{@link ucm_environment.ucm_resources.impl.CommunicationPortDefinitionImpl <em>Communication Port Definition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_environment.ucm_resources.impl.CommunicationPortDefinitionImpl
		 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getCommunicationPortDefinition()
		 * @generated
		 */
		EClass COMMUNICATION_PORT_DEFINITION = eINSTANCE.getCommunicationPortDefinition();

		/**
		 * The meta object literal for the '{@link ucm_environment.ucm_resources.impl.MemoryPartitionDefinitionImpl <em>Memory Partition Definition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_environment.ucm_resources.impl.MemoryPartitionDefinitionImpl
		 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getMemoryPartitionDefinition()
		 * @generated
		 */
		EClass MEMORY_PARTITION_DEFINITION = eINSTANCE.getMemoryPartitionDefinition();

		/**
		 * The meta object literal for the '{@link ucm_environment.ucm_resources.impl.ExecutionResourceDefinitionImpl <em>Execution Resource Definition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_environment.ucm_resources.impl.ExecutionResourceDefinitionImpl
		 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getExecutionResourceDefinition()
		 * @generated
		 */
		EClass EXECUTION_RESOURCE_DEFINITION = eINSTANCE.getExecutionResourceDefinition();

		/**
		 * The meta object literal for the '{@link ucm_environment.ucm_resources.impl.ComputationResourceImpl <em>Computation Resource</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_environment.ucm_resources.impl.ComputationResourceImpl
		 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getComputationResource()
		 * @generated
		 */
		EClass COMPUTATION_RESOURCE = eINSTANCE.getComputationResource();

		/**
		 * The meta object literal for the '<em><b>Definition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPUTATION_RESOURCE__DEFINITION = eINSTANCE.getComputationResource_Definition();

		/**
		 * The meta object literal for the '{@link ucm_environment.ucm_resources.impl.CommunicationResourceImpl <em>Communication Resource</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_environment.ucm_resources.impl.CommunicationResourceImpl
		 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getCommunicationResource()
		 * @generated
		 */
		EClass COMMUNICATION_RESOURCE = eINSTANCE.getCommunicationResource();

		/**
		 * The meta object literal for the '<em><b>Definition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATION_RESOURCE__DEFINITION = eINSTANCE.getCommunicationResource_Definition();

		/**
		 * The meta object literal for the '<em><b>Communication Port</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATION_RESOURCE__COMMUNICATION_PORT = eINSTANCE.getCommunicationResource_CommunicationPort();

		/**
		 * The meta object literal for the '{@link ucm_environment.ucm_resources.impl.CommunicationPortImpl <em>Communication Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_environment.ucm_resources.impl.CommunicationPortImpl
		 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getCommunicationPort()
		 * @generated
		 */
		EClass COMMUNICATION_PORT = eINSTANCE.getCommunicationPort();

		/**
		 * The meta object literal for the '<em><b>Computation Resource</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATION_PORT__COMPUTATION_RESOURCE = eINSTANCE.getCommunicationPort_ComputationResource();

		/**
		 * The meta object literal for the '<em><b>Computation Port</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATION_PORT__COMPUTATION_PORT = eINSTANCE.getCommunicationPort_ComputationPort();

		/**
		 * The meta object literal for the '{@link ucm_environment.ucm_resources.impl.ExecutionResourceImpl <em>Execution Resource</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_environment.ucm_resources.impl.ExecutionResourceImpl
		 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getExecutionResource()
		 * @generated
		 */
		EClass EXECUTION_RESOURCE = eINSTANCE.getExecutionResource();

		/**
		 * The meta object literal for the '<em><b>Definition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXECUTION_RESOURCE__DEFINITION = eINSTANCE.getExecutionResource_Definition();

		/**
		 * The meta object literal for the '{@link ucm_environment.ucm_resources.impl.ResourceAllocationParameterImpl <em>Resource Allocation Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_environment.ucm_resources.impl.ResourceAllocationParameterImpl
		 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getResourceAllocationParameter()
		 * @generated
		 */
		EClass RESOURCE_ALLOCATION_PARAMETER = eINSTANCE.getResourceAllocationParameter();

		/**
		 * The meta object literal for the '<em><b>Allowed Resource Definition</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESOURCE_ALLOCATION_PARAMETER__ALLOWED_RESOURCE_DEFINITION = eINSTANCE.getResourceAllocationParameter_AllowedResourceDefinition();

		/**
		 * The meta object literal for the '<em><b>Min</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESOURCE_ALLOCATION_PARAMETER__MIN = eINSTANCE.getResourceAllocationParameter_Min();

		/**
		 * The meta object literal for the '<em><b>Max</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESOURCE_ALLOCATION_PARAMETER__MAX = eINSTANCE.getResourceAllocationParameter_Max();

		/**
		 * The meta object literal for the '{@link ucm_environment.ucm_resources.impl.ResourceAllocationParameterValueImpl <em>Resource Allocation Parameter Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_environment.ucm_resources.impl.ResourceAllocationParameterValueImpl
		 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getResourceAllocationParameterValue()
		 * @generated
		 */
		EClass RESOURCE_ALLOCATION_PARAMETER_VALUE = eINSTANCE.getResourceAllocationParameterValue();

		/**
		 * The meta object literal for the '<em><b>Parameter Definition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESOURCE_ALLOCATION_PARAMETER_VALUE__PARAMETER_DEFINITION = eINSTANCE.getResourceAllocationParameterValue_ParameterDefinition();

		/**
		 * The meta object literal for the '<em><b>Resource</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESOURCE_ALLOCATION_PARAMETER_VALUE__RESOURCE = eINSTANCE.getResourceAllocationParameterValue_Resource();

		/**
		 * The meta object literal for the '{@link ucm_environment.ucm_resources.impl.MemoryPartitionImpl <em>Memory Partition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_environment.ucm_resources.impl.MemoryPartitionImpl
		 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getMemoryPartition()
		 * @generated
		 */
		EClass MEMORY_PARTITION = eINSTANCE.getMemoryPartition();

		/**
		 * The meta object literal for the '<em><b>Memory Partition Definition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MEMORY_PARTITION__MEMORY_PARTITION_DEFINITION = eINSTANCE.getMemoryPartition_MemoryPartitionDefinition();

		/**
		 * The meta object literal for the '<em><b>Configuration Parameter Value</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MEMORY_PARTITION__CONFIGURATION_PARAMETER_VALUE = eINSTANCE.getMemoryPartition_ConfigurationParameterValue();

		/**
		 * The meta object literal for the '<em><b>Execution Resource</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MEMORY_PARTITION__EXECUTION_RESOURCE = eINSTANCE.getMemoryPartition_ExecutionResource();

		/**
		 * The meta object literal for the '{@link ucm_environment.ucm_resources.impl.ResourceAllocationAndConfigurationParameterImpl <em>Resource Allocation And Configuration Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_environment.ucm_resources.impl.ResourceAllocationAndConfigurationParameterImpl
		 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getResourceAllocationAndConfigurationParameter()
		 * @generated
		 */
		EClass RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER = eINSTANCE.getResourceAllocationAndConfigurationParameter();

		/**
		 * The meta object literal for the '<em><b>Allowed Resource Definition</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__ALLOWED_RESOURCE_DEFINITION = eINSTANCE.getResourceAllocationAndConfigurationParameter_AllowedResourceDefinition();

		/**
		 * The meta object literal for the '<em><b>Min</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__MIN = eINSTANCE.getResourceAllocationAndConfigurationParameter_Min();

		/**
		 * The meta object literal for the '<em><b>Max</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__MAX = eINSTANCE.getResourceAllocationAndConfigurationParameter_Max();

		/**
		 * The meta object literal for the '{@link ucm_environment.ucm_resources.impl.ResourceAllocationAndConfigurationParameterValueImpl <em>Resource Allocation And Configuration Parameter Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_environment.ucm_resources.impl.ResourceAllocationAndConfigurationParameterValueImpl
		 * @see ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl#getResourceAllocationAndConfigurationParameterValue()
		 * @generated
		 */
		EClass RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER_VALUE = eINSTANCE.getResourceAllocationAndConfigurationParameterValue();

		/**
		 * The meta object literal for the '<em><b>Parameter Definition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER_VALUE__PARAMETER_DEFINITION = eINSTANCE.getResourceAllocationAndConfigurationParameterValue_ParameterDefinition();

		/**
		 * The meta object literal for the '<em><b>Resource</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER_VALUE__RESOURCE = eINSTANCE.getResourceAllocationAndConfigurationParameterValue_Resource();

	}

} //Ucm_resourcesPackage
