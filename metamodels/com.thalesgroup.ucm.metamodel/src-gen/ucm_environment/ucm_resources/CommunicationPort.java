/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_environment.ucm_resources;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Communication Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_environment.ucm_resources.CommunicationPort#getComputationResource <em>Computation Resource</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.CommunicationPort#getComputationPort <em>Computation Port</em>}</li>
 * </ul>
 *
 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getCommunicationPort()
 * @model
 * @generated
 */
public interface CommunicationPort extends IEnvironmentResource {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Computation Resource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Computation Resource</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Computation Resource</em>' reference.
	 * @see #setComputationResource(ComputationResource)
	 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getCommunicationPort_ComputationResource()
	 * @model required="true"
	 * @generated
	 */
	ComputationResource getComputationResource();

	/**
	 * Sets the value of the '{@link ucm_environment.ucm_resources.CommunicationPort#getComputationResource <em>Computation Resource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Computation Resource</em>' reference.
	 * @see #getComputationResource()
	 * @generated
	 */
	void setComputationResource(ComputationResource value);

	/**
	 * Returns the value of the '<em><b>Computation Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Computation Port</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Computation Port</em>' reference.
	 * @see #setComputationPort(ComputationResourcePort)
	 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getCommunicationPort_ComputationPort()
	 * @model required="true"
	 * @generated
	 */
	ComputationResourcePort getComputationPort();

	/**
	 * Sets the value of the '{@link ucm_environment.ucm_resources.CommunicationPort#getComputationPort <em>Computation Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Computation Port</em>' reference.
	 * @see #getComputationPort()
	 * @generated
	 */
	void setComputationPort(ComputationResourcePort value);

} // CommunicationPort
