/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_environment.ucm_resources;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Memory Partition Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getMemoryPartitionDefinition()
 * @model
 * @generated
 */
public interface MemoryPartitionDefinition extends IEnvironmentResourceDefinition {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

} // MemoryPartitionDefinition
