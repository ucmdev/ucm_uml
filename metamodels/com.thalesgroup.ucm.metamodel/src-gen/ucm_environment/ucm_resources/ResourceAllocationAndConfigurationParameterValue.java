/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_environment.ucm_resources;

import ucm_base.ucm_contracts.IConfigurationParameterValue;
import ucm_base.ucm_contracts.IValued;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Resource Allocation And Configuration Parameter Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_environment.ucm_resources.ResourceAllocationAndConfigurationParameterValue#getParameterDefinition <em>Parameter Definition</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.ResourceAllocationAndConfigurationParameterValue#getResource <em>Resource</em>}</li>
 * </ul>
 *
 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getResourceAllocationAndConfigurationParameterValue()
 * @model
 * @generated
 */
public interface ResourceAllocationAndConfigurationParameterValue extends IConfigurationParameterValue, IValued {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Parameter Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Definition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Definition</em>' reference.
	 * @see #setParameterDefinition(ResourceAllocationAndConfigurationParameter)
	 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getResourceAllocationAndConfigurationParameterValue_ParameterDefinition()
	 * @model required="true"
	 * @generated
	 */
	ResourceAllocationAndConfigurationParameter getParameterDefinition();

	/**
	 * Sets the value of the '{@link ucm_environment.ucm_resources.ResourceAllocationAndConfigurationParameterValue#getParameterDefinition <em>Parameter Definition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter Definition</em>' reference.
	 * @see #getParameterDefinition()
	 * @generated
	 */
	void setParameterDefinition(ResourceAllocationAndConfigurationParameter value);

	/**
	 * Returns the value of the '<em><b>Resource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource</em>' reference.
	 * @see #setResource(IEnvironmentResource)
	 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getResourceAllocationAndConfigurationParameterValue_Resource()
	 * @model required="true"
	 * @generated
	 */
	IEnvironmentResource getResource();

	/**
	 * Sets the value of the '{@link ucm_environment.ucm_resources.ResourceAllocationAndConfigurationParameterValue#getResource <em>Resource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Resource</em>' reference.
	 * @see #getResource()
	 * @generated
	 */
	void setResource(IEnvironmentResource value);

} // ResourceAllocationAndConfigurationParameterValue
