/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_environment.ucm_resources;

import org.eclipse.emf.common.util.EList;

import ucm_base.ucm_contracts.IConfigurationParameter;
import ucm_base.ucm_contracts.IHasDatatype;
import ucm_base.ucm_contracts.IHasDefaultValue;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Resource Allocation And Configuration Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_environment.ucm_resources.ResourceAllocationAndConfigurationParameter#getAllowedResourceDefinition <em>Allowed Resource Definition</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.ResourceAllocationAndConfigurationParameter#getMin <em>Min</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.ResourceAllocationAndConfigurationParameter#getMax <em>Max</em>}</li>
 * </ul>
 *
 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getResourceAllocationAndConfigurationParameter()
 * @model
 * @generated
 */
public interface ResourceAllocationAndConfigurationParameter extends IConfigurationParameter, IHasDatatype, IHasDefaultValue {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Allowed Resource Definition</b></em>' reference list.
	 * The list contents are of type {@link ucm_environment.ucm_resources.IEnvironmentResourceDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Allowed Resource Definition</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Allowed Resource Definition</em>' reference list.
	 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getResourceAllocationAndConfigurationParameter_AllowedResourceDefinition()
	 * @model required="true"
	 * @generated
	 */
	EList<IEnvironmentResourceDefinition> getAllowedResourceDefinition();

	/**
	 * Returns the value of the '<em><b>Min</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min</em>' attribute.
	 * @see #setMin(long)
	 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getResourceAllocationAndConfigurationParameter_Min()
	 * @model default="1" required="true"
	 * @generated
	 */
	long getMin();

	/**
	 * Sets the value of the '{@link ucm_environment.ucm_resources.ResourceAllocationAndConfigurationParameter#getMin <em>Min</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min</em>' attribute.
	 * @see #getMin()
	 * @generated
	 */
	void setMin(long value);

	/**
	 * Returns the value of the '<em><b>Max</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max</em>' attribute.
	 * @see #setMax(long)
	 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getResourceAllocationAndConfigurationParameter_Max()
	 * @model default="1" required="true"
	 * @generated
	 */
	long getMax();

	/**
	 * Sets the value of the '{@link ucm_environment.ucm_resources.ResourceAllocationAndConfigurationParameter#getMax <em>Max</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max</em>' attribute.
	 * @see #getMax()
	 * @generated
	 */
	void setMax(long value);

} // ResourceAllocationAndConfigurationParameter
