/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_environment.ucm_resources;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Communication Resource Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_environment.ucm_resources.CommunicationResourceDefinition#getCommunicationPort <em>Communication Port</em>}</li>
 * </ul>
 *
 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getCommunicationResourceDefinition()
 * @model
 * @generated
 */
public interface CommunicationResourceDefinition extends IEnvironmentResourceDefinition {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Communication Port</b></em>' containment reference list.
	 * The list contents are of type {@link ucm_environment.ucm_resources.CommunicationResourcePort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Communication Port</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Communication Port</em>' containment reference list.
	 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getCommunicationResourceDefinition_CommunicationPort()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<CommunicationResourcePort> getCommunicationPort();

} // CommunicationResourceDefinition
