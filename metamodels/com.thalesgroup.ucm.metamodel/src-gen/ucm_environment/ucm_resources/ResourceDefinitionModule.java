/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_environment.ucm_resources;

import org.eclipse.emf.common.util.EList;

import ucm_base.ucm_commons.IPlatformModule;

import ucm_base.ucm_contracts.ContractModule;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Resource Definition Module</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_environment.ucm_resources.ResourceDefinitionModule#getComputationResourceDefinition <em>Computation Resource Definition</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.ResourceDefinitionModule#getCommunicationResourceDefinition <em>Communication Resource Definition</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.ResourceDefinitionModule#getExecutionResourceDefinition <em>Execution Resource Definition</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.ResourceDefinitionModule#getMemoryPartitionDefinition <em>Memory Partition Definition</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.ResourceDefinitionModule#getContractModule <em>Contract Module</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.ResourceDefinitionModule#getCommunicationPortDefinition <em>Communication Port Definition</em>}</li>
 * </ul>
 *
 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getResourceDefinitionModule()
 * @model
 * @generated
 */
public interface ResourceDefinitionModule extends IPlatformModule {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Computation Resource Definition</b></em>' containment reference list.
	 * The list contents are of type {@link ucm_environment.ucm_resources.ComputationResourceDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Computation Resource Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Computation Resource Definition</em>' containment reference list.
	 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getResourceDefinitionModule_ComputationResourceDefinition()
	 * @model containment="true"
	 * @generated
	 */
	EList<ComputationResourceDefinition> getComputationResourceDefinition();

	/**
	 * Returns the value of the '<em><b>Communication Resource Definition</b></em>' containment reference list.
	 * The list contents are of type {@link ucm_environment.ucm_resources.CommunicationResourceDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Communication Resource Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Communication Resource Definition</em>' containment reference list.
	 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getResourceDefinitionModule_CommunicationResourceDefinition()
	 * @model containment="true"
	 * @generated
	 */
	EList<CommunicationResourceDefinition> getCommunicationResourceDefinition();

	/**
	 * Returns the value of the '<em><b>Execution Resource Definition</b></em>' containment reference list.
	 * The list contents are of type {@link ucm_environment.ucm_resources.ExecutionResourceDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Execution Resource Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Execution Resource Definition</em>' containment reference list.
	 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getResourceDefinitionModule_ExecutionResourceDefinition()
	 * @model containment="true"
	 * @generated
	 */
	EList<ExecutionResourceDefinition> getExecutionResourceDefinition();

	/**
	 * Returns the value of the '<em><b>Memory Partition Definition</b></em>' containment reference list.
	 * The list contents are of type {@link ucm_environment.ucm_resources.MemoryPartitionDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Memory Partition Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Memory Partition Definition</em>' containment reference list.
	 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getResourceDefinitionModule_MemoryPartitionDefinition()
	 * @model containment="true"
	 * @generated
	 */
	EList<MemoryPartitionDefinition> getMemoryPartitionDefinition();

	/**
	 * Returns the value of the '<em><b>Contract Module</b></em>' containment reference list.
	 * The list contents are of type {@link ucm_base.ucm_contracts.ContractModule}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contract Module</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contract Module</em>' containment reference list.
	 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getResourceDefinitionModule_ContractModule()
	 * @model containment="true"
	 * @generated
	 */
	EList<ContractModule> getContractModule();

	/**
	 * Returns the value of the '<em><b>Communication Port Definition</b></em>' containment reference list.
	 * The list contents are of type {@link ucm_environment.ucm_resources.CommunicationPortDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Communication Port Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Communication Port Definition</em>' containment reference list.
	 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getResourceDefinitionModule_CommunicationPortDefinition()
	 * @model containment="true"
	 * @generated
	 */
	EList<CommunicationPortDefinition> getCommunicationPortDefinition();

} // ResourceDefinitionModule
