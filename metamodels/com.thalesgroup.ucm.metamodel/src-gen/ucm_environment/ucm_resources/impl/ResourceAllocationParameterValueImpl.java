/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_environment.ucm_resources.impl;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import com.thalesgroup.ucm.metamodel.validation.UcmSupplementValidationFactory;

import ucm_base.ucm_contracts.impl.IConfigurationParameterValueImpl;
import ucm_environment.ucm_resources.IEnvironmentResource;
import ucm_environment.ucm_resources.ResourceAllocationParameter;
import ucm_environment.ucm_resources.ResourceAllocationParameterValue;
import ucm_environment.ucm_resources.Ucm_resourcesPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>Resource Allocation Parameter Value</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_environment.ucm_resources.impl.ResourceAllocationParameterValueImpl#getParameterDefinition <em>Parameter Definition</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.impl.ResourceAllocationParameterValueImpl#getResource <em>Resource</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ResourceAllocationParameterValueImpl extends IConfigurationParameterValueImpl
		implements ResourceAllocationParameterValue {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached value of the '{@link #getParameterDefinition() <em>Parameter
	 * Definition</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getParameterDefinition()
	 * @generated
	 * @ordered
	 */
	protected ResourceAllocationParameter parameterDefinition;

	/**
	 * The cached value of the '{@link #getResource() <em>Resource</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getResource()
	 * @generated
	 * @ordered
	 */
	protected IEnvironmentResource resource;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected ResourceAllocationParameterValueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_resourcesPackage.Literals.RESOURCE_ALLOCATION_PARAMETER_VALUE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceAllocationParameter getParameterDefinition() {
		if (parameterDefinition != null && parameterDefinition.eIsProxy()) {
			InternalEObject oldParameterDefinition = (InternalEObject)parameterDefinition;
			parameterDefinition = (ResourceAllocationParameter)eResolveProxy(oldParameterDefinition);
			if (parameterDefinition != oldParameterDefinition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_resourcesPackage.RESOURCE_ALLOCATION_PARAMETER_VALUE__PARAMETER_DEFINITION, oldParameterDefinition, parameterDefinition));
			}
		}
		return parameterDefinition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceAllocationParameter basicGetParameterDefinition() {
		return parameterDefinition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setParameterDefinition(ResourceAllocationParameter newParameterDefinition) {
		ResourceAllocationParameter oldParameterDefinition = parameterDefinition;
		parameterDefinition = newParameterDefinition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_resourcesPackage.RESOURCE_ALLOCATION_PARAMETER_VALUE__PARAMETER_DEFINITION, oldParameterDefinition, parameterDefinition));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public IEnvironmentResource getResource() {
		if (resource != null && resource.eIsProxy()) {
			InternalEObject oldResource = (InternalEObject)resource;
			resource = (IEnvironmentResource)eResolveProxy(oldResource);
			if (resource != oldResource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_resourcesPackage.RESOURCE_ALLOCATION_PARAMETER_VALUE__RESOURCE, oldResource, resource));
			}
		}
		return resource;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public IEnvironmentResource basicGetResource() {
		return resource;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setResource(IEnvironmentResource newResource) {
		IEnvironmentResource oldResource = resource;
		resource = newResource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_resourcesPackage.RESOURCE_ALLOCATION_PARAMETER_VALUE__RESOURCE, oldResource, resource));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean parameterDefinitionConsistency(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and
		// message
		// Ensure that you remove @generated or mark it @generated NOT
		return UcmSupplementValidationFactory.INSTANCE.getContractValidator().parameterDefinitionConsistency(this,
				diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_PARAMETER_VALUE__PARAMETER_DEFINITION:
				if (resolve) return getParameterDefinition();
				return basicGetParameterDefinition();
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_PARAMETER_VALUE__RESOURCE:
				if (resolve) return getResource();
				return basicGetResource();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_PARAMETER_VALUE__PARAMETER_DEFINITION:
				setParameterDefinition((ResourceAllocationParameter)newValue);
				return;
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_PARAMETER_VALUE__RESOURCE:
				setResource((IEnvironmentResource)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_PARAMETER_VALUE__PARAMETER_DEFINITION:
				setParameterDefinition((ResourceAllocationParameter)null);
				return;
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_PARAMETER_VALUE__RESOURCE:
				setResource((IEnvironmentResource)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_PARAMETER_VALUE__PARAMETER_DEFINITION:
				return parameterDefinition != null;
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_PARAMETER_VALUE__RESOURCE:
				return resource != null;
		}
		return super.eIsSet(featureID);
	}

} // ResourceAllocationParameterValueImpl
