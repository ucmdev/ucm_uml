/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_environment.ucm_resources.impl;

import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import com.thalesgroup.ucm.metamodel.validation.UcmSupplementValidationFactory;

import ucm_environment.ucm_resources.CommunicationPort;
import ucm_environment.ucm_resources.CommunicationResource;
import ucm_environment.ucm_resources.CommunicationResourceDefinition;
import ucm_environment.ucm_resources.Ucm_resourcesPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>Communication Resource</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_environment.ucm_resources.impl.CommunicationResourceImpl#getDefinition <em>Definition</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.impl.CommunicationResourceImpl#getCommunicationPort <em>Communication Port</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CommunicationResourceImpl extends IEnvironmentResourceImpl implements CommunicationResource {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached value of the '{@link #getDefinition() <em>Definition</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getDefinition()
	 * @generated
	 * @ordered
	 */
	protected CommunicationResourceDefinition definition;

	/**
	 * The cached value of the '{@link #getCommunicationPort() <em>Communication Port</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getCommunicationPort()
	 * @generated
	 * @ordered
	 */
	protected EList<CommunicationPort> communicationPort;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected CommunicationResourceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_resourcesPackage.Literals.COMMUNICATION_RESOURCE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public CommunicationResourceDefinition getDefinition() {
		if (definition != null && definition.eIsProxy()) {
			InternalEObject oldDefinition = (InternalEObject)definition;
			definition = (CommunicationResourceDefinition)eResolveProxy(oldDefinition);
			if (definition != oldDefinition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_resourcesPackage.COMMUNICATION_RESOURCE__DEFINITION, oldDefinition, definition));
			}
		}
		return definition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public CommunicationResourceDefinition basicGetDefinition() {
		return definition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefinition(CommunicationResourceDefinition newDefinition) {
		CommunicationResourceDefinition oldDefinition = definition;
		definition = newDefinition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_resourcesPackage.COMMUNICATION_RESOURCE__DEFINITION, oldDefinition, definition));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CommunicationPort> getCommunicationPort() {
		if (communicationPort == null) {
			communicationPort = new EObjectContainmentEList<CommunicationPort>(CommunicationPort.class, this, Ucm_resourcesPackage.COMMUNICATION_RESOURCE__COMMUNICATION_PORT);
		}
		return communicationPort;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean portCardinality(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and
		// message
		// Ensure that you remove @generated or mark it @generated NOT
		return UcmSupplementValidationFactory.INSTANCE.getResourceValidator().portCardinality(this, diagnostics,
				context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE__COMMUNICATION_PORT:
				return ((InternalEList<?>)getCommunicationPort()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE__DEFINITION:
				if (resolve) return getDefinition();
				return basicGetDefinition();
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE__COMMUNICATION_PORT:
				return getCommunicationPort();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE__DEFINITION:
				setDefinition((CommunicationResourceDefinition)newValue);
				return;
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE__COMMUNICATION_PORT:
				getCommunicationPort().clear();
				getCommunicationPort().addAll((Collection<? extends CommunicationPort>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE__DEFINITION:
				setDefinition((CommunicationResourceDefinition)null);
				return;
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE__COMMUNICATION_PORT:
				getCommunicationPort().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE__DEFINITION:
				return definition != null;
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE__COMMUNICATION_PORT:
				return communicationPort != null && !communicationPort.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} // CommunicationResourceImpl
