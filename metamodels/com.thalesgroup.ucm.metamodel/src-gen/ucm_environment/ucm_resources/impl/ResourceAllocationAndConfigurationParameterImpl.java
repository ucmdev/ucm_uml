/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_environment.ucm_resources.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import ucm_base.ucm_contracts.IDataType;
import ucm_base.ucm_contracts.IHasDatatype;
import ucm_base.ucm_contracts.IHasDefaultValue;
import ucm_base.ucm_contracts.Ucm_contractsPackage;

import ucm_base.ucm_contracts.impl.IConfigurationParameterImpl;

import ucm_environment.ucm_resources.IEnvironmentResourceDefinition;
import ucm_environment.ucm_resources.ResourceAllocationAndConfigurationParameter;
import ucm_environment.ucm_resources.Ucm_resourcesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Resource Allocation And Configuration Parameter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_environment.ucm_resources.impl.ResourceAllocationAndConfigurationParameterImpl#getType <em>Type</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.impl.ResourceAllocationAndConfigurationParameterImpl#getDefaultValue <em>Default Value</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.impl.ResourceAllocationAndConfigurationParameterImpl#getAllowedResourceDefinition <em>Allowed Resource Definition</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.impl.ResourceAllocationAndConfigurationParameterImpl#getMin <em>Min</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.impl.ResourceAllocationAndConfigurationParameterImpl#getMax <em>Max</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ResourceAllocationAndConfigurationParameterImpl extends IConfigurationParameterImpl implements ResourceAllocationAndConfigurationParameter {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected IDataType type;

	/**
	 * The default value of the '{@link #getDefaultValue() <em>Default Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultValue()
	 * @generated
	 * @ordered
	 */
	protected static final String DEFAULT_VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDefaultValue() <em>Default Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultValue()
	 * @generated
	 * @ordered
	 */
	protected String defaultValue = DEFAULT_VALUE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAllowedResourceDefinition() <em>Allowed Resource Definition</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAllowedResourceDefinition()
	 * @generated
	 * @ordered
	 */
	protected EList<IEnvironmentResourceDefinition> allowedResourceDefinition;

	/**
	 * The default value of the '{@link #getMin() <em>Min</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMin()
	 * @generated
	 * @ordered
	 */
	protected static final long MIN_EDEFAULT = 1L;

	/**
	 * The cached value of the '{@link #getMin() <em>Min</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMin()
	 * @generated
	 * @ordered
	 */
	protected long min = MIN_EDEFAULT;

	/**
	 * The default value of the '{@link #getMax() <em>Max</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMax()
	 * @generated
	 * @ordered
	 */
	protected static final long MAX_EDEFAULT = 1L;

	/**
	 * The cached value of the '{@link #getMax() <em>Max</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMax()
	 * @generated
	 * @ordered
	 */
	protected long max = MAX_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ResourceAllocationAndConfigurationParameterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_resourcesPackage.Literals.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IDataType getType() {
		if (type != null && type.eIsProxy()) {
			InternalEObject oldType = (InternalEObject)type;
			type = (IDataType)eResolveProxy(oldType);
			if (type != oldType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__TYPE, oldType, type));
			}
		}
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IDataType basicGetType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(IDataType newType) {
		IDataType oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDefaultValue() {
		return defaultValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefaultValue(String newDefaultValue) {
		String oldDefaultValue = defaultValue;
		defaultValue = newDefaultValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__DEFAULT_VALUE, oldDefaultValue, defaultValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IEnvironmentResourceDefinition> getAllowedResourceDefinition() {
		if (allowedResourceDefinition == null) {
			allowedResourceDefinition = new EObjectResolvingEList<IEnvironmentResourceDefinition>(IEnvironmentResourceDefinition.class, this, Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__ALLOWED_RESOURCE_DEFINITION);
		}
		return allowedResourceDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getMin() {
		return min;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMin(long newMin) {
		long oldMin = min;
		min = newMin;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__MIN, oldMin, min));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getMax() {
		return max;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMax(long newMax) {
		long oldMax = max;
		max = newMax;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__MAX, oldMax, max));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__TYPE:
				if (resolve) return getType();
				return basicGetType();
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__DEFAULT_VALUE:
				return getDefaultValue();
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__ALLOWED_RESOURCE_DEFINITION:
				return getAllowedResourceDefinition();
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__MIN:
				return getMin();
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__MAX:
				return getMax();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__TYPE:
				setType((IDataType)newValue);
				return;
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__DEFAULT_VALUE:
				setDefaultValue((String)newValue);
				return;
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__ALLOWED_RESOURCE_DEFINITION:
				getAllowedResourceDefinition().clear();
				getAllowedResourceDefinition().addAll((Collection<? extends IEnvironmentResourceDefinition>)newValue);
				return;
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__MIN:
				setMin((Long)newValue);
				return;
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__MAX:
				setMax((Long)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__TYPE:
				setType((IDataType)null);
				return;
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__DEFAULT_VALUE:
				setDefaultValue(DEFAULT_VALUE_EDEFAULT);
				return;
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__ALLOWED_RESOURCE_DEFINITION:
				getAllowedResourceDefinition().clear();
				return;
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__MIN:
				setMin(MIN_EDEFAULT);
				return;
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__MAX:
				setMax(MAX_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__TYPE:
				return type != null;
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__DEFAULT_VALUE:
				return DEFAULT_VALUE_EDEFAULT == null ? defaultValue != null : !DEFAULT_VALUE_EDEFAULT.equals(defaultValue);
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__ALLOWED_RESOURCE_DEFINITION:
				return allowedResourceDefinition != null && !allowedResourceDefinition.isEmpty();
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__MIN:
				return min != MIN_EDEFAULT;
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__MAX:
				return max != MAX_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == IHasDatatype.class) {
			switch (derivedFeatureID) {
				case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__TYPE: return Ucm_contractsPackage.IHAS_DATATYPE__TYPE;
				default: return -1;
			}
		}
		if (baseClass == IHasDefaultValue.class) {
			switch (derivedFeatureID) {
				case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__DEFAULT_VALUE: return Ucm_contractsPackage.IHAS_DEFAULT_VALUE__DEFAULT_VALUE;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == IHasDatatype.class) {
			switch (baseFeatureID) {
				case Ucm_contractsPackage.IHAS_DATATYPE__TYPE: return Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__TYPE;
				default: return -1;
			}
		}
		if (baseClass == IHasDefaultValue.class) {
			switch (baseFeatureID) {
				case Ucm_contractsPackage.IHAS_DEFAULT_VALUE__DEFAULT_VALUE: return Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__DEFAULT_VALUE;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (defaultValue: ");
		result.append(defaultValue);
		result.append(", min: ");
		result.append(min);
		result.append(", max: ");
		result.append(max);
		result.append(')');
		return result.toString();
	}

} //ResourceAllocationAndConfigurationParameterImpl
