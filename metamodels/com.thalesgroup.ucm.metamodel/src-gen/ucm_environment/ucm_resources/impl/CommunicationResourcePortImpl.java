/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_environment.ucm_resources.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import ucm_base.ucm_commons.IComment;
import ucm_base.ucm_commons.INamed;
import ucm_base.ucm_commons.Ucm_commonsPackage;
import ucm_base.ucm_contracts.impl.IConfigurableImpl;

import ucm_environment.ucm_resources.CommunicationPortDefinition;
import ucm_environment.ucm_resources.CommunicationResourcePort;
import ucm_environment.ucm_resources.Ucm_resourcesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Communication Resource Port</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_environment.ucm_resources.impl.CommunicationResourcePortImpl#getName <em>Name</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.impl.CommunicationResourcePortImpl#getComment <em>Comment</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.impl.CommunicationResourcePortImpl#getDefinition <em>Definition</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.impl.CommunicationResourcePortImpl#getMin <em>Min</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.impl.CommunicationResourcePortImpl#getMax <em>Max</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CommunicationResourcePortImpl extends IConfigurableImpl implements CommunicationResourcePort {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getComment() <em>Comment</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComment()
	 * @generated
	 * @ordered
	 */
	protected EList<IComment> comment;

	/**
	 * The cached value of the '{@link #getDefinition() <em>Definition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefinition()
	 * @generated
	 * @ordered
	 */
	protected CommunicationPortDefinition definition;

	/**
	 * The default value of the '{@link #getMin() <em>Min</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMin()
	 * @generated
	 * @ordered
	 */
	protected static final long MIN_EDEFAULT = 1L;

	/**
	 * The cached value of the '{@link #getMin() <em>Min</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMin()
	 * @generated
	 * @ordered
	 */
	protected long min = MIN_EDEFAULT;

	/**
	 * The default value of the '{@link #getMax() <em>Max</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMax()
	 * @generated
	 * @ordered
	 */
	protected static final long MAX_EDEFAULT = -1L;

	/**
	 * The cached value of the '{@link #getMax() <em>Max</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMax()
	 * @generated
	 * @ordered
	 */
	protected long max = MAX_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CommunicationResourcePortImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_resourcesPackage.Literals.COMMUNICATION_RESOURCE_PORT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_resourcesPackage.COMMUNICATION_RESOURCE_PORT__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IComment> getComment() {
		if (comment == null) {
			comment = new EObjectContainmentEList<IComment>(IComment.class, this, Ucm_resourcesPackage.COMMUNICATION_RESOURCE_PORT__COMMENT);
		}
		return comment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommunicationPortDefinition getDefinition() {
		if (definition != null && definition.eIsProxy()) {
			InternalEObject oldDefinition = (InternalEObject)definition;
			definition = (CommunicationPortDefinition)eResolveProxy(oldDefinition);
			if (definition != oldDefinition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_resourcesPackage.COMMUNICATION_RESOURCE_PORT__DEFINITION, oldDefinition, definition));
			}
		}
		return definition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommunicationPortDefinition basicGetDefinition() {
		return definition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefinition(CommunicationPortDefinition newDefinition) {
		CommunicationPortDefinition oldDefinition = definition;
		definition = newDefinition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_resourcesPackage.COMMUNICATION_RESOURCE_PORT__DEFINITION, oldDefinition, definition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getMin() {
		return min;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMin(long newMin) {
		long oldMin = min;
		min = newMin;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_resourcesPackage.COMMUNICATION_RESOURCE_PORT__MIN, oldMin, min));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getMax() {
		return max;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMax(long newMax) {
		long oldMax = max;
		max = newMax;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_resourcesPackage.COMMUNICATION_RESOURCE_PORT__MAX, oldMax, max));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE_PORT__COMMENT:
				return ((InternalEList<?>)getComment()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE_PORT__NAME:
				return getName();
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE_PORT__COMMENT:
				return getComment();
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE_PORT__DEFINITION:
				if (resolve) return getDefinition();
				return basicGetDefinition();
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE_PORT__MIN:
				return getMin();
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE_PORT__MAX:
				return getMax();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE_PORT__NAME:
				setName((String)newValue);
				return;
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE_PORT__COMMENT:
				getComment().clear();
				getComment().addAll((Collection<? extends IComment>)newValue);
				return;
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE_PORT__DEFINITION:
				setDefinition((CommunicationPortDefinition)newValue);
				return;
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE_PORT__MIN:
				setMin((Long)newValue);
				return;
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE_PORT__MAX:
				setMax((Long)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE_PORT__NAME:
				setName(NAME_EDEFAULT);
				return;
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE_PORT__COMMENT:
				getComment().clear();
				return;
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE_PORT__DEFINITION:
				setDefinition((CommunicationPortDefinition)null);
				return;
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE_PORT__MIN:
				setMin(MIN_EDEFAULT);
				return;
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE_PORT__MAX:
				setMax(MAX_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE_PORT__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE_PORT__COMMENT:
				return comment != null && !comment.isEmpty();
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE_PORT__DEFINITION:
				return definition != null;
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE_PORT__MIN:
				return min != MIN_EDEFAULT;
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE_PORT__MAX:
				return max != MAX_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == INamed.class) {
			switch (derivedFeatureID) {
				case Ucm_resourcesPackage.COMMUNICATION_RESOURCE_PORT__NAME: return Ucm_commonsPackage.INAMED__NAME;
				case Ucm_resourcesPackage.COMMUNICATION_RESOURCE_PORT__COMMENT: return Ucm_commonsPackage.INAMED__COMMENT;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == INamed.class) {
			switch (baseFeatureID) {
				case Ucm_commonsPackage.INAMED__NAME: return Ucm_resourcesPackage.COMMUNICATION_RESOURCE_PORT__NAME;
				case Ucm_commonsPackage.INAMED__COMMENT: return Ucm_resourcesPackage.COMMUNICATION_RESOURCE_PORT__COMMENT;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", min: ");
		result.append(min);
		result.append(", max: ");
		result.append(max);
		result.append(')');
		return result.toString();
	}

} //CommunicationResourcePortImpl
