/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_environment.ucm_resources.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import ucm_environment.ucm_resources.CommunicationPort;
import ucm_environment.ucm_resources.ComputationResource;
import ucm_environment.ucm_resources.ComputationResourcePort;
import ucm_environment.ucm_resources.Ucm_resourcesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Communication Port</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_environment.ucm_resources.impl.CommunicationPortImpl#getComputationResource <em>Computation Resource</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.impl.CommunicationPortImpl#getComputationPort <em>Computation Port</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CommunicationPortImpl extends IEnvironmentResourceImpl implements CommunicationPort {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached value of the '{@link #getComputationResource() <em>Computation Resource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComputationResource()
	 * @generated
	 * @ordered
	 */
	protected ComputationResource computationResource;

	/**
	 * The cached value of the '{@link #getComputationPort() <em>Computation Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComputationPort()
	 * @generated
	 * @ordered
	 */
	protected ComputationResourcePort computationPort;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CommunicationPortImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_resourcesPackage.Literals.COMMUNICATION_PORT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComputationResource getComputationResource() {
		if (computationResource != null && computationResource.eIsProxy()) {
			InternalEObject oldComputationResource = (InternalEObject)computationResource;
			computationResource = (ComputationResource)eResolveProxy(oldComputationResource);
			if (computationResource != oldComputationResource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_resourcesPackage.COMMUNICATION_PORT__COMPUTATION_RESOURCE, oldComputationResource, computationResource));
			}
		}
		return computationResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComputationResource basicGetComputationResource() {
		return computationResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComputationResource(ComputationResource newComputationResource) {
		ComputationResource oldComputationResource = computationResource;
		computationResource = newComputationResource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_resourcesPackage.COMMUNICATION_PORT__COMPUTATION_RESOURCE, oldComputationResource, computationResource));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComputationResourcePort getComputationPort() {
		if (computationPort != null && computationPort.eIsProxy()) {
			InternalEObject oldComputationPort = (InternalEObject)computationPort;
			computationPort = (ComputationResourcePort)eResolveProxy(oldComputationPort);
			if (computationPort != oldComputationPort) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_resourcesPackage.COMMUNICATION_PORT__COMPUTATION_PORT, oldComputationPort, computationPort));
			}
		}
		return computationPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComputationResourcePort basicGetComputationPort() {
		return computationPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComputationPort(ComputationResourcePort newComputationPort) {
		ComputationResourcePort oldComputationPort = computationPort;
		computationPort = newComputationPort;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_resourcesPackage.COMMUNICATION_PORT__COMPUTATION_PORT, oldComputationPort, computationPort));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_resourcesPackage.COMMUNICATION_PORT__COMPUTATION_RESOURCE:
				if (resolve) return getComputationResource();
				return basicGetComputationResource();
			case Ucm_resourcesPackage.COMMUNICATION_PORT__COMPUTATION_PORT:
				if (resolve) return getComputationPort();
				return basicGetComputationPort();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_resourcesPackage.COMMUNICATION_PORT__COMPUTATION_RESOURCE:
				setComputationResource((ComputationResource)newValue);
				return;
			case Ucm_resourcesPackage.COMMUNICATION_PORT__COMPUTATION_PORT:
				setComputationPort((ComputationResourcePort)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_resourcesPackage.COMMUNICATION_PORT__COMPUTATION_RESOURCE:
				setComputationResource((ComputationResource)null);
				return;
			case Ucm_resourcesPackage.COMMUNICATION_PORT__COMPUTATION_PORT:
				setComputationPort((ComputationResourcePort)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_resourcesPackage.COMMUNICATION_PORT__COMPUTATION_RESOURCE:
				return computationResource != null;
			case Ucm_resourcesPackage.COMMUNICATION_PORT__COMPUTATION_PORT:
				return computationPort != null;
		}
		return super.eIsSet(featureID);
	}

} //CommunicationPortImpl
