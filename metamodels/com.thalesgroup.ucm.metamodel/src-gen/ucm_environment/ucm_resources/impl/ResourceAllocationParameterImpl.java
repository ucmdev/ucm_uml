/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_environment.ucm_resources.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import ucm_base.ucm_contracts.impl.IConfigurationParameterImpl;

import ucm_environment.ucm_resources.IEnvironmentResourceDefinition;
import ucm_environment.ucm_resources.ResourceAllocationParameter;
import ucm_environment.ucm_resources.Ucm_resourcesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Resource Allocation Parameter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_environment.ucm_resources.impl.ResourceAllocationParameterImpl#getAllowedResourceDefinition <em>Allowed Resource Definition</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.impl.ResourceAllocationParameterImpl#getMin <em>Min</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.impl.ResourceAllocationParameterImpl#getMax <em>Max</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ResourceAllocationParameterImpl extends IConfigurationParameterImpl implements ResourceAllocationParameter {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached value of the '{@link #getAllowedResourceDefinition() <em>Allowed Resource Definition</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAllowedResourceDefinition()
	 * @generated
	 * @ordered
	 */
	protected EList<IEnvironmentResourceDefinition> allowedResourceDefinition;

	/**
	 * The default value of the '{@link #getMin() <em>Min</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMin()
	 * @generated
	 * @ordered
	 */
	protected static final long MIN_EDEFAULT = 1L;

	/**
	 * The cached value of the '{@link #getMin() <em>Min</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMin()
	 * @generated
	 * @ordered
	 */
	protected long min = MIN_EDEFAULT;

	/**
	 * The default value of the '{@link #getMax() <em>Max</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMax()
	 * @generated
	 * @ordered
	 */
	protected static final long MAX_EDEFAULT = 1L;

	/**
	 * The cached value of the '{@link #getMax() <em>Max</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMax()
	 * @generated
	 * @ordered
	 */
	protected long max = MAX_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ResourceAllocationParameterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_resourcesPackage.Literals.RESOURCE_ALLOCATION_PARAMETER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IEnvironmentResourceDefinition> getAllowedResourceDefinition() {
		if (allowedResourceDefinition == null) {
			allowedResourceDefinition = new EObjectResolvingEList<IEnvironmentResourceDefinition>(IEnvironmentResourceDefinition.class, this, Ucm_resourcesPackage.RESOURCE_ALLOCATION_PARAMETER__ALLOWED_RESOURCE_DEFINITION);
		}
		return allowedResourceDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getMin() {
		return min;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMin(long newMin) {
		long oldMin = min;
		min = newMin;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_resourcesPackage.RESOURCE_ALLOCATION_PARAMETER__MIN, oldMin, min));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getMax() {
		return max;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMax(long newMax) {
		long oldMax = max;
		max = newMax;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_resourcesPackage.RESOURCE_ALLOCATION_PARAMETER__MAX, oldMax, max));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_PARAMETER__ALLOWED_RESOURCE_DEFINITION:
				return getAllowedResourceDefinition();
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_PARAMETER__MIN:
				return getMin();
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_PARAMETER__MAX:
				return getMax();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_PARAMETER__ALLOWED_RESOURCE_DEFINITION:
				getAllowedResourceDefinition().clear();
				getAllowedResourceDefinition().addAll((Collection<? extends IEnvironmentResourceDefinition>)newValue);
				return;
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_PARAMETER__MIN:
				setMin((Long)newValue);
				return;
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_PARAMETER__MAX:
				setMax((Long)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_PARAMETER__ALLOWED_RESOURCE_DEFINITION:
				getAllowedResourceDefinition().clear();
				return;
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_PARAMETER__MIN:
				setMin(MIN_EDEFAULT);
				return;
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_PARAMETER__MAX:
				setMax(MAX_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_PARAMETER__ALLOWED_RESOURCE_DEFINITION:
				return allowedResourceDefinition != null && !allowedResourceDefinition.isEmpty();
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_PARAMETER__MIN:
				return min != MIN_EDEFAULT;
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_PARAMETER__MAX:
				return max != MAX_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (min: ");
		result.append(min);
		result.append(", max: ");
		result.append(max);
		result.append(')');
		return result.toString();
	}

} //ResourceAllocationParameterImpl
