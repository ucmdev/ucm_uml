/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_environment.ucm_resources.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import ucm_environment.ucm_resources.CommunicationResourceDefinition;
import ucm_environment.ucm_resources.CommunicationResourcePort;
import ucm_environment.ucm_resources.Ucm_resourcesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Communication Resource Definition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_environment.ucm_resources.impl.CommunicationResourceDefinitionImpl#getCommunicationPort <em>Communication Port</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CommunicationResourceDefinitionImpl extends IEnvironmentResourceDefinitionImpl implements CommunicationResourceDefinition {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached value of the '{@link #getCommunicationPort() <em>Communication Port</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCommunicationPort()
	 * @generated
	 * @ordered
	 */
	protected EList<CommunicationResourcePort> communicationPort;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CommunicationResourceDefinitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_resourcesPackage.Literals.COMMUNICATION_RESOURCE_DEFINITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CommunicationResourcePort> getCommunicationPort() {
		if (communicationPort == null) {
			communicationPort = new EObjectContainmentEList<CommunicationResourcePort>(CommunicationResourcePort.class, this, Ucm_resourcesPackage.COMMUNICATION_RESOURCE_DEFINITION__COMMUNICATION_PORT);
		}
		return communicationPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE_DEFINITION__COMMUNICATION_PORT:
				return ((InternalEList<?>)getCommunicationPort()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE_DEFINITION__COMMUNICATION_PORT:
				return getCommunicationPort();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE_DEFINITION__COMMUNICATION_PORT:
				getCommunicationPort().clear();
				getCommunicationPort().addAll((Collection<? extends CommunicationResourcePort>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE_DEFINITION__COMMUNICATION_PORT:
				getCommunicationPort().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE_DEFINITION__COMMUNICATION_PORT:
				return communicationPort != null && !communicationPort.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //CommunicationResourceDefinitionImpl
