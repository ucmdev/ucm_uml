/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_environment.ucm_resources.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import ucm_base.ucm_commons.impl.IPlatformModuleImpl;

import ucm_base.ucm_contracts.ContractModule;

import ucm_environment.ucm_resources.CommunicationPortDefinition;
import ucm_environment.ucm_resources.CommunicationResourceDefinition;
import ucm_environment.ucm_resources.ComputationResourceDefinition;
import ucm_environment.ucm_resources.ExecutionResourceDefinition;
import ucm_environment.ucm_resources.MemoryPartitionDefinition;
import ucm_environment.ucm_resources.ResourceDefinitionModule;
import ucm_environment.ucm_resources.Ucm_resourcesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Resource Definition Module</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_environment.ucm_resources.impl.ResourceDefinitionModuleImpl#getComputationResourceDefinition <em>Computation Resource Definition</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.impl.ResourceDefinitionModuleImpl#getCommunicationResourceDefinition <em>Communication Resource Definition</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.impl.ResourceDefinitionModuleImpl#getExecutionResourceDefinition <em>Execution Resource Definition</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.impl.ResourceDefinitionModuleImpl#getMemoryPartitionDefinition <em>Memory Partition Definition</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.impl.ResourceDefinitionModuleImpl#getContractModule <em>Contract Module</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.impl.ResourceDefinitionModuleImpl#getCommunicationPortDefinition <em>Communication Port Definition</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ResourceDefinitionModuleImpl extends IPlatformModuleImpl implements ResourceDefinitionModule {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached value of the '{@link #getComputationResourceDefinition() <em>Computation Resource Definition</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComputationResourceDefinition()
	 * @generated
	 * @ordered
	 */
	protected EList<ComputationResourceDefinition> computationResourceDefinition;

	/**
	 * The cached value of the '{@link #getCommunicationResourceDefinition() <em>Communication Resource Definition</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCommunicationResourceDefinition()
	 * @generated
	 * @ordered
	 */
	protected EList<CommunicationResourceDefinition> communicationResourceDefinition;

	/**
	 * The cached value of the '{@link #getExecutionResourceDefinition() <em>Execution Resource Definition</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutionResourceDefinition()
	 * @generated
	 * @ordered
	 */
	protected EList<ExecutionResourceDefinition> executionResourceDefinition;

	/**
	 * The cached value of the '{@link #getMemoryPartitionDefinition() <em>Memory Partition Definition</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMemoryPartitionDefinition()
	 * @generated
	 * @ordered
	 */
	protected EList<MemoryPartitionDefinition> memoryPartitionDefinition;

	/**
	 * The cached value of the '{@link #getContractModule() <em>Contract Module</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContractModule()
	 * @generated
	 * @ordered
	 */
	protected EList<ContractModule> contractModule;

	/**
	 * The cached value of the '{@link #getCommunicationPortDefinition() <em>Communication Port Definition</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCommunicationPortDefinition()
	 * @generated
	 * @ordered
	 */
	protected EList<CommunicationPortDefinition> communicationPortDefinition;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ResourceDefinitionModuleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_resourcesPackage.Literals.RESOURCE_DEFINITION_MODULE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComputationResourceDefinition> getComputationResourceDefinition() {
		if (computationResourceDefinition == null) {
			computationResourceDefinition = new EObjectContainmentEList<ComputationResourceDefinition>(ComputationResourceDefinition.class, this, Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__COMPUTATION_RESOURCE_DEFINITION);
		}
		return computationResourceDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CommunicationResourceDefinition> getCommunicationResourceDefinition() {
		if (communicationResourceDefinition == null) {
			communicationResourceDefinition = new EObjectContainmentEList<CommunicationResourceDefinition>(CommunicationResourceDefinition.class, this, Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__COMMUNICATION_RESOURCE_DEFINITION);
		}
		return communicationResourceDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExecutionResourceDefinition> getExecutionResourceDefinition() {
		if (executionResourceDefinition == null) {
			executionResourceDefinition = new EObjectContainmentEList<ExecutionResourceDefinition>(ExecutionResourceDefinition.class, this, Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__EXECUTION_RESOURCE_DEFINITION);
		}
		return executionResourceDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MemoryPartitionDefinition> getMemoryPartitionDefinition() {
		if (memoryPartitionDefinition == null) {
			memoryPartitionDefinition = new EObjectContainmentEList<MemoryPartitionDefinition>(MemoryPartitionDefinition.class, this, Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__MEMORY_PARTITION_DEFINITION);
		}
		return memoryPartitionDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ContractModule> getContractModule() {
		if (contractModule == null) {
			contractModule = new EObjectContainmentEList<ContractModule>(ContractModule.class, this, Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__CONTRACT_MODULE);
		}
		return contractModule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CommunicationPortDefinition> getCommunicationPortDefinition() {
		if (communicationPortDefinition == null) {
			communicationPortDefinition = new EObjectContainmentEList<CommunicationPortDefinition>(CommunicationPortDefinition.class, this, Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__COMMUNICATION_PORT_DEFINITION);
		}
		return communicationPortDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__COMPUTATION_RESOURCE_DEFINITION:
				return ((InternalEList<?>)getComputationResourceDefinition()).basicRemove(otherEnd, msgs);
			case Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__COMMUNICATION_RESOURCE_DEFINITION:
				return ((InternalEList<?>)getCommunicationResourceDefinition()).basicRemove(otherEnd, msgs);
			case Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__EXECUTION_RESOURCE_DEFINITION:
				return ((InternalEList<?>)getExecutionResourceDefinition()).basicRemove(otherEnd, msgs);
			case Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__MEMORY_PARTITION_DEFINITION:
				return ((InternalEList<?>)getMemoryPartitionDefinition()).basicRemove(otherEnd, msgs);
			case Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__CONTRACT_MODULE:
				return ((InternalEList<?>)getContractModule()).basicRemove(otherEnd, msgs);
			case Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__COMMUNICATION_PORT_DEFINITION:
				return ((InternalEList<?>)getCommunicationPortDefinition()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__COMPUTATION_RESOURCE_DEFINITION:
				return getComputationResourceDefinition();
			case Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__COMMUNICATION_RESOURCE_DEFINITION:
				return getCommunicationResourceDefinition();
			case Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__EXECUTION_RESOURCE_DEFINITION:
				return getExecutionResourceDefinition();
			case Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__MEMORY_PARTITION_DEFINITION:
				return getMemoryPartitionDefinition();
			case Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__CONTRACT_MODULE:
				return getContractModule();
			case Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__COMMUNICATION_PORT_DEFINITION:
				return getCommunicationPortDefinition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__COMPUTATION_RESOURCE_DEFINITION:
				getComputationResourceDefinition().clear();
				getComputationResourceDefinition().addAll((Collection<? extends ComputationResourceDefinition>)newValue);
				return;
			case Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__COMMUNICATION_RESOURCE_DEFINITION:
				getCommunicationResourceDefinition().clear();
				getCommunicationResourceDefinition().addAll((Collection<? extends CommunicationResourceDefinition>)newValue);
				return;
			case Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__EXECUTION_RESOURCE_DEFINITION:
				getExecutionResourceDefinition().clear();
				getExecutionResourceDefinition().addAll((Collection<? extends ExecutionResourceDefinition>)newValue);
				return;
			case Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__MEMORY_PARTITION_DEFINITION:
				getMemoryPartitionDefinition().clear();
				getMemoryPartitionDefinition().addAll((Collection<? extends MemoryPartitionDefinition>)newValue);
				return;
			case Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__CONTRACT_MODULE:
				getContractModule().clear();
				getContractModule().addAll((Collection<? extends ContractModule>)newValue);
				return;
			case Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__COMMUNICATION_PORT_DEFINITION:
				getCommunicationPortDefinition().clear();
				getCommunicationPortDefinition().addAll((Collection<? extends CommunicationPortDefinition>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__COMPUTATION_RESOURCE_DEFINITION:
				getComputationResourceDefinition().clear();
				return;
			case Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__COMMUNICATION_RESOURCE_DEFINITION:
				getCommunicationResourceDefinition().clear();
				return;
			case Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__EXECUTION_RESOURCE_DEFINITION:
				getExecutionResourceDefinition().clear();
				return;
			case Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__MEMORY_PARTITION_DEFINITION:
				getMemoryPartitionDefinition().clear();
				return;
			case Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__CONTRACT_MODULE:
				getContractModule().clear();
				return;
			case Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__COMMUNICATION_PORT_DEFINITION:
				getCommunicationPortDefinition().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__COMPUTATION_RESOURCE_DEFINITION:
				return computationResourceDefinition != null && !computationResourceDefinition.isEmpty();
			case Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__COMMUNICATION_RESOURCE_DEFINITION:
				return communicationResourceDefinition != null && !communicationResourceDefinition.isEmpty();
			case Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__EXECUTION_RESOURCE_DEFINITION:
				return executionResourceDefinition != null && !executionResourceDefinition.isEmpty();
			case Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__MEMORY_PARTITION_DEFINITION:
				return memoryPartitionDefinition != null && !memoryPartitionDefinition.isEmpty();
			case Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__CONTRACT_MODULE:
				return contractModule != null && !contractModule.isEmpty();
			case Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__COMMUNICATION_PORT_DEFINITION:
				return communicationPortDefinition != null && !communicationPortDefinition.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ResourceDefinitionModuleImpl
