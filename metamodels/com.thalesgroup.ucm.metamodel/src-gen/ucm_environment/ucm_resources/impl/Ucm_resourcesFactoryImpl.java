/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_environment.ucm_resources.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import ucm_environment.ucm_resources.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Ucm_resourcesFactoryImpl extends EFactoryImpl implements Ucm_resourcesFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Ucm_resourcesFactory init() {
		try {
			Ucm_resourcesFactory theUcm_resourcesFactory = (Ucm_resourcesFactory)EPackage.Registry.INSTANCE.getEFactory(Ucm_resourcesPackage.eNS_URI);
			if (theUcm_resourcesFactory != null) {
				return theUcm_resourcesFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new Ucm_resourcesFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ucm_resourcesFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE: return createResourceDefinitionModule();
			case Ucm_resourcesPackage.ENVIRONMENT_MODULE: return createEnvironmentModule();
			case Ucm_resourcesPackage.IENVIRONMENT_RESOURCE: return createIEnvironmentResource();
			case Ucm_resourcesPackage.COMPUTATION_RESOURCE_DEFINITION: return createComputationResourceDefinition();
			case Ucm_resourcesPackage.COMPUTATION_RESOURCE_PORT: return createComputationResourcePort();
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE_DEFINITION: return createCommunicationResourceDefinition();
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE_PORT: return createCommunicationResourcePort();
			case Ucm_resourcesPackage.COMMUNICATION_PORT_DEFINITION: return createCommunicationPortDefinition();
			case Ucm_resourcesPackage.MEMORY_PARTITION_DEFINITION: return createMemoryPartitionDefinition();
			case Ucm_resourcesPackage.EXECUTION_RESOURCE_DEFINITION: return createExecutionResourceDefinition();
			case Ucm_resourcesPackage.COMPUTATION_RESOURCE: return createComputationResource();
			case Ucm_resourcesPackage.COMMUNICATION_RESOURCE: return createCommunicationResource();
			case Ucm_resourcesPackage.COMMUNICATION_PORT: return createCommunicationPort();
			case Ucm_resourcesPackage.EXECUTION_RESOURCE: return createExecutionResource();
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_PARAMETER: return createResourceAllocationParameter();
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_PARAMETER_VALUE: return createResourceAllocationParameterValue();
			case Ucm_resourcesPackage.MEMORY_PARTITION: return createMemoryPartition();
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER: return createResourceAllocationAndConfigurationParameter();
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER_VALUE: return createResourceAllocationAndConfigurationParameterValue();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceDefinitionModule createResourceDefinitionModule() {
		ResourceDefinitionModuleImpl resourceDefinitionModule = new ResourceDefinitionModuleImpl();
		return resourceDefinitionModule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnvironmentModule createEnvironmentModule() {
		EnvironmentModuleImpl environmentModule = new EnvironmentModuleImpl();
		return environmentModule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IEnvironmentResource createIEnvironmentResource() {
		IEnvironmentResourceImpl iEnvironmentResource = new IEnvironmentResourceImpl();
		return iEnvironmentResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComputationResourceDefinition createComputationResourceDefinition() {
		ComputationResourceDefinitionImpl computationResourceDefinition = new ComputationResourceDefinitionImpl();
		return computationResourceDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComputationResourcePort createComputationResourcePort() {
		ComputationResourcePortImpl computationResourcePort = new ComputationResourcePortImpl();
		return computationResourcePort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommunicationResourceDefinition createCommunicationResourceDefinition() {
		CommunicationResourceDefinitionImpl communicationResourceDefinition = new CommunicationResourceDefinitionImpl();
		return communicationResourceDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommunicationResourcePort createCommunicationResourcePort() {
		CommunicationResourcePortImpl communicationResourcePort = new CommunicationResourcePortImpl();
		return communicationResourcePort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommunicationPortDefinition createCommunicationPortDefinition() {
		CommunicationPortDefinitionImpl communicationPortDefinition = new CommunicationPortDefinitionImpl();
		return communicationPortDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MemoryPartitionDefinition createMemoryPartitionDefinition() {
		MemoryPartitionDefinitionImpl memoryPartitionDefinition = new MemoryPartitionDefinitionImpl();
		return memoryPartitionDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExecutionResourceDefinition createExecutionResourceDefinition() {
		ExecutionResourceDefinitionImpl executionResourceDefinition = new ExecutionResourceDefinitionImpl();
		return executionResourceDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComputationResource createComputationResource() {
		ComputationResourceImpl computationResource = new ComputationResourceImpl();
		return computationResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommunicationResource createCommunicationResource() {
		CommunicationResourceImpl communicationResource = new CommunicationResourceImpl();
		return communicationResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommunicationPort createCommunicationPort() {
		CommunicationPortImpl communicationPort = new CommunicationPortImpl();
		return communicationPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExecutionResource createExecutionResource() {
		ExecutionResourceImpl executionResource = new ExecutionResourceImpl();
		return executionResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceAllocationParameter createResourceAllocationParameter() {
		ResourceAllocationParameterImpl resourceAllocationParameter = new ResourceAllocationParameterImpl();
		return resourceAllocationParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceAllocationParameterValue createResourceAllocationParameterValue() {
		ResourceAllocationParameterValueImpl resourceAllocationParameterValue = new ResourceAllocationParameterValueImpl();
		return resourceAllocationParameterValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MemoryPartition createMemoryPartition() {
		MemoryPartitionImpl memoryPartition = new MemoryPartitionImpl();
		return memoryPartition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceAllocationAndConfigurationParameter createResourceAllocationAndConfigurationParameter() {
		ResourceAllocationAndConfigurationParameterImpl resourceAllocationAndConfigurationParameter = new ResourceAllocationAndConfigurationParameterImpl();
		return resourceAllocationAndConfigurationParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceAllocationAndConfigurationParameterValue createResourceAllocationAndConfigurationParameterValue() {
		ResourceAllocationAndConfigurationParameterValueImpl resourceAllocationAndConfigurationParameterValue = new ResourceAllocationAndConfigurationParameterValueImpl();
		return resourceAllocationAndConfigurationParameterValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ucm_resourcesPackage getUcm_resourcesPackage() {
		return (Ucm_resourcesPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static Ucm_resourcesPackage getPackage() {
		return Ucm_resourcesPackage.eINSTANCE;
	}

} //Ucm_resourcesFactoryImpl
