/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_environment.ucm_resources.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import ucm_base.ucm_commons.Ucm_commonsPackage;

import ucm_base.ucm_components.Ucm_componentsPackage;

import ucm_base.ucm_contracts.Ucm_contractsPackage;

import ucm_base.ucm_interactions.Ucm_interactionsPackage;

import ucm_base.ucm_technicalpolicies.Ucm_technicalpoliciesPackage;

import ucm_basic_projects.Ucm_basic_projectsPackage;

import ucm_environment.ucm_resources.CommunicationPort;
import ucm_environment.ucm_resources.CommunicationPortDefinition;
import ucm_environment.ucm_resources.CommunicationResource;
import ucm_environment.ucm_resources.CommunicationResourceDefinition;
import ucm_environment.ucm_resources.CommunicationResourcePort;
import ucm_environment.ucm_resources.ComputationResource;
import ucm_environment.ucm_resources.ComputationResourceDefinition;
import ucm_environment.ucm_resources.ComputationResourcePort;
import ucm_environment.ucm_resources.EnvironmentModule;
import ucm_environment.ucm_resources.ExecutionResource;
import ucm_environment.ucm_resources.ExecutionResourceDefinition;
import ucm_environment.ucm_resources.IEnvironmentResource;
import ucm_environment.ucm_resources.IEnvironmentResourceDefinition;
import ucm_environment.ucm_resources.MemoryPartition;
import ucm_environment.ucm_resources.MemoryPartitionDefinition;
import ucm_environment.ucm_resources.ResourceAllocationAndConfigurationParameter;
import ucm_environment.ucm_resources.ResourceAllocationAndConfigurationParameterValue;
import ucm_environment.ucm_resources.ResourceAllocationParameter;
import ucm_environment.ucm_resources.ResourceAllocationParameterValue;
import ucm_environment.ucm_resources.ResourceDefinitionModule;
import ucm_environment.ucm_resources.Ucm_resourcesFactory;
import ucm_environment.ucm_resources.Ucm_resourcesPackage;

import ucm_environment.ucm_resources.util.Ucm_resourcesValidator;
import ucm_extended_projects.Ucm_extended_projectsPackage;

import ucm_extended_projects.impl.Ucm_extended_projectsPackageImpl;

import ucm_supplement.ucm_deployments.Ucm_deploymentsPackage;

import ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl;

import ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage;

import ucm_supplement.ucm_detailed_components.impl.Ucm_detailed_componentsPackageImpl;

import ucm_supplement.ucm_enhanced_configuration.Ucm_enhanced_configurationPackage;

import ucm_supplement.ucm_enhanced_configuration.impl.Ucm_enhanced_configurationPackageImpl;

import ucm_supplement.ucm_enhanced_portspec.Ucm_enhanced_portspecPackage;

import ucm_supplement.ucm_enhanced_portspec.impl.Ucm_enhanced_portspecPackageImpl;

import ucm_supplement.ucm_test_cases.Ucm_test_casesPackage;

import ucm_supplement.ucm_test_cases.impl.Ucm_test_casesPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Ucm_resourcesPackageImpl extends EPackageImpl implements Ucm_resourcesPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass resourceDefinitionModuleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass environmentModuleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iEnvironmentResourceDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iEnvironmentResourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass computationResourceDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass computationResourcePortEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass communicationResourceDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass communicationResourcePortEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass communicationPortDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass memoryPartitionDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass executionResourceDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass computationResourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass communicationResourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass communicationPortEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass executionResourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass resourceAllocationParameterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass resourceAllocationParameterValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass memoryPartitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass resourceAllocationAndConfigurationParameterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass resourceAllocationAndConfigurationParameterValueEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private Ucm_resourcesPackageImpl() {
		super(eNS_URI, Ucm_resourcesFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link Ucm_resourcesPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static Ucm_resourcesPackage init() {
		if (isInited) return (Ucm_resourcesPackage)EPackage.Registry.INSTANCE.getEPackage(Ucm_resourcesPackage.eNS_URI);

		// Obtain or create and register package
		Ucm_resourcesPackageImpl theUcm_resourcesPackage = (Ucm_resourcesPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof Ucm_resourcesPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new Ucm_resourcesPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		Ucm_basic_projectsPackage.eINSTANCE.eClass();
		Ucm_commonsPackage.eINSTANCE.eClass();
		Ucm_technicalpoliciesPackage.eINSTANCE.eClass();
		Ucm_contractsPackage.eINSTANCE.eClass();
		Ucm_interactionsPackage.eINSTANCE.eClass();
		Ucm_componentsPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Ucm_extended_projectsPackageImpl theUcm_extended_projectsPackage = (Ucm_extended_projectsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_extended_projectsPackage.eNS_URI) instanceof Ucm_extended_projectsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_extended_projectsPackage.eNS_URI) : Ucm_extended_projectsPackage.eINSTANCE);
		Ucm_detailed_componentsPackageImpl theUcm_detailed_componentsPackage = (Ucm_detailed_componentsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_detailed_componentsPackage.eNS_URI) instanceof Ucm_detailed_componentsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_detailed_componentsPackage.eNS_URI) : Ucm_detailed_componentsPackage.eINSTANCE);
		Ucm_enhanced_portspecPackageImpl theUcm_enhanced_portspecPackage = (Ucm_enhanced_portspecPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_enhanced_portspecPackage.eNS_URI) instanceof Ucm_enhanced_portspecPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_enhanced_portspecPackage.eNS_URI) : Ucm_enhanced_portspecPackage.eINSTANCE);
		Ucm_deploymentsPackageImpl theUcm_deploymentsPackage = (Ucm_deploymentsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_deploymentsPackage.eNS_URI) instanceof Ucm_deploymentsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_deploymentsPackage.eNS_URI) : Ucm_deploymentsPackage.eINSTANCE);
		Ucm_test_casesPackageImpl theUcm_test_casesPackage = (Ucm_test_casesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_test_casesPackage.eNS_URI) instanceof Ucm_test_casesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_test_casesPackage.eNS_URI) : Ucm_test_casesPackage.eINSTANCE);
		Ucm_enhanced_configurationPackageImpl theUcm_enhanced_configurationPackage = (Ucm_enhanced_configurationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_enhanced_configurationPackage.eNS_URI) instanceof Ucm_enhanced_configurationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_enhanced_configurationPackage.eNS_URI) : Ucm_enhanced_configurationPackage.eINSTANCE);

		// Create package meta-data objects
		theUcm_resourcesPackage.createPackageContents();
		theUcm_extended_projectsPackage.createPackageContents();
		theUcm_detailed_componentsPackage.createPackageContents();
		theUcm_enhanced_portspecPackage.createPackageContents();
		theUcm_deploymentsPackage.createPackageContents();
		theUcm_test_casesPackage.createPackageContents();
		theUcm_enhanced_configurationPackage.createPackageContents();

		// Initialize created meta-data
		theUcm_resourcesPackage.initializePackageContents();
		theUcm_extended_projectsPackage.initializePackageContents();
		theUcm_detailed_componentsPackage.initializePackageContents();
		theUcm_enhanced_portspecPackage.initializePackageContents();
		theUcm_deploymentsPackage.initializePackageContents();
		theUcm_test_casesPackage.initializePackageContents();
		theUcm_enhanced_configurationPackage.initializePackageContents();

		// Register package validator
		EValidator.Registry.INSTANCE.put
			(theUcm_resourcesPackage, 
			 new EValidator.Descriptor() {
				 public EValidator getEValidator() {
					 return Ucm_resourcesValidator.INSTANCE;
				 }
			 });

		// Mark meta-data to indicate it can't be changed
		theUcm_resourcesPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(Ucm_resourcesPackage.eNS_URI, theUcm_resourcesPackage);
		return theUcm_resourcesPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getResourceDefinitionModule() {
		return resourceDefinitionModuleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getResourceDefinitionModule_ComputationResourceDefinition() {
		return (EReference)resourceDefinitionModuleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getResourceDefinitionModule_CommunicationResourceDefinition() {
		return (EReference)resourceDefinitionModuleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getResourceDefinitionModule_ExecutionResourceDefinition() {
		return (EReference)resourceDefinitionModuleEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getResourceDefinitionModule_MemoryPartitionDefinition() {
		return (EReference)resourceDefinitionModuleEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getResourceDefinitionModule_ContractModule() {
		return (EReference)resourceDefinitionModuleEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getResourceDefinitionModule_CommunicationPortDefinition() {
		return (EReference)resourceDefinitionModuleEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEnvironmentModule() {
		return environmentModuleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEnvironmentModule_ComputationResource() {
		return (EReference)environmentModuleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEnvironmentModule_CommunicationResource() {
		return (EReference)environmentModuleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIEnvironmentResourceDefinition() {
		return iEnvironmentResourceDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIEnvironmentResource() {
		return iEnvironmentResourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComputationResourceDefinition() {
		return computationResourceDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComputationResourceDefinition_ComputationPort() {
		return (EReference)computationResourceDefinitionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComputationResourceDefinition_PerformanceFactor() {
		return (EAttribute)computationResourceDefinitionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComputationResourcePort() {
		return computationResourcePortEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComputationResourcePort_Definition() {
		return (EReference)computationResourcePortEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCommunicationResourceDefinition() {
		return communicationResourceDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCommunicationResourceDefinition_CommunicationPort() {
		return (EReference)communicationResourceDefinitionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCommunicationResourcePort() {
		return communicationResourcePortEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCommunicationResourcePort_Definition() {
		return (EReference)communicationResourcePortEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCommunicationResourcePort_Min() {
		return (EAttribute)communicationResourcePortEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCommunicationResourcePort_Max() {
		return (EAttribute)communicationResourcePortEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCommunicationPortDefinition() {
		return communicationPortDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMemoryPartitionDefinition() {
		return memoryPartitionDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExecutionResourceDefinition() {
		return executionResourceDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComputationResource() {
		return computationResourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComputationResource_Definition() {
		return (EReference)computationResourceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCommunicationResource() {
		return communicationResourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCommunicationResource_Definition() {
		return (EReference)communicationResourceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCommunicationResource_CommunicationPort() {
		return (EReference)communicationResourceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCommunicationPort() {
		return communicationPortEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCommunicationPort_ComputationResource() {
		return (EReference)communicationPortEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCommunicationPort_ComputationPort() {
		return (EReference)communicationPortEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExecutionResource() {
		return executionResourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExecutionResource_Definition() {
		return (EReference)executionResourceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getResourceAllocationParameter() {
		return resourceAllocationParameterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getResourceAllocationParameter_AllowedResourceDefinition() {
		return (EReference)resourceAllocationParameterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getResourceAllocationParameter_Min() {
		return (EAttribute)resourceAllocationParameterEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getResourceAllocationParameter_Max() {
		return (EAttribute)resourceAllocationParameterEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getResourceAllocationParameterValue() {
		return resourceAllocationParameterValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getResourceAllocationParameterValue_ParameterDefinition() {
		return (EReference)resourceAllocationParameterValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getResourceAllocationParameterValue_Resource() {
		return (EReference)resourceAllocationParameterValueEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMemoryPartition() {
		return memoryPartitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMemoryPartition_MemoryPartitionDefinition() {
		return (EReference)memoryPartitionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMemoryPartition_ConfigurationParameterValue() {
		return (EReference)memoryPartitionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMemoryPartition_ExecutionResource() {
		return (EReference)memoryPartitionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getResourceAllocationAndConfigurationParameter() {
		return resourceAllocationAndConfigurationParameterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getResourceAllocationAndConfigurationParameter_AllowedResourceDefinition() {
		return (EReference)resourceAllocationAndConfigurationParameterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getResourceAllocationAndConfigurationParameter_Min() {
		return (EAttribute)resourceAllocationAndConfigurationParameterEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getResourceAllocationAndConfigurationParameter_Max() {
		return (EAttribute)resourceAllocationAndConfigurationParameterEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getResourceAllocationAndConfigurationParameterValue() {
		return resourceAllocationAndConfigurationParameterValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getResourceAllocationAndConfigurationParameterValue_ParameterDefinition() {
		return (EReference)resourceAllocationAndConfigurationParameterValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getResourceAllocationAndConfigurationParameterValue_Resource() {
		return (EReference)resourceAllocationAndConfigurationParameterValueEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ucm_resourcesFactory getUcm_resourcesFactory() {
		return (Ucm_resourcesFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		resourceDefinitionModuleEClass = createEClass(RESOURCE_DEFINITION_MODULE);
		createEReference(resourceDefinitionModuleEClass, RESOURCE_DEFINITION_MODULE__COMPUTATION_RESOURCE_DEFINITION);
		createEReference(resourceDefinitionModuleEClass, RESOURCE_DEFINITION_MODULE__COMMUNICATION_RESOURCE_DEFINITION);
		createEReference(resourceDefinitionModuleEClass, RESOURCE_DEFINITION_MODULE__EXECUTION_RESOURCE_DEFINITION);
		createEReference(resourceDefinitionModuleEClass, RESOURCE_DEFINITION_MODULE__MEMORY_PARTITION_DEFINITION);
		createEReference(resourceDefinitionModuleEClass, RESOURCE_DEFINITION_MODULE__CONTRACT_MODULE);
		createEReference(resourceDefinitionModuleEClass, RESOURCE_DEFINITION_MODULE__COMMUNICATION_PORT_DEFINITION);

		environmentModuleEClass = createEClass(ENVIRONMENT_MODULE);
		createEReference(environmentModuleEClass, ENVIRONMENT_MODULE__COMPUTATION_RESOURCE);
		createEReference(environmentModuleEClass, ENVIRONMENT_MODULE__COMMUNICATION_RESOURCE);

		iEnvironmentResourceDefinitionEClass = createEClass(IENVIRONMENT_RESOURCE_DEFINITION);

		iEnvironmentResourceEClass = createEClass(IENVIRONMENT_RESOURCE);

		computationResourceDefinitionEClass = createEClass(COMPUTATION_RESOURCE_DEFINITION);
		createEReference(computationResourceDefinitionEClass, COMPUTATION_RESOURCE_DEFINITION__COMPUTATION_PORT);
		createEAttribute(computationResourceDefinitionEClass, COMPUTATION_RESOURCE_DEFINITION__PERFORMANCE_FACTOR);

		computationResourcePortEClass = createEClass(COMPUTATION_RESOURCE_PORT);
		createEReference(computationResourcePortEClass, COMPUTATION_RESOURCE_PORT__DEFINITION);

		communicationResourceDefinitionEClass = createEClass(COMMUNICATION_RESOURCE_DEFINITION);
		createEReference(communicationResourceDefinitionEClass, COMMUNICATION_RESOURCE_DEFINITION__COMMUNICATION_PORT);

		communicationResourcePortEClass = createEClass(COMMUNICATION_RESOURCE_PORT);
		createEReference(communicationResourcePortEClass, COMMUNICATION_RESOURCE_PORT__DEFINITION);
		createEAttribute(communicationResourcePortEClass, COMMUNICATION_RESOURCE_PORT__MIN);
		createEAttribute(communicationResourcePortEClass, COMMUNICATION_RESOURCE_PORT__MAX);

		communicationPortDefinitionEClass = createEClass(COMMUNICATION_PORT_DEFINITION);

		memoryPartitionDefinitionEClass = createEClass(MEMORY_PARTITION_DEFINITION);

		executionResourceDefinitionEClass = createEClass(EXECUTION_RESOURCE_DEFINITION);

		computationResourceEClass = createEClass(COMPUTATION_RESOURCE);
		createEReference(computationResourceEClass, COMPUTATION_RESOURCE__DEFINITION);

		communicationResourceEClass = createEClass(COMMUNICATION_RESOURCE);
		createEReference(communicationResourceEClass, COMMUNICATION_RESOURCE__DEFINITION);
		createEReference(communicationResourceEClass, COMMUNICATION_RESOURCE__COMMUNICATION_PORT);

		communicationPortEClass = createEClass(COMMUNICATION_PORT);
		createEReference(communicationPortEClass, COMMUNICATION_PORT__COMPUTATION_RESOURCE);
		createEReference(communicationPortEClass, COMMUNICATION_PORT__COMPUTATION_PORT);

		executionResourceEClass = createEClass(EXECUTION_RESOURCE);
		createEReference(executionResourceEClass, EXECUTION_RESOURCE__DEFINITION);

		resourceAllocationParameterEClass = createEClass(RESOURCE_ALLOCATION_PARAMETER);
		createEReference(resourceAllocationParameterEClass, RESOURCE_ALLOCATION_PARAMETER__ALLOWED_RESOURCE_DEFINITION);
		createEAttribute(resourceAllocationParameterEClass, RESOURCE_ALLOCATION_PARAMETER__MIN);
		createEAttribute(resourceAllocationParameterEClass, RESOURCE_ALLOCATION_PARAMETER__MAX);

		resourceAllocationParameterValueEClass = createEClass(RESOURCE_ALLOCATION_PARAMETER_VALUE);
		createEReference(resourceAllocationParameterValueEClass, RESOURCE_ALLOCATION_PARAMETER_VALUE__PARAMETER_DEFINITION);
		createEReference(resourceAllocationParameterValueEClass, RESOURCE_ALLOCATION_PARAMETER_VALUE__RESOURCE);

		memoryPartitionEClass = createEClass(MEMORY_PARTITION);
		createEReference(memoryPartitionEClass, MEMORY_PARTITION__MEMORY_PARTITION_DEFINITION);
		createEReference(memoryPartitionEClass, MEMORY_PARTITION__CONFIGURATION_PARAMETER_VALUE);
		createEReference(memoryPartitionEClass, MEMORY_PARTITION__EXECUTION_RESOURCE);

		resourceAllocationAndConfigurationParameterEClass = createEClass(RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER);
		createEReference(resourceAllocationAndConfigurationParameterEClass, RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__ALLOWED_RESOURCE_DEFINITION);
		createEAttribute(resourceAllocationAndConfigurationParameterEClass, RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__MIN);
		createEAttribute(resourceAllocationAndConfigurationParameterEClass, RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER__MAX);

		resourceAllocationAndConfigurationParameterValueEClass = createEClass(RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER_VALUE);
		createEReference(resourceAllocationAndConfigurationParameterValueEClass, RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER_VALUE__PARAMETER_DEFINITION);
		createEReference(resourceAllocationAndConfigurationParameterValueEClass, RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER_VALUE__RESOURCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		Ucm_commonsPackage theUcm_commonsPackage = (Ucm_commonsPackage)EPackage.Registry.INSTANCE.getEPackage(Ucm_commonsPackage.eNS_URI);
		Ucm_contractsPackage theUcm_contractsPackage = (Ucm_contractsPackage)EPackage.Registry.INSTANCE.getEPackage(Ucm_contractsPackage.eNS_URI);
		Ucm_deploymentsPackage theUcm_deploymentsPackage = (Ucm_deploymentsPackage)EPackage.Registry.INSTANCE.getEPackage(Ucm_deploymentsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		resourceDefinitionModuleEClass.getESuperTypes().add(theUcm_commonsPackage.getIPlatformModule());
		environmentModuleEClass.getESuperTypes().add(theUcm_commonsPackage.getIApplicationModule());
		iEnvironmentResourceDefinitionEClass.getESuperTypes().add(theUcm_contractsPackage.getIConfigurable());
		iEnvironmentResourceDefinitionEClass.getESuperTypes().add(theUcm_commonsPackage.getINamed());
		iEnvironmentResourceEClass.getESuperTypes().add(theUcm_contractsPackage.getIConfigured());
		iEnvironmentResourceEClass.getESuperTypes().add(theUcm_commonsPackage.getINamed());
		computationResourceDefinitionEClass.getESuperTypes().add(this.getIEnvironmentResourceDefinition());
		computationResourcePortEClass.getESuperTypes().add(theUcm_contractsPackage.getIConfigurable());
		computationResourcePortEClass.getESuperTypes().add(theUcm_commonsPackage.getINamed());
		communicationResourceDefinitionEClass.getESuperTypes().add(this.getIEnvironmentResourceDefinition());
		communicationResourcePortEClass.getESuperTypes().add(theUcm_contractsPackage.getIConfigurable());
		communicationResourcePortEClass.getESuperTypes().add(theUcm_commonsPackage.getINamed());
		communicationPortDefinitionEClass.getESuperTypes().add(this.getIEnvironmentResourceDefinition());
		memoryPartitionDefinitionEClass.getESuperTypes().add(this.getIEnvironmentResourceDefinition());
		executionResourceDefinitionEClass.getESuperTypes().add(this.getIEnvironmentResourceDefinition());
		computationResourceEClass.getESuperTypes().add(this.getIEnvironmentResource());
		communicationResourceEClass.getESuperTypes().add(this.getIEnvironmentResource());
		communicationPortEClass.getESuperTypes().add(this.getIEnvironmentResource());
		executionResourceEClass.getESuperTypes().add(this.getIEnvironmentResource());
		resourceAllocationParameterEClass.getESuperTypes().add(theUcm_contractsPackage.getIConfigurationParameter());
		resourceAllocationParameterValueEClass.getESuperTypes().add(theUcm_contractsPackage.getIConfigurationParameterValue());
		memoryPartitionEClass.getESuperTypes().add(theUcm_deploymentsPackage.getIComponentNode());
		resourceAllocationAndConfigurationParameterEClass.getESuperTypes().add(theUcm_contractsPackage.getIConfigurationParameter());
		resourceAllocationAndConfigurationParameterEClass.getESuperTypes().add(theUcm_contractsPackage.getIHasDatatype());
		resourceAllocationAndConfigurationParameterEClass.getESuperTypes().add(theUcm_contractsPackage.getIHasDefaultValue());
		resourceAllocationAndConfigurationParameterValueEClass.getESuperTypes().add(theUcm_contractsPackage.getIConfigurationParameterValue());
		resourceAllocationAndConfigurationParameterValueEClass.getESuperTypes().add(theUcm_contractsPackage.getIValued());

		// Initialize classes and features; add operations and parameters
		initEClass(resourceDefinitionModuleEClass, ResourceDefinitionModule.class, "ResourceDefinitionModule", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getResourceDefinitionModule_ComputationResourceDefinition(), this.getComputationResourceDefinition(), null, "computationResourceDefinition", null, 0, -1, ResourceDefinitionModule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getResourceDefinitionModule_CommunicationResourceDefinition(), this.getCommunicationResourceDefinition(), null, "communicationResourceDefinition", null, 0, -1, ResourceDefinitionModule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getResourceDefinitionModule_ExecutionResourceDefinition(), this.getExecutionResourceDefinition(), null, "executionResourceDefinition", null, 0, -1, ResourceDefinitionModule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getResourceDefinitionModule_MemoryPartitionDefinition(), this.getMemoryPartitionDefinition(), null, "memoryPartitionDefinition", null, 0, -1, ResourceDefinitionModule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getResourceDefinitionModule_ContractModule(), theUcm_contractsPackage.getContractModule(), null, "contractModule", null, 0, -1, ResourceDefinitionModule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getResourceDefinitionModule_CommunicationPortDefinition(), this.getCommunicationPortDefinition(), null, "communicationPortDefinition", null, 0, -1, ResourceDefinitionModule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(environmentModuleEClass, EnvironmentModule.class, "EnvironmentModule", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEnvironmentModule_ComputationResource(), this.getComputationResource(), null, "computationResource", null, 0, -1, EnvironmentModule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEnvironmentModule_CommunicationResource(), this.getCommunicationResource(), null, "communicationResource", null, 0, -1, EnvironmentModule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = addEOperation(environmentModuleEClass, ecorePackage.getEBoolean(), "oneConnectionPerPort", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
		EGenericType g1 = createEGenericType(ecorePackage.getEMap());
		EGenericType g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(environmentModuleEClass, ecorePackage.getEBoolean(), "initializationIsComplete", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(iEnvironmentResourceDefinitionEClass, IEnvironmentResourceDefinition.class, "IEnvironmentResourceDefinition", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(iEnvironmentResourceEClass, IEnvironmentResource.class, "IEnvironmentResource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(computationResourceDefinitionEClass, ComputationResourceDefinition.class, "ComputationResourceDefinition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getComputationResourceDefinition_ComputationPort(), this.getComputationResourcePort(), null, "computationPort", null, 0, -1, ComputationResourceDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComputationResourceDefinition_PerformanceFactor(), ecorePackage.getEDouble(), "performanceFactor", null, 1, 1, ComputationResourceDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(computationResourcePortEClass, ComputationResourcePort.class, "ComputationResourcePort", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getComputationResourcePort_Definition(), this.getCommunicationPortDefinition(), null, "definition", null, 1, 1, ComputationResourcePort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(communicationResourceDefinitionEClass, CommunicationResourceDefinition.class, "CommunicationResourceDefinition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCommunicationResourceDefinition_CommunicationPort(), this.getCommunicationResourcePort(), null, "communicationPort", null, 1, -1, CommunicationResourceDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(communicationResourcePortEClass, CommunicationResourcePort.class, "CommunicationResourcePort", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCommunicationResourcePort_Definition(), this.getCommunicationPortDefinition(), null, "definition", null, 1, 1, CommunicationResourcePort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCommunicationResourcePort_Min(), ecorePackage.getELong(), "min", "1", 1, 1, CommunicationResourcePort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCommunicationResourcePort_Max(), ecorePackage.getELong(), "max", "-1", 1, 1, CommunicationResourcePort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(communicationPortDefinitionEClass, CommunicationPortDefinition.class, "CommunicationPortDefinition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(memoryPartitionDefinitionEClass, MemoryPartitionDefinition.class, "MemoryPartitionDefinition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(executionResourceDefinitionEClass, ExecutionResourceDefinition.class, "ExecutionResourceDefinition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(computationResourceEClass, ComputationResource.class, "ComputationResource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getComputationResource_Definition(), this.getComputationResourceDefinition(), null, "definition", null, 1, 1, ComputationResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(communicationResourceEClass, CommunicationResource.class, "CommunicationResource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCommunicationResource_Definition(), this.getCommunicationResourceDefinition(), null, "definition", null, 1, 1, CommunicationResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCommunicationResource_CommunicationPort(), this.getCommunicationPort(), null, "communicationPort", null, 0, -1, CommunicationResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(communicationResourceEClass, ecorePackage.getEBoolean(), "portCardinality", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(communicationPortEClass, CommunicationPort.class, "CommunicationPort", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCommunicationPort_ComputationResource(), this.getComputationResource(), null, "computationResource", null, 1, 1, CommunicationPort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCommunicationPort_ComputationPort(), this.getComputationResourcePort(), null, "computationPort", null, 1, 1, CommunicationPort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(executionResourceEClass, ExecutionResource.class, "ExecutionResource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getExecutionResource_Definition(), this.getExecutionResourceDefinition(), null, "definition", null, 1, 1, ExecutionResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(resourceAllocationParameterEClass, ResourceAllocationParameter.class, "ResourceAllocationParameter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getResourceAllocationParameter_AllowedResourceDefinition(), this.getIEnvironmentResourceDefinition(), null, "allowedResourceDefinition", null, 1, -1, ResourceAllocationParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getResourceAllocationParameter_Min(), ecorePackage.getELong(), "min", "1", 1, 1, ResourceAllocationParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getResourceAllocationParameter_Max(), ecorePackage.getELong(), "max", "1", 1, 1, ResourceAllocationParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(resourceAllocationParameterValueEClass, ResourceAllocationParameterValue.class, "ResourceAllocationParameterValue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getResourceAllocationParameterValue_ParameterDefinition(), this.getResourceAllocationParameter(), null, "parameterDefinition", null, 1, 1, ResourceAllocationParameterValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getResourceAllocationParameterValue_Resource(), this.getIEnvironmentResource(), null, "resource", null, 1, 1, ResourceAllocationParameterValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(resourceAllocationParameterValueEClass, ecorePackage.getEBoolean(), "parameterDefinitionConsistency", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(memoryPartitionEClass, MemoryPartition.class, "MemoryPartition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMemoryPartition_MemoryPartitionDefinition(), this.getMemoryPartitionDefinition(), null, "memoryPartitionDefinition", null, 1, 1, MemoryPartition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMemoryPartition_ConfigurationParameterValue(), theUcm_contractsPackage.getIConfigurationParameterValue(), null, "configurationParameterValue", null, 0, -1, MemoryPartition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMemoryPartition_ExecutionResource(), this.getExecutionResource(), null, "executionResource", null, 0, -1, MemoryPartition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(resourceAllocationAndConfigurationParameterEClass, ResourceAllocationAndConfigurationParameter.class, "ResourceAllocationAndConfigurationParameter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getResourceAllocationAndConfigurationParameter_AllowedResourceDefinition(), this.getIEnvironmentResourceDefinition(), null, "allowedResourceDefinition", null, 1, -1, ResourceAllocationAndConfigurationParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getResourceAllocationAndConfigurationParameter_Min(), ecorePackage.getELong(), "min", "1", 1, 1, ResourceAllocationAndConfigurationParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getResourceAllocationAndConfigurationParameter_Max(), ecorePackage.getELong(), "max", "1", 1, 1, ResourceAllocationAndConfigurationParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(resourceAllocationAndConfigurationParameterValueEClass, ResourceAllocationAndConfigurationParameterValue.class, "ResourceAllocationAndConfigurationParameterValue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getResourceAllocationAndConfigurationParameterValue_ParameterDefinition(), this.getResourceAllocationAndConfigurationParameter(), null, "parameterDefinition", null, 1, 1, ResourceAllocationAndConfigurationParameterValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getResourceAllocationAndConfigurationParameterValue_Resource(), this.getIEnvironmentResource(), null, "resource", null, 1, 1, ResourceAllocationAndConfigurationParameterValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //Ucm_resourcesPackageImpl
