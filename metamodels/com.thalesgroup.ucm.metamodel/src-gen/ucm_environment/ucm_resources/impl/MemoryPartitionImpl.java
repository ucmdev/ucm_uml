/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_environment.ucm_resources.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import ucm_base.ucm_contracts.IConfigurationParameterValue;

import ucm_environment.ucm_resources.ExecutionResource;
import ucm_environment.ucm_resources.MemoryPartition;
import ucm_environment.ucm_resources.MemoryPartitionDefinition;
import ucm_environment.ucm_resources.Ucm_resourcesPackage;

import ucm_supplement.ucm_deployments.impl.IComponentNodeImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Memory Partition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_environment.ucm_resources.impl.MemoryPartitionImpl#getMemoryPartitionDefinition <em>Memory Partition Definition</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.impl.MemoryPartitionImpl#getConfigurationParameterValue <em>Configuration Parameter Value</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.impl.MemoryPartitionImpl#getExecutionResource <em>Execution Resource</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MemoryPartitionImpl extends IComponentNodeImpl implements MemoryPartition {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached value of the '{@link #getMemoryPartitionDefinition() <em>Memory Partition Definition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMemoryPartitionDefinition()
	 * @generated
	 * @ordered
	 */
	protected MemoryPartitionDefinition memoryPartitionDefinition;

	/**
	 * The cached value of the '{@link #getConfigurationParameterValue() <em>Configuration Parameter Value</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConfigurationParameterValue()
	 * @generated
	 * @ordered
	 */
	protected EList<IConfigurationParameterValue> configurationParameterValue;

	/**
	 * The cached value of the '{@link #getExecutionResource() <em>Execution Resource</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutionResource()
	 * @generated
	 * @ordered
	 */
	protected EList<ExecutionResource> executionResource;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MemoryPartitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_resourcesPackage.Literals.MEMORY_PARTITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MemoryPartitionDefinition getMemoryPartitionDefinition() {
		if (memoryPartitionDefinition != null && memoryPartitionDefinition.eIsProxy()) {
			InternalEObject oldMemoryPartitionDefinition = (InternalEObject)memoryPartitionDefinition;
			memoryPartitionDefinition = (MemoryPartitionDefinition)eResolveProxy(oldMemoryPartitionDefinition);
			if (memoryPartitionDefinition != oldMemoryPartitionDefinition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_resourcesPackage.MEMORY_PARTITION__MEMORY_PARTITION_DEFINITION, oldMemoryPartitionDefinition, memoryPartitionDefinition));
			}
		}
		return memoryPartitionDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MemoryPartitionDefinition basicGetMemoryPartitionDefinition() {
		return memoryPartitionDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMemoryPartitionDefinition(MemoryPartitionDefinition newMemoryPartitionDefinition) {
		MemoryPartitionDefinition oldMemoryPartitionDefinition = memoryPartitionDefinition;
		memoryPartitionDefinition = newMemoryPartitionDefinition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_resourcesPackage.MEMORY_PARTITION__MEMORY_PARTITION_DEFINITION, oldMemoryPartitionDefinition, memoryPartitionDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IConfigurationParameterValue> getConfigurationParameterValue() {
		if (configurationParameterValue == null) {
			configurationParameterValue = new EObjectContainmentEList<IConfigurationParameterValue>(IConfigurationParameterValue.class, this, Ucm_resourcesPackage.MEMORY_PARTITION__CONFIGURATION_PARAMETER_VALUE);
		}
		return configurationParameterValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExecutionResource> getExecutionResource() {
		if (executionResource == null) {
			executionResource = new EObjectContainmentEList<ExecutionResource>(ExecutionResource.class, this, Ucm_resourcesPackage.MEMORY_PARTITION__EXECUTION_RESOURCE);
		}
		return executionResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Ucm_resourcesPackage.MEMORY_PARTITION__CONFIGURATION_PARAMETER_VALUE:
				return ((InternalEList<?>)getConfigurationParameterValue()).basicRemove(otherEnd, msgs);
			case Ucm_resourcesPackage.MEMORY_PARTITION__EXECUTION_RESOURCE:
				return ((InternalEList<?>)getExecutionResource()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_resourcesPackage.MEMORY_PARTITION__MEMORY_PARTITION_DEFINITION:
				if (resolve) return getMemoryPartitionDefinition();
				return basicGetMemoryPartitionDefinition();
			case Ucm_resourcesPackage.MEMORY_PARTITION__CONFIGURATION_PARAMETER_VALUE:
				return getConfigurationParameterValue();
			case Ucm_resourcesPackage.MEMORY_PARTITION__EXECUTION_RESOURCE:
				return getExecutionResource();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_resourcesPackage.MEMORY_PARTITION__MEMORY_PARTITION_DEFINITION:
				setMemoryPartitionDefinition((MemoryPartitionDefinition)newValue);
				return;
			case Ucm_resourcesPackage.MEMORY_PARTITION__CONFIGURATION_PARAMETER_VALUE:
				getConfigurationParameterValue().clear();
				getConfigurationParameterValue().addAll((Collection<? extends IConfigurationParameterValue>)newValue);
				return;
			case Ucm_resourcesPackage.MEMORY_PARTITION__EXECUTION_RESOURCE:
				getExecutionResource().clear();
				getExecutionResource().addAll((Collection<? extends ExecutionResource>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_resourcesPackage.MEMORY_PARTITION__MEMORY_PARTITION_DEFINITION:
				setMemoryPartitionDefinition((MemoryPartitionDefinition)null);
				return;
			case Ucm_resourcesPackage.MEMORY_PARTITION__CONFIGURATION_PARAMETER_VALUE:
				getConfigurationParameterValue().clear();
				return;
			case Ucm_resourcesPackage.MEMORY_PARTITION__EXECUTION_RESOURCE:
				getExecutionResource().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_resourcesPackage.MEMORY_PARTITION__MEMORY_PARTITION_DEFINITION:
				return memoryPartitionDefinition != null;
			case Ucm_resourcesPackage.MEMORY_PARTITION__CONFIGURATION_PARAMETER_VALUE:
				return configurationParameterValue != null && !configurationParameterValue.isEmpty();
			case Ucm_resourcesPackage.MEMORY_PARTITION__EXECUTION_RESOURCE:
				return executionResource != null && !executionResource.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //MemoryPartitionImpl
