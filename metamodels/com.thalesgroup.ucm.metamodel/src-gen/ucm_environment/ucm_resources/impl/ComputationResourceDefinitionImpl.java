/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_environment.ucm_resources.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import ucm_environment.ucm_resources.ComputationResourceDefinition;
import ucm_environment.ucm_resources.ComputationResourcePort;
import ucm_environment.ucm_resources.Ucm_resourcesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Computation Resource Definition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_environment.ucm_resources.impl.ComputationResourceDefinitionImpl#getComputationPort <em>Computation Port</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.impl.ComputationResourceDefinitionImpl#getPerformanceFactor <em>Performance Factor</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComputationResourceDefinitionImpl extends IEnvironmentResourceDefinitionImpl implements ComputationResourceDefinition {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached value of the '{@link #getComputationPort() <em>Computation Port</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComputationPort()
	 * @generated
	 * @ordered
	 */
	protected EList<ComputationResourcePort> computationPort;

	/**
	 * The default value of the '{@link #getPerformanceFactor() <em>Performance Factor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPerformanceFactor()
	 * @generated
	 * @ordered
	 */
	protected static final double PERFORMANCE_FACTOR_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getPerformanceFactor() <em>Performance Factor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPerformanceFactor()
	 * @generated
	 * @ordered
	 */
	protected double performanceFactor = PERFORMANCE_FACTOR_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComputationResourceDefinitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_resourcesPackage.Literals.COMPUTATION_RESOURCE_DEFINITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComputationResourcePort> getComputationPort() {
		if (computationPort == null) {
			computationPort = new EObjectContainmentEList<ComputationResourcePort>(ComputationResourcePort.class, this, Ucm_resourcesPackage.COMPUTATION_RESOURCE_DEFINITION__COMPUTATION_PORT);
		}
		return computationPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getPerformanceFactor() {
		return performanceFactor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPerformanceFactor(double newPerformanceFactor) {
		double oldPerformanceFactor = performanceFactor;
		performanceFactor = newPerformanceFactor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_resourcesPackage.COMPUTATION_RESOURCE_DEFINITION__PERFORMANCE_FACTOR, oldPerformanceFactor, performanceFactor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Ucm_resourcesPackage.COMPUTATION_RESOURCE_DEFINITION__COMPUTATION_PORT:
				return ((InternalEList<?>)getComputationPort()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_resourcesPackage.COMPUTATION_RESOURCE_DEFINITION__COMPUTATION_PORT:
				return getComputationPort();
			case Ucm_resourcesPackage.COMPUTATION_RESOURCE_DEFINITION__PERFORMANCE_FACTOR:
				return getPerformanceFactor();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_resourcesPackage.COMPUTATION_RESOURCE_DEFINITION__COMPUTATION_PORT:
				getComputationPort().clear();
				getComputationPort().addAll((Collection<? extends ComputationResourcePort>)newValue);
				return;
			case Ucm_resourcesPackage.COMPUTATION_RESOURCE_DEFINITION__PERFORMANCE_FACTOR:
				setPerformanceFactor((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_resourcesPackage.COMPUTATION_RESOURCE_DEFINITION__COMPUTATION_PORT:
				getComputationPort().clear();
				return;
			case Ucm_resourcesPackage.COMPUTATION_RESOURCE_DEFINITION__PERFORMANCE_FACTOR:
				setPerformanceFactor(PERFORMANCE_FACTOR_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_resourcesPackage.COMPUTATION_RESOURCE_DEFINITION__COMPUTATION_PORT:
				return computationPort != null && !computationPort.isEmpty();
			case Ucm_resourcesPackage.COMPUTATION_RESOURCE_DEFINITION__PERFORMANCE_FACTOR:
				return performanceFactor != PERFORMANCE_FACTOR_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (performanceFactor: ");
		result.append(performanceFactor);
		result.append(')');
		return result.toString();
	}

} //ComputationResourceDefinitionImpl
