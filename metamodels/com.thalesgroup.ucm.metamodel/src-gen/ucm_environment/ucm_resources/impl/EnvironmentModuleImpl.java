/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_environment.ucm_resources.impl;

import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.emf.ecore.util.InternalEList;

import com.thalesgroup.ucm.metamodel.validation.UcmSupplementValidationFactory;

import ucm_base.ucm_commons.impl.IApplicationModuleImpl;
import ucm_environment.ucm_resources.CommunicationResource;
import ucm_environment.ucm_resources.ComputationResource;
import ucm_environment.ucm_resources.EnvironmentModule;
import ucm_environment.ucm_resources.Ucm_resourcesPackage;
import ucm_environment.ucm_resources.util.Ucm_resourcesValidator;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>Environment Module</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_environment.ucm_resources.impl.EnvironmentModuleImpl#getComputationResource <em>Computation Resource</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.impl.EnvironmentModuleImpl#getCommunicationResource <em>Communication Resource</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EnvironmentModuleImpl extends IApplicationModuleImpl implements EnvironmentModule {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached value of the '{@link #getComputationResource() <em>Computation Resource</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getComputationResource()
	 * @generated
	 * @ordered
	 */
	protected EList<ComputationResource> computationResource;

	/**
	 * The cached value of the '{@link #getCommunicationResource()
	 * <em>Communication Resource</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getCommunicationResource()
	 * @generated
	 * @ordered
	 */
	protected EList<CommunicationResource> communicationResource;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected EnvironmentModuleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_resourcesPackage.Literals.ENVIRONMENT_MODULE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComputationResource> getComputationResource() {
		if (computationResource == null) {
			computationResource = new EObjectContainmentEList<ComputationResource>(ComputationResource.class, this, Ucm_resourcesPackage.ENVIRONMENT_MODULE__COMPUTATION_RESOURCE);
		}
		return computationResource;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CommunicationResource> getCommunicationResource() {
		if (communicationResource == null) {
			communicationResource = new EObjectContainmentEList<CommunicationResource>(CommunicationResource.class, this, Ucm_resourcesPackage.ENVIRONMENT_MODULE__COMMUNICATION_RESOURCE);
		}
		return communicationResource;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean oneConnectionPerPort(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and
		// message
		// Ensure that you remove @generated or mark it @generated NOT
		return UcmSupplementValidationFactory.INSTANCE.getResourceValidator().oneConnectionPerPort(this, diagnostics,
				context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean initializationIsComplete(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 Ucm_resourcesValidator.DIAGNOSTIC_SOURCE,
						 Ucm_resourcesValidator.ENVIRONMENT_MODULE__INITIALIZATION_IS_COMPLETE,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "initializationIsComplete", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Ucm_resourcesPackage.ENVIRONMENT_MODULE__COMPUTATION_RESOURCE:
				return ((InternalEList<?>)getComputationResource()).basicRemove(otherEnd, msgs);
			case Ucm_resourcesPackage.ENVIRONMENT_MODULE__COMMUNICATION_RESOURCE:
				return ((InternalEList<?>)getCommunicationResource()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_resourcesPackage.ENVIRONMENT_MODULE__COMPUTATION_RESOURCE:
				return getComputationResource();
			case Ucm_resourcesPackage.ENVIRONMENT_MODULE__COMMUNICATION_RESOURCE:
				return getCommunicationResource();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_resourcesPackage.ENVIRONMENT_MODULE__COMPUTATION_RESOURCE:
				getComputationResource().clear();
				getComputationResource().addAll((Collection<? extends ComputationResource>)newValue);
				return;
			case Ucm_resourcesPackage.ENVIRONMENT_MODULE__COMMUNICATION_RESOURCE:
				getCommunicationResource().clear();
				getCommunicationResource().addAll((Collection<? extends CommunicationResource>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_resourcesPackage.ENVIRONMENT_MODULE__COMPUTATION_RESOURCE:
				getComputationResource().clear();
				return;
			case Ucm_resourcesPackage.ENVIRONMENT_MODULE__COMMUNICATION_RESOURCE:
				getCommunicationResource().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_resourcesPackage.ENVIRONMENT_MODULE__COMPUTATION_RESOURCE:
				return computationResource != null && !computationResource.isEmpty();
			case Ucm_resourcesPackage.ENVIRONMENT_MODULE__COMMUNICATION_RESOURCE:
				return communicationResource != null && !communicationResource.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} // EnvironmentModuleImpl
