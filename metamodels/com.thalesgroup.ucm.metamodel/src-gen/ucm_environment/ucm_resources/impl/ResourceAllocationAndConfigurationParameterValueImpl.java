/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_environment.ucm_resources.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import ucm_base.ucm_contracts.IValued;
import ucm_base.ucm_contracts.Ucm_contractsPackage;

import ucm_base.ucm_contracts.impl.IConfigurationParameterValueImpl;

import ucm_environment.ucm_resources.IEnvironmentResource;
import ucm_environment.ucm_resources.ResourceAllocationAndConfigurationParameter;
import ucm_environment.ucm_resources.ResourceAllocationAndConfigurationParameterValue;
import ucm_environment.ucm_resources.Ucm_resourcesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Resource Allocation And Configuration Parameter Value</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_environment.ucm_resources.impl.ResourceAllocationAndConfigurationParameterValueImpl#getValue <em>Value</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.impl.ResourceAllocationAndConfigurationParameterValueImpl#getParameterDefinition <em>Parameter Definition</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.impl.ResourceAllocationAndConfigurationParameterValueImpl#getResource <em>Resource</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ResourceAllocationAndConfigurationParameterValueImpl extends IConfigurationParameterValueImpl implements ResourceAllocationAndConfigurationParameterValue {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected String value = VALUE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getParameterDefinition() <em>Parameter Definition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameterDefinition()
	 * @generated
	 * @ordered
	 */
	protected ResourceAllocationAndConfigurationParameter parameterDefinition;

	/**
	 * The cached value of the '{@link #getResource() <em>Resource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResource()
	 * @generated
	 * @ordered
	 */
	protected IEnvironmentResource resource;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ResourceAllocationAndConfigurationParameterValueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_resourcesPackage.Literals.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER_VALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValue(String newValue) {
		String oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER_VALUE__VALUE, oldValue, value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceAllocationAndConfigurationParameter getParameterDefinition() {
		if (parameterDefinition != null && parameterDefinition.eIsProxy()) {
			InternalEObject oldParameterDefinition = (InternalEObject)parameterDefinition;
			parameterDefinition = (ResourceAllocationAndConfigurationParameter)eResolveProxy(oldParameterDefinition);
			if (parameterDefinition != oldParameterDefinition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER_VALUE__PARAMETER_DEFINITION, oldParameterDefinition, parameterDefinition));
			}
		}
		return parameterDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceAllocationAndConfigurationParameter basicGetParameterDefinition() {
		return parameterDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParameterDefinition(ResourceAllocationAndConfigurationParameter newParameterDefinition) {
		ResourceAllocationAndConfigurationParameter oldParameterDefinition = parameterDefinition;
		parameterDefinition = newParameterDefinition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER_VALUE__PARAMETER_DEFINITION, oldParameterDefinition, parameterDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IEnvironmentResource getResource() {
		if (resource != null && resource.eIsProxy()) {
			InternalEObject oldResource = (InternalEObject)resource;
			resource = (IEnvironmentResource)eResolveProxy(oldResource);
			if (resource != oldResource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER_VALUE__RESOURCE, oldResource, resource));
			}
		}
		return resource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IEnvironmentResource basicGetResource() {
		return resource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResource(IEnvironmentResource newResource) {
		IEnvironmentResource oldResource = resource;
		resource = newResource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER_VALUE__RESOURCE, oldResource, resource));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER_VALUE__VALUE:
				return getValue();
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER_VALUE__PARAMETER_DEFINITION:
				if (resolve) return getParameterDefinition();
				return basicGetParameterDefinition();
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER_VALUE__RESOURCE:
				if (resolve) return getResource();
				return basicGetResource();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER_VALUE__VALUE:
				setValue((String)newValue);
				return;
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER_VALUE__PARAMETER_DEFINITION:
				setParameterDefinition((ResourceAllocationAndConfigurationParameter)newValue);
				return;
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER_VALUE__RESOURCE:
				setResource((IEnvironmentResource)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER_VALUE__VALUE:
				setValue(VALUE_EDEFAULT);
				return;
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER_VALUE__PARAMETER_DEFINITION:
				setParameterDefinition((ResourceAllocationAndConfigurationParameter)null);
				return;
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER_VALUE__RESOURCE:
				setResource((IEnvironmentResource)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER_VALUE__VALUE:
				return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER_VALUE__PARAMETER_DEFINITION:
				return parameterDefinition != null;
			case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER_VALUE__RESOURCE:
				return resource != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == IValued.class) {
			switch (derivedFeatureID) {
				case Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER_VALUE__VALUE: return Ucm_contractsPackage.IVALUED__VALUE;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == IValued.class) {
			switch (baseFeatureID) {
				case Ucm_contractsPackage.IVALUED__VALUE: return Ucm_resourcesPackage.RESOURCE_ALLOCATION_AND_CONFIGURATION_PARAMETER_VALUE__VALUE;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (value: ");
		result.append(value);
		result.append(')');
		return result.toString();
	}

} //ResourceAllocationAndConfigurationParameterValueImpl
