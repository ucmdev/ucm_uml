/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_environment.ucm_resources;

import ucm_base.ucm_commons.INamed;
import ucm_base.ucm_contracts.IConfigurable;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Computation Resource Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_environment.ucm_resources.ComputationResourcePort#getDefinition <em>Definition</em>}</li>
 * </ul>
 *
 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getComputationResourcePort()
 * @model
 * @generated
 */
public interface ComputationResourcePort extends IConfigurable, INamed {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Definition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Definition</em>' reference.
	 * @see #setDefinition(CommunicationPortDefinition)
	 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getComputationResourcePort_Definition()
	 * @model required="true"
	 * @generated
	 */
	CommunicationPortDefinition getDefinition();

	/**
	 * Sets the value of the '{@link ucm_environment.ucm_resources.ComputationResourcePort#getDefinition <em>Definition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Definition</em>' reference.
	 * @see #getDefinition()
	 * @generated
	 */
	void setDefinition(CommunicationPortDefinition value);

} // ComputationResourcePort
