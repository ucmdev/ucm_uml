/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_environment.ucm_resources;

import java.util.Map;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import ucm_base.ucm_commons.IApplicationModule;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Environment Module</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_environment.ucm_resources.EnvironmentModule#getComputationResource <em>Computation Resource</em>}</li>
 *   <li>{@link ucm_environment.ucm_resources.EnvironmentModule#getCommunicationResource <em>Communication Resource</em>}</li>
 * </ul>
 *
 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getEnvironmentModule()
 * @model
 * @generated
 */
public interface EnvironmentModule extends IApplicationModule {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Computation Resource</b></em>' containment reference list.
	 * The list contents are of type {@link ucm_environment.ucm_resources.ComputationResource}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Computation Resource</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Computation Resource</em>' containment reference list.
	 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getEnvironmentModule_ComputationResource()
	 * @model containment="true"
	 * @generated
	 */
	EList<ComputationResource> getComputationResource();

	/**
	 * Returns the value of the '<em><b>Communication Resource</b></em>' containment reference list.
	 * The list contents are of type {@link ucm_environment.ucm_resources.CommunicationResource}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Communication Resource</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Communication Resource</em>' containment reference list.
	 * @see ucm_environment.ucm_resources.Ucm_resourcesPackage#getEnvironmentModule_CommunicationResource()
	 * @model containment="true"
	 * @generated
	 */
	EList<CommunicationResource> getCommunicationResource();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean oneConnectionPerPort(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean initializationIsComplete(DiagnosticChain diagnostics, Map<Object, Object> context);

} // EnvironmentModule
