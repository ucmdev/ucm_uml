/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_extended_projects;

import ucm_environment.ucm_resources.EnvironmentModule;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>XEnvironment Module</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ucm_extended_projects.Ucm_extended_projectsPackage#getXEnvironmentModule()
 * @model abstract="true"
 * @generated
 */
public interface XEnvironmentModule extends EnvironmentModule {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

} // XEnvironmentModule
