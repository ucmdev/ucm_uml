/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_extended_projects;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import ucm_basic_projects.Ucm_basic_projectsPackage;

import ucm_environment.ucm_resources.Ucm_resourcesPackage;

import ucm_supplement.ucm_deployments.Ucm_deploymentsPackage;

import ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ucm_extended_projects.Ucm_extended_projectsFactory
 * @model kind="package"
 * @generated
 */
public interface Ucm_extended_projectsPackage extends EPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ucm_extended_projects";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://com.thalesgroup.ucm/1.0/metamodels/projects";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ucm_extended_projects";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Ucm_extended_projectsPackage eINSTANCE = ucm_extended_projects.impl.Ucm_extended_projectsPackageImpl.init();

	/**
	 * The meta object id for the '{@link ucm_extended_projects.impl.ExtendedProjectImpl <em>Extended Project</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_extended_projects.impl.ExtendedProjectImpl
	 * @see ucm_extended_projects.impl.Ucm_extended_projectsPackageImpl#getExtendedProject()
	 * @generated
	 */
	int EXTENDED_PROJECT = 0;

	/**
	 * The feature id for the '<em><b>Module</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENDED_PROJECT__MODULE = Ucm_basic_projectsPackage.BASIC_PROJECT__MODULE;

	/**
	 * The number of structural features of the '<em>Extended Project</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENDED_PROJECT_FEATURE_COUNT = Ucm_basic_projectsPackage.BASIC_PROJECT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link ucm_extended_projects.impl.XDetailedComponentImplementationImpl <em>XDetailed Component Implementation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_extended_projects.impl.XDetailedComponentImplementationImpl
	 * @see ucm_extended_projects.impl.Ucm_extended_projectsPackageImpl#getXDetailedComponentImplementation()
	 * @generated
	 */
	int XDETAILED_COMPONENT_IMPLEMENTATION = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XDETAILED_COMPONENT_IMPLEMENTATION__NAME = Ucm_detailed_componentsPackage.DETAILED_COMPONENT_IMPLEMENTATION__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XDETAILED_COMPONENT_IMPLEMENTATION__COMMENT = Ucm_detailed_componentsPackage.DETAILED_COMPONENT_IMPLEMENTATION__COMMENT;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XDETAILED_COMPONENT_IMPLEMENTATION__ANNOTATION = Ucm_detailed_componentsPackage.DETAILED_COMPONENT_IMPLEMENTATION__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XDETAILED_COMPONENT_IMPLEMENTATION__TYPE = Ucm_detailed_componentsPackage.DETAILED_COMPONENT_IMPLEMENTATION__TYPE;

	/**
	 * The feature id for the '<em><b>Programming Language</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XDETAILED_COMPONENT_IMPLEMENTATION__PROGRAMMING_LANGUAGE = Ucm_detailed_componentsPackage.DETAILED_COMPONENT_IMPLEMENTATION__PROGRAMMING_LANGUAGE;

	/**
	 * The feature id for the '<em><b>Policy</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XDETAILED_COMPONENT_IMPLEMENTATION__POLICY = Ucm_detailed_componentsPackage.DETAILED_COMPONENT_IMPLEMENTATION__POLICY;

	/**
	 * The feature id for the '<em><b>Port Implementation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XDETAILED_COMPONENT_IMPLEMENTATION__PORT_IMPLEMENTATION = Ucm_detailed_componentsPackage.DETAILED_COMPONENT_IMPLEMENTATION__PORT_IMPLEMENTATION;

	/**
	 * The feature id for the '<em><b>Execution Scenario</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XDETAILED_COMPONENT_IMPLEMENTATION__EXECUTION_SCENARIO = Ucm_detailed_componentsPackage.DETAILED_COMPONENT_IMPLEMENTATION__EXECUTION_SCENARIO;

	/**
	 * The feature id for the '<em><b>Extends</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XDETAILED_COMPONENT_IMPLEMENTATION__EXTENDS = Ucm_detailed_componentsPackage.DETAILED_COMPONENT_IMPLEMENTATION__EXTENDS;

	/**
	 * The number of structural features of the '<em>XDetailed Component Implementation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XDETAILED_COMPONENT_IMPLEMENTATION_FEATURE_COUNT = Ucm_detailed_componentsPackage.DETAILED_COMPONENT_IMPLEMENTATION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link ucm_extended_projects.impl.XDeploymentModuleImpl <em>XDeployment Module</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_extended_projects.impl.XDeploymentModuleImpl
	 * @see ucm_extended_projects.impl.Ucm_extended_projectsPackageImpl#getXDeploymentModule()
	 * @generated
	 */
	int XDEPLOYMENT_MODULE = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XDEPLOYMENT_MODULE__NAME = Ucm_deploymentsPackage.DEPLOYMENT_MODULE__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XDEPLOYMENT_MODULE__COMMENT = Ucm_deploymentsPackage.DEPLOYMENT_MODULE__COMMENT;

	/**
	 * The feature id for the '<em><b>Assembly</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XDEPLOYMENT_MODULE__ASSEMBLY = Ucm_deploymentsPackage.DEPLOYMENT_MODULE__ASSEMBLY;

	/**
	 * The feature id for the '<em><b>Allocation Plan</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XDEPLOYMENT_MODULE__ALLOCATION_PLAN = Ucm_deploymentsPackage.DEPLOYMENT_MODULE__ALLOCATION_PLAN;

	/**
	 * The feature id for the '<em><b>Artefact</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XDEPLOYMENT_MODULE__ARTEFACT = Ucm_deploymentsPackage.DEPLOYMENT_MODULE__ARTEFACT;

	/**
	 * The feature id for the '<em><b>Test Case Group</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XDEPLOYMENT_MODULE__TEST_CASE_GROUP = Ucm_deploymentsPackage.DEPLOYMENT_MODULE__TEST_CASE_GROUP;

	/**
	 * The feature id for the '<em><b>Component Instance</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XDEPLOYMENT_MODULE__COMPONENT_INSTANCE = Ucm_deploymentsPackage.DEPLOYMENT_MODULE__COMPONENT_INSTANCE;

	/**
	 * The feature id for the '<em><b>Connection Instance</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XDEPLOYMENT_MODULE__CONNECTION_INSTANCE = Ucm_deploymentsPackage.DEPLOYMENT_MODULE__CONNECTION_INSTANCE;

	/**
	 * The number of structural features of the '<em>XDeployment Module</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XDEPLOYMENT_MODULE_FEATURE_COUNT = Ucm_deploymentsPackage.DEPLOYMENT_MODULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link ucm_extended_projects.impl.XEnvironmentModuleImpl <em>XEnvironment Module</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_extended_projects.impl.XEnvironmentModuleImpl
	 * @see ucm_extended_projects.impl.Ucm_extended_projectsPackageImpl#getXEnvironmentModule()
	 * @generated
	 */
	int XENVIRONMENT_MODULE = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XENVIRONMENT_MODULE__NAME = Ucm_resourcesPackage.ENVIRONMENT_MODULE__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XENVIRONMENT_MODULE__COMMENT = Ucm_resourcesPackage.ENVIRONMENT_MODULE__COMMENT;

	/**
	 * The feature id for the '<em><b>Computation Resource</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XENVIRONMENT_MODULE__COMPUTATION_RESOURCE = Ucm_resourcesPackage.ENVIRONMENT_MODULE__COMPUTATION_RESOURCE;

	/**
	 * The feature id for the '<em><b>Communication Resource</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XENVIRONMENT_MODULE__COMMUNICATION_RESOURCE = Ucm_resourcesPackage.ENVIRONMENT_MODULE__COMMUNICATION_RESOURCE;

	/**
	 * The number of structural features of the '<em>XEnvironment Module</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XENVIRONMENT_MODULE_FEATURE_COUNT = Ucm_resourcesPackage.ENVIRONMENT_MODULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link ucm_extended_projects.impl.XResourceDefinitionModuleImpl <em>XResource Definition Module</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_extended_projects.impl.XResourceDefinitionModuleImpl
	 * @see ucm_extended_projects.impl.Ucm_extended_projectsPackageImpl#getXResourceDefinitionModule()
	 * @generated
	 */
	int XRESOURCE_DEFINITION_MODULE = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XRESOURCE_DEFINITION_MODULE__NAME = Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XRESOURCE_DEFINITION_MODULE__COMMENT = Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__COMMENT;

	/**
	 * The feature id for the '<em><b>Computation Resource Definition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XRESOURCE_DEFINITION_MODULE__COMPUTATION_RESOURCE_DEFINITION = Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__COMPUTATION_RESOURCE_DEFINITION;

	/**
	 * The feature id for the '<em><b>Communication Resource Definition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XRESOURCE_DEFINITION_MODULE__COMMUNICATION_RESOURCE_DEFINITION = Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__COMMUNICATION_RESOURCE_DEFINITION;

	/**
	 * The feature id for the '<em><b>Execution Resource Definition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XRESOURCE_DEFINITION_MODULE__EXECUTION_RESOURCE_DEFINITION = Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__EXECUTION_RESOURCE_DEFINITION;

	/**
	 * The feature id for the '<em><b>Memory Partition Definition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XRESOURCE_DEFINITION_MODULE__MEMORY_PARTITION_DEFINITION = Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__MEMORY_PARTITION_DEFINITION;

	/**
	 * The feature id for the '<em><b>Contract Module</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XRESOURCE_DEFINITION_MODULE__CONTRACT_MODULE = Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__CONTRACT_MODULE;

	/**
	 * The feature id for the '<em><b>Communication Port Definition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XRESOURCE_DEFINITION_MODULE__COMMUNICATION_PORT_DEFINITION = Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE__COMMUNICATION_PORT_DEFINITION;

	/**
	 * The number of structural features of the '<em>XResource Definition Module</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XRESOURCE_DEFINITION_MODULE_FEATURE_COUNT = Ucm_resourcesPackage.RESOURCE_DEFINITION_MODULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link ucm_extended_projects.impl.XMemoryPartitionImpl <em>XMemory Partition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_extended_projects.impl.XMemoryPartitionImpl
	 * @see ucm_extended_projects.impl.Ucm_extended_projectsPackageImpl#getXMemoryPartition()
	 * @generated
	 */
	int XMEMORY_PARTITION = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XMEMORY_PARTITION__NAME = Ucm_resourcesPackage.MEMORY_PARTITION__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XMEMORY_PARTITION__COMMENT = Ucm_resourcesPackage.MEMORY_PARTITION__COMMENT;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XMEMORY_PARTITION__ANNOTATION = Ucm_resourcesPackage.MEMORY_PARTITION__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Language</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XMEMORY_PARTITION__LANGUAGE = Ucm_resourcesPackage.MEMORY_PARTITION__LANGUAGE;

	/**
	 * The feature id for the '<em><b>Component Instance</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XMEMORY_PARTITION__COMPONENT_INSTANCE = Ucm_resourcesPackage.MEMORY_PARTITION__COMPONENT_INSTANCE;

	/**
	 * The feature id for the '<em><b>Memory Partition Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XMEMORY_PARTITION__MEMORY_PARTITION_DEFINITION = Ucm_resourcesPackage.MEMORY_PARTITION__MEMORY_PARTITION_DEFINITION;

	/**
	 * The feature id for the '<em><b>Configuration Parameter Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XMEMORY_PARTITION__CONFIGURATION_PARAMETER_VALUE = Ucm_resourcesPackage.MEMORY_PARTITION__CONFIGURATION_PARAMETER_VALUE;

	/**
	 * The feature id for the '<em><b>Execution Resource</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XMEMORY_PARTITION__EXECUTION_RESOURCE = Ucm_resourcesPackage.MEMORY_PARTITION__EXECUTION_RESOURCE;

	/**
	 * The number of structural features of the '<em>XMemory Partition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XMEMORY_PARTITION_FEATURE_COUNT = Ucm_resourcesPackage.MEMORY_PARTITION_FEATURE_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link ucm_extended_projects.ExtendedProject <em>Extended Project</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Extended Project</em>'.
	 * @see ucm_extended_projects.ExtendedProject
	 * @generated
	 */
	EClass getExtendedProject();

	/**
	 * Returns the meta object for class '{@link ucm_extended_projects.XDetailedComponentImplementation <em>XDetailed Component Implementation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XDetailed Component Implementation</em>'.
	 * @see ucm_extended_projects.XDetailedComponentImplementation
	 * @generated
	 */
	EClass getXDetailedComponentImplementation();

	/**
	 * Returns the meta object for class '{@link ucm_extended_projects.XDeploymentModule <em>XDeployment Module</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XDeployment Module</em>'.
	 * @see ucm_extended_projects.XDeploymentModule
	 * @generated
	 */
	EClass getXDeploymentModule();

	/**
	 * Returns the meta object for class '{@link ucm_extended_projects.XEnvironmentModule <em>XEnvironment Module</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XEnvironment Module</em>'.
	 * @see ucm_extended_projects.XEnvironmentModule
	 * @generated
	 */
	EClass getXEnvironmentModule();

	/**
	 * Returns the meta object for class '{@link ucm_extended_projects.XResourceDefinitionModule <em>XResource Definition Module</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XResource Definition Module</em>'.
	 * @see ucm_extended_projects.XResourceDefinitionModule
	 * @generated
	 */
	EClass getXResourceDefinitionModule();

	/**
	 * Returns the meta object for class '{@link ucm_extended_projects.XMemoryPartition <em>XMemory Partition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XMemory Partition</em>'.
	 * @see ucm_extended_projects.XMemoryPartition
	 * @generated
	 */
	EClass getXMemoryPartition();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	Ucm_extended_projectsFactory getUcm_extended_projectsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ucm_extended_projects.impl.ExtendedProjectImpl <em>Extended Project</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_extended_projects.impl.ExtendedProjectImpl
		 * @see ucm_extended_projects.impl.Ucm_extended_projectsPackageImpl#getExtendedProject()
		 * @generated
		 */
		EClass EXTENDED_PROJECT = eINSTANCE.getExtendedProject();

		/**
		 * The meta object literal for the '{@link ucm_extended_projects.impl.XDetailedComponentImplementationImpl <em>XDetailed Component Implementation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_extended_projects.impl.XDetailedComponentImplementationImpl
		 * @see ucm_extended_projects.impl.Ucm_extended_projectsPackageImpl#getXDetailedComponentImplementation()
		 * @generated
		 */
		EClass XDETAILED_COMPONENT_IMPLEMENTATION = eINSTANCE.getXDetailedComponentImplementation();

		/**
		 * The meta object literal for the '{@link ucm_extended_projects.impl.XDeploymentModuleImpl <em>XDeployment Module</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_extended_projects.impl.XDeploymentModuleImpl
		 * @see ucm_extended_projects.impl.Ucm_extended_projectsPackageImpl#getXDeploymentModule()
		 * @generated
		 */
		EClass XDEPLOYMENT_MODULE = eINSTANCE.getXDeploymentModule();

		/**
		 * The meta object literal for the '{@link ucm_extended_projects.impl.XEnvironmentModuleImpl <em>XEnvironment Module</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_extended_projects.impl.XEnvironmentModuleImpl
		 * @see ucm_extended_projects.impl.Ucm_extended_projectsPackageImpl#getXEnvironmentModule()
		 * @generated
		 */
		EClass XENVIRONMENT_MODULE = eINSTANCE.getXEnvironmentModule();

		/**
		 * The meta object literal for the '{@link ucm_extended_projects.impl.XResourceDefinitionModuleImpl <em>XResource Definition Module</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_extended_projects.impl.XResourceDefinitionModuleImpl
		 * @see ucm_extended_projects.impl.Ucm_extended_projectsPackageImpl#getXResourceDefinitionModule()
		 * @generated
		 */
		EClass XRESOURCE_DEFINITION_MODULE = eINSTANCE.getXResourceDefinitionModule();

		/**
		 * The meta object literal for the '{@link ucm_extended_projects.impl.XMemoryPartitionImpl <em>XMemory Partition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_extended_projects.impl.XMemoryPartitionImpl
		 * @see ucm_extended_projects.impl.Ucm_extended_projectsPackageImpl#getXMemoryPartition()
		 * @generated
		 */
		EClass XMEMORY_PARTITION = eINSTANCE.getXMemoryPartition();

	}

} //Ucm_extended_projectsPackage
