/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_extended_projects;

import ucm_supplement.ucm_detailed_components.DetailedComponentImplementation;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>XDetailed Component Implementation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ucm_extended_projects.Ucm_extended_projectsPackage#getXDetailedComponentImplementation()
 * @model abstract="true"
 * @generated
 */
public interface XDetailedComponentImplementation extends DetailedComponentImplementation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

} // XDetailedComponentImplementation
