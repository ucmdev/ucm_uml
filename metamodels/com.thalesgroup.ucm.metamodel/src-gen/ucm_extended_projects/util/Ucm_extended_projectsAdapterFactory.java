/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_extended_projects.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import ucm_base.ucm_commons.IApplicationModule;
import ucm_base.ucm_commons.IModule;
import ucm_base.ucm_commons.INamed;
import ucm_base.ucm_commons.IPlatformModule;

import ucm_base.ucm_components.AtomicComponentImplementation;
import ucm_base.ucm_components.IComponent;
import ucm_base.ucm_components.IComponentImplementation;

import ucm_base.ucm_contracts.IAnnotable;

import ucm_basic_projects.BasicProject;
import ucm_basic_projects.IProject;

import ucm_environment.ucm_resources.EnvironmentModule;
import ucm_environment.ucm_resources.MemoryPartition;
import ucm_environment.ucm_resources.ResourceDefinitionModule;

import ucm_extended_projects.*;

import ucm_supplement.ucm_deployments.DeploymentModule;
import ucm_supplement.ucm_deployments.IComponentNode;

import ucm_supplement.ucm_detailed_components.DetailedComponentImplementation;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see ucm_extended_projects.Ucm_extended_projectsPackage
 * @generated
 */
public class Ucm_extended_projectsAdapterFactory extends AdapterFactoryImpl {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static Ucm_extended_projectsPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ucm_extended_projectsAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = Ucm_extended_projectsPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Ucm_extended_projectsSwitch<Adapter> modelSwitch =
		new Ucm_extended_projectsSwitch<Adapter>() {
			@Override
			public Adapter caseExtendedProject(ExtendedProject object) {
				return createExtendedProjectAdapter();
			}
			@Override
			public Adapter caseXDetailedComponentImplementation(XDetailedComponentImplementation object) {
				return createXDetailedComponentImplementationAdapter();
			}
			@Override
			public Adapter caseXDeploymentModule(XDeploymentModule object) {
				return createXDeploymentModuleAdapter();
			}
			@Override
			public Adapter caseXEnvironmentModule(XEnvironmentModule object) {
				return createXEnvironmentModuleAdapter();
			}
			@Override
			public Adapter caseXResourceDefinitionModule(XResourceDefinitionModule object) {
				return createXResourceDefinitionModuleAdapter();
			}
			@Override
			public Adapter caseXMemoryPartition(XMemoryPartition object) {
				return createXMemoryPartitionAdapter();
			}
			@Override
			public Adapter caseIProject(IProject object) {
				return createIProjectAdapter();
			}
			@Override
			public Adapter caseBasicProject(BasicProject object) {
				return createBasicProjectAdapter();
			}
			@Override
			public Adapter caseINamed(INamed object) {
				return createINamedAdapter();
			}
			@Override
			public Adapter caseIAnnotable(IAnnotable object) {
				return createIAnnotableAdapter();
			}
			@Override
			public Adapter caseIComponent(IComponent object) {
				return createIComponentAdapter();
			}
			@Override
			public Adapter caseIComponentImplementation(IComponentImplementation object) {
				return createIComponentImplementationAdapter();
			}
			@Override
			public Adapter caseAtomicComponentImplementation(AtomicComponentImplementation object) {
				return createAtomicComponentImplementationAdapter();
			}
			@Override
			public Adapter caseDetailedComponentImplementation(DetailedComponentImplementation object) {
				return createDetailedComponentImplementationAdapter();
			}
			@Override
			public Adapter caseIModule(IModule object) {
				return createIModuleAdapter();
			}
			@Override
			public Adapter caseIApplicationModule(IApplicationModule object) {
				return createIApplicationModuleAdapter();
			}
			@Override
			public Adapter caseDeploymentModule(DeploymentModule object) {
				return createDeploymentModuleAdapter();
			}
			@Override
			public Adapter caseEnvironmentModule(EnvironmentModule object) {
				return createEnvironmentModuleAdapter();
			}
			@Override
			public Adapter caseIPlatformModule(IPlatformModule object) {
				return createIPlatformModuleAdapter();
			}
			@Override
			public Adapter caseResourceDefinitionModule(ResourceDefinitionModule object) {
				return createResourceDefinitionModuleAdapter();
			}
			@Override
			public Adapter caseIComponentNode(IComponentNode object) {
				return createIComponentNodeAdapter();
			}
			@Override
			public Adapter caseMemoryPartition(MemoryPartition object) {
				return createMemoryPartitionAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link ucm_extended_projects.ExtendedProject <em>Extended Project</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_extended_projects.ExtendedProject
	 * @generated
	 */
	public Adapter createExtendedProjectAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_extended_projects.XDetailedComponentImplementation <em>XDetailed Component Implementation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_extended_projects.XDetailedComponentImplementation
	 * @generated
	 */
	public Adapter createXDetailedComponentImplementationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_extended_projects.XDeploymentModule <em>XDeployment Module</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_extended_projects.XDeploymentModule
	 * @generated
	 */
	public Adapter createXDeploymentModuleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_extended_projects.XEnvironmentModule <em>XEnvironment Module</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_extended_projects.XEnvironmentModule
	 * @generated
	 */
	public Adapter createXEnvironmentModuleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_extended_projects.XResourceDefinitionModule <em>XResource Definition Module</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_extended_projects.XResourceDefinitionModule
	 * @generated
	 */
	public Adapter createXResourceDefinitionModuleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_extended_projects.XMemoryPartition <em>XMemory Partition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_extended_projects.XMemoryPartition
	 * @generated
	 */
	public Adapter createXMemoryPartitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_basic_projects.IProject <em>IProject</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_basic_projects.IProject
	 * @generated
	 */
	public Adapter createIProjectAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_basic_projects.BasicProject <em>Basic Project</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_basic_projects.BasicProject
	 * @generated
	 */
	public Adapter createBasicProjectAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_base.ucm_commons.INamed <em>INamed</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_base.ucm_commons.INamed
	 * @generated
	 */
	public Adapter createINamedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_base.ucm_contracts.IAnnotable <em>IAnnotable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_base.ucm_contracts.IAnnotable
	 * @generated
	 */
	public Adapter createIAnnotableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_base.ucm_components.IComponent <em>IComponent</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_base.ucm_components.IComponent
	 * @generated
	 */
	public Adapter createIComponentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_base.ucm_components.IComponentImplementation <em>IComponent Implementation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_base.ucm_components.IComponentImplementation
	 * @generated
	 */
	public Adapter createIComponentImplementationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_base.ucm_components.AtomicComponentImplementation <em>Atomic Component Implementation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_base.ucm_components.AtomicComponentImplementation
	 * @generated
	 */
	public Adapter createAtomicComponentImplementationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_supplement.ucm_detailed_components.DetailedComponentImplementation <em>Detailed Component Implementation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_supplement.ucm_detailed_components.DetailedComponentImplementation
	 * @generated
	 */
	public Adapter createDetailedComponentImplementationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_base.ucm_commons.IModule <em>IModule</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_base.ucm_commons.IModule
	 * @generated
	 */
	public Adapter createIModuleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_base.ucm_commons.IApplicationModule <em>IApplication Module</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_base.ucm_commons.IApplicationModule
	 * @generated
	 */
	public Adapter createIApplicationModuleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_supplement.ucm_deployments.DeploymentModule <em>Deployment Module</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_supplement.ucm_deployments.DeploymentModule
	 * @generated
	 */
	public Adapter createDeploymentModuleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_environment.ucm_resources.EnvironmentModule <em>Environment Module</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_environment.ucm_resources.EnvironmentModule
	 * @generated
	 */
	public Adapter createEnvironmentModuleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_base.ucm_commons.IPlatformModule <em>IPlatform Module</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_base.ucm_commons.IPlatformModule
	 * @generated
	 */
	public Adapter createIPlatformModuleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_environment.ucm_resources.ResourceDefinitionModule <em>Resource Definition Module</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_environment.ucm_resources.ResourceDefinitionModule
	 * @generated
	 */
	public Adapter createResourceDefinitionModuleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_supplement.ucm_deployments.IComponentNode <em>IComponent Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_supplement.ucm_deployments.IComponentNode
	 * @generated
	 */
	public Adapter createIComponentNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_environment.ucm_resources.MemoryPartition <em>Memory Partition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_environment.ucm_resources.MemoryPartition
	 * @generated
	 */
	public Adapter createMemoryPartitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //Ucm_extended_projectsAdapterFactory
