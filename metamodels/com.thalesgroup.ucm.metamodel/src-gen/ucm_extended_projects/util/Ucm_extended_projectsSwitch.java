/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_extended_projects.util;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import ucm_base.ucm_commons.IApplicationModule;
import ucm_base.ucm_commons.IModule;
import ucm_base.ucm_commons.INamed;
import ucm_base.ucm_commons.IPlatformModule;

import ucm_base.ucm_components.AtomicComponentImplementation;
import ucm_base.ucm_components.IComponent;
import ucm_base.ucm_components.IComponentImplementation;

import ucm_base.ucm_contracts.IAnnotable;

import ucm_basic_projects.BasicProject;
import ucm_basic_projects.IProject;

import ucm_environment.ucm_resources.EnvironmentModule;
import ucm_environment.ucm_resources.MemoryPartition;
import ucm_environment.ucm_resources.ResourceDefinitionModule;

import ucm_extended_projects.*;

import ucm_supplement.ucm_deployments.DeploymentModule;
import ucm_supplement.ucm_deployments.IComponentNode;

import ucm_supplement.ucm_detailed_components.DetailedComponentImplementation;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see ucm_extended_projects.Ucm_extended_projectsPackage
 * @generated
 */
public class Ucm_extended_projectsSwitch<T> {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static Ucm_extended_projectsPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ucm_extended_projectsSwitch() {
		if (modelPackage == null) {
			modelPackage = Ucm_extended_projectsPackage.eINSTANCE;
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	public T doSwitch(EObject theEObject) {
		return doSwitch(theEObject.eClass(), theEObject);
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(EClass theEClass, EObject theEObject) {
		if (theEClass.eContainer() == modelPackage) {
			return doSwitch(theEClass.getClassifierID(), theEObject);
		}
		else {
			List<EClass> eSuperTypes = theEClass.getESuperTypes();
			return
				eSuperTypes.isEmpty() ?
					defaultCase(theEObject) :
					doSwitch(eSuperTypes.get(0), theEObject);
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case Ucm_extended_projectsPackage.EXTENDED_PROJECT: {
				ExtendedProject extendedProject = (ExtendedProject)theEObject;
				T result = caseExtendedProject(extendedProject);
				if (result == null) result = caseBasicProject(extendedProject);
				if (result == null) result = caseIProject(extendedProject);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_extended_projectsPackage.XDETAILED_COMPONENT_IMPLEMENTATION: {
				XDetailedComponentImplementation xDetailedComponentImplementation = (XDetailedComponentImplementation)theEObject;
				T result = caseXDetailedComponentImplementation(xDetailedComponentImplementation);
				if (result == null) result = caseDetailedComponentImplementation(xDetailedComponentImplementation);
				if (result == null) result = caseAtomicComponentImplementation(xDetailedComponentImplementation);
				if (result == null) result = caseIComponentImplementation(xDetailedComponentImplementation);
				if (result == null) result = caseIComponent(xDetailedComponentImplementation);
				if (result == null) result = caseINamed(xDetailedComponentImplementation);
				if (result == null) result = caseIAnnotable(xDetailedComponentImplementation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_extended_projectsPackage.XDEPLOYMENT_MODULE: {
				XDeploymentModule xDeploymentModule = (XDeploymentModule)theEObject;
				T result = caseXDeploymentModule(xDeploymentModule);
				if (result == null) result = caseDeploymentModule(xDeploymentModule);
				if (result == null) result = caseIApplicationModule(xDeploymentModule);
				if (result == null) result = caseIModule(xDeploymentModule);
				if (result == null) result = caseINamed(xDeploymentModule);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_extended_projectsPackage.XENVIRONMENT_MODULE: {
				XEnvironmentModule xEnvironmentModule = (XEnvironmentModule)theEObject;
				T result = caseXEnvironmentModule(xEnvironmentModule);
				if (result == null) result = caseEnvironmentModule(xEnvironmentModule);
				if (result == null) result = caseIApplicationModule(xEnvironmentModule);
				if (result == null) result = caseIModule(xEnvironmentModule);
				if (result == null) result = caseINamed(xEnvironmentModule);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_extended_projectsPackage.XRESOURCE_DEFINITION_MODULE: {
				XResourceDefinitionModule xResourceDefinitionModule = (XResourceDefinitionModule)theEObject;
				T result = caseXResourceDefinitionModule(xResourceDefinitionModule);
				if (result == null) result = caseResourceDefinitionModule(xResourceDefinitionModule);
				if (result == null) result = caseIPlatformModule(xResourceDefinitionModule);
				if (result == null) result = caseIModule(xResourceDefinitionModule);
				if (result == null) result = caseINamed(xResourceDefinitionModule);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_extended_projectsPackage.XMEMORY_PARTITION: {
				XMemoryPartition xMemoryPartition = (XMemoryPartition)theEObject;
				T result = caseXMemoryPartition(xMemoryPartition);
				if (result == null) result = caseMemoryPartition(xMemoryPartition);
				if (result == null) result = caseIComponentNode(xMemoryPartition);
				if (result == null) result = caseINamed(xMemoryPartition);
				if (result == null) result = caseIAnnotable(xMemoryPartition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Extended Project</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Extended Project</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExtendedProject(ExtendedProject object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XDetailed Component Implementation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XDetailed Component Implementation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXDetailedComponentImplementation(XDetailedComponentImplementation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XDeployment Module</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XDeployment Module</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXDeploymentModule(XDeploymentModule object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XEnvironment Module</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XEnvironment Module</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXEnvironmentModule(XEnvironmentModule object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XResource Definition Module</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XResource Definition Module</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXResourceDefinitionModule(XResourceDefinitionModule object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XMemory Partition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XMemory Partition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXMemoryPartition(XMemoryPartition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IProject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IProject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIProject(IProject object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Basic Project</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Basic Project</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBasicProject(BasicProject object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>INamed</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>INamed</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseINamed(INamed object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IAnnotable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IAnnotable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIAnnotable(IAnnotable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IComponent</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IComponent</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIComponent(IComponent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IComponent Implementation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IComponent Implementation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIComponentImplementation(IComponentImplementation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Atomic Component Implementation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Atomic Component Implementation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAtomicComponentImplementation(AtomicComponentImplementation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Detailed Component Implementation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Detailed Component Implementation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDetailedComponentImplementation(DetailedComponentImplementation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IModule</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IModule</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIModule(IModule object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IApplication Module</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IApplication Module</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIApplicationModule(IApplicationModule object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Deployment Module</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Deployment Module</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeploymentModule(DeploymentModule object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Environment Module</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Environment Module</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEnvironmentModule(EnvironmentModule object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IPlatform Module</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IPlatform Module</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIPlatformModule(IPlatformModule object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Resource Definition Module</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Resource Definition Module</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResourceDefinitionModule(ResourceDefinitionModule object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IComponent Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IComponent Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIComponentNode(IComponentNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Memory Partition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Memory Partition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMemoryPartition(MemoryPartition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public T defaultCase(EObject object) {
		return null;
	}

} //Ucm_extended_projectsSwitch
