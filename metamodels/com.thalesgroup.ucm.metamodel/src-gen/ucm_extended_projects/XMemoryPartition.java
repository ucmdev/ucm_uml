/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_extended_projects;

import ucm_environment.ucm_resources.MemoryPartition;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>XMemory Partition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ucm_extended_projects.Ucm_extended_projectsPackage#getXMemoryPartition()
 * @model abstract="true"
 * @generated
 */
public interface XMemoryPartition extends MemoryPartition {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

} // XMemoryPartition
