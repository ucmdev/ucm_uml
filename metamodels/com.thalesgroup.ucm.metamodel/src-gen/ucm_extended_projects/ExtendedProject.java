/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_extended_projects;

import ucm_basic_projects.BasicProject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Extended Project</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ucm_extended_projects.Ucm_extended_projectsPackage#getExtendedProject()
 * @model
 * @generated
 */
public interface ExtendedProject extends BasicProject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

} // ExtendedProject
