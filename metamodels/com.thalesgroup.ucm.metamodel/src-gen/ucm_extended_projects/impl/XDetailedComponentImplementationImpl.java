/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_extended_projects.impl;

import org.eclipse.emf.ecore.EClass;

import ucm_extended_projects.Ucm_extended_projectsPackage;
import ucm_extended_projects.XDetailedComponentImplementation;

import ucm_supplement.ucm_detailed_components.impl.DetailedComponentImplementationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>XDetailed Component Implementation</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class XDetailedComponentImplementationImpl extends DetailedComponentImplementationImpl implements XDetailedComponentImplementation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected XDetailedComponentImplementationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_extended_projectsPackage.Literals.XDETAILED_COMPONENT_IMPLEMENTATION;
	}

} //XDetailedComponentImplementationImpl
