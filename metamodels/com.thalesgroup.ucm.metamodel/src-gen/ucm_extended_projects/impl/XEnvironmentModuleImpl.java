/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_extended_projects.impl;

import org.eclipse.emf.ecore.EClass;

import ucm_environment.ucm_resources.impl.EnvironmentModuleImpl;

import ucm_extended_projects.Ucm_extended_projectsPackage;
import ucm_extended_projects.XEnvironmentModule;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>XEnvironment Module</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class XEnvironmentModuleImpl extends EnvironmentModuleImpl implements XEnvironmentModule {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected XEnvironmentModuleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_extended_projectsPackage.Literals.XENVIRONMENT_MODULE;
	}

} //XEnvironmentModuleImpl
