/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_extended_projects.impl;

import org.eclipse.emf.ecore.EClass;

import ucm_basic_projects.impl.BasicProjectImpl;

import ucm_extended_projects.ExtendedProject;
import ucm_extended_projects.Ucm_extended_projectsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Extended Project</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ExtendedProjectImpl extends BasicProjectImpl implements ExtendedProject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExtendedProjectImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_extended_projectsPackage.Literals.EXTENDED_PROJECT;
	}

} //ExtendedProjectImpl
