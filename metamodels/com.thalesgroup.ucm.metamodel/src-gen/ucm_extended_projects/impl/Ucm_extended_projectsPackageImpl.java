/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_extended_projects.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import ucm_base.ucm_commons.Ucm_commonsPackage;

import ucm_base.ucm_components.Ucm_componentsPackage;

import ucm_base.ucm_contracts.Ucm_contractsPackage;

import ucm_base.ucm_interactions.Ucm_interactionsPackage;

import ucm_base.ucm_technicalpolicies.Ucm_technicalpoliciesPackage;

import ucm_basic_projects.Ucm_basic_projectsPackage;

import ucm_environment.ucm_resources.Ucm_resourcesPackage;

import ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl;

import ucm_extended_projects.ExtendedProject;
import ucm_extended_projects.Ucm_extended_projectsFactory;
import ucm_extended_projects.Ucm_extended_projectsPackage;
import ucm_extended_projects.XDeploymentModule;
import ucm_extended_projects.XDetailedComponentImplementation;
import ucm_extended_projects.XEnvironmentModule;
import ucm_extended_projects.XMemoryPartition;
import ucm_extended_projects.XResourceDefinitionModule;

import ucm_supplement.ucm_deployments.Ucm_deploymentsPackage;

import ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl;

import ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage;

import ucm_supplement.ucm_detailed_components.impl.Ucm_detailed_componentsPackageImpl;

import ucm_supplement.ucm_enhanced_configuration.Ucm_enhanced_configurationPackage;

import ucm_supplement.ucm_enhanced_configuration.impl.Ucm_enhanced_configurationPackageImpl;

import ucm_supplement.ucm_enhanced_portspec.Ucm_enhanced_portspecPackage;

import ucm_supplement.ucm_enhanced_portspec.impl.Ucm_enhanced_portspecPackageImpl;

import ucm_supplement.ucm_test_cases.Ucm_test_casesPackage;

import ucm_supplement.ucm_test_cases.impl.Ucm_test_casesPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Ucm_extended_projectsPackageImpl extends EPackageImpl implements Ucm_extended_projectsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass extendedProjectEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xDetailedComponentImplementationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xDeploymentModuleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xEnvironmentModuleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xResourceDefinitionModuleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xMemoryPartitionEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ucm_extended_projects.Ucm_extended_projectsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private Ucm_extended_projectsPackageImpl() {
		super(eNS_URI, Ucm_extended_projectsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link Ucm_extended_projectsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static Ucm_extended_projectsPackage init() {
		if (isInited) return (Ucm_extended_projectsPackage)EPackage.Registry.INSTANCE.getEPackage(Ucm_extended_projectsPackage.eNS_URI);

		// Obtain or create and register package
		Ucm_extended_projectsPackageImpl theUcm_extended_projectsPackage = (Ucm_extended_projectsPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof Ucm_extended_projectsPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new Ucm_extended_projectsPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		Ucm_basic_projectsPackage.eINSTANCE.eClass();
		Ucm_commonsPackage.eINSTANCE.eClass();
		Ucm_technicalpoliciesPackage.eINSTANCE.eClass();
		Ucm_contractsPackage.eINSTANCE.eClass();
		Ucm_interactionsPackage.eINSTANCE.eClass();
		Ucm_componentsPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Ucm_detailed_componentsPackageImpl theUcm_detailed_componentsPackage = (Ucm_detailed_componentsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_detailed_componentsPackage.eNS_URI) instanceof Ucm_detailed_componentsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_detailed_componentsPackage.eNS_URI) : Ucm_detailed_componentsPackage.eINSTANCE);
		Ucm_enhanced_portspecPackageImpl theUcm_enhanced_portspecPackage = (Ucm_enhanced_portspecPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_enhanced_portspecPackage.eNS_URI) instanceof Ucm_enhanced_portspecPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_enhanced_portspecPackage.eNS_URI) : Ucm_enhanced_portspecPackage.eINSTANCE);
		Ucm_deploymentsPackageImpl theUcm_deploymentsPackage = (Ucm_deploymentsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_deploymentsPackage.eNS_URI) instanceof Ucm_deploymentsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_deploymentsPackage.eNS_URI) : Ucm_deploymentsPackage.eINSTANCE);
		Ucm_test_casesPackageImpl theUcm_test_casesPackage = (Ucm_test_casesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_test_casesPackage.eNS_URI) instanceof Ucm_test_casesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_test_casesPackage.eNS_URI) : Ucm_test_casesPackage.eINSTANCE);
		Ucm_enhanced_configurationPackageImpl theUcm_enhanced_configurationPackage = (Ucm_enhanced_configurationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_enhanced_configurationPackage.eNS_URI) instanceof Ucm_enhanced_configurationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_enhanced_configurationPackage.eNS_URI) : Ucm_enhanced_configurationPackage.eINSTANCE);
		Ucm_resourcesPackageImpl theUcm_resourcesPackage = (Ucm_resourcesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_resourcesPackage.eNS_URI) instanceof Ucm_resourcesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_resourcesPackage.eNS_URI) : Ucm_resourcesPackage.eINSTANCE);

		// Create package meta-data objects
		theUcm_extended_projectsPackage.createPackageContents();
		theUcm_detailed_componentsPackage.createPackageContents();
		theUcm_enhanced_portspecPackage.createPackageContents();
		theUcm_deploymentsPackage.createPackageContents();
		theUcm_test_casesPackage.createPackageContents();
		theUcm_enhanced_configurationPackage.createPackageContents();
		theUcm_resourcesPackage.createPackageContents();

		// Initialize created meta-data
		theUcm_extended_projectsPackage.initializePackageContents();
		theUcm_detailed_componentsPackage.initializePackageContents();
		theUcm_enhanced_portspecPackage.initializePackageContents();
		theUcm_deploymentsPackage.initializePackageContents();
		theUcm_test_casesPackage.initializePackageContents();
		theUcm_enhanced_configurationPackage.initializePackageContents();
		theUcm_resourcesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theUcm_extended_projectsPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(Ucm_extended_projectsPackage.eNS_URI, theUcm_extended_projectsPackage);
		return theUcm_extended_projectsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExtendedProject() {
		return extendedProjectEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXDetailedComponentImplementation() {
		return xDetailedComponentImplementationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXDeploymentModule() {
		return xDeploymentModuleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXEnvironmentModule() {
		return xEnvironmentModuleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXResourceDefinitionModule() {
		return xResourceDefinitionModuleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXMemoryPartition() {
		return xMemoryPartitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ucm_extended_projectsFactory getUcm_extended_projectsFactory() {
		return (Ucm_extended_projectsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		extendedProjectEClass = createEClass(EXTENDED_PROJECT);

		xDetailedComponentImplementationEClass = createEClass(XDETAILED_COMPONENT_IMPLEMENTATION);

		xDeploymentModuleEClass = createEClass(XDEPLOYMENT_MODULE);

		xEnvironmentModuleEClass = createEClass(XENVIRONMENT_MODULE);

		xResourceDefinitionModuleEClass = createEClass(XRESOURCE_DEFINITION_MODULE);

		xMemoryPartitionEClass = createEClass(XMEMORY_PARTITION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		Ucm_basic_projectsPackage theUcm_basic_projectsPackage = (Ucm_basic_projectsPackage)EPackage.Registry.INSTANCE.getEPackage(Ucm_basic_projectsPackage.eNS_URI);
		Ucm_detailed_componentsPackage theUcm_detailed_componentsPackage = (Ucm_detailed_componentsPackage)EPackage.Registry.INSTANCE.getEPackage(Ucm_detailed_componentsPackage.eNS_URI);
		Ucm_deploymentsPackage theUcm_deploymentsPackage = (Ucm_deploymentsPackage)EPackage.Registry.INSTANCE.getEPackage(Ucm_deploymentsPackage.eNS_URI);
		Ucm_resourcesPackage theUcm_resourcesPackage = (Ucm_resourcesPackage)EPackage.Registry.INSTANCE.getEPackage(Ucm_resourcesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		extendedProjectEClass.getESuperTypes().add(theUcm_basic_projectsPackage.getBasicProject());
		xDetailedComponentImplementationEClass.getESuperTypes().add(theUcm_detailed_componentsPackage.getDetailedComponentImplementation());
		xDeploymentModuleEClass.getESuperTypes().add(theUcm_deploymentsPackage.getDeploymentModule());
		xEnvironmentModuleEClass.getESuperTypes().add(theUcm_resourcesPackage.getEnvironmentModule());
		xResourceDefinitionModuleEClass.getESuperTypes().add(theUcm_resourcesPackage.getResourceDefinitionModule());
		xMemoryPartitionEClass.getESuperTypes().add(theUcm_resourcesPackage.getMemoryPartition());

		// Initialize classes and features; add operations and parameters
		initEClass(extendedProjectEClass, ExtendedProject.class, "ExtendedProject", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(xDetailedComponentImplementationEClass, XDetailedComponentImplementation.class, "XDetailedComponentImplementation", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(xDeploymentModuleEClass, XDeploymentModule.class, "XDeploymentModule", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(xEnvironmentModuleEClass, XEnvironmentModule.class, "XEnvironmentModule", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(xResourceDefinitionModuleEClass, XResourceDefinitionModule.class, "XResourceDefinitionModule", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(xMemoryPartitionEClass, XMemoryPartition.class, "XMemoryPartition", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //Ucm_extended_projectsPackageImpl
