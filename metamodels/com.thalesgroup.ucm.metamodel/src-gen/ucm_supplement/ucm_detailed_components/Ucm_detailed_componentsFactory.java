/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_detailed_components;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage
 * @generated
 */
public interface Ucm_detailed_componentsFactory extends EFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Ucm_detailed_componentsFactory eINSTANCE = ucm_supplement.ucm_detailed_components.impl.Ucm_detailed_componentsFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Detailed Component Implementation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Detailed Component Implementation</em>'.
	 * @generated
	 */
	DetailedComponentImplementation createDetailedComponentImplementation();

	/**
	 * Returns a new object of class '<em>Component Port Implementation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Component Port Implementation</em>'.
	 * @generated
	 */
	ComponentPortImplementation createComponentPortImplementation();

	/**
	 * Returns a new object of class '<em>Technical Port Implementation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Technical Port Implementation</em>'.
	 * @generated
	 */
	TechnicalPortImplementation createTechnicalPortImplementation();

	/**
	 * Returns a new object of class '<em>Port And Method</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port And Method</em>'.
	 * @generated
	 */
	PortAndMethod createPortAndMethod();

	/**
	 * Returns a new object of class '<em>Execution Scenario</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Execution Scenario</em>'.
	 * @generated
	 */
	ExecutionScenario createExecutionScenario();

	/**
	 * Returns a new object of class '<em>Computation Step</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Computation Step</em>'.
	 * @generated
	 */
	ComputationStep createComputationStep();

	/**
	 * Returns a new object of class '<em>Computation With Ending Communication Step</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Computation With Ending Communication Step</em>'.
	 * @generated
	 */
	ComputationWithEndingCommunicationStep createComputationWithEndingCommunicationStep();

	/**
	 * Returns a new object of class '<em>Loop Step</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Loop Step</em>'.
	 * @generated
	 */
	LoopStep createLoopStep();

	/**
	 * Returns a new object of class '<em>Alternative Step</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Alternative Step</em>'.
	 * @generated
	 */
	AlternativeStep createAlternativeStep();

	/**
	 * Returns a new object of class '<em>Sequence Step</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sequence Step</em>'.
	 * @generated
	 */
	SequenceStep createSequenceStep();

	/**
	 * Returns a new object of class '<em>Component Multiplex Port Implementation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Component Multiplex Port Implementation</em>'.
	 * @generated
	 */
	ComponentMultiplexPortImplementation createComponentMultiplexPortImplementation();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	Ucm_detailed_componentsPackage getUcm_detailed_componentsPackage();

} //Ucm_detailed_componentsFactory
