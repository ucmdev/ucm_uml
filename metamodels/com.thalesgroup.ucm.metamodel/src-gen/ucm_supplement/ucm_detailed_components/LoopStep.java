/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_detailed_components;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Loop Step</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_detailed_components.LoopStep#getStep <em>Step</em>}</li>
 *   <li>{@link ucm_supplement.ucm_detailed_components.LoopStep#getMinIterations <em>Min Iterations</em>}</li>
 *   <li>{@link ucm_supplement.ucm_detailed_components.LoopStep#getMaxIterations <em>Max Iterations</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getLoopStep()
 * @model
 * @generated
 */
public interface LoopStep extends IExecutionStep {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Step</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Step</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Step</em>' containment reference.
	 * @see #setStep(IExecutionStep)
	 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getLoopStep_Step()
	 * @model containment="true" required="true"
	 * @generated
	 */
	IExecutionStep getStep();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_detailed_components.LoopStep#getStep <em>Step</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Step</em>' containment reference.
	 * @see #getStep()
	 * @generated
	 */
	void setStep(IExecutionStep value);

	/**
	 * Returns the value of the '<em><b>Min Iterations</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min Iterations</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Iterations</em>' attribute.
	 * @see #setMinIterations(long)
	 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getLoopStep_MinIterations()
	 * @model
	 * @generated
	 */
	long getMinIterations();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_detailed_components.LoopStep#getMinIterations <em>Min Iterations</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Iterations</em>' attribute.
	 * @see #getMinIterations()
	 * @generated
	 */
	void setMinIterations(long value);

	/**
	 * Returns the value of the '<em><b>Max Iterations</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Iterations</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Iterations</em>' attribute.
	 * @see #setMaxIterations(long)
	 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getLoopStep_MaxIterations()
	 * @model
	 * @generated
	 */
	long getMaxIterations();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_detailed_components.LoopStep#getMaxIterations <em>Max Iterations</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Iterations</em>' attribute.
	 * @see #getMaxIterations()
	 * @generated
	 */
	void setMaxIterations(long value);

} // LoopStep
