/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_detailed_components;

import java.util.Map;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EObject;

import ucm_base.ucm_contracts.Method;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port And Method</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_detailed_components.PortAndMethod#getMethod <em>Method</em>}</li>
 *   <li>{@link ucm_supplement.ucm_detailed_components.PortAndMethod#getPortImplementation <em>Port Implementation</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getPortAndMethod()
 * @model
 * @generated
 */
public interface PortAndMethod extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Method</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Method</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Method</em>' reference.
	 * @see #setMethod(Method)
	 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getPortAndMethod_Method()
	 * @model required="true"
	 * @generated
	 */
	Method getMethod();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_detailed_components.PortAndMethod#getMethod <em>Method</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Method</em>' reference.
	 * @see #getMethod()
	 * @generated
	 */
	void setMethod(Method value);

	/**
	 * Returns the value of the '<em><b>Port Implementation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port Implementation</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port Implementation</em>' reference.
	 * @see #setPortImplementation(IPortElementImplementation)
	 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getPortAndMethod_PortImplementation()
	 * @model required="true"
	 * @generated
	 */
	IPortElementImplementation getPortImplementation();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_detailed_components.PortAndMethod#getPortImplementation <em>Port Implementation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port Implementation</em>' reference.
	 * @see #getPortImplementation()
	 * @generated
	 */
	void setPortImplementation(IPortElementImplementation value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean consistency(DiagnosticChain diagnostics, Map<Object, Object> context);

} // PortAndMethod
