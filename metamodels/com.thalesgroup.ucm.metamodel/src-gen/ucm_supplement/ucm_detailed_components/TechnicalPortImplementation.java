/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_detailed_components;

import java.util.Map;
import org.eclipse.emf.common.util.DiagnosticChain;
import ucm_base.ucm_components.ComponentTechnicalPolicy;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Technical Port Implementation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_detailed_components.TechnicalPortImplementation#getPolicy <em>Policy</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getTechnicalPortImplementation()
 * @model
 * @generated
 */
public interface TechnicalPortImplementation extends IPortElementImplementation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Policy</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Policy</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Policy</em>' reference.
	 * @see #setPolicy(ComponentTechnicalPolicy)
	 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getTechnicalPortImplementation_Policy()
	 * @model required="true"
	 * @generated
	 */
	ComponentTechnicalPolicy getPolicy();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_detailed_components.TechnicalPortImplementation#getPolicy <em>Policy</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Policy</em>' reference.
	 * @see #getPolicy()
	 * @generated
	 */
	void setPolicy(ComponentTechnicalPolicy value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean portImplementationConsistency(DiagnosticChain diagnostics, Map<Object, Object> context);

} // TechnicalPortImplementation
