/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_detailed_components;

import ucm_base.ucm_contracts.ucm_datatypes.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component Multiplex Port Implementation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_detailed_components.ComponentMultiplexPortImplementation#getBindingName <em>Binding Name</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getComponentMultiplexPortImplementation()
 * @model
 * @generated
 */
public interface ComponentMultiplexPortImplementation extends ComponentPortImplementation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Binding Name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Binding Name</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Binding Name</em>' reference.
	 * @see #setBindingName(Enumerator)
	 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getComponentMultiplexPortImplementation_BindingName()
	 * @model
	 * @generated
	 */
	Enumerator getBindingName();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_detailed_components.ComponentMultiplexPortImplementation#getBindingName <em>Binding Name</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Binding Name</em>' reference.
	 * @see #getBindingName()
	 * @generated
	 */
	void setBindingName(Enumerator value);

} // ComponentMultiplexPortImplementation
