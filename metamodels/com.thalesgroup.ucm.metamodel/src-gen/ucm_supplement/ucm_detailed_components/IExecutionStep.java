/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_detailed_components;

import ucm_base.ucm_commons.INamed;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IExecution Step</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getIExecutionStep()
 * @model abstract="true"
 * @generated
 */
public interface IExecutionStep extends INamed {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

} // IExecutionStep
