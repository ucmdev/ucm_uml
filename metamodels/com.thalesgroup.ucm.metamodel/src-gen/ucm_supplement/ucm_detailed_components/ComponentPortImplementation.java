/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_detailed_components;

import java.util.Map;
import org.eclipse.emf.common.util.DiagnosticChain;
import ucm_base.ucm_components.Port;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component Port Implementation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_detailed_components.ComponentPortImplementation#getPort <em>Port</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getComponentPortImplementation()
 * @model
 * @generated
 */
public interface ComponentPortImplementation extends IPortElementImplementation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port</em>' reference.
	 * @see #setPort(Port)
	 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getComponentPortImplementation_Port()
	 * @model required="true"
	 * @generated
	 */
	Port getPort();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_detailed_components.ComponentPortImplementation#getPort <em>Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port</em>' reference.
	 * @see #getPort()
	 * @generated
	 */
	void setPort(Port value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean portImplementationConsistency(DiagnosticChain diagnostics, Map<Object, Object> context);

} // ComponentPortImplementation
