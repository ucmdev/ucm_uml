/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_detailed_components;

import java.util.Map;
import org.eclipse.emf.common.util.DiagnosticChain;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Computation Step</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_detailed_components.ComputationStep#getSpecifiedWCET <em>Specified WCET</em>}</li>
 *   <li>{@link ucm_supplement.ucm_detailed_components.ComputationStep#getSpecifiedBCET <em>Specified BCET</em>}</li>
 *   <li>{@link ucm_supplement.ucm_detailed_components.ComputationStep#getMeasuredWCET <em>Measured WCET</em>}</li>
 *   <li>{@link ucm_supplement.ucm_detailed_components.ComputationStep#getMeasuredBCET <em>Measured BCET</em>}</li>
 *   <li>{@link ucm_supplement.ucm_detailed_components.ComputationStep#getEvaluatedWCET <em>Evaluated WCET</em>}</li>
 *   <li>{@link ucm_supplement.ucm_detailed_components.ComputationStep#getEvaluatedBCET <em>Evaluated BCET</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getComputationStep()
 * @model
 * @generated
 */
public interface ComputationStep extends IExecutionStep {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Specified WCET</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Specified WCET</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specified WCET</em>' attribute.
	 * @see #setSpecifiedWCET(double)
	 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getComputationStep_SpecifiedWCET()
	 * @model
	 * @generated
	 */
	double getSpecifiedWCET();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_detailed_components.ComputationStep#getSpecifiedWCET <em>Specified WCET</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Specified WCET</em>' attribute.
	 * @see #getSpecifiedWCET()
	 * @generated
	 */
	void setSpecifiedWCET(double value);

	/**
	 * Returns the value of the '<em><b>Specified BCET</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Specified BCET</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specified BCET</em>' attribute.
	 * @see #setSpecifiedBCET(double)
	 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getComputationStep_SpecifiedBCET()
	 * @model
	 * @generated
	 */
	double getSpecifiedBCET();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_detailed_components.ComputationStep#getSpecifiedBCET <em>Specified BCET</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Specified BCET</em>' attribute.
	 * @see #getSpecifiedBCET()
	 * @generated
	 */
	void setSpecifiedBCET(double value);

	/**
	 * Returns the value of the '<em><b>Measured WCET</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Measured WCET</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Measured WCET</em>' attribute.
	 * @see #setMeasuredWCET(double)
	 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getComputationStep_MeasuredWCET()
	 * @model
	 * @generated
	 */
	double getMeasuredWCET();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_detailed_components.ComputationStep#getMeasuredWCET <em>Measured WCET</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Measured WCET</em>' attribute.
	 * @see #getMeasuredWCET()
	 * @generated
	 */
	void setMeasuredWCET(double value);

	/**
	 * Returns the value of the '<em><b>Measured BCET</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Measured BCET</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Measured BCET</em>' attribute.
	 * @see #setMeasuredBCET(double)
	 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getComputationStep_MeasuredBCET()
	 * @model
	 * @generated
	 */
	double getMeasuredBCET();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_detailed_components.ComputationStep#getMeasuredBCET <em>Measured BCET</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Measured BCET</em>' attribute.
	 * @see #getMeasuredBCET()
	 * @generated
	 */
	void setMeasuredBCET(double value);

	/**
	 * Returns the value of the '<em><b>Evaluated WCET</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Evaluated WCET</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Evaluated WCET</em>' attribute.
	 * @see #setEvaluatedWCET(double)
	 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getComputationStep_EvaluatedWCET()
	 * @model
	 * @generated
	 */
	double getEvaluatedWCET();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_detailed_components.ComputationStep#getEvaluatedWCET <em>Evaluated WCET</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Evaluated WCET</em>' attribute.
	 * @see #getEvaluatedWCET()
	 * @generated
	 */
	void setEvaluatedWCET(double value);

	/**
	 * Returns the value of the '<em><b>Evaluated BCET</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Evaluated BCET</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Evaluated BCET</em>' attribute.
	 * @see #setEvaluatedBCET(double)
	 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getComputationStep_EvaluatedBCET()
	 * @model
	 * @generated
	 */
	double getEvaluatedBCET();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_detailed_components.ComputationStep#getEvaluatedBCET <em>Evaluated BCET</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Evaluated BCET</em>' attribute.
	 * @see #getEvaluatedBCET()
	 * @generated
	 */
	void setEvaluatedBCET(double value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean valueConsistency(DiagnosticChain diagnostics, Map<Object, Object> context);

} // ComputationStep
