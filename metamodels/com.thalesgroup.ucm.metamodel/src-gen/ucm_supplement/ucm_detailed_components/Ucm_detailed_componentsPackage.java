/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_detailed_components;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import ucm_base.ucm_commons.Ucm_commonsPackage;

import ucm_base.ucm_components.Ucm_componentsPackage;

import ucm_base.ucm_contracts.Ucm_contractsPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsFactory
 * @model kind="package"
 * @generated
 */
public interface Ucm_detailed_componentsPackage extends EPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ucm_detailed_components";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://com.thalesgroup.ucm/1.0/metamodels/supplement/detailedcomponents";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ucm_detailed_components";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Ucm_detailed_componentsPackage eINSTANCE = ucm_supplement.ucm_detailed_components.impl.Ucm_detailed_componentsPackageImpl.init();

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_detailed_components.impl.DetailedComponentImplementationImpl <em>Detailed Component Implementation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_detailed_components.impl.DetailedComponentImplementationImpl
	 * @see ucm_supplement.ucm_detailed_components.impl.Ucm_detailed_componentsPackageImpl#getDetailedComponentImplementation()
	 * @generated
	 */
	int DETAILED_COMPONENT_IMPLEMENTATION = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DETAILED_COMPONENT_IMPLEMENTATION__NAME = Ucm_componentsPackage.ATOMIC_COMPONENT_IMPLEMENTATION__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DETAILED_COMPONENT_IMPLEMENTATION__COMMENT = Ucm_componentsPackage.ATOMIC_COMPONENT_IMPLEMENTATION__COMMENT;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DETAILED_COMPONENT_IMPLEMENTATION__ANNOTATION = Ucm_componentsPackage.ATOMIC_COMPONENT_IMPLEMENTATION__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DETAILED_COMPONENT_IMPLEMENTATION__TYPE = Ucm_componentsPackage.ATOMIC_COMPONENT_IMPLEMENTATION__TYPE;

	/**
	 * The feature id for the '<em><b>Programming Language</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DETAILED_COMPONENT_IMPLEMENTATION__PROGRAMMING_LANGUAGE = Ucm_componentsPackage.ATOMIC_COMPONENT_IMPLEMENTATION__PROGRAMMING_LANGUAGE;

	/**
	 * The feature id for the '<em><b>Policy</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DETAILED_COMPONENT_IMPLEMENTATION__POLICY = Ucm_componentsPackage.ATOMIC_COMPONENT_IMPLEMENTATION__POLICY;

	/**
	 * The feature id for the '<em><b>Port Implementation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DETAILED_COMPONENT_IMPLEMENTATION__PORT_IMPLEMENTATION = Ucm_componentsPackage.ATOMIC_COMPONENT_IMPLEMENTATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Execution Scenario</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DETAILED_COMPONENT_IMPLEMENTATION__EXECUTION_SCENARIO = Ucm_componentsPackage.ATOMIC_COMPONENT_IMPLEMENTATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Extends</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DETAILED_COMPONENT_IMPLEMENTATION__EXTENDS = Ucm_componentsPackage.ATOMIC_COMPONENT_IMPLEMENTATION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Detailed Component Implementation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DETAILED_COMPONENT_IMPLEMENTATION_FEATURE_COUNT = Ucm_componentsPackage.ATOMIC_COMPONENT_IMPLEMENTATION_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_detailed_components.impl.IPortElementImplementationImpl <em>IPort Element Implementation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_detailed_components.impl.IPortElementImplementationImpl
	 * @see ucm_supplement.ucm_detailed_components.impl.Ucm_detailed_componentsPackageImpl#getIPortElementImplementation()
	 * @generated
	 */
	int IPORT_ELEMENT_IMPLEMENTATION = 1;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPORT_ELEMENT_IMPLEMENTATION__ANNOTATION = Ucm_contractsPackage.IANNOTABLE__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPORT_ELEMENT_IMPLEMENTATION__NAME = Ucm_contractsPackage.IANNOTABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPORT_ELEMENT_IMPLEMENTATION__COMMENT = Ucm_contractsPackage.IANNOTABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Port Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPORT_ELEMENT_IMPLEMENTATION__PORT_ELEMENT = Ucm_contractsPackage.IANNOTABLE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Intf</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPORT_ELEMENT_IMPLEMENTATION__INTF = Ucm_contractsPackage.IANNOTABLE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>IPort Element Implementation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPORT_ELEMENT_IMPLEMENTATION_FEATURE_COUNT = Ucm_contractsPackage.IANNOTABLE_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_detailed_components.impl.ComponentPortImplementationImpl <em>Component Port Implementation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_detailed_components.impl.ComponentPortImplementationImpl
	 * @see ucm_supplement.ucm_detailed_components.impl.Ucm_detailed_componentsPackageImpl#getComponentPortImplementation()
	 * @generated
	 */
	int COMPONENT_PORT_IMPLEMENTATION = 2;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_PORT_IMPLEMENTATION__ANNOTATION = IPORT_ELEMENT_IMPLEMENTATION__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_PORT_IMPLEMENTATION__NAME = IPORT_ELEMENT_IMPLEMENTATION__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_PORT_IMPLEMENTATION__COMMENT = IPORT_ELEMENT_IMPLEMENTATION__COMMENT;

	/**
	 * The feature id for the '<em><b>Port Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_PORT_IMPLEMENTATION__PORT_ELEMENT = IPORT_ELEMENT_IMPLEMENTATION__PORT_ELEMENT;

	/**
	 * The feature id for the '<em><b>Intf</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_PORT_IMPLEMENTATION__INTF = IPORT_ELEMENT_IMPLEMENTATION__INTF;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_PORT_IMPLEMENTATION__PORT = IPORT_ELEMENT_IMPLEMENTATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Component Port Implementation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_PORT_IMPLEMENTATION_FEATURE_COUNT = IPORT_ELEMENT_IMPLEMENTATION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_detailed_components.impl.TechnicalPortImplementationImpl <em>Technical Port Implementation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_detailed_components.impl.TechnicalPortImplementationImpl
	 * @see ucm_supplement.ucm_detailed_components.impl.Ucm_detailed_componentsPackageImpl#getTechnicalPortImplementation()
	 * @generated
	 */
	int TECHNICAL_PORT_IMPLEMENTATION = 3;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TECHNICAL_PORT_IMPLEMENTATION__ANNOTATION = IPORT_ELEMENT_IMPLEMENTATION__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TECHNICAL_PORT_IMPLEMENTATION__NAME = IPORT_ELEMENT_IMPLEMENTATION__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TECHNICAL_PORT_IMPLEMENTATION__COMMENT = IPORT_ELEMENT_IMPLEMENTATION__COMMENT;

	/**
	 * The feature id for the '<em><b>Port Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TECHNICAL_PORT_IMPLEMENTATION__PORT_ELEMENT = IPORT_ELEMENT_IMPLEMENTATION__PORT_ELEMENT;

	/**
	 * The feature id for the '<em><b>Intf</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TECHNICAL_PORT_IMPLEMENTATION__INTF = IPORT_ELEMENT_IMPLEMENTATION__INTF;

	/**
	 * The feature id for the '<em><b>Policy</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TECHNICAL_PORT_IMPLEMENTATION__POLICY = IPORT_ELEMENT_IMPLEMENTATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Technical Port Implementation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TECHNICAL_PORT_IMPLEMENTATION_FEATURE_COUNT = IPORT_ELEMENT_IMPLEMENTATION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_detailed_components.impl.PortAndMethodImpl <em>Port And Method</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_detailed_components.impl.PortAndMethodImpl
	 * @see ucm_supplement.ucm_detailed_components.impl.Ucm_detailed_componentsPackageImpl#getPortAndMethod()
	 * @generated
	 */
	int PORT_AND_METHOD = 4;

	/**
	 * The feature id for the '<em><b>Method</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_AND_METHOD__METHOD = 0;

	/**
	 * The feature id for the '<em><b>Port Implementation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_AND_METHOD__PORT_IMPLEMENTATION = 1;

	/**
	 * The number of structural features of the '<em>Port And Method</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_AND_METHOD_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_detailed_components.impl.ExecutionScenarioImpl <em>Execution Scenario</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_detailed_components.impl.ExecutionScenarioImpl
	 * @see ucm_supplement.ucm_detailed_components.impl.Ucm_detailed_componentsPackageImpl#getExecutionScenario()
	 * @generated
	 */
	int EXECUTION_SCENARIO = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_SCENARIO__NAME = Ucm_commonsPackage.INAMED__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_SCENARIO__COMMENT = Ucm_commonsPackage.INAMED__COMMENT;

	/**
	 * The feature id for the '<em><b>Port And Method</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_SCENARIO__PORT_AND_METHOD = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Associated Step</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_SCENARIO__ASSOCIATED_STEP = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Execution Scenario</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_SCENARIO_FEATURE_COUNT = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_detailed_components.impl.IExecutionStepImpl <em>IExecution Step</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_detailed_components.impl.IExecutionStepImpl
	 * @see ucm_supplement.ucm_detailed_components.impl.Ucm_detailed_componentsPackageImpl#getIExecutionStep()
	 * @generated
	 */
	int IEXECUTION_STEP = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEXECUTION_STEP__NAME = Ucm_commonsPackage.INAMED__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEXECUTION_STEP__COMMENT = Ucm_commonsPackage.INAMED__COMMENT;

	/**
	 * The number of structural features of the '<em>IExecution Step</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEXECUTION_STEP_FEATURE_COUNT = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_detailed_components.impl.ComputationStepImpl <em>Computation Step</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_detailed_components.impl.ComputationStepImpl
	 * @see ucm_supplement.ucm_detailed_components.impl.Ucm_detailed_componentsPackageImpl#getComputationStep()
	 * @generated
	 */
	int COMPUTATION_STEP = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_STEP__NAME = IEXECUTION_STEP__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_STEP__COMMENT = IEXECUTION_STEP__COMMENT;

	/**
	 * The feature id for the '<em><b>Specified WCET</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_STEP__SPECIFIED_WCET = IEXECUTION_STEP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Specified BCET</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_STEP__SPECIFIED_BCET = IEXECUTION_STEP_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Measured WCET</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_STEP__MEASURED_WCET = IEXECUTION_STEP_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Measured BCET</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_STEP__MEASURED_BCET = IEXECUTION_STEP_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Evaluated WCET</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_STEP__EVALUATED_WCET = IEXECUTION_STEP_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Evaluated BCET</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_STEP__EVALUATED_BCET = IEXECUTION_STEP_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Computation Step</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_STEP_FEATURE_COUNT = IEXECUTION_STEP_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_detailed_components.impl.ComputationWithEndingCommunicationStepImpl <em>Computation With Ending Communication Step</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_detailed_components.impl.ComputationWithEndingCommunicationStepImpl
	 * @see ucm_supplement.ucm_detailed_components.impl.Ucm_detailed_componentsPackageImpl#getComputationWithEndingCommunicationStep()
	 * @generated
	 */
	int COMPUTATION_WITH_ENDING_COMMUNICATION_STEP = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_WITH_ENDING_COMMUNICATION_STEP__NAME = COMPUTATION_STEP__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_WITH_ENDING_COMMUNICATION_STEP__COMMENT = COMPUTATION_STEP__COMMENT;

	/**
	 * The feature id for the '<em><b>Specified WCET</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_WITH_ENDING_COMMUNICATION_STEP__SPECIFIED_WCET = COMPUTATION_STEP__SPECIFIED_WCET;

	/**
	 * The feature id for the '<em><b>Specified BCET</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_WITH_ENDING_COMMUNICATION_STEP__SPECIFIED_BCET = COMPUTATION_STEP__SPECIFIED_BCET;

	/**
	 * The feature id for the '<em><b>Measured WCET</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_WITH_ENDING_COMMUNICATION_STEP__MEASURED_WCET = COMPUTATION_STEP__MEASURED_WCET;

	/**
	 * The feature id for the '<em><b>Measured BCET</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_WITH_ENDING_COMMUNICATION_STEP__MEASURED_BCET = COMPUTATION_STEP__MEASURED_BCET;

	/**
	 * The feature id for the '<em><b>Evaluated WCET</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_WITH_ENDING_COMMUNICATION_STEP__EVALUATED_WCET = COMPUTATION_STEP__EVALUATED_WCET;

	/**
	 * The feature id for the '<em><b>Evaluated BCET</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_WITH_ENDING_COMMUNICATION_STEP__EVALUATED_BCET = COMPUTATION_STEP__EVALUATED_BCET;

	/**
	 * The feature id for the '<em><b>Port And Method To Call</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_WITH_ENDING_COMMUNICATION_STEP__PORT_AND_METHOD_TO_CALL = COMPUTATION_STEP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Computation With Ending Communication Step</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_WITH_ENDING_COMMUNICATION_STEP_FEATURE_COUNT = COMPUTATION_STEP_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_detailed_components.impl.LoopStepImpl <em>Loop Step</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_detailed_components.impl.LoopStepImpl
	 * @see ucm_supplement.ucm_detailed_components.impl.Ucm_detailed_componentsPackageImpl#getLoopStep()
	 * @generated
	 */
	int LOOP_STEP = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_STEP__NAME = IEXECUTION_STEP__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_STEP__COMMENT = IEXECUTION_STEP__COMMENT;

	/**
	 * The feature id for the '<em><b>Step</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_STEP__STEP = IEXECUTION_STEP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Min Iterations</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_STEP__MIN_ITERATIONS = IEXECUTION_STEP_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Max Iterations</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_STEP__MAX_ITERATIONS = IEXECUTION_STEP_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Loop Step</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_STEP_FEATURE_COUNT = IEXECUTION_STEP_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_detailed_components.impl.AlternativeStepImpl <em>Alternative Step</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_detailed_components.impl.AlternativeStepImpl
	 * @see ucm_supplement.ucm_detailed_components.impl.Ucm_detailed_componentsPackageImpl#getAlternativeStep()
	 * @generated
	 */
	int ALTERNATIVE_STEP = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALTERNATIVE_STEP__NAME = IEXECUTION_STEP__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALTERNATIVE_STEP__COMMENT = IEXECUTION_STEP__COMMENT;

	/**
	 * The feature id for the '<em><b>Step</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALTERNATIVE_STEP__STEP = IEXECUTION_STEP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Alternative Step</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALTERNATIVE_STEP_FEATURE_COUNT = IEXECUTION_STEP_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_detailed_components.impl.SequenceStepImpl <em>Sequence Step</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_detailed_components.impl.SequenceStepImpl
	 * @see ucm_supplement.ucm_detailed_components.impl.Ucm_detailed_componentsPackageImpl#getSequenceStep()
	 * @generated
	 */
	int SEQUENCE_STEP = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_STEP__NAME = IEXECUTION_STEP__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_STEP__COMMENT = IEXECUTION_STEP__COMMENT;

	/**
	 * The feature id for the '<em><b>Step</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_STEP__STEP = IEXECUTION_STEP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Sequence Step</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_STEP_FEATURE_COUNT = IEXECUTION_STEP_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_detailed_components.impl.ComponentMultiplexPortImplementationImpl <em>Component Multiplex Port Implementation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_detailed_components.impl.ComponentMultiplexPortImplementationImpl
	 * @see ucm_supplement.ucm_detailed_components.impl.Ucm_detailed_componentsPackageImpl#getComponentMultiplexPortImplementation()
	 * @generated
	 */
	int COMPONENT_MULTIPLEX_PORT_IMPLEMENTATION = 12;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_MULTIPLEX_PORT_IMPLEMENTATION__ANNOTATION = COMPONENT_PORT_IMPLEMENTATION__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_MULTIPLEX_PORT_IMPLEMENTATION__NAME = COMPONENT_PORT_IMPLEMENTATION__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_MULTIPLEX_PORT_IMPLEMENTATION__COMMENT = COMPONENT_PORT_IMPLEMENTATION__COMMENT;

	/**
	 * The feature id for the '<em><b>Port Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_MULTIPLEX_PORT_IMPLEMENTATION__PORT_ELEMENT = COMPONENT_PORT_IMPLEMENTATION__PORT_ELEMENT;

	/**
	 * The feature id for the '<em><b>Intf</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_MULTIPLEX_PORT_IMPLEMENTATION__INTF = COMPONENT_PORT_IMPLEMENTATION__INTF;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_MULTIPLEX_PORT_IMPLEMENTATION__PORT = COMPONENT_PORT_IMPLEMENTATION__PORT;

	/**
	 * The feature id for the '<em><b>Binding Name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_MULTIPLEX_PORT_IMPLEMENTATION__BINDING_NAME = COMPONENT_PORT_IMPLEMENTATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Component Multiplex Port Implementation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_MULTIPLEX_PORT_IMPLEMENTATION_FEATURE_COUNT = COMPONENT_PORT_IMPLEMENTATION_FEATURE_COUNT + 1;


	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_detailed_components.DetailedComponentImplementation <em>Detailed Component Implementation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Detailed Component Implementation</em>'.
	 * @see ucm_supplement.ucm_detailed_components.DetailedComponentImplementation
	 * @generated
	 */
	EClass getDetailedComponentImplementation();

	/**
	 * Returns the meta object for the containment reference list '{@link ucm_supplement.ucm_detailed_components.DetailedComponentImplementation#getPortImplementation <em>Port Implementation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Port Implementation</em>'.
	 * @see ucm_supplement.ucm_detailed_components.DetailedComponentImplementation#getPortImplementation()
	 * @see #getDetailedComponentImplementation()
	 * @generated
	 */
	EReference getDetailedComponentImplementation_PortImplementation();

	/**
	 * Returns the meta object for the containment reference list '{@link ucm_supplement.ucm_detailed_components.DetailedComponentImplementation#getExecutionScenario <em>Execution Scenario</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Execution Scenario</em>'.
	 * @see ucm_supplement.ucm_detailed_components.DetailedComponentImplementation#getExecutionScenario()
	 * @see #getDetailedComponentImplementation()
	 * @generated
	 */
	EReference getDetailedComponentImplementation_ExecutionScenario();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_detailed_components.DetailedComponentImplementation#getExtends <em>Extends</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Extends</em>'.
	 * @see ucm_supplement.ucm_detailed_components.DetailedComponentImplementation#getExtends()
	 * @see #getDetailedComponentImplementation()
	 * @generated
	 */
	EReference getDetailedComponentImplementation_Extends();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_detailed_components.IPortElementImplementation <em>IPort Element Implementation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IPort Element Implementation</em>'.
	 * @see ucm_supplement.ucm_detailed_components.IPortElementImplementation
	 * @generated
	 */
	EClass getIPortElementImplementation();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_detailed_components.IPortElementImplementation#getPortElement <em>Port Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Port Element</em>'.
	 * @see ucm_supplement.ucm_detailed_components.IPortElementImplementation#getPortElement()
	 * @see #getIPortElementImplementation()
	 * @generated
	 */
	EReference getIPortElementImplementation_PortElement();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_detailed_components.IPortElementImplementation#getIntf <em>Intf</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Intf</em>'.
	 * @see ucm_supplement.ucm_detailed_components.IPortElementImplementation#getIntf()
	 * @see #getIPortElementImplementation()
	 * @generated
	 */
	EReference getIPortElementImplementation_Intf();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_detailed_components.ComponentPortImplementation <em>Component Port Implementation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component Port Implementation</em>'.
	 * @see ucm_supplement.ucm_detailed_components.ComponentPortImplementation
	 * @generated
	 */
	EClass getComponentPortImplementation();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_detailed_components.ComponentPortImplementation#getPort <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Port</em>'.
	 * @see ucm_supplement.ucm_detailed_components.ComponentPortImplementation#getPort()
	 * @see #getComponentPortImplementation()
	 * @generated
	 */
	EReference getComponentPortImplementation_Port();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_detailed_components.TechnicalPortImplementation <em>Technical Port Implementation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Technical Port Implementation</em>'.
	 * @see ucm_supplement.ucm_detailed_components.TechnicalPortImplementation
	 * @generated
	 */
	EClass getTechnicalPortImplementation();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_detailed_components.TechnicalPortImplementation#getPolicy <em>Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Policy</em>'.
	 * @see ucm_supplement.ucm_detailed_components.TechnicalPortImplementation#getPolicy()
	 * @see #getTechnicalPortImplementation()
	 * @generated
	 */
	EReference getTechnicalPortImplementation_Policy();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_detailed_components.PortAndMethod <em>Port And Method</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port And Method</em>'.
	 * @see ucm_supplement.ucm_detailed_components.PortAndMethod
	 * @generated
	 */
	EClass getPortAndMethod();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_detailed_components.PortAndMethod#getMethod <em>Method</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Method</em>'.
	 * @see ucm_supplement.ucm_detailed_components.PortAndMethod#getMethod()
	 * @see #getPortAndMethod()
	 * @generated
	 */
	EReference getPortAndMethod_Method();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_detailed_components.PortAndMethod#getPortImplementation <em>Port Implementation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Port Implementation</em>'.
	 * @see ucm_supplement.ucm_detailed_components.PortAndMethod#getPortImplementation()
	 * @see #getPortAndMethod()
	 * @generated
	 */
	EReference getPortAndMethod_PortImplementation();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_detailed_components.ExecutionScenario <em>Execution Scenario</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Execution Scenario</em>'.
	 * @see ucm_supplement.ucm_detailed_components.ExecutionScenario
	 * @generated
	 */
	EClass getExecutionScenario();

	/**
	 * Returns the meta object for the containment reference '{@link ucm_supplement.ucm_detailed_components.ExecutionScenario#getPortAndMethod <em>Port And Method</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Port And Method</em>'.
	 * @see ucm_supplement.ucm_detailed_components.ExecutionScenario#getPortAndMethod()
	 * @see #getExecutionScenario()
	 * @generated
	 */
	EReference getExecutionScenario_PortAndMethod();

	/**
	 * Returns the meta object for the containment reference '{@link ucm_supplement.ucm_detailed_components.ExecutionScenario#getAssociatedStep <em>Associated Step</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Associated Step</em>'.
	 * @see ucm_supplement.ucm_detailed_components.ExecutionScenario#getAssociatedStep()
	 * @see #getExecutionScenario()
	 * @generated
	 */
	EReference getExecutionScenario_AssociatedStep();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_detailed_components.IExecutionStep <em>IExecution Step</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IExecution Step</em>'.
	 * @see ucm_supplement.ucm_detailed_components.IExecutionStep
	 * @generated
	 */
	EClass getIExecutionStep();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_detailed_components.ComputationStep <em>Computation Step</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Computation Step</em>'.
	 * @see ucm_supplement.ucm_detailed_components.ComputationStep
	 * @generated
	 */
	EClass getComputationStep();

	/**
	 * Returns the meta object for the attribute '{@link ucm_supplement.ucm_detailed_components.ComputationStep#getSpecifiedWCET <em>Specified WCET</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Specified WCET</em>'.
	 * @see ucm_supplement.ucm_detailed_components.ComputationStep#getSpecifiedWCET()
	 * @see #getComputationStep()
	 * @generated
	 */
	EAttribute getComputationStep_SpecifiedWCET();

	/**
	 * Returns the meta object for the attribute '{@link ucm_supplement.ucm_detailed_components.ComputationStep#getSpecifiedBCET <em>Specified BCET</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Specified BCET</em>'.
	 * @see ucm_supplement.ucm_detailed_components.ComputationStep#getSpecifiedBCET()
	 * @see #getComputationStep()
	 * @generated
	 */
	EAttribute getComputationStep_SpecifiedBCET();

	/**
	 * Returns the meta object for the attribute '{@link ucm_supplement.ucm_detailed_components.ComputationStep#getMeasuredWCET <em>Measured WCET</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Measured WCET</em>'.
	 * @see ucm_supplement.ucm_detailed_components.ComputationStep#getMeasuredWCET()
	 * @see #getComputationStep()
	 * @generated
	 */
	EAttribute getComputationStep_MeasuredWCET();

	/**
	 * Returns the meta object for the attribute '{@link ucm_supplement.ucm_detailed_components.ComputationStep#getMeasuredBCET <em>Measured BCET</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Measured BCET</em>'.
	 * @see ucm_supplement.ucm_detailed_components.ComputationStep#getMeasuredBCET()
	 * @see #getComputationStep()
	 * @generated
	 */
	EAttribute getComputationStep_MeasuredBCET();

	/**
	 * Returns the meta object for the attribute '{@link ucm_supplement.ucm_detailed_components.ComputationStep#getEvaluatedWCET <em>Evaluated WCET</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Evaluated WCET</em>'.
	 * @see ucm_supplement.ucm_detailed_components.ComputationStep#getEvaluatedWCET()
	 * @see #getComputationStep()
	 * @generated
	 */
	EAttribute getComputationStep_EvaluatedWCET();

	/**
	 * Returns the meta object for the attribute '{@link ucm_supplement.ucm_detailed_components.ComputationStep#getEvaluatedBCET <em>Evaluated BCET</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Evaluated BCET</em>'.
	 * @see ucm_supplement.ucm_detailed_components.ComputationStep#getEvaluatedBCET()
	 * @see #getComputationStep()
	 * @generated
	 */
	EAttribute getComputationStep_EvaluatedBCET();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_detailed_components.ComputationWithEndingCommunicationStep <em>Computation With Ending Communication Step</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Computation With Ending Communication Step</em>'.
	 * @see ucm_supplement.ucm_detailed_components.ComputationWithEndingCommunicationStep
	 * @generated
	 */
	EClass getComputationWithEndingCommunicationStep();

	/**
	 * Returns the meta object for the containment reference '{@link ucm_supplement.ucm_detailed_components.ComputationWithEndingCommunicationStep#getPortAndMethodToCall <em>Port And Method To Call</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Port And Method To Call</em>'.
	 * @see ucm_supplement.ucm_detailed_components.ComputationWithEndingCommunicationStep#getPortAndMethodToCall()
	 * @see #getComputationWithEndingCommunicationStep()
	 * @generated
	 */
	EReference getComputationWithEndingCommunicationStep_PortAndMethodToCall();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_detailed_components.LoopStep <em>Loop Step</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Loop Step</em>'.
	 * @see ucm_supplement.ucm_detailed_components.LoopStep
	 * @generated
	 */
	EClass getLoopStep();

	/**
	 * Returns the meta object for the containment reference '{@link ucm_supplement.ucm_detailed_components.LoopStep#getStep <em>Step</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Step</em>'.
	 * @see ucm_supplement.ucm_detailed_components.LoopStep#getStep()
	 * @see #getLoopStep()
	 * @generated
	 */
	EReference getLoopStep_Step();

	/**
	 * Returns the meta object for the attribute '{@link ucm_supplement.ucm_detailed_components.LoopStep#getMinIterations <em>Min Iterations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min Iterations</em>'.
	 * @see ucm_supplement.ucm_detailed_components.LoopStep#getMinIterations()
	 * @see #getLoopStep()
	 * @generated
	 */
	EAttribute getLoopStep_MinIterations();

	/**
	 * Returns the meta object for the attribute '{@link ucm_supplement.ucm_detailed_components.LoopStep#getMaxIterations <em>Max Iterations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Iterations</em>'.
	 * @see ucm_supplement.ucm_detailed_components.LoopStep#getMaxIterations()
	 * @see #getLoopStep()
	 * @generated
	 */
	EAttribute getLoopStep_MaxIterations();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_detailed_components.AlternativeStep <em>Alternative Step</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Alternative Step</em>'.
	 * @see ucm_supplement.ucm_detailed_components.AlternativeStep
	 * @generated
	 */
	EClass getAlternativeStep();

	/**
	 * Returns the meta object for the reference list '{@link ucm_supplement.ucm_detailed_components.AlternativeStep#getStep <em>Step</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Step</em>'.
	 * @see ucm_supplement.ucm_detailed_components.AlternativeStep#getStep()
	 * @see #getAlternativeStep()
	 * @generated
	 */
	EReference getAlternativeStep_Step();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_detailed_components.SequenceStep <em>Sequence Step</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sequence Step</em>'.
	 * @see ucm_supplement.ucm_detailed_components.SequenceStep
	 * @generated
	 */
	EClass getSequenceStep();

	/**
	 * Returns the meta object for the containment reference list '{@link ucm_supplement.ucm_detailed_components.SequenceStep#getStep <em>Step</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Step</em>'.
	 * @see ucm_supplement.ucm_detailed_components.SequenceStep#getStep()
	 * @see #getSequenceStep()
	 * @generated
	 */
	EReference getSequenceStep_Step();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_detailed_components.ComponentMultiplexPortImplementation <em>Component Multiplex Port Implementation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component Multiplex Port Implementation</em>'.
	 * @see ucm_supplement.ucm_detailed_components.ComponentMultiplexPortImplementation
	 * @generated
	 */
	EClass getComponentMultiplexPortImplementation();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_detailed_components.ComponentMultiplexPortImplementation#getBindingName <em>Binding Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Binding Name</em>'.
	 * @see ucm_supplement.ucm_detailed_components.ComponentMultiplexPortImplementation#getBindingName()
	 * @see #getComponentMultiplexPortImplementation()
	 * @generated
	 */
	EReference getComponentMultiplexPortImplementation_BindingName();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	Ucm_detailed_componentsFactory getUcm_detailed_componentsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_detailed_components.impl.DetailedComponentImplementationImpl <em>Detailed Component Implementation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_detailed_components.impl.DetailedComponentImplementationImpl
		 * @see ucm_supplement.ucm_detailed_components.impl.Ucm_detailed_componentsPackageImpl#getDetailedComponentImplementation()
		 * @generated
		 */
		EClass DETAILED_COMPONENT_IMPLEMENTATION = eINSTANCE.getDetailedComponentImplementation();

		/**
		 * The meta object literal for the '<em><b>Port Implementation</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DETAILED_COMPONENT_IMPLEMENTATION__PORT_IMPLEMENTATION = eINSTANCE.getDetailedComponentImplementation_PortImplementation();

		/**
		 * The meta object literal for the '<em><b>Execution Scenario</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DETAILED_COMPONENT_IMPLEMENTATION__EXECUTION_SCENARIO = eINSTANCE.getDetailedComponentImplementation_ExecutionScenario();

		/**
		 * The meta object literal for the '<em><b>Extends</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DETAILED_COMPONENT_IMPLEMENTATION__EXTENDS = eINSTANCE.getDetailedComponentImplementation_Extends();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_detailed_components.impl.IPortElementImplementationImpl <em>IPort Element Implementation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_detailed_components.impl.IPortElementImplementationImpl
		 * @see ucm_supplement.ucm_detailed_components.impl.Ucm_detailed_componentsPackageImpl#getIPortElementImplementation()
		 * @generated
		 */
		EClass IPORT_ELEMENT_IMPLEMENTATION = eINSTANCE.getIPortElementImplementation();

		/**
		 * The meta object literal for the '<em><b>Port Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IPORT_ELEMENT_IMPLEMENTATION__PORT_ELEMENT = eINSTANCE.getIPortElementImplementation_PortElement();

		/**
		 * The meta object literal for the '<em><b>Intf</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IPORT_ELEMENT_IMPLEMENTATION__INTF = eINSTANCE.getIPortElementImplementation_Intf();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_detailed_components.impl.ComponentPortImplementationImpl <em>Component Port Implementation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_detailed_components.impl.ComponentPortImplementationImpl
		 * @see ucm_supplement.ucm_detailed_components.impl.Ucm_detailed_componentsPackageImpl#getComponentPortImplementation()
		 * @generated
		 */
		EClass COMPONENT_PORT_IMPLEMENTATION = eINSTANCE.getComponentPortImplementation();

		/**
		 * The meta object literal for the '<em><b>Port</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_PORT_IMPLEMENTATION__PORT = eINSTANCE.getComponentPortImplementation_Port();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_detailed_components.impl.TechnicalPortImplementationImpl <em>Technical Port Implementation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_detailed_components.impl.TechnicalPortImplementationImpl
		 * @see ucm_supplement.ucm_detailed_components.impl.Ucm_detailed_componentsPackageImpl#getTechnicalPortImplementation()
		 * @generated
		 */
		EClass TECHNICAL_PORT_IMPLEMENTATION = eINSTANCE.getTechnicalPortImplementation();

		/**
		 * The meta object literal for the '<em><b>Policy</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TECHNICAL_PORT_IMPLEMENTATION__POLICY = eINSTANCE.getTechnicalPortImplementation_Policy();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_detailed_components.impl.PortAndMethodImpl <em>Port And Method</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_detailed_components.impl.PortAndMethodImpl
		 * @see ucm_supplement.ucm_detailed_components.impl.Ucm_detailed_componentsPackageImpl#getPortAndMethod()
		 * @generated
		 */
		EClass PORT_AND_METHOD = eINSTANCE.getPortAndMethod();

		/**
		 * The meta object literal for the '<em><b>Method</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_AND_METHOD__METHOD = eINSTANCE.getPortAndMethod_Method();

		/**
		 * The meta object literal for the '<em><b>Port Implementation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_AND_METHOD__PORT_IMPLEMENTATION = eINSTANCE.getPortAndMethod_PortImplementation();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_detailed_components.impl.ExecutionScenarioImpl <em>Execution Scenario</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_detailed_components.impl.ExecutionScenarioImpl
		 * @see ucm_supplement.ucm_detailed_components.impl.Ucm_detailed_componentsPackageImpl#getExecutionScenario()
		 * @generated
		 */
		EClass EXECUTION_SCENARIO = eINSTANCE.getExecutionScenario();

		/**
		 * The meta object literal for the '<em><b>Port And Method</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXECUTION_SCENARIO__PORT_AND_METHOD = eINSTANCE.getExecutionScenario_PortAndMethod();

		/**
		 * The meta object literal for the '<em><b>Associated Step</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXECUTION_SCENARIO__ASSOCIATED_STEP = eINSTANCE.getExecutionScenario_AssociatedStep();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_detailed_components.impl.IExecutionStepImpl <em>IExecution Step</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_detailed_components.impl.IExecutionStepImpl
		 * @see ucm_supplement.ucm_detailed_components.impl.Ucm_detailed_componentsPackageImpl#getIExecutionStep()
		 * @generated
		 */
		EClass IEXECUTION_STEP = eINSTANCE.getIExecutionStep();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_detailed_components.impl.ComputationStepImpl <em>Computation Step</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_detailed_components.impl.ComputationStepImpl
		 * @see ucm_supplement.ucm_detailed_components.impl.Ucm_detailed_componentsPackageImpl#getComputationStep()
		 * @generated
		 */
		EClass COMPUTATION_STEP = eINSTANCE.getComputationStep();

		/**
		 * The meta object literal for the '<em><b>Specified WCET</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPUTATION_STEP__SPECIFIED_WCET = eINSTANCE.getComputationStep_SpecifiedWCET();

		/**
		 * The meta object literal for the '<em><b>Specified BCET</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPUTATION_STEP__SPECIFIED_BCET = eINSTANCE.getComputationStep_SpecifiedBCET();

		/**
		 * The meta object literal for the '<em><b>Measured WCET</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPUTATION_STEP__MEASURED_WCET = eINSTANCE.getComputationStep_MeasuredWCET();

		/**
		 * The meta object literal for the '<em><b>Measured BCET</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPUTATION_STEP__MEASURED_BCET = eINSTANCE.getComputationStep_MeasuredBCET();

		/**
		 * The meta object literal for the '<em><b>Evaluated WCET</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPUTATION_STEP__EVALUATED_WCET = eINSTANCE.getComputationStep_EvaluatedWCET();

		/**
		 * The meta object literal for the '<em><b>Evaluated BCET</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPUTATION_STEP__EVALUATED_BCET = eINSTANCE.getComputationStep_EvaluatedBCET();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_detailed_components.impl.ComputationWithEndingCommunicationStepImpl <em>Computation With Ending Communication Step</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_detailed_components.impl.ComputationWithEndingCommunicationStepImpl
		 * @see ucm_supplement.ucm_detailed_components.impl.Ucm_detailed_componentsPackageImpl#getComputationWithEndingCommunicationStep()
		 * @generated
		 */
		EClass COMPUTATION_WITH_ENDING_COMMUNICATION_STEP = eINSTANCE.getComputationWithEndingCommunicationStep();

		/**
		 * The meta object literal for the '<em><b>Port And Method To Call</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPUTATION_WITH_ENDING_COMMUNICATION_STEP__PORT_AND_METHOD_TO_CALL = eINSTANCE.getComputationWithEndingCommunicationStep_PortAndMethodToCall();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_detailed_components.impl.LoopStepImpl <em>Loop Step</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_detailed_components.impl.LoopStepImpl
		 * @see ucm_supplement.ucm_detailed_components.impl.Ucm_detailed_componentsPackageImpl#getLoopStep()
		 * @generated
		 */
		EClass LOOP_STEP = eINSTANCE.getLoopStep();

		/**
		 * The meta object literal for the '<em><b>Step</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOOP_STEP__STEP = eINSTANCE.getLoopStep_Step();

		/**
		 * The meta object literal for the '<em><b>Min Iterations</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOOP_STEP__MIN_ITERATIONS = eINSTANCE.getLoopStep_MinIterations();

		/**
		 * The meta object literal for the '<em><b>Max Iterations</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOOP_STEP__MAX_ITERATIONS = eINSTANCE.getLoopStep_MaxIterations();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_detailed_components.impl.AlternativeStepImpl <em>Alternative Step</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_detailed_components.impl.AlternativeStepImpl
		 * @see ucm_supplement.ucm_detailed_components.impl.Ucm_detailed_componentsPackageImpl#getAlternativeStep()
		 * @generated
		 */
		EClass ALTERNATIVE_STEP = eINSTANCE.getAlternativeStep();

		/**
		 * The meta object literal for the '<em><b>Step</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ALTERNATIVE_STEP__STEP = eINSTANCE.getAlternativeStep_Step();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_detailed_components.impl.SequenceStepImpl <em>Sequence Step</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_detailed_components.impl.SequenceStepImpl
		 * @see ucm_supplement.ucm_detailed_components.impl.Ucm_detailed_componentsPackageImpl#getSequenceStep()
		 * @generated
		 */
		EClass SEQUENCE_STEP = eINSTANCE.getSequenceStep();

		/**
		 * The meta object literal for the '<em><b>Step</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEQUENCE_STEP__STEP = eINSTANCE.getSequenceStep_Step();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_detailed_components.impl.ComponentMultiplexPortImplementationImpl <em>Component Multiplex Port Implementation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_detailed_components.impl.ComponentMultiplexPortImplementationImpl
		 * @see ucm_supplement.ucm_detailed_components.impl.Ucm_detailed_componentsPackageImpl#getComponentMultiplexPortImplementation()
		 * @generated
		 */
		EClass COMPONENT_MULTIPLEX_PORT_IMPLEMENTATION = eINSTANCE.getComponentMultiplexPortImplementation();

		/**
		 * The meta object literal for the '<em><b>Binding Name</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_MULTIPLEX_PORT_IMPLEMENTATION__BINDING_NAME = eINSTANCE.getComponentMultiplexPortImplementation_BindingName();

	}

} //Ucm_detailed_componentsPackage
