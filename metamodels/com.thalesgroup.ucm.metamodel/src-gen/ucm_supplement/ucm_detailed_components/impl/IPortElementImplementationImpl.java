/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_detailed_components.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import ucm_base.ucm_commons.IComment;
import ucm_base.ucm_commons.INamed;
import ucm_base.ucm_commons.Ucm_commonsPackage;

import ucm_base.ucm_contracts.Interface;
import ucm_base.ucm_contracts.impl.IAnnotableImpl;

import ucm_base.ucm_interactions.PortElement;

import ucm_supplement.ucm_detailed_components.IPortElementImplementation;
import ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>IPort Element Implementation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_detailed_components.impl.IPortElementImplementationImpl#getName <em>Name</em>}</li>
 *   <li>{@link ucm_supplement.ucm_detailed_components.impl.IPortElementImplementationImpl#getComment <em>Comment</em>}</li>
 *   <li>{@link ucm_supplement.ucm_detailed_components.impl.IPortElementImplementationImpl#getPortElement <em>Port Element</em>}</li>
 *   <li>{@link ucm_supplement.ucm_detailed_components.impl.IPortElementImplementationImpl#getIntf <em>Intf</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class IPortElementImplementationImpl extends IAnnotableImpl implements IPortElementImplementation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getComment() <em>Comment</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComment()
	 * @generated
	 * @ordered
	 */
	protected EList<IComment> comment;

	/**
	 * The cached value of the '{@link #getPortElement() <em>Port Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPortElement()
	 * @generated
	 * @ordered
	 */
	protected PortElement portElement;

	/**
	 * The cached value of the '{@link #getIntf() <em>Intf</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntf()
	 * @generated
	 * @ordered
	 */
	protected Interface intf;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IPortElementImplementationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_detailed_componentsPackage.Literals.IPORT_ELEMENT_IMPLEMENTATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_detailed_componentsPackage.IPORT_ELEMENT_IMPLEMENTATION__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IComment> getComment() {
		if (comment == null) {
			comment = new EObjectContainmentEList<IComment>(IComment.class, this, Ucm_detailed_componentsPackage.IPORT_ELEMENT_IMPLEMENTATION__COMMENT);
		}
		return comment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortElement getPortElement() {
		if (portElement != null && portElement.eIsProxy()) {
			InternalEObject oldPortElement = (InternalEObject)portElement;
			portElement = (PortElement)eResolveProxy(oldPortElement);
			if (portElement != oldPortElement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_detailed_componentsPackage.IPORT_ELEMENT_IMPLEMENTATION__PORT_ELEMENT, oldPortElement, portElement));
			}
		}
		return portElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortElement basicGetPortElement() {
		return portElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPortElement(PortElement newPortElement) {
		PortElement oldPortElement = portElement;
		portElement = newPortElement;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_detailed_componentsPackage.IPORT_ELEMENT_IMPLEMENTATION__PORT_ELEMENT, oldPortElement, portElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Interface getIntf() {
		if (intf != null && intf.eIsProxy()) {
			InternalEObject oldIntf = (InternalEObject)intf;
			intf = (Interface)eResolveProxy(oldIntf);
			if (intf != oldIntf) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_detailed_componentsPackage.IPORT_ELEMENT_IMPLEMENTATION__INTF, oldIntf, intf));
			}
		}
		return intf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Interface basicGetIntf() {
		return intf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIntf(Interface newIntf) {
		Interface oldIntf = intf;
		intf = newIntf;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_detailed_componentsPackage.IPORT_ELEMENT_IMPLEMENTATION__INTF, oldIntf, intf));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Ucm_detailed_componentsPackage.IPORT_ELEMENT_IMPLEMENTATION__COMMENT:
				return ((InternalEList<?>)getComment()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_detailed_componentsPackage.IPORT_ELEMENT_IMPLEMENTATION__NAME:
				return getName();
			case Ucm_detailed_componentsPackage.IPORT_ELEMENT_IMPLEMENTATION__COMMENT:
				return getComment();
			case Ucm_detailed_componentsPackage.IPORT_ELEMENT_IMPLEMENTATION__PORT_ELEMENT:
				if (resolve) return getPortElement();
				return basicGetPortElement();
			case Ucm_detailed_componentsPackage.IPORT_ELEMENT_IMPLEMENTATION__INTF:
				if (resolve) return getIntf();
				return basicGetIntf();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_detailed_componentsPackage.IPORT_ELEMENT_IMPLEMENTATION__NAME:
				setName((String)newValue);
				return;
			case Ucm_detailed_componentsPackage.IPORT_ELEMENT_IMPLEMENTATION__COMMENT:
				getComment().clear();
				getComment().addAll((Collection<? extends IComment>)newValue);
				return;
			case Ucm_detailed_componentsPackage.IPORT_ELEMENT_IMPLEMENTATION__PORT_ELEMENT:
				setPortElement((PortElement)newValue);
				return;
			case Ucm_detailed_componentsPackage.IPORT_ELEMENT_IMPLEMENTATION__INTF:
				setIntf((Interface)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_detailed_componentsPackage.IPORT_ELEMENT_IMPLEMENTATION__NAME:
				setName(NAME_EDEFAULT);
				return;
			case Ucm_detailed_componentsPackage.IPORT_ELEMENT_IMPLEMENTATION__COMMENT:
				getComment().clear();
				return;
			case Ucm_detailed_componentsPackage.IPORT_ELEMENT_IMPLEMENTATION__PORT_ELEMENT:
				setPortElement((PortElement)null);
				return;
			case Ucm_detailed_componentsPackage.IPORT_ELEMENT_IMPLEMENTATION__INTF:
				setIntf((Interface)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_detailed_componentsPackage.IPORT_ELEMENT_IMPLEMENTATION__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case Ucm_detailed_componentsPackage.IPORT_ELEMENT_IMPLEMENTATION__COMMENT:
				return comment != null && !comment.isEmpty();
			case Ucm_detailed_componentsPackage.IPORT_ELEMENT_IMPLEMENTATION__PORT_ELEMENT:
				return portElement != null;
			case Ucm_detailed_componentsPackage.IPORT_ELEMENT_IMPLEMENTATION__INTF:
				return intf != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == INamed.class) {
			switch (derivedFeatureID) {
				case Ucm_detailed_componentsPackage.IPORT_ELEMENT_IMPLEMENTATION__NAME: return Ucm_commonsPackage.INAMED__NAME;
				case Ucm_detailed_componentsPackage.IPORT_ELEMENT_IMPLEMENTATION__COMMENT: return Ucm_commonsPackage.INAMED__COMMENT;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == INamed.class) {
			switch (baseFeatureID) {
				case Ucm_commonsPackage.INAMED__NAME: return Ucm_detailed_componentsPackage.IPORT_ELEMENT_IMPLEMENTATION__NAME;
				case Ucm_commonsPackage.INAMED__COMMENT: return Ucm_detailed_componentsPackage.IPORT_ELEMENT_IMPLEMENTATION__COMMENT;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //IPortElementImplementationImpl
