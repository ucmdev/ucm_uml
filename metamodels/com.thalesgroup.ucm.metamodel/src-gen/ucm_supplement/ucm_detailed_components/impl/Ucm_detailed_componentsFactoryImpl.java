/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_detailed_components.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import ucm_supplement.ucm_detailed_components.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Ucm_detailed_componentsFactoryImpl extends EFactoryImpl implements Ucm_detailed_componentsFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Ucm_detailed_componentsFactory init() {
		try {
			Ucm_detailed_componentsFactory theUcm_detailed_componentsFactory = (Ucm_detailed_componentsFactory)EPackage.Registry.INSTANCE.getEFactory(Ucm_detailed_componentsPackage.eNS_URI);
			if (theUcm_detailed_componentsFactory != null) {
				return theUcm_detailed_componentsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new Ucm_detailed_componentsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ucm_detailed_componentsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case Ucm_detailed_componentsPackage.DETAILED_COMPONENT_IMPLEMENTATION: return createDetailedComponentImplementation();
			case Ucm_detailed_componentsPackage.COMPONENT_PORT_IMPLEMENTATION: return createComponentPortImplementation();
			case Ucm_detailed_componentsPackage.TECHNICAL_PORT_IMPLEMENTATION: return createTechnicalPortImplementation();
			case Ucm_detailed_componentsPackage.PORT_AND_METHOD: return createPortAndMethod();
			case Ucm_detailed_componentsPackage.EXECUTION_SCENARIO: return createExecutionScenario();
			case Ucm_detailed_componentsPackage.COMPUTATION_STEP: return createComputationStep();
			case Ucm_detailed_componentsPackage.COMPUTATION_WITH_ENDING_COMMUNICATION_STEP: return createComputationWithEndingCommunicationStep();
			case Ucm_detailed_componentsPackage.LOOP_STEP: return createLoopStep();
			case Ucm_detailed_componentsPackage.ALTERNATIVE_STEP: return createAlternativeStep();
			case Ucm_detailed_componentsPackage.SEQUENCE_STEP: return createSequenceStep();
			case Ucm_detailed_componentsPackage.COMPONENT_MULTIPLEX_PORT_IMPLEMENTATION: return createComponentMultiplexPortImplementation();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DetailedComponentImplementation createDetailedComponentImplementation() {
		DetailedComponentImplementationImpl detailedComponentImplementation = new DetailedComponentImplementationImpl();
		return detailedComponentImplementation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentPortImplementation createComponentPortImplementation() {
		ComponentPortImplementationImpl componentPortImplementation = new ComponentPortImplementationImpl();
		return componentPortImplementation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TechnicalPortImplementation createTechnicalPortImplementation() {
		TechnicalPortImplementationImpl technicalPortImplementation = new TechnicalPortImplementationImpl();
		return technicalPortImplementation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortAndMethod createPortAndMethod() {
		PortAndMethodImpl portAndMethod = new PortAndMethodImpl();
		return portAndMethod;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExecutionScenario createExecutionScenario() {
		ExecutionScenarioImpl executionScenario = new ExecutionScenarioImpl();
		return executionScenario;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComputationStep createComputationStep() {
		ComputationStepImpl computationStep = new ComputationStepImpl();
		return computationStep;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComputationWithEndingCommunicationStep createComputationWithEndingCommunicationStep() {
		ComputationWithEndingCommunicationStepImpl computationWithEndingCommunicationStep = new ComputationWithEndingCommunicationStepImpl();
		return computationWithEndingCommunicationStep;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LoopStep createLoopStep() {
		LoopStepImpl loopStep = new LoopStepImpl();
		return loopStep;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AlternativeStep createAlternativeStep() {
		AlternativeStepImpl alternativeStep = new AlternativeStepImpl();
		return alternativeStep;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SequenceStep createSequenceStep() {
		SequenceStepImpl sequenceStep = new SequenceStepImpl();
		return sequenceStep;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentMultiplexPortImplementation createComponentMultiplexPortImplementation() {
		ComponentMultiplexPortImplementationImpl componentMultiplexPortImplementation = new ComponentMultiplexPortImplementationImpl();
		return componentMultiplexPortImplementation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ucm_detailed_componentsPackage getUcm_detailed_componentsPackage() {
		return (Ucm_detailed_componentsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static Ucm_detailed_componentsPackage getPackage() {
		return Ucm_detailed_componentsPackage.eINSTANCE;
	}

} //Ucm_detailed_componentsFactoryImpl
