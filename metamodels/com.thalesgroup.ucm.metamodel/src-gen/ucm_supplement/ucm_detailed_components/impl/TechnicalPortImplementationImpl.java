/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_detailed_components.impl;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import com.thalesgroup.ucm.metamodel.validation.UcmSupplementValidationFactory;

import ucm_base.ucm_components.ComponentTechnicalPolicy;
import ucm_supplement.ucm_detailed_components.TechnicalPortImplementation;
import ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>Technical Port Implementation</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_detailed_components.impl.TechnicalPortImplementationImpl#getPolicy <em>Policy</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TechnicalPortImplementationImpl extends IPortElementImplementationImpl
		implements TechnicalPortImplementation {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached value of the '{@link #getPolicy() <em>Policy</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getPolicy()
	 * @generated
	 * @ordered
	 */
	protected ComponentTechnicalPolicy policy;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected TechnicalPortImplementationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_detailed_componentsPackage.Literals.TECHNICAL_PORT_IMPLEMENTATION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentTechnicalPolicy getPolicy() {
		if (policy != null && policy.eIsProxy()) {
			InternalEObject oldPolicy = (InternalEObject)policy;
			policy = (ComponentTechnicalPolicy)eResolveProxy(oldPolicy);
			if (policy != oldPolicy) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_detailed_componentsPackage.TECHNICAL_PORT_IMPLEMENTATION__POLICY, oldPolicy, policy));
			}
		}
		return policy;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentTechnicalPolicy basicGetPolicy() {
		return policy;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setPolicy(ComponentTechnicalPolicy newPolicy) {
		ComponentTechnicalPolicy oldPolicy = policy;
		policy = newPolicy;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_detailed_componentsPackage.TECHNICAL_PORT_IMPLEMENTATION__POLICY, oldPolicy, policy));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean portImplementationConsistency(DiagnosticChain diagnostics, Map<Object, Object> context) {
		return UcmSupplementValidationFactory.INSTANCE.getDetailedComponentValidator()
				.portImplementationConsistency(this, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_detailed_componentsPackage.TECHNICAL_PORT_IMPLEMENTATION__POLICY:
				if (resolve) return getPolicy();
				return basicGetPolicy();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_detailed_componentsPackage.TECHNICAL_PORT_IMPLEMENTATION__POLICY:
				setPolicy((ComponentTechnicalPolicy)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_detailed_componentsPackage.TECHNICAL_PORT_IMPLEMENTATION__POLICY:
				setPolicy((ComponentTechnicalPolicy)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_detailed_componentsPackage.TECHNICAL_PORT_IMPLEMENTATION__POLICY:
				return policy != null;
		}
		return super.eIsSet(featureID);
	}

} // TechnicalPortImplementationImpl
