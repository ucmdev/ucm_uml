/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_detailed_components.impl;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import com.thalesgroup.ucm.metamodel.validation.UcmSupplementValidationFactory;

import ucm_supplement.ucm_detailed_components.ComputationStep;
import ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>Computation Step</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_detailed_components.impl.ComputationStepImpl#getSpecifiedWCET <em>Specified WCET</em>}</li>
 *   <li>{@link ucm_supplement.ucm_detailed_components.impl.ComputationStepImpl#getSpecifiedBCET <em>Specified BCET</em>}</li>
 *   <li>{@link ucm_supplement.ucm_detailed_components.impl.ComputationStepImpl#getMeasuredWCET <em>Measured WCET</em>}</li>
 *   <li>{@link ucm_supplement.ucm_detailed_components.impl.ComputationStepImpl#getMeasuredBCET <em>Measured BCET</em>}</li>
 *   <li>{@link ucm_supplement.ucm_detailed_components.impl.ComputationStepImpl#getEvaluatedWCET <em>Evaluated WCET</em>}</li>
 *   <li>{@link ucm_supplement.ucm_detailed_components.impl.ComputationStepImpl#getEvaluatedBCET <em>Evaluated BCET</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComputationStepImpl extends IExecutionStepImpl implements ComputationStep {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The default value of the '{@link #getSpecifiedWCET() <em>Specified WCET</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getSpecifiedWCET()
	 * @generated
	 * @ordered
	 */
	protected static final double SPECIFIED_WCET_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getSpecifiedWCET() <em>Specified WCET</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getSpecifiedWCET()
	 * @generated
	 * @ordered
	 */
	protected double specifiedWCET = SPECIFIED_WCET_EDEFAULT;

	/**
	 * The default value of the '{@link #getSpecifiedBCET() <em>Specified BCET</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getSpecifiedBCET()
	 * @generated
	 * @ordered
	 */
	protected static final double SPECIFIED_BCET_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getSpecifiedBCET() <em>Specified BCET</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getSpecifiedBCET()
	 * @generated
	 * @ordered
	 */
	protected double specifiedBCET = SPECIFIED_BCET_EDEFAULT;

	/**
	 * The default value of the '{@link #getMeasuredWCET() <em>Measured WCET</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getMeasuredWCET()
	 * @generated
	 * @ordered
	 */
	protected static final double MEASURED_WCET_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getMeasuredWCET() <em>Measured WCET</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getMeasuredWCET()
	 * @generated
	 * @ordered
	 */
	protected double measuredWCET = MEASURED_WCET_EDEFAULT;

	/**
	 * The default value of the '{@link #getMeasuredBCET() <em>Measured BCET</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getMeasuredBCET()
	 * @generated
	 * @ordered
	 */
	protected static final double MEASURED_BCET_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getMeasuredBCET() <em>Measured BCET</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getMeasuredBCET()
	 * @generated
	 * @ordered
	 */
	protected double measuredBCET = MEASURED_BCET_EDEFAULT;

	/**
	 * The default value of the '{@link #getEvaluatedWCET() <em>Evaluated WCET</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getEvaluatedWCET()
	 * @generated
	 * @ordered
	 */
	protected static final double EVALUATED_WCET_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getEvaluatedWCET() <em>Evaluated WCET</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getEvaluatedWCET()
	 * @generated
	 * @ordered
	 */
	protected double evaluatedWCET = EVALUATED_WCET_EDEFAULT;

	/**
	 * The default value of the '{@link #getEvaluatedBCET() <em>Evaluated BCET</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getEvaluatedBCET()
	 * @generated
	 * @ordered
	 */
	protected static final double EVALUATED_BCET_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getEvaluatedBCET() <em>Evaluated BCET</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getEvaluatedBCET()
	 * @generated
	 * @ordered
	 */
	protected double evaluatedBCET = EVALUATED_BCET_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected ComputationStepImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_detailed_componentsPackage.Literals.COMPUTATION_STEP;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public double getSpecifiedWCET() {
		return specifiedWCET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSpecifiedWCET(double newSpecifiedWCET) {
		double oldSpecifiedWCET = specifiedWCET;
		specifiedWCET = newSpecifiedWCET;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_detailed_componentsPackage.COMPUTATION_STEP__SPECIFIED_WCET, oldSpecifiedWCET, specifiedWCET));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public double getSpecifiedBCET() {
		return specifiedBCET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSpecifiedBCET(double newSpecifiedBCET) {
		double oldSpecifiedBCET = specifiedBCET;
		specifiedBCET = newSpecifiedBCET;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_detailed_componentsPackage.COMPUTATION_STEP__SPECIFIED_BCET, oldSpecifiedBCET, specifiedBCET));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public double getMeasuredWCET() {
		return measuredWCET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMeasuredWCET(double newMeasuredWCET) {
		double oldMeasuredWCET = measuredWCET;
		measuredWCET = newMeasuredWCET;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_detailed_componentsPackage.COMPUTATION_STEP__MEASURED_WCET, oldMeasuredWCET, measuredWCET));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public double getMeasuredBCET() {
		return measuredBCET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMeasuredBCET(double newMeasuredBCET) {
		double oldMeasuredBCET = measuredBCET;
		measuredBCET = newMeasuredBCET;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_detailed_componentsPackage.COMPUTATION_STEP__MEASURED_BCET, oldMeasuredBCET, measuredBCET));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public double getEvaluatedWCET() {
		return evaluatedWCET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEvaluatedWCET(double newEvaluatedWCET) {
		double oldEvaluatedWCET = evaluatedWCET;
		evaluatedWCET = newEvaluatedWCET;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_detailed_componentsPackage.COMPUTATION_STEP__EVALUATED_WCET, oldEvaluatedWCET, evaluatedWCET));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public double getEvaluatedBCET() {
		return evaluatedBCET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEvaluatedBCET(double newEvaluatedBCET) {
		double oldEvaluatedBCET = evaluatedBCET;
		evaluatedBCET = newEvaluatedBCET;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_detailed_componentsPackage.COMPUTATION_STEP__EVALUATED_BCET, oldEvaluatedBCET, evaluatedBCET));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean valueConsistency(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and
		// message
		// Ensure that you remove @generated or mark it @generated NOT
		return UcmSupplementValidationFactory.INSTANCE.getDetailedComponentValidator().valueConsistency(this,
				diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_detailed_componentsPackage.COMPUTATION_STEP__SPECIFIED_WCET:
				return getSpecifiedWCET();
			case Ucm_detailed_componentsPackage.COMPUTATION_STEP__SPECIFIED_BCET:
				return getSpecifiedBCET();
			case Ucm_detailed_componentsPackage.COMPUTATION_STEP__MEASURED_WCET:
				return getMeasuredWCET();
			case Ucm_detailed_componentsPackage.COMPUTATION_STEP__MEASURED_BCET:
				return getMeasuredBCET();
			case Ucm_detailed_componentsPackage.COMPUTATION_STEP__EVALUATED_WCET:
				return getEvaluatedWCET();
			case Ucm_detailed_componentsPackage.COMPUTATION_STEP__EVALUATED_BCET:
				return getEvaluatedBCET();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_detailed_componentsPackage.COMPUTATION_STEP__SPECIFIED_WCET:
				setSpecifiedWCET((Double)newValue);
				return;
			case Ucm_detailed_componentsPackage.COMPUTATION_STEP__SPECIFIED_BCET:
				setSpecifiedBCET((Double)newValue);
				return;
			case Ucm_detailed_componentsPackage.COMPUTATION_STEP__MEASURED_WCET:
				setMeasuredWCET((Double)newValue);
				return;
			case Ucm_detailed_componentsPackage.COMPUTATION_STEP__MEASURED_BCET:
				setMeasuredBCET((Double)newValue);
				return;
			case Ucm_detailed_componentsPackage.COMPUTATION_STEP__EVALUATED_WCET:
				setEvaluatedWCET((Double)newValue);
				return;
			case Ucm_detailed_componentsPackage.COMPUTATION_STEP__EVALUATED_BCET:
				setEvaluatedBCET((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_detailed_componentsPackage.COMPUTATION_STEP__SPECIFIED_WCET:
				setSpecifiedWCET(SPECIFIED_WCET_EDEFAULT);
				return;
			case Ucm_detailed_componentsPackage.COMPUTATION_STEP__SPECIFIED_BCET:
				setSpecifiedBCET(SPECIFIED_BCET_EDEFAULT);
				return;
			case Ucm_detailed_componentsPackage.COMPUTATION_STEP__MEASURED_WCET:
				setMeasuredWCET(MEASURED_WCET_EDEFAULT);
				return;
			case Ucm_detailed_componentsPackage.COMPUTATION_STEP__MEASURED_BCET:
				setMeasuredBCET(MEASURED_BCET_EDEFAULT);
				return;
			case Ucm_detailed_componentsPackage.COMPUTATION_STEP__EVALUATED_WCET:
				setEvaluatedWCET(EVALUATED_WCET_EDEFAULT);
				return;
			case Ucm_detailed_componentsPackage.COMPUTATION_STEP__EVALUATED_BCET:
				setEvaluatedBCET(EVALUATED_BCET_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_detailed_componentsPackage.COMPUTATION_STEP__SPECIFIED_WCET:
				return specifiedWCET != SPECIFIED_WCET_EDEFAULT;
			case Ucm_detailed_componentsPackage.COMPUTATION_STEP__SPECIFIED_BCET:
				return specifiedBCET != SPECIFIED_BCET_EDEFAULT;
			case Ucm_detailed_componentsPackage.COMPUTATION_STEP__MEASURED_WCET:
				return measuredWCET != MEASURED_WCET_EDEFAULT;
			case Ucm_detailed_componentsPackage.COMPUTATION_STEP__MEASURED_BCET:
				return measuredBCET != MEASURED_BCET_EDEFAULT;
			case Ucm_detailed_componentsPackage.COMPUTATION_STEP__EVALUATED_WCET:
				return evaluatedWCET != EVALUATED_WCET_EDEFAULT;
			case Ucm_detailed_componentsPackage.COMPUTATION_STEP__EVALUATED_BCET:
				return evaluatedBCET != EVALUATED_BCET_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (specifiedWCET: ");
		result.append(specifiedWCET);
		result.append(", specifiedBCET: ");
		result.append(specifiedBCET);
		result.append(", measuredWCET: ");
		result.append(measuredWCET);
		result.append(", measuredBCET: ");
		result.append(measuredBCET);
		result.append(", evaluatedWCET: ");
		result.append(evaluatedWCET);
		result.append(", evaluatedBCET: ");
		result.append(evaluatedBCET);
		result.append(')');
		return result.toString();
	}

} // ComputationStepImpl
