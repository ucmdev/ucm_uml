/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_detailed_components.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import ucm_supplement.ucm_detailed_components.ComputationWithCommunicationsStep;
import ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Computation With Communications Step</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_detailed_components.impl.ComputationWithCommunicationsStepImpl#getMinNumberOfCalls <em>Min Number Of Calls</em>}</li>
 *   <li>{@link ucm_supplement.ucm_detailed_components.impl.ComputationWithCommunicationsStepImpl#getMaxNumberOfCalls <em>Max Number Of Calls</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComputationWithCommunicationsStepImpl extends ComputationWithEndingCommunicationStepImpl implements ComputationWithCommunicationsStep {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The default value of the '{@link #getMinNumberOfCalls() <em>Min Number Of Calls</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinNumberOfCalls()
	 * @generated
	 * @ordered
	 */
	protected static final long MIN_NUMBER_OF_CALLS_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getMinNumberOfCalls() <em>Min Number Of Calls</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinNumberOfCalls()
	 * @generated
	 * @ordered
	 */
	protected long minNumberOfCalls = MIN_NUMBER_OF_CALLS_EDEFAULT;

	/**
	 * The default value of the '{@link #getMaxNumberOfCalls() <em>Max Number Of Calls</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxNumberOfCalls()
	 * @generated
	 * @ordered
	 */
	protected static final long MAX_NUMBER_OF_CALLS_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getMaxNumberOfCalls() <em>Max Number Of Calls</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxNumberOfCalls()
	 * @generated
	 * @ordered
	 */
	protected long maxNumberOfCalls = MAX_NUMBER_OF_CALLS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComputationWithCommunicationsStepImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_detailed_componentsPackage.Literals.COMPUTATION_WITH_COMMUNICATIONS_STEP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getMinNumberOfCalls() {
		return minNumberOfCalls;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMinNumberOfCalls(long newMinNumberOfCalls) {
		long oldMinNumberOfCalls = minNumberOfCalls;
		minNumberOfCalls = newMinNumberOfCalls;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_detailed_componentsPackage.COMPUTATION_WITH_COMMUNICATIONS_STEP__MIN_NUMBER_OF_CALLS, oldMinNumberOfCalls, minNumberOfCalls));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getMaxNumberOfCalls() {
		return maxNumberOfCalls;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaxNumberOfCalls(long newMaxNumberOfCalls) {
		long oldMaxNumberOfCalls = maxNumberOfCalls;
		maxNumberOfCalls = newMaxNumberOfCalls;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_detailed_componentsPackage.COMPUTATION_WITH_COMMUNICATIONS_STEP__MAX_NUMBER_OF_CALLS, oldMaxNumberOfCalls, maxNumberOfCalls));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_detailed_componentsPackage.COMPUTATION_WITH_COMMUNICATIONS_STEP__MIN_NUMBER_OF_CALLS:
				return getMinNumberOfCalls();
			case Ucm_detailed_componentsPackage.COMPUTATION_WITH_COMMUNICATIONS_STEP__MAX_NUMBER_OF_CALLS:
				return getMaxNumberOfCalls();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_detailed_componentsPackage.COMPUTATION_WITH_COMMUNICATIONS_STEP__MIN_NUMBER_OF_CALLS:
				setMinNumberOfCalls((Long)newValue);
				return;
			case Ucm_detailed_componentsPackage.COMPUTATION_WITH_COMMUNICATIONS_STEP__MAX_NUMBER_OF_CALLS:
				setMaxNumberOfCalls((Long)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_detailed_componentsPackage.COMPUTATION_WITH_COMMUNICATIONS_STEP__MIN_NUMBER_OF_CALLS:
				setMinNumberOfCalls(MIN_NUMBER_OF_CALLS_EDEFAULT);
				return;
			case Ucm_detailed_componentsPackage.COMPUTATION_WITH_COMMUNICATIONS_STEP__MAX_NUMBER_OF_CALLS:
				setMaxNumberOfCalls(MAX_NUMBER_OF_CALLS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_detailed_componentsPackage.COMPUTATION_WITH_COMMUNICATIONS_STEP__MIN_NUMBER_OF_CALLS:
				return minNumberOfCalls != MIN_NUMBER_OF_CALLS_EDEFAULT;
			case Ucm_detailed_componentsPackage.COMPUTATION_WITH_COMMUNICATIONS_STEP__MAX_NUMBER_OF_CALLS:
				return maxNumberOfCalls != MAX_NUMBER_OF_CALLS_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (minNumberOfCalls: ");
		result.append(minNumberOfCalls);
		result.append(", maxNumberOfCalls: ");
		result.append(maxNumberOfCalls);
		result.append(')');
		return result.toString();
	}

} //ComputationWithCommunicationsStepImpl
