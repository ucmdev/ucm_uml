/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_detailed_components.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import ucm_supplement.ucm_detailed_components.ComputationWithEndingCommunicationStep;
import ucm_supplement.ucm_detailed_components.PortAndMethod;
import ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Computation With Ending Communication Step</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_detailed_components.impl.ComputationWithEndingCommunicationStepImpl#getPortAndMethodToCall <em>Port And Method To Call</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComputationWithEndingCommunicationStepImpl extends ComputationStepImpl implements ComputationWithEndingCommunicationStep {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached value of the '{@link #getPortAndMethodToCall() <em>Port And Method To Call</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPortAndMethodToCall()
	 * @generated
	 * @ordered
	 */
	protected PortAndMethod portAndMethodToCall;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComputationWithEndingCommunicationStepImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_detailed_componentsPackage.Literals.COMPUTATION_WITH_ENDING_COMMUNICATION_STEP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortAndMethod getPortAndMethodToCall() {
		return portAndMethodToCall;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPortAndMethodToCall(PortAndMethod newPortAndMethodToCall, NotificationChain msgs) {
		PortAndMethod oldPortAndMethodToCall = portAndMethodToCall;
		portAndMethodToCall = newPortAndMethodToCall;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Ucm_detailed_componentsPackage.COMPUTATION_WITH_ENDING_COMMUNICATION_STEP__PORT_AND_METHOD_TO_CALL, oldPortAndMethodToCall, newPortAndMethodToCall);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPortAndMethodToCall(PortAndMethod newPortAndMethodToCall) {
		if (newPortAndMethodToCall != portAndMethodToCall) {
			NotificationChain msgs = null;
			if (portAndMethodToCall != null)
				msgs = ((InternalEObject)portAndMethodToCall).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - Ucm_detailed_componentsPackage.COMPUTATION_WITH_ENDING_COMMUNICATION_STEP__PORT_AND_METHOD_TO_CALL, null, msgs);
			if (newPortAndMethodToCall != null)
				msgs = ((InternalEObject)newPortAndMethodToCall).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - Ucm_detailed_componentsPackage.COMPUTATION_WITH_ENDING_COMMUNICATION_STEP__PORT_AND_METHOD_TO_CALL, null, msgs);
			msgs = basicSetPortAndMethodToCall(newPortAndMethodToCall, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_detailed_componentsPackage.COMPUTATION_WITH_ENDING_COMMUNICATION_STEP__PORT_AND_METHOD_TO_CALL, newPortAndMethodToCall, newPortAndMethodToCall));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Ucm_detailed_componentsPackage.COMPUTATION_WITH_ENDING_COMMUNICATION_STEP__PORT_AND_METHOD_TO_CALL:
				return basicSetPortAndMethodToCall(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_detailed_componentsPackage.COMPUTATION_WITH_ENDING_COMMUNICATION_STEP__PORT_AND_METHOD_TO_CALL:
				return getPortAndMethodToCall();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_detailed_componentsPackage.COMPUTATION_WITH_ENDING_COMMUNICATION_STEP__PORT_AND_METHOD_TO_CALL:
				setPortAndMethodToCall((PortAndMethod)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_detailed_componentsPackage.COMPUTATION_WITH_ENDING_COMMUNICATION_STEP__PORT_AND_METHOD_TO_CALL:
				setPortAndMethodToCall((PortAndMethod)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_detailed_componentsPackage.COMPUTATION_WITH_ENDING_COMMUNICATION_STEP__PORT_AND_METHOD_TO_CALL:
				return portAndMethodToCall != null;
		}
		return super.eIsSet(featureID);
	}

} //ComputationWithEndingCommunicationStepImpl
