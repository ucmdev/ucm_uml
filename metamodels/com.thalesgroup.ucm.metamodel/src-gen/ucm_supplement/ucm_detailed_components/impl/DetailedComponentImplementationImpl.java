/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_detailed_components.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import ucm_base.ucm_components.impl.AtomicComponentImplementationImpl;

import ucm_supplement.ucm_detailed_components.DetailedComponentImplementation;
import ucm_supplement.ucm_detailed_components.ExecutionScenario;
import ucm_supplement.ucm_detailed_components.IPortElementImplementation;
import ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Detailed Component Implementation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_detailed_components.impl.DetailedComponentImplementationImpl#getPortImplementation <em>Port Implementation</em>}</li>
 *   <li>{@link ucm_supplement.ucm_detailed_components.impl.DetailedComponentImplementationImpl#getExecutionScenario <em>Execution Scenario</em>}</li>
 *   <li>{@link ucm_supplement.ucm_detailed_components.impl.DetailedComponentImplementationImpl#getExtends <em>Extends</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DetailedComponentImplementationImpl extends AtomicComponentImplementationImpl implements DetailedComponentImplementation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached value of the '{@link #getPortImplementation() <em>Port Implementation</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPortImplementation()
	 * @generated
	 * @ordered
	 */
	protected EList<IPortElementImplementation> portImplementation;

	/**
	 * The cached value of the '{@link #getExecutionScenario() <em>Execution Scenario</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutionScenario()
	 * @generated
	 * @ordered
	 */
	protected EList<ExecutionScenario> executionScenario;

	/**
	 * The cached value of the '{@link #getExtends() <em>Extends</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtends()
	 * @generated
	 * @ordered
	 */
	protected DetailedComponentImplementation extends_;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DetailedComponentImplementationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_detailed_componentsPackage.Literals.DETAILED_COMPONENT_IMPLEMENTATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IPortElementImplementation> getPortImplementation() {
		if (portImplementation == null) {
			portImplementation = new EObjectContainmentEList<IPortElementImplementation>(IPortElementImplementation.class, this, Ucm_detailed_componentsPackage.DETAILED_COMPONENT_IMPLEMENTATION__PORT_IMPLEMENTATION);
		}
		return portImplementation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExecutionScenario> getExecutionScenario() {
		if (executionScenario == null) {
			executionScenario = new EObjectContainmentEList<ExecutionScenario>(ExecutionScenario.class, this, Ucm_detailed_componentsPackage.DETAILED_COMPONENT_IMPLEMENTATION__EXECUTION_SCENARIO);
		}
		return executionScenario;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DetailedComponentImplementation getExtends() {
		if (extends_ != null && extends_.eIsProxy()) {
			InternalEObject oldExtends = (InternalEObject)extends_;
			extends_ = (DetailedComponentImplementation)eResolveProxy(oldExtends);
			if (extends_ != oldExtends) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_detailed_componentsPackage.DETAILED_COMPONENT_IMPLEMENTATION__EXTENDS, oldExtends, extends_));
			}
		}
		return extends_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DetailedComponentImplementation basicGetExtends() {
		return extends_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExtends(DetailedComponentImplementation newExtends) {
		DetailedComponentImplementation oldExtends = extends_;
		extends_ = newExtends;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_detailed_componentsPackage.DETAILED_COMPONENT_IMPLEMENTATION__EXTENDS, oldExtends, extends_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Ucm_detailed_componentsPackage.DETAILED_COMPONENT_IMPLEMENTATION__PORT_IMPLEMENTATION:
				return ((InternalEList<?>)getPortImplementation()).basicRemove(otherEnd, msgs);
			case Ucm_detailed_componentsPackage.DETAILED_COMPONENT_IMPLEMENTATION__EXECUTION_SCENARIO:
				return ((InternalEList<?>)getExecutionScenario()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_detailed_componentsPackage.DETAILED_COMPONENT_IMPLEMENTATION__PORT_IMPLEMENTATION:
				return getPortImplementation();
			case Ucm_detailed_componentsPackage.DETAILED_COMPONENT_IMPLEMENTATION__EXECUTION_SCENARIO:
				return getExecutionScenario();
			case Ucm_detailed_componentsPackage.DETAILED_COMPONENT_IMPLEMENTATION__EXTENDS:
				if (resolve) return getExtends();
				return basicGetExtends();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_detailed_componentsPackage.DETAILED_COMPONENT_IMPLEMENTATION__PORT_IMPLEMENTATION:
				getPortImplementation().clear();
				getPortImplementation().addAll((Collection<? extends IPortElementImplementation>)newValue);
				return;
			case Ucm_detailed_componentsPackage.DETAILED_COMPONENT_IMPLEMENTATION__EXECUTION_SCENARIO:
				getExecutionScenario().clear();
				getExecutionScenario().addAll((Collection<? extends ExecutionScenario>)newValue);
				return;
			case Ucm_detailed_componentsPackage.DETAILED_COMPONENT_IMPLEMENTATION__EXTENDS:
				setExtends((DetailedComponentImplementation)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_detailed_componentsPackage.DETAILED_COMPONENT_IMPLEMENTATION__PORT_IMPLEMENTATION:
				getPortImplementation().clear();
				return;
			case Ucm_detailed_componentsPackage.DETAILED_COMPONENT_IMPLEMENTATION__EXECUTION_SCENARIO:
				getExecutionScenario().clear();
				return;
			case Ucm_detailed_componentsPackage.DETAILED_COMPONENT_IMPLEMENTATION__EXTENDS:
				setExtends((DetailedComponentImplementation)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_detailed_componentsPackage.DETAILED_COMPONENT_IMPLEMENTATION__PORT_IMPLEMENTATION:
				return portImplementation != null && !portImplementation.isEmpty();
			case Ucm_detailed_componentsPackage.DETAILED_COMPONENT_IMPLEMENTATION__EXECUTION_SCENARIO:
				return executionScenario != null && !executionScenario.isEmpty();
			case Ucm_detailed_componentsPackage.DETAILED_COMPONENT_IMPLEMENTATION__EXTENDS:
				return extends_ != null;
		}
		return super.eIsSet(featureID);
	}

} //DetailedComponentImplementationImpl
