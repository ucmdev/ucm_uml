/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_detailed_components.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import ucm_base.ucm_commons.Ucm_commonsPackage;

import ucm_base.ucm_components.Ucm_componentsPackage;

import ucm_base.ucm_contracts.Ucm_contractsPackage;

import ucm_base.ucm_contracts.ucm_datatypes.Ucm_datatypesPackage;

import ucm_base.ucm_interactions.Ucm_interactionsPackage;

import ucm_base.ucm_technicalpolicies.Ucm_technicalpoliciesPackage;

import ucm_basic_projects.Ucm_basic_projectsPackage;

import ucm_environment.ucm_resources.Ucm_resourcesPackage;

import ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl;

import ucm_extended_projects.Ucm_extended_projectsPackage;

import ucm_extended_projects.impl.Ucm_extended_projectsPackageImpl;

import ucm_supplement.ucm_deployments.Ucm_deploymentsPackage;

import ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl;

import ucm_supplement.ucm_detailed_components.AlternativeStep;
import ucm_supplement.ucm_detailed_components.ComponentMultiplexPortImplementation;
import ucm_supplement.ucm_detailed_components.ComponentPortImplementation;
import ucm_supplement.ucm_detailed_components.ComputationStep;
import ucm_supplement.ucm_detailed_components.ComputationWithEndingCommunicationStep;
import ucm_supplement.ucm_detailed_components.DetailedComponentImplementation;
import ucm_supplement.ucm_detailed_components.ExecutionScenario;
import ucm_supplement.ucm_detailed_components.IExecutionStep;
import ucm_supplement.ucm_detailed_components.IPortElementImplementation;
import ucm_supplement.ucm_detailed_components.LoopStep;
import ucm_supplement.ucm_detailed_components.PortAndMethod;
import ucm_supplement.ucm_detailed_components.SequenceStep;
import ucm_supplement.ucm_detailed_components.TechnicalPortImplementation;
import ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsFactory;
import ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage;

import ucm_supplement.ucm_detailed_components.util.Ucm_detailed_componentsValidator;
import ucm_supplement.ucm_enhanced_configuration.Ucm_enhanced_configurationPackage;

import ucm_supplement.ucm_enhanced_configuration.impl.Ucm_enhanced_configurationPackageImpl;

import ucm_supplement.ucm_enhanced_portspec.Ucm_enhanced_portspecPackage;

import ucm_supplement.ucm_enhanced_portspec.impl.Ucm_enhanced_portspecPackageImpl;

import ucm_supplement.ucm_test_cases.Ucm_test_casesPackage;

import ucm_supplement.ucm_test_cases.impl.Ucm_test_casesPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Ucm_detailed_componentsPackageImpl extends EPackageImpl implements Ucm_detailed_componentsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass detailedComponentImplementationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iPortElementImplementationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass componentPortImplementationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass technicalPortImplementationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portAndMethodEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass executionScenarioEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iExecutionStepEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass computationStepEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass computationWithEndingCommunicationStepEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass loopStepEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass alternativeStepEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sequenceStepEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass componentMultiplexPortImplementationEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private Ucm_detailed_componentsPackageImpl() {
		super(eNS_URI, Ucm_detailed_componentsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link Ucm_detailed_componentsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static Ucm_detailed_componentsPackage init() {
		if (isInited) return (Ucm_detailed_componentsPackage)EPackage.Registry.INSTANCE.getEPackage(Ucm_detailed_componentsPackage.eNS_URI);

		// Obtain or create and register package
		Ucm_detailed_componentsPackageImpl theUcm_detailed_componentsPackage = (Ucm_detailed_componentsPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof Ucm_detailed_componentsPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new Ucm_detailed_componentsPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		Ucm_basic_projectsPackage.eINSTANCE.eClass();
		Ucm_commonsPackage.eINSTANCE.eClass();
		Ucm_technicalpoliciesPackage.eINSTANCE.eClass();
		Ucm_contractsPackage.eINSTANCE.eClass();
		Ucm_interactionsPackage.eINSTANCE.eClass();
		Ucm_componentsPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Ucm_extended_projectsPackageImpl theUcm_extended_projectsPackage = (Ucm_extended_projectsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_extended_projectsPackage.eNS_URI) instanceof Ucm_extended_projectsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_extended_projectsPackage.eNS_URI) : Ucm_extended_projectsPackage.eINSTANCE);
		Ucm_enhanced_portspecPackageImpl theUcm_enhanced_portspecPackage = (Ucm_enhanced_portspecPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_enhanced_portspecPackage.eNS_URI) instanceof Ucm_enhanced_portspecPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_enhanced_portspecPackage.eNS_URI) : Ucm_enhanced_portspecPackage.eINSTANCE);
		Ucm_deploymentsPackageImpl theUcm_deploymentsPackage = (Ucm_deploymentsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_deploymentsPackage.eNS_URI) instanceof Ucm_deploymentsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_deploymentsPackage.eNS_URI) : Ucm_deploymentsPackage.eINSTANCE);
		Ucm_test_casesPackageImpl theUcm_test_casesPackage = (Ucm_test_casesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_test_casesPackage.eNS_URI) instanceof Ucm_test_casesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_test_casesPackage.eNS_URI) : Ucm_test_casesPackage.eINSTANCE);
		Ucm_enhanced_configurationPackageImpl theUcm_enhanced_configurationPackage = (Ucm_enhanced_configurationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_enhanced_configurationPackage.eNS_URI) instanceof Ucm_enhanced_configurationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_enhanced_configurationPackage.eNS_URI) : Ucm_enhanced_configurationPackage.eINSTANCE);
		Ucm_resourcesPackageImpl theUcm_resourcesPackage = (Ucm_resourcesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_resourcesPackage.eNS_URI) instanceof Ucm_resourcesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_resourcesPackage.eNS_URI) : Ucm_resourcesPackage.eINSTANCE);

		// Create package meta-data objects
		theUcm_detailed_componentsPackage.createPackageContents();
		theUcm_extended_projectsPackage.createPackageContents();
		theUcm_enhanced_portspecPackage.createPackageContents();
		theUcm_deploymentsPackage.createPackageContents();
		theUcm_test_casesPackage.createPackageContents();
		theUcm_enhanced_configurationPackage.createPackageContents();
		theUcm_resourcesPackage.createPackageContents();

		// Initialize created meta-data
		theUcm_detailed_componentsPackage.initializePackageContents();
		theUcm_extended_projectsPackage.initializePackageContents();
		theUcm_enhanced_portspecPackage.initializePackageContents();
		theUcm_deploymentsPackage.initializePackageContents();
		theUcm_test_casesPackage.initializePackageContents();
		theUcm_enhanced_configurationPackage.initializePackageContents();
		theUcm_resourcesPackage.initializePackageContents();

		// Register package validator
		EValidator.Registry.INSTANCE.put
			(theUcm_detailed_componentsPackage, 
			 new EValidator.Descriptor() {
				 public EValidator getEValidator() {
					 return Ucm_detailed_componentsValidator.INSTANCE;
				 }
			 });

		// Mark meta-data to indicate it can't be changed
		theUcm_detailed_componentsPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(Ucm_detailed_componentsPackage.eNS_URI, theUcm_detailed_componentsPackage);
		return theUcm_detailed_componentsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDetailedComponentImplementation() {
		return detailedComponentImplementationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDetailedComponentImplementation_PortImplementation() {
		return (EReference)detailedComponentImplementationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDetailedComponentImplementation_ExecutionScenario() {
		return (EReference)detailedComponentImplementationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDetailedComponentImplementation_Extends() {
		return (EReference)detailedComponentImplementationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIPortElementImplementation() {
		return iPortElementImplementationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIPortElementImplementation_PortElement() {
		return (EReference)iPortElementImplementationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIPortElementImplementation_Intf() {
		return (EReference)iPortElementImplementationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComponentPortImplementation() {
		return componentPortImplementationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentPortImplementation_Port() {
		return (EReference)componentPortImplementationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTechnicalPortImplementation() {
		return technicalPortImplementationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTechnicalPortImplementation_Policy() {
		return (EReference)technicalPortImplementationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortAndMethod() {
		return portAndMethodEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortAndMethod_Method() {
		return (EReference)portAndMethodEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortAndMethod_PortImplementation() {
		return (EReference)portAndMethodEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExecutionScenario() {
		return executionScenarioEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExecutionScenario_PortAndMethod() {
		return (EReference)executionScenarioEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExecutionScenario_AssociatedStep() {
		return (EReference)executionScenarioEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIExecutionStep() {
		return iExecutionStepEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComputationStep() {
		return computationStepEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComputationStep_SpecifiedWCET() {
		return (EAttribute)computationStepEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComputationStep_SpecifiedBCET() {
		return (EAttribute)computationStepEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComputationStep_MeasuredWCET() {
		return (EAttribute)computationStepEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComputationStep_MeasuredBCET() {
		return (EAttribute)computationStepEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComputationStep_EvaluatedWCET() {
		return (EAttribute)computationStepEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComputationStep_EvaluatedBCET() {
		return (EAttribute)computationStepEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComputationWithEndingCommunicationStep() {
		return computationWithEndingCommunicationStepEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComputationWithEndingCommunicationStep_PortAndMethodToCall() {
		return (EReference)computationWithEndingCommunicationStepEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLoopStep() {
		return loopStepEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLoopStep_Step() {
		return (EReference)loopStepEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLoopStep_MinIterations() {
		return (EAttribute)loopStepEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLoopStep_MaxIterations() {
		return (EAttribute)loopStepEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAlternativeStep() {
		return alternativeStepEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAlternativeStep_Step() {
		return (EReference)alternativeStepEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSequenceStep() {
		return sequenceStepEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSequenceStep_Step() {
		return (EReference)sequenceStepEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComponentMultiplexPortImplementation() {
		return componentMultiplexPortImplementationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentMultiplexPortImplementation_BindingName() {
		return (EReference)componentMultiplexPortImplementationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ucm_detailed_componentsFactory getUcm_detailed_componentsFactory() {
		return (Ucm_detailed_componentsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		detailedComponentImplementationEClass = createEClass(DETAILED_COMPONENT_IMPLEMENTATION);
		createEReference(detailedComponentImplementationEClass, DETAILED_COMPONENT_IMPLEMENTATION__PORT_IMPLEMENTATION);
		createEReference(detailedComponentImplementationEClass, DETAILED_COMPONENT_IMPLEMENTATION__EXECUTION_SCENARIO);
		createEReference(detailedComponentImplementationEClass, DETAILED_COMPONENT_IMPLEMENTATION__EXTENDS);

		iPortElementImplementationEClass = createEClass(IPORT_ELEMENT_IMPLEMENTATION);
		createEReference(iPortElementImplementationEClass, IPORT_ELEMENT_IMPLEMENTATION__PORT_ELEMENT);
		createEReference(iPortElementImplementationEClass, IPORT_ELEMENT_IMPLEMENTATION__INTF);

		componentPortImplementationEClass = createEClass(COMPONENT_PORT_IMPLEMENTATION);
		createEReference(componentPortImplementationEClass, COMPONENT_PORT_IMPLEMENTATION__PORT);

		technicalPortImplementationEClass = createEClass(TECHNICAL_PORT_IMPLEMENTATION);
		createEReference(technicalPortImplementationEClass, TECHNICAL_PORT_IMPLEMENTATION__POLICY);

		portAndMethodEClass = createEClass(PORT_AND_METHOD);
		createEReference(portAndMethodEClass, PORT_AND_METHOD__METHOD);
		createEReference(portAndMethodEClass, PORT_AND_METHOD__PORT_IMPLEMENTATION);

		executionScenarioEClass = createEClass(EXECUTION_SCENARIO);
		createEReference(executionScenarioEClass, EXECUTION_SCENARIO__PORT_AND_METHOD);
		createEReference(executionScenarioEClass, EXECUTION_SCENARIO__ASSOCIATED_STEP);

		iExecutionStepEClass = createEClass(IEXECUTION_STEP);

		computationStepEClass = createEClass(COMPUTATION_STEP);
		createEAttribute(computationStepEClass, COMPUTATION_STEP__SPECIFIED_WCET);
		createEAttribute(computationStepEClass, COMPUTATION_STEP__SPECIFIED_BCET);
		createEAttribute(computationStepEClass, COMPUTATION_STEP__MEASURED_WCET);
		createEAttribute(computationStepEClass, COMPUTATION_STEP__MEASURED_BCET);
		createEAttribute(computationStepEClass, COMPUTATION_STEP__EVALUATED_WCET);
		createEAttribute(computationStepEClass, COMPUTATION_STEP__EVALUATED_BCET);

		computationWithEndingCommunicationStepEClass = createEClass(COMPUTATION_WITH_ENDING_COMMUNICATION_STEP);
		createEReference(computationWithEndingCommunicationStepEClass, COMPUTATION_WITH_ENDING_COMMUNICATION_STEP__PORT_AND_METHOD_TO_CALL);

		loopStepEClass = createEClass(LOOP_STEP);
		createEReference(loopStepEClass, LOOP_STEP__STEP);
		createEAttribute(loopStepEClass, LOOP_STEP__MIN_ITERATIONS);
		createEAttribute(loopStepEClass, LOOP_STEP__MAX_ITERATIONS);

		alternativeStepEClass = createEClass(ALTERNATIVE_STEP);
		createEReference(alternativeStepEClass, ALTERNATIVE_STEP__STEP);

		sequenceStepEClass = createEClass(SEQUENCE_STEP);
		createEReference(sequenceStepEClass, SEQUENCE_STEP__STEP);

		componentMultiplexPortImplementationEClass = createEClass(COMPONENT_MULTIPLEX_PORT_IMPLEMENTATION);
		createEReference(componentMultiplexPortImplementationEClass, COMPONENT_MULTIPLEX_PORT_IMPLEMENTATION__BINDING_NAME);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		Ucm_componentsPackage theUcm_componentsPackage = (Ucm_componentsPackage)EPackage.Registry.INSTANCE.getEPackage(Ucm_componentsPackage.eNS_URI);
		Ucm_contractsPackage theUcm_contractsPackage = (Ucm_contractsPackage)EPackage.Registry.INSTANCE.getEPackage(Ucm_contractsPackage.eNS_URI);
		Ucm_commonsPackage theUcm_commonsPackage = (Ucm_commonsPackage)EPackage.Registry.INSTANCE.getEPackage(Ucm_commonsPackage.eNS_URI);
		Ucm_interactionsPackage theUcm_interactionsPackage = (Ucm_interactionsPackage)EPackage.Registry.INSTANCE.getEPackage(Ucm_interactionsPackage.eNS_URI);
		Ucm_datatypesPackage theUcm_datatypesPackage = (Ucm_datatypesPackage)EPackage.Registry.INSTANCE.getEPackage(Ucm_datatypesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		detailedComponentImplementationEClass.getESuperTypes().add(theUcm_componentsPackage.getAtomicComponentImplementation());
		iPortElementImplementationEClass.getESuperTypes().add(theUcm_contractsPackage.getIAnnotable());
		iPortElementImplementationEClass.getESuperTypes().add(theUcm_commonsPackage.getINamed());
		componentPortImplementationEClass.getESuperTypes().add(this.getIPortElementImplementation());
		technicalPortImplementationEClass.getESuperTypes().add(this.getIPortElementImplementation());
		executionScenarioEClass.getESuperTypes().add(theUcm_commonsPackage.getINamed());
		iExecutionStepEClass.getESuperTypes().add(theUcm_commonsPackage.getINamed());
		computationStepEClass.getESuperTypes().add(this.getIExecutionStep());
		computationWithEndingCommunicationStepEClass.getESuperTypes().add(this.getComputationStep());
		loopStepEClass.getESuperTypes().add(this.getIExecutionStep());
		alternativeStepEClass.getESuperTypes().add(this.getIExecutionStep());
		sequenceStepEClass.getESuperTypes().add(this.getIExecutionStep());
		componentMultiplexPortImplementationEClass.getESuperTypes().add(this.getComponentPortImplementation());

		// Initialize classes and features; add operations and parameters
		initEClass(detailedComponentImplementationEClass, DetailedComponentImplementation.class, "DetailedComponentImplementation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDetailedComponentImplementation_PortImplementation(), this.getIPortElementImplementation(), null, "portImplementation", null, 0, -1, DetailedComponentImplementation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDetailedComponentImplementation_ExecutionScenario(), this.getExecutionScenario(), null, "executionScenario", null, 0, -1, DetailedComponentImplementation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDetailedComponentImplementation_Extends(), this.getDetailedComponentImplementation(), null, "extends", null, 0, 1, DetailedComponentImplementation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(iPortElementImplementationEClass, IPortElementImplementation.class, "IPortElementImplementation", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIPortElementImplementation_PortElement(), theUcm_interactionsPackage.getPortElement(), null, "portElement", null, 1, 1, IPortElementImplementation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIPortElementImplementation_Intf(), theUcm_contractsPackage.getInterface(), null, "intf", null, 1, 1, IPortElementImplementation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(componentPortImplementationEClass, ComponentPortImplementation.class, "ComponentPortImplementation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getComponentPortImplementation_Port(), theUcm_componentsPackage.getPort(), null, "port", null, 1, 1, ComponentPortImplementation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = addEOperation(componentPortImplementationEClass, ecorePackage.getEBoolean(), "portImplementationConsistency", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
		EGenericType g1 = createEGenericType(ecorePackage.getEMap());
		EGenericType g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(technicalPortImplementationEClass, TechnicalPortImplementation.class, "TechnicalPortImplementation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTechnicalPortImplementation_Policy(), theUcm_componentsPackage.getComponentTechnicalPolicy(), null, "policy", null, 1, 1, TechnicalPortImplementation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(technicalPortImplementationEClass, ecorePackage.getEBoolean(), "portImplementationConsistency", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(portAndMethodEClass, PortAndMethod.class, "PortAndMethod", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPortAndMethod_Method(), theUcm_contractsPackage.getMethod(), null, "method", null, 1, 1, PortAndMethod.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPortAndMethod_PortImplementation(), this.getIPortElementImplementation(), null, "portImplementation", null, 1, 1, PortAndMethod.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(portAndMethodEClass, ecorePackage.getEBoolean(), "consistency", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(executionScenarioEClass, ExecutionScenario.class, "ExecutionScenario", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getExecutionScenario_PortAndMethod(), this.getPortAndMethod(), null, "portAndMethod", null, 1, 1, ExecutionScenario.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getExecutionScenario_AssociatedStep(), this.getIExecutionStep(), null, "associatedStep", null, 1, 1, ExecutionScenario.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(iExecutionStepEClass, IExecutionStep.class, "IExecutionStep", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(computationStepEClass, ComputationStep.class, "ComputationStep", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getComputationStep_SpecifiedWCET(), ecorePackage.getEDouble(), "specifiedWCET", null, 0, 1, ComputationStep.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComputationStep_SpecifiedBCET(), ecorePackage.getEDouble(), "specifiedBCET", null, 0, 1, ComputationStep.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComputationStep_MeasuredWCET(), ecorePackage.getEDouble(), "measuredWCET", null, 0, 1, ComputationStep.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComputationStep_MeasuredBCET(), ecorePackage.getEDouble(), "measuredBCET", null, 0, 1, ComputationStep.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComputationStep_EvaluatedWCET(), ecorePackage.getEDouble(), "evaluatedWCET", null, 0, 1, ComputationStep.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComputationStep_EvaluatedBCET(), ecorePackage.getEDouble(), "evaluatedBCET", null, 0, 1, ComputationStep.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(computationStepEClass, ecorePackage.getEBoolean(), "valueConsistency", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(computationWithEndingCommunicationStepEClass, ComputationWithEndingCommunicationStep.class, "ComputationWithEndingCommunicationStep", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getComputationWithEndingCommunicationStep_PortAndMethodToCall(), this.getPortAndMethod(), null, "portAndMethodToCall", null, 1, 1, ComputationWithEndingCommunicationStep.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(loopStepEClass, LoopStep.class, "LoopStep", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLoopStep_Step(), this.getIExecutionStep(), null, "step", null, 1, 1, LoopStep.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLoopStep_MinIterations(), ecorePackage.getELong(), "minIterations", null, 0, 1, LoopStep.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLoopStep_MaxIterations(), ecorePackage.getELong(), "maxIterations", null, 0, 1, LoopStep.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(alternativeStepEClass, AlternativeStep.class, "AlternativeStep", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAlternativeStep_Step(), this.getIExecutionStep(), null, "step", null, 1, -1, AlternativeStep.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sequenceStepEClass, SequenceStep.class, "SequenceStep", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSequenceStep_Step(), this.getIExecutionStep(), null, "step", null, 1, -1, SequenceStep.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(componentMultiplexPortImplementationEClass, ComponentMultiplexPortImplementation.class, "ComponentMultiplexPortImplementation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getComponentMultiplexPortImplementation_BindingName(), theUcm_datatypesPackage.getEnumerator(), null, "bindingName", null, 0, 1, ComponentMultiplexPortImplementation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //Ucm_detailed_componentsPackageImpl
