/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_detailed_components.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import ucm_base.ucm_commons.impl.INamedImpl;

import ucm_supplement.ucm_detailed_components.ExecutionScenario;
import ucm_supplement.ucm_detailed_components.IExecutionStep;
import ucm_supplement.ucm_detailed_components.PortAndMethod;
import ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Execution Scenario</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_detailed_components.impl.ExecutionScenarioImpl#getPortAndMethod <em>Port And Method</em>}</li>
 *   <li>{@link ucm_supplement.ucm_detailed_components.impl.ExecutionScenarioImpl#getAssociatedStep <em>Associated Step</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ExecutionScenarioImpl extends INamedImpl implements ExecutionScenario {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached value of the '{@link #getPortAndMethod() <em>Port And Method</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPortAndMethod()
	 * @generated
	 * @ordered
	 */
	protected PortAndMethod portAndMethod;

	/**
	 * The cached value of the '{@link #getAssociatedStep() <em>Associated Step</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssociatedStep()
	 * @generated
	 * @ordered
	 */
	protected IExecutionStep associatedStep;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExecutionScenarioImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_detailed_componentsPackage.Literals.EXECUTION_SCENARIO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortAndMethod getPortAndMethod() {
		return portAndMethod;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPortAndMethod(PortAndMethod newPortAndMethod, NotificationChain msgs) {
		PortAndMethod oldPortAndMethod = portAndMethod;
		portAndMethod = newPortAndMethod;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Ucm_detailed_componentsPackage.EXECUTION_SCENARIO__PORT_AND_METHOD, oldPortAndMethod, newPortAndMethod);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPortAndMethod(PortAndMethod newPortAndMethod) {
		if (newPortAndMethod != portAndMethod) {
			NotificationChain msgs = null;
			if (portAndMethod != null)
				msgs = ((InternalEObject)portAndMethod).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - Ucm_detailed_componentsPackage.EXECUTION_SCENARIO__PORT_AND_METHOD, null, msgs);
			if (newPortAndMethod != null)
				msgs = ((InternalEObject)newPortAndMethod).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - Ucm_detailed_componentsPackage.EXECUTION_SCENARIO__PORT_AND_METHOD, null, msgs);
			msgs = basicSetPortAndMethod(newPortAndMethod, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_detailed_componentsPackage.EXECUTION_SCENARIO__PORT_AND_METHOD, newPortAndMethod, newPortAndMethod));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IExecutionStep getAssociatedStep() {
		return associatedStep;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssociatedStep(IExecutionStep newAssociatedStep, NotificationChain msgs) {
		IExecutionStep oldAssociatedStep = associatedStep;
		associatedStep = newAssociatedStep;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Ucm_detailed_componentsPackage.EXECUTION_SCENARIO__ASSOCIATED_STEP, oldAssociatedStep, newAssociatedStep);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssociatedStep(IExecutionStep newAssociatedStep) {
		if (newAssociatedStep != associatedStep) {
			NotificationChain msgs = null;
			if (associatedStep != null)
				msgs = ((InternalEObject)associatedStep).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - Ucm_detailed_componentsPackage.EXECUTION_SCENARIO__ASSOCIATED_STEP, null, msgs);
			if (newAssociatedStep != null)
				msgs = ((InternalEObject)newAssociatedStep).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - Ucm_detailed_componentsPackage.EXECUTION_SCENARIO__ASSOCIATED_STEP, null, msgs);
			msgs = basicSetAssociatedStep(newAssociatedStep, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_detailed_componentsPackage.EXECUTION_SCENARIO__ASSOCIATED_STEP, newAssociatedStep, newAssociatedStep));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Ucm_detailed_componentsPackage.EXECUTION_SCENARIO__PORT_AND_METHOD:
				return basicSetPortAndMethod(null, msgs);
			case Ucm_detailed_componentsPackage.EXECUTION_SCENARIO__ASSOCIATED_STEP:
				return basicSetAssociatedStep(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_detailed_componentsPackage.EXECUTION_SCENARIO__PORT_AND_METHOD:
				return getPortAndMethod();
			case Ucm_detailed_componentsPackage.EXECUTION_SCENARIO__ASSOCIATED_STEP:
				return getAssociatedStep();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_detailed_componentsPackage.EXECUTION_SCENARIO__PORT_AND_METHOD:
				setPortAndMethod((PortAndMethod)newValue);
				return;
			case Ucm_detailed_componentsPackage.EXECUTION_SCENARIO__ASSOCIATED_STEP:
				setAssociatedStep((IExecutionStep)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_detailed_componentsPackage.EXECUTION_SCENARIO__PORT_AND_METHOD:
				setPortAndMethod((PortAndMethod)null);
				return;
			case Ucm_detailed_componentsPackage.EXECUTION_SCENARIO__ASSOCIATED_STEP:
				setAssociatedStep((IExecutionStep)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_detailed_componentsPackage.EXECUTION_SCENARIO__PORT_AND_METHOD:
				return portAndMethod != null;
			case Ucm_detailed_componentsPackage.EXECUTION_SCENARIO__ASSOCIATED_STEP:
				return associatedStep != null;
		}
		return super.eIsSet(featureID);
	}

} //ExecutionScenarioImpl
