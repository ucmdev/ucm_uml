/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_detailed_components.impl;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import com.thalesgroup.ucm.metamodel.validation.UcmSupplementValidationFactory;

import ucm_base.ucm_contracts.Method;
import ucm_supplement.ucm_detailed_components.IPortElementImplementation;
import ucm_supplement.ucm_detailed_components.PortAndMethod;
import ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Port
 * And Method</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_detailed_components.impl.PortAndMethodImpl#getMethod <em>Method</em>}</li>
 *   <li>{@link ucm_supplement.ucm_detailed_components.impl.PortAndMethodImpl#getPortImplementation <em>Port Implementation</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PortAndMethodImpl extends MinimalEObjectImpl.Container implements PortAndMethod {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached value of the '{@link #getMethod() <em>Method</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getMethod()
	 * @generated
	 * @ordered
	 */
	protected Method method;

	/**
	 * The cached value of the '{@link #getPortImplementation() <em>Port Implementation</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getPortImplementation()
	 * @generated
	 * @ordered
	 */
	protected IPortElementImplementation portImplementation;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected PortAndMethodImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_detailed_componentsPackage.Literals.PORT_AND_METHOD;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Method getMethod() {
		if (method != null && method.eIsProxy()) {
			InternalEObject oldMethod = (InternalEObject)method;
			method = (Method)eResolveProxy(oldMethod);
			if (method != oldMethod) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_detailed_componentsPackage.PORT_AND_METHOD__METHOD, oldMethod, method));
			}
		}
		return method;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Method basicGetMethod() {
		return method;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setMethod(Method newMethod) {
		Method oldMethod = method;
		method = newMethod;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_detailed_componentsPackage.PORT_AND_METHOD__METHOD, oldMethod, method));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public IPortElementImplementation getPortImplementation() {
		if (portImplementation != null && portImplementation.eIsProxy()) {
			InternalEObject oldPortImplementation = (InternalEObject)portImplementation;
			portImplementation = (IPortElementImplementation)eResolveProxy(oldPortImplementation);
			if (portImplementation != oldPortImplementation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_detailed_componentsPackage.PORT_AND_METHOD__PORT_IMPLEMENTATION, oldPortImplementation, portImplementation));
			}
		}
		return portImplementation;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public IPortElementImplementation basicGetPortImplementation() {
		return portImplementation;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setPortImplementation(IPortElementImplementation newPortImplementation) {
		IPortElementImplementation oldPortImplementation = portImplementation;
		portImplementation = newPortImplementation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_detailed_componentsPackage.PORT_AND_METHOD__PORT_IMPLEMENTATION, oldPortImplementation, portImplementation));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean consistency(DiagnosticChain diagnostics, Map<Object, Object> context) {
		return UcmSupplementValidationFactory.INSTANCE.getDetailedComponentValidator().consistency(this, diagnostics,
				context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_detailed_componentsPackage.PORT_AND_METHOD__METHOD:
				if (resolve) return getMethod();
				return basicGetMethod();
			case Ucm_detailed_componentsPackage.PORT_AND_METHOD__PORT_IMPLEMENTATION:
				if (resolve) return getPortImplementation();
				return basicGetPortImplementation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_detailed_componentsPackage.PORT_AND_METHOD__METHOD:
				setMethod((Method)newValue);
				return;
			case Ucm_detailed_componentsPackage.PORT_AND_METHOD__PORT_IMPLEMENTATION:
				setPortImplementation((IPortElementImplementation)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_detailed_componentsPackage.PORT_AND_METHOD__METHOD:
				setMethod((Method)null);
				return;
			case Ucm_detailed_componentsPackage.PORT_AND_METHOD__PORT_IMPLEMENTATION:
				setPortImplementation((IPortElementImplementation)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_detailed_componentsPackage.PORT_AND_METHOD__METHOD:
				return method != null;
			case Ucm_detailed_componentsPackage.PORT_AND_METHOD__PORT_IMPLEMENTATION:
				return portImplementation != null;
		}
		return super.eIsSet(featureID);
	}

} // PortAndMethodImpl
