/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_detailed_components;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Computation With Ending Communication Step</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_detailed_components.ComputationWithEndingCommunicationStep#getPortAndMethodToCall <em>Port And Method To Call</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getComputationWithEndingCommunicationStep()
 * @model
 * @generated
 */
public interface ComputationWithEndingCommunicationStep extends ComputationStep {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Port And Method To Call</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port And Method To Call</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port And Method To Call</em>' containment reference.
	 * @see #setPortAndMethodToCall(PortAndMethod)
	 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getComputationWithEndingCommunicationStep_PortAndMethodToCall()
	 * @model containment="true" required="true"
	 * @generated
	 */
	PortAndMethod getPortAndMethodToCall();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_detailed_components.ComputationWithEndingCommunicationStep#getPortAndMethodToCall <em>Port And Method To Call</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port And Method To Call</em>' containment reference.
	 * @see #getPortAndMethodToCall()
	 * @generated
	 */
	void setPortAndMethodToCall(PortAndMethod value);

} // ComputationWithEndingCommunicationStep
