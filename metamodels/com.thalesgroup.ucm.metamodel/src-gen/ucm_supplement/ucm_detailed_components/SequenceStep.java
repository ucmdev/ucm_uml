/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_detailed_components;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sequence Step</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_detailed_components.SequenceStep#getStep <em>Step</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getSequenceStep()
 * @model
 * @generated
 */
public interface SequenceStep extends IExecutionStep {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Step</b></em>' containment reference list.
	 * The list contents are of type {@link ucm_supplement.ucm_detailed_components.IExecutionStep}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Step</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Step</em>' containment reference list.
	 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getSequenceStep_Step()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<IExecutionStep> getStep();

} // SequenceStep
