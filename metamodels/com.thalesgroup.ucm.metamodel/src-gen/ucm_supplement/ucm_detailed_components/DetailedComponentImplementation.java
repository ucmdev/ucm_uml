/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_detailed_components;

import org.eclipse.emf.common.util.EList;

import ucm_base.ucm_components.AtomicComponentImplementation;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Detailed Component Implementation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_detailed_components.DetailedComponentImplementation#getPortImplementation <em>Port Implementation</em>}</li>
 *   <li>{@link ucm_supplement.ucm_detailed_components.DetailedComponentImplementation#getExecutionScenario <em>Execution Scenario</em>}</li>
 *   <li>{@link ucm_supplement.ucm_detailed_components.DetailedComponentImplementation#getExtends <em>Extends</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getDetailedComponentImplementation()
 * @model
 * @generated
 */
public interface DetailedComponentImplementation extends AtomicComponentImplementation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Port Implementation</b></em>' containment reference list.
	 * The list contents are of type {@link ucm_supplement.ucm_detailed_components.IPortElementImplementation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port Implementation</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port Implementation</em>' containment reference list.
	 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getDetailedComponentImplementation_PortImplementation()
	 * @model containment="true"
	 * @generated
	 */
	EList<IPortElementImplementation> getPortImplementation();

	/**
	 * Returns the value of the '<em><b>Execution Scenario</b></em>' containment reference list.
	 * The list contents are of type {@link ucm_supplement.ucm_detailed_components.ExecutionScenario}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Execution Scenario</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Execution Scenario</em>' containment reference list.
	 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getDetailedComponentImplementation_ExecutionScenario()
	 * @model containment="true"
	 * @generated
	 */
	EList<ExecutionScenario> getExecutionScenario();

	/**
	 * Returns the value of the '<em><b>Extends</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extends</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extends</em>' reference.
	 * @see #setExtends(DetailedComponentImplementation)
	 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getDetailedComponentImplementation_Extends()
	 * @model
	 * @generated
	 */
	DetailedComponentImplementation getExtends();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_detailed_components.DetailedComponentImplementation#getExtends <em>Extends</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Extends</em>' reference.
	 * @see #getExtends()
	 * @generated
	 */
	void setExtends(DetailedComponentImplementation value);

} // DetailedComponentImplementation
