/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_detailed_components;

import ucm_base.ucm_commons.INamed;

import ucm_base.ucm_contracts.IAnnotable;

import ucm_base.ucm_contracts.Interface;
import ucm_base.ucm_interactions.PortElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IPort Element Implementation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_detailed_components.IPortElementImplementation#getPortElement <em>Port Element</em>}</li>
 *   <li>{@link ucm_supplement.ucm_detailed_components.IPortElementImplementation#getIntf <em>Intf</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getIPortElementImplementation()
 * @model abstract="true"
 * @generated
 */
public interface IPortElementImplementation extends IAnnotable, INamed {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Port Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port Element</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port Element</em>' reference.
	 * @see #setPortElement(PortElement)
	 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getIPortElementImplementation_PortElement()
	 * @model required="true"
	 * @generated
	 */
	PortElement getPortElement();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_detailed_components.IPortElementImplementation#getPortElement <em>Port Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port Element</em>' reference.
	 * @see #getPortElement()
	 * @generated
	 */
	void setPortElement(PortElement value);

	/**
	 * Returns the value of the '<em><b>Intf</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Intf</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Intf</em>' reference.
	 * @see #setIntf(Interface)
	 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getIPortElementImplementation_Intf()
	 * @model required="true"
	 * @generated
	 */
	Interface getIntf();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_detailed_components.IPortElementImplementation#getIntf <em>Intf</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Intf</em>' reference.
	 * @see #getIntf()
	 * @generated
	 */
	void setIntf(Interface value);

} // IPortElementImplementation
