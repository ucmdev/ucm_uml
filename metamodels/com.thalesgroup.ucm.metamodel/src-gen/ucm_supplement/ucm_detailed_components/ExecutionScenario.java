/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_detailed_components;

import ucm_base.ucm_commons.INamed;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Execution Scenario</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_detailed_components.ExecutionScenario#getPortAndMethod <em>Port And Method</em>}</li>
 *   <li>{@link ucm_supplement.ucm_detailed_components.ExecutionScenario#getAssociatedStep <em>Associated Step</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getExecutionScenario()
 * @model
 * @generated
 */
public interface ExecutionScenario extends INamed {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Port And Method</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port And Method</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port And Method</em>' containment reference.
	 * @see #setPortAndMethod(PortAndMethod)
	 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getExecutionScenario_PortAndMethod()
	 * @model containment="true" required="true"
	 * @generated
	 */
	PortAndMethod getPortAndMethod();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_detailed_components.ExecutionScenario#getPortAndMethod <em>Port And Method</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port And Method</em>' containment reference.
	 * @see #getPortAndMethod()
	 * @generated
	 */
	void setPortAndMethod(PortAndMethod value);

	/**
	 * Returns the value of the '<em><b>Associated Step</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Associated Step</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Associated Step</em>' containment reference.
	 * @see #setAssociatedStep(IExecutionStep)
	 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getExecutionScenario_AssociatedStep()
	 * @model containment="true" required="true"
	 * @generated
	 */
	IExecutionStep getAssociatedStep();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_detailed_components.ExecutionScenario#getAssociatedStep <em>Associated Step</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Associated Step</em>' containment reference.
	 * @see #getAssociatedStep()
	 * @generated
	 */
	void setAssociatedStep(IExecutionStep value);

} // ExecutionScenario
