/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_detailed_components.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import ucm_base.ucm_commons.INamed;

import ucm_base.ucm_components.AtomicComponentImplementation;
import ucm_base.ucm_components.IComponent;
import ucm_base.ucm_components.IComponentImplementation;

import ucm_base.ucm_contracts.IAnnotable;

import ucm_supplement.ucm_detailed_components.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage
 * @generated
 */
public class Ucm_detailed_componentsAdapterFactory extends AdapterFactoryImpl {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static Ucm_detailed_componentsPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ucm_detailed_componentsAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = Ucm_detailed_componentsPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Ucm_detailed_componentsSwitch<Adapter> modelSwitch =
		new Ucm_detailed_componentsSwitch<Adapter>() {
			@Override
			public Adapter caseDetailedComponentImplementation(DetailedComponentImplementation object) {
				return createDetailedComponentImplementationAdapter();
			}
			@Override
			public Adapter caseIPortElementImplementation(IPortElementImplementation object) {
				return createIPortElementImplementationAdapter();
			}
			@Override
			public Adapter caseComponentPortImplementation(ComponentPortImplementation object) {
				return createComponentPortImplementationAdapter();
			}
			@Override
			public Adapter caseTechnicalPortImplementation(TechnicalPortImplementation object) {
				return createTechnicalPortImplementationAdapter();
			}
			@Override
			public Adapter casePortAndMethod(PortAndMethod object) {
				return createPortAndMethodAdapter();
			}
			@Override
			public Adapter caseExecutionScenario(ExecutionScenario object) {
				return createExecutionScenarioAdapter();
			}
			@Override
			public Adapter caseIExecutionStep(IExecutionStep object) {
				return createIExecutionStepAdapter();
			}
			@Override
			public Adapter caseComputationStep(ComputationStep object) {
				return createComputationStepAdapter();
			}
			@Override
			public Adapter caseComputationWithEndingCommunicationStep(ComputationWithEndingCommunicationStep object) {
				return createComputationWithEndingCommunicationStepAdapter();
			}
			@Override
			public Adapter caseLoopStep(LoopStep object) {
				return createLoopStepAdapter();
			}
			@Override
			public Adapter caseAlternativeStep(AlternativeStep object) {
				return createAlternativeStepAdapter();
			}
			@Override
			public Adapter caseSequenceStep(SequenceStep object) {
				return createSequenceStepAdapter();
			}
			@Override
			public Adapter caseComponentMultiplexPortImplementation(ComponentMultiplexPortImplementation object) {
				return createComponentMultiplexPortImplementationAdapter();
			}
			@Override
			public Adapter caseINamed(INamed object) {
				return createINamedAdapter();
			}
			@Override
			public Adapter caseIAnnotable(IAnnotable object) {
				return createIAnnotableAdapter();
			}
			@Override
			public Adapter caseIComponent(IComponent object) {
				return createIComponentAdapter();
			}
			@Override
			public Adapter caseIComponentImplementation(IComponentImplementation object) {
				return createIComponentImplementationAdapter();
			}
			@Override
			public Adapter caseAtomicComponentImplementation(AtomicComponentImplementation object) {
				return createAtomicComponentImplementationAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link ucm_supplement.ucm_detailed_components.DetailedComponentImplementation <em>Detailed Component Implementation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_supplement.ucm_detailed_components.DetailedComponentImplementation
	 * @generated
	 */
	public Adapter createDetailedComponentImplementationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_supplement.ucm_detailed_components.IPortElementImplementation <em>IPort Element Implementation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_supplement.ucm_detailed_components.IPortElementImplementation
	 * @generated
	 */
	public Adapter createIPortElementImplementationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_supplement.ucm_detailed_components.ComponentPortImplementation <em>Component Port Implementation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_supplement.ucm_detailed_components.ComponentPortImplementation
	 * @generated
	 */
	public Adapter createComponentPortImplementationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_supplement.ucm_detailed_components.TechnicalPortImplementation <em>Technical Port Implementation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_supplement.ucm_detailed_components.TechnicalPortImplementation
	 * @generated
	 */
	public Adapter createTechnicalPortImplementationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_supplement.ucm_detailed_components.PortAndMethod <em>Port And Method</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_supplement.ucm_detailed_components.PortAndMethod
	 * @generated
	 */
	public Adapter createPortAndMethodAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_supplement.ucm_detailed_components.ExecutionScenario <em>Execution Scenario</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_supplement.ucm_detailed_components.ExecutionScenario
	 * @generated
	 */
	public Adapter createExecutionScenarioAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_supplement.ucm_detailed_components.IExecutionStep <em>IExecution Step</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_supplement.ucm_detailed_components.IExecutionStep
	 * @generated
	 */
	public Adapter createIExecutionStepAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_supplement.ucm_detailed_components.ComputationStep <em>Computation Step</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_supplement.ucm_detailed_components.ComputationStep
	 * @generated
	 */
	public Adapter createComputationStepAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_supplement.ucm_detailed_components.ComputationWithEndingCommunicationStep <em>Computation With Ending Communication Step</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_supplement.ucm_detailed_components.ComputationWithEndingCommunicationStep
	 * @generated
	 */
	public Adapter createComputationWithEndingCommunicationStepAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_supplement.ucm_detailed_components.LoopStep <em>Loop Step</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_supplement.ucm_detailed_components.LoopStep
	 * @generated
	 */
	public Adapter createLoopStepAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_supplement.ucm_detailed_components.AlternativeStep <em>Alternative Step</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_supplement.ucm_detailed_components.AlternativeStep
	 * @generated
	 */
	public Adapter createAlternativeStepAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_supplement.ucm_detailed_components.SequenceStep <em>Sequence Step</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_supplement.ucm_detailed_components.SequenceStep
	 * @generated
	 */
	public Adapter createSequenceStepAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_supplement.ucm_detailed_components.ComponentMultiplexPortImplementation <em>Component Multiplex Port Implementation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_supplement.ucm_detailed_components.ComponentMultiplexPortImplementation
	 * @generated
	 */
	public Adapter createComponentMultiplexPortImplementationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_base.ucm_commons.INamed <em>INamed</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_base.ucm_commons.INamed
	 * @generated
	 */
	public Adapter createINamedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_base.ucm_contracts.IAnnotable <em>IAnnotable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_base.ucm_contracts.IAnnotable
	 * @generated
	 */
	public Adapter createIAnnotableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_base.ucm_components.IComponent <em>IComponent</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_base.ucm_components.IComponent
	 * @generated
	 */
	public Adapter createIComponentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_base.ucm_components.IComponentImplementation <em>IComponent Implementation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_base.ucm_components.IComponentImplementation
	 * @generated
	 */
	public Adapter createIComponentImplementationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_base.ucm_components.AtomicComponentImplementation <em>Atomic Component Implementation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_base.ucm_components.AtomicComponentImplementation
	 * @generated
	 */
	public Adapter createAtomicComponentImplementationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //Ucm_detailed_componentsAdapterFactory
