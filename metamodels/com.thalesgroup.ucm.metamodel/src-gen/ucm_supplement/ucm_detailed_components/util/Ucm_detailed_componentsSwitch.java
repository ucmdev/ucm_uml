/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_detailed_components.util;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import ucm_base.ucm_commons.INamed;

import ucm_base.ucm_components.AtomicComponentImplementation;
import ucm_base.ucm_components.IComponent;
import ucm_base.ucm_components.IComponentImplementation;

import ucm_base.ucm_contracts.IAnnotable;

import ucm_supplement.ucm_detailed_components.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage
 * @generated
 */
public class Ucm_detailed_componentsSwitch<T> {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static Ucm_detailed_componentsPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ucm_detailed_componentsSwitch() {
		if (modelPackage == null) {
			modelPackage = Ucm_detailed_componentsPackage.eINSTANCE;
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	public T doSwitch(EObject theEObject) {
		return doSwitch(theEObject.eClass(), theEObject);
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(EClass theEClass, EObject theEObject) {
		if (theEClass.eContainer() == modelPackage) {
			return doSwitch(theEClass.getClassifierID(), theEObject);
		}
		else {
			List<EClass> eSuperTypes = theEClass.getESuperTypes();
			return
				eSuperTypes.isEmpty() ?
					defaultCase(theEObject) :
					doSwitch(eSuperTypes.get(0), theEObject);
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case Ucm_detailed_componentsPackage.DETAILED_COMPONENT_IMPLEMENTATION: {
				DetailedComponentImplementation detailedComponentImplementation = (DetailedComponentImplementation)theEObject;
				T result = caseDetailedComponentImplementation(detailedComponentImplementation);
				if (result == null) result = caseAtomicComponentImplementation(detailedComponentImplementation);
				if (result == null) result = caseIComponentImplementation(detailedComponentImplementation);
				if (result == null) result = caseIComponent(detailedComponentImplementation);
				if (result == null) result = caseINamed(detailedComponentImplementation);
				if (result == null) result = caseIAnnotable(detailedComponentImplementation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_detailed_componentsPackage.IPORT_ELEMENT_IMPLEMENTATION: {
				IPortElementImplementation iPortElementImplementation = (IPortElementImplementation)theEObject;
				T result = caseIPortElementImplementation(iPortElementImplementation);
				if (result == null) result = caseIAnnotable(iPortElementImplementation);
				if (result == null) result = caseINamed(iPortElementImplementation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_detailed_componentsPackage.COMPONENT_PORT_IMPLEMENTATION: {
				ComponentPortImplementation componentPortImplementation = (ComponentPortImplementation)theEObject;
				T result = caseComponentPortImplementation(componentPortImplementation);
				if (result == null) result = caseIPortElementImplementation(componentPortImplementation);
				if (result == null) result = caseIAnnotable(componentPortImplementation);
				if (result == null) result = caseINamed(componentPortImplementation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_detailed_componentsPackage.TECHNICAL_PORT_IMPLEMENTATION: {
				TechnicalPortImplementation technicalPortImplementation = (TechnicalPortImplementation)theEObject;
				T result = caseTechnicalPortImplementation(technicalPortImplementation);
				if (result == null) result = caseIPortElementImplementation(technicalPortImplementation);
				if (result == null) result = caseIAnnotable(technicalPortImplementation);
				if (result == null) result = caseINamed(technicalPortImplementation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_detailed_componentsPackage.PORT_AND_METHOD: {
				PortAndMethod portAndMethod = (PortAndMethod)theEObject;
				T result = casePortAndMethod(portAndMethod);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_detailed_componentsPackage.EXECUTION_SCENARIO: {
				ExecutionScenario executionScenario = (ExecutionScenario)theEObject;
				T result = caseExecutionScenario(executionScenario);
				if (result == null) result = caseINamed(executionScenario);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_detailed_componentsPackage.IEXECUTION_STEP: {
				IExecutionStep iExecutionStep = (IExecutionStep)theEObject;
				T result = caseIExecutionStep(iExecutionStep);
				if (result == null) result = caseINamed(iExecutionStep);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_detailed_componentsPackage.COMPUTATION_STEP: {
				ComputationStep computationStep = (ComputationStep)theEObject;
				T result = caseComputationStep(computationStep);
				if (result == null) result = caseIExecutionStep(computationStep);
				if (result == null) result = caseINamed(computationStep);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_detailed_componentsPackage.COMPUTATION_WITH_ENDING_COMMUNICATION_STEP: {
				ComputationWithEndingCommunicationStep computationWithEndingCommunicationStep = (ComputationWithEndingCommunicationStep)theEObject;
				T result = caseComputationWithEndingCommunicationStep(computationWithEndingCommunicationStep);
				if (result == null) result = caseComputationStep(computationWithEndingCommunicationStep);
				if (result == null) result = caseIExecutionStep(computationWithEndingCommunicationStep);
				if (result == null) result = caseINamed(computationWithEndingCommunicationStep);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_detailed_componentsPackage.LOOP_STEP: {
				LoopStep loopStep = (LoopStep)theEObject;
				T result = caseLoopStep(loopStep);
				if (result == null) result = caseIExecutionStep(loopStep);
				if (result == null) result = caseINamed(loopStep);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_detailed_componentsPackage.ALTERNATIVE_STEP: {
				AlternativeStep alternativeStep = (AlternativeStep)theEObject;
				T result = caseAlternativeStep(alternativeStep);
				if (result == null) result = caseIExecutionStep(alternativeStep);
				if (result == null) result = caseINamed(alternativeStep);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_detailed_componentsPackage.SEQUENCE_STEP: {
				SequenceStep sequenceStep = (SequenceStep)theEObject;
				T result = caseSequenceStep(sequenceStep);
				if (result == null) result = caseIExecutionStep(sequenceStep);
				if (result == null) result = caseINamed(sequenceStep);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_detailed_componentsPackage.COMPONENT_MULTIPLEX_PORT_IMPLEMENTATION: {
				ComponentMultiplexPortImplementation componentMultiplexPortImplementation = (ComponentMultiplexPortImplementation)theEObject;
				T result = caseComponentMultiplexPortImplementation(componentMultiplexPortImplementation);
				if (result == null) result = caseComponentPortImplementation(componentMultiplexPortImplementation);
				if (result == null) result = caseIPortElementImplementation(componentMultiplexPortImplementation);
				if (result == null) result = caseIAnnotable(componentMultiplexPortImplementation);
				if (result == null) result = caseINamed(componentMultiplexPortImplementation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Detailed Component Implementation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Detailed Component Implementation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDetailedComponentImplementation(DetailedComponentImplementation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IPort Element Implementation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IPort Element Implementation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIPortElementImplementation(IPortElementImplementation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component Port Implementation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component Port Implementation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponentPortImplementation(ComponentPortImplementation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Technical Port Implementation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Technical Port Implementation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTechnicalPortImplementation(TechnicalPortImplementation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port And Method</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port And Method</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortAndMethod(PortAndMethod object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Execution Scenario</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Execution Scenario</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExecutionScenario(ExecutionScenario object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IExecution Step</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IExecution Step</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIExecutionStep(IExecutionStep object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Computation Step</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Computation Step</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComputationStep(ComputationStep object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Computation With Ending Communication Step</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Computation With Ending Communication Step</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComputationWithEndingCommunicationStep(ComputationWithEndingCommunicationStep object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Loop Step</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Loop Step</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLoopStep(LoopStep object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Alternative Step</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Alternative Step</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAlternativeStep(AlternativeStep object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sequence Step</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sequence Step</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSequenceStep(SequenceStep object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component Multiplex Port Implementation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component Multiplex Port Implementation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponentMultiplexPortImplementation(ComponentMultiplexPortImplementation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>INamed</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>INamed</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseINamed(INamed object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IAnnotable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IAnnotable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIAnnotable(IAnnotable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IComponent</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IComponent</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIComponent(IComponent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IComponent Implementation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IComponent Implementation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIComponentImplementation(IComponentImplementation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Atomic Component Implementation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Atomic Component Implementation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAtomicComponentImplementation(AtomicComponentImplementation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public T defaultCase(EObject object) {
		return null;
	}

} //Ucm_detailed_componentsSwitch
