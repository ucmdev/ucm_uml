/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_detailed_components.util;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

import ucm_base.ucm_components.util.Ucm_componentsValidator;

import ucm_supplement.ucm_detailed_components.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage
 * @generated
 */
public class Ucm_detailed_componentsValidator extends EObjectValidator {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final Ucm_detailed_componentsValidator INSTANCE = new Ucm_detailed_componentsValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "ucm_supplement.ucm_detailed_components";

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Port Implementation Consistency' of 'Component Port Implementation'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int COMPONENT_PORT_IMPLEMENTATION__PORT_IMPLEMENTATION_CONSISTENCY = 1;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Port Implementation Consistency' of 'Technical Port Implementation'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int TECHNICAL_PORT_IMPLEMENTATION__PORT_IMPLEMENTATION_CONSISTENCY = 2;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Consistency' of 'Port And Method'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int PORT_AND_METHOD__CONSISTENCY = 3;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Value Consistency' of 'Computation Step'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int COMPUTATION_STEP__VALUE_CONSISTENCY = 4;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 4;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * The cached base package validator.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Ucm_componentsValidator ucm_componentsValidator;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ucm_detailed_componentsValidator() {
		super();
		ucm_componentsValidator = Ucm_componentsValidator.INSTANCE;
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return Ucm_detailed_componentsPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case Ucm_detailed_componentsPackage.DETAILED_COMPONENT_IMPLEMENTATION:
				return validateDetailedComponentImplementation((DetailedComponentImplementation)value, diagnostics, context);
			case Ucm_detailed_componentsPackage.IPORT_ELEMENT_IMPLEMENTATION:
				return validateIPortElementImplementation((IPortElementImplementation)value, diagnostics, context);
			case Ucm_detailed_componentsPackage.COMPONENT_PORT_IMPLEMENTATION:
				return validateComponentPortImplementation((ComponentPortImplementation)value, diagnostics, context);
			case Ucm_detailed_componentsPackage.TECHNICAL_PORT_IMPLEMENTATION:
				return validateTechnicalPortImplementation((TechnicalPortImplementation)value, diagnostics, context);
			case Ucm_detailed_componentsPackage.PORT_AND_METHOD:
				return validatePortAndMethod((PortAndMethod)value, diagnostics, context);
			case Ucm_detailed_componentsPackage.EXECUTION_SCENARIO:
				return validateExecutionScenario((ExecutionScenario)value, diagnostics, context);
			case Ucm_detailed_componentsPackage.IEXECUTION_STEP:
				return validateIExecutionStep((IExecutionStep)value, diagnostics, context);
			case Ucm_detailed_componentsPackage.COMPUTATION_STEP:
				return validateComputationStep((ComputationStep)value, diagnostics, context);
			case Ucm_detailed_componentsPackage.COMPUTATION_WITH_ENDING_COMMUNICATION_STEP:
				return validateComputationWithEndingCommunicationStep((ComputationWithEndingCommunicationStep)value, diagnostics, context);
			case Ucm_detailed_componentsPackage.LOOP_STEP:
				return validateLoopStep((LoopStep)value, diagnostics, context);
			case Ucm_detailed_componentsPackage.ALTERNATIVE_STEP:
				return validateAlternativeStep((AlternativeStep)value, diagnostics, context);
			case Ucm_detailed_componentsPackage.SEQUENCE_STEP:
				return validateSequenceStep((SequenceStep)value, diagnostics, context);
			case Ucm_detailed_componentsPackage.COMPONENT_MULTIPLEX_PORT_IMPLEMENTATION:
				return validateComponentMultiplexPortImplementation((ComponentMultiplexPortImplementation)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDetailedComponentImplementation(DetailedComponentImplementation detailedComponentImplementation, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(detailedComponentImplementation, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(detailedComponentImplementation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(detailedComponentImplementation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(detailedComponentImplementation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(detailedComponentImplementation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(detailedComponentImplementation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(detailedComponentImplementation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(detailedComponentImplementation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(detailedComponentImplementation, diagnostics, context);
		if (result || diagnostics != null) result &= ucm_componentsValidator.validateAtomicComponentImplementation_policyMultiplicity(detailedComponentImplementation, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIPortElementImplementation(IPortElementImplementation iPortElementImplementation, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(iPortElementImplementation, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComponentPortImplementation(ComponentPortImplementation componentPortImplementation, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(componentPortImplementation, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(componentPortImplementation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(componentPortImplementation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(componentPortImplementation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(componentPortImplementation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(componentPortImplementation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(componentPortImplementation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(componentPortImplementation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(componentPortImplementation, diagnostics, context);
		if (result || diagnostics != null) result &= validateComponentPortImplementation_portImplementationConsistency(componentPortImplementation, diagnostics, context);
		return result;
	}

	/**
	 * Validates the portImplementationConsistency constraint of '<em>Component Port Implementation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComponentPortImplementation_portImplementationConsistency(ComponentPortImplementation componentPortImplementation, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return componentPortImplementation.portImplementationConsistency(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTechnicalPortImplementation(TechnicalPortImplementation technicalPortImplementation, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(technicalPortImplementation, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(technicalPortImplementation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(technicalPortImplementation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(technicalPortImplementation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(technicalPortImplementation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(technicalPortImplementation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(technicalPortImplementation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(technicalPortImplementation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(technicalPortImplementation, diagnostics, context);
		if (result || diagnostics != null) result &= validateTechnicalPortImplementation_portImplementationConsistency(technicalPortImplementation, diagnostics, context);
		return result;
	}

	/**
	 * Validates the portImplementationConsistency constraint of '<em>Technical Port Implementation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTechnicalPortImplementation_portImplementationConsistency(TechnicalPortImplementation technicalPortImplementation, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return technicalPortImplementation.portImplementationConsistency(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePortAndMethod(PortAndMethod portAndMethod, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(portAndMethod, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(portAndMethod, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(portAndMethod, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(portAndMethod, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(portAndMethod, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(portAndMethod, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(portAndMethod, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(portAndMethod, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(portAndMethod, diagnostics, context);
		if (result || diagnostics != null) result &= validatePortAndMethod_consistency(portAndMethod, diagnostics, context);
		return result;
	}

	/**
	 * Validates the consistency constraint of '<em>Port And Method</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePortAndMethod_consistency(PortAndMethod portAndMethod, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return portAndMethod.consistency(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateExecutionScenario(ExecutionScenario executionScenario, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(executionScenario, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIExecutionStep(IExecutionStep iExecutionStep, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(iExecutionStep, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComputationStep(ComputationStep computationStep, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(computationStep, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(computationStep, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(computationStep, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(computationStep, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(computationStep, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(computationStep, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(computationStep, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(computationStep, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(computationStep, diagnostics, context);
		if (result || diagnostics != null) result &= validateComputationStep_valueConsistency(computationStep, diagnostics, context);
		return result;
	}

	/**
	 * Validates the valueConsistency constraint of '<em>Computation Step</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComputationStep_valueConsistency(ComputationStep computationStep, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return computationStep.valueConsistency(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComputationWithEndingCommunicationStep(ComputationWithEndingCommunicationStep computationWithEndingCommunicationStep, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(computationWithEndingCommunicationStep, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(computationWithEndingCommunicationStep, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(computationWithEndingCommunicationStep, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(computationWithEndingCommunicationStep, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(computationWithEndingCommunicationStep, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(computationWithEndingCommunicationStep, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(computationWithEndingCommunicationStep, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(computationWithEndingCommunicationStep, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(computationWithEndingCommunicationStep, diagnostics, context);
		if (result || diagnostics != null) result &= validateComputationStep_valueConsistency(computationWithEndingCommunicationStep, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLoopStep(LoopStep loopStep, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(loopStep, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAlternativeStep(AlternativeStep alternativeStep, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(alternativeStep, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSequenceStep(SequenceStep sequenceStep, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(sequenceStep, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComponentMultiplexPortImplementation(ComponentMultiplexPortImplementation componentMultiplexPortImplementation, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(componentMultiplexPortImplementation, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(componentMultiplexPortImplementation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(componentMultiplexPortImplementation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(componentMultiplexPortImplementation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(componentMultiplexPortImplementation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(componentMultiplexPortImplementation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(componentMultiplexPortImplementation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(componentMultiplexPortImplementation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(componentMultiplexPortImplementation, diagnostics, context);
		if (result || diagnostics != null) result &= validateComponentPortImplementation_portImplementationConsistency(componentMultiplexPortImplementation, diagnostics, context);
		return result;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //Ucm_detailed_componentsValidator
