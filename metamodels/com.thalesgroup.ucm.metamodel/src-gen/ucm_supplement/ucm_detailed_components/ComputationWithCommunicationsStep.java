/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_detailed_components;

import java.util.Map;
import org.eclipse.emf.common.util.DiagnosticChain;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Computation With Communications Step</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_detailed_components.ComputationWithCommunicationsStep#getMinNumberOfCalls <em>Min Number Of Calls</em>}</li>
 *   <li>{@link ucm_supplement.ucm_detailed_components.ComputationWithCommunicationsStep#getMaxNumberOfCalls <em>Max Number Of Calls</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getComputationWithCommunicationsStep()
 * @model
 * @generated
 */
public interface ComputationWithCommunicationsStep extends ComputationWithEndingCommunicationStep {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Min Number Of Calls</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min Number Of Calls</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Number Of Calls</em>' attribute.
	 * @see #setMinNumberOfCalls(long)
	 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getComputationWithCommunicationsStep_MinNumberOfCalls()
	 * @model
	 * @generated
	 */
	long getMinNumberOfCalls();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_detailed_components.ComputationWithCommunicationsStep#getMinNumberOfCalls <em>Min Number Of Calls</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Number Of Calls</em>' attribute.
	 * @see #getMinNumberOfCalls()
	 * @generated
	 */
	void setMinNumberOfCalls(long value);

	/**
	 * Returns the value of the '<em><b>Max Number Of Calls</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Number Of Calls</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Number Of Calls</em>' attribute.
	 * @see #setMaxNumberOfCalls(long)
	 * @see ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage#getComputationWithCommunicationsStep_MaxNumberOfCalls()
	 * @model
	 * @generated
	 */
	long getMaxNumberOfCalls();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_detailed_components.ComputationWithCommunicationsStep#getMaxNumberOfCalls <em>Max Number Of Calls</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Number Of Calls</em>' attribute.
	 * @see #getMaxNumberOfCalls()
	 * @generated
	 */
	void setMaxNumberOfCalls(long value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean valueConsistency(DiagnosticChain diagnostics, Map<Object, Object> context);

} // ComputationWithCommunicationsStep
