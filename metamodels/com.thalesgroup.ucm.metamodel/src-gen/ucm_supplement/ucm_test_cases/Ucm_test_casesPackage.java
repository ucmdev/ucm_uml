/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_test_cases;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import ucm_base.ucm_commons.Ucm_commonsPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ucm_supplement.ucm_test_cases.Ucm_test_casesFactory
 * @model kind="package"
 * @generated
 */
public interface Ucm_test_casesPackage extends EPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ucm_test_cases";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://com.thalesgroup.ucm/1.0/metamodels/supplement/testcases";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ucm_test_cases";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Ucm_test_casesPackage eINSTANCE = ucm_supplement.ucm_test_cases.impl.Ucm_test_casesPackageImpl.init();

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_test_cases.impl.TestCaseGroupImpl <em>Test Case Group</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_test_cases.impl.TestCaseGroupImpl
	 * @see ucm_supplement.ucm_test_cases.impl.Ucm_test_casesPackageImpl#getTestCaseGroup()
	 * @generated
	 */
	int TEST_CASE_GROUP = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_GROUP__NAME = Ucm_commonsPackage.IAPPLICATION_MODULE__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_GROUP__COMMENT = Ucm_commonsPackage.IAPPLICATION_MODULE__COMMENT;

	/**
	 * The feature id for the '<em><b>Allocation Plan</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_GROUP__ALLOCATION_PLAN = Ucm_commonsPackage.IAPPLICATION_MODULE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Test Case</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_GROUP__TEST_CASE = Ucm_commonsPackage.IAPPLICATION_MODULE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Test Case Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_GROUP_FEATURE_COUNT = Ucm_commonsPackage.IAPPLICATION_MODULE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_test_cases.impl.TestCaseImpl <em>Test Case</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_test_cases.impl.TestCaseImpl
	 * @see ucm_supplement.ucm_test_cases.impl.Ucm_test_casesPackageImpl#getTestCase()
	 * @generated
	 */
	int TEST_CASE = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE__NAME = Ucm_commonsPackage.INAMED__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE__COMMENT = Ucm_commonsPackage.INAMED__COMMENT;

	/**
	 * The feature id for the '<em><b>Stimulus</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE__STIMULUS = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Observation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE__OBSERVATION = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Global Offset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE__GLOBAL_OFFSET = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Test Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_FEATURE_COUNT = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_test_cases.impl.ITestCaseEventImpl <em>ITest Case Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_test_cases.impl.ITestCaseEventImpl
	 * @see ucm_supplement.ucm_test_cases.impl.Ucm_test_casesPackageImpl#getITestCaseEvent()
	 * @generated
	 */
	int ITEST_CASE_EVENT = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITEST_CASE_EVENT__NAME = Ucm_commonsPackage.INAMED__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITEST_CASE_EVENT__COMMENT = Ucm_commonsPackage.INAMED__COMMENT;

	/**
	 * The feature id for the '<em><b>Component Instance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITEST_CASE_EVENT__COMPONENT_INSTANCE = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Port Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITEST_CASE_EVENT__PORT_ELEMENT = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Method</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITEST_CASE_EVENT__METHOD = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Interaction Point</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITEST_CASE_EVENT__INTERACTION_POINT = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>ITest Case Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITEST_CASE_EVENT_FEATURE_COUNT = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_test_cases.impl.IComponentInteractionPointReferenceImpl <em>IComponent Interaction Point Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_test_cases.impl.IComponentInteractionPointReferenceImpl
	 * @see ucm_supplement.ucm_test_cases.impl.Ucm_test_casesPackageImpl#getIComponentInteractionPointReference()
	 * @generated
	 */
	int ICOMPONENT_INTERACTION_POINT_REFERENCE = 3;

	/**
	 * The number of structural features of the '<em>IComponent Interaction Point Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICOMPONENT_INTERACTION_POINT_REFERENCE_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_test_cases.impl.ComponentPortReferenceImpl <em>Component Port Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_test_cases.impl.ComponentPortReferenceImpl
	 * @see ucm_supplement.ucm_test_cases.impl.Ucm_test_casesPackageImpl#getComponentPortReference()
	 * @generated
	 */
	int COMPONENT_PORT_REFERENCE = 4;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_PORT_REFERENCE__PORT = ICOMPONENT_INTERACTION_POINT_REFERENCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Component Port Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_PORT_REFERENCE_FEATURE_COUNT = ICOMPONENT_INTERACTION_POINT_REFERENCE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_test_cases.impl.ComponentMultiplexPortReferenceImpl <em>Component Multiplex Port Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_test_cases.impl.ComponentMultiplexPortReferenceImpl
	 * @see ucm_supplement.ucm_test_cases.impl.Ucm_test_casesPackageImpl#getComponentMultiplexPortReference()
	 * @generated
	 */
	int COMPONENT_MULTIPLEX_PORT_REFERENCE = 5;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_MULTIPLEX_PORT_REFERENCE__PORT = COMPONENT_PORT_REFERENCE__PORT;

	/**
	 * The feature id for the '<em><b>Binding Name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_MULTIPLEX_PORT_REFERENCE__BINDING_NAME = COMPONENT_PORT_REFERENCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Component Multiplex Port Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_MULTIPLEX_PORT_REFERENCE_FEATURE_COUNT = COMPONENT_PORT_REFERENCE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_test_cases.impl.TechnicalPolicyReferenceImpl <em>Technical Policy Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_test_cases.impl.TechnicalPolicyReferenceImpl
	 * @see ucm_supplement.ucm_test_cases.impl.Ucm_test_casesPackageImpl#getTechnicalPolicyReference()
	 * @generated
	 */
	int TECHNICAL_POLICY_REFERENCE = 6;

	/**
	 * The feature id for the '<em><b>Technical Policy</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TECHNICAL_POLICY_REFERENCE__TECHNICAL_POLICY = ICOMPONENT_INTERACTION_POINT_REFERENCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Technical Policy Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TECHNICAL_POLICY_REFERENCE_FEATURE_COUNT = ICOMPONENT_INTERACTION_POINT_REFERENCE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_test_cases.impl.TestCaseStimulusImpl <em>Test Case Stimulus</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_test_cases.impl.TestCaseStimulusImpl
	 * @see ucm_supplement.ucm_test_cases.impl.Ucm_test_casesPackageImpl#getTestCaseStimulus()
	 * @generated
	 */
	int TEST_CASE_STIMULUS = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_STIMULUS__NAME = ITEST_CASE_EVENT__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_STIMULUS__COMMENT = ITEST_CASE_EVENT__COMMENT;

	/**
	 * The feature id for the '<em><b>Component Instance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_STIMULUS__COMPONENT_INSTANCE = ITEST_CASE_EVENT__COMPONENT_INSTANCE;

	/**
	 * The feature id for the '<em><b>Port Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_STIMULUS__PORT_ELEMENT = ITEST_CASE_EVENT__PORT_ELEMENT;

	/**
	 * The feature id for the '<em><b>Method</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_STIMULUS__METHOD = ITEST_CASE_EVENT__METHOD;

	/**
	 * The feature id for the '<em><b>Interaction Point</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_STIMULUS__INTERACTION_POINT = ITEST_CASE_EVENT__INTERACTION_POINT;

	/**
	 * The feature id for the '<em><b>Delay</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_STIMULUS__DELAY = ITEST_CASE_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Test Case Stimulus</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_STIMULUS_FEATURE_COUNT = ITEST_CASE_EVENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_test_cases.impl.TestCaseObservationImpl <em>Test Case Observation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_test_cases.impl.TestCaseObservationImpl
	 * @see ucm_supplement.ucm_test_cases.impl.Ucm_test_casesPackageImpl#getTestCaseObservation()
	 * @generated
	 */
	int TEST_CASE_OBSERVATION = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_OBSERVATION__NAME = ITEST_CASE_EVENT__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_OBSERVATION__COMMENT = ITEST_CASE_EVENT__COMMENT;

	/**
	 * The feature id for the '<em><b>Component Instance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_OBSERVATION__COMPONENT_INSTANCE = ITEST_CASE_EVENT__COMPONENT_INSTANCE;

	/**
	 * The feature id for the '<em><b>Port Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_OBSERVATION__PORT_ELEMENT = ITEST_CASE_EVENT__PORT_ELEMENT;

	/**
	 * The feature id for the '<em><b>Method</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_OBSERVATION__METHOD = ITEST_CASE_EVENT__METHOD;

	/**
	 * The feature id for the '<em><b>Interaction Point</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_OBSERVATION__INTERACTION_POINT = ITEST_CASE_EVENT__INTERACTION_POINT;

	/**
	 * The feature id for the '<em><b>Min Delay</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_OBSERVATION__MIN_DELAY = ITEST_CASE_EVENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Max Delay</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_OBSERVATION__MAX_DELAY = ITEST_CASE_EVENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Order</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_OBSERVATION__ORDER = ITEST_CASE_EVENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Optional</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_OBSERVATION__OPTIONAL = ITEST_CASE_EVENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Test Case Observation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_OBSERVATION_FEATURE_COUNT = ITEST_CASE_EVENT_FEATURE_COUNT + 4;


	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_test_cases.TestCaseGroup <em>Test Case Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Test Case Group</em>'.
	 * @see ucm_supplement.ucm_test_cases.TestCaseGroup
	 * @generated
	 */
	EClass getTestCaseGroup();

	/**
	 * Returns the meta object for the reference list '{@link ucm_supplement.ucm_test_cases.TestCaseGroup#getAllocationPlan <em>Allocation Plan</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Allocation Plan</em>'.
	 * @see ucm_supplement.ucm_test_cases.TestCaseGroup#getAllocationPlan()
	 * @see #getTestCaseGroup()
	 * @generated
	 */
	EReference getTestCaseGroup_AllocationPlan();

	/**
	 * Returns the meta object for the containment reference list '{@link ucm_supplement.ucm_test_cases.TestCaseGroup#getTestCase <em>Test Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Test Case</em>'.
	 * @see ucm_supplement.ucm_test_cases.TestCaseGroup#getTestCase()
	 * @see #getTestCaseGroup()
	 * @generated
	 */
	EReference getTestCaseGroup_TestCase();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_test_cases.TestCase <em>Test Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Test Case</em>'.
	 * @see ucm_supplement.ucm_test_cases.TestCase
	 * @generated
	 */
	EClass getTestCase();

	/**
	 * Returns the meta object for the containment reference '{@link ucm_supplement.ucm_test_cases.TestCase#getStimulus <em>Stimulus</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Stimulus</em>'.
	 * @see ucm_supplement.ucm_test_cases.TestCase#getStimulus()
	 * @see #getTestCase()
	 * @generated
	 */
	EReference getTestCase_Stimulus();

	/**
	 * Returns the meta object for the containment reference list '{@link ucm_supplement.ucm_test_cases.TestCase#getObservation <em>Observation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Observation</em>'.
	 * @see ucm_supplement.ucm_test_cases.TestCase#getObservation()
	 * @see #getTestCase()
	 * @generated
	 */
	EReference getTestCase_Observation();

	/**
	 * Returns the meta object for the attribute '{@link ucm_supplement.ucm_test_cases.TestCase#getGlobalOffset <em>Global Offset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Global Offset</em>'.
	 * @see ucm_supplement.ucm_test_cases.TestCase#getGlobalOffset()
	 * @see #getTestCase()
	 * @generated
	 */
	EAttribute getTestCase_GlobalOffset();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_test_cases.ITestCaseEvent <em>ITest Case Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ITest Case Event</em>'.
	 * @see ucm_supplement.ucm_test_cases.ITestCaseEvent
	 * @generated
	 */
	EClass getITestCaseEvent();

	/**
	 * Returns the meta object for the containment reference '{@link ucm_supplement.ucm_test_cases.ITestCaseEvent#getComponentInstance <em>Component Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Component Instance</em>'.
	 * @see ucm_supplement.ucm_test_cases.ITestCaseEvent#getComponentInstance()
	 * @see #getITestCaseEvent()
	 * @generated
	 */
	EReference getITestCaseEvent_ComponentInstance();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_test_cases.ITestCaseEvent#getPortElement <em>Port Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Port Element</em>'.
	 * @see ucm_supplement.ucm_test_cases.ITestCaseEvent#getPortElement()
	 * @see #getITestCaseEvent()
	 * @generated
	 */
	EReference getITestCaseEvent_PortElement();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_test_cases.ITestCaseEvent#getMethod <em>Method</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Method</em>'.
	 * @see ucm_supplement.ucm_test_cases.ITestCaseEvent#getMethod()
	 * @see #getITestCaseEvent()
	 * @generated
	 */
	EReference getITestCaseEvent_Method();

	/**
	 * Returns the meta object for the containment reference '{@link ucm_supplement.ucm_test_cases.ITestCaseEvent#getInteractionPoint <em>Interaction Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Interaction Point</em>'.
	 * @see ucm_supplement.ucm_test_cases.ITestCaseEvent#getInteractionPoint()
	 * @see #getITestCaseEvent()
	 * @generated
	 */
	EReference getITestCaseEvent_InteractionPoint();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_test_cases.IComponentInteractionPointReference <em>IComponent Interaction Point Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IComponent Interaction Point Reference</em>'.
	 * @see ucm_supplement.ucm_test_cases.IComponentInteractionPointReference
	 * @generated
	 */
	EClass getIComponentInteractionPointReference();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_test_cases.ComponentPortReference <em>Component Port Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component Port Reference</em>'.
	 * @see ucm_supplement.ucm_test_cases.ComponentPortReference
	 * @generated
	 */
	EClass getComponentPortReference();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_test_cases.ComponentPortReference#getPort <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Port</em>'.
	 * @see ucm_supplement.ucm_test_cases.ComponentPortReference#getPort()
	 * @see #getComponentPortReference()
	 * @generated
	 */
	EReference getComponentPortReference_Port();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_test_cases.ComponentMultiplexPortReference <em>Component Multiplex Port Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component Multiplex Port Reference</em>'.
	 * @see ucm_supplement.ucm_test_cases.ComponentMultiplexPortReference
	 * @generated
	 */
	EClass getComponentMultiplexPortReference();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_test_cases.ComponentMultiplexPortReference#getBindingName <em>Binding Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Binding Name</em>'.
	 * @see ucm_supplement.ucm_test_cases.ComponentMultiplexPortReference#getBindingName()
	 * @see #getComponentMultiplexPortReference()
	 * @generated
	 */
	EReference getComponentMultiplexPortReference_BindingName();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_test_cases.TechnicalPolicyReference <em>Technical Policy Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Technical Policy Reference</em>'.
	 * @see ucm_supplement.ucm_test_cases.TechnicalPolicyReference
	 * @generated
	 */
	EClass getTechnicalPolicyReference();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_test_cases.TechnicalPolicyReference#getTechnicalPolicy <em>Technical Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Technical Policy</em>'.
	 * @see ucm_supplement.ucm_test_cases.TechnicalPolicyReference#getTechnicalPolicy()
	 * @see #getTechnicalPolicyReference()
	 * @generated
	 */
	EReference getTechnicalPolicyReference_TechnicalPolicy();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_test_cases.TestCaseStimulus <em>Test Case Stimulus</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Test Case Stimulus</em>'.
	 * @see ucm_supplement.ucm_test_cases.TestCaseStimulus
	 * @generated
	 */
	EClass getTestCaseStimulus();

	/**
	 * Returns the meta object for the attribute '{@link ucm_supplement.ucm_test_cases.TestCaseStimulus#getDelay <em>Delay</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Delay</em>'.
	 * @see ucm_supplement.ucm_test_cases.TestCaseStimulus#getDelay()
	 * @see #getTestCaseStimulus()
	 * @generated
	 */
	EAttribute getTestCaseStimulus_Delay();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_test_cases.TestCaseObservation <em>Test Case Observation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Test Case Observation</em>'.
	 * @see ucm_supplement.ucm_test_cases.TestCaseObservation
	 * @generated
	 */
	EClass getTestCaseObservation();

	/**
	 * Returns the meta object for the attribute '{@link ucm_supplement.ucm_test_cases.TestCaseObservation#getMinDelay <em>Min Delay</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min Delay</em>'.
	 * @see ucm_supplement.ucm_test_cases.TestCaseObservation#getMinDelay()
	 * @see #getTestCaseObservation()
	 * @generated
	 */
	EAttribute getTestCaseObservation_MinDelay();

	/**
	 * Returns the meta object for the attribute '{@link ucm_supplement.ucm_test_cases.TestCaseObservation#getMaxDelay <em>Max Delay</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Delay</em>'.
	 * @see ucm_supplement.ucm_test_cases.TestCaseObservation#getMaxDelay()
	 * @see #getTestCaseObservation()
	 * @generated
	 */
	EAttribute getTestCaseObservation_MaxDelay();

	/**
	 * Returns the meta object for the attribute '{@link ucm_supplement.ucm_test_cases.TestCaseObservation#getOrder <em>Order</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Order</em>'.
	 * @see ucm_supplement.ucm_test_cases.TestCaseObservation#getOrder()
	 * @see #getTestCaseObservation()
	 * @generated
	 */
	EAttribute getTestCaseObservation_Order();

	/**
	 * Returns the meta object for the attribute '{@link ucm_supplement.ucm_test_cases.TestCaseObservation#isOptional <em>Optional</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Optional</em>'.
	 * @see ucm_supplement.ucm_test_cases.TestCaseObservation#isOptional()
	 * @see #getTestCaseObservation()
	 * @generated
	 */
	EAttribute getTestCaseObservation_Optional();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	Ucm_test_casesFactory getUcm_test_casesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_test_cases.impl.TestCaseGroupImpl <em>Test Case Group</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_test_cases.impl.TestCaseGroupImpl
		 * @see ucm_supplement.ucm_test_cases.impl.Ucm_test_casesPackageImpl#getTestCaseGroup()
		 * @generated
		 */
		EClass TEST_CASE_GROUP = eINSTANCE.getTestCaseGroup();

		/**
		 * The meta object literal for the '<em><b>Allocation Plan</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEST_CASE_GROUP__ALLOCATION_PLAN = eINSTANCE.getTestCaseGroup_AllocationPlan();

		/**
		 * The meta object literal for the '<em><b>Test Case</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEST_CASE_GROUP__TEST_CASE = eINSTANCE.getTestCaseGroup_TestCase();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_test_cases.impl.TestCaseImpl <em>Test Case</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_test_cases.impl.TestCaseImpl
		 * @see ucm_supplement.ucm_test_cases.impl.Ucm_test_casesPackageImpl#getTestCase()
		 * @generated
		 */
		EClass TEST_CASE = eINSTANCE.getTestCase();

		/**
		 * The meta object literal for the '<em><b>Stimulus</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEST_CASE__STIMULUS = eINSTANCE.getTestCase_Stimulus();

		/**
		 * The meta object literal for the '<em><b>Observation</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEST_CASE__OBSERVATION = eINSTANCE.getTestCase_Observation();

		/**
		 * The meta object literal for the '<em><b>Global Offset</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEST_CASE__GLOBAL_OFFSET = eINSTANCE.getTestCase_GlobalOffset();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_test_cases.impl.ITestCaseEventImpl <em>ITest Case Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_test_cases.impl.ITestCaseEventImpl
		 * @see ucm_supplement.ucm_test_cases.impl.Ucm_test_casesPackageImpl#getITestCaseEvent()
		 * @generated
		 */
		EClass ITEST_CASE_EVENT = eINSTANCE.getITestCaseEvent();

		/**
		 * The meta object literal for the '<em><b>Component Instance</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ITEST_CASE_EVENT__COMPONENT_INSTANCE = eINSTANCE.getITestCaseEvent_ComponentInstance();

		/**
		 * The meta object literal for the '<em><b>Port Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ITEST_CASE_EVENT__PORT_ELEMENT = eINSTANCE.getITestCaseEvent_PortElement();

		/**
		 * The meta object literal for the '<em><b>Method</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ITEST_CASE_EVENT__METHOD = eINSTANCE.getITestCaseEvent_Method();

		/**
		 * The meta object literal for the '<em><b>Interaction Point</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ITEST_CASE_EVENT__INTERACTION_POINT = eINSTANCE.getITestCaseEvent_InteractionPoint();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_test_cases.impl.IComponentInteractionPointReferenceImpl <em>IComponent Interaction Point Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_test_cases.impl.IComponentInteractionPointReferenceImpl
		 * @see ucm_supplement.ucm_test_cases.impl.Ucm_test_casesPackageImpl#getIComponentInteractionPointReference()
		 * @generated
		 */
		EClass ICOMPONENT_INTERACTION_POINT_REFERENCE = eINSTANCE.getIComponentInteractionPointReference();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_test_cases.impl.ComponentPortReferenceImpl <em>Component Port Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_test_cases.impl.ComponentPortReferenceImpl
		 * @see ucm_supplement.ucm_test_cases.impl.Ucm_test_casesPackageImpl#getComponentPortReference()
		 * @generated
		 */
		EClass COMPONENT_PORT_REFERENCE = eINSTANCE.getComponentPortReference();

		/**
		 * The meta object literal for the '<em><b>Port</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_PORT_REFERENCE__PORT = eINSTANCE.getComponentPortReference_Port();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_test_cases.impl.ComponentMultiplexPortReferenceImpl <em>Component Multiplex Port Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_test_cases.impl.ComponentMultiplexPortReferenceImpl
		 * @see ucm_supplement.ucm_test_cases.impl.Ucm_test_casesPackageImpl#getComponentMultiplexPortReference()
		 * @generated
		 */
		EClass COMPONENT_MULTIPLEX_PORT_REFERENCE = eINSTANCE.getComponentMultiplexPortReference();

		/**
		 * The meta object literal for the '<em><b>Binding Name</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_MULTIPLEX_PORT_REFERENCE__BINDING_NAME = eINSTANCE.getComponentMultiplexPortReference_BindingName();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_test_cases.impl.TechnicalPolicyReferenceImpl <em>Technical Policy Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_test_cases.impl.TechnicalPolicyReferenceImpl
		 * @see ucm_supplement.ucm_test_cases.impl.Ucm_test_casesPackageImpl#getTechnicalPolicyReference()
		 * @generated
		 */
		EClass TECHNICAL_POLICY_REFERENCE = eINSTANCE.getTechnicalPolicyReference();

		/**
		 * The meta object literal for the '<em><b>Technical Policy</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TECHNICAL_POLICY_REFERENCE__TECHNICAL_POLICY = eINSTANCE.getTechnicalPolicyReference_TechnicalPolicy();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_test_cases.impl.TestCaseStimulusImpl <em>Test Case Stimulus</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_test_cases.impl.TestCaseStimulusImpl
		 * @see ucm_supplement.ucm_test_cases.impl.Ucm_test_casesPackageImpl#getTestCaseStimulus()
		 * @generated
		 */
		EClass TEST_CASE_STIMULUS = eINSTANCE.getTestCaseStimulus();

		/**
		 * The meta object literal for the '<em><b>Delay</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEST_CASE_STIMULUS__DELAY = eINSTANCE.getTestCaseStimulus_Delay();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_test_cases.impl.TestCaseObservationImpl <em>Test Case Observation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_test_cases.impl.TestCaseObservationImpl
		 * @see ucm_supplement.ucm_test_cases.impl.Ucm_test_casesPackageImpl#getTestCaseObservation()
		 * @generated
		 */
		EClass TEST_CASE_OBSERVATION = eINSTANCE.getTestCaseObservation();

		/**
		 * The meta object literal for the '<em><b>Min Delay</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEST_CASE_OBSERVATION__MIN_DELAY = eINSTANCE.getTestCaseObservation_MinDelay();

		/**
		 * The meta object literal for the '<em><b>Max Delay</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEST_CASE_OBSERVATION__MAX_DELAY = eINSTANCE.getTestCaseObservation_MaxDelay();

		/**
		 * The meta object literal for the '<em><b>Order</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEST_CASE_OBSERVATION__ORDER = eINSTANCE.getTestCaseObservation_Order();

		/**
		 * The meta object literal for the '<em><b>Optional</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEST_CASE_OBSERVATION__OPTIONAL = eINSTANCE.getTestCaseObservation_Optional();

	}

} //Ucm_test_casesPackage
