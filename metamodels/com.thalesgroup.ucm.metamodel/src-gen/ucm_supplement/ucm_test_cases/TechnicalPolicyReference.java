/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_test_cases;

import ucm_base.ucm_components.ComponentTechnicalPolicy;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Technical Policy Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_test_cases.TechnicalPolicyReference#getTechnicalPolicy <em>Technical Policy</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_test_cases.Ucm_test_casesPackage#getTechnicalPolicyReference()
 * @model
 * @generated
 */
public interface TechnicalPolicyReference extends IComponentInteractionPointReference {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Technical Policy</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Technical Policy</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Technical Policy</em>' reference.
	 * @see #setTechnicalPolicy(ComponentTechnicalPolicy)
	 * @see ucm_supplement.ucm_test_cases.Ucm_test_casesPackage#getTechnicalPolicyReference_TechnicalPolicy()
	 * @model required="true"
	 * @generated
	 */
	ComponentTechnicalPolicy getTechnicalPolicy();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_test_cases.TechnicalPolicyReference#getTechnicalPolicy <em>Technical Policy</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Technical Policy</em>' reference.
	 * @see #getTechnicalPolicy()
	 * @generated
	 */
	void setTechnicalPolicy(ComponentTechnicalPolicy value);

} // TechnicalPolicyReference
