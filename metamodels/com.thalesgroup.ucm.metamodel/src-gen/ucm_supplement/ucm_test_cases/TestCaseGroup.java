/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_test_cases;

import org.eclipse.emf.common.util.EList;
import ucm_base.ucm_commons.IApplicationModule;
import ucm_supplement.ucm_deployments.AllocationPlan;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Test Case Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_test_cases.TestCaseGroup#getAllocationPlan <em>Allocation Plan</em>}</li>
 *   <li>{@link ucm_supplement.ucm_test_cases.TestCaseGroup#getTestCase <em>Test Case</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_test_cases.Ucm_test_casesPackage#getTestCaseGroup()
 * @model
 * @generated
 */
public interface TestCaseGroup extends IApplicationModule {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Allocation Plan</b></em>' reference list.
	 * The list contents are of type {@link ucm_supplement.ucm_deployments.AllocationPlan}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Allocation Plan</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Allocation Plan</em>' reference list.
	 * @see ucm_supplement.ucm_test_cases.Ucm_test_casesPackage#getTestCaseGroup_AllocationPlan()
	 * @model
	 * @generated
	 */
	EList<AllocationPlan> getAllocationPlan();

	/**
	 * Returns the value of the '<em><b>Test Case</b></em>' containment reference list.
	 * The list contents are of type {@link ucm_supplement.ucm_test_cases.TestCase}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Test Case</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Test Case</em>' containment reference list.
	 * @see ucm_supplement.ucm_test_cases.Ucm_test_casesPackage#getTestCaseGroup_TestCase()
	 * @model containment="true"
	 * @generated
	 */
	EList<TestCase> getTestCase();

} // TestCaseGroup
