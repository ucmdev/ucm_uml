/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_test_cases.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;
import ucm_base.ucm_commons.IApplicationModule;
import ucm_base.ucm_commons.IModule;
import ucm_base.ucm_commons.INamed;

import ucm_supplement.ucm_test_cases.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see ucm_supplement.ucm_test_cases.Ucm_test_casesPackage
 * @generated
 */
public class Ucm_test_casesAdapterFactory extends AdapterFactoryImpl {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static Ucm_test_casesPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ucm_test_casesAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = Ucm_test_casesPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Ucm_test_casesSwitch<Adapter> modelSwitch =
		new Ucm_test_casesSwitch<Adapter>() {
			@Override
			public Adapter caseTestCaseGroup(TestCaseGroup object) {
				return createTestCaseGroupAdapter();
			}
			@Override
			public Adapter caseTestCase(TestCase object) {
				return createTestCaseAdapter();
			}
			@Override
			public Adapter caseITestCaseEvent(ITestCaseEvent object) {
				return createITestCaseEventAdapter();
			}
			@Override
			public Adapter caseIComponentInteractionPointReference(IComponentInteractionPointReference object) {
				return createIComponentInteractionPointReferenceAdapter();
			}
			@Override
			public Adapter caseComponentPortReference(ComponentPortReference object) {
				return createComponentPortReferenceAdapter();
			}
			@Override
			public Adapter caseComponentMultiplexPortReference(ComponentMultiplexPortReference object) {
				return createComponentMultiplexPortReferenceAdapter();
			}
			@Override
			public Adapter caseTechnicalPolicyReference(TechnicalPolicyReference object) {
				return createTechnicalPolicyReferenceAdapter();
			}
			@Override
			public Adapter caseTestCaseStimulus(TestCaseStimulus object) {
				return createTestCaseStimulusAdapter();
			}
			@Override
			public Adapter caseTestCaseObservation(TestCaseObservation object) {
				return createTestCaseObservationAdapter();
			}
			@Override
			public Adapter caseINamed(INamed object) {
				return createINamedAdapter();
			}
			@Override
			public Adapter caseIModule(IModule object) {
				return createIModuleAdapter();
			}
			@Override
			public Adapter caseIApplicationModule(IApplicationModule object) {
				return createIApplicationModuleAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link ucm_supplement.ucm_test_cases.TestCaseGroup <em>Test Case Group</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_supplement.ucm_test_cases.TestCaseGroup
	 * @generated
	 */
	public Adapter createTestCaseGroupAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_supplement.ucm_test_cases.TestCase <em>Test Case</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_supplement.ucm_test_cases.TestCase
	 * @generated
	 */
	public Adapter createTestCaseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_supplement.ucm_test_cases.ITestCaseEvent <em>ITest Case Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_supplement.ucm_test_cases.ITestCaseEvent
	 * @generated
	 */
	public Adapter createITestCaseEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_supplement.ucm_test_cases.IComponentInteractionPointReference <em>IComponent Interaction Point Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_supplement.ucm_test_cases.IComponentInteractionPointReference
	 * @generated
	 */
	public Adapter createIComponentInteractionPointReferenceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_supplement.ucm_test_cases.ComponentPortReference <em>Component Port Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_supplement.ucm_test_cases.ComponentPortReference
	 * @generated
	 */
	public Adapter createComponentPortReferenceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_supplement.ucm_test_cases.ComponentMultiplexPortReference <em>Component Multiplex Port Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_supplement.ucm_test_cases.ComponentMultiplexPortReference
	 * @generated
	 */
	public Adapter createComponentMultiplexPortReferenceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_supplement.ucm_test_cases.TechnicalPolicyReference <em>Technical Policy Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_supplement.ucm_test_cases.TechnicalPolicyReference
	 * @generated
	 */
	public Adapter createTechnicalPolicyReferenceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_supplement.ucm_test_cases.TestCaseStimulus <em>Test Case Stimulus</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_supplement.ucm_test_cases.TestCaseStimulus
	 * @generated
	 */
	public Adapter createTestCaseStimulusAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_supplement.ucm_test_cases.TestCaseObservation <em>Test Case Observation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_supplement.ucm_test_cases.TestCaseObservation
	 * @generated
	 */
	public Adapter createTestCaseObservationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_base.ucm_commons.INamed <em>INamed</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_base.ucm_commons.INamed
	 * @generated
	 */
	public Adapter createINamedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_base.ucm_commons.IModule <em>IModule</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_base.ucm_commons.IModule
	 * @generated
	 */
	public Adapter createIModuleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_base.ucm_commons.IApplicationModule <em>IApplication Module</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_base.ucm_commons.IApplicationModule
	 * @generated
	 */
	public Adapter createIApplicationModuleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //Ucm_test_casesAdapterFactory
