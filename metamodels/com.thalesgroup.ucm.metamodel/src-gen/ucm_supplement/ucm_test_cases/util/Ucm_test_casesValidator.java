/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_test_cases.util;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

import ucm_supplement.ucm_test_cases.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see ucm_supplement.ucm_test_cases.Ucm_test_casesPackage
 * @generated
 */
public class Ucm_test_casesValidator extends EObjectValidator {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final Ucm_test_casesValidator INSTANCE = new Ucm_test_casesValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "ucm_supplement.ucm_test_cases";

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Component Instance Location' of 'ITest Case Event'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int ITEST_CASE_EVENT__COMPONENT_INSTANCE_LOCATION = 1;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Method And Port Element Consistency' of 'ITest Case Event'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int ITEST_CASE_EVENT__METHOD_AND_PORT_ELEMENT_CONSISTENCY = 2;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Value Consistency' of 'Test Case Observation'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int TEST_CASE_OBSERVATION__VALUE_CONSISTENCY = 3;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 3;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ucm_test_casesValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return Ucm_test_casesPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case Ucm_test_casesPackage.TEST_CASE_GROUP:
				return validateTestCaseGroup((TestCaseGroup)value, diagnostics, context);
			case Ucm_test_casesPackage.TEST_CASE:
				return validateTestCase((TestCase)value, diagnostics, context);
			case Ucm_test_casesPackage.ITEST_CASE_EVENT:
				return validateITestCaseEvent((ITestCaseEvent)value, diagnostics, context);
			case Ucm_test_casesPackage.ICOMPONENT_INTERACTION_POINT_REFERENCE:
				return validateIComponentInteractionPointReference((IComponentInteractionPointReference)value, diagnostics, context);
			case Ucm_test_casesPackage.COMPONENT_PORT_REFERENCE:
				return validateComponentPortReference((ComponentPortReference)value, diagnostics, context);
			case Ucm_test_casesPackage.COMPONENT_MULTIPLEX_PORT_REFERENCE:
				return validateComponentMultiplexPortReference((ComponentMultiplexPortReference)value, diagnostics, context);
			case Ucm_test_casesPackage.TECHNICAL_POLICY_REFERENCE:
				return validateTechnicalPolicyReference((TechnicalPolicyReference)value, diagnostics, context);
			case Ucm_test_casesPackage.TEST_CASE_STIMULUS:
				return validateTestCaseStimulus((TestCaseStimulus)value, diagnostics, context);
			case Ucm_test_casesPackage.TEST_CASE_OBSERVATION:
				return validateTestCaseObservation((TestCaseObservation)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTestCaseGroup(TestCaseGroup testCaseGroup, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(testCaseGroup, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTestCase(TestCase testCase, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(testCase, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateITestCaseEvent(ITestCaseEvent iTestCaseEvent, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(iTestCaseEvent, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(iTestCaseEvent, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(iTestCaseEvent, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(iTestCaseEvent, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(iTestCaseEvent, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(iTestCaseEvent, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(iTestCaseEvent, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(iTestCaseEvent, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(iTestCaseEvent, diagnostics, context);
		if (result || diagnostics != null) result &= validateITestCaseEvent_componentInstanceLocation(iTestCaseEvent, diagnostics, context);
		if (result || diagnostics != null) result &= validateITestCaseEvent_methodAndPortElementConsistency(iTestCaseEvent, diagnostics, context);
		return result;
	}

	/**
	 * Validates the componentInstanceLocation constraint of '<em>ITest Case Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateITestCaseEvent_componentInstanceLocation(ITestCaseEvent iTestCaseEvent, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return iTestCaseEvent.componentInstanceLocation(diagnostics, context);
	}

	/**
	 * Validates the methodAndPortElementConsistency constraint of '<em>ITest Case Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateITestCaseEvent_methodAndPortElementConsistency(ITestCaseEvent iTestCaseEvent, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return iTestCaseEvent.methodAndPortElementConsistency(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIComponentInteractionPointReference(IComponentInteractionPointReference iComponentInteractionPointReference, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(iComponentInteractionPointReference, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComponentPortReference(ComponentPortReference componentPortReference, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(componentPortReference, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComponentMultiplexPortReference(ComponentMultiplexPortReference componentMultiplexPortReference, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(componentMultiplexPortReference, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTechnicalPolicyReference(TechnicalPolicyReference technicalPolicyReference, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(technicalPolicyReference, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTestCaseStimulus(TestCaseStimulus testCaseStimulus, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(testCaseStimulus, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(testCaseStimulus, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(testCaseStimulus, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(testCaseStimulus, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(testCaseStimulus, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(testCaseStimulus, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(testCaseStimulus, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(testCaseStimulus, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(testCaseStimulus, diagnostics, context);
		if (result || diagnostics != null) result &= validateITestCaseEvent_componentInstanceLocation(testCaseStimulus, diagnostics, context);
		if (result || diagnostics != null) result &= validateITestCaseEvent_methodAndPortElementConsistency(testCaseStimulus, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTestCaseObservation(TestCaseObservation testCaseObservation, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(testCaseObservation, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(testCaseObservation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(testCaseObservation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(testCaseObservation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(testCaseObservation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(testCaseObservation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(testCaseObservation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(testCaseObservation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(testCaseObservation, diagnostics, context);
		if (result || diagnostics != null) result &= validateITestCaseEvent_componentInstanceLocation(testCaseObservation, diagnostics, context);
		if (result || diagnostics != null) result &= validateITestCaseEvent_methodAndPortElementConsistency(testCaseObservation, diagnostics, context);
		if (result || diagnostics != null) result &= validateTestCaseObservation_valueConsistency(testCaseObservation, diagnostics, context);
		return result;
	}

	/**
	 * Validates the valueConsistency constraint of '<em>Test Case Observation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTestCaseObservation_valueConsistency(TestCaseObservation testCaseObservation, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return testCaseObservation.valueConsistency(diagnostics, context);
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //Ucm_test_casesValidator
