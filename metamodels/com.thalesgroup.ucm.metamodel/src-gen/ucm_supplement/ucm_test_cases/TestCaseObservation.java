/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_test_cases;

import java.util.Map;
import org.eclipse.emf.common.util.DiagnosticChain;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Test Case Observation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_test_cases.TestCaseObservation#getMinDelay <em>Min Delay</em>}</li>
 *   <li>{@link ucm_supplement.ucm_test_cases.TestCaseObservation#getMaxDelay <em>Max Delay</em>}</li>
 *   <li>{@link ucm_supplement.ucm_test_cases.TestCaseObservation#getOrder <em>Order</em>}</li>
 *   <li>{@link ucm_supplement.ucm_test_cases.TestCaseObservation#isOptional <em>Optional</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_test_cases.Ucm_test_casesPackage#getTestCaseObservation()
 * @model
 * @generated
 */
public interface TestCaseObservation extends ITestCaseEvent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Min Delay</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min Delay</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Delay</em>' attribute.
	 * @see #setMinDelay(long)
	 * @see ucm_supplement.ucm_test_cases.Ucm_test_casesPackage#getTestCaseObservation_MinDelay()
	 * @model required="true"
	 * @generated
	 */
	long getMinDelay();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_test_cases.TestCaseObservation#getMinDelay <em>Min Delay</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Delay</em>' attribute.
	 * @see #getMinDelay()
	 * @generated
	 */
	void setMinDelay(long value);

	/**
	 * Returns the value of the '<em><b>Max Delay</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Delay</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Delay</em>' attribute.
	 * @see #setMaxDelay(long)
	 * @see ucm_supplement.ucm_test_cases.Ucm_test_casesPackage#getTestCaseObservation_MaxDelay()
	 * @model required="true"
	 * @generated
	 */
	long getMaxDelay();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_test_cases.TestCaseObservation#getMaxDelay <em>Max Delay</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Delay</em>' attribute.
	 * @see #getMaxDelay()
	 * @generated
	 */
	void setMaxDelay(long value);

	/**
	 * Returns the value of the '<em><b>Order</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Order</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Order</em>' attribute.
	 * @see #setOrder(long)
	 * @see ucm_supplement.ucm_test_cases.Ucm_test_casesPackage#getTestCaseObservation_Order()
	 * @model required="true"
	 * @generated
	 */
	long getOrder();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_test_cases.TestCaseObservation#getOrder <em>Order</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Order</em>' attribute.
	 * @see #getOrder()
	 * @generated
	 */
	void setOrder(long value);

	/**
	 * Returns the value of the '<em><b>Optional</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Optional</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Optional</em>' attribute.
	 * @see #setOptional(boolean)
	 * @see ucm_supplement.ucm_test_cases.Ucm_test_casesPackage#getTestCaseObservation_Optional()
	 * @model required="true"
	 * @generated
	 */
	boolean isOptional();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_test_cases.TestCaseObservation#isOptional <em>Optional</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Optional</em>' attribute.
	 * @see #isOptional()
	 * @generated
	 */
	void setOptional(boolean value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean valueConsistency(DiagnosticChain diagnostics, Map<Object, Object> context);

} // TestCaseObservation
