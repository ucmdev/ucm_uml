/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_test_cases;

import java.util.Map;
import org.eclipse.emf.common.util.DiagnosticChain;
import ucm_base.ucm_commons.INamed;

import ucm_base.ucm_contracts.Method;

import ucm_base.ucm_interactions.PortElement;
import ucm_supplement.ucm_deployments.ComponentInstance;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ITest Case Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_test_cases.ITestCaseEvent#getComponentInstance <em>Component Instance</em>}</li>
 *   <li>{@link ucm_supplement.ucm_test_cases.ITestCaseEvent#getPortElement <em>Port Element</em>}</li>
 *   <li>{@link ucm_supplement.ucm_test_cases.ITestCaseEvent#getMethod <em>Method</em>}</li>
 *   <li>{@link ucm_supplement.ucm_test_cases.ITestCaseEvent#getInteractionPoint <em>Interaction Point</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_test_cases.Ucm_test_casesPackage#getITestCaseEvent()
 * @model abstract="true"
 * @generated
 */
public interface ITestCaseEvent extends INamed {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Component Instance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Instance</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Instance</em>' containment reference.
	 * @see #setComponentInstance(ComponentInstance)
	 * @see ucm_supplement.ucm_test_cases.Ucm_test_casesPackage#getITestCaseEvent_ComponentInstance()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ComponentInstance getComponentInstance();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_test_cases.ITestCaseEvent#getComponentInstance <em>Component Instance</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component Instance</em>' containment reference.
	 * @see #getComponentInstance()
	 * @generated
	 */
	void setComponentInstance(ComponentInstance value);

	/**
	 * Returns the value of the '<em><b>Port Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port Element</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port Element</em>' reference.
	 * @see #setPortElement(PortElement)
	 * @see ucm_supplement.ucm_test_cases.Ucm_test_casesPackage#getITestCaseEvent_PortElement()
	 * @model
	 * @generated
	 */
	PortElement getPortElement();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_test_cases.ITestCaseEvent#getPortElement <em>Port Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port Element</em>' reference.
	 * @see #getPortElement()
	 * @generated
	 */
	void setPortElement(PortElement value);

	/**
	 * Returns the value of the '<em><b>Method</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Method</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Method</em>' reference.
	 * @see #setMethod(Method)
	 * @see ucm_supplement.ucm_test_cases.Ucm_test_casesPackage#getITestCaseEvent_Method()
	 * @model
	 * @generated
	 */
	Method getMethod();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_test_cases.ITestCaseEvent#getMethod <em>Method</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Method</em>' reference.
	 * @see #getMethod()
	 * @generated
	 */
	void setMethod(Method value);

	/**
	 * Returns the value of the '<em><b>Interaction Point</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interaction Point</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interaction Point</em>' containment reference.
	 * @see #setInteractionPoint(IComponentInteractionPointReference)
	 * @see ucm_supplement.ucm_test_cases.Ucm_test_casesPackage#getITestCaseEvent_InteractionPoint()
	 * @model containment="true" required="true"
	 * @generated
	 */
	IComponentInteractionPointReference getInteractionPoint();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_test_cases.ITestCaseEvent#getInteractionPoint <em>Interaction Point</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interaction Point</em>' containment reference.
	 * @see #getInteractionPoint()
	 * @generated
	 */
	void setInteractionPoint(IComponentInteractionPointReference value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean componentInstanceLocation(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean methodAndPortElementConsistency(DiagnosticChain diagnostics, Map<Object, Object> context);

} // ITestCaseEvent
