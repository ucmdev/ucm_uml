/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_test_cases;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IComponent Interaction Point Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ucm_supplement.ucm_test_cases.Ucm_test_casesPackage#getIComponentInteractionPointReference()
 * @model abstract="true"
 * @generated
 */
public interface IComponentInteractionPointReference extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

} // IComponentInteractionPointReference
