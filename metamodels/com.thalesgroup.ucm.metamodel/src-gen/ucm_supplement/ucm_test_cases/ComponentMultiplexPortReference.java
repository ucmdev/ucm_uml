/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_test_cases;

import ucm_base.ucm_contracts.ucm_datatypes.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component Multiplex Port Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_test_cases.ComponentMultiplexPortReference#getBindingName <em>Binding Name</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_test_cases.Ucm_test_casesPackage#getComponentMultiplexPortReference()
 * @model
 * @generated
 */
public interface ComponentMultiplexPortReference extends ComponentPortReference {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Binding Name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Binding Name</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Binding Name</em>' reference.
	 * @see #setBindingName(Enumerator)
	 * @see ucm_supplement.ucm_test_cases.Ucm_test_casesPackage#getComponentMultiplexPortReference_BindingName()
	 * @model
	 * @generated
	 */
	Enumerator getBindingName();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_test_cases.ComponentMultiplexPortReference#getBindingName <em>Binding Name</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Binding Name</em>' reference.
	 * @see #getBindingName()
	 * @generated
	 */
	void setBindingName(Enumerator value);

} // ComponentMultiplexPortReference
