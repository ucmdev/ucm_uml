/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_test_cases;

import org.eclipse.emf.common.util.EList;

import ucm_base.ucm_commons.INamed;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Test Case</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_test_cases.TestCase#getStimulus <em>Stimulus</em>}</li>
 *   <li>{@link ucm_supplement.ucm_test_cases.TestCase#getObservation <em>Observation</em>}</li>
 *   <li>{@link ucm_supplement.ucm_test_cases.TestCase#getGlobalOffset <em>Global Offset</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_test_cases.Ucm_test_casesPackage#getTestCase()
 * @model
 * @generated
 */
public interface TestCase extends INamed {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Stimulus</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Stimulus</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stimulus</em>' containment reference.
	 * @see #setStimulus(TestCaseStimulus)
	 * @see ucm_supplement.ucm_test_cases.Ucm_test_casesPackage#getTestCase_Stimulus()
	 * @model containment="true"
	 * @generated
	 */
	TestCaseStimulus getStimulus();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_test_cases.TestCase#getStimulus <em>Stimulus</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Stimulus</em>' containment reference.
	 * @see #getStimulus()
	 * @generated
	 */
	void setStimulus(TestCaseStimulus value);

	/**
	 * Returns the value of the '<em><b>Observation</b></em>' containment reference list.
	 * The list contents are of type {@link ucm_supplement.ucm_test_cases.TestCaseObservation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Observation</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Observation</em>' containment reference list.
	 * @see ucm_supplement.ucm_test_cases.Ucm_test_casesPackage#getTestCase_Observation()
	 * @model containment="true"
	 * @generated
	 */
	EList<TestCaseObservation> getObservation();

	/**
	 * Returns the value of the '<em><b>Global Offset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Global Offset</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Global Offset</em>' attribute.
	 * @see #setGlobalOffset(long)
	 * @see ucm_supplement.ucm_test_cases.Ucm_test_casesPackage#getTestCase_GlobalOffset()
	 * @model
	 * @generated
	 */
	long getGlobalOffset();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_test_cases.TestCase#getGlobalOffset <em>Global Offset</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Global Offset</em>' attribute.
	 * @see #getGlobalOffset()
	 * @generated
	 */
	void setGlobalOffset(long value);

} // TestCase
