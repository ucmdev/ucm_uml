/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_test_cases.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import ucm_base.ucm_commons.impl.IApplicationModuleImpl;
import ucm_supplement.ucm_deployments.AllocationPlan;

import ucm_supplement.ucm_test_cases.TestCase;
import ucm_supplement.ucm_test_cases.TestCaseGroup;
import ucm_supplement.ucm_test_cases.Ucm_test_casesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Test Case Group</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_test_cases.impl.TestCaseGroupImpl#getAllocationPlan <em>Allocation Plan</em>}</li>
 *   <li>{@link ucm_supplement.ucm_test_cases.impl.TestCaseGroupImpl#getTestCase <em>Test Case</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TestCaseGroupImpl extends IApplicationModuleImpl implements TestCaseGroup {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached value of the '{@link #getAllocationPlan() <em>Allocation Plan</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAllocationPlan()
	 * @generated
	 * @ordered
	 */
	protected EList<AllocationPlan> allocationPlan;

	/**
	 * The cached value of the '{@link #getTestCase() <em>Test Case</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTestCase()
	 * @generated
	 * @ordered
	 */
	protected EList<TestCase> testCase;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TestCaseGroupImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_test_casesPackage.Literals.TEST_CASE_GROUP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AllocationPlan> getAllocationPlan() {
		if (allocationPlan == null) {
			allocationPlan = new EObjectResolvingEList<AllocationPlan>(AllocationPlan.class, this, Ucm_test_casesPackage.TEST_CASE_GROUP__ALLOCATION_PLAN);
		}
		return allocationPlan;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TestCase> getTestCase() {
		if (testCase == null) {
			testCase = new EObjectContainmentEList<TestCase>(TestCase.class, this, Ucm_test_casesPackage.TEST_CASE_GROUP__TEST_CASE);
		}
		return testCase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Ucm_test_casesPackage.TEST_CASE_GROUP__TEST_CASE:
				return ((InternalEList<?>)getTestCase()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_test_casesPackage.TEST_CASE_GROUP__ALLOCATION_PLAN:
				return getAllocationPlan();
			case Ucm_test_casesPackage.TEST_CASE_GROUP__TEST_CASE:
				return getTestCase();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_test_casesPackage.TEST_CASE_GROUP__ALLOCATION_PLAN:
				getAllocationPlan().clear();
				getAllocationPlan().addAll((Collection<? extends AllocationPlan>)newValue);
				return;
			case Ucm_test_casesPackage.TEST_CASE_GROUP__TEST_CASE:
				getTestCase().clear();
				getTestCase().addAll((Collection<? extends TestCase>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_test_casesPackage.TEST_CASE_GROUP__ALLOCATION_PLAN:
				getAllocationPlan().clear();
				return;
			case Ucm_test_casesPackage.TEST_CASE_GROUP__TEST_CASE:
				getTestCase().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_test_casesPackage.TEST_CASE_GROUP__ALLOCATION_PLAN:
				return allocationPlan != null && !allocationPlan.isEmpty();
			case Ucm_test_casesPackage.TEST_CASE_GROUP__TEST_CASE:
				return testCase != null && !testCase.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //TestCaseGroupImpl
