/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_test_cases.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import ucm_base.ucm_commons.impl.INamedImpl;

import ucm_supplement.ucm_test_cases.TestCase;
import ucm_supplement.ucm_test_cases.TestCaseObservation;
import ucm_supplement.ucm_test_cases.TestCaseStimulus;
import ucm_supplement.ucm_test_cases.Ucm_test_casesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Test Case</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_test_cases.impl.TestCaseImpl#getStimulus <em>Stimulus</em>}</li>
 *   <li>{@link ucm_supplement.ucm_test_cases.impl.TestCaseImpl#getObservation <em>Observation</em>}</li>
 *   <li>{@link ucm_supplement.ucm_test_cases.impl.TestCaseImpl#getGlobalOffset <em>Global Offset</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TestCaseImpl extends INamedImpl implements TestCase {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached value of the '{@link #getStimulus() <em>Stimulus</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStimulus()
	 * @generated
	 * @ordered
	 */
	protected TestCaseStimulus stimulus;

	/**
	 * The cached value of the '{@link #getObservation() <em>Observation</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObservation()
	 * @generated
	 * @ordered
	 */
	protected EList<TestCaseObservation> observation;

	/**
	 * The default value of the '{@link #getGlobalOffset() <em>Global Offset</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGlobalOffset()
	 * @generated
	 * @ordered
	 */
	protected static final long GLOBAL_OFFSET_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getGlobalOffset() <em>Global Offset</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGlobalOffset()
	 * @generated
	 * @ordered
	 */
	protected long globalOffset = GLOBAL_OFFSET_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TestCaseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_test_casesPackage.Literals.TEST_CASE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestCaseStimulus getStimulus() {
		return stimulus;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStimulus(TestCaseStimulus newStimulus, NotificationChain msgs) {
		TestCaseStimulus oldStimulus = stimulus;
		stimulus = newStimulus;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Ucm_test_casesPackage.TEST_CASE__STIMULUS, oldStimulus, newStimulus);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStimulus(TestCaseStimulus newStimulus) {
		if (newStimulus != stimulus) {
			NotificationChain msgs = null;
			if (stimulus != null)
				msgs = ((InternalEObject)stimulus).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - Ucm_test_casesPackage.TEST_CASE__STIMULUS, null, msgs);
			if (newStimulus != null)
				msgs = ((InternalEObject)newStimulus).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - Ucm_test_casesPackage.TEST_CASE__STIMULUS, null, msgs);
			msgs = basicSetStimulus(newStimulus, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_test_casesPackage.TEST_CASE__STIMULUS, newStimulus, newStimulus));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TestCaseObservation> getObservation() {
		if (observation == null) {
			observation = new EObjectContainmentEList<TestCaseObservation>(TestCaseObservation.class, this, Ucm_test_casesPackage.TEST_CASE__OBSERVATION);
		}
		return observation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getGlobalOffset() {
		return globalOffset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGlobalOffset(long newGlobalOffset) {
		long oldGlobalOffset = globalOffset;
		globalOffset = newGlobalOffset;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_test_casesPackage.TEST_CASE__GLOBAL_OFFSET, oldGlobalOffset, globalOffset));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Ucm_test_casesPackage.TEST_CASE__STIMULUS:
				return basicSetStimulus(null, msgs);
			case Ucm_test_casesPackage.TEST_CASE__OBSERVATION:
				return ((InternalEList<?>)getObservation()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_test_casesPackage.TEST_CASE__STIMULUS:
				return getStimulus();
			case Ucm_test_casesPackage.TEST_CASE__OBSERVATION:
				return getObservation();
			case Ucm_test_casesPackage.TEST_CASE__GLOBAL_OFFSET:
				return getGlobalOffset();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_test_casesPackage.TEST_CASE__STIMULUS:
				setStimulus((TestCaseStimulus)newValue);
				return;
			case Ucm_test_casesPackage.TEST_CASE__OBSERVATION:
				getObservation().clear();
				getObservation().addAll((Collection<? extends TestCaseObservation>)newValue);
				return;
			case Ucm_test_casesPackage.TEST_CASE__GLOBAL_OFFSET:
				setGlobalOffset((Long)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_test_casesPackage.TEST_CASE__STIMULUS:
				setStimulus((TestCaseStimulus)null);
				return;
			case Ucm_test_casesPackage.TEST_CASE__OBSERVATION:
				getObservation().clear();
				return;
			case Ucm_test_casesPackage.TEST_CASE__GLOBAL_OFFSET:
				setGlobalOffset(GLOBAL_OFFSET_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_test_casesPackage.TEST_CASE__STIMULUS:
				return stimulus != null;
			case Ucm_test_casesPackage.TEST_CASE__OBSERVATION:
				return observation != null && !observation.isEmpty();
			case Ucm_test_casesPackage.TEST_CASE__GLOBAL_OFFSET:
				return globalOffset != GLOBAL_OFFSET_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (globalOffset: ");
		result.append(globalOffset);
		result.append(')');
		return result.toString();
	}

} //TestCaseImpl
