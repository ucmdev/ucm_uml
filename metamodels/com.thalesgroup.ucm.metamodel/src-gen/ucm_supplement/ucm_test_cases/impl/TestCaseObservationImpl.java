/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_test_cases.impl;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import com.thalesgroup.ucm.metamodel.validation.UcmSupplementValidationFactory;

import ucm_supplement.ucm_test_cases.TestCaseObservation;
import ucm_supplement.ucm_test_cases.Ucm_test_casesPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Test
 * Case Observation</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_test_cases.impl.TestCaseObservationImpl#getMinDelay <em>Min Delay</em>}</li>
 *   <li>{@link ucm_supplement.ucm_test_cases.impl.TestCaseObservationImpl#getMaxDelay <em>Max Delay</em>}</li>
 *   <li>{@link ucm_supplement.ucm_test_cases.impl.TestCaseObservationImpl#getOrder <em>Order</em>}</li>
 *   <li>{@link ucm_supplement.ucm_test_cases.impl.TestCaseObservationImpl#isOptional <em>Optional</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TestCaseObservationImpl extends ITestCaseEventImpl implements TestCaseObservation {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The default value of the '{@link #getMinDelay() <em>Min Delay</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getMinDelay()
	 * @generated
	 * @ordered
	 */
	protected static final long MIN_DELAY_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getMinDelay() <em>Min Delay</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getMinDelay()
	 * @generated
	 * @ordered
	 */
	protected long minDelay = MIN_DELAY_EDEFAULT;

	/**
	 * The default value of the '{@link #getMaxDelay() <em>Max Delay</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getMaxDelay()
	 * @generated
	 * @ordered
	 */
	protected static final long MAX_DELAY_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getMaxDelay() <em>Max Delay</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getMaxDelay()
	 * @generated
	 * @ordered
	 */
	protected long maxDelay = MAX_DELAY_EDEFAULT;

	/**
	 * The default value of the '{@link #getOrder() <em>Order</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getOrder()
	 * @generated
	 * @ordered
	 */
	protected static final long ORDER_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getOrder() <em>Order</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getOrder()
	 * @generated
	 * @ordered
	 */
	protected long order = ORDER_EDEFAULT;

	/**
	 * The default value of the '{@link #isOptional() <em>Optional</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #isOptional()
	 * @generated
	 * @ordered
	 */
	protected static final boolean OPTIONAL_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isOptional() <em>Optional</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #isOptional()
	 * @generated
	 * @ordered
	 */
	protected boolean optional = OPTIONAL_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected TestCaseObservationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_test_casesPackage.Literals.TEST_CASE_OBSERVATION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public long getMinDelay() {
		return minDelay;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setMinDelay(long newMinDelay) {
		long oldMinDelay = minDelay;
		minDelay = newMinDelay;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_test_casesPackage.TEST_CASE_OBSERVATION__MIN_DELAY, oldMinDelay, minDelay));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public long getMaxDelay() {
		return maxDelay;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaxDelay(long newMaxDelay) {
		long oldMaxDelay = maxDelay;
		maxDelay = newMaxDelay;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_test_casesPackage.TEST_CASE_OBSERVATION__MAX_DELAY, oldMaxDelay, maxDelay));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public long getOrder() {
		return order;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrder(long newOrder) {
		long oldOrder = order;
		order = newOrder;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_test_casesPackage.TEST_CASE_OBSERVATION__ORDER, oldOrder, order));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isOptional() {
		return optional;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setOptional(boolean newOptional) {
		boolean oldOptional = optional;
		optional = newOptional;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_test_casesPackage.TEST_CASE_OBSERVATION__OPTIONAL, oldOptional, optional));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean valueConsistency(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and
		// message
		// Ensure that you remove @generated or mark it @generated NOT
		return UcmSupplementValidationFactory.INSTANCE.getTestCaseValidator().valueConsistency(this, diagnostics,
				context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_test_casesPackage.TEST_CASE_OBSERVATION__MIN_DELAY:
				return getMinDelay();
			case Ucm_test_casesPackage.TEST_CASE_OBSERVATION__MAX_DELAY:
				return getMaxDelay();
			case Ucm_test_casesPackage.TEST_CASE_OBSERVATION__ORDER:
				return getOrder();
			case Ucm_test_casesPackage.TEST_CASE_OBSERVATION__OPTIONAL:
				return isOptional();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_test_casesPackage.TEST_CASE_OBSERVATION__MIN_DELAY:
				setMinDelay((Long)newValue);
				return;
			case Ucm_test_casesPackage.TEST_CASE_OBSERVATION__MAX_DELAY:
				setMaxDelay((Long)newValue);
				return;
			case Ucm_test_casesPackage.TEST_CASE_OBSERVATION__ORDER:
				setOrder((Long)newValue);
				return;
			case Ucm_test_casesPackage.TEST_CASE_OBSERVATION__OPTIONAL:
				setOptional((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_test_casesPackage.TEST_CASE_OBSERVATION__MIN_DELAY:
				setMinDelay(MIN_DELAY_EDEFAULT);
				return;
			case Ucm_test_casesPackage.TEST_CASE_OBSERVATION__MAX_DELAY:
				setMaxDelay(MAX_DELAY_EDEFAULT);
				return;
			case Ucm_test_casesPackage.TEST_CASE_OBSERVATION__ORDER:
				setOrder(ORDER_EDEFAULT);
				return;
			case Ucm_test_casesPackage.TEST_CASE_OBSERVATION__OPTIONAL:
				setOptional(OPTIONAL_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_test_casesPackage.TEST_CASE_OBSERVATION__MIN_DELAY:
				return minDelay != MIN_DELAY_EDEFAULT;
			case Ucm_test_casesPackage.TEST_CASE_OBSERVATION__MAX_DELAY:
				return maxDelay != MAX_DELAY_EDEFAULT;
			case Ucm_test_casesPackage.TEST_CASE_OBSERVATION__ORDER:
				return order != ORDER_EDEFAULT;
			case Ucm_test_casesPackage.TEST_CASE_OBSERVATION__OPTIONAL:
				return optional != OPTIONAL_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (minDelay: ");
		result.append(minDelay);
		result.append(", maxDelay: ");
		result.append(maxDelay);
		result.append(", order: ");
		result.append(order);
		result.append(", optional: ");
		result.append(optional);
		result.append(')');
		return result.toString();
	}

} // TestCaseObservationImpl
