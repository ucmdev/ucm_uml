/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_test_cases.impl;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import com.thalesgroup.ucm.metamodel.validation.UcmSupplementValidationFactory;

import ucm_base.ucm_commons.impl.INamedImpl;
import ucm_base.ucm_contracts.Method;
import ucm_base.ucm_interactions.PortElement;
import ucm_supplement.ucm_deployments.ComponentInstance;
import ucm_supplement.ucm_test_cases.IComponentInteractionPointReference;
import ucm_supplement.ucm_test_cases.ITestCaseEvent;
import ucm_supplement.ucm_test_cases.Ucm_test_casesPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>ITest
 * Case Event</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_test_cases.impl.ITestCaseEventImpl#getComponentInstance <em>Component Instance</em>}</li>
 *   <li>{@link ucm_supplement.ucm_test_cases.impl.ITestCaseEventImpl#getPortElement <em>Port Element</em>}</li>
 *   <li>{@link ucm_supplement.ucm_test_cases.impl.ITestCaseEventImpl#getMethod <em>Method</em>}</li>
 *   <li>{@link ucm_supplement.ucm_test_cases.impl.ITestCaseEventImpl#getInteractionPoint <em>Interaction Point</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ITestCaseEventImpl extends INamedImpl implements ITestCaseEvent {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached value of the '{@link #getComponentInstance() <em>Component Instance</em>}' containment reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getComponentInstance()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance componentInstance;

	/**
	 * The cached value of the '{@link #getPortElement() <em>Port Element</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getPortElement()
	 * @generated
	 * @ordered
	 */
	protected PortElement portElement;

	/**
	 * The cached value of the '{@link #getMethod() <em>Method</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getMethod()
	 * @generated
	 * @ordered
	 */
	protected Method method;

	/**
	 * The cached value of the '{@link #getInteractionPoint() <em>Interaction Point</em>}' containment reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getInteractionPoint()
	 * @generated
	 * @ordered
	 */
	protected IComponentInteractionPointReference interactionPoint;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected ITestCaseEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_test_casesPackage.Literals.ITEST_CASE_EVENT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance getComponentInstance() {
		return componentInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetComponentInstance(ComponentInstance newComponentInstance, NotificationChain msgs) {
		ComponentInstance oldComponentInstance = componentInstance;
		componentInstance = newComponentInstance;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Ucm_test_casesPackage.ITEST_CASE_EVENT__COMPONENT_INSTANCE, oldComponentInstance, newComponentInstance);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComponentInstance(ComponentInstance newComponentInstance) {
		if (newComponentInstance != componentInstance) {
			NotificationChain msgs = null;
			if (componentInstance != null)
				msgs = ((InternalEObject)componentInstance).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - Ucm_test_casesPackage.ITEST_CASE_EVENT__COMPONENT_INSTANCE, null, msgs);
			if (newComponentInstance != null)
				msgs = ((InternalEObject)newComponentInstance).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - Ucm_test_casesPackage.ITEST_CASE_EVENT__COMPONENT_INSTANCE, null, msgs);
			msgs = basicSetComponentInstance(newComponentInstance, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_test_casesPackage.ITEST_CASE_EVENT__COMPONENT_INSTANCE, newComponentInstance, newComponentInstance));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public PortElement getPortElement() {
		if (portElement != null && portElement.eIsProxy()) {
			InternalEObject oldPortElement = (InternalEObject)portElement;
			portElement = (PortElement)eResolveProxy(oldPortElement);
			if (portElement != oldPortElement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_test_casesPackage.ITEST_CASE_EVENT__PORT_ELEMENT, oldPortElement, portElement));
			}
		}
		return portElement;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public PortElement basicGetPortElement() {
		return portElement;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setPortElement(PortElement newPortElement) {
		PortElement oldPortElement = portElement;
		portElement = newPortElement;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_test_casesPackage.ITEST_CASE_EVENT__PORT_ELEMENT, oldPortElement, portElement));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Method getMethod() {
		if (method != null && method.eIsProxy()) {
			InternalEObject oldMethod = (InternalEObject)method;
			method = (Method)eResolveProxy(oldMethod);
			if (method != oldMethod) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_test_casesPackage.ITEST_CASE_EVENT__METHOD, oldMethod, method));
			}
		}
		return method;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Method basicGetMethod() {
		return method;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setMethod(Method newMethod) {
		Method oldMethod = method;
		method = newMethod;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_test_casesPackage.ITEST_CASE_EVENT__METHOD, oldMethod, method));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public IComponentInteractionPointReference getInteractionPoint() {
		return interactionPoint;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInteractionPoint(IComponentInteractionPointReference newInteractionPoint,
			NotificationChain msgs) {
		IComponentInteractionPointReference oldInteractionPoint = interactionPoint;
		interactionPoint = newInteractionPoint;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Ucm_test_casesPackage.ITEST_CASE_EVENT__INTERACTION_POINT, oldInteractionPoint, newInteractionPoint);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setInteractionPoint(IComponentInteractionPointReference newInteractionPoint) {
		if (newInteractionPoint != interactionPoint) {
			NotificationChain msgs = null;
			if (interactionPoint != null)
				msgs = ((InternalEObject)interactionPoint).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - Ucm_test_casesPackage.ITEST_CASE_EVENT__INTERACTION_POINT, null, msgs);
			if (newInteractionPoint != null)
				msgs = ((InternalEObject)newInteractionPoint).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - Ucm_test_casesPackage.ITEST_CASE_EVENT__INTERACTION_POINT, null, msgs);
			msgs = basicSetInteractionPoint(newInteractionPoint, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_test_casesPackage.ITEST_CASE_EVENT__INTERACTION_POINT, newInteractionPoint, newInteractionPoint));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean componentInstanceLocation(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and
		// message
		// Ensure that you remove @generated or mark it @generated NOT
		return UcmSupplementValidationFactory.INSTANCE.getTestCaseValidator().componentInstanceLocation(this,
				diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean methodAndPortElementConsistency(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and
		// message
		// Ensure that you remove @generated or mark it @generated NOT
		return UcmSupplementValidationFactory.INSTANCE.getTestCaseValidator().methodAndPortElementConsistency(this,
				diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean methodInPortElement(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and
		// message
		// Ensure that you remove @generated or mark it @generated NOT
		return UcmSupplementValidationFactory.INSTANCE.getTestCaseValidator().methodAndPortElementConsistency(this,
				diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Ucm_test_casesPackage.ITEST_CASE_EVENT__COMPONENT_INSTANCE:
				return basicSetComponentInstance(null, msgs);
			case Ucm_test_casesPackage.ITEST_CASE_EVENT__INTERACTION_POINT:
				return basicSetInteractionPoint(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_test_casesPackage.ITEST_CASE_EVENT__COMPONENT_INSTANCE:
				return getComponentInstance();
			case Ucm_test_casesPackage.ITEST_CASE_EVENT__PORT_ELEMENT:
				if (resolve) return getPortElement();
				return basicGetPortElement();
			case Ucm_test_casesPackage.ITEST_CASE_EVENT__METHOD:
				if (resolve) return getMethod();
				return basicGetMethod();
			case Ucm_test_casesPackage.ITEST_CASE_EVENT__INTERACTION_POINT:
				return getInteractionPoint();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_test_casesPackage.ITEST_CASE_EVENT__COMPONENT_INSTANCE:
				setComponentInstance((ComponentInstance)newValue);
				return;
			case Ucm_test_casesPackage.ITEST_CASE_EVENT__PORT_ELEMENT:
				setPortElement((PortElement)newValue);
				return;
			case Ucm_test_casesPackage.ITEST_CASE_EVENT__METHOD:
				setMethod((Method)newValue);
				return;
			case Ucm_test_casesPackage.ITEST_CASE_EVENT__INTERACTION_POINT:
				setInteractionPoint((IComponentInteractionPointReference)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_test_casesPackage.ITEST_CASE_EVENT__COMPONENT_INSTANCE:
				setComponentInstance((ComponentInstance)null);
				return;
			case Ucm_test_casesPackage.ITEST_CASE_EVENT__PORT_ELEMENT:
				setPortElement((PortElement)null);
				return;
			case Ucm_test_casesPackage.ITEST_CASE_EVENT__METHOD:
				setMethod((Method)null);
				return;
			case Ucm_test_casesPackage.ITEST_CASE_EVENT__INTERACTION_POINT:
				setInteractionPoint((IComponentInteractionPointReference)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_test_casesPackage.ITEST_CASE_EVENT__COMPONENT_INSTANCE:
				return componentInstance != null;
			case Ucm_test_casesPackage.ITEST_CASE_EVENT__PORT_ELEMENT:
				return portElement != null;
			case Ucm_test_casesPackage.ITEST_CASE_EVENT__METHOD:
				return method != null;
			case Ucm_test_casesPackage.ITEST_CASE_EVENT__INTERACTION_POINT:
				return interactionPoint != null;
		}
		return super.eIsSet(featureID);
	}

} // ITestCaseEventImpl
