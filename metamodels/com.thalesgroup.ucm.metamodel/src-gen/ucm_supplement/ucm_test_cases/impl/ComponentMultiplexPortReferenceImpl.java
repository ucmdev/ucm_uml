/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_test_cases.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import ucm_base.ucm_contracts.ucm_datatypes.Enumerator;

import ucm_supplement.ucm_test_cases.ComponentMultiplexPortReference;
import ucm_supplement.ucm_test_cases.Ucm_test_casesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Component Multiplex Port Reference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_test_cases.impl.ComponentMultiplexPortReferenceImpl#getBindingName <em>Binding Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComponentMultiplexPortReferenceImpl extends ComponentPortReferenceImpl implements ComponentMultiplexPortReference {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached value of the '{@link #getBindingName() <em>Binding Name</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBindingName()
	 * @generated
	 * @ordered
	 */
	protected Enumerator bindingName;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComponentMultiplexPortReferenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_test_casesPackage.Literals.COMPONENT_MULTIPLEX_PORT_REFERENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Enumerator getBindingName() {
		if (bindingName != null && bindingName.eIsProxy()) {
			InternalEObject oldBindingName = (InternalEObject)bindingName;
			bindingName = (Enumerator)eResolveProxy(oldBindingName);
			if (bindingName != oldBindingName) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_test_casesPackage.COMPONENT_MULTIPLEX_PORT_REFERENCE__BINDING_NAME, oldBindingName, bindingName));
			}
		}
		return bindingName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Enumerator basicGetBindingName() {
		return bindingName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBindingName(Enumerator newBindingName) {
		Enumerator oldBindingName = bindingName;
		bindingName = newBindingName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_test_casesPackage.COMPONENT_MULTIPLEX_PORT_REFERENCE__BINDING_NAME, oldBindingName, bindingName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_test_casesPackage.COMPONENT_MULTIPLEX_PORT_REFERENCE__BINDING_NAME:
				if (resolve) return getBindingName();
				return basicGetBindingName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_test_casesPackage.COMPONENT_MULTIPLEX_PORT_REFERENCE__BINDING_NAME:
				setBindingName((Enumerator)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_test_casesPackage.COMPONENT_MULTIPLEX_PORT_REFERENCE__BINDING_NAME:
				setBindingName((Enumerator)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_test_casesPackage.COMPONENT_MULTIPLEX_PORT_REFERENCE__BINDING_NAME:
				return bindingName != null;
		}
		return super.eIsSet(featureID);
	}

} //ComponentMultiplexPortReferenceImpl
