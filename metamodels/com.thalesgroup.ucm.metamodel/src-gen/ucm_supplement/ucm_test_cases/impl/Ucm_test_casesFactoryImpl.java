/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_test_cases.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import ucm_supplement.ucm_test_cases.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Ucm_test_casesFactoryImpl extends EFactoryImpl implements Ucm_test_casesFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Ucm_test_casesFactory init() {
		try {
			Ucm_test_casesFactory theUcm_test_casesFactory = (Ucm_test_casesFactory)EPackage.Registry.INSTANCE.getEFactory(Ucm_test_casesPackage.eNS_URI);
			if (theUcm_test_casesFactory != null) {
				return theUcm_test_casesFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new Ucm_test_casesFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ucm_test_casesFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case Ucm_test_casesPackage.TEST_CASE_GROUP: return createTestCaseGroup();
			case Ucm_test_casesPackage.TEST_CASE: return createTestCase();
			case Ucm_test_casesPackage.COMPONENT_PORT_REFERENCE: return createComponentPortReference();
			case Ucm_test_casesPackage.COMPONENT_MULTIPLEX_PORT_REFERENCE: return createComponentMultiplexPortReference();
			case Ucm_test_casesPackage.TECHNICAL_POLICY_REFERENCE: return createTechnicalPolicyReference();
			case Ucm_test_casesPackage.TEST_CASE_STIMULUS: return createTestCaseStimulus();
			case Ucm_test_casesPackage.TEST_CASE_OBSERVATION: return createTestCaseObservation();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestCaseGroup createTestCaseGroup() {
		TestCaseGroupImpl testCaseGroup = new TestCaseGroupImpl();
		return testCaseGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestCase createTestCase() {
		TestCaseImpl testCase = new TestCaseImpl();
		return testCase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentPortReference createComponentPortReference() {
		ComponentPortReferenceImpl componentPortReference = new ComponentPortReferenceImpl();
		return componentPortReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentMultiplexPortReference createComponentMultiplexPortReference() {
		ComponentMultiplexPortReferenceImpl componentMultiplexPortReference = new ComponentMultiplexPortReferenceImpl();
		return componentMultiplexPortReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TechnicalPolicyReference createTechnicalPolicyReference() {
		TechnicalPolicyReferenceImpl technicalPolicyReference = new TechnicalPolicyReferenceImpl();
		return technicalPolicyReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestCaseStimulus createTestCaseStimulus() {
		TestCaseStimulusImpl testCaseStimulus = new TestCaseStimulusImpl();
		return testCaseStimulus;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestCaseObservation createTestCaseObservation() {
		TestCaseObservationImpl testCaseObservation = new TestCaseObservationImpl();
		return testCaseObservation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ucm_test_casesPackage getUcm_test_casesPackage() {
		return (Ucm_test_casesPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static Ucm_test_casesPackage getPackage() {
		return Ucm_test_casesPackage.eINSTANCE;
	}

} //Ucm_test_casesFactoryImpl
