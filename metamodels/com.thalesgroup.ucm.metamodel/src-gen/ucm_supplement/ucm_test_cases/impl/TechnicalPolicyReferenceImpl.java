/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_test_cases.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import ucm_base.ucm_components.ComponentTechnicalPolicy;

import ucm_supplement.ucm_test_cases.TechnicalPolicyReference;
import ucm_supplement.ucm_test_cases.Ucm_test_casesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Technical Policy Reference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_test_cases.impl.TechnicalPolicyReferenceImpl#getTechnicalPolicy <em>Technical Policy</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TechnicalPolicyReferenceImpl extends IComponentInteractionPointReferenceImpl implements TechnicalPolicyReference {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached value of the '{@link #getTechnicalPolicy() <em>Technical Policy</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTechnicalPolicy()
	 * @generated
	 * @ordered
	 */
	protected ComponentTechnicalPolicy technicalPolicy;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TechnicalPolicyReferenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_test_casesPackage.Literals.TECHNICAL_POLICY_REFERENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentTechnicalPolicy getTechnicalPolicy() {
		if (technicalPolicy != null && technicalPolicy.eIsProxy()) {
			InternalEObject oldTechnicalPolicy = (InternalEObject)technicalPolicy;
			technicalPolicy = (ComponentTechnicalPolicy)eResolveProxy(oldTechnicalPolicy);
			if (technicalPolicy != oldTechnicalPolicy) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_test_casesPackage.TECHNICAL_POLICY_REFERENCE__TECHNICAL_POLICY, oldTechnicalPolicy, technicalPolicy));
			}
		}
		return technicalPolicy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentTechnicalPolicy basicGetTechnicalPolicy() {
		return technicalPolicy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTechnicalPolicy(ComponentTechnicalPolicy newTechnicalPolicy) {
		ComponentTechnicalPolicy oldTechnicalPolicy = technicalPolicy;
		technicalPolicy = newTechnicalPolicy;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_test_casesPackage.TECHNICAL_POLICY_REFERENCE__TECHNICAL_POLICY, oldTechnicalPolicy, technicalPolicy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_test_casesPackage.TECHNICAL_POLICY_REFERENCE__TECHNICAL_POLICY:
				if (resolve) return getTechnicalPolicy();
				return basicGetTechnicalPolicy();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_test_casesPackage.TECHNICAL_POLICY_REFERENCE__TECHNICAL_POLICY:
				setTechnicalPolicy((ComponentTechnicalPolicy)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_test_casesPackage.TECHNICAL_POLICY_REFERENCE__TECHNICAL_POLICY:
				setTechnicalPolicy((ComponentTechnicalPolicy)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_test_casesPackage.TECHNICAL_POLICY_REFERENCE__TECHNICAL_POLICY:
				return technicalPolicy != null;
		}
		return super.eIsSet(featureID);
	}

} //TechnicalPolicyReferenceImpl
