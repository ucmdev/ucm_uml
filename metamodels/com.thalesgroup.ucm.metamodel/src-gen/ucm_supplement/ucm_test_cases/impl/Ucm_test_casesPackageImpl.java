/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_test_cases.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import ucm_base.ucm_commons.Ucm_commonsPackage;

import ucm_base.ucm_components.Ucm_componentsPackage;

import ucm_base.ucm_contracts.Ucm_contractsPackage;

import ucm_base.ucm_contracts.ucm_datatypes.Ucm_datatypesPackage;

import ucm_base.ucm_interactions.Ucm_interactionsPackage;

import ucm_base.ucm_technicalpolicies.Ucm_technicalpoliciesPackage;

import ucm_basic_projects.Ucm_basic_projectsPackage;

import ucm_environment.ucm_resources.Ucm_resourcesPackage;

import ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl;

import ucm_extended_projects.Ucm_extended_projectsPackage;

import ucm_extended_projects.impl.Ucm_extended_projectsPackageImpl;

import ucm_supplement.ucm_deployments.Ucm_deploymentsPackage;

import ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl;

import ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage;

import ucm_supplement.ucm_detailed_components.impl.Ucm_detailed_componentsPackageImpl;

import ucm_supplement.ucm_enhanced_configuration.Ucm_enhanced_configurationPackage;

import ucm_supplement.ucm_enhanced_configuration.impl.Ucm_enhanced_configurationPackageImpl;

import ucm_supplement.ucm_enhanced_portspec.Ucm_enhanced_portspecPackage;

import ucm_supplement.ucm_enhanced_portspec.impl.Ucm_enhanced_portspecPackageImpl;

import ucm_supplement.ucm_test_cases.ComponentMultiplexPortReference;
import ucm_supplement.ucm_test_cases.ComponentPortReference;
import ucm_supplement.ucm_test_cases.IComponentInteractionPointReference;
import ucm_supplement.ucm_test_cases.ITestCaseEvent;
import ucm_supplement.ucm_test_cases.TechnicalPolicyReference;
import ucm_supplement.ucm_test_cases.TestCase;
import ucm_supplement.ucm_test_cases.TestCaseGroup;
import ucm_supplement.ucm_test_cases.TestCaseObservation;
import ucm_supplement.ucm_test_cases.TestCaseStimulus;
import ucm_supplement.ucm_test_cases.Ucm_test_casesFactory;
import ucm_supplement.ucm_test_cases.Ucm_test_casesPackage;
import ucm_supplement.ucm_test_cases.util.Ucm_test_casesValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Ucm_test_casesPackageImpl extends EPackageImpl implements Ucm_test_casesPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass testCaseGroupEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass testCaseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iTestCaseEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iComponentInteractionPointReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass componentPortReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass componentMultiplexPortReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass technicalPolicyReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass testCaseStimulusEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass testCaseObservationEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ucm_supplement.ucm_test_cases.Ucm_test_casesPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private Ucm_test_casesPackageImpl() {
		super(eNS_URI, Ucm_test_casesFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link Ucm_test_casesPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static Ucm_test_casesPackage init() {
		if (isInited) return (Ucm_test_casesPackage)EPackage.Registry.INSTANCE.getEPackage(Ucm_test_casesPackage.eNS_URI);

		// Obtain or create and register package
		Ucm_test_casesPackageImpl theUcm_test_casesPackage = (Ucm_test_casesPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof Ucm_test_casesPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new Ucm_test_casesPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		Ucm_basic_projectsPackage.eINSTANCE.eClass();
		Ucm_commonsPackage.eINSTANCE.eClass();
		Ucm_technicalpoliciesPackage.eINSTANCE.eClass();
		Ucm_contractsPackage.eINSTANCE.eClass();
		Ucm_interactionsPackage.eINSTANCE.eClass();
		Ucm_componentsPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Ucm_extended_projectsPackageImpl theUcm_extended_projectsPackage = (Ucm_extended_projectsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_extended_projectsPackage.eNS_URI) instanceof Ucm_extended_projectsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_extended_projectsPackage.eNS_URI) : Ucm_extended_projectsPackage.eINSTANCE);
		Ucm_detailed_componentsPackageImpl theUcm_detailed_componentsPackage = (Ucm_detailed_componentsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_detailed_componentsPackage.eNS_URI) instanceof Ucm_detailed_componentsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_detailed_componentsPackage.eNS_URI) : Ucm_detailed_componentsPackage.eINSTANCE);
		Ucm_enhanced_portspecPackageImpl theUcm_enhanced_portspecPackage = (Ucm_enhanced_portspecPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_enhanced_portspecPackage.eNS_URI) instanceof Ucm_enhanced_portspecPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_enhanced_portspecPackage.eNS_URI) : Ucm_enhanced_portspecPackage.eINSTANCE);
		Ucm_deploymentsPackageImpl theUcm_deploymentsPackage = (Ucm_deploymentsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_deploymentsPackage.eNS_URI) instanceof Ucm_deploymentsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_deploymentsPackage.eNS_URI) : Ucm_deploymentsPackage.eINSTANCE);
		Ucm_enhanced_configurationPackageImpl theUcm_enhanced_configurationPackage = (Ucm_enhanced_configurationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_enhanced_configurationPackage.eNS_URI) instanceof Ucm_enhanced_configurationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_enhanced_configurationPackage.eNS_URI) : Ucm_enhanced_configurationPackage.eINSTANCE);
		Ucm_resourcesPackageImpl theUcm_resourcesPackage = (Ucm_resourcesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_resourcesPackage.eNS_URI) instanceof Ucm_resourcesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_resourcesPackage.eNS_URI) : Ucm_resourcesPackage.eINSTANCE);

		// Create package meta-data objects
		theUcm_test_casesPackage.createPackageContents();
		theUcm_extended_projectsPackage.createPackageContents();
		theUcm_detailed_componentsPackage.createPackageContents();
		theUcm_enhanced_portspecPackage.createPackageContents();
		theUcm_deploymentsPackage.createPackageContents();
		theUcm_enhanced_configurationPackage.createPackageContents();
		theUcm_resourcesPackage.createPackageContents();

		// Initialize created meta-data
		theUcm_test_casesPackage.initializePackageContents();
		theUcm_extended_projectsPackage.initializePackageContents();
		theUcm_detailed_componentsPackage.initializePackageContents();
		theUcm_enhanced_portspecPackage.initializePackageContents();
		theUcm_deploymentsPackage.initializePackageContents();
		theUcm_enhanced_configurationPackage.initializePackageContents();
		theUcm_resourcesPackage.initializePackageContents();

		// Register package validator
		EValidator.Registry.INSTANCE.put
			(theUcm_test_casesPackage, 
			 new EValidator.Descriptor() {
				 public EValidator getEValidator() {
					 return Ucm_test_casesValidator.INSTANCE;
				 }
			 });

		// Mark meta-data to indicate it can't be changed
		theUcm_test_casesPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(Ucm_test_casesPackage.eNS_URI, theUcm_test_casesPackage);
		return theUcm_test_casesPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTestCaseGroup() {
		return testCaseGroupEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestCaseGroup_AllocationPlan() {
		return (EReference)testCaseGroupEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestCaseGroup_TestCase() {
		return (EReference)testCaseGroupEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTestCase() {
		return testCaseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestCase_Stimulus() {
		return (EReference)testCaseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestCase_Observation() {
		return (EReference)testCaseEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTestCase_GlobalOffset() {
		return (EAttribute)testCaseEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getITestCaseEvent() {
		return iTestCaseEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getITestCaseEvent_ComponentInstance() {
		return (EReference)iTestCaseEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getITestCaseEvent_PortElement() {
		return (EReference)iTestCaseEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getITestCaseEvent_Method() {
		return (EReference)iTestCaseEventEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getITestCaseEvent_InteractionPoint() {
		return (EReference)iTestCaseEventEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIComponentInteractionPointReference() {
		return iComponentInteractionPointReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComponentPortReference() {
		return componentPortReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentPortReference_Port() {
		return (EReference)componentPortReferenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComponentMultiplexPortReference() {
		return componentMultiplexPortReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentMultiplexPortReference_BindingName() {
		return (EReference)componentMultiplexPortReferenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTechnicalPolicyReference() {
		return technicalPolicyReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTechnicalPolicyReference_TechnicalPolicy() {
		return (EReference)technicalPolicyReferenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTestCaseStimulus() {
		return testCaseStimulusEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTestCaseStimulus_Delay() {
		return (EAttribute)testCaseStimulusEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTestCaseObservation() {
		return testCaseObservationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTestCaseObservation_MinDelay() {
		return (EAttribute)testCaseObservationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTestCaseObservation_MaxDelay() {
		return (EAttribute)testCaseObservationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTestCaseObservation_Order() {
		return (EAttribute)testCaseObservationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTestCaseObservation_Optional() {
		return (EAttribute)testCaseObservationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ucm_test_casesFactory getUcm_test_casesFactory() {
		return (Ucm_test_casesFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		testCaseGroupEClass = createEClass(TEST_CASE_GROUP);
		createEReference(testCaseGroupEClass, TEST_CASE_GROUP__ALLOCATION_PLAN);
		createEReference(testCaseGroupEClass, TEST_CASE_GROUP__TEST_CASE);

		testCaseEClass = createEClass(TEST_CASE);
		createEReference(testCaseEClass, TEST_CASE__STIMULUS);
		createEReference(testCaseEClass, TEST_CASE__OBSERVATION);
		createEAttribute(testCaseEClass, TEST_CASE__GLOBAL_OFFSET);

		iTestCaseEventEClass = createEClass(ITEST_CASE_EVENT);
		createEReference(iTestCaseEventEClass, ITEST_CASE_EVENT__COMPONENT_INSTANCE);
		createEReference(iTestCaseEventEClass, ITEST_CASE_EVENT__PORT_ELEMENT);
		createEReference(iTestCaseEventEClass, ITEST_CASE_EVENT__METHOD);
		createEReference(iTestCaseEventEClass, ITEST_CASE_EVENT__INTERACTION_POINT);

		iComponentInteractionPointReferenceEClass = createEClass(ICOMPONENT_INTERACTION_POINT_REFERENCE);

		componentPortReferenceEClass = createEClass(COMPONENT_PORT_REFERENCE);
		createEReference(componentPortReferenceEClass, COMPONENT_PORT_REFERENCE__PORT);

		componentMultiplexPortReferenceEClass = createEClass(COMPONENT_MULTIPLEX_PORT_REFERENCE);
		createEReference(componentMultiplexPortReferenceEClass, COMPONENT_MULTIPLEX_PORT_REFERENCE__BINDING_NAME);

		technicalPolicyReferenceEClass = createEClass(TECHNICAL_POLICY_REFERENCE);
		createEReference(technicalPolicyReferenceEClass, TECHNICAL_POLICY_REFERENCE__TECHNICAL_POLICY);

		testCaseStimulusEClass = createEClass(TEST_CASE_STIMULUS);
		createEAttribute(testCaseStimulusEClass, TEST_CASE_STIMULUS__DELAY);

		testCaseObservationEClass = createEClass(TEST_CASE_OBSERVATION);
		createEAttribute(testCaseObservationEClass, TEST_CASE_OBSERVATION__MIN_DELAY);
		createEAttribute(testCaseObservationEClass, TEST_CASE_OBSERVATION__MAX_DELAY);
		createEAttribute(testCaseObservationEClass, TEST_CASE_OBSERVATION__ORDER);
		createEAttribute(testCaseObservationEClass, TEST_CASE_OBSERVATION__OPTIONAL);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		Ucm_commonsPackage theUcm_commonsPackage = (Ucm_commonsPackage)EPackage.Registry.INSTANCE.getEPackage(Ucm_commonsPackage.eNS_URI);
		Ucm_deploymentsPackage theUcm_deploymentsPackage = (Ucm_deploymentsPackage)EPackage.Registry.INSTANCE.getEPackage(Ucm_deploymentsPackage.eNS_URI);
		Ucm_interactionsPackage theUcm_interactionsPackage = (Ucm_interactionsPackage)EPackage.Registry.INSTANCE.getEPackage(Ucm_interactionsPackage.eNS_URI);
		Ucm_contractsPackage theUcm_contractsPackage = (Ucm_contractsPackage)EPackage.Registry.INSTANCE.getEPackage(Ucm_contractsPackage.eNS_URI);
		Ucm_componentsPackage theUcm_componentsPackage = (Ucm_componentsPackage)EPackage.Registry.INSTANCE.getEPackage(Ucm_componentsPackage.eNS_URI);
		Ucm_datatypesPackage theUcm_datatypesPackage = (Ucm_datatypesPackage)EPackage.Registry.INSTANCE.getEPackage(Ucm_datatypesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		testCaseGroupEClass.getESuperTypes().add(theUcm_commonsPackage.getIApplicationModule());
		testCaseEClass.getESuperTypes().add(theUcm_commonsPackage.getINamed());
		iTestCaseEventEClass.getESuperTypes().add(theUcm_commonsPackage.getINamed());
		componentPortReferenceEClass.getESuperTypes().add(this.getIComponentInteractionPointReference());
		componentMultiplexPortReferenceEClass.getESuperTypes().add(this.getComponentPortReference());
		technicalPolicyReferenceEClass.getESuperTypes().add(this.getIComponentInteractionPointReference());
		testCaseStimulusEClass.getESuperTypes().add(this.getITestCaseEvent());
		testCaseObservationEClass.getESuperTypes().add(this.getITestCaseEvent());

		// Initialize classes and features; add operations and parameters
		initEClass(testCaseGroupEClass, TestCaseGroup.class, "TestCaseGroup", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTestCaseGroup_AllocationPlan(), theUcm_deploymentsPackage.getAllocationPlan(), null, "allocationPlan", null, 0, -1, TestCaseGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTestCaseGroup_TestCase(), this.getTestCase(), null, "testCase", null, 0, -1, TestCaseGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(testCaseEClass, TestCase.class, "TestCase", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTestCase_Stimulus(), this.getTestCaseStimulus(), null, "stimulus", null, 0, 1, TestCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTestCase_Observation(), this.getTestCaseObservation(), null, "observation", null, 0, -1, TestCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTestCase_GlobalOffset(), ecorePackage.getELong(), "globalOffset", null, 0, 1, TestCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(iTestCaseEventEClass, ITestCaseEvent.class, "ITestCaseEvent", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getITestCaseEvent_ComponentInstance(), theUcm_deploymentsPackage.getComponentInstance(), null, "componentInstance", null, 1, 1, ITestCaseEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getITestCaseEvent_PortElement(), theUcm_interactionsPackage.getPortElement(), null, "portElement", null, 0, 1, ITestCaseEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getITestCaseEvent_Method(), theUcm_contractsPackage.getMethod(), null, "method", null, 0, 1, ITestCaseEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getITestCaseEvent_InteractionPoint(), this.getIComponentInteractionPointReference(), null, "interactionPoint", null, 1, 1, ITestCaseEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = addEOperation(iTestCaseEventEClass, ecorePackage.getEBoolean(), "componentInstanceLocation", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
		EGenericType g1 = createEGenericType(ecorePackage.getEMap());
		EGenericType g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(iTestCaseEventEClass, ecorePackage.getEBoolean(), "methodAndPortElementConsistency", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(iComponentInteractionPointReferenceEClass, IComponentInteractionPointReference.class, "IComponentInteractionPointReference", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(componentPortReferenceEClass, ComponentPortReference.class, "ComponentPortReference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getComponentPortReference_Port(), theUcm_componentsPackage.getPort(), null, "port", null, 1, 1, ComponentPortReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(componentMultiplexPortReferenceEClass, ComponentMultiplexPortReference.class, "ComponentMultiplexPortReference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getComponentMultiplexPortReference_BindingName(), theUcm_datatypesPackage.getEnumerator(), null, "bindingName", null, 0, 1, ComponentMultiplexPortReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(technicalPolicyReferenceEClass, TechnicalPolicyReference.class, "TechnicalPolicyReference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTechnicalPolicyReference_TechnicalPolicy(), theUcm_componentsPackage.getComponentTechnicalPolicy(), null, "technicalPolicy", null, 1, 1, TechnicalPolicyReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(testCaseStimulusEClass, TestCaseStimulus.class, "TestCaseStimulus", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTestCaseStimulus_Delay(), ecorePackage.getELong(), "delay", null, 0, 1, TestCaseStimulus.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(testCaseObservationEClass, TestCaseObservation.class, "TestCaseObservation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTestCaseObservation_MinDelay(), ecorePackage.getELong(), "minDelay", null, 1, 1, TestCaseObservation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTestCaseObservation_MaxDelay(), ecorePackage.getELong(), "maxDelay", null, 1, 1, TestCaseObservation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTestCaseObservation_Order(), ecorePackage.getELong(), "order", null, 1, 1, TestCaseObservation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTestCaseObservation_Optional(), ecorePackage.getEBoolean(), "optional", null, 1, 1, TestCaseObservation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(testCaseObservationEClass, ecorePackage.getEBoolean(), "valueConsistency", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //Ucm_test_casesPackageImpl
