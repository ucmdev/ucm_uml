/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_test_cases;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see ucm_supplement.ucm_test_cases.Ucm_test_casesPackage
 * @generated
 */
public interface Ucm_test_casesFactory extends EFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Ucm_test_casesFactory eINSTANCE = ucm_supplement.ucm_test_cases.impl.Ucm_test_casesFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Test Case Group</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Test Case Group</em>'.
	 * @generated
	 */
	TestCaseGroup createTestCaseGroup();

	/**
	 * Returns a new object of class '<em>Test Case</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Test Case</em>'.
	 * @generated
	 */
	TestCase createTestCase();

	/**
	 * Returns a new object of class '<em>Component Port Reference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Component Port Reference</em>'.
	 * @generated
	 */
	ComponentPortReference createComponentPortReference();

	/**
	 * Returns a new object of class '<em>Component Multiplex Port Reference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Component Multiplex Port Reference</em>'.
	 * @generated
	 */
	ComponentMultiplexPortReference createComponentMultiplexPortReference();

	/**
	 * Returns a new object of class '<em>Technical Policy Reference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Technical Policy Reference</em>'.
	 * @generated
	 */
	TechnicalPolicyReference createTechnicalPolicyReference();

	/**
	 * Returns a new object of class '<em>Test Case Stimulus</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Test Case Stimulus</em>'.
	 * @generated
	 */
	TestCaseStimulus createTestCaseStimulus();

	/**
	 * Returns a new object of class '<em>Test Case Observation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Test Case Observation</em>'.
	 * @generated
	 */
	TestCaseObservation createTestCaseObservation();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	Ucm_test_casesPackage getUcm_test_casesPackage();

} //Ucm_test_casesFactory
