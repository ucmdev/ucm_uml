/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_enhanced_configuration.util;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

import ucm_supplement.ucm_enhanced_configuration.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see ucm_supplement.ucm_enhanced_configuration.Ucm_enhanced_configurationPackage
 * @generated
 */
public class Ucm_enhanced_configurationValidator extends EObjectValidator {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final Ucm_enhanced_configurationValidator INSTANCE = new Ucm_enhanced_configurationValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "ucm_supplement.ucm_enhanced_configuration";

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Parameter Definition Consistency' of 'Policy Dependency Parameter Value'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int POLICY_DEPENDENCY_PARAMETER_VALUE__PARAMETER_DEFINITION_CONSISTENCY = 1;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 1;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ucm_enhanced_configurationValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return Ucm_enhanced_configurationPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case Ucm_enhanced_configurationPackage.RESTRICTIVE_ANNOTATION_DEFINITION:
				return validateRestrictiveAnnotationDefinition((RestrictiveAnnotationDefinition)value, diagnostics, context);
			case Ucm_enhanced_configurationPackage.POLICY_DEPENDENCY_PARAMETER:
				return validatePolicyDependencyParameter((PolicyDependencyParameter)value, diagnostics, context);
			case Ucm_enhanced_configurationPackage.POLICY_DEPENDENCY_PARAMETER_VALUE:
				return validatePolicyDependencyParameterValue((PolicyDependencyParameterValue)value, diagnostics, context);
			case Ucm_enhanced_configurationPackage.ANNOTABLE_ENTITIES:
				return validateAnnotableEntities((AnnotableEntities)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRestrictiveAnnotationDefinition(RestrictiveAnnotationDefinition restrictiveAnnotationDefinition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(restrictiveAnnotationDefinition, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePolicyDependencyParameter(PolicyDependencyParameter policyDependencyParameter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(policyDependencyParameter, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePolicyDependencyParameterValue(PolicyDependencyParameterValue policyDependencyParameterValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(policyDependencyParameterValue, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(policyDependencyParameterValue, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(policyDependencyParameterValue, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(policyDependencyParameterValue, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(policyDependencyParameterValue, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(policyDependencyParameterValue, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(policyDependencyParameterValue, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(policyDependencyParameterValue, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(policyDependencyParameterValue, diagnostics, context);
		if (result || diagnostics != null) result &= validatePolicyDependencyParameterValue_parameterDefinitionConsistency(policyDependencyParameterValue, diagnostics, context);
		return result;
	}

	/**
	 * Validates the parameterDefinitionConsistency constraint of '<em>Policy Dependency Parameter Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePolicyDependencyParameterValue_parameterDefinitionConsistency(PolicyDependencyParameterValue policyDependencyParameterValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return policyDependencyParameterValue.parameterDefinitionConsistency(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAnnotableEntities(AnnotableEntities annotableEntities, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //Ucm_enhanced_configurationValidator
