/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_enhanced_configuration.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import ucm_base.ucm_commons.INamed;

import ucm_base.ucm_contracts.AnnotationDefinition;
import ucm_base.ucm_contracts.IConfigurable;
import ucm_base.ucm_contracts.IConfigurationParameter;
import ucm_base.ucm_contracts.IConfigurationParameterValue;
import ucm_supplement.ucm_enhanced_configuration.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see ucm_supplement.ucm_enhanced_configuration.Ucm_enhanced_configurationPackage
 * @generated
 */
public class Ucm_enhanced_configurationAdapterFactory extends AdapterFactoryImpl {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static Ucm_enhanced_configurationPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ucm_enhanced_configurationAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = Ucm_enhanced_configurationPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Ucm_enhanced_configurationSwitch<Adapter> modelSwitch =
		new Ucm_enhanced_configurationSwitch<Adapter>() {
			@Override
			public Adapter caseRestrictiveAnnotationDefinition(RestrictiveAnnotationDefinition object) {
				return createRestrictiveAnnotationDefinitionAdapter();
			}
			@Override
			public Adapter casePolicyDependencyParameter(PolicyDependencyParameter object) {
				return createPolicyDependencyParameterAdapter();
			}
			@Override
			public Adapter casePolicyDependencyParameterValue(PolicyDependencyParameterValue object) {
				return createPolicyDependencyParameterValueAdapter();
			}
			@Override
			public Adapter caseIConfigurable(IConfigurable object) {
				return createIConfigurableAdapter();
			}
			@Override
			public Adapter caseINamed(INamed object) {
				return createINamedAdapter();
			}
			@Override
			public Adapter caseAnnotationDefinition(AnnotationDefinition object) {
				return createAnnotationDefinitionAdapter();
			}
			@Override
			public Adapter caseIConfigurationParameter(IConfigurationParameter object) {
				return createIConfigurationParameterAdapter();
			}
			@Override
			public Adapter caseIConfigurationParameterValue(IConfigurationParameterValue object) {
				return createIConfigurationParameterValueAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link ucm_supplement.ucm_enhanced_configuration.RestrictiveAnnotationDefinition <em>Restrictive Annotation Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_supplement.ucm_enhanced_configuration.RestrictiveAnnotationDefinition
	 * @generated
	 */
	public Adapter createRestrictiveAnnotationDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_supplement.ucm_enhanced_configuration.PolicyDependencyParameter <em>Policy Dependency Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_supplement.ucm_enhanced_configuration.PolicyDependencyParameter
	 * @generated
	 */
	public Adapter createPolicyDependencyParameterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_supplement.ucm_enhanced_configuration.PolicyDependencyParameterValue <em>Policy Dependency Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_supplement.ucm_enhanced_configuration.PolicyDependencyParameterValue
	 * @generated
	 */
	public Adapter createPolicyDependencyParameterValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_base.ucm_contracts.IConfigurable <em>IConfigurable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_base.ucm_contracts.IConfigurable
	 * @generated
	 */
	public Adapter createIConfigurableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_base.ucm_commons.INamed <em>INamed</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_base.ucm_commons.INamed
	 * @generated
	 */
	public Adapter createINamedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_base.ucm_contracts.AnnotationDefinition <em>Annotation Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_base.ucm_contracts.AnnotationDefinition
	 * @generated
	 */
	public Adapter createAnnotationDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_base.ucm_contracts.IConfigurationParameter <em>IConfiguration Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_base.ucm_contracts.IConfigurationParameter
	 * @generated
	 */
	public Adapter createIConfigurationParameterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_base.ucm_contracts.IConfigurationParameterValue <em>IConfiguration Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_base.ucm_contracts.IConfigurationParameterValue
	 * @generated
	 */
	public Adapter createIConfigurationParameterValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //Ucm_enhanced_configurationAdapterFactory
