/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_enhanced_configuration.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import ucm_supplement.ucm_enhanced_configuration.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Ucm_enhanced_configurationFactoryImpl extends EFactoryImpl implements Ucm_enhanced_configurationFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Ucm_enhanced_configurationFactory init() {
		try {
			Ucm_enhanced_configurationFactory theUcm_enhanced_configurationFactory = (Ucm_enhanced_configurationFactory)EPackage.Registry.INSTANCE.getEFactory(Ucm_enhanced_configurationPackage.eNS_URI);
			if (theUcm_enhanced_configurationFactory != null) {
				return theUcm_enhanced_configurationFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new Ucm_enhanced_configurationFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ucm_enhanced_configurationFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case Ucm_enhanced_configurationPackage.RESTRICTIVE_ANNOTATION_DEFINITION: return createRestrictiveAnnotationDefinition();
			case Ucm_enhanced_configurationPackage.POLICY_DEPENDENCY_PARAMETER: return createPolicyDependencyParameter();
			case Ucm_enhanced_configurationPackage.POLICY_DEPENDENCY_PARAMETER_VALUE: return createPolicyDependencyParameterValue();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case Ucm_enhanced_configurationPackage.ANNOTABLE_ENTITIES:
				return createAnnotableEntitiesFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case Ucm_enhanced_configurationPackage.ANNOTABLE_ENTITIES:
				return convertAnnotableEntitiesToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RestrictiveAnnotationDefinition createRestrictiveAnnotationDefinition() {
		RestrictiveAnnotationDefinitionImpl restrictiveAnnotationDefinition = new RestrictiveAnnotationDefinitionImpl();
		return restrictiveAnnotationDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PolicyDependencyParameter createPolicyDependencyParameter() {
		PolicyDependencyParameterImpl policyDependencyParameter = new PolicyDependencyParameterImpl();
		return policyDependencyParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PolicyDependencyParameterValue createPolicyDependencyParameterValue() {
		PolicyDependencyParameterValueImpl policyDependencyParameterValue = new PolicyDependencyParameterValueImpl();
		return policyDependencyParameterValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnotableEntities createAnnotableEntitiesFromString(EDataType eDataType, String initialValue) {
		AnnotableEntities result = AnnotableEntities.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAnnotableEntitiesToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ucm_enhanced_configurationPackage getUcm_enhanced_configurationPackage() {
		return (Ucm_enhanced_configurationPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static Ucm_enhanced_configurationPackage getPackage() {
		return Ucm_enhanced_configurationPackage.eINSTANCE;
	}

} //Ucm_enhanced_configurationFactoryImpl
