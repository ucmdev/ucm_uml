/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_enhanced_configuration.impl;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import com.thalesgroup.ucm.metamodel.validation.UcmSupplementValidationFactory;

import ucm_base.ucm_components.ComponentTechnicalPolicy;
import ucm_base.ucm_contracts.impl.IConfigurationParameterValueImpl;
import ucm_supplement.ucm_enhanced_configuration.PolicyDependencyParameter;
import ucm_supplement.ucm_enhanced_configuration.PolicyDependencyParameterValue;
import ucm_supplement.ucm_enhanced_configuration.Ucm_enhanced_configurationPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Policy
 * Dependency Parameter Value</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_enhanced_configuration.impl.PolicyDependencyParameterValueImpl#getParameterDefinition <em>Parameter Definition</em>}</li>
 *   <li>{@link ucm_supplement.ucm_enhanced_configuration.impl.PolicyDependencyParameterValueImpl#getPolicy <em>Policy</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PolicyDependencyParameterValueImpl extends IConfigurationParameterValueImpl
		implements PolicyDependencyParameterValue {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached value of the '{@link #getParameterDefinition() <em>Parameter
	 * Definition</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getParameterDefinition()
	 * @generated
	 * @ordered
	 */
	protected PolicyDependencyParameter parameterDefinition;

	/**
	 * The cached value of the '{@link #getPolicy() <em>Policy</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getPolicy()
	 * @generated
	 * @ordered
	 */
	protected ComponentTechnicalPolicy policy;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected PolicyDependencyParameterValueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_enhanced_configurationPackage.Literals.POLICY_DEPENDENCY_PARAMETER_VALUE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public PolicyDependencyParameter getParameterDefinition() {
		if (parameterDefinition != null && parameterDefinition.eIsProxy()) {
			InternalEObject oldParameterDefinition = (InternalEObject)parameterDefinition;
			parameterDefinition = (PolicyDependencyParameter)eResolveProxy(oldParameterDefinition);
			if (parameterDefinition != oldParameterDefinition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_enhanced_configurationPackage.POLICY_DEPENDENCY_PARAMETER_VALUE__PARAMETER_DEFINITION, oldParameterDefinition, parameterDefinition));
			}
		}
		return parameterDefinition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public PolicyDependencyParameter basicGetParameterDefinition() {
		return parameterDefinition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setParameterDefinition(PolicyDependencyParameter newParameterDefinition) {
		PolicyDependencyParameter oldParameterDefinition = parameterDefinition;
		parameterDefinition = newParameterDefinition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_enhanced_configurationPackage.POLICY_DEPENDENCY_PARAMETER_VALUE__PARAMETER_DEFINITION, oldParameterDefinition, parameterDefinition));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentTechnicalPolicy getPolicy() {
		if (policy != null && policy.eIsProxy()) {
			InternalEObject oldPolicy = (InternalEObject)policy;
			policy = (ComponentTechnicalPolicy)eResolveProxy(oldPolicy);
			if (policy != oldPolicy) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_enhanced_configurationPackage.POLICY_DEPENDENCY_PARAMETER_VALUE__POLICY, oldPolicy, policy));
			}
		}
		return policy;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentTechnicalPolicy basicGetPolicy() {
		return policy;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setPolicy(ComponentTechnicalPolicy newPolicy) {
		ComponentTechnicalPolicy oldPolicy = policy;
		policy = newPolicy;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_enhanced_configurationPackage.POLICY_DEPENDENCY_PARAMETER_VALUE__POLICY, oldPolicy, policy));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean parameterDefinitionConsistency(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and
		// message
		// Ensure that you remove @generated or mark it @generated NOT
		return UcmSupplementValidationFactory.INSTANCE.getContractValidator().parameterDefinitionConsistency(this,
				diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_enhanced_configurationPackage.POLICY_DEPENDENCY_PARAMETER_VALUE__PARAMETER_DEFINITION:
				if (resolve) return getParameterDefinition();
				return basicGetParameterDefinition();
			case Ucm_enhanced_configurationPackage.POLICY_DEPENDENCY_PARAMETER_VALUE__POLICY:
				if (resolve) return getPolicy();
				return basicGetPolicy();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_enhanced_configurationPackage.POLICY_DEPENDENCY_PARAMETER_VALUE__PARAMETER_DEFINITION:
				setParameterDefinition((PolicyDependencyParameter)newValue);
				return;
			case Ucm_enhanced_configurationPackage.POLICY_DEPENDENCY_PARAMETER_VALUE__POLICY:
				setPolicy((ComponentTechnicalPolicy)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_enhanced_configurationPackage.POLICY_DEPENDENCY_PARAMETER_VALUE__PARAMETER_DEFINITION:
				setParameterDefinition((PolicyDependencyParameter)null);
				return;
			case Ucm_enhanced_configurationPackage.POLICY_DEPENDENCY_PARAMETER_VALUE__POLICY:
				setPolicy((ComponentTechnicalPolicy)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_enhanced_configurationPackage.POLICY_DEPENDENCY_PARAMETER_VALUE__PARAMETER_DEFINITION:
				return parameterDefinition != null;
			case Ucm_enhanced_configurationPackage.POLICY_DEPENDENCY_PARAMETER_VALUE__POLICY:
				return policy != null;
		}
		return super.eIsSet(featureID);
	}

} // PolicyDependencyParameterValueImpl
