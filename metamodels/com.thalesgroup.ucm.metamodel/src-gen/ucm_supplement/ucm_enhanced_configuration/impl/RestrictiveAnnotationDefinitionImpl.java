/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_enhanced_configuration.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

import ucm_base.ucm_contracts.impl.AnnotationDefinitionImpl;

import ucm_supplement.ucm_enhanced_configuration.AnnotableEntities;
import ucm_supplement.ucm_enhanced_configuration.RestrictiveAnnotationDefinition;
import ucm_supplement.ucm_enhanced_configuration.Ucm_enhanced_configurationPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Restrictive Annotation Definition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_enhanced_configuration.impl.RestrictiveAnnotationDefinitionImpl#getAnnotableEntities <em>Annotable Entities</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RestrictiveAnnotationDefinitionImpl extends AnnotationDefinitionImpl implements RestrictiveAnnotationDefinition {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached value of the '{@link #getAnnotableEntities() <em>Annotable Entities</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotableEntities()
	 * @generated
	 * @ordered
	 */
	protected EList<AnnotableEntities> annotableEntities;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RestrictiveAnnotationDefinitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_enhanced_configurationPackage.Literals.RESTRICTIVE_ANNOTATION_DEFINITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AnnotableEntities> getAnnotableEntities() {
		if (annotableEntities == null) {
			annotableEntities = new EDataTypeUniqueEList<AnnotableEntities>(AnnotableEntities.class, this, Ucm_enhanced_configurationPackage.RESTRICTIVE_ANNOTATION_DEFINITION__ANNOTABLE_ENTITIES);
		}
		return annotableEntities;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_enhanced_configurationPackage.RESTRICTIVE_ANNOTATION_DEFINITION__ANNOTABLE_ENTITIES:
				return getAnnotableEntities();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_enhanced_configurationPackage.RESTRICTIVE_ANNOTATION_DEFINITION__ANNOTABLE_ENTITIES:
				getAnnotableEntities().clear();
				getAnnotableEntities().addAll((Collection<? extends AnnotableEntities>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_enhanced_configurationPackage.RESTRICTIVE_ANNOTATION_DEFINITION__ANNOTABLE_ENTITIES:
				getAnnotableEntities().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_enhanced_configurationPackage.RESTRICTIVE_ANNOTATION_DEFINITION__ANNOTABLE_ENTITIES:
				return annotableEntities != null && !annotableEntities.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (annotableEntities: ");
		result.append(annotableEntities);
		result.append(')');
		return result.toString();
	}

} //RestrictiveAnnotationDefinitionImpl
