/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_enhanced_configuration.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import ucm_base.ucm_commons.Ucm_commonsPackage;

import ucm_base.ucm_components.Ucm_componentsPackage;

import ucm_base.ucm_contracts.Ucm_contractsPackage;

import ucm_base.ucm_interactions.Ucm_interactionsPackage;

import ucm_base.ucm_technicalpolicies.Ucm_technicalpoliciesPackage;

import ucm_basic_projects.Ucm_basic_projectsPackage;

import ucm_environment.ucm_resources.Ucm_resourcesPackage;

import ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl;

import ucm_extended_projects.Ucm_extended_projectsPackage;

import ucm_extended_projects.impl.Ucm_extended_projectsPackageImpl;

import ucm_supplement.ucm_deployments.Ucm_deploymentsPackage;

import ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl;

import ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage;

import ucm_supplement.ucm_detailed_components.impl.Ucm_detailed_componentsPackageImpl;

import ucm_supplement.ucm_enhanced_configuration.AnnotableEntities;
import ucm_supplement.ucm_enhanced_configuration.PolicyDependencyParameter;
import ucm_supplement.ucm_enhanced_configuration.PolicyDependencyParameterValue;
import ucm_supplement.ucm_enhanced_configuration.RestrictiveAnnotationDefinition;
import ucm_supplement.ucm_enhanced_configuration.Ucm_enhanced_configurationFactory;
import ucm_supplement.ucm_enhanced_configuration.Ucm_enhanced_configurationPackage;

import ucm_supplement.ucm_enhanced_configuration.util.Ucm_enhanced_configurationValidator;
import ucm_supplement.ucm_enhanced_portspec.Ucm_enhanced_portspecPackage;

import ucm_supplement.ucm_enhanced_portspec.impl.Ucm_enhanced_portspecPackageImpl;

import ucm_supplement.ucm_test_cases.Ucm_test_casesPackage;

import ucm_supplement.ucm_test_cases.impl.Ucm_test_casesPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Ucm_enhanced_configurationPackageImpl extends EPackageImpl implements Ucm_enhanced_configurationPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass restrictiveAnnotationDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass policyDependencyParameterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass policyDependencyParameterValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum annotableEntitiesEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ucm_supplement.ucm_enhanced_configuration.Ucm_enhanced_configurationPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private Ucm_enhanced_configurationPackageImpl() {
		super(eNS_URI, Ucm_enhanced_configurationFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link Ucm_enhanced_configurationPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static Ucm_enhanced_configurationPackage init() {
		if (isInited) return (Ucm_enhanced_configurationPackage)EPackage.Registry.INSTANCE.getEPackage(Ucm_enhanced_configurationPackage.eNS_URI);

		// Obtain or create and register package
		Ucm_enhanced_configurationPackageImpl theUcm_enhanced_configurationPackage = (Ucm_enhanced_configurationPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof Ucm_enhanced_configurationPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new Ucm_enhanced_configurationPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		Ucm_basic_projectsPackage.eINSTANCE.eClass();
		Ucm_commonsPackage.eINSTANCE.eClass();
		Ucm_technicalpoliciesPackage.eINSTANCE.eClass();
		Ucm_contractsPackage.eINSTANCE.eClass();
		Ucm_interactionsPackage.eINSTANCE.eClass();
		Ucm_componentsPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Ucm_extended_projectsPackageImpl theUcm_extended_projectsPackage = (Ucm_extended_projectsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_extended_projectsPackage.eNS_URI) instanceof Ucm_extended_projectsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_extended_projectsPackage.eNS_URI) : Ucm_extended_projectsPackage.eINSTANCE);
		Ucm_detailed_componentsPackageImpl theUcm_detailed_componentsPackage = (Ucm_detailed_componentsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_detailed_componentsPackage.eNS_URI) instanceof Ucm_detailed_componentsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_detailed_componentsPackage.eNS_URI) : Ucm_detailed_componentsPackage.eINSTANCE);
		Ucm_enhanced_portspecPackageImpl theUcm_enhanced_portspecPackage = (Ucm_enhanced_portspecPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_enhanced_portspecPackage.eNS_URI) instanceof Ucm_enhanced_portspecPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_enhanced_portspecPackage.eNS_URI) : Ucm_enhanced_portspecPackage.eINSTANCE);
		Ucm_deploymentsPackageImpl theUcm_deploymentsPackage = (Ucm_deploymentsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_deploymentsPackage.eNS_URI) instanceof Ucm_deploymentsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_deploymentsPackage.eNS_URI) : Ucm_deploymentsPackage.eINSTANCE);
		Ucm_test_casesPackageImpl theUcm_test_casesPackage = (Ucm_test_casesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_test_casesPackage.eNS_URI) instanceof Ucm_test_casesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_test_casesPackage.eNS_URI) : Ucm_test_casesPackage.eINSTANCE);
		Ucm_resourcesPackageImpl theUcm_resourcesPackage = (Ucm_resourcesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_resourcesPackage.eNS_URI) instanceof Ucm_resourcesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_resourcesPackage.eNS_URI) : Ucm_resourcesPackage.eINSTANCE);

		// Create package meta-data objects
		theUcm_enhanced_configurationPackage.createPackageContents();
		theUcm_extended_projectsPackage.createPackageContents();
		theUcm_detailed_componentsPackage.createPackageContents();
		theUcm_enhanced_portspecPackage.createPackageContents();
		theUcm_deploymentsPackage.createPackageContents();
		theUcm_test_casesPackage.createPackageContents();
		theUcm_resourcesPackage.createPackageContents();

		// Initialize created meta-data
		theUcm_enhanced_configurationPackage.initializePackageContents();
		theUcm_extended_projectsPackage.initializePackageContents();
		theUcm_detailed_componentsPackage.initializePackageContents();
		theUcm_enhanced_portspecPackage.initializePackageContents();
		theUcm_deploymentsPackage.initializePackageContents();
		theUcm_test_casesPackage.initializePackageContents();
		theUcm_resourcesPackage.initializePackageContents();

		// Register package validator
		EValidator.Registry.INSTANCE.put
			(theUcm_enhanced_configurationPackage, 
			 new EValidator.Descriptor() {
				 public EValidator getEValidator() {
					 return Ucm_enhanced_configurationValidator.INSTANCE;
				 }
			 });

		// Mark meta-data to indicate it can't be changed
		theUcm_enhanced_configurationPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(Ucm_enhanced_configurationPackage.eNS_URI, theUcm_enhanced_configurationPackage);
		return theUcm_enhanced_configurationPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRestrictiveAnnotationDefinition() {
		return restrictiveAnnotationDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRestrictiveAnnotationDefinition_AnnotableEntities() {
		return (EAttribute)restrictiveAnnotationDefinitionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPolicyDependencyParameter() {
		return policyDependencyParameterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPolicyDependencyParameter_AllowedPolicyDefinition() {
		return (EReference)policyDependencyParameterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPolicyDependencyParameter_Min() {
		return (EAttribute)policyDependencyParameterEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPolicyDependencyParameter_Max() {
		return (EAttribute)policyDependencyParameterEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPolicyDependencyParameterValue() {
		return policyDependencyParameterValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPolicyDependencyParameterValue_ParameterDefinition() {
		return (EReference)policyDependencyParameterValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPolicyDependencyParameterValue_Policy() {
		return (EReference)policyDependencyParameterValueEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getAnnotableEntities() {
		return annotableEntitiesEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ucm_enhanced_configurationFactory getUcm_enhanced_configurationFactory() {
		return (Ucm_enhanced_configurationFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		restrictiveAnnotationDefinitionEClass = createEClass(RESTRICTIVE_ANNOTATION_DEFINITION);
		createEAttribute(restrictiveAnnotationDefinitionEClass, RESTRICTIVE_ANNOTATION_DEFINITION__ANNOTABLE_ENTITIES);

		policyDependencyParameterEClass = createEClass(POLICY_DEPENDENCY_PARAMETER);
		createEReference(policyDependencyParameterEClass, POLICY_DEPENDENCY_PARAMETER__ALLOWED_POLICY_DEFINITION);
		createEAttribute(policyDependencyParameterEClass, POLICY_DEPENDENCY_PARAMETER__MIN);
		createEAttribute(policyDependencyParameterEClass, POLICY_DEPENDENCY_PARAMETER__MAX);

		policyDependencyParameterValueEClass = createEClass(POLICY_DEPENDENCY_PARAMETER_VALUE);
		createEReference(policyDependencyParameterValueEClass, POLICY_DEPENDENCY_PARAMETER_VALUE__PARAMETER_DEFINITION);
		createEReference(policyDependencyParameterValueEClass, POLICY_DEPENDENCY_PARAMETER_VALUE__POLICY);

		// Create enums
		annotableEntitiesEEnum = createEEnum(ANNOTABLE_ENTITIES);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		Ucm_contractsPackage theUcm_contractsPackage = (Ucm_contractsPackage)EPackage.Registry.INSTANCE.getEPackage(Ucm_contractsPackage.eNS_URI);
		Ucm_technicalpoliciesPackage theUcm_technicalpoliciesPackage = (Ucm_technicalpoliciesPackage)EPackage.Registry.INSTANCE.getEPackage(Ucm_technicalpoliciesPackage.eNS_URI);
		Ucm_componentsPackage theUcm_componentsPackage = (Ucm_componentsPackage)EPackage.Registry.INSTANCE.getEPackage(Ucm_componentsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		restrictiveAnnotationDefinitionEClass.getESuperTypes().add(theUcm_contractsPackage.getAnnotationDefinition());
		policyDependencyParameterEClass.getESuperTypes().add(theUcm_contractsPackage.getIConfigurationParameter());
		policyDependencyParameterValueEClass.getESuperTypes().add(theUcm_contractsPackage.getIConfigurationParameterValue());

		// Initialize classes and features; add operations and parameters
		initEClass(restrictiveAnnotationDefinitionEClass, RestrictiveAnnotationDefinition.class, "RestrictiveAnnotationDefinition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRestrictiveAnnotationDefinition_AnnotableEntities(), this.getAnnotableEntities(), "annotableEntities", null, 0, -1, RestrictiveAnnotationDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(policyDependencyParameterEClass, PolicyDependencyParameter.class, "PolicyDependencyParameter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPolicyDependencyParameter_AllowedPolicyDefinition(), theUcm_technicalpoliciesPackage.getTechnicalPolicyDefinition(), null, "allowedPolicyDefinition", null, 1, -1, PolicyDependencyParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPolicyDependencyParameter_Min(), ecorePackage.getELong(), "min", "1", 1, 1, PolicyDependencyParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPolicyDependencyParameter_Max(), ecorePackage.getELong(), "max", "1", 1, 1, PolicyDependencyParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(policyDependencyParameterValueEClass, PolicyDependencyParameterValue.class, "PolicyDependencyParameterValue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPolicyDependencyParameterValue_ParameterDefinition(), this.getPolicyDependencyParameter(), null, "parameterDefinition", null, 1, 1, PolicyDependencyParameterValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPolicyDependencyParameterValue_Policy(), theUcm_componentsPackage.getComponentTechnicalPolicy(), null, "policy", null, 1, 1, PolicyDependencyParameterValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = addEOperation(policyDependencyParameterValueEClass, ecorePackage.getEBoolean(), "parameterDefinitionConsistency", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
		EGenericType g1 = createEGenericType(ecorePackage.getEMap());
		EGenericType g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(annotableEntitiesEEnum, AnnotableEntities.class, "AnnotableEntities");
		addEEnumLiteral(annotableEntitiesEEnum, AnnotableEntities.ALL);
		addEEnumLiteral(annotableEntitiesEEnum, AnnotableEntities.INTERFACE);
		addEEnumLiteral(annotableEntitiesEEnum, AnnotableEntities.METHOD);
		addEEnumLiteral(annotableEntitiesEEnum, AnnotableEntities.ATTRIBUTE);
		addEEnumLiteral(annotableEntitiesEEnum, AnnotableEntities.DATA_TYPE);
		addEEnumLiteral(annotableEntitiesEEnum, AnnotableEntities.COMPONENT_DEFINITION);
		addEEnumLiteral(annotableEntitiesEEnum, AnnotableEntities.COMPONENT_TYPE);
		addEEnumLiteral(annotableEntitiesEEnum, AnnotableEntities.PORT);
		addEEnumLiteral(annotableEntitiesEEnum, AnnotableEntities.COMPONENT_IMPLEMENTATION);
		addEEnumLiteral(annotableEntitiesEEnum, AnnotableEntities.ATOMIC_IMPLEMENTATION);
		addEEnumLiteral(annotableEntitiesEEnum, AnnotableEntities.COMPOSITE_IMPLEMENTATION);
		addEEnumLiteral(annotableEntitiesEEnum, AnnotableEntities.ASSEMBLY_PART);
		addEEnumLiteral(annotableEntitiesEEnum, AnnotableEntities.COMPONENT_INSTANCE);
		addEEnumLiteral(annotableEntitiesEEnum, AnnotableEntities.PORT_IMPLEMENTATION);
		addEEnumLiteral(annotableEntitiesEEnum, AnnotableEntities.ARTEFACT);
		addEEnumLiteral(annotableEntitiesEEnum, AnnotableEntities.COMPONENT_NODE);

		// Create resource
		createResource(eNS_URI);
	}

} //Ucm_enhanced_configurationPackageImpl
