/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_enhanced_configuration.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import ucm_base.ucm_contracts.impl.IConfigurationParameterImpl;

import ucm_base.ucm_technicalpolicies.TechnicalPolicyDefinition;

import ucm_supplement.ucm_enhanced_configuration.PolicyDependencyParameter;
import ucm_supplement.ucm_enhanced_configuration.Ucm_enhanced_configurationPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Policy Dependency Parameter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_enhanced_configuration.impl.PolicyDependencyParameterImpl#getAllowedPolicyDefinition <em>Allowed Policy Definition</em>}</li>
 *   <li>{@link ucm_supplement.ucm_enhanced_configuration.impl.PolicyDependencyParameterImpl#getMin <em>Min</em>}</li>
 *   <li>{@link ucm_supplement.ucm_enhanced_configuration.impl.PolicyDependencyParameterImpl#getMax <em>Max</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PolicyDependencyParameterImpl extends IConfigurationParameterImpl implements PolicyDependencyParameter {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached value of the '{@link #getAllowedPolicyDefinition() <em>Allowed Policy Definition</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAllowedPolicyDefinition()
	 * @generated
	 * @ordered
	 */
	protected EList<TechnicalPolicyDefinition> allowedPolicyDefinition;

	/**
	 * The default value of the '{@link #getMin() <em>Min</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMin()
	 * @generated
	 * @ordered
	 */
	protected static final long MIN_EDEFAULT = 1L;

	/**
	 * The cached value of the '{@link #getMin() <em>Min</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMin()
	 * @generated
	 * @ordered
	 */
	protected long min = MIN_EDEFAULT;

	/**
	 * The default value of the '{@link #getMax() <em>Max</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMax()
	 * @generated
	 * @ordered
	 */
	protected static final long MAX_EDEFAULT = 1L;

	/**
	 * The cached value of the '{@link #getMax() <em>Max</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMax()
	 * @generated
	 * @ordered
	 */
	protected long max = MAX_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PolicyDependencyParameterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_enhanced_configurationPackage.Literals.POLICY_DEPENDENCY_PARAMETER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TechnicalPolicyDefinition> getAllowedPolicyDefinition() {
		if (allowedPolicyDefinition == null) {
			allowedPolicyDefinition = new EObjectResolvingEList<TechnicalPolicyDefinition>(TechnicalPolicyDefinition.class, this, Ucm_enhanced_configurationPackage.POLICY_DEPENDENCY_PARAMETER__ALLOWED_POLICY_DEFINITION);
		}
		return allowedPolicyDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getMin() {
		return min;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMin(long newMin) {
		long oldMin = min;
		min = newMin;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_enhanced_configurationPackage.POLICY_DEPENDENCY_PARAMETER__MIN, oldMin, min));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getMax() {
		return max;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMax(long newMax) {
		long oldMax = max;
		max = newMax;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_enhanced_configurationPackage.POLICY_DEPENDENCY_PARAMETER__MAX, oldMax, max));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_enhanced_configurationPackage.POLICY_DEPENDENCY_PARAMETER__ALLOWED_POLICY_DEFINITION:
				return getAllowedPolicyDefinition();
			case Ucm_enhanced_configurationPackage.POLICY_DEPENDENCY_PARAMETER__MIN:
				return getMin();
			case Ucm_enhanced_configurationPackage.POLICY_DEPENDENCY_PARAMETER__MAX:
				return getMax();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_enhanced_configurationPackage.POLICY_DEPENDENCY_PARAMETER__ALLOWED_POLICY_DEFINITION:
				getAllowedPolicyDefinition().clear();
				getAllowedPolicyDefinition().addAll((Collection<? extends TechnicalPolicyDefinition>)newValue);
				return;
			case Ucm_enhanced_configurationPackage.POLICY_DEPENDENCY_PARAMETER__MIN:
				setMin((Long)newValue);
				return;
			case Ucm_enhanced_configurationPackage.POLICY_DEPENDENCY_PARAMETER__MAX:
				setMax((Long)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_enhanced_configurationPackage.POLICY_DEPENDENCY_PARAMETER__ALLOWED_POLICY_DEFINITION:
				getAllowedPolicyDefinition().clear();
				return;
			case Ucm_enhanced_configurationPackage.POLICY_DEPENDENCY_PARAMETER__MIN:
				setMin(MIN_EDEFAULT);
				return;
			case Ucm_enhanced_configurationPackage.POLICY_DEPENDENCY_PARAMETER__MAX:
				setMax(MAX_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_enhanced_configurationPackage.POLICY_DEPENDENCY_PARAMETER__ALLOWED_POLICY_DEFINITION:
				return allowedPolicyDefinition != null && !allowedPolicyDefinition.isEmpty();
			case Ucm_enhanced_configurationPackage.POLICY_DEPENDENCY_PARAMETER__MIN:
				return min != MIN_EDEFAULT;
			case Ucm_enhanced_configurationPackage.POLICY_DEPENDENCY_PARAMETER__MAX:
				return max != MAX_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (min: ");
		result.append(min);
		result.append(", max: ");
		result.append(max);
		result.append(')');
		return result.toString();
	}

} //PolicyDependencyParameterImpl
