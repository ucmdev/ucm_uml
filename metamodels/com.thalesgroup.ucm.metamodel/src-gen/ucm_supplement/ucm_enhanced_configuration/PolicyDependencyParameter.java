/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_enhanced_configuration;

import org.eclipse.emf.common.util.EList;

import ucm_base.ucm_contracts.IConfigurationParameter;

import ucm_base.ucm_technicalpolicies.TechnicalPolicyDefinition;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Policy Dependency Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_enhanced_configuration.PolicyDependencyParameter#getAllowedPolicyDefinition <em>Allowed Policy Definition</em>}</li>
 *   <li>{@link ucm_supplement.ucm_enhanced_configuration.PolicyDependencyParameter#getMin <em>Min</em>}</li>
 *   <li>{@link ucm_supplement.ucm_enhanced_configuration.PolicyDependencyParameter#getMax <em>Max</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_enhanced_configuration.Ucm_enhanced_configurationPackage#getPolicyDependencyParameter()
 * @model
 * @generated
 */
public interface PolicyDependencyParameter extends IConfigurationParameter {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Allowed Policy Definition</b></em>' reference list.
	 * The list contents are of type {@link ucm_base.ucm_technicalpolicies.TechnicalPolicyDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Allowed Policy Definition</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Allowed Policy Definition</em>' reference list.
	 * @see ucm_supplement.ucm_enhanced_configuration.Ucm_enhanced_configurationPackage#getPolicyDependencyParameter_AllowedPolicyDefinition()
	 * @model required="true"
	 * @generated
	 */
	EList<TechnicalPolicyDefinition> getAllowedPolicyDefinition();

	/**
	 * Returns the value of the '<em><b>Min</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min</em>' attribute.
	 * @see #setMin(long)
	 * @see ucm_supplement.ucm_enhanced_configuration.Ucm_enhanced_configurationPackage#getPolicyDependencyParameter_Min()
	 * @model default="1" required="true"
	 * @generated
	 */
	long getMin();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_enhanced_configuration.PolicyDependencyParameter#getMin <em>Min</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min</em>' attribute.
	 * @see #getMin()
	 * @generated
	 */
	void setMin(long value);

	/**
	 * Returns the value of the '<em><b>Max</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max</em>' attribute.
	 * @see #setMax(long)
	 * @see ucm_supplement.ucm_enhanced_configuration.Ucm_enhanced_configurationPackage#getPolicyDependencyParameter_Max()
	 * @model default="1" required="true"
	 * @generated
	 */
	long getMax();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_enhanced_configuration.PolicyDependencyParameter#getMax <em>Max</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max</em>' attribute.
	 * @see #getMax()
	 * @generated
	 */
	void setMax(long value);

} // PolicyDependencyParameter
