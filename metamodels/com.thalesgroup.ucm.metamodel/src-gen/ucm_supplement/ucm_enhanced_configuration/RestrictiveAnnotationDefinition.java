/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_enhanced_configuration;

import org.eclipse.emf.common.util.EList;

import ucm_base.ucm_contracts.AnnotationDefinition;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Restrictive Annotation Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_enhanced_configuration.RestrictiveAnnotationDefinition#getAnnotableEntities <em>Annotable Entities</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_enhanced_configuration.Ucm_enhanced_configurationPackage#getRestrictiveAnnotationDefinition()
 * @model
 * @generated
 */
public interface RestrictiveAnnotationDefinition extends AnnotationDefinition {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Annotable Entities</b></em>' attribute list.
	 * The list contents are of type {@link ucm_supplement.ucm_enhanced_configuration.AnnotableEntities}.
	 * The literals are from the enumeration {@link ucm_supplement.ucm_enhanced_configuration.AnnotableEntities}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Annotable Entities</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotable Entities</em>' attribute list.
	 * @see ucm_supplement.ucm_enhanced_configuration.AnnotableEntities
	 * @see ucm_supplement.ucm_enhanced_configuration.Ucm_enhanced_configurationPackage#getRestrictiveAnnotationDefinition_AnnotableEntities()
	 * @model
	 * @generated
	 */
	EList<AnnotableEntities> getAnnotableEntities();

} // RestrictiveAnnotationDefinition
