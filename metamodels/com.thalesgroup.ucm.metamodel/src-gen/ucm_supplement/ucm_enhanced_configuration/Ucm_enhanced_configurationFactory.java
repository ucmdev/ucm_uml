/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_enhanced_configuration;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see ucm_supplement.ucm_enhanced_configuration.Ucm_enhanced_configurationPackage
 * @generated
 */
public interface Ucm_enhanced_configurationFactory extends EFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Ucm_enhanced_configurationFactory eINSTANCE = ucm_supplement.ucm_enhanced_configuration.impl.Ucm_enhanced_configurationFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Restrictive Annotation Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Restrictive Annotation Definition</em>'.
	 * @generated
	 */
	RestrictiveAnnotationDefinition createRestrictiveAnnotationDefinition();

	/**
	 * Returns a new object of class '<em>Policy Dependency Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Policy Dependency Parameter</em>'.
	 * @generated
	 */
	PolicyDependencyParameter createPolicyDependencyParameter();

	/**
	 * Returns a new object of class '<em>Policy Dependency Parameter Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Policy Dependency Parameter Value</em>'.
	 * @generated
	 */
	PolicyDependencyParameterValue createPolicyDependencyParameterValue();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	Ucm_enhanced_configurationPackage getUcm_enhanced_configurationPackage();

} //Ucm_enhanced_configurationFactory
