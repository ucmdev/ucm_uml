/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_enhanced_configuration;

import java.util.Map;
import org.eclipse.emf.common.util.DiagnosticChain;
import ucm_base.ucm_components.ComponentTechnicalPolicy;

import ucm_base.ucm_contracts.IConfigurationParameterValue;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Policy Dependency Parameter Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_enhanced_configuration.PolicyDependencyParameterValue#getParameterDefinition <em>Parameter Definition</em>}</li>
 *   <li>{@link ucm_supplement.ucm_enhanced_configuration.PolicyDependencyParameterValue#getPolicy <em>Policy</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_enhanced_configuration.Ucm_enhanced_configurationPackage#getPolicyDependencyParameterValue()
 * @model
 * @generated
 */
public interface PolicyDependencyParameterValue extends IConfigurationParameterValue {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Parameter Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Definition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Definition</em>' reference.
	 * @see #setParameterDefinition(PolicyDependencyParameter)
	 * @see ucm_supplement.ucm_enhanced_configuration.Ucm_enhanced_configurationPackage#getPolicyDependencyParameterValue_ParameterDefinition()
	 * @model required="true"
	 * @generated
	 */
	PolicyDependencyParameter getParameterDefinition();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_enhanced_configuration.PolicyDependencyParameterValue#getParameterDefinition <em>Parameter Definition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter Definition</em>' reference.
	 * @see #getParameterDefinition()
	 * @generated
	 */
	void setParameterDefinition(PolicyDependencyParameter value);

	/**
	 * Returns the value of the '<em><b>Policy</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Policy</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Policy</em>' reference.
	 * @see #setPolicy(ComponentTechnicalPolicy)
	 * @see ucm_supplement.ucm_enhanced_configuration.Ucm_enhanced_configurationPackage#getPolicyDependencyParameterValue_Policy()
	 * @model required="true"
	 * @generated
	 */
	ComponentTechnicalPolicy getPolicy();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_enhanced_configuration.PolicyDependencyParameterValue#getPolicy <em>Policy</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Policy</em>' reference.
	 * @see #getPolicy()
	 * @generated
	 */
	void setPolicy(ComponentTechnicalPolicy value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean parameterDefinitionConsistency(DiagnosticChain diagnostics, Map<Object, Object> context);

} // PolicyDependencyParameterValue
