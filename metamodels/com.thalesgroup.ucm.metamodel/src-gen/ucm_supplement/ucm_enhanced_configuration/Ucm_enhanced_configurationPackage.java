/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_enhanced_configuration;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import ucm_base.ucm_contracts.Ucm_contractsPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ucm_supplement.ucm_enhanced_configuration.Ucm_enhanced_configurationFactory
 * @model kind="package"
 * @generated
 */
public interface Ucm_enhanced_configurationPackage extends EPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ucm_enhanced_configuration";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://com.thalesgroup.ucm/1.0/metamodels/supplement/enhancedconfiguration";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ucm_enhanced_configuration";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Ucm_enhanced_configurationPackage eINSTANCE = ucm_supplement.ucm_enhanced_configuration.impl.Ucm_enhanced_configurationPackageImpl.init();

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_enhanced_configuration.impl.RestrictiveAnnotationDefinitionImpl <em>Restrictive Annotation Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_enhanced_configuration.impl.RestrictiveAnnotationDefinitionImpl
	 * @see ucm_supplement.ucm_enhanced_configuration.impl.Ucm_enhanced_configurationPackageImpl#getRestrictiveAnnotationDefinition()
	 * @generated
	 */
	int RESTRICTIVE_ANNOTATION_DEFINITION = 0;

	/**
	 * The feature id for the '<em><b>Configuration Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESTRICTIVE_ANNOTATION_DEFINITION__CONFIGURATION_PARAMETER = Ucm_contractsPackage.ANNOTATION_DEFINITION__CONFIGURATION_PARAMETER;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESTRICTIVE_ANNOTATION_DEFINITION__NAME = Ucm_contractsPackage.ANNOTATION_DEFINITION__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESTRICTIVE_ANNOTATION_DEFINITION__COMMENT = Ucm_contractsPackage.ANNOTATION_DEFINITION__COMMENT;

	/**
	 * The feature id for the '<em><b>Annotable Entities</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESTRICTIVE_ANNOTATION_DEFINITION__ANNOTABLE_ENTITIES = Ucm_contractsPackage.ANNOTATION_DEFINITION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Restrictive Annotation Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESTRICTIVE_ANNOTATION_DEFINITION_FEATURE_COUNT = Ucm_contractsPackage.ANNOTATION_DEFINITION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_enhanced_configuration.impl.PolicyDependencyParameterImpl <em>Policy Dependency Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_enhanced_configuration.impl.PolicyDependencyParameterImpl
	 * @see ucm_supplement.ucm_enhanced_configuration.impl.Ucm_enhanced_configurationPackageImpl#getPolicyDependencyParameter()
	 * @generated
	 */
	int POLICY_DEPENDENCY_PARAMETER = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLICY_DEPENDENCY_PARAMETER__NAME = Ucm_contractsPackage.ICONFIGURATION_PARAMETER__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLICY_DEPENDENCY_PARAMETER__COMMENT = Ucm_contractsPackage.ICONFIGURATION_PARAMETER__COMMENT;

	/**
	 * The feature id for the '<em><b>Allowed Policy Definition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLICY_DEPENDENCY_PARAMETER__ALLOWED_POLICY_DEFINITION = Ucm_contractsPackage.ICONFIGURATION_PARAMETER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Min</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLICY_DEPENDENCY_PARAMETER__MIN = Ucm_contractsPackage.ICONFIGURATION_PARAMETER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Max</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLICY_DEPENDENCY_PARAMETER__MAX = Ucm_contractsPackage.ICONFIGURATION_PARAMETER_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Policy Dependency Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLICY_DEPENDENCY_PARAMETER_FEATURE_COUNT = Ucm_contractsPackage.ICONFIGURATION_PARAMETER_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_enhanced_configuration.impl.PolicyDependencyParameterValueImpl <em>Policy Dependency Parameter Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_enhanced_configuration.impl.PolicyDependencyParameterValueImpl
	 * @see ucm_supplement.ucm_enhanced_configuration.impl.Ucm_enhanced_configurationPackageImpl#getPolicyDependencyParameterValue()
	 * @generated
	 */
	int POLICY_DEPENDENCY_PARAMETER_VALUE = 2;

	/**
	 * The feature id for the '<em><b>Parameter Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLICY_DEPENDENCY_PARAMETER_VALUE__PARAMETER_DEFINITION = Ucm_contractsPackage.ICONFIGURATION_PARAMETER_VALUE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Policy</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLICY_DEPENDENCY_PARAMETER_VALUE__POLICY = Ucm_contractsPackage.ICONFIGURATION_PARAMETER_VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Policy Dependency Parameter Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLICY_DEPENDENCY_PARAMETER_VALUE_FEATURE_COUNT = Ucm_contractsPackage.ICONFIGURATION_PARAMETER_VALUE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_enhanced_configuration.AnnotableEntities <em>Annotable Entities</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_enhanced_configuration.AnnotableEntities
	 * @see ucm_supplement.ucm_enhanced_configuration.impl.Ucm_enhanced_configurationPackageImpl#getAnnotableEntities()
	 * @generated
	 */
	int ANNOTABLE_ENTITIES = 3;


	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_enhanced_configuration.RestrictiveAnnotationDefinition <em>Restrictive Annotation Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Restrictive Annotation Definition</em>'.
	 * @see ucm_supplement.ucm_enhanced_configuration.RestrictiveAnnotationDefinition
	 * @generated
	 */
	EClass getRestrictiveAnnotationDefinition();

	/**
	 * Returns the meta object for the attribute list '{@link ucm_supplement.ucm_enhanced_configuration.RestrictiveAnnotationDefinition#getAnnotableEntities <em>Annotable Entities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Annotable Entities</em>'.
	 * @see ucm_supplement.ucm_enhanced_configuration.RestrictiveAnnotationDefinition#getAnnotableEntities()
	 * @see #getRestrictiveAnnotationDefinition()
	 * @generated
	 */
	EAttribute getRestrictiveAnnotationDefinition_AnnotableEntities();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_enhanced_configuration.PolicyDependencyParameter <em>Policy Dependency Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Policy Dependency Parameter</em>'.
	 * @see ucm_supplement.ucm_enhanced_configuration.PolicyDependencyParameter
	 * @generated
	 */
	EClass getPolicyDependencyParameter();

	/**
	 * Returns the meta object for the reference list '{@link ucm_supplement.ucm_enhanced_configuration.PolicyDependencyParameter#getAllowedPolicyDefinition <em>Allowed Policy Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Allowed Policy Definition</em>'.
	 * @see ucm_supplement.ucm_enhanced_configuration.PolicyDependencyParameter#getAllowedPolicyDefinition()
	 * @see #getPolicyDependencyParameter()
	 * @generated
	 */
	EReference getPolicyDependencyParameter_AllowedPolicyDefinition();

	/**
	 * Returns the meta object for the attribute '{@link ucm_supplement.ucm_enhanced_configuration.PolicyDependencyParameter#getMin <em>Min</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min</em>'.
	 * @see ucm_supplement.ucm_enhanced_configuration.PolicyDependencyParameter#getMin()
	 * @see #getPolicyDependencyParameter()
	 * @generated
	 */
	EAttribute getPolicyDependencyParameter_Min();

	/**
	 * Returns the meta object for the attribute '{@link ucm_supplement.ucm_enhanced_configuration.PolicyDependencyParameter#getMax <em>Max</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max</em>'.
	 * @see ucm_supplement.ucm_enhanced_configuration.PolicyDependencyParameter#getMax()
	 * @see #getPolicyDependencyParameter()
	 * @generated
	 */
	EAttribute getPolicyDependencyParameter_Max();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_enhanced_configuration.PolicyDependencyParameterValue <em>Policy Dependency Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Policy Dependency Parameter Value</em>'.
	 * @see ucm_supplement.ucm_enhanced_configuration.PolicyDependencyParameterValue
	 * @generated
	 */
	EClass getPolicyDependencyParameterValue();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_enhanced_configuration.PolicyDependencyParameterValue#getParameterDefinition <em>Parameter Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parameter Definition</em>'.
	 * @see ucm_supplement.ucm_enhanced_configuration.PolicyDependencyParameterValue#getParameterDefinition()
	 * @see #getPolicyDependencyParameterValue()
	 * @generated
	 */
	EReference getPolicyDependencyParameterValue_ParameterDefinition();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_enhanced_configuration.PolicyDependencyParameterValue#getPolicy <em>Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Policy</em>'.
	 * @see ucm_supplement.ucm_enhanced_configuration.PolicyDependencyParameterValue#getPolicy()
	 * @see #getPolicyDependencyParameterValue()
	 * @generated
	 */
	EReference getPolicyDependencyParameterValue_Policy();

	/**
	 * Returns the meta object for enum '{@link ucm_supplement.ucm_enhanced_configuration.AnnotableEntities <em>Annotable Entities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Annotable Entities</em>'.
	 * @see ucm_supplement.ucm_enhanced_configuration.AnnotableEntities
	 * @generated
	 */
	EEnum getAnnotableEntities();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	Ucm_enhanced_configurationFactory getUcm_enhanced_configurationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_enhanced_configuration.impl.RestrictiveAnnotationDefinitionImpl <em>Restrictive Annotation Definition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_enhanced_configuration.impl.RestrictiveAnnotationDefinitionImpl
		 * @see ucm_supplement.ucm_enhanced_configuration.impl.Ucm_enhanced_configurationPackageImpl#getRestrictiveAnnotationDefinition()
		 * @generated
		 */
		EClass RESTRICTIVE_ANNOTATION_DEFINITION = eINSTANCE.getRestrictiveAnnotationDefinition();

		/**
		 * The meta object literal for the '<em><b>Annotable Entities</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESTRICTIVE_ANNOTATION_DEFINITION__ANNOTABLE_ENTITIES = eINSTANCE.getRestrictiveAnnotationDefinition_AnnotableEntities();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_enhanced_configuration.impl.PolicyDependencyParameterImpl <em>Policy Dependency Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_enhanced_configuration.impl.PolicyDependencyParameterImpl
		 * @see ucm_supplement.ucm_enhanced_configuration.impl.Ucm_enhanced_configurationPackageImpl#getPolicyDependencyParameter()
		 * @generated
		 */
		EClass POLICY_DEPENDENCY_PARAMETER = eINSTANCE.getPolicyDependencyParameter();

		/**
		 * The meta object literal for the '<em><b>Allowed Policy Definition</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference POLICY_DEPENDENCY_PARAMETER__ALLOWED_POLICY_DEFINITION = eINSTANCE.getPolicyDependencyParameter_AllowedPolicyDefinition();

		/**
		 * The meta object literal for the '<em><b>Min</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POLICY_DEPENDENCY_PARAMETER__MIN = eINSTANCE.getPolicyDependencyParameter_Min();

		/**
		 * The meta object literal for the '<em><b>Max</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POLICY_DEPENDENCY_PARAMETER__MAX = eINSTANCE.getPolicyDependencyParameter_Max();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_enhanced_configuration.impl.PolicyDependencyParameterValueImpl <em>Policy Dependency Parameter Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_enhanced_configuration.impl.PolicyDependencyParameterValueImpl
		 * @see ucm_supplement.ucm_enhanced_configuration.impl.Ucm_enhanced_configurationPackageImpl#getPolicyDependencyParameterValue()
		 * @generated
		 */
		EClass POLICY_DEPENDENCY_PARAMETER_VALUE = eINSTANCE.getPolicyDependencyParameterValue();

		/**
		 * The meta object literal for the '<em><b>Parameter Definition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference POLICY_DEPENDENCY_PARAMETER_VALUE__PARAMETER_DEFINITION = eINSTANCE.getPolicyDependencyParameterValue_ParameterDefinition();

		/**
		 * The meta object literal for the '<em><b>Policy</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference POLICY_DEPENDENCY_PARAMETER_VALUE__POLICY = eINSTANCE.getPolicyDependencyParameterValue_Policy();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_enhanced_configuration.AnnotableEntities <em>Annotable Entities</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_enhanced_configuration.AnnotableEntities
		 * @see ucm_supplement.ucm_enhanced_configuration.impl.Ucm_enhanced_configurationPackageImpl#getAnnotableEntities()
		 * @generated
		 */
		EEnum ANNOTABLE_ENTITIES = eINSTANCE.getAnnotableEntities();

	}

} //Ucm_enhanced_configurationPackage
