/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_enhanced_configuration;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Annotable Entities</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see ucm_supplement.ucm_enhanced_configuration.Ucm_enhanced_configurationPackage#getAnnotableEntities()
 * @model
 * @generated
 */
public enum AnnotableEntities implements Enumerator {
	/**
	 * The '<em><b>All</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ALL_VALUE
	 * @generated
	 * @ordered
	 */
	ALL(0, "all", "all"),

	/**
	 * The '<em><b>Interface</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTERFACE_VALUE
	 * @generated
	 * @ordered
	 */
	INTERFACE(1, "interface", "interface"),

	/**
	 * The '<em><b>Method</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #METHOD_VALUE
	 * @generated
	 * @ordered
	 */
	METHOD(2, "method", "method"),

	/**
	 * The '<em><b>Attribute</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ATTRIBUTE_VALUE
	 * @generated
	 * @ordered
	 */
	ATTRIBUTE(3, "attribute", "attribute"),

	/**
	 * The '<em><b>Data Type</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DATA_TYPE_VALUE
	 * @generated
	 * @ordered
	 */
	DATA_TYPE(4, "dataType", "dataType"),

	/**
	 * The '<em><b>Component Definition</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #COMPONENT_DEFINITION_VALUE
	 * @generated
	 * @ordered
	 */
	COMPONENT_DEFINITION(5, "componentDefinition", "componentDefinition"),

	/**
	 * The '<em><b>Component Type</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #COMPONENT_TYPE_VALUE
	 * @generated
	 * @ordered
	 */
	COMPONENT_TYPE(6, "componentType", "componentType"),

	/**
	 * The '<em><b>Port</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PORT_VALUE
	 * @generated
	 * @ordered
	 */
	PORT(7, "port", "port"),

	/**
	 * The '<em><b>Component Implementation</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #COMPONENT_IMPLEMENTATION_VALUE
	 * @generated
	 * @ordered
	 */
	COMPONENT_IMPLEMENTATION(8, "componentImplementation", "componentImplementation"),

	/**
	 * The '<em><b>Atomic Implementation</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ATOMIC_IMPLEMENTATION_VALUE
	 * @generated
	 * @ordered
	 */
	ATOMIC_IMPLEMENTATION(9, "atomicImplementation", "atomicImplementation"),

	/**
	 * The '<em><b>Composite Implementation</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #COMPOSITE_IMPLEMENTATION_VALUE
	 * @generated
	 * @ordered
	 */
	COMPOSITE_IMPLEMENTATION(10, "compositeImplementation", "compositeImplementation"),

	/**
	 * The '<em><b>Assembly Part</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSEMBLY_PART_VALUE
	 * @generated
	 * @ordered
	 */
	ASSEMBLY_PART(11, "assemblyPart", "assemblyPart"),

	/**
	 * The '<em><b>Component Instance</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #COMPONENT_INSTANCE_VALUE
	 * @generated
	 * @ordered
	 */
	COMPONENT_INSTANCE(12, "componentInstance", "componentInstance"),

	/**
	 * The '<em><b>Port Implementation</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PORT_IMPLEMENTATION_VALUE
	 * @generated
	 * @ordered
	 */
	PORT_IMPLEMENTATION(13, "portImplementation", "portImplementation"),

	/**
	 * The '<em><b>Artefact</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ARTEFACT_VALUE
	 * @generated
	 * @ordered
	 */
	ARTEFACT(14, "artefact", "artefact"),

	/**
	 * The '<em><b>Component Node</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #COMPONENT_NODE_VALUE
	 * @generated
	 * @ordered
	 */
	COMPONENT_NODE(15, "componentNode", "componentNode");

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The '<em><b>All</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>All</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ALL
	 * @model name="all"
	 * @generated
	 * @ordered
	 */
	public static final int ALL_VALUE = 0;

	/**
	 * The '<em><b>Interface</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Interface</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTERFACE
	 * @model name="interface"
	 * @generated
	 * @ordered
	 */
	public static final int INTERFACE_VALUE = 1;

	/**
	 * The '<em><b>Method</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Method</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #METHOD
	 * @model name="method"
	 * @generated
	 * @ordered
	 */
	public static final int METHOD_VALUE = 2;

	/**
	 * The '<em><b>Attribute</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Attribute</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ATTRIBUTE
	 * @model name="attribute"
	 * @generated
	 * @ordered
	 */
	public static final int ATTRIBUTE_VALUE = 3;

	/**
	 * The '<em><b>Data Type</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Data Type</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DATA_TYPE
	 * @model name="dataType"
	 * @generated
	 * @ordered
	 */
	public static final int DATA_TYPE_VALUE = 4;

	/**
	 * The '<em><b>Component Definition</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Component Definition</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #COMPONENT_DEFINITION
	 * @model name="componentDefinition"
	 * @generated
	 * @ordered
	 */
	public static final int COMPONENT_DEFINITION_VALUE = 5;

	/**
	 * The '<em><b>Component Type</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Component Type</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #COMPONENT_TYPE
	 * @model name="componentType"
	 * @generated
	 * @ordered
	 */
	public static final int COMPONENT_TYPE_VALUE = 6;

	/**
	 * The '<em><b>Port</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Port</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PORT
	 * @model name="port"
	 * @generated
	 * @ordered
	 */
	public static final int PORT_VALUE = 7;

	/**
	 * The '<em><b>Component Implementation</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Component Implementation</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #COMPONENT_IMPLEMENTATION
	 * @model name="componentImplementation"
	 * @generated
	 * @ordered
	 */
	public static final int COMPONENT_IMPLEMENTATION_VALUE = 8;

	/**
	 * The '<em><b>Atomic Implementation</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Atomic Implementation</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ATOMIC_IMPLEMENTATION
	 * @model name="atomicImplementation"
	 * @generated
	 * @ordered
	 */
	public static final int ATOMIC_IMPLEMENTATION_VALUE = 9;

	/**
	 * The '<em><b>Composite Implementation</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Composite Implementation</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #COMPOSITE_IMPLEMENTATION
	 * @model name="compositeImplementation"
	 * @generated
	 * @ordered
	 */
	public static final int COMPOSITE_IMPLEMENTATION_VALUE = 10;

	/**
	 * The '<em><b>Assembly Part</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Assembly Part</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSEMBLY_PART
	 * @model name="assemblyPart"
	 * @generated
	 * @ordered
	 */
	public static final int ASSEMBLY_PART_VALUE = 11;

	/**
	 * The '<em><b>Component Instance</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Component Instance</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #COMPONENT_INSTANCE
	 * @model name="componentInstance"
	 * @generated
	 * @ordered
	 */
	public static final int COMPONENT_INSTANCE_VALUE = 12;

	/**
	 * The '<em><b>Port Implementation</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Port Implementation</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PORT_IMPLEMENTATION
	 * @model name="portImplementation"
	 * @generated
	 * @ordered
	 */
	public static final int PORT_IMPLEMENTATION_VALUE = 13;

	/**
	 * The '<em><b>Artefact</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Artefact</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ARTEFACT
	 * @model name="artefact"
	 * @generated
	 * @ordered
	 */
	public static final int ARTEFACT_VALUE = 14;

	/**
	 * The '<em><b>Component Node</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Component Node</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #COMPONENT_NODE
	 * @model name="componentNode"
	 * @generated
	 * @ordered
	 */
	public static final int COMPONENT_NODE_VALUE = 15;

	/**
	 * An array of all the '<em><b>Annotable Entities</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final AnnotableEntities[] VALUES_ARRAY =
		new AnnotableEntities[] {
			ALL,
			INTERFACE,
			METHOD,
			ATTRIBUTE,
			DATA_TYPE,
			COMPONENT_DEFINITION,
			COMPONENT_TYPE,
			PORT,
			COMPONENT_IMPLEMENTATION,
			ATOMIC_IMPLEMENTATION,
			COMPOSITE_IMPLEMENTATION,
			ASSEMBLY_PART,
			COMPONENT_INSTANCE,
			PORT_IMPLEMENTATION,
			ARTEFACT,
			COMPONENT_NODE,
		};

	/**
	 * A public read-only list of all the '<em><b>Annotable Entities</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<AnnotableEntities> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Annotable Entities</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static AnnotableEntities get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			AnnotableEntities result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Annotable Entities</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static AnnotableEntities getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			AnnotableEntities result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Annotable Entities</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static AnnotableEntities get(int value) {
		switch (value) {
			case ALL_VALUE: return ALL;
			case INTERFACE_VALUE: return INTERFACE;
			case METHOD_VALUE: return METHOD;
			case ATTRIBUTE_VALUE: return ATTRIBUTE;
			case DATA_TYPE_VALUE: return DATA_TYPE;
			case COMPONENT_DEFINITION_VALUE: return COMPONENT_DEFINITION;
			case COMPONENT_TYPE_VALUE: return COMPONENT_TYPE;
			case PORT_VALUE: return PORT;
			case COMPONENT_IMPLEMENTATION_VALUE: return COMPONENT_IMPLEMENTATION;
			case ATOMIC_IMPLEMENTATION_VALUE: return ATOMIC_IMPLEMENTATION;
			case COMPOSITE_IMPLEMENTATION_VALUE: return COMPOSITE_IMPLEMENTATION;
			case ASSEMBLY_PART_VALUE: return ASSEMBLY_PART;
			case COMPONENT_INSTANCE_VALUE: return COMPONENT_INSTANCE;
			case PORT_IMPLEMENTATION_VALUE: return PORT_IMPLEMENTATION;
			case ARTEFACT_VALUE: return ARTEFACT;
			case COMPONENT_NODE_VALUE: return COMPONENT_NODE;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private AnnotableEntities(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //AnnotableEntities
