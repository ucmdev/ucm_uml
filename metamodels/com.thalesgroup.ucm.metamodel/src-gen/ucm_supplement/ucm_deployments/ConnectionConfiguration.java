/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_deployments;

import java.util.Map;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import ucm_base.ucm_commons.INamed;

import ucm_base.ucm_contracts.IConfigured;

import ucm_base.ucm_interactions.ConnectorDefinition;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Connection Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_deployments.ConnectionConfiguration#getConnection <em>Connection</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.ConnectionConfiguration#getEndConfiguration <em>End Configuration</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.ConnectionConfiguration#getRefinedIn <em>Refined In</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getConnectionConfiguration()
 * @model
 * @generated
 */
public interface ConnectionConfiguration extends INamed, IConfigured {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Connection</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connection</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connection</em>' reference.
	 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getConnectionConfiguration_Connection()
	 * @model required="true" changeable="false"
	 * @generated
	 */
	ConnectionInstance getConnection();

	/**
	 * Returns the value of the '<em><b>End Configuration</b></em>' containment reference list.
	 * The list contents are of type {@link ucm_supplement.ucm_deployments.ConnectionEndConfiguration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End Configuration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Configuration</em>' containment reference list.
	 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getConnectionConfiguration_EndConfiguration()
	 * @model containment="true"
	 * @generated
	 */
	EList<ConnectionEndConfiguration> getEndConfiguration();

	/**
	 * Returns the value of the '<em><b>Refined In</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Refined In</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Refined In</em>' reference.
	 * @see #setRefinedIn(ConnectorDefinition)
	 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getConnectionConfiguration_RefinedIn()
	 * @model
	 * @generated
	 */
	ConnectorDefinition getRefinedIn();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_deployments.ConnectionConfiguration#getRefinedIn <em>Refined In</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Refined In</em>' reference.
	 * @see #getRefinedIn()
	 * @generated
	 */
	void setRefinedIn(ConnectorDefinition value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean instanceBelongToAppAssembly(DiagnosticChain diagnostics, Map<Object, Object> context);

} // ConnectionConfiguration
