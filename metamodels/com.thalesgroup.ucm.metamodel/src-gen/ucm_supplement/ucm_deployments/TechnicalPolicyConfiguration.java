/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_deployments;

import java.util.Map;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import ucm_base.ucm_commons.INamed;

import ucm_base.ucm_components.ComponentTechnicalPolicy;

import ucm_base.ucm_contracts.IConfigured;

import ucm_base.ucm_technicalpolicies.TechnicalPolicyDefinition;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Technical Policy Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_deployments.TechnicalPolicyConfiguration#getPolicy <em>Policy</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.TechnicalPolicyConfiguration#getComponentInstance <em>Component Instance</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.TechnicalPolicyConfiguration#getRefinedIn <em>Refined In</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getTechnicalPolicyConfiguration()
 * @model
 * @generated
 */
public interface TechnicalPolicyConfiguration extends INamed, IConfigured {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Policy</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Policy</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Policy</em>' reference.
	 * @see #setPolicy(ComponentTechnicalPolicy)
	 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getTechnicalPolicyConfiguration_Policy()
	 * @model required="true"
	 * @generated
	 */
	ComponentTechnicalPolicy getPolicy();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_deployments.TechnicalPolicyConfiguration#getPolicy <em>Policy</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Policy</em>' reference.
	 * @see #getPolicy()
	 * @generated
	 */
	void setPolicy(ComponentTechnicalPolicy value);

	/**
	 * Returns the value of the '<em><b>Component Instance</b></em>' reference list.
	 * The list contents are of type {@link ucm_supplement.ucm_deployments.ComponentInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Instance</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Instance</em>' reference list.
	 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getTechnicalPolicyConfiguration_ComponentInstance()
	 * @model required="true"
	 * @generated
	 */
	EList<ComponentInstance> getComponentInstance();

	/**
	 * Returns the value of the '<em><b>Refined In</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Refined In</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Refined In</em>' reference.
	 * @see #setRefinedIn(TechnicalPolicyDefinition)
	 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getTechnicalPolicyConfiguration_RefinedIn()
	 * @model
	 * @generated
	 */
	TechnicalPolicyDefinition getRefinedIn();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_deployments.TechnicalPolicyConfiguration#getRefinedIn <em>Refined In</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Refined In</em>' reference.
	 * @see #getRefinedIn()
	 * @generated
	 */
	void setRefinedIn(TechnicalPolicyDefinition value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean policyManagesComponent(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean instanceBelongToAppAssembly(DiagnosticChain diagnostics, Map<Object, Object> context);

} // TechnicalPolicyConfiguration
