/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_deployments;

import java.util.Map;
import org.eclipse.emf.common.util.DiagnosticChain;
import ucm_base.ucm_commons.INamed;

import ucm_base.ucm_components.AssemblyPart;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Assembly Part Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_deployments.AssemblyPartReference#getReferenceToSubpart <em>Reference To Subpart</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.AssemblyPartReference#getPart <em>Part</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getAssemblyPartReference()
 * @model
 * @generated
 */
public interface AssemblyPartReference extends INamed {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Reference To Subpart</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reference To Subpart</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference To Subpart</em>' containment reference.
	 * @see #setReferenceToSubpart(AssemblyPartReference)
	 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getAssemblyPartReference_ReferenceToSubpart()
	 * @model containment="true"
	 * @generated
	 */
	AssemblyPartReference getReferenceToSubpart();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_deployments.AssemblyPartReference#getReferenceToSubpart <em>Reference To Subpart</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reference To Subpart</em>' containment reference.
	 * @see #getReferenceToSubpart()
	 * @generated
	 */
	void setReferenceToSubpart(AssemblyPartReference value);

	/**
	 * Returns the value of the '<em><b>Part</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Part</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Part</em>' reference.
	 * @see #setPart(AssemblyPart)
	 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getAssemblyPartReference_Part()
	 * @model required="true"
	 * @generated
	 */
	AssemblyPart getPart();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_deployments.AssemblyPartReference#getPart <em>Part</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Part</em>' reference.
	 * @see #getPart()
	 * @generated
	 */
	void setPart(AssemblyPart value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean subpartBelongsToPart(DiagnosticChain diagnostics, Map<Object, Object> context);

} // AssemblyPartReference
