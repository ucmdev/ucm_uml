/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_deployments;

import java.util.Map;
import org.eclipse.emf.common.util.DiagnosticChain;
import ucm_base.ucm_commons.INamed;

import ucm_base.ucm_components.Port;

import ucm_base.ucm_contracts.IAnnotable;

import ucm_base.ucm_interactions.PortElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component Instance Port Initialization</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_deployments.ComponentInstancePortInitialization#getPort <em>Port</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.ComponentInstancePortInitialization#getPortElement <em>Port Element</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getComponentInstancePortInitialization()
 * @model
 * @generated
 */
public interface ComponentInstancePortInitialization extends IHasAttributeInitialization, INamed, IAnnotable {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port</em>' reference.
	 * @see #setPort(Port)
	 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getComponentInstancePortInitialization_Port()
	 * @model required="true"
	 * @generated
	 */
	Port getPort();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_deployments.ComponentInstancePortInitialization#getPort <em>Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port</em>' reference.
	 * @see #getPort()
	 * @generated
	 */
	void setPort(Port value);

	/**
	 * Returns the value of the '<em><b>Port Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port Element</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port Element</em>' reference.
	 * @see #setPortElement(PortElement)
	 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getComponentInstancePortInitialization_PortElement()
	 * @model required="true"
	 * @generated
	 */
	PortElement getPortElement();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_deployments.ComponentInstancePortInitialization#getPortElement <em>Port Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port Element</em>' reference.
	 * @see #getPortElement()
	 * @generated
	 */
	void setPortElement(PortElement value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean attributeBelongsToPort(DiagnosticChain diagnostics, Map<Object, Object> context);

} // ComponentInstancePortInitialization
