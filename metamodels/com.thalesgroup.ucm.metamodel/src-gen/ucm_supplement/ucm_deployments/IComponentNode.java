/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_deployments;

import org.eclipse.emf.common.util.EList;

import ucm_base.ucm_commons.INamed;

import ucm_base.ucm_contracts.IAnnotable;

import ucm_base.ucm_technicalpolicies.Language;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IComponent Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_deployments.IComponentNode#getLanguage <em>Language</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.IComponentNode#getComponentInstance <em>Component Instance</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getIComponentNode()
 * @model abstract="true"
 * @generated
 */
public interface IComponentNode extends INamed, IAnnotable {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Language</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Language</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Language</em>' reference.
	 * @see #setLanguage(Language)
	 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getIComponentNode_Language()
	 * @model
	 * @generated
	 */
	Language getLanguage();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_deployments.IComponentNode#getLanguage <em>Language</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Language</em>' reference.
	 * @see #getLanguage()
	 * @generated
	 */
	void setLanguage(Language value);

	/**
	 * Returns the value of the '<em><b>Component Instance</b></em>' reference list.
	 * The list contents are of type {@link ucm_supplement.ucm_deployments.ComponentInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Instance</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Instance</em>' reference list.
	 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getIComponentNode_ComponentInstance()
	 * @model
	 * @generated
	 */
	EList<ComponentInstance> getComponentInstance();

} // IComponentNode
