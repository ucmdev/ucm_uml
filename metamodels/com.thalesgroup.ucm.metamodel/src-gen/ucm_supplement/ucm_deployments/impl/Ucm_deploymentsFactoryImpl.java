/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_deployments.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import ucm_supplement.ucm_deployments.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Ucm_deploymentsFactoryImpl extends EFactoryImpl implements Ucm_deploymentsFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Ucm_deploymentsFactory init() {
		try {
			Ucm_deploymentsFactory theUcm_deploymentsFactory = (Ucm_deploymentsFactory)EPackage.Registry.INSTANCE.getEFactory(Ucm_deploymentsPackage.eNS_URI);
			if (theUcm_deploymentsFactory != null) {
				return theUcm_deploymentsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new Ucm_deploymentsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ucm_deploymentsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case Ucm_deploymentsPackage.DEPLOYMENT_MODULE: return createDeploymentModule();
			case Ucm_deploymentsPackage.APP_ASSEMBLY: return createAppAssembly();
			case Ucm_deploymentsPackage.ALLOCATION_PLAN: return createAllocationPlan();
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION: return createComponentInstanceInitialization();
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION: return createComponentInstancePortInitialization();
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_POLICY_INITIALIZATION: return createComponentInstancePolicyInitialization();
			case Ucm_deploymentsPackage.ASSEMBLY_PART_REFERENCE: return createAssemblyPartReference();
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE: return createComponentInstance();
			case Ucm_deploymentsPackage.CONNECTION_INSTANCE: return createConnectionInstance();
			case Ucm_deploymentsPackage.CONNECTION_CONFIGURATION: return createConnectionConfiguration();
			case Ucm_deploymentsPackage.CONNECTION_END_CONFIGURATION: return createConnectionEndConfiguration();
			case Ucm_deploymentsPackage.TECHNICAL_POLICY_CONFIGURATION: return createTechnicalPolicyConfiguration();
			case Ucm_deploymentsPackage.ARTEFACT: return createArtefact();
			case Ucm_deploymentsPackage.ATTRIBUTE_VALUE: return createAttributeValue();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeploymentModule createDeploymentModule() {
		DeploymentModuleImpl deploymentModule = new DeploymentModuleImpl();
		return deploymentModule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AppAssembly createAppAssembly() {
		AppAssemblyImpl appAssembly = new AppAssemblyImpl();
		return appAssembly;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AllocationPlan createAllocationPlan() {
		AllocationPlanImpl allocationPlan = new AllocationPlanImpl();
		return allocationPlan;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstanceInitialization createComponentInstanceInitialization() {
		ComponentInstanceInitializationImpl componentInstanceInitialization = new ComponentInstanceInitializationImpl();
		return componentInstanceInitialization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstancePortInitialization createComponentInstancePortInitialization() {
		ComponentInstancePortInitializationImpl componentInstancePortInitialization = new ComponentInstancePortInitializationImpl();
		return componentInstancePortInitialization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstancePolicyInitialization createComponentInstancePolicyInitialization() {
		ComponentInstancePolicyInitializationImpl componentInstancePolicyInitialization = new ComponentInstancePolicyInitializationImpl();
		return componentInstancePolicyInitialization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyPartReference createAssemblyPartReference() {
		AssemblyPartReferenceImpl assemblyPartReference = new AssemblyPartReferenceImpl();
		return assemblyPartReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance createComponentInstance() {
		ComponentInstanceImpl componentInstance = new ComponentInstanceImpl();
		return componentInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionInstance createConnectionInstance() {
		ConnectionInstanceImpl connectionInstance = new ConnectionInstanceImpl();
		return connectionInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionConfiguration createConnectionConfiguration() {
		ConnectionConfigurationImpl connectionConfiguration = new ConnectionConfigurationImpl();
		return connectionConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionEndConfiguration createConnectionEndConfiguration() {
		ConnectionEndConfigurationImpl connectionEndConfiguration = new ConnectionEndConfigurationImpl();
		return connectionEndConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TechnicalPolicyConfiguration createTechnicalPolicyConfiguration() {
		TechnicalPolicyConfigurationImpl technicalPolicyConfiguration = new TechnicalPolicyConfigurationImpl();
		return technicalPolicyConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Artefact createArtefact() {
		ArtefactImpl artefact = new ArtefactImpl();
		return artefact;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeValue createAttributeValue() {
		AttributeValueImpl attributeValue = new AttributeValueImpl();
		return attributeValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ucm_deploymentsPackage getUcm_deploymentsPackage() {
		return (Ucm_deploymentsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static Ucm_deploymentsPackage getPackage() {
		return Ucm_deploymentsPackage.eINSTANCE;
	}

} //Ucm_deploymentsFactoryImpl
