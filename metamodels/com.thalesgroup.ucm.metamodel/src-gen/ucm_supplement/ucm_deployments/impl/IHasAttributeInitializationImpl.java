/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_deployments.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import ucm_supplement.ucm_deployments.AttributeValue;
import ucm_supplement.ucm_deployments.IHasAttributeInitialization;
import ucm_supplement.ucm_deployments.Ucm_deploymentsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>IHas Attribute Initialization</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_deployments.impl.IHasAttributeInitializationImpl#getAttributeInitialization <em>Attribute Initialization</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class IHasAttributeInitializationImpl extends MinimalEObjectImpl.Container implements IHasAttributeInitialization {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached value of the '{@link #getAttributeInitialization() <em>Attribute Initialization</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttributeInitialization()
	 * @generated
	 * @ordered
	 */
	protected EList<AttributeValue> attributeInitialization;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IHasAttributeInitializationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_deploymentsPackage.Literals.IHAS_ATTRIBUTE_INITIALIZATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AttributeValue> getAttributeInitialization() {
		if (attributeInitialization == null) {
			attributeInitialization = new EObjectContainmentEList<AttributeValue>(AttributeValue.class, this, Ucm_deploymentsPackage.IHAS_ATTRIBUTE_INITIALIZATION__ATTRIBUTE_INITIALIZATION);
		}
		return attributeInitialization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Ucm_deploymentsPackage.IHAS_ATTRIBUTE_INITIALIZATION__ATTRIBUTE_INITIALIZATION:
				return ((InternalEList<?>)getAttributeInitialization()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_deploymentsPackage.IHAS_ATTRIBUTE_INITIALIZATION__ATTRIBUTE_INITIALIZATION:
				return getAttributeInitialization();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_deploymentsPackage.IHAS_ATTRIBUTE_INITIALIZATION__ATTRIBUTE_INITIALIZATION:
				getAttributeInitialization().clear();
				getAttributeInitialization().addAll((Collection<? extends AttributeValue>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_deploymentsPackage.IHAS_ATTRIBUTE_INITIALIZATION__ATTRIBUTE_INITIALIZATION:
				getAttributeInitialization().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_deploymentsPackage.IHAS_ATTRIBUTE_INITIALIZATION__ATTRIBUTE_INITIALIZATION:
				return attributeInitialization != null && !attributeInitialization.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //IHasAttributeInitializationImpl
