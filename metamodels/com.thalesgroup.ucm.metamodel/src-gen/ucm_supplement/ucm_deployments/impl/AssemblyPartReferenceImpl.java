/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_deployments.impl;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import com.thalesgroup.ucm.metamodel.validation.UcmSupplementValidationFactory;

import ucm_base.ucm_commons.impl.INamedImpl;
import ucm_base.ucm_components.AssemblyPart;
import ucm_supplement.ucm_deployments.AssemblyPartReference;
import ucm_supplement.ucm_deployments.Ucm_deploymentsPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>Assembly Part Reference</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_deployments.impl.AssemblyPartReferenceImpl#getReferenceToSubpart <em>Reference To Subpart</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.impl.AssemblyPartReferenceImpl#getPart <em>Part</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AssemblyPartReferenceImpl extends INamedImpl implements AssemblyPartReference {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached value of the '{@link #getReferenceToSubpart() <em>Reference To Subpart</em>}' containment reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getReferenceToSubpart()
	 * @generated
	 * @ordered
	 */
	protected AssemblyPartReference referenceToSubpart;

	/**
	 * The cached value of the '{@link #getPart() <em>Part</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getPart()
	 * @generated
	 * @ordered
	 */
	protected AssemblyPart part;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected AssemblyPartReferenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_deploymentsPackage.Literals.ASSEMBLY_PART_REFERENCE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyPartReference getReferenceToSubpart() {
		return referenceToSubpart;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReferenceToSubpart(AssemblyPartReference newReferenceToSubpart,
			NotificationChain msgs) {
		AssemblyPartReference oldReferenceToSubpart = referenceToSubpart;
		referenceToSubpart = newReferenceToSubpart;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Ucm_deploymentsPackage.ASSEMBLY_PART_REFERENCE__REFERENCE_TO_SUBPART, oldReferenceToSubpart, newReferenceToSubpart);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setReferenceToSubpart(AssemblyPartReference newReferenceToSubpart) {
		if (newReferenceToSubpart != referenceToSubpart) {
			NotificationChain msgs = null;
			if (referenceToSubpart != null)
				msgs = ((InternalEObject)referenceToSubpart).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - Ucm_deploymentsPackage.ASSEMBLY_PART_REFERENCE__REFERENCE_TO_SUBPART, null, msgs);
			if (newReferenceToSubpart != null)
				msgs = ((InternalEObject)newReferenceToSubpart).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - Ucm_deploymentsPackage.ASSEMBLY_PART_REFERENCE__REFERENCE_TO_SUBPART, null, msgs);
			msgs = basicSetReferenceToSubpart(newReferenceToSubpart, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_deploymentsPackage.ASSEMBLY_PART_REFERENCE__REFERENCE_TO_SUBPART, newReferenceToSubpart, newReferenceToSubpart));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyPart getPart() {
		if (part != null && part.eIsProxy()) {
			InternalEObject oldPart = (InternalEObject)part;
			part = (AssemblyPart)eResolveProxy(oldPart);
			if (part != oldPart) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_deploymentsPackage.ASSEMBLY_PART_REFERENCE__PART, oldPart, part));
			}
		}
		return part;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyPart basicGetPart() {
		return part;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setPart(AssemblyPart newPart) {
		AssemblyPart oldPart = part;
		part = newPart;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_deploymentsPackage.ASSEMBLY_PART_REFERENCE__PART, oldPart, part));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean subpartBelongsToPart(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and
		// message
		// Ensure that you remove @generated or mark it @generated NOT
		return UcmSupplementValidationFactory.INSTANCE.getDeploymentValidator().subpartBelongsToPart(this, diagnostics,
				context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Ucm_deploymentsPackage.ASSEMBLY_PART_REFERENCE__REFERENCE_TO_SUBPART:
				return basicSetReferenceToSubpart(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_deploymentsPackage.ASSEMBLY_PART_REFERENCE__REFERENCE_TO_SUBPART:
				return getReferenceToSubpart();
			case Ucm_deploymentsPackage.ASSEMBLY_PART_REFERENCE__PART:
				if (resolve) return getPart();
				return basicGetPart();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_deploymentsPackage.ASSEMBLY_PART_REFERENCE__REFERENCE_TO_SUBPART:
				setReferenceToSubpart((AssemblyPartReference)newValue);
				return;
			case Ucm_deploymentsPackage.ASSEMBLY_PART_REFERENCE__PART:
				setPart((AssemblyPart)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_deploymentsPackage.ASSEMBLY_PART_REFERENCE__REFERENCE_TO_SUBPART:
				setReferenceToSubpart((AssemblyPartReference)null);
				return;
			case Ucm_deploymentsPackage.ASSEMBLY_PART_REFERENCE__PART:
				setPart((AssemblyPart)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_deploymentsPackage.ASSEMBLY_PART_REFERENCE__REFERENCE_TO_SUBPART:
				return referenceToSubpart != null;
			case Ucm_deploymentsPackage.ASSEMBLY_PART_REFERENCE__PART:
				return part != null;
		}
		return super.eIsSet(featureID);
	}

} // AssemblyPartReferenceImpl
