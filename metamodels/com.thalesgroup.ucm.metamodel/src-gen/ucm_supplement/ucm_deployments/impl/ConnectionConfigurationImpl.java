/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_deployments.impl;

import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import com.thalesgroup.ucm.metamodel.validation.UcmSupplementValidationFactory;

import ucm_base.ucm_commons.impl.INamedImpl;
import ucm_base.ucm_contracts.IConfigurationParameterValue;
import ucm_base.ucm_contracts.IConfigured;
import ucm_base.ucm_contracts.Ucm_contractsPackage;
import ucm_base.ucm_interactions.ConnectorDefinition;
import ucm_supplement.ucm_deployments.ConnectionConfiguration;
import ucm_supplement.ucm_deployments.ConnectionEndConfiguration;
import ucm_supplement.ucm_deployments.ConnectionInstance;
import ucm_supplement.ucm_deployments.Ucm_deploymentsPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>Connection Configuration</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_deployments.impl.ConnectionConfigurationImpl#getConfigurationValue <em>Configuration Value</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.impl.ConnectionConfigurationImpl#getConnection <em>Connection</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.impl.ConnectionConfigurationImpl#getEndConfiguration <em>End Configuration</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.impl.ConnectionConfigurationImpl#getRefinedIn <em>Refined In</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConnectionConfigurationImpl extends INamedImpl implements ConnectionConfiguration {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached value of the '{@link #getConfigurationValue()
	 * <em>Configuration Value</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getConfigurationValue()
	 * @generated
	 * @ordered
	 */
	protected EList<IConfigurationParameterValue> configurationValue;

	/**
	 * The cached value of the '{@link #getConnection() <em>Connection</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getConnection()
	 * @generated
	 * @ordered
	 */
	protected ConnectionInstance connection;

	/**
	 * The cached value of the '{@link #getEndConfiguration() <em>End Configuration</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndConfiguration()
	 * @generated
	 * @ordered
	 */
	protected EList<ConnectionEndConfiguration> endConfiguration;

	/**
	 * The cached value of the '{@link #getRefinedIn() <em>Refined In</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getRefinedIn()
	 * @generated
	 * @ordered
	 */
	protected ConnectorDefinition refinedIn;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected ConnectionConfigurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_deploymentsPackage.Literals.CONNECTION_CONFIGURATION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IConfigurationParameterValue> getConfigurationValue() {
		if (configurationValue == null) {
			configurationValue = new EObjectContainmentEList<IConfigurationParameterValue>(IConfigurationParameterValue.class, this, Ucm_deploymentsPackage.CONNECTION_CONFIGURATION__CONFIGURATION_VALUE);
		}
		return configurationValue;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionInstance getConnection() {
		if (connection != null && connection.eIsProxy()) {
			InternalEObject oldConnection = (InternalEObject)connection;
			connection = (ConnectionInstance)eResolveProxy(oldConnection);
			if (connection != oldConnection) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_deploymentsPackage.CONNECTION_CONFIGURATION__CONNECTION, oldConnection, connection));
			}
		}
		return connection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionInstance basicGetConnection() {
		return connection;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ConnectionEndConfiguration> getEndConfiguration() {
		if (endConfiguration == null) {
			endConfiguration = new EObjectContainmentEList<ConnectionEndConfiguration>(ConnectionEndConfiguration.class, this, Ucm_deploymentsPackage.CONNECTION_CONFIGURATION__END_CONFIGURATION);
		}
		return endConfiguration;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectorDefinition getRefinedIn() {
		if (refinedIn != null && refinedIn.eIsProxy()) {
			InternalEObject oldRefinedIn = (InternalEObject)refinedIn;
			refinedIn = (ConnectorDefinition)eResolveProxy(oldRefinedIn);
			if (refinedIn != oldRefinedIn) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_deploymentsPackage.CONNECTION_CONFIGURATION__REFINED_IN, oldRefinedIn, refinedIn));
			}
		}
		return refinedIn;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectorDefinition basicGetRefinedIn() {
		return refinedIn;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setRefinedIn(ConnectorDefinition newRefinedIn) {
		ConnectorDefinition oldRefinedIn = refinedIn;
		refinedIn = newRefinedIn;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_deploymentsPackage.CONNECTION_CONFIGURATION__REFINED_IN, oldRefinedIn, refinedIn));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean instanceBelongToAppAssembly(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and
		// message
		// Ensure that you remove @generated or mark it @generated NOT
		return UcmSupplementValidationFactory.INSTANCE.getDeploymentValidator().instanceBelongToAppAssembly(this,
				diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Ucm_deploymentsPackage.CONNECTION_CONFIGURATION__CONFIGURATION_VALUE:
				return ((InternalEList<?>)getConfigurationValue()).basicRemove(otherEnd, msgs);
			case Ucm_deploymentsPackage.CONNECTION_CONFIGURATION__END_CONFIGURATION:
				return ((InternalEList<?>)getEndConfiguration()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_deploymentsPackage.CONNECTION_CONFIGURATION__CONFIGURATION_VALUE:
				return getConfigurationValue();
			case Ucm_deploymentsPackage.CONNECTION_CONFIGURATION__CONNECTION:
				if (resolve) return getConnection();
				return basicGetConnection();
			case Ucm_deploymentsPackage.CONNECTION_CONFIGURATION__END_CONFIGURATION:
				return getEndConfiguration();
			case Ucm_deploymentsPackage.CONNECTION_CONFIGURATION__REFINED_IN:
				if (resolve) return getRefinedIn();
				return basicGetRefinedIn();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_deploymentsPackage.CONNECTION_CONFIGURATION__CONFIGURATION_VALUE:
				getConfigurationValue().clear();
				getConfigurationValue().addAll((Collection<? extends IConfigurationParameterValue>)newValue);
				return;
			case Ucm_deploymentsPackage.CONNECTION_CONFIGURATION__END_CONFIGURATION:
				getEndConfiguration().clear();
				getEndConfiguration().addAll((Collection<? extends ConnectionEndConfiguration>)newValue);
				return;
			case Ucm_deploymentsPackage.CONNECTION_CONFIGURATION__REFINED_IN:
				setRefinedIn((ConnectorDefinition)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_deploymentsPackage.CONNECTION_CONFIGURATION__CONFIGURATION_VALUE:
				getConfigurationValue().clear();
				return;
			case Ucm_deploymentsPackage.CONNECTION_CONFIGURATION__END_CONFIGURATION:
				getEndConfiguration().clear();
				return;
			case Ucm_deploymentsPackage.CONNECTION_CONFIGURATION__REFINED_IN:
				setRefinedIn((ConnectorDefinition)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_deploymentsPackage.CONNECTION_CONFIGURATION__CONFIGURATION_VALUE:
				return configurationValue != null && !configurationValue.isEmpty();
			case Ucm_deploymentsPackage.CONNECTION_CONFIGURATION__CONNECTION:
				return connection != null;
			case Ucm_deploymentsPackage.CONNECTION_CONFIGURATION__END_CONFIGURATION:
				return endConfiguration != null && !endConfiguration.isEmpty();
			case Ucm_deploymentsPackage.CONNECTION_CONFIGURATION__REFINED_IN:
				return refinedIn != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == IConfigured.class) {
			switch (derivedFeatureID) {
				case Ucm_deploymentsPackage.CONNECTION_CONFIGURATION__CONFIGURATION_VALUE: return Ucm_contractsPackage.ICONFIGURED__CONFIGURATION_VALUE;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == IConfigured.class) {
			switch (baseFeatureID) {
				case Ucm_contractsPackage.ICONFIGURED__CONFIGURATION_VALUE: return Ucm_deploymentsPackage.CONNECTION_CONFIGURATION__CONFIGURATION_VALUE;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} // ConnectionConfigurationImpl
