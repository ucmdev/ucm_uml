/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_deployments.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import ucm_base.ucm_commons.Ucm_commonsPackage;

import ucm_base.ucm_components.Ucm_componentsPackage;

import ucm_base.ucm_contracts.Ucm_contractsPackage;

import ucm_base.ucm_interactions.Ucm_interactionsPackage;

import ucm_base.ucm_technicalpolicies.Ucm_technicalpoliciesPackage;

import ucm_basic_projects.Ucm_basic_projectsPackage;

import ucm_environment.ucm_resources.Ucm_resourcesPackage;

import ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl;

import ucm_extended_projects.Ucm_extended_projectsPackage;

import ucm_extended_projects.impl.Ucm_extended_projectsPackageImpl;

import ucm_supplement.ucm_deployments.AllocationPlan;
import ucm_supplement.ucm_deployments.AppAssembly;
import ucm_supplement.ucm_deployments.Artefact;
import ucm_supplement.ucm_deployments.AssemblyPartReference;
import ucm_supplement.ucm_deployments.AttributeValue;
import ucm_supplement.ucm_deployments.ComponentInstance;
import ucm_supplement.ucm_deployments.ComponentInstanceInitialization;
import ucm_supplement.ucm_deployments.ComponentInstancePolicyInitialization;
import ucm_supplement.ucm_deployments.ComponentInstancePortInitialization;
import ucm_supplement.ucm_deployments.ConnectionConfiguration;
import ucm_supplement.ucm_deployments.ConnectionEndConfiguration;
import ucm_supplement.ucm_deployments.ConnectionInstance;
import ucm_supplement.ucm_deployments.DeploymentModule;
import ucm_supplement.ucm_deployments.IComponentNode;
import ucm_supplement.ucm_deployments.IHasAttributeInitialization;
import ucm_supplement.ucm_deployments.TechnicalPolicyConfiguration;
import ucm_supplement.ucm_deployments.Ucm_deploymentsFactory;
import ucm_supplement.ucm_deployments.Ucm_deploymentsPackage;

import ucm_supplement.ucm_deployments.util.Ucm_deploymentsValidator;
import ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage;

import ucm_supplement.ucm_detailed_components.impl.Ucm_detailed_componentsPackageImpl;

import ucm_supplement.ucm_enhanced_configuration.Ucm_enhanced_configurationPackage;

import ucm_supplement.ucm_enhanced_configuration.impl.Ucm_enhanced_configurationPackageImpl;

import ucm_supplement.ucm_enhanced_portspec.Ucm_enhanced_portspecPackage;

import ucm_supplement.ucm_enhanced_portspec.impl.Ucm_enhanced_portspecPackageImpl;

import ucm_supplement.ucm_test_cases.Ucm_test_casesPackage;

import ucm_supplement.ucm_test_cases.impl.Ucm_test_casesPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Ucm_deploymentsPackageImpl extends EPackageImpl implements Ucm_deploymentsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deploymentModuleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass appAssemblyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass allocationPlanEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass componentInstanceInitializationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass componentInstancePortInitializationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass componentInstancePolicyInitializationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assemblyPartReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass componentInstanceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connectionInstanceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connectionConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connectionEndConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass technicalPolicyConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass artefactEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iComponentNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iHasAttributeInitializationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attributeValueEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private Ucm_deploymentsPackageImpl() {
		super(eNS_URI, Ucm_deploymentsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link Ucm_deploymentsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static Ucm_deploymentsPackage init() {
		if (isInited) return (Ucm_deploymentsPackage)EPackage.Registry.INSTANCE.getEPackage(Ucm_deploymentsPackage.eNS_URI);

		// Obtain or create and register package
		Ucm_deploymentsPackageImpl theUcm_deploymentsPackage = (Ucm_deploymentsPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof Ucm_deploymentsPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new Ucm_deploymentsPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		Ucm_basic_projectsPackage.eINSTANCE.eClass();
		Ucm_commonsPackage.eINSTANCE.eClass();
		Ucm_technicalpoliciesPackage.eINSTANCE.eClass();
		Ucm_contractsPackage.eINSTANCE.eClass();
		Ucm_interactionsPackage.eINSTANCE.eClass();
		Ucm_componentsPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Ucm_extended_projectsPackageImpl theUcm_extended_projectsPackage = (Ucm_extended_projectsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_extended_projectsPackage.eNS_URI) instanceof Ucm_extended_projectsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_extended_projectsPackage.eNS_URI) : Ucm_extended_projectsPackage.eINSTANCE);
		Ucm_detailed_componentsPackageImpl theUcm_detailed_componentsPackage = (Ucm_detailed_componentsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_detailed_componentsPackage.eNS_URI) instanceof Ucm_detailed_componentsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_detailed_componentsPackage.eNS_URI) : Ucm_detailed_componentsPackage.eINSTANCE);
		Ucm_enhanced_portspecPackageImpl theUcm_enhanced_portspecPackage = (Ucm_enhanced_portspecPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_enhanced_portspecPackage.eNS_URI) instanceof Ucm_enhanced_portspecPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_enhanced_portspecPackage.eNS_URI) : Ucm_enhanced_portspecPackage.eINSTANCE);
		Ucm_test_casesPackageImpl theUcm_test_casesPackage = (Ucm_test_casesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_test_casesPackage.eNS_URI) instanceof Ucm_test_casesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_test_casesPackage.eNS_URI) : Ucm_test_casesPackage.eINSTANCE);
		Ucm_enhanced_configurationPackageImpl theUcm_enhanced_configurationPackage = (Ucm_enhanced_configurationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_enhanced_configurationPackage.eNS_URI) instanceof Ucm_enhanced_configurationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_enhanced_configurationPackage.eNS_URI) : Ucm_enhanced_configurationPackage.eINSTANCE);
		Ucm_resourcesPackageImpl theUcm_resourcesPackage = (Ucm_resourcesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_resourcesPackage.eNS_URI) instanceof Ucm_resourcesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_resourcesPackage.eNS_URI) : Ucm_resourcesPackage.eINSTANCE);

		// Create package meta-data objects
		theUcm_deploymentsPackage.createPackageContents();
		theUcm_extended_projectsPackage.createPackageContents();
		theUcm_detailed_componentsPackage.createPackageContents();
		theUcm_enhanced_portspecPackage.createPackageContents();
		theUcm_test_casesPackage.createPackageContents();
		theUcm_enhanced_configurationPackage.createPackageContents();
		theUcm_resourcesPackage.createPackageContents();

		// Initialize created meta-data
		theUcm_deploymentsPackage.initializePackageContents();
		theUcm_extended_projectsPackage.initializePackageContents();
		theUcm_detailed_componentsPackage.initializePackageContents();
		theUcm_enhanced_portspecPackage.initializePackageContents();
		theUcm_test_casesPackage.initializePackageContents();
		theUcm_enhanced_configurationPackage.initializePackageContents();
		theUcm_resourcesPackage.initializePackageContents();

		// Register package validator
		EValidator.Registry.INSTANCE.put
			(theUcm_deploymentsPackage, 
			 new EValidator.Descriptor() {
				 public EValidator getEValidator() {
					 return Ucm_deploymentsValidator.INSTANCE;
				 }
			 });

		// Mark meta-data to indicate it can't be changed
		theUcm_deploymentsPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(Ucm_deploymentsPackage.eNS_URI, theUcm_deploymentsPackage);
		return theUcm_deploymentsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDeploymentModule() {
		return deploymentModuleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeploymentModule_Assembly() {
		return (EReference)deploymentModuleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeploymentModule_AllocationPlan() {
		return (EReference)deploymentModuleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeploymentModule_Artefact() {
		return (EReference)deploymentModuleEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeploymentModule_TestCaseGroup() {
		return (EReference)deploymentModuleEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeploymentModule_ComponentInstance() {
		return (EReference)deploymentModuleEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeploymentModule_ConnectionInstance() {
		return (EReference)deploymentModuleEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAppAssembly() {
		return appAssemblyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAllocationPlan() {
		return allocationPlanEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAllocationPlan_Node() {
		return (EReference)allocationPlanEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAllocationPlan_ComponentInstanceInitialization() {
		return (EReference)allocationPlanEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAllocationPlan_ConnectionConfiguration() {
		return (EReference)allocationPlanEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAllocationPlan_TechnicalPolicyConfiguration() {
		return (EReference)allocationPlanEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAllocationPlan_Refines() {
		return (EReference)allocationPlanEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComponentInstanceInitialization() {
		return componentInstanceInitializationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentInstanceInitialization_ComponentInstance() {
		return (EReference)componentInstanceInitializationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentInstanceInitialization_PortInitialization() {
		return (EReference)componentInstanceInitializationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentInstanceInitialization_PolicyInitialization() {
		return (EReference)componentInstanceInitializationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComponentInstancePortInitialization() {
		return componentInstancePortInitializationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentInstancePortInitialization_Port() {
		return (EReference)componentInstancePortInitializationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentInstancePortInitialization_PortElement() {
		return (EReference)componentInstancePortInitializationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComponentInstancePolicyInitialization() {
		return componentInstancePolicyInitializationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentInstancePolicyInitialization_PortElement() {
		return (EReference)componentInstancePolicyInitializationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentInstancePolicyInitialization_Policy() {
		return (EReference)componentInstancePolicyInitializationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssemblyPartReference() {
		return assemblyPartReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssemblyPartReference_ReferenceToSubpart() {
		return (EReference)assemblyPartReferenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssemblyPartReference_Part() {
		return (EReference)assemblyPartReferenceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComponentInstance() {
		return componentInstanceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentInstance_PartReference() {
		return (EReference)componentInstanceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConnectionInstance() {
		return connectionInstanceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionInstance_Connection() {
		return (EReference)connectionInstanceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionInstance_PartReference() {
		return (EReference)connectionInstanceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConnectionConfiguration() {
		return connectionConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionConfiguration_Connection() {
		return (EReference)connectionConfigurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionConfiguration_EndConfiguration() {
		return (EReference)connectionConfigurationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionConfiguration_RefinedIn() {
		return (EReference)connectionConfigurationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConnectionEndConfiguration() {
		return connectionEndConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionEndConfiguration_ComponentInstance() {
		return (EReference)connectionEndConfigurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionEndConfiguration_Port() {
		return (EReference)connectionEndConfigurationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTechnicalPolicyConfiguration() {
		return technicalPolicyConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTechnicalPolicyConfiguration_Policy() {
		return (EReference)technicalPolicyConfigurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTechnicalPolicyConfiguration_ComponentInstance() {
		return (EReference)technicalPolicyConfigurationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTechnicalPolicyConfiguration_RefinedIn() {
		return (EReference)technicalPolicyConfigurationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getArtefact() {
		return artefactEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getArtefact_ComponentImplementation() {
		return (EReference)artefactEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIComponentNode() {
		return iComponentNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIComponentNode_Language() {
		return (EReference)iComponentNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIComponentNode_ComponentInstance() {
		return (EReference)iComponentNodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIHasAttributeInitialization() {
		return iHasAttributeInitializationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIHasAttributeInitialization_AttributeInitialization() {
		return (EReference)iHasAttributeInitializationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttributeValue() {
		return attributeValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttributeValue_Attribute() {
		return (EReference)attributeValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ucm_deploymentsFactory getUcm_deploymentsFactory() {
		return (Ucm_deploymentsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		deploymentModuleEClass = createEClass(DEPLOYMENT_MODULE);
		createEReference(deploymentModuleEClass, DEPLOYMENT_MODULE__ASSEMBLY);
		createEReference(deploymentModuleEClass, DEPLOYMENT_MODULE__ALLOCATION_PLAN);
		createEReference(deploymentModuleEClass, DEPLOYMENT_MODULE__ARTEFACT);
		createEReference(deploymentModuleEClass, DEPLOYMENT_MODULE__TEST_CASE_GROUP);
		createEReference(deploymentModuleEClass, DEPLOYMENT_MODULE__COMPONENT_INSTANCE);
		createEReference(deploymentModuleEClass, DEPLOYMENT_MODULE__CONNECTION_INSTANCE);

		appAssemblyEClass = createEClass(APP_ASSEMBLY);

		allocationPlanEClass = createEClass(ALLOCATION_PLAN);
		createEReference(allocationPlanEClass, ALLOCATION_PLAN__NODE);
		createEReference(allocationPlanEClass, ALLOCATION_PLAN__COMPONENT_INSTANCE_INITIALIZATION);
		createEReference(allocationPlanEClass, ALLOCATION_PLAN__CONNECTION_CONFIGURATION);
		createEReference(allocationPlanEClass, ALLOCATION_PLAN__TECHNICAL_POLICY_CONFIGURATION);
		createEReference(allocationPlanEClass, ALLOCATION_PLAN__REFINES);

		componentInstanceInitializationEClass = createEClass(COMPONENT_INSTANCE_INITIALIZATION);
		createEReference(componentInstanceInitializationEClass, COMPONENT_INSTANCE_INITIALIZATION__COMPONENT_INSTANCE);
		createEReference(componentInstanceInitializationEClass, COMPONENT_INSTANCE_INITIALIZATION__PORT_INITIALIZATION);
		createEReference(componentInstanceInitializationEClass, COMPONENT_INSTANCE_INITIALIZATION__POLICY_INITIALIZATION);

		componentInstancePortInitializationEClass = createEClass(COMPONENT_INSTANCE_PORT_INITIALIZATION);
		createEReference(componentInstancePortInitializationEClass, COMPONENT_INSTANCE_PORT_INITIALIZATION__PORT);
		createEReference(componentInstancePortInitializationEClass, COMPONENT_INSTANCE_PORT_INITIALIZATION__PORT_ELEMENT);

		componentInstancePolicyInitializationEClass = createEClass(COMPONENT_INSTANCE_POLICY_INITIALIZATION);
		createEReference(componentInstancePolicyInitializationEClass, COMPONENT_INSTANCE_POLICY_INITIALIZATION__PORT_ELEMENT);
		createEReference(componentInstancePolicyInitializationEClass, COMPONENT_INSTANCE_POLICY_INITIALIZATION__POLICY);

		assemblyPartReferenceEClass = createEClass(ASSEMBLY_PART_REFERENCE);
		createEReference(assemblyPartReferenceEClass, ASSEMBLY_PART_REFERENCE__REFERENCE_TO_SUBPART);
		createEReference(assemblyPartReferenceEClass, ASSEMBLY_PART_REFERENCE__PART);

		componentInstanceEClass = createEClass(COMPONENT_INSTANCE);
		createEReference(componentInstanceEClass, COMPONENT_INSTANCE__PART_REFERENCE);

		connectionInstanceEClass = createEClass(CONNECTION_INSTANCE);
		createEReference(connectionInstanceEClass, CONNECTION_INSTANCE__CONNECTION);
		createEReference(connectionInstanceEClass, CONNECTION_INSTANCE__PART_REFERENCE);

		connectionConfigurationEClass = createEClass(CONNECTION_CONFIGURATION);
		createEReference(connectionConfigurationEClass, CONNECTION_CONFIGURATION__CONNECTION);
		createEReference(connectionConfigurationEClass, CONNECTION_CONFIGURATION__END_CONFIGURATION);
		createEReference(connectionConfigurationEClass, CONNECTION_CONFIGURATION__REFINED_IN);

		connectionEndConfigurationEClass = createEClass(CONNECTION_END_CONFIGURATION);
		createEReference(connectionEndConfigurationEClass, CONNECTION_END_CONFIGURATION__COMPONENT_INSTANCE);
		createEReference(connectionEndConfigurationEClass, CONNECTION_END_CONFIGURATION__PORT);

		technicalPolicyConfigurationEClass = createEClass(TECHNICAL_POLICY_CONFIGURATION);
		createEReference(technicalPolicyConfigurationEClass, TECHNICAL_POLICY_CONFIGURATION__POLICY);
		createEReference(technicalPolicyConfigurationEClass, TECHNICAL_POLICY_CONFIGURATION__COMPONENT_INSTANCE);
		createEReference(technicalPolicyConfigurationEClass, TECHNICAL_POLICY_CONFIGURATION__REFINED_IN);

		artefactEClass = createEClass(ARTEFACT);
		createEReference(artefactEClass, ARTEFACT__COMPONENT_IMPLEMENTATION);

		iComponentNodeEClass = createEClass(ICOMPONENT_NODE);
		createEReference(iComponentNodeEClass, ICOMPONENT_NODE__LANGUAGE);
		createEReference(iComponentNodeEClass, ICOMPONENT_NODE__COMPONENT_INSTANCE);

		iHasAttributeInitializationEClass = createEClass(IHAS_ATTRIBUTE_INITIALIZATION);
		createEReference(iHasAttributeInitializationEClass, IHAS_ATTRIBUTE_INITIALIZATION__ATTRIBUTE_INITIALIZATION);

		attributeValueEClass = createEClass(ATTRIBUTE_VALUE);
		createEReference(attributeValueEClass, ATTRIBUTE_VALUE__ATTRIBUTE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		Ucm_commonsPackage theUcm_commonsPackage = (Ucm_commonsPackage)EPackage.Registry.INSTANCE.getEPackage(Ucm_commonsPackage.eNS_URI);
		Ucm_test_casesPackage theUcm_test_casesPackage = (Ucm_test_casesPackage)EPackage.Registry.INSTANCE.getEPackage(Ucm_test_casesPackage.eNS_URI);
		Ucm_componentsPackage theUcm_componentsPackage = (Ucm_componentsPackage)EPackage.Registry.INSTANCE.getEPackage(Ucm_componentsPackage.eNS_URI);
		Ucm_contractsPackage theUcm_contractsPackage = (Ucm_contractsPackage)EPackage.Registry.INSTANCE.getEPackage(Ucm_contractsPackage.eNS_URI);
		Ucm_interactionsPackage theUcm_interactionsPackage = (Ucm_interactionsPackage)EPackage.Registry.INSTANCE.getEPackage(Ucm_interactionsPackage.eNS_URI);
		Ucm_technicalpoliciesPackage theUcm_technicalpoliciesPackage = (Ucm_technicalpoliciesPackage)EPackage.Registry.INSTANCE.getEPackage(Ucm_technicalpoliciesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		deploymentModuleEClass.getESuperTypes().add(theUcm_commonsPackage.getIApplicationModule());
		appAssemblyEClass.getESuperTypes().add(theUcm_componentsPackage.getIAssembly());
		allocationPlanEClass.getESuperTypes().add(theUcm_commonsPackage.getIApplicationModule());
		componentInstanceInitializationEClass.getESuperTypes().add(this.getIHasAttributeInitialization());
		componentInstanceInitializationEClass.getESuperTypes().add(theUcm_commonsPackage.getINamed());
		componentInstanceInitializationEClass.getESuperTypes().add(theUcm_contractsPackage.getIAnnotable());
		componentInstancePortInitializationEClass.getESuperTypes().add(this.getIHasAttributeInitialization());
		componentInstancePortInitializationEClass.getESuperTypes().add(theUcm_commonsPackage.getINamed());
		componentInstancePortInitializationEClass.getESuperTypes().add(theUcm_contractsPackage.getIAnnotable());
		componentInstancePolicyInitializationEClass.getESuperTypes().add(this.getIHasAttributeInitialization());
		componentInstancePolicyInitializationEClass.getESuperTypes().add(theUcm_commonsPackage.getINamed());
		assemblyPartReferenceEClass.getESuperTypes().add(theUcm_commonsPackage.getINamed());
		componentInstanceEClass.getESuperTypes().add(theUcm_commonsPackage.getINamed());
		connectionInstanceEClass.getESuperTypes().add(theUcm_commonsPackage.getINamed());
		connectionConfigurationEClass.getESuperTypes().add(theUcm_commonsPackage.getINamed());
		connectionConfigurationEClass.getESuperTypes().add(theUcm_contractsPackage.getIConfigured());
		connectionEndConfigurationEClass.getESuperTypes().add(theUcm_commonsPackage.getINamed());
		connectionEndConfigurationEClass.getESuperTypes().add(theUcm_contractsPackage.getIConfigured());
		technicalPolicyConfigurationEClass.getESuperTypes().add(theUcm_commonsPackage.getINamed());
		technicalPolicyConfigurationEClass.getESuperTypes().add(theUcm_contractsPackage.getIConfigured());
		artefactEClass.getESuperTypes().add(theUcm_commonsPackage.getINamed());
		artefactEClass.getESuperTypes().add(theUcm_contractsPackage.getIAnnotable());
		iComponentNodeEClass.getESuperTypes().add(theUcm_commonsPackage.getINamed());
		iComponentNodeEClass.getESuperTypes().add(theUcm_contractsPackage.getIAnnotable());
		attributeValueEClass.getESuperTypes().add(theUcm_contractsPackage.getIValued());

		// Initialize classes and features; add operations and parameters
		initEClass(deploymentModuleEClass, DeploymentModule.class, "DeploymentModule", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDeploymentModule_Assembly(), this.getAppAssembly(), null, "assembly", null, 1, 1, DeploymentModule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeploymentModule_AllocationPlan(), this.getAllocationPlan(), null, "allocationPlan", null, 0, -1, DeploymentModule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeploymentModule_Artefact(), this.getArtefact(), null, "artefact", null, 0, -1, DeploymentModule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeploymentModule_TestCaseGroup(), theUcm_test_casesPackage.getTestCaseGroup(), null, "testCaseGroup", null, 0, -1, DeploymentModule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeploymentModule_ComponentInstance(), this.getComponentInstance(), null, "componentInstance", null, 0, -1, DeploymentModule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeploymentModule_ConnectionInstance(), this.getConnectionInstance(), null, "connectionInstance", null, 0, -1, DeploymentModule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = addEOperation(deploymentModuleEClass, ecorePackage.getEBoolean(), "instancesBelongToAppAssembly", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
		EGenericType g1 = createEGenericType(ecorePackage.getEMap());
		EGenericType g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(appAssemblyEClass, AppAssembly.class, "AppAssembly", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(allocationPlanEClass, AllocationPlan.class, "AllocationPlan", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAllocationPlan_Node(), this.getIComponentNode(), null, "node", null, 0, -1, AllocationPlan.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAllocationPlan_ComponentInstanceInitialization(), this.getComponentInstanceInitialization(), null, "componentInstanceInitialization", null, 0, -1, AllocationPlan.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAllocationPlan_ConnectionConfiguration(), this.getConnectionConfiguration(), null, "connectionConfiguration", null, 0, -1, AllocationPlan.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAllocationPlan_TechnicalPolicyConfiguration(), this.getTechnicalPolicyConfiguration(), null, "technicalPolicyConfiguration", null, 0, -1, AllocationPlan.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAllocationPlan_Refines(), this.getAllocationPlan(), null, "refines", null, 0, 1, AllocationPlan.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(allocationPlanEClass, ecorePackage.getEBoolean(), "configurationIsComplete", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(allocationPlanEClass, ecorePackage.getEBoolean(), "initializationIsComplete", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(allocationPlanEClass, ecorePackage.getEBoolean(), "instancesBelongToTheSameDeployment", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(componentInstanceInitializationEClass, ComponentInstanceInitialization.class, "ComponentInstanceInitialization", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getComponentInstanceInitialization_ComponentInstance(), this.getComponentInstance(), null, "componentInstance", null, 1, 1, ComponentInstanceInitialization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getComponentInstanceInitialization_PortInitialization(), this.getComponentInstancePortInitialization(), null, "portInitialization", null, 0, -1, ComponentInstanceInitialization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getComponentInstanceInitialization_PolicyInitialization(), this.getComponentInstancePolicyInitialization(), null, "policyInitialization", null, 0, -1, ComponentInstanceInitialization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(componentInstanceInitializationEClass, ecorePackage.getEBoolean(), "attributeBelongsToComponent", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(componentInstanceInitializationEClass, ecorePackage.getEBoolean(), "instanceBelongToAppAssembly", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(componentInstancePortInitializationEClass, ComponentInstancePortInitialization.class, "ComponentInstancePortInitialization", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getComponentInstancePortInitialization_Port(), theUcm_componentsPackage.getPort(), null, "port", null, 1, 1, ComponentInstancePortInitialization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getComponentInstancePortInitialization_PortElement(), theUcm_interactionsPackage.getPortElement(), null, "portElement", null, 1, 1, ComponentInstancePortInitialization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(componentInstancePortInitializationEClass, ecorePackage.getEBoolean(), "attributeBelongsToPort", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(componentInstancePolicyInitializationEClass, ComponentInstancePolicyInitialization.class, "ComponentInstancePolicyInitialization", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getComponentInstancePolicyInitialization_PortElement(), theUcm_interactionsPackage.getPortElement(), null, "portElement", null, 1, 1, ComponentInstancePolicyInitialization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getComponentInstancePolicyInitialization_Policy(), theUcm_componentsPackage.getComponentTechnicalPolicy(), null, "policy", null, 1, 1, ComponentInstancePolicyInitialization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(componentInstancePolicyInitializationEClass, ecorePackage.getEBoolean(), "attributeBelongsToPolicy", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(assemblyPartReferenceEClass, AssemblyPartReference.class, "AssemblyPartReference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAssemblyPartReference_ReferenceToSubpart(), this.getAssemblyPartReference(), null, "referenceToSubpart", null, 0, 1, AssemblyPartReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssemblyPartReference_Part(), theUcm_componentsPackage.getAssemblyPart(), null, "part", null, 1, 1, AssemblyPartReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(assemblyPartReferenceEClass, ecorePackage.getEBoolean(), "subpartBelongsToPart", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(componentInstanceEClass, ComponentInstance.class, "ComponentInstance", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getComponentInstance_PartReference(), this.getAssemblyPartReference(), null, "partReference", null, 1, 1, ComponentInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(connectionInstanceEClass, ConnectionInstance.class, "ConnectionInstance", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConnectionInstance_Connection(), theUcm_componentsPackage.getConnection(), null, "connection", null, 1, 1, ConnectionInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionInstance_PartReference(), this.getAssemblyPartReference(), null, "partReference", null, 0, 1, ConnectionInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(connectionInstanceEClass, ecorePackage.getEBoolean(), "connectionBelongsToPart", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(connectionConfigurationEClass, ConnectionConfiguration.class, "ConnectionConfiguration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConnectionConfiguration_Connection(), this.getConnectionInstance(), null, "connection", null, 1, 1, ConnectionConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionConfiguration_EndConfiguration(), this.getConnectionEndConfiguration(), null, "endConfiguration", null, 0, -1, ConnectionConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionConfiguration_RefinedIn(), theUcm_interactionsPackage.getConnectorDefinition(), null, "refinedIn", null, 0, 1, ConnectionConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(connectionConfigurationEClass, ecorePackage.getEBoolean(), "instanceBelongToAppAssembly", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(connectionEndConfigurationEClass, ConnectionEndConfiguration.class, "ConnectionEndConfiguration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConnectionEndConfiguration_ComponentInstance(), this.getComponentInstance(), null, "componentInstance", null, 1, 1, ConnectionEndConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionEndConfiguration_Port(), theUcm_componentsPackage.getPort(), null, "port", null, 1, 1, ConnectionEndConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(technicalPolicyConfigurationEClass, TechnicalPolicyConfiguration.class, "TechnicalPolicyConfiguration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTechnicalPolicyConfiguration_Policy(), theUcm_componentsPackage.getComponentTechnicalPolicy(), null, "policy", null, 1, 1, TechnicalPolicyConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTechnicalPolicyConfiguration_ComponentInstance(), this.getComponentInstance(), null, "componentInstance", null, 1, -1, TechnicalPolicyConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTechnicalPolicyConfiguration_RefinedIn(), theUcm_technicalpoliciesPackage.getTechnicalPolicyDefinition(), null, "refinedIn", null, 0, 1, TechnicalPolicyConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(technicalPolicyConfigurationEClass, ecorePackage.getEBoolean(), "policyManagesComponent", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(technicalPolicyConfigurationEClass, ecorePackage.getEBoolean(), "instanceBelongToAppAssembly", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(artefactEClass, Artefact.class, "Artefact", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getArtefact_ComponentImplementation(), theUcm_componentsPackage.getAtomicComponentImplementation(), null, "componentImplementation", null, 0, -1, Artefact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(iComponentNodeEClass, IComponentNode.class, "IComponentNode", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIComponentNode_Language(), theUcm_technicalpoliciesPackage.getLanguage(), null, "language", null, 0, 1, IComponentNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIComponentNode_ComponentInstance(), this.getComponentInstance(), null, "componentInstance", null, 0, -1, IComponentNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(iHasAttributeInitializationEClass, IHasAttributeInitialization.class, "IHasAttributeInitialization", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIHasAttributeInitialization_AttributeInitialization(), this.getAttributeValue(), null, "attributeInitialization", null, 0, -1, IHasAttributeInitialization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(attributeValueEClass, AttributeValue.class, "AttributeValue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAttributeValue_Attribute(), theUcm_contractsPackage.getAttribute(), null, "attribute", null, 1, 1, AttributeValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //Ucm_deploymentsPackageImpl
