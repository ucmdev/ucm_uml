/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_deployments.impl;

import java.util.Collection;

import java.util.Map;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.emf.ecore.util.InternalEList;

import ucm_base.ucm_commons.impl.IApplicationModuleImpl;

import ucm_supplement.ucm_deployments.AllocationPlan;
import ucm_supplement.ucm_deployments.AppAssembly;
import ucm_supplement.ucm_deployments.Artefact;
import ucm_supplement.ucm_deployments.ComponentInstance;
import ucm_supplement.ucm_deployments.ConnectionInstance;
import ucm_supplement.ucm_deployments.DeploymentModule;
import ucm_supplement.ucm_deployments.Ucm_deploymentsPackage;

import ucm_supplement.ucm_deployments.util.Ucm_deploymentsValidator;
import ucm_supplement.ucm_test_cases.TestCaseGroup;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Deployment Module</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_deployments.impl.DeploymentModuleImpl#getAssembly <em>Assembly</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.impl.DeploymentModuleImpl#getAllocationPlan <em>Allocation Plan</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.impl.DeploymentModuleImpl#getArtefact <em>Artefact</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.impl.DeploymentModuleImpl#getTestCaseGroup <em>Test Case Group</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.impl.DeploymentModuleImpl#getComponentInstance <em>Component Instance</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.impl.DeploymentModuleImpl#getConnectionInstance <em>Connection Instance</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DeploymentModuleImpl extends IApplicationModuleImpl implements DeploymentModule {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached value of the '{@link #getAssembly() <em>Assembly</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssembly()
	 * @generated
	 * @ordered
	 */
	protected AppAssembly assembly;

	/**
	 * The cached value of the '{@link #getAllocationPlan() <em>Allocation Plan</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAllocationPlan()
	 * @generated
	 * @ordered
	 */
	protected EList<AllocationPlan> allocationPlan;

	/**
	 * The cached value of the '{@link #getArtefact() <em>Artefact</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArtefact()
	 * @generated
	 * @ordered
	 */
	protected EList<Artefact> artefact;

	/**
	 * The cached value of the '{@link #getTestCaseGroup() <em>Test Case Group</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTestCaseGroup()
	 * @generated
	 * @ordered
	 */
	protected EList<TestCaseGroup> testCaseGroup;

	/**
	 * The cached value of the '{@link #getComponentInstance() <em>Component Instance</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponentInstance()
	 * @generated
	 * @ordered
	 */
	protected EList<ComponentInstance> componentInstance;

	/**
	 * The cached value of the '{@link #getConnectionInstance() <em>Connection Instance</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConnectionInstance()
	 * @generated
	 * @ordered
	 */
	protected EList<ConnectionInstance> connectionInstance;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeploymentModuleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_deploymentsPackage.Literals.DEPLOYMENT_MODULE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AppAssembly getAssembly() {
		return assembly;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssembly(AppAssembly newAssembly, NotificationChain msgs) {
		AppAssembly oldAssembly = assembly;
		assembly = newAssembly;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Ucm_deploymentsPackage.DEPLOYMENT_MODULE__ASSEMBLY, oldAssembly, newAssembly);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssembly(AppAssembly newAssembly) {
		if (newAssembly != assembly) {
			NotificationChain msgs = null;
			if (assembly != null)
				msgs = ((InternalEObject)assembly).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - Ucm_deploymentsPackage.DEPLOYMENT_MODULE__ASSEMBLY, null, msgs);
			if (newAssembly != null)
				msgs = ((InternalEObject)newAssembly).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - Ucm_deploymentsPackage.DEPLOYMENT_MODULE__ASSEMBLY, null, msgs);
			msgs = basicSetAssembly(newAssembly, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_deploymentsPackage.DEPLOYMENT_MODULE__ASSEMBLY, newAssembly, newAssembly));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AllocationPlan> getAllocationPlan() {
		if (allocationPlan == null) {
			allocationPlan = new EObjectContainmentEList<AllocationPlan>(AllocationPlan.class, this, Ucm_deploymentsPackage.DEPLOYMENT_MODULE__ALLOCATION_PLAN);
		}
		return allocationPlan;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Artefact> getArtefact() {
		if (artefact == null) {
			artefact = new EObjectContainmentEList<Artefact>(Artefact.class, this, Ucm_deploymentsPackage.DEPLOYMENT_MODULE__ARTEFACT);
		}
		return artefact;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TestCaseGroup> getTestCaseGroup() {
		if (testCaseGroup == null) {
			testCaseGroup = new EObjectContainmentEList<TestCaseGroup>(TestCaseGroup.class, this, Ucm_deploymentsPackage.DEPLOYMENT_MODULE__TEST_CASE_GROUP);
		}
		return testCaseGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComponentInstance> getComponentInstance() {
		if (componentInstance == null) {
			componentInstance = new EObjectContainmentEList<ComponentInstance>(ComponentInstance.class, this, Ucm_deploymentsPackage.DEPLOYMENT_MODULE__COMPONENT_INSTANCE);
		}
		return componentInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ConnectionInstance> getConnectionInstance() {
		if (connectionInstance == null) {
			connectionInstance = new EObjectContainmentEList<ConnectionInstance>(ConnectionInstance.class, this, Ucm_deploymentsPackage.DEPLOYMENT_MODULE__CONNECTION_INSTANCE);
		}
		return connectionInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean instancesBelongToAppAssembly(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 Ucm_deploymentsValidator.DIAGNOSTIC_SOURCE,
						 Ucm_deploymentsValidator.DEPLOYMENT_MODULE__INSTANCES_BELONG_TO_APP_ASSEMBLY,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "instancesBelongToAppAssembly", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Ucm_deploymentsPackage.DEPLOYMENT_MODULE__ASSEMBLY:
				return basicSetAssembly(null, msgs);
			case Ucm_deploymentsPackage.DEPLOYMENT_MODULE__ALLOCATION_PLAN:
				return ((InternalEList<?>)getAllocationPlan()).basicRemove(otherEnd, msgs);
			case Ucm_deploymentsPackage.DEPLOYMENT_MODULE__ARTEFACT:
				return ((InternalEList<?>)getArtefact()).basicRemove(otherEnd, msgs);
			case Ucm_deploymentsPackage.DEPLOYMENT_MODULE__TEST_CASE_GROUP:
				return ((InternalEList<?>)getTestCaseGroup()).basicRemove(otherEnd, msgs);
			case Ucm_deploymentsPackage.DEPLOYMENT_MODULE__COMPONENT_INSTANCE:
				return ((InternalEList<?>)getComponentInstance()).basicRemove(otherEnd, msgs);
			case Ucm_deploymentsPackage.DEPLOYMENT_MODULE__CONNECTION_INSTANCE:
				return ((InternalEList<?>)getConnectionInstance()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_deploymentsPackage.DEPLOYMENT_MODULE__ASSEMBLY:
				return getAssembly();
			case Ucm_deploymentsPackage.DEPLOYMENT_MODULE__ALLOCATION_PLAN:
				return getAllocationPlan();
			case Ucm_deploymentsPackage.DEPLOYMENT_MODULE__ARTEFACT:
				return getArtefact();
			case Ucm_deploymentsPackage.DEPLOYMENT_MODULE__TEST_CASE_GROUP:
				return getTestCaseGroup();
			case Ucm_deploymentsPackage.DEPLOYMENT_MODULE__COMPONENT_INSTANCE:
				return getComponentInstance();
			case Ucm_deploymentsPackage.DEPLOYMENT_MODULE__CONNECTION_INSTANCE:
				return getConnectionInstance();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_deploymentsPackage.DEPLOYMENT_MODULE__ASSEMBLY:
				setAssembly((AppAssembly)newValue);
				return;
			case Ucm_deploymentsPackage.DEPLOYMENT_MODULE__ALLOCATION_PLAN:
				getAllocationPlan().clear();
				getAllocationPlan().addAll((Collection<? extends AllocationPlan>)newValue);
				return;
			case Ucm_deploymentsPackage.DEPLOYMENT_MODULE__ARTEFACT:
				getArtefact().clear();
				getArtefact().addAll((Collection<? extends Artefact>)newValue);
				return;
			case Ucm_deploymentsPackage.DEPLOYMENT_MODULE__TEST_CASE_GROUP:
				getTestCaseGroup().clear();
				getTestCaseGroup().addAll((Collection<? extends TestCaseGroup>)newValue);
				return;
			case Ucm_deploymentsPackage.DEPLOYMENT_MODULE__COMPONENT_INSTANCE:
				getComponentInstance().clear();
				getComponentInstance().addAll((Collection<? extends ComponentInstance>)newValue);
				return;
			case Ucm_deploymentsPackage.DEPLOYMENT_MODULE__CONNECTION_INSTANCE:
				getConnectionInstance().clear();
				getConnectionInstance().addAll((Collection<? extends ConnectionInstance>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_deploymentsPackage.DEPLOYMENT_MODULE__ASSEMBLY:
				setAssembly((AppAssembly)null);
				return;
			case Ucm_deploymentsPackage.DEPLOYMENT_MODULE__ALLOCATION_PLAN:
				getAllocationPlan().clear();
				return;
			case Ucm_deploymentsPackage.DEPLOYMENT_MODULE__ARTEFACT:
				getArtefact().clear();
				return;
			case Ucm_deploymentsPackage.DEPLOYMENT_MODULE__TEST_CASE_GROUP:
				getTestCaseGroup().clear();
				return;
			case Ucm_deploymentsPackage.DEPLOYMENT_MODULE__COMPONENT_INSTANCE:
				getComponentInstance().clear();
				return;
			case Ucm_deploymentsPackage.DEPLOYMENT_MODULE__CONNECTION_INSTANCE:
				getConnectionInstance().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_deploymentsPackage.DEPLOYMENT_MODULE__ASSEMBLY:
				return assembly != null;
			case Ucm_deploymentsPackage.DEPLOYMENT_MODULE__ALLOCATION_PLAN:
				return allocationPlan != null && !allocationPlan.isEmpty();
			case Ucm_deploymentsPackage.DEPLOYMENT_MODULE__ARTEFACT:
				return artefact != null && !artefact.isEmpty();
			case Ucm_deploymentsPackage.DEPLOYMENT_MODULE__TEST_CASE_GROUP:
				return testCaseGroup != null && !testCaseGroup.isEmpty();
			case Ucm_deploymentsPackage.DEPLOYMENT_MODULE__COMPONENT_INSTANCE:
				return componentInstance != null && !componentInstance.isEmpty();
			case Ucm_deploymentsPackage.DEPLOYMENT_MODULE__CONNECTION_INSTANCE:
				return connectionInstance != null && !connectionInstance.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DeploymentModuleImpl
