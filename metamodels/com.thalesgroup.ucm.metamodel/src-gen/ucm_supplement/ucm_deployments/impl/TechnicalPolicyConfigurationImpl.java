/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_deployments.impl;

import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import com.thalesgroup.ucm.metamodel.validation.UcmSupplementValidationFactory;

import ucm_base.ucm_commons.impl.INamedImpl;
import ucm_base.ucm_components.ComponentTechnicalPolicy;
import ucm_base.ucm_contracts.IConfigurationParameterValue;
import ucm_base.ucm_contracts.IConfigured;
import ucm_base.ucm_contracts.Ucm_contractsPackage;
import ucm_base.ucm_technicalpolicies.TechnicalPolicyDefinition;
import ucm_supplement.ucm_deployments.ComponentInstance;
import ucm_supplement.ucm_deployments.TechnicalPolicyConfiguration;
import ucm_supplement.ucm_deployments.Ucm_deploymentsPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>Technical Policy Configuration</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_deployments.impl.TechnicalPolicyConfigurationImpl#getConfigurationValue <em>Configuration Value</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.impl.TechnicalPolicyConfigurationImpl#getPolicy <em>Policy</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.impl.TechnicalPolicyConfigurationImpl#getComponentInstance <em>Component Instance</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.impl.TechnicalPolicyConfigurationImpl#getRefinedIn <em>Refined In</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TechnicalPolicyConfigurationImpl extends INamedImpl implements TechnicalPolicyConfiguration {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached value of the '{@link #getConfigurationValue()
	 * <em>Configuration Value</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getConfigurationValue()
	 * @generated
	 * @ordered
	 */
	protected EList<IConfigurationParameterValue> configurationValue;

	/**
	 * The cached value of the '{@link #getPolicy() <em>Policy</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getPolicy()
	 * @generated
	 * @ordered
	 */
	protected ComponentTechnicalPolicy policy;

	/**
	 * The cached value of the '{@link #getComponentInstance() <em>Component Instance</em>}' reference list.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getComponentInstance()
	 * @generated
	 * @ordered
	 */
	protected EList<ComponentInstance> componentInstance;

	/**
	 * The cached value of the '{@link #getRefinedIn() <em>Refined In</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getRefinedIn()
	 * @generated
	 * @ordered
	 */
	protected TechnicalPolicyDefinition refinedIn;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected TechnicalPolicyConfigurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_deploymentsPackage.Literals.TECHNICAL_POLICY_CONFIGURATION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IConfigurationParameterValue> getConfigurationValue() {
		if (configurationValue == null) {
			configurationValue = new EObjectContainmentEList<IConfigurationParameterValue>(IConfigurationParameterValue.class, this, Ucm_deploymentsPackage.TECHNICAL_POLICY_CONFIGURATION__CONFIGURATION_VALUE);
		}
		return configurationValue;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentTechnicalPolicy getPolicy() {
		if (policy != null && policy.eIsProxy()) {
			InternalEObject oldPolicy = (InternalEObject)policy;
			policy = (ComponentTechnicalPolicy)eResolveProxy(oldPolicy);
			if (policy != oldPolicy) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_deploymentsPackage.TECHNICAL_POLICY_CONFIGURATION__POLICY, oldPolicy, policy));
			}
		}
		return policy;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentTechnicalPolicy basicGetPolicy() {
		return policy;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setPolicy(ComponentTechnicalPolicy newPolicy) {
		ComponentTechnicalPolicy oldPolicy = policy;
		policy = newPolicy;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_deploymentsPackage.TECHNICAL_POLICY_CONFIGURATION__POLICY, oldPolicy, policy));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComponentInstance> getComponentInstance() {
		if (componentInstance == null) {
			componentInstance = new EObjectResolvingEList<ComponentInstance>(ComponentInstance.class, this, Ucm_deploymentsPackage.TECHNICAL_POLICY_CONFIGURATION__COMPONENT_INSTANCE);
		}
		return componentInstance;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public TechnicalPolicyDefinition getRefinedIn() {
		if (refinedIn != null && refinedIn.eIsProxy()) {
			InternalEObject oldRefinedIn = (InternalEObject)refinedIn;
			refinedIn = (TechnicalPolicyDefinition)eResolveProxy(oldRefinedIn);
			if (refinedIn != oldRefinedIn) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_deploymentsPackage.TECHNICAL_POLICY_CONFIGURATION__REFINED_IN, oldRefinedIn, refinedIn));
			}
		}
		return refinedIn;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public TechnicalPolicyDefinition basicGetRefinedIn() {
		return refinedIn;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setRefinedIn(TechnicalPolicyDefinition newRefinedIn) {
		TechnicalPolicyDefinition oldRefinedIn = refinedIn;
		refinedIn = newRefinedIn;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_deploymentsPackage.TECHNICAL_POLICY_CONFIGURATION__REFINED_IN, oldRefinedIn, refinedIn));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean policyManagesComponent(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and
		// message
		// Ensure that you remove @generated or mark it @generated NOT
		return UcmSupplementValidationFactory.INSTANCE.getDeploymentValidator().policyManagesComponent(this,
				diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean instanceBelongToAppAssembly(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and
		// message
		// Ensure that you remove @generated or mark it @generated NOT
		return UcmSupplementValidationFactory.INSTANCE.getDeploymentValidator().instanceBelongToAppAssembly(this,
				diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Ucm_deploymentsPackage.TECHNICAL_POLICY_CONFIGURATION__CONFIGURATION_VALUE:
				return ((InternalEList<?>)getConfigurationValue()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_deploymentsPackage.TECHNICAL_POLICY_CONFIGURATION__CONFIGURATION_VALUE:
				return getConfigurationValue();
			case Ucm_deploymentsPackage.TECHNICAL_POLICY_CONFIGURATION__POLICY:
				if (resolve) return getPolicy();
				return basicGetPolicy();
			case Ucm_deploymentsPackage.TECHNICAL_POLICY_CONFIGURATION__COMPONENT_INSTANCE:
				return getComponentInstance();
			case Ucm_deploymentsPackage.TECHNICAL_POLICY_CONFIGURATION__REFINED_IN:
				if (resolve) return getRefinedIn();
				return basicGetRefinedIn();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_deploymentsPackage.TECHNICAL_POLICY_CONFIGURATION__CONFIGURATION_VALUE:
				getConfigurationValue().clear();
				getConfigurationValue().addAll((Collection<? extends IConfigurationParameterValue>)newValue);
				return;
			case Ucm_deploymentsPackage.TECHNICAL_POLICY_CONFIGURATION__POLICY:
				setPolicy((ComponentTechnicalPolicy)newValue);
				return;
			case Ucm_deploymentsPackage.TECHNICAL_POLICY_CONFIGURATION__COMPONENT_INSTANCE:
				getComponentInstance().clear();
				getComponentInstance().addAll((Collection<? extends ComponentInstance>)newValue);
				return;
			case Ucm_deploymentsPackage.TECHNICAL_POLICY_CONFIGURATION__REFINED_IN:
				setRefinedIn((TechnicalPolicyDefinition)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_deploymentsPackage.TECHNICAL_POLICY_CONFIGURATION__CONFIGURATION_VALUE:
				getConfigurationValue().clear();
				return;
			case Ucm_deploymentsPackage.TECHNICAL_POLICY_CONFIGURATION__POLICY:
				setPolicy((ComponentTechnicalPolicy)null);
				return;
			case Ucm_deploymentsPackage.TECHNICAL_POLICY_CONFIGURATION__COMPONENT_INSTANCE:
				getComponentInstance().clear();
				return;
			case Ucm_deploymentsPackage.TECHNICAL_POLICY_CONFIGURATION__REFINED_IN:
				setRefinedIn((TechnicalPolicyDefinition)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_deploymentsPackage.TECHNICAL_POLICY_CONFIGURATION__CONFIGURATION_VALUE:
				return configurationValue != null && !configurationValue.isEmpty();
			case Ucm_deploymentsPackage.TECHNICAL_POLICY_CONFIGURATION__POLICY:
				return policy != null;
			case Ucm_deploymentsPackage.TECHNICAL_POLICY_CONFIGURATION__COMPONENT_INSTANCE:
				return componentInstance != null && !componentInstance.isEmpty();
			case Ucm_deploymentsPackage.TECHNICAL_POLICY_CONFIGURATION__REFINED_IN:
				return refinedIn != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == IConfigured.class) {
			switch (derivedFeatureID) {
				case Ucm_deploymentsPackage.TECHNICAL_POLICY_CONFIGURATION__CONFIGURATION_VALUE: return Ucm_contractsPackage.ICONFIGURED__CONFIGURATION_VALUE;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == IConfigured.class) {
			switch (baseFeatureID) {
				case Ucm_contractsPackage.ICONFIGURED__CONFIGURATION_VALUE: return Ucm_deploymentsPackage.TECHNICAL_POLICY_CONFIGURATION__CONFIGURATION_VALUE;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} // TechnicalPolicyConfigurationImpl
