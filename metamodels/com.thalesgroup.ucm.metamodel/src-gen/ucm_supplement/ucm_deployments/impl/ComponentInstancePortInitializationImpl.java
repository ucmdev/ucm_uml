/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_deployments.impl;

import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import com.thalesgroup.ucm.metamodel.validation.UcmSupplementValidationFactory;

import ucm_base.ucm_commons.IComment;
import ucm_base.ucm_commons.INamed;
import ucm_base.ucm_commons.Ucm_commonsPackage;
import ucm_base.ucm_components.Port;
import ucm_base.ucm_contracts.Annotation;
import ucm_base.ucm_contracts.IAnnotable;
import ucm_base.ucm_contracts.Ucm_contractsPackage;
import ucm_base.ucm_interactions.PortElement;
import ucm_supplement.ucm_deployments.ComponentInstancePortInitialization;
import ucm_supplement.ucm_deployments.Ucm_deploymentsPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>Component Instance Port Initialization</b></em>'. <!-- end-user-doc
 * -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link ucm_supplement.ucm_deployments.impl.ComponentInstancePortInitializationImpl#getName
 * <em>Name</em>}</li>
 * <li>{@link ucm_supplement.ucm_deployments.impl.ComponentInstancePortInitializationImpl#getComment
 * <em>Comment</em>}</li>
 * <li>{@link ucm_supplement.ucm_deployments.impl.ComponentInstancePortInitializationImpl#getAnnotation
 * <em>Annotation</em>}</li>
 * <li>{@link ucm_supplement.ucm_deployments.impl.ComponentInstancePortInitializationImpl#getPort
 * <em>Port</em>}</li>
 * <li>{@link ucm_supplement.ucm_deployments.impl.ComponentInstancePortInitializationImpl#getPortElement
 * <em>Port Element</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComponentInstancePortInitializationImpl extends IHasAttributeInitializationImpl
		implements ComponentInstancePortInitialization {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getComment() <em>Comment</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getComment()
	 * @generated
	 * @ordered
	 */
	protected EList<IComment> comment;

	/**
	 * The cached value of the '{@link #getAnnotation() <em>Annotation</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getAnnotation()
	 * @generated
	 * @ordered
	 */
	protected EList<Annotation> annotation;

	/**
	 * The cached value of the '{@link #getPort() <em>Port</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getPort()
	 * @generated
	 * @ordered
	 */
	protected Port port;

	/**
	 * The cached value of the '{@link #getPortElement() <em>Port Element</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getPortElement()
	 * @generated
	 * @ordered
	 */
	protected PortElement portElement;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected ComponentInstancePortInitializationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_deploymentsPackage.Literals.COMPONENT_INSTANCE_PORT_INITIALIZATION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IComment> getComment() {
		if (comment == null) {
			comment = new EObjectContainmentEList<IComment>(IComment.class, this, Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION__COMMENT);
		}
		return comment;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Annotation> getAnnotation() {
		if (annotation == null) {
			annotation = new EObjectContainmentEList<Annotation>(Annotation.class, this, Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION__ANNOTATION);
		}
		return annotation;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Port getPort() {
		if (port != null && port.eIsProxy()) {
			InternalEObject oldPort = (InternalEObject)port;
			port = (Port)eResolveProxy(oldPort);
			if (port != oldPort) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION__PORT, oldPort, port));
			}
		}
		return port;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Port basicGetPort() {
		return port;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setPort(Port newPort) {
		Port oldPort = port;
		port = newPort;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION__PORT, oldPort, port));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public PortElement getPortElement() {
		if (portElement != null && portElement.eIsProxy()) {
			InternalEObject oldPortElement = (InternalEObject)portElement;
			portElement = (PortElement)eResolveProxy(oldPortElement);
			if (portElement != oldPortElement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION__PORT_ELEMENT, oldPortElement, portElement));
			}
		}
		return portElement;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public PortElement basicGetPortElement() {
		return portElement;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setPortElement(PortElement newPortElement) {
		PortElement oldPortElement = portElement;
		portElement = newPortElement;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION__PORT_ELEMENT, oldPortElement, portElement));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean attributeBelongsToPort(DiagnosticChain diagnostics, Map<Object, Object> context) {
		return UcmSupplementValidationFactory.INSTANCE.getDeploymentValidator().attributeBelongsToPort(this,
				diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION__COMMENT:
				return ((InternalEList<?>)getComment()).basicRemove(otherEnd, msgs);
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION__ANNOTATION:
				return ((InternalEList<?>)getAnnotation()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION__NAME:
				return getName();
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION__COMMENT:
				return getComment();
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION__ANNOTATION:
				return getAnnotation();
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION__PORT:
				if (resolve) return getPort();
				return basicGetPort();
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION__PORT_ELEMENT:
				if (resolve) return getPortElement();
				return basicGetPortElement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION__NAME:
				setName((String)newValue);
				return;
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION__COMMENT:
				getComment().clear();
				getComment().addAll((Collection<? extends IComment>)newValue);
				return;
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION__ANNOTATION:
				getAnnotation().clear();
				getAnnotation().addAll((Collection<? extends Annotation>)newValue);
				return;
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION__PORT:
				setPort((Port)newValue);
				return;
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION__PORT_ELEMENT:
				setPortElement((PortElement)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION__NAME:
				setName(NAME_EDEFAULT);
				return;
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION__COMMENT:
				getComment().clear();
				return;
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION__ANNOTATION:
				getAnnotation().clear();
				return;
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION__PORT:
				setPort((Port)null);
				return;
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION__PORT_ELEMENT:
				setPortElement((PortElement)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION__COMMENT:
				return comment != null && !comment.isEmpty();
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION__ANNOTATION:
				return annotation != null && !annotation.isEmpty();
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION__PORT:
				return port != null;
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION__PORT_ELEMENT:
				return portElement != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == INamed.class) {
			switch (derivedFeatureID) {
				case Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION__NAME: return Ucm_commonsPackage.INAMED__NAME;
				case Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION__COMMENT: return Ucm_commonsPackage.INAMED__COMMENT;
				default: return -1;
			}
		}
		if (baseClass == IAnnotable.class) {
			switch (derivedFeatureID) {
				case Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION__ANNOTATION: return Ucm_contractsPackage.IANNOTABLE__ANNOTATION;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == INamed.class) {
			switch (baseFeatureID) {
				case Ucm_commonsPackage.INAMED__NAME: return Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION__NAME;
				case Ucm_commonsPackage.INAMED__COMMENT: return Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION__COMMENT;
				default: return -1;
			}
		}
		if (baseClass == IAnnotable.class) {
			switch (baseFeatureID) {
				case Ucm_contractsPackage.IANNOTABLE__ANNOTATION: return Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION__ANNOTATION;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} // ComponentInstancePortInitializationImpl
