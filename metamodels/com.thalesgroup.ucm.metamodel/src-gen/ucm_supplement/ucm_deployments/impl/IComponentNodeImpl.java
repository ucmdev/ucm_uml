/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_deployments.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import ucm_base.ucm_commons.impl.INamedImpl;

import ucm_base.ucm_contracts.Annotation;
import ucm_base.ucm_contracts.IAnnotable;
import ucm_base.ucm_contracts.Ucm_contractsPackage;

import ucm_base.ucm_technicalpolicies.Language;
import ucm_supplement.ucm_deployments.ComponentInstance;
import ucm_supplement.ucm_deployments.IComponentNode;
import ucm_supplement.ucm_deployments.Ucm_deploymentsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>IComponent Node</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_deployments.impl.IComponentNodeImpl#getAnnotation <em>Annotation</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.impl.IComponentNodeImpl#getLanguage <em>Language</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.impl.IComponentNodeImpl#getComponentInstance <em>Component Instance</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class IComponentNodeImpl extends INamedImpl implements IComponentNode {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached value of the '{@link #getAnnotation() <em>Annotation</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotation()
	 * @generated
	 * @ordered
	 */
	protected EList<Annotation> annotation;

	/**
	 * The cached value of the '{@link #getLanguage() <em>Language</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLanguage()
	 * @generated
	 * @ordered
	 */
	protected Language language;

	/**
	 * The cached value of the '{@link #getComponentInstance() <em>Component Instance</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponentInstance()
	 * @generated
	 * @ordered
	 */
	protected EList<ComponentInstance> componentInstance;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IComponentNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_deploymentsPackage.Literals.ICOMPONENT_NODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Annotation> getAnnotation() {
		if (annotation == null) {
			annotation = new EObjectContainmentEList<Annotation>(Annotation.class, this, Ucm_deploymentsPackage.ICOMPONENT_NODE__ANNOTATION);
		}
		return annotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Language getLanguage() {
		if (language != null && language.eIsProxy()) {
			InternalEObject oldLanguage = (InternalEObject)language;
			language = (Language)eResolveProxy(oldLanguage);
			if (language != oldLanguage) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_deploymentsPackage.ICOMPONENT_NODE__LANGUAGE, oldLanguage, language));
			}
		}
		return language;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Language basicGetLanguage() {
		return language;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLanguage(Language newLanguage) {
		Language oldLanguage = language;
		language = newLanguage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_deploymentsPackage.ICOMPONENT_NODE__LANGUAGE, oldLanguage, language));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComponentInstance> getComponentInstance() {
		if (componentInstance == null) {
			componentInstance = new EObjectResolvingEList<ComponentInstance>(ComponentInstance.class, this, Ucm_deploymentsPackage.ICOMPONENT_NODE__COMPONENT_INSTANCE);
		}
		return componentInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Ucm_deploymentsPackage.ICOMPONENT_NODE__ANNOTATION:
				return ((InternalEList<?>)getAnnotation()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_deploymentsPackage.ICOMPONENT_NODE__ANNOTATION:
				return getAnnotation();
			case Ucm_deploymentsPackage.ICOMPONENT_NODE__LANGUAGE:
				if (resolve) return getLanguage();
				return basicGetLanguage();
			case Ucm_deploymentsPackage.ICOMPONENT_NODE__COMPONENT_INSTANCE:
				return getComponentInstance();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_deploymentsPackage.ICOMPONENT_NODE__ANNOTATION:
				getAnnotation().clear();
				getAnnotation().addAll((Collection<? extends Annotation>)newValue);
				return;
			case Ucm_deploymentsPackage.ICOMPONENT_NODE__LANGUAGE:
				setLanguage((Language)newValue);
				return;
			case Ucm_deploymentsPackage.ICOMPONENT_NODE__COMPONENT_INSTANCE:
				getComponentInstance().clear();
				getComponentInstance().addAll((Collection<? extends ComponentInstance>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_deploymentsPackage.ICOMPONENT_NODE__ANNOTATION:
				getAnnotation().clear();
				return;
			case Ucm_deploymentsPackage.ICOMPONENT_NODE__LANGUAGE:
				setLanguage((Language)null);
				return;
			case Ucm_deploymentsPackage.ICOMPONENT_NODE__COMPONENT_INSTANCE:
				getComponentInstance().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_deploymentsPackage.ICOMPONENT_NODE__ANNOTATION:
				return annotation != null && !annotation.isEmpty();
			case Ucm_deploymentsPackage.ICOMPONENT_NODE__LANGUAGE:
				return language != null;
			case Ucm_deploymentsPackage.ICOMPONENT_NODE__COMPONENT_INSTANCE:
				return componentInstance != null && !componentInstance.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == IAnnotable.class) {
			switch (derivedFeatureID) {
				case Ucm_deploymentsPackage.ICOMPONENT_NODE__ANNOTATION: return Ucm_contractsPackage.IANNOTABLE__ANNOTATION;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == IAnnotable.class) {
			switch (baseFeatureID) {
				case Ucm_contractsPackage.IANNOTABLE__ANNOTATION: return Ucm_deploymentsPackage.ICOMPONENT_NODE__ANNOTATION;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //IComponentNodeImpl
