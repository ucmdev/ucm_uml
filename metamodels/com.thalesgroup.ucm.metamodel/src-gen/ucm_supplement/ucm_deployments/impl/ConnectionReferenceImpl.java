/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_deployments.impl;

import java.util.Map;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.emf.ecore.util.EObjectValidator;
import ucm_base.ucm_commons.impl.INamedImpl;

import ucm_base.ucm_components.Connection;

import ucm_supplement.ucm_deployments.AssemblyPartReference;
import ucm_supplement.ucm_deployments.ConnectionReference;
import ucm_supplement.ucm_deployments.Ucm_deploymentsPackage;
import ucm_supplement.ucm_deployments.util.Ucm_deploymentsValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Connection Reference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_deployments.impl.ConnectionReferenceImpl#getNestingComponentInstance <em>Nesting Component Instance</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.impl.ConnectionReferenceImpl#getConnection <em>Connection</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConnectionReferenceImpl extends INamedImpl implements ConnectionReference {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached value of the '{@link #getNestingComponentInstance() <em>Nesting Component Instance</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNestingComponentInstance()
	 * @generated
	 * @ordered
	 */
	protected AssemblyPartReference nestingComponentInstance;

	/**
	 * The cached value of the '{@link #getConnection() <em>Connection</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConnection()
	 * @generated
	 * @ordered
	 */
	protected Connection connection;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConnectionReferenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_deploymentsPackage.Literals.CONNECTION_REFERENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyPartReference getNestingComponentInstance() {
		return nestingComponentInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNestingComponentInstance(AssemblyPartReference newNestingComponentInstance, NotificationChain msgs) {
		AssemblyPartReference oldNestingComponentInstance = nestingComponentInstance;
		nestingComponentInstance = newNestingComponentInstance;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Ucm_deploymentsPackage.CONNECTION_REFERENCE__NESTING_COMPONENT_INSTANCE, oldNestingComponentInstance, newNestingComponentInstance);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNestingComponentInstance(AssemblyPartReference newNestingComponentInstance) {
		if (newNestingComponentInstance != nestingComponentInstance) {
			NotificationChain msgs = null;
			if (nestingComponentInstance != null)
				msgs = ((InternalEObject)nestingComponentInstance).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - Ucm_deploymentsPackage.CONNECTION_REFERENCE__NESTING_COMPONENT_INSTANCE, null, msgs);
			if (newNestingComponentInstance != null)
				msgs = ((InternalEObject)newNestingComponentInstance).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - Ucm_deploymentsPackage.CONNECTION_REFERENCE__NESTING_COMPONENT_INSTANCE, null, msgs);
			msgs = basicSetNestingComponentInstance(newNestingComponentInstance, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_deploymentsPackage.CONNECTION_REFERENCE__NESTING_COMPONENT_INSTANCE, newNestingComponentInstance, newNestingComponentInstance));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Connection getConnection() {
		if (connection != null && connection.eIsProxy()) {
			InternalEObject oldConnection = (InternalEObject)connection;
			connection = (Connection)eResolveProxy(oldConnection);
			if (connection != oldConnection) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_deploymentsPackage.CONNECTION_REFERENCE__CONNECTION, oldConnection, connection));
			}
		}
		return connection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Connection basicGetConnection() {
		return connection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConnection(Connection newConnection) {
		Connection oldConnection = connection;
		connection = newConnection;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_deploymentsPackage.CONNECTION_REFERENCE__CONNECTION, oldConnection, connection));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean connectionBelongsToPart(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 Ucm_deploymentsValidator.DIAGNOSTIC_SOURCE,
						 Ucm_deploymentsValidator.CONNECTION_REFERENCE__CONNECTION_BELONGS_TO_PART,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "connectionBelongsToPart", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Ucm_deploymentsPackage.CONNECTION_REFERENCE__NESTING_COMPONENT_INSTANCE:
				return basicSetNestingComponentInstance(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_deploymentsPackage.CONNECTION_REFERENCE__NESTING_COMPONENT_INSTANCE:
				return getNestingComponentInstance();
			case Ucm_deploymentsPackage.CONNECTION_REFERENCE__CONNECTION:
				if (resolve) return getConnection();
				return basicGetConnection();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_deploymentsPackage.CONNECTION_REFERENCE__NESTING_COMPONENT_INSTANCE:
				setNestingComponentInstance((AssemblyPartReference)newValue);
				return;
			case Ucm_deploymentsPackage.CONNECTION_REFERENCE__CONNECTION:
				setConnection((Connection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_deploymentsPackage.CONNECTION_REFERENCE__NESTING_COMPONENT_INSTANCE:
				setNestingComponentInstance((AssemblyPartReference)null);
				return;
			case Ucm_deploymentsPackage.CONNECTION_REFERENCE__CONNECTION:
				setConnection((Connection)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_deploymentsPackage.CONNECTION_REFERENCE__NESTING_COMPONENT_INSTANCE:
				return nestingComponentInstance != null;
			case Ucm_deploymentsPackage.CONNECTION_REFERENCE__CONNECTION:
				return connection != null;
		}
		return super.eIsSet(featureID);
	}

} //ConnectionReferenceImpl
