/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_deployments.impl;

import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import com.thalesgroup.ucm.metamodel.validation.UcmSupplementValidationFactory;

import ucm_base.ucm_commons.IComment;
import ucm_base.ucm_commons.INamed;
import ucm_base.ucm_commons.Ucm_commonsPackage;
import ucm_base.ucm_contracts.Annotation;
import ucm_base.ucm_contracts.IAnnotable;
import ucm_base.ucm_contracts.Ucm_contractsPackage;
import ucm_supplement.ucm_deployments.ComponentInstance;
import ucm_supplement.ucm_deployments.ComponentInstanceInitialization;
import ucm_supplement.ucm_deployments.ComponentInstancePolicyInitialization;
import ucm_supplement.ucm_deployments.ComponentInstancePortInitialization;
import ucm_supplement.ucm_deployments.Ucm_deploymentsPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>Component Instance Initialization</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_deployments.impl.ComponentInstanceInitializationImpl#getName <em>Name</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.impl.ComponentInstanceInitializationImpl#getComment <em>Comment</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.impl.ComponentInstanceInitializationImpl#getAnnotation <em>Annotation</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.impl.ComponentInstanceInitializationImpl#getComponentInstance <em>Component Instance</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.impl.ComponentInstanceInitializationImpl#getPortInitialization <em>Port Initialization</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.impl.ComponentInstanceInitializationImpl#getPolicyInitialization <em>Policy Initialization</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComponentInstanceInitializationImpl extends IHasAttributeInitializationImpl
		implements ComponentInstanceInitialization {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getComment() <em>Comment</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getComment()
	 * @generated
	 * @ordered
	 */
	protected EList<IComment> comment;

	/**
	 * The cached value of the '{@link #getAnnotation() <em>Annotation</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getAnnotation()
	 * @generated
	 * @ordered
	 */
	protected EList<Annotation> annotation;

	/**
	 * The cached value of the '{@link #getComponentInstance() <em>Component Instance</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getComponentInstance()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance componentInstance;

	/**
	 * The cached value of the '{@link #getPortInitialization() <em>Port Initialization</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPortInitialization()
	 * @generated
	 * @ordered
	 */
	protected EList<ComponentInstancePortInitialization> portInitialization;

	/**
	 * The cached value of the '{@link #getPolicyInitialization() <em>Policy Initialization</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPolicyInitialization()
	 * @generated
	 * @ordered
	 */
	protected EList<ComponentInstancePolicyInitialization> policyInitialization;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected ComponentInstanceInitializationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_deploymentsPackage.Literals.COMPONENT_INSTANCE_INITIALIZATION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IComment> getComment() {
		if (comment == null) {
			comment = new EObjectContainmentEList<IComment>(IComment.class, this, Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__COMMENT);
		}
		return comment;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Annotation> getAnnotation() {
		if (annotation == null) {
			annotation = new EObjectContainmentEList<Annotation>(Annotation.class, this, Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__ANNOTATION);
		}
		return annotation;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance getComponentInstance() {
		if (componentInstance != null && componentInstance.eIsProxy()) {
			InternalEObject oldComponentInstance = (InternalEObject)componentInstance;
			componentInstance = (ComponentInstance)eResolveProxy(oldComponentInstance);
			if (componentInstance != oldComponentInstance) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__COMPONENT_INSTANCE, oldComponentInstance, componentInstance));
			}
		}
		return componentInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance basicGetComponentInstance() {
		return componentInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComponentInstance(ComponentInstance newComponentInstance) {
		ComponentInstance oldComponentInstance = componentInstance;
		componentInstance = newComponentInstance;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__COMPONENT_INSTANCE, oldComponentInstance, componentInstance));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComponentInstancePortInitialization> getPortInitialization() {
		if (portInitialization == null) {
			portInitialization = new EObjectContainmentEList<ComponentInstancePortInitialization>(ComponentInstancePortInitialization.class, this, Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__PORT_INITIALIZATION);
		}
		return portInitialization;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComponentInstancePolicyInitialization> getPolicyInitialization() {
		if (policyInitialization == null) {
			policyInitialization = new EObjectContainmentEList<ComponentInstancePolicyInitialization>(ComponentInstancePolicyInitialization.class, this, Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__POLICY_INITIALIZATION);
		}
		return policyInitialization;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean attributeBelongsToComponent(DiagnosticChain diagnostics, Map<Object, Object> context) {
		return UcmSupplementValidationFactory.INSTANCE.getDeploymentValidator().attributeBelongsToComponent(this,
				diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean instanceBelongToAppAssembly(DiagnosticChain diagnostics, Map<Object, Object> context) {
		return UcmSupplementValidationFactory.INSTANCE.getDeploymentValidator().instanceBelongToAppAssembly(this,
				diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__COMMENT:
				return ((InternalEList<?>)getComment()).basicRemove(otherEnd, msgs);
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__ANNOTATION:
				return ((InternalEList<?>)getAnnotation()).basicRemove(otherEnd, msgs);
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__PORT_INITIALIZATION:
				return ((InternalEList<?>)getPortInitialization()).basicRemove(otherEnd, msgs);
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__POLICY_INITIALIZATION:
				return ((InternalEList<?>)getPolicyInitialization()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__NAME:
				return getName();
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__COMMENT:
				return getComment();
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__ANNOTATION:
				return getAnnotation();
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__COMPONENT_INSTANCE:
				if (resolve) return getComponentInstance();
				return basicGetComponentInstance();
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__PORT_INITIALIZATION:
				return getPortInitialization();
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__POLICY_INITIALIZATION:
				return getPolicyInitialization();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__NAME:
				setName((String)newValue);
				return;
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__COMMENT:
				getComment().clear();
				getComment().addAll((Collection<? extends IComment>)newValue);
				return;
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__ANNOTATION:
				getAnnotation().clear();
				getAnnotation().addAll((Collection<? extends Annotation>)newValue);
				return;
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__COMPONENT_INSTANCE:
				setComponentInstance((ComponentInstance)newValue);
				return;
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__PORT_INITIALIZATION:
				getPortInitialization().clear();
				getPortInitialization().addAll((Collection<? extends ComponentInstancePortInitialization>)newValue);
				return;
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__POLICY_INITIALIZATION:
				getPolicyInitialization().clear();
				getPolicyInitialization().addAll((Collection<? extends ComponentInstancePolicyInitialization>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__NAME:
				setName(NAME_EDEFAULT);
				return;
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__COMMENT:
				getComment().clear();
				return;
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__ANNOTATION:
				getAnnotation().clear();
				return;
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__COMPONENT_INSTANCE:
				setComponentInstance((ComponentInstance)null);
				return;
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__PORT_INITIALIZATION:
				getPortInitialization().clear();
				return;
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__POLICY_INITIALIZATION:
				getPolicyInitialization().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__COMMENT:
				return comment != null && !comment.isEmpty();
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__ANNOTATION:
				return annotation != null && !annotation.isEmpty();
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__COMPONENT_INSTANCE:
				return componentInstance != null;
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__PORT_INITIALIZATION:
				return portInitialization != null && !portInitialization.isEmpty();
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__POLICY_INITIALIZATION:
				return policyInitialization != null && !policyInitialization.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == INamed.class) {
			switch (derivedFeatureID) {
				case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__NAME: return Ucm_commonsPackage.INAMED__NAME;
				case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__COMMENT: return Ucm_commonsPackage.INAMED__COMMENT;
				default: return -1;
			}
		}
		if (baseClass == IAnnotable.class) {
			switch (derivedFeatureID) {
				case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__ANNOTATION: return Ucm_contractsPackage.IANNOTABLE__ANNOTATION;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == INamed.class) {
			switch (baseFeatureID) {
				case Ucm_commonsPackage.INAMED__NAME: return Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__NAME;
				case Ucm_commonsPackage.INAMED__COMMENT: return Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__COMMENT;
				default: return -1;
			}
		}
		if (baseClass == IAnnotable.class) {
			switch (baseFeatureID) {
				case Ucm_contractsPackage.IANNOTABLE__ANNOTATION: return Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION__ANNOTATION;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} // ComponentInstanceInitializationImpl
