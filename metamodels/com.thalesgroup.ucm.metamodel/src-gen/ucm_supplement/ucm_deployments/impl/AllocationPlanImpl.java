/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_deployments.impl;

import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.emf.ecore.util.InternalEList;

import com.thalesgroup.ucm.metamodel.validation.UcmSupplementValidationFactory;

import ucm_base.ucm_commons.impl.IApplicationModuleImpl;
import ucm_supplement.ucm_deployments.AllocationPlan;
import ucm_supplement.ucm_deployments.ComponentInstanceInitialization;
import ucm_supplement.ucm_deployments.ConnectionConfiguration;
import ucm_supplement.ucm_deployments.IComponentNode;
import ucm_supplement.ucm_deployments.TechnicalPolicyConfiguration;
import ucm_supplement.ucm_deployments.Ucm_deploymentsPackage;
import ucm_supplement.ucm_deployments.util.Ucm_deploymentsValidator;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>Allocation Plan</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_deployments.impl.AllocationPlanImpl#getNode <em>Node</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.impl.AllocationPlanImpl#getComponentInstanceInitialization <em>Component Instance Initialization</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.impl.AllocationPlanImpl#getConnectionConfiguration <em>Connection Configuration</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.impl.AllocationPlanImpl#getTechnicalPolicyConfiguration <em>Technical Policy Configuration</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.impl.AllocationPlanImpl#getRefines <em>Refines</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AllocationPlanImpl extends IApplicationModuleImpl implements AllocationPlan {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached value of the '{@link #getNode() <em>Node</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getNode()
	 * @generated
	 * @ordered
	 */
	protected EList<IComponentNode> node;

	/**
	 * The cached value of the '{@link #getComponentInstanceInitialization() <em>Component Instance Initialization</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getComponentInstanceInitialization()
	 * @generated
	 * @ordered
	 */
	protected EList<ComponentInstanceInitialization> componentInstanceInitialization;

	/**
	 * The cached value of the '{@link #getConnectionConfiguration()
	 * <em>Connection Configuration</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getConnectionConfiguration()
	 * @generated
	 * @ordered
	 */
	protected EList<ConnectionConfiguration> connectionConfiguration;

	/**
	 * The cached value of the '{@link #getTechnicalPolicyConfiguration() <em>Technical Policy Configuration</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getTechnicalPolicyConfiguration()
	 * @generated
	 * @ordered
	 */
	protected EList<TechnicalPolicyConfiguration> technicalPolicyConfiguration;

	/**
	 * The cached value of the '{@link #getRefines() <em>Refines</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getRefines()
	 * @generated
	 * @ordered
	 */
	protected AllocationPlan refines;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected AllocationPlanImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_deploymentsPackage.Literals.ALLOCATION_PLAN;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IComponentNode> getNode() {
		if (node == null) {
			node = new EObjectContainmentEList<IComponentNode>(IComponentNode.class, this, Ucm_deploymentsPackage.ALLOCATION_PLAN__NODE);
		}
		return node;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComponentInstanceInitialization> getComponentInstanceInitialization() {
		if (componentInstanceInitialization == null) {
			componentInstanceInitialization = new EObjectContainmentEList<ComponentInstanceInitialization>(ComponentInstanceInitialization.class, this, Ucm_deploymentsPackage.ALLOCATION_PLAN__COMPONENT_INSTANCE_INITIALIZATION);
		}
		return componentInstanceInitialization;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ConnectionConfiguration> getConnectionConfiguration() {
		if (connectionConfiguration == null) {
			connectionConfiguration = new EObjectContainmentEList<ConnectionConfiguration>(ConnectionConfiguration.class, this, Ucm_deploymentsPackage.ALLOCATION_PLAN__CONNECTION_CONFIGURATION);
		}
		return connectionConfiguration;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TechnicalPolicyConfiguration> getTechnicalPolicyConfiguration() {
		if (technicalPolicyConfiguration == null) {
			technicalPolicyConfiguration = new EObjectContainmentEList<TechnicalPolicyConfiguration>(TechnicalPolicyConfiguration.class, this, Ucm_deploymentsPackage.ALLOCATION_PLAN__TECHNICAL_POLICY_CONFIGURATION);
		}
		return technicalPolicyConfiguration;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public AllocationPlan getRefines() {
		if (refines != null && refines.eIsProxy()) {
			InternalEObject oldRefines = (InternalEObject)refines;
			refines = (AllocationPlan)eResolveProxy(oldRefines);
			if (refines != oldRefines) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_deploymentsPackage.ALLOCATION_PLAN__REFINES, oldRefines, refines));
			}
		}
		return refines;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public AllocationPlan basicGetRefines() {
		return refines;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setRefines(AllocationPlan newRefines) {
		AllocationPlan oldRefines = refines;
		refines = newRefines;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_deploymentsPackage.ALLOCATION_PLAN__REFINES, oldRefines, refines));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean configurationIsComplete(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 Ucm_deploymentsValidator.DIAGNOSTIC_SOURCE,
						 Ucm_deploymentsValidator.ALLOCATION_PLAN__CONFIGURATION_IS_COMPLETE,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "configurationIsComplete", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean initializationIsComplete(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 Ucm_deploymentsValidator.DIAGNOSTIC_SOURCE,
						 Ucm_deploymentsValidator.ALLOCATION_PLAN__INITIALIZATION_IS_COMPLETE,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "initializationIsComplete", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean instancesBelongToTheSameDeployment(DiagnosticChain diagnostics, Map<Object, Object> context) {
		return UcmSupplementValidationFactory.INSTANCE.getDeploymentValidator().instancesBelongToTheSameDeployment(this,
				diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Ucm_deploymentsPackage.ALLOCATION_PLAN__NODE:
				return ((InternalEList<?>)getNode()).basicRemove(otherEnd, msgs);
			case Ucm_deploymentsPackage.ALLOCATION_PLAN__COMPONENT_INSTANCE_INITIALIZATION:
				return ((InternalEList<?>)getComponentInstanceInitialization()).basicRemove(otherEnd, msgs);
			case Ucm_deploymentsPackage.ALLOCATION_PLAN__CONNECTION_CONFIGURATION:
				return ((InternalEList<?>)getConnectionConfiguration()).basicRemove(otherEnd, msgs);
			case Ucm_deploymentsPackage.ALLOCATION_PLAN__TECHNICAL_POLICY_CONFIGURATION:
				return ((InternalEList<?>)getTechnicalPolicyConfiguration()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_deploymentsPackage.ALLOCATION_PLAN__NODE:
				return getNode();
			case Ucm_deploymentsPackage.ALLOCATION_PLAN__COMPONENT_INSTANCE_INITIALIZATION:
				return getComponentInstanceInitialization();
			case Ucm_deploymentsPackage.ALLOCATION_PLAN__CONNECTION_CONFIGURATION:
				return getConnectionConfiguration();
			case Ucm_deploymentsPackage.ALLOCATION_PLAN__TECHNICAL_POLICY_CONFIGURATION:
				return getTechnicalPolicyConfiguration();
			case Ucm_deploymentsPackage.ALLOCATION_PLAN__REFINES:
				if (resolve) return getRefines();
				return basicGetRefines();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_deploymentsPackage.ALLOCATION_PLAN__NODE:
				getNode().clear();
				getNode().addAll((Collection<? extends IComponentNode>)newValue);
				return;
			case Ucm_deploymentsPackage.ALLOCATION_PLAN__COMPONENT_INSTANCE_INITIALIZATION:
				getComponentInstanceInitialization().clear();
				getComponentInstanceInitialization().addAll((Collection<? extends ComponentInstanceInitialization>)newValue);
				return;
			case Ucm_deploymentsPackage.ALLOCATION_PLAN__CONNECTION_CONFIGURATION:
				getConnectionConfiguration().clear();
				getConnectionConfiguration().addAll((Collection<? extends ConnectionConfiguration>)newValue);
				return;
			case Ucm_deploymentsPackage.ALLOCATION_PLAN__TECHNICAL_POLICY_CONFIGURATION:
				getTechnicalPolicyConfiguration().clear();
				getTechnicalPolicyConfiguration().addAll((Collection<? extends TechnicalPolicyConfiguration>)newValue);
				return;
			case Ucm_deploymentsPackage.ALLOCATION_PLAN__REFINES:
				setRefines((AllocationPlan)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_deploymentsPackage.ALLOCATION_PLAN__NODE:
				getNode().clear();
				return;
			case Ucm_deploymentsPackage.ALLOCATION_PLAN__COMPONENT_INSTANCE_INITIALIZATION:
				getComponentInstanceInitialization().clear();
				return;
			case Ucm_deploymentsPackage.ALLOCATION_PLAN__CONNECTION_CONFIGURATION:
				getConnectionConfiguration().clear();
				return;
			case Ucm_deploymentsPackage.ALLOCATION_PLAN__TECHNICAL_POLICY_CONFIGURATION:
				getTechnicalPolicyConfiguration().clear();
				return;
			case Ucm_deploymentsPackage.ALLOCATION_PLAN__REFINES:
				setRefines((AllocationPlan)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_deploymentsPackage.ALLOCATION_PLAN__NODE:
				return node != null && !node.isEmpty();
			case Ucm_deploymentsPackage.ALLOCATION_PLAN__COMPONENT_INSTANCE_INITIALIZATION:
				return componentInstanceInitialization != null && !componentInstanceInitialization.isEmpty();
			case Ucm_deploymentsPackage.ALLOCATION_PLAN__CONNECTION_CONFIGURATION:
				return connectionConfiguration != null && !connectionConfiguration.isEmpty();
			case Ucm_deploymentsPackage.ALLOCATION_PLAN__TECHNICAL_POLICY_CONFIGURATION:
				return technicalPolicyConfiguration != null && !technicalPolicyConfiguration.isEmpty();
			case Ucm_deploymentsPackage.ALLOCATION_PLAN__REFINES:
				return refines != null;
		}
		return super.eIsSet(featureID);
	}

} // AllocationPlanImpl
