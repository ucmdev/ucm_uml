/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_deployments;

import org.eclipse.emf.common.util.EList;

import ucm_base.ucm_commons.INamed;

import ucm_base.ucm_components.AtomicComponentImplementation;

import ucm_base.ucm_contracts.IAnnotable;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Artefact</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_deployments.Artefact#getComponentImplementation <em>Component Implementation</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getArtefact()
 * @model
 * @generated
 */
public interface Artefact extends INamed, IAnnotable {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Component Implementation</b></em>' reference list.
	 * The list contents are of type {@link ucm_base.ucm_components.AtomicComponentImplementation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Implementation</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Implementation</em>' reference list.
	 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getArtefact_ComponentImplementation()
	 * @model
	 * @generated
	 */
	EList<AtomicComponentImplementation> getComponentImplementation();

} // Artefact
