/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_deployments;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IHas Attribute Initialization</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_deployments.IHasAttributeInitialization#getAttributeInitialization <em>Attribute Initialization</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getIHasAttributeInitialization()
 * @model abstract="true"
 * @generated
 */
public interface IHasAttributeInitialization extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Attribute Initialization</b></em>' containment reference list.
	 * The list contents are of type {@link ucm_supplement.ucm_deployments.AttributeValue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attribute Initialization</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute Initialization</em>' containment reference list.
	 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getIHasAttributeInitialization_AttributeInitialization()
	 * @model containment="true"
	 * @generated
	 */
	EList<AttributeValue> getAttributeInitialization();

} // IHasAttributeInitialization
