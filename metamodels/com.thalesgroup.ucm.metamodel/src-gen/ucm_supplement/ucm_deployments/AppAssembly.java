/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_deployments;

import ucm_base.ucm_components.IAssembly;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>App Assembly</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getAppAssembly()
 * @model
 * @generated
 */
public interface AppAssembly extends IAssembly {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

} // AppAssembly
