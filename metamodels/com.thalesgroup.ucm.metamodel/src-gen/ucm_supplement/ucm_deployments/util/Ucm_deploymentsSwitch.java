/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_deployments.util;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import ucm_base.ucm_commons.IApplicationModule;
import ucm_base.ucm_commons.IModule;
import ucm_base.ucm_commons.INamed;

import ucm_base.ucm_components.IAssembly;

import ucm_base.ucm_contracts.IAnnotable;
import ucm_base.ucm_contracts.IConfigured;
import ucm_base.ucm_contracts.IValued;

import ucm_supplement.ucm_deployments.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage
 * @generated
 */
public class Ucm_deploymentsSwitch<T> {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static Ucm_deploymentsPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ucm_deploymentsSwitch() {
		if (modelPackage == null) {
			modelPackage = Ucm_deploymentsPackage.eINSTANCE;
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	public T doSwitch(EObject theEObject) {
		return doSwitch(theEObject.eClass(), theEObject);
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(EClass theEClass, EObject theEObject) {
		if (theEClass.eContainer() == modelPackage) {
			return doSwitch(theEClass.getClassifierID(), theEObject);
		}
		else {
			List<EClass> eSuperTypes = theEClass.getESuperTypes();
			return
				eSuperTypes.isEmpty() ?
					defaultCase(theEObject) :
					doSwitch(eSuperTypes.get(0), theEObject);
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case Ucm_deploymentsPackage.DEPLOYMENT_MODULE: {
				DeploymentModule deploymentModule = (DeploymentModule)theEObject;
				T result = caseDeploymentModule(deploymentModule);
				if (result == null) result = caseIApplicationModule(deploymentModule);
				if (result == null) result = caseIModule(deploymentModule);
				if (result == null) result = caseINamed(deploymentModule);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_deploymentsPackage.APP_ASSEMBLY: {
				AppAssembly appAssembly = (AppAssembly)theEObject;
				T result = caseAppAssembly(appAssembly);
				if (result == null) result = caseIAssembly(appAssembly);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_deploymentsPackage.ALLOCATION_PLAN: {
				AllocationPlan allocationPlan = (AllocationPlan)theEObject;
				T result = caseAllocationPlan(allocationPlan);
				if (result == null) result = caseIApplicationModule(allocationPlan);
				if (result == null) result = caseIModule(allocationPlan);
				if (result == null) result = caseINamed(allocationPlan);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION: {
				ComponentInstanceInitialization componentInstanceInitialization = (ComponentInstanceInitialization)theEObject;
				T result = caseComponentInstanceInitialization(componentInstanceInitialization);
				if (result == null) result = caseIHasAttributeInitialization(componentInstanceInitialization);
				if (result == null) result = caseINamed(componentInstanceInitialization);
				if (result == null) result = caseIAnnotable(componentInstanceInitialization);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION: {
				ComponentInstancePortInitialization componentInstancePortInitialization = (ComponentInstancePortInitialization)theEObject;
				T result = caseComponentInstancePortInitialization(componentInstancePortInitialization);
				if (result == null) result = caseIHasAttributeInitialization(componentInstancePortInitialization);
				if (result == null) result = caseINamed(componentInstancePortInitialization);
				if (result == null) result = caseIAnnotable(componentInstancePortInitialization);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_POLICY_INITIALIZATION: {
				ComponentInstancePolicyInitialization componentInstancePolicyInitialization = (ComponentInstancePolicyInitialization)theEObject;
				T result = caseComponentInstancePolicyInitialization(componentInstancePolicyInitialization);
				if (result == null) result = caseIHasAttributeInitialization(componentInstancePolicyInitialization);
				if (result == null) result = caseINamed(componentInstancePolicyInitialization);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_deploymentsPackage.ASSEMBLY_PART_REFERENCE: {
				AssemblyPartReference assemblyPartReference = (AssemblyPartReference)theEObject;
				T result = caseAssemblyPartReference(assemblyPartReference);
				if (result == null) result = caseINamed(assemblyPartReference);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE: {
				ComponentInstance componentInstance = (ComponentInstance)theEObject;
				T result = caseComponentInstance(componentInstance);
				if (result == null) result = caseINamed(componentInstance);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_deploymentsPackage.CONNECTION_INSTANCE: {
				ConnectionInstance connectionInstance = (ConnectionInstance)theEObject;
				T result = caseConnectionInstance(connectionInstance);
				if (result == null) result = caseINamed(connectionInstance);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_deploymentsPackage.CONNECTION_CONFIGURATION: {
				ConnectionConfiguration connectionConfiguration = (ConnectionConfiguration)theEObject;
				T result = caseConnectionConfiguration(connectionConfiguration);
				if (result == null) result = caseINamed(connectionConfiguration);
				if (result == null) result = caseIConfigured(connectionConfiguration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_deploymentsPackage.CONNECTION_END_CONFIGURATION: {
				ConnectionEndConfiguration connectionEndConfiguration = (ConnectionEndConfiguration)theEObject;
				T result = caseConnectionEndConfiguration(connectionEndConfiguration);
				if (result == null) result = caseINamed(connectionEndConfiguration);
				if (result == null) result = caseIConfigured(connectionEndConfiguration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_deploymentsPackage.TECHNICAL_POLICY_CONFIGURATION: {
				TechnicalPolicyConfiguration technicalPolicyConfiguration = (TechnicalPolicyConfiguration)theEObject;
				T result = caseTechnicalPolicyConfiguration(technicalPolicyConfiguration);
				if (result == null) result = caseINamed(technicalPolicyConfiguration);
				if (result == null) result = caseIConfigured(technicalPolicyConfiguration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_deploymentsPackage.ARTEFACT: {
				Artefact artefact = (Artefact)theEObject;
				T result = caseArtefact(artefact);
				if (result == null) result = caseINamed(artefact);
				if (result == null) result = caseIAnnotable(artefact);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_deploymentsPackage.ICOMPONENT_NODE: {
				IComponentNode iComponentNode = (IComponentNode)theEObject;
				T result = caseIComponentNode(iComponentNode);
				if (result == null) result = caseINamed(iComponentNode);
				if (result == null) result = caseIAnnotable(iComponentNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_deploymentsPackage.IHAS_ATTRIBUTE_INITIALIZATION: {
				IHasAttributeInitialization iHasAttributeInitialization = (IHasAttributeInitialization)theEObject;
				T result = caseIHasAttributeInitialization(iHasAttributeInitialization);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ucm_deploymentsPackage.ATTRIBUTE_VALUE: {
				AttributeValue attributeValue = (AttributeValue)theEObject;
				T result = caseAttributeValue(attributeValue);
				if (result == null) result = caseIValued(attributeValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Deployment Module</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Deployment Module</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeploymentModule(DeploymentModule object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>App Assembly</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>App Assembly</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAppAssembly(AppAssembly object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Allocation Plan</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Allocation Plan</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAllocationPlan(AllocationPlan object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component Instance Initialization</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component Instance Initialization</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponentInstanceInitialization(ComponentInstanceInitialization object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component Instance Port Initialization</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component Instance Port Initialization</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponentInstancePortInitialization(ComponentInstancePortInitialization object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component Instance Policy Initialization</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component Instance Policy Initialization</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponentInstancePolicyInitialization(ComponentInstancePolicyInitialization object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Assembly Part Reference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Assembly Part Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssemblyPartReference(AssemblyPartReference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component Instance</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component Instance</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponentInstance(ComponentInstance object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Connection Instance</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Connection Instance</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConnectionInstance(ConnectionInstance object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Connection Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Connection Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConnectionConfiguration(ConnectionConfiguration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Connection End Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Connection End Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConnectionEndConfiguration(ConnectionEndConfiguration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Technical Policy Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Technical Policy Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTechnicalPolicyConfiguration(TechnicalPolicyConfiguration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Artefact</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Artefact</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseArtefact(Artefact object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IComponent Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IComponent Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIComponentNode(IComponentNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IHas Attribute Initialization</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IHas Attribute Initialization</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIHasAttributeInitialization(IHasAttributeInitialization object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attribute Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attribute Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAttributeValue(AttributeValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>INamed</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>INamed</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseINamed(INamed object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IModule</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IModule</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIModule(IModule object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IApplication Module</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IApplication Module</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIApplicationModule(IApplicationModule object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IAssembly</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IAssembly</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIAssembly(IAssembly object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IAnnotable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IAnnotable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIAnnotable(IAnnotable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IConfigured</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IConfigured</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIConfigured(IConfigured object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IValued</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IValued</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIValued(IValued object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public T defaultCase(EObject object) {
		return null;
	}

} //Ucm_deploymentsSwitch
