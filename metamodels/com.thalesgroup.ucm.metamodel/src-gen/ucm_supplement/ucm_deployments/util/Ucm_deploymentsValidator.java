/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_deployments.util;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

import ucm_supplement.ucm_deployments.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage
 * @generated
 */
public class Ucm_deploymentsValidator extends EObjectValidator {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final Ucm_deploymentsValidator INSTANCE = new Ucm_deploymentsValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "ucm_supplement.ucm_deployments";

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Instances Belong To App Assembly' of 'Deployment Module'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int DEPLOYMENT_MODULE__INSTANCES_BELONG_TO_APP_ASSEMBLY = 1;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Configuration Is Complete' of 'Allocation Plan'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int ALLOCATION_PLAN__CONFIGURATION_IS_COMPLETE = 2;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Initialization Is Complete' of 'Allocation Plan'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int ALLOCATION_PLAN__INITIALIZATION_IS_COMPLETE = 3;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Instances Belong To The Same Deployment' of 'Allocation Plan'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int ALLOCATION_PLAN__INSTANCES_BELONG_TO_THE_SAME_DEPLOYMENT = 4;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Attribute Belongs To Component' of 'Component Instance Initialization'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int COMPONENT_INSTANCE_INITIALIZATION__ATTRIBUTE_BELONGS_TO_COMPONENT = 5;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Instance Belong To App Assembly' of 'Component Instance Initialization'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int COMPONENT_INSTANCE_INITIALIZATION__INSTANCE_BELONG_TO_APP_ASSEMBLY = 6;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Attribute Belongs To Port' of 'Component Instance Port Initialization'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int COMPONENT_INSTANCE_PORT_INITIALIZATION__ATTRIBUTE_BELONGS_TO_PORT = 7;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Attribute Belongs To Policy' of 'Component Instance Policy Initialization'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int COMPONENT_INSTANCE_POLICY_INITIALIZATION__ATTRIBUTE_BELONGS_TO_POLICY = 8;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Subpart Belongs To Part' of 'Assembly Part Reference'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int ASSEMBLY_PART_REFERENCE__SUBPART_BELONGS_TO_PART = 9;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Connection Belongs To Part' of 'Connection Instance'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int CONNECTION_INSTANCE__CONNECTION_BELONGS_TO_PART = 10;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Instance Belong To App Assembly' of 'Connection Configuration'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int CONNECTION_CONFIGURATION__INSTANCE_BELONG_TO_APP_ASSEMBLY = 11;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Policy Manages Component' of 'Technical Policy Configuration'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int TECHNICAL_POLICY_CONFIGURATION__POLICY_MANAGES_COMPONENT = 12;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Instance Belong To App Assembly' of 'Technical Policy Configuration'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int TECHNICAL_POLICY_CONFIGURATION__INSTANCE_BELONG_TO_APP_ASSEMBLY = 13;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 13;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ucm_deploymentsValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return Ucm_deploymentsPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case Ucm_deploymentsPackage.DEPLOYMENT_MODULE:
				return validateDeploymentModule((DeploymentModule)value, diagnostics, context);
			case Ucm_deploymentsPackage.APP_ASSEMBLY:
				return validateAppAssembly((AppAssembly)value, diagnostics, context);
			case Ucm_deploymentsPackage.ALLOCATION_PLAN:
				return validateAllocationPlan((AllocationPlan)value, diagnostics, context);
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_INITIALIZATION:
				return validateComponentInstanceInitialization((ComponentInstanceInitialization)value, diagnostics, context);
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_PORT_INITIALIZATION:
				return validateComponentInstancePortInitialization((ComponentInstancePortInitialization)value, diagnostics, context);
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE_POLICY_INITIALIZATION:
				return validateComponentInstancePolicyInitialization((ComponentInstancePolicyInitialization)value, diagnostics, context);
			case Ucm_deploymentsPackage.ASSEMBLY_PART_REFERENCE:
				return validateAssemblyPartReference((AssemblyPartReference)value, diagnostics, context);
			case Ucm_deploymentsPackage.COMPONENT_INSTANCE:
				return validateComponentInstance((ComponentInstance)value, diagnostics, context);
			case Ucm_deploymentsPackage.CONNECTION_INSTANCE:
				return validateConnectionInstance((ConnectionInstance)value, diagnostics, context);
			case Ucm_deploymentsPackage.CONNECTION_CONFIGURATION:
				return validateConnectionConfiguration((ConnectionConfiguration)value, diagnostics, context);
			case Ucm_deploymentsPackage.CONNECTION_END_CONFIGURATION:
				return validateConnectionEndConfiguration((ConnectionEndConfiguration)value, diagnostics, context);
			case Ucm_deploymentsPackage.TECHNICAL_POLICY_CONFIGURATION:
				return validateTechnicalPolicyConfiguration((TechnicalPolicyConfiguration)value, diagnostics, context);
			case Ucm_deploymentsPackage.ARTEFACT:
				return validateArtefact((Artefact)value, diagnostics, context);
			case Ucm_deploymentsPackage.ICOMPONENT_NODE:
				return validateIComponentNode((IComponentNode)value, diagnostics, context);
			case Ucm_deploymentsPackage.IHAS_ATTRIBUTE_INITIALIZATION:
				return validateIHasAttributeInitialization((IHasAttributeInitialization)value, diagnostics, context);
			case Ucm_deploymentsPackage.ATTRIBUTE_VALUE:
				return validateAttributeValue((AttributeValue)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDeploymentModule(DeploymentModule deploymentModule, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(deploymentModule, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(deploymentModule, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(deploymentModule, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(deploymentModule, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(deploymentModule, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(deploymentModule, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(deploymentModule, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(deploymentModule, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(deploymentModule, diagnostics, context);
		if (result || diagnostics != null) result &= validateDeploymentModule_instancesBelongToAppAssembly(deploymentModule, diagnostics, context);
		return result;
	}

	/**
	 * Validates the instancesBelongToAppAssembly constraint of '<em>Deployment Module</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDeploymentModule_instancesBelongToAppAssembly(DeploymentModule deploymentModule, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return deploymentModule.instancesBelongToAppAssembly(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAppAssembly(AppAssembly appAssembly, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(appAssembly, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAllocationPlan(AllocationPlan allocationPlan, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(allocationPlan, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(allocationPlan, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(allocationPlan, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(allocationPlan, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(allocationPlan, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(allocationPlan, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(allocationPlan, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(allocationPlan, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(allocationPlan, diagnostics, context);
		if (result || diagnostics != null) result &= validateAllocationPlan_configurationIsComplete(allocationPlan, diagnostics, context);
		if (result || diagnostics != null) result &= validateAllocationPlan_initializationIsComplete(allocationPlan, diagnostics, context);
		if (result || diagnostics != null) result &= validateAllocationPlan_instancesBelongToTheSameDeployment(allocationPlan, diagnostics, context);
		return result;
	}

	/**
	 * Validates the configurationIsComplete constraint of '<em>Allocation Plan</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAllocationPlan_configurationIsComplete(AllocationPlan allocationPlan, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return allocationPlan.configurationIsComplete(diagnostics, context);
	}

	/**
	 * Validates the initializationIsComplete constraint of '<em>Allocation Plan</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAllocationPlan_initializationIsComplete(AllocationPlan allocationPlan, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return allocationPlan.initializationIsComplete(diagnostics, context);
	}

	/**
	 * Validates the instancesBelongToTheSameDeployment constraint of '<em>Allocation Plan</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAllocationPlan_instancesBelongToTheSameDeployment(AllocationPlan allocationPlan, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return allocationPlan.instancesBelongToTheSameDeployment(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComponentInstanceInitialization(ComponentInstanceInitialization componentInstanceInitialization, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(componentInstanceInitialization, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(componentInstanceInitialization, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(componentInstanceInitialization, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(componentInstanceInitialization, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(componentInstanceInitialization, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(componentInstanceInitialization, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(componentInstanceInitialization, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(componentInstanceInitialization, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(componentInstanceInitialization, diagnostics, context);
		if (result || diagnostics != null) result &= validateComponentInstanceInitialization_attributeBelongsToComponent(componentInstanceInitialization, diagnostics, context);
		if (result || diagnostics != null) result &= validateComponentInstanceInitialization_instanceBelongToAppAssembly(componentInstanceInitialization, diagnostics, context);
		return result;
	}

	/**
	 * Validates the attributeBelongsToComponent constraint of '<em>Component Instance Initialization</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComponentInstanceInitialization_attributeBelongsToComponent(ComponentInstanceInitialization componentInstanceInitialization, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return componentInstanceInitialization.attributeBelongsToComponent(diagnostics, context);
	}

	/**
	 * Validates the instanceBelongToAppAssembly constraint of '<em>Component Instance Initialization</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComponentInstanceInitialization_instanceBelongToAppAssembly(ComponentInstanceInitialization componentInstanceInitialization, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return componentInstanceInitialization.instanceBelongToAppAssembly(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComponentInstancePortInitialization(ComponentInstancePortInitialization componentInstancePortInitialization, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(componentInstancePortInitialization, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(componentInstancePortInitialization, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(componentInstancePortInitialization, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(componentInstancePortInitialization, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(componentInstancePortInitialization, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(componentInstancePortInitialization, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(componentInstancePortInitialization, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(componentInstancePortInitialization, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(componentInstancePortInitialization, diagnostics, context);
		if (result || diagnostics != null) result &= validateComponentInstancePortInitialization_attributeBelongsToPort(componentInstancePortInitialization, diagnostics, context);
		return result;
	}

	/**
	 * Validates the attributeBelongsToPort constraint of '<em>Component Instance Port Initialization</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComponentInstancePortInitialization_attributeBelongsToPort(ComponentInstancePortInitialization componentInstancePortInitialization, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return componentInstancePortInitialization.attributeBelongsToPort(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComponentInstancePolicyInitialization(ComponentInstancePolicyInitialization componentInstancePolicyInitialization, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(componentInstancePolicyInitialization, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(componentInstancePolicyInitialization, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(componentInstancePolicyInitialization, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(componentInstancePolicyInitialization, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(componentInstancePolicyInitialization, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(componentInstancePolicyInitialization, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(componentInstancePolicyInitialization, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(componentInstancePolicyInitialization, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(componentInstancePolicyInitialization, diagnostics, context);
		if (result || diagnostics != null) result &= validateComponentInstancePolicyInitialization_attributeBelongsToPolicy(componentInstancePolicyInitialization, diagnostics, context);
		return result;
	}

	/**
	 * Validates the attributeBelongsToPolicy constraint of '<em>Component Instance Policy Initialization</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComponentInstancePolicyInitialization_attributeBelongsToPolicy(ComponentInstancePolicyInitialization componentInstancePolicyInitialization, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return componentInstancePolicyInitialization.attributeBelongsToPolicy(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssemblyPartReference(AssemblyPartReference assemblyPartReference, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(assemblyPartReference, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(assemblyPartReference, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(assemblyPartReference, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(assemblyPartReference, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(assemblyPartReference, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(assemblyPartReference, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(assemblyPartReference, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(assemblyPartReference, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(assemblyPartReference, diagnostics, context);
		if (result || diagnostics != null) result &= validateAssemblyPartReference_subpartBelongsToPart(assemblyPartReference, diagnostics, context);
		return result;
	}

	/**
	 * Validates the subpartBelongsToPart constraint of '<em>Assembly Part Reference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssemblyPartReference_subpartBelongsToPart(AssemblyPartReference assemblyPartReference, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return assemblyPartReference.subpartBelongsToPart(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComponentInstance(ComponentInstance componentInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(componentInstance, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateConnectionInstance(ConnectionInstance connectionInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(connectionInstance, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(connectionInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(connectionInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(connectionInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(connectionInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(connectionInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(connectionInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(connectionInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(connectionInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validateConnectionInstance_connectionBelongsToPart(connectionInstance, diagnostics, context);
		return result;
	}

	/**
	 * Validates the connectionBelongsToPart constraint of '<em>Connection Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateConnectionInstance_connectionBelongsToPart(ConnectionInstance connectionInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return connectionInstance.connectionBelongsToPart(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateConnectionConfiguration(ConnectionConfiguration connectionConfiguration, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(connectionConfiguration, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(connectionConfiguration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(connectionConfiguration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(connectionConfiguration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(connectionConfiguration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(connectionConfiguration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(connectionConfiguration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(connectionConfiguration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(connectionConfiguration, diagnostics, context);
		if (result || diagnostics != null) result &= validateConnectionConfiguration_instanceBelongToAppAssembly(connectionConfiguration, diagnostics, context);
		return result;
	}

	/**
	 * Validates the instanceBelongToAppAssembly constraint of '<em>Connection Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateConnectionConfiguration_instanceBelongToAppAssembly(ConnectionConfiguration connectionConfiguration, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return connectionConfiguration.instanceBelongToAppAssembly(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateConnectionEndConfiguration(ConnectionEndConfiguration connectionEndConfiguration, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(connectionEndConfiguration, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTechnicalPolicyConfiguration(TechnicalPolicyConfiguration technicalPolicyConfiguration, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(technicalPolicyConfiguration, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(technicalPolicyConfiguration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(technicalPolicyConfiguration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(technicalPolicyConfiguration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(technicalPolicyConfiguration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(technicalPolicyConfiguration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(technicalPolicyConfiguration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(technicalPolicyConfiguration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(technicalPolicyConfiguration, diagnostics, context);
		if (result || diagnostics != null) result &= validateTechnicalPolicyConfiguration_policyManagesComponent(technicalPolicyConfiguration, diagnostics, context);
		if (result || diagnostics != null) result &= validateTechnicalPolicyConfiguration_instanceBelongToAppAssembly(technicalPolicyConfiguration, diagnostics, context);
		return result;
	}

	/**
	 * Validates the policyManagesComponent constraint of '<em>Technical Policy Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTechnicalPolicyConfiguration_policyManagesComponent(TechnicalPolicyConfiguration technicalPolicyConfiguration, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return technicalPolicyConfiguration.policyManagesComponent(diagnostics, context);
	}

	/**
	 * Validates the instanceBelongToAppAssembly constraint of '<em>Technical Policy Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTechnicalPolicyConfiguration_instanceBelongToAppAssembly(TechnicalPolicyConfiguration technicalPolicyConfiguration, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return technicalPolicyConfiguration.instanceBelongToAppAssembly(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateArtefact(Artefact artefact, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(artefact, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIComponentNode(IComponentNode iComponentNode, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(iComponentNode, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIHasAttributeInitialization(IHasAttributeInitialization iHasAttributeInitialization, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(iHasAttributeInitialization, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAttributeValue(AttributeValue attributeValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(attributeValue, diagnostics, context);
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //Ucm_deploymentsValidator
