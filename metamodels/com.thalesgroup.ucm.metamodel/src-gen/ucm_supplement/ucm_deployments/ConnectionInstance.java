/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_deployments;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

import ucm_base.ucm_commons.INamed;

import ucm_base.ucm_components.Connection;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Connection Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_deployments.ConnectionInstance#getConnection <em>Connection</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.ConnectionInstance#getPartReference <em>Part Reference</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getConnectionInstance()
 * @model
 * @generated
 */
public interface ConnectionInstance extends INamed {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Connection</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connection</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connection</em>' reference.
	 * @see #setConnection(Connection)
	 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getConnectionInstance_Connection()
	 * @model required="true"
	 * @generated
	 */
	Connection getConnection();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_deployments.ConnectionInstance#getConnection <em>Connection</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Connection</em>' reference.
	 * @see #getConnection()
	 * @generated
	 */
	void setConnection(Connection value);

	/**
	 * Returns the value of the '<em><b>Part Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Part Reference</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Part Reference</em>' containment reference.
	 * @see #setPartReference(AssemblyPartReference)
	 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getConnectionInstance_PartReference()
	 * @model containment="true"
	 * @generated
	 */
	AssemblyPartReference getPartReference();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_deployments.ConnectionInstance#getPartReference <em>Part Reference</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Part Reference</em>' containment reference.
	 * @see #getPartReference()
	 * @generated
	 */
	void setPartReference(AssemblyPartReference value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean connectionBelongsToPart(DiagnosticChain diagnostics, Map<Object, Object> context);

} // ConnectionInstance
