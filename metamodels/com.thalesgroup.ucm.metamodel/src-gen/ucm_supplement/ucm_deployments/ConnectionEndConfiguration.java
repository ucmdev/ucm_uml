/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_deployments;

import ucm_base.ucm_commons.INamed;
import ucm_base.ucm_components.Port;
import ucm_base.ucm_contracts.IConfigured;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Connection End Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_deployments.ConnectionEndConfiguration#getComponentInstance <em>Component Instance</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.ConnectionEndConfiguration#getPort <em>Port</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getConnectionEndConfiguration()
 * @model
 * @generated
 */
public interface ConnectionEndConfiguration extends INamed, IConfigured {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Component Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Instance</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Instance</em>' reference.
	 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getConnectionEndConfiguration_ComponentInstance()
	 * @model required="true" changeable="false"
	 * @generated
	 */
	ComponentInstance getComponentInstance();

	/**
	 * Returns the value of the '<em><b>Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port</em>' reference.
	 * @see #setPort(Port)
	 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getConnectionEndConfiguration_Port()
	 * @model required="true"
	 * @generated
	 */
	Port getPort();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_deployments.ConnectionEndConfiguration#getPort <em>Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port</em>' reference.
	 * @see #getPort()
	 * @generated
	 */
	void setPort(Port value);

} // ConnectionEndConfiguration
