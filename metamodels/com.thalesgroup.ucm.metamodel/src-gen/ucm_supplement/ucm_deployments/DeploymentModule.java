/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_deployments;

import java.util.Map;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import ucm_base.ucm_commons.IApplicationModule;

import ucm_supplement.ucm_test_cases.TestCaseGroup;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Deployment Module</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_deployments.DeploymentModule#getAssembly <em>Assembly</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.DeploymentModule#getAllocationPlan <em>Allocation Plan</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.DeploymentModule#getArtefact <em>Artefact</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.DeploymentModule#getTestCaseGroup <em>Test Case Group</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.DeploymentModule#getComponentInstance <em>Component Instance</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.DeploymentModule#getConnectionInstance <em>Connection Instance</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getDeploymentModule()
 * @model
 * @generated
 */
public interface DeploymentModule extends IApplicationModule {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Assembly</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assembly</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assembly</em>' containment reference.
	 * @see #setAssembly(AppAssembly)
	 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getDeploymentModule_Assembly()
	 * @model containment="true" required="true"
	 * @generated
	 */
	AppAssembly getAssembly();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_deployments.DeploymentModule#getAssembly <em>Assembly</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assembly</em>' containment reference.
	 * @see #getAssembly()
	 * @generated
	 */
	void setAssembly(AppAssembly value);

	/**
	 * Returns the value of the '<em><b>Allocation Plan</b></em>' containment reference list.
	 * The list contents are of type {@link ucm_supplement.ucm_deployments.AllocationPlan}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Allocation Plan</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Allocation Plan</em>' containment reference list.
	 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getDeploymentModule_AllocationPlan()
	 * @model containment="true"
	 * @generated
	 */
	EList<AllocationPlan> getAllocationPlan();

	/**
	 * Returns the value of the '<em><b>Artefact</b></em>' containment reference list.
	 * The list contents are of type {@link ucm_supplement.ucm_deployments.Artefact}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Artefact</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Artefact</em>' containment reference list.
	 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getDeploymentModule_Artefact()
	 * @model containment="true"
	 * @generated
	 */
	EList<Artefact> getArtefact();

	/**
	 * Returns the value of the '<em><b>Test Case Group</b></em>' containment reference list.
	 * The list contents are of type {@link ucm_supplement.ucm_test_cases.TestCaseGroup}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Test Case Group</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Test Case Group</em>' containment reference list.
	 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getDeploymentModule_TestCaseGroup()
	 * @model containment="true"
	 * @generated
	 */
	EList<TestCaseGroup> getTestCaseGroup();

	/**
	 * Returns the value of the '<em><b>Component Instance</b></em>' containment reference list.
	 * The list contents are of type {@link ucm_supplement.ucm_deployments.ComponentInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Instance</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Instance</em>' containment reference list.
	 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getDeploymentModule_ComponentInstance()
	 * @model containment="true"
	 * @generated
	 */
	EList<ComponentInstance> getComponentInstance();

	/**
	 * Returns the value of the '<em><b>Connection Instance</b></em>' containment reference list.
	 * The list contents are of type {@link ucm_supplement.ucm_deployments.ConnectionInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connection Instance</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connection Instance</em>' containment reference list.
	 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getDeploymentModule_ConnectionInstance()
	 * @model containment="true"
	 * @generated
	 */
	EList<ConnectionInstance> getConnectionInstance();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean instancesBelongToAppAssembly(DiagnosticChain diagnostics, Map<Object, Object> context);

} // DeploymentModule
