/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_deployments;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage
 * @generated
 */
public interface Ucm_deploymentsFactory extends EFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Ucm_deploymentsFactory eINSTANCE = ucm_supplement.ucm_deployments.impl.Ucm_deploymentsFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Deployment Module</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Deployment Module</em>'.
	 * @generated
	 */
	DeploymentModule createDeploymentModule();

	/**
	 * Returns a new object of class '<em>App Assembly</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>App Assembly</em>'.
	 * @generated
	 */
	AppAssembly createAppAssembly();

	/**
	 * Returns a new object of class '<em>Allocation Plan</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Allocation Plan</em>'.
	 * @generated
	 */
	AllocationPlan createAllocationPlan();

	/**
	 * Returns a new object of class '<em>Component Instance Initialization</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Component Instance Initialization</em>'.
	 * @generated
	 */
	ComponentInstanceInitialization createComponentInstanceInitialization();

	/**
	 * Returns a new object of class '<em>Component Instance Port Initialization</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Component Instance Port Initialization</em>'.
	 * @generated
	 */
	ComponentInstancePortInitialization createComponentInstancePortInitialization();

	/**
	 * Returns a new object of class '<em>Component Instance Policy Initialization</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Component Instance Policy Initialization</em>'.
	 * @generated
	 */
	ComponentInstancePolicyInitialization createComponentInstancePolicyInitialization();

	/**
	 * Returns a new object of class '<em>Assembly Part Reference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Assembly Part Reference</em>'.
	 * @generated
	 */
	AssemblyPartReference createAssemblyPartReference();

	/**
	 * Returns a new object of class '<em>Component Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Component Instance</em>'.
	 * @generated
	 */
	ComponentInstance createComponentInstance();

	/**
	 * Returns a new object of class '<em>Connection Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Connection Instance</em>'.
	 * @generated
	 */
	ConnectionInstance createConnectionInstance();

	/**
	 * Returns a new object of class '<em>Connection Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Connection Configuration</em>'.
	 * @generated
	 */
	ConnectionConfiguration createConnectionConfiguration();

	/**
	 * Returns a new object of class '<em>Connection End Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Connection End Configuration</em>'.
	 * @generated
	 */
	ConnectionEndConfiguration createConnectionEndConfiguration();

	/**
	 * Returns a new object of class '<em>Technical Policy Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Technical Policy Configuration</em>'.
	 * @generated
	 */
	TechnicalPolicyConfiguration createTechnicalPolicyConfiguration();

	/**
	 * Returns a new object of class '<em>Artefact</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Artefact</em>'.
	 * @generated
	 */
	Artefact createArtefact();

	/**
	 * Returns a new object of class '<em>Attribute Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Attribute Value</em>'.
	 * @generated
	 */
	AttributeValue createAttributeValue();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	Ucm_deploymentsPackage getUcm_deploymentsPackage();

} //Ucm_deploymentsFactory
