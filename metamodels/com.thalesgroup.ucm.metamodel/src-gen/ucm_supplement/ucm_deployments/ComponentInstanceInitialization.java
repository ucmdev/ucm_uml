/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_deployments;

import java.util.Map;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import ucm_base.ucm_commons.INamed;

import ucm_base.ucm_contracts.IAnnotable;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component Instance Initialization</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_deployments.ComponentInstanceInitialization#getComponentInstance <em>Component Instance</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.ComponentInstanceInitialization#getPortInitialization <em>Port Initialization</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.ComponentInstanceInitialization#getPolicyInitialization <em>Policy Initialization</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getComponentInstanceInitialization()
 * @model
 * @generated
 */
public interface ComponentInstanceInitialization extends IHasAttributeInitialization, INamed, IAnnotable {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Component Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Instance</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Instance</em>' reference.
	 * @see #setComponentInstance(ComponentInstance)
	 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getComponentInstanceInitialization_ComponentInstance()
	 * @model required="true"
	 * @generated
	 */
	ComponentInstance getComponentInstance();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_deployments.ComponentInstanceInitialization#getComponentInstance <em>Component Instance</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component Instance</em>' reference.
	 * @see #getComponentInstance()
	 * @generated
	 */
	void setComponentInstance(ComponentInstance value);

	/**
	 * Returns the value of the '<em><b>Port Initialization</b></em>' containment reference list.
	 * The list contents are of type {@link ucm_supplement.ucm_deployments.ComponentInstancePortInitialization}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port Initialization</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port Initialization</em>' containment reference list.
	 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getComponentInstanceInitialization_PortInitialization()
	 * @model containment="true"
	 * @generated
	 */
	EList<ComponentInstancePortInitialization> getPortInitialization();

	/**
	 * Returns the value of the '<em><b>Policy Initialization</b></em>' containment reference list.
	 * The list contents are of type {@link ucm_supplement.ucm_deployments.ComponentInstancePolicyInitialization}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Policy Initialization</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Policy Initialization</em>' containment reference list.
	 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getComponentInstanceInitialization_PolicyInitialization()
	 * @model containment="true"
	 * @generated
	 */
	EList<ComponentInstancePolicyInitialization> getPolicyInitialization();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean attributeBelongsToComponent(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean instanceBelongToAppAssembly(DiagnosticChain diagnostics, Map<Object, Object> context);

} // ComponentInstanceInitialization
