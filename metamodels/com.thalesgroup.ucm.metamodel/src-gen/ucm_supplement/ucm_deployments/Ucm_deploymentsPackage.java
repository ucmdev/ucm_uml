/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_deployments;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import ucm_base.ucm_commons.Ucm_commonsPackage;

import ucm_base.ucm_components.Ucm_componentsPackage;

import ucm_base.ucm_contracts.Ucm_contractsPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsFactory
 * @model kind="package"
 * @generated
 */
public interface Ucm_deploymentsPackage extends EPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ucm_deployments";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://com.thalesgroup.ucm/1.0/metamodels/supplements/deployments";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ucm_deployments";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Ucm_deploymentsPackage eINSTANCE = ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl.init();

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_deployments.impl.DeploymentModuleImpl <em>Deployment Module</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_deployments.impl.DeploymentModuleImpl
	 * @see ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl#getDeploymentModule()
	 * @generated
	 */
	int DEPLOYMENT_MODULE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_MODULE__NAME = Ucm_commonsPackage.IAPPLICATION_MODULE__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_MODULE__COMMENT = Ucm_commonsPackage.IAPPLICATION_MODULE__COMMENT;

	/**
	 * The feature id for the '<em><b>Assembly</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_MODULE__ASSEMBLY = Ucm_commonsPackage.IAPPLICATION_MODULE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Allocation Plan</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_MODULE__ALLOCATION_PLAN = Ucm_commonsPackage.IAPPLICATION_MODULE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Artefact</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_MODULE__ARTEFACT = Ucm_commonsPackage.IAPPLICATION_MODULE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Test Case Group</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_MODULE__TEST_CASE_GROUP = Ucm_commonsPackage.IAPPLICATION_MODULE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Component Instance</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_MODULE__COMPONENT_INSTANCE = Ucm_commonsPackage.IAPPLICATION_MODULE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Connection Instance</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_MODULE__CONNECTION_INSTANCE = Ucm_commonsPackage.IAPPLICATION_MODULE_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Deployment Module</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_MODULE_FEATURE_COUNT = Ucm_commonsPackage.IAPPLICATION_MODULE_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_deployments.impl.AppAssemblyImpl <em>App Assembly</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_deployments.impl.AppAssemblyImpl
	 * @see ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl#getAppAssembly()
	 * @generated
	 */
	int APP_ASSEMBLY = 1;

	/**
	 * The feature id for the '<em><b>Part</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APP_ASSEMBLY__PART = Ucm_componentsPackage.IASSEMBLY__PART;

	/**
	 * The feature id for the '<em><b>Internal Connection</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APP_ASSEMBLY__INTERNAL_CONNECTION = Ucm_componentsPackage.IASSEMBLY__INTERNAL_CONNECTION;

	/**
	 * The number of structural features of the '<em>App Assembly</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APP_ASSEMBLY_FEATURE_COUNT = Ucm_componentsPackage.IASSEMBLY_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_deployments.impl.AllocationPlanImpl <em>Allocation Plan</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_deployments.impl.AllocationPlanImpl
	 * @see ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl#getAllocationPlan()
	 * @generated
	 */
	int ALLOCATION_PLAN = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALLOCATION_PLAN__NAME = Ucm_commonsPackage.IAPPLICATION_MODULE__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALLOCATION_PLAN__COMMENT = Ucm_commonsPackage.IAPPLICATION_MODULE__COMMENT;

	/**
	 * The feature id for the '<em><b>Node</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALLOCATION_PLAN__NODE = Ucm_commonsPackage.IAPPLICATION_MODULE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Component Instance Initialization</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALLOCATION_PLAN__COMPONENT_INSTANCE_INITIALIZATION = Ucm_commonsPackage.IAPPLICATION_MODULE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Connection Configuration</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALLOCATION_PLAN__CONNECTION_CONFIGURATION = Ucm_commonsPackage.IAPPLICATION_MODULE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Technical Policy Configuration</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALLOCATION_PLAN__TECHNICAL_POLICY_CONFIGURATION = Ucm_commonsPackage.IAPPLICATION_MODULE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Refines</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALLOCATION_PLAN__REFINES = Ucm_commonsPackage.IAPPLICATION_MODULE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Allocation Plan</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALLOCATION_PLAN_FEATURE_COUNT = Ucm_commonsPackage.IAPPLICATION_MODULE_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_deployments.impl.IHasAttributeInitializationImpl <em>IHas Attribute Initialization</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_deployments.impl.IHasAttributeInitializationImpl
	 * @see ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl#getIHasAttributeInitialization()
	 * @generated
	 */
	int IHAS_ATTRIBUTE_INITIALIZATION = 14;

	/**
	 * The feature id for the '<em><b>Attribute Initialization</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHAS_ATTRIBUTE_INITIALIZATION__ATTRIBUTE_INITIALIZATION = 0;

	/**
	 * The number of structural features of the '<em>IHas Attribute Initialization</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHAS_ATTRIBUTE_INITIALIZATION_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_deployments.impl.ComponentInstanceInitializationImpl <em>Component Instance Initialization</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_deployments.impl.ComponentInstanceInitializationImpl
	 * @see ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl#getComponentInstanceInitialization()
	 * @generated
	 */
	int COMPONENT_INSTANCE_INITIALIZATION = 3;

	/**
	 * The feature id for the '<em><b>Attribute Initialization</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE_INITIALIZATION__ATTRIBUTE_INITIALIZATION = IHAS_ATTRIBUTE_INITIALIZATION__ATTRIBUTE_INITIALIZATION;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE_INITIALIZATION__NAME = IHAS_ATTRIBUTE_INITIALIZATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE_INITIALIZATION__COMMENT = IHAS_ATTRIBUTE_INITIALIZATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE_INITIALIZATION__ANNOTATION = IHAS_ATTRIBUTE_INITIALIZATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Component Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE_INITIALIZATION__COMPONENT_INSTANCE = IHAS_ATTRIBUTE_INITIALIZATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Port Initialization</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE_INITIALIZATION__PORT_INITIALIZATION = IHAS_ATTRIBUTE_INITIALIZATION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Policy Initialization</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE_INITIALIZATION__POLICY_INITIALIZATION = IHAS_ATTRIBUTE_INITIALIZATION_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Component Instance Initialization</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE_INITIALIZATION_FEATURE_COUNT = IHAS_ATTRIBUTE_INITIALIZATION_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_deployments.impl.ComponentInstancePortInitializationImpl <em>Component Instance Port Initialization</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_deployments.impl.ComponentInstancePortInitializationImpl
	 * @see ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl#getComponentInstancePortInitialization()
	 * @generated
	 */
	int COMPONENT_INSTANCE_PORT_INITIALIZATION = 4;

	/**
	 * The feature id for the '<em><b>Attribute Initialization</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE_PORT_INITIALIZATION__ATTRIBUTE_INITIALIZATION = IHAS_ATTRIBUTE_INITIALIZATION__ATTRIBUTE_INITIALIZATION;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE_PORT_INITIALIZATION__NAME = IHAS_ATTRIBUTE_INITIALIZATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE_PORT_INITIALIZATION__COMMENT = IHAS_ATTRIBUTE_INITIALIZATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE_PORT_INITIALIZATION__ANNOTATION = IHAS_ATTRIBUTE_INITIALIZATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE_PORT_INITIALIZATION__PORT = IHAS_ATTRIBUTE_INITIALIZATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Port Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE_PORT_INITIALIZATION__PORT_ELEMENT = IHAS_ATTRIBUTE_INITIALIZATION_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Component Instance Port Initialization</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE_PORT_INITIALIZATION_FEATURE_COUNT = IHAS_ATTRIBUTE_INITIALIZATION_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_deployments.impl.ComponentInstancePolicyInitializationImpl <em>Component Instance Policy Initialization</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_deployments.impl.ComponentInstancePolicyInitializationImpl
	 * @see ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl#getComponentInstancePolicyInitialization()
	 * @generated
	 */
	int COMPONENT_INSTANCE_POLICY_INITIALIZATION = 5;

	/**
	 * The feature id for the '<em><b>Attribute Initialization</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE_POLICY_INITIALIZATION__ATTRIBUTE_INITIALIZATION = IHAS_ATTRIBUTE_INITIALIZATION__ATTRIBUTE_INITIALIZATION;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE_POLICY_INITIALIZATION__NAME = IHAS_ATTRIBUTE_INITIALIZATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE_POLICY_INITIALIZATION__COMMENT = IHAS_ATTRIBUTE_INITIALIZATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Port Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE_POLICY_INITIALIZATION__PORT_ELEMENT = IHAS_ATTRIBUTE_INITIALIZATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Policy</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE_POLICY_INITIALIZATION__POLICY = IHAS_ATTRIBUTE_INITIALIZATION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Component Instance Policy Initialization</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE_POLICY_INITIALIZATION_FEATURE_COUNT = IHAS_ATTRIBUTE_INITIALIZATION_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_deployments.impl.AssemblyPartReferenceImpl <em>Assembly Part Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_deployments.impl.AssemblyPartReferenceImpl
	 * @see ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl#getAssemblyPartReference()
	 * @generated
	 */
	int ASSEMBLY_PART_REFERENCE = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSEMBLY_PART_REFERENCE__NAME = Ucm_commonsPackage.INAMED__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSEMBLY_PART_REFERENCE__COMMENT = Ucm_commonsPackage.INAMED__COMMENT;

	/**
	 * The feature id for the '<em><b>Reference To Subpart</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSEMBLY_PART_REFERENCE__REFERENCE_TO_SUBPART = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Part</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSEMBLY_PART_REFERENCE__PART = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Assembly Part Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSEMBLY_PART_REFERENCE_FEATURE_COUNT = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_deployments.impl.ComponentInstanceImpl <em>Component Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_deployments.impl.ComponentInstanceImpl
	 * @see ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl#getComponentInstance()
	 * @generated
	 */
	int COMPONENT_INSTANCE = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE__NAME = Ucm_commonsPackage.INAMED__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE__COMMENT = Ucm_commonsPackage.INAMED__COMMENT;

	/**
	 * The feature id for the '<em><b>Part Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE__PART_REFERENCE = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Component Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE_FEATURE_COUNT = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_deployments.impl.ConnectionInstanceImpl <em>Connection Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_deployments.impl.ConnectionInstanceImpl
	 * @see ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl#getConnectionInstance()
	 * @generated
	 */
	int CONNECTION_INSTANCE = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_INSTANCE__NAME = Ucm_commonsPackage.INAMED__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_INSTANCE__COMMENT = Ucm_commonsPackage.INAMED__COMMENT;

	/**
	 * The feature id for the '<em><b>Connection</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_INSTANCE__CONNECTION = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Part Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_INSTANCE__PART_REFERENCE = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Connection Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_INSTANCE_FEATURE_COUNT = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_deployments.impl.ConnectionConfigurationImpl <em>Connection Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_deployments.impl.ConnectionConfigurationImpl
	 * @see ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl#getConnectionConfiguration()
	 * @generated
	 */
	int CONNECTION_CONFIGURATION = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_CONFIGURATION__NAME = Ucm_commonsPackage.INAMED__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_CONFIGURATION__COMMENT = Ucm_commonsPackage.INAMED__COMMENT;

	/**
	 * The feature id for the '<em><b>Configuration Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_CONFIGURATION__CONFIGURATION_VALUE = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Connection</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_CONFIGURATION__CONNECTION = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>End Configuration</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_CONFIGURATION__END_CONFIGURATION = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Refined In</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_CONFIGURATION__REFINED_IN = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Connection Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_CONFIGURATION_FEATURE_COUNT = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_deployments.impl.ConnectionEndConfigurationImpl <em>Connection End Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_deployments.impl.ConnectionEndConfigurationImpl
	 * @see ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl#getConnectionEndConfiguration()
	 * @generated
	 */
	int CONNECTION_END_CONFIGURATION = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_END_CONFIGURATION__NAME = Ucm_commonsPackage.INAMED__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_END_CONFIGURATION__COMMENT = Ucm_commonsPackage.INAMED__COMMENT;

	/**
	 * The feature id for the '<em><b>Configuration Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_END_CONFIGURATION__CONFIGURATION_VALUE = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Component Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_END_CONFIGURATION__COMPONENT_INSTANCE = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_END_CONFIGURATION__PORT = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Connection End Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_END_CONFIGURATION_FEATURE_COUNT = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_deployments.impl.TechnicalPolicyConfigurationImpl <em>Technical Policy Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_deployments.impl.TechnicalPolicyConfigurationImpl
	 * @see ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl#getTechnicalPolicyConfiguration()
	 * @generated
	 */
	int TECHNICAL_POLICY_CONFIGURATION = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TECHNICAL_POLICY_CONFIGURATION__NAME = Ucm_commonsPackage.INAMED__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TECHNICAL_POLICY_CONFIGURATION__COMMENT = Ucm_commonsPackage.INAMED__COMMENT;

	/**
	 * The feature id for the '<em><b>Configuration Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TECHNICAL_POLICY_CONFIGURATION__CONFIGURATION_VALUE = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Policy</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TECHNICAL_POLICY_CONFIGURATION__POLICY = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Component Instance</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TECHNICAL_POLICY_CONFIGURATION__COMPONENT_INSTANCE = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Refined In</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TECHNICAL_POLICY_CONFIGURATION__REFINED_IN = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Technical Policy Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TECHNICAL_POLICY_CONFIGURATION_FEATURE_COUNT = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_deployments.impl.ArtefactImpl <em>Artefact</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_deployments.impl.ArtefactImpl
	 * @see ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl#getArtefact()
	 * @generated
	 */
	int ARTEFACT = 12;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT__NAME = Ucm_commonsPackage.INAMED__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT__COMMENT = Ucm_commonsPackage.INAMED__COMMENT;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT__ANNOTATION = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Component Implementation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT__COMPONENT_IMPLEMENTATION = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Artefact</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT_FEATURE_COUNT = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_deployments.impl.IComponentNodeImpl <em>IComponent Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_deployments.impl.IComponentNodeImpl
	 * @see ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl#getIComponentNode()
	 * @generated
	 */
	int ICOMPONENT_NODE = 13;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICOMPONENT_NODE__NAME = Ucm_commonsPackage.INAMED__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICOMPONENT_NODE__COMMENT = Ucm_commonsPackage.INAMED__COMMENT;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICOMPONENT_NODE__ANNOTATION = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Language</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICOMPONENT_NODE__LANGUAGE = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Component Instance</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICOMPONENT_NODE__COMPONENT_INSTANCE = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>IComponent Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICOMPONENT_NODE_FEATURE_COUNT = Ucm_commonsPackage.INAMED_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_deployments.impl.AttributeValueImpl <em>Attribute Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_deployments.impl.AttributeValueImpl
	 * @see ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl#getAttributeValue()
	 * @generated
	 */
	int ATTRIBUTE_VALUE = 15;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_VALUE__VALUE = Ucm_contractsPackage.IVALUED__VALUE;

	/**
	 * The feature id for the '<em><b>Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_VALUE__ATTRIBUTE = Ucm_contractsPackage.IVALUED_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Attribute Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_VALUE_FEATURE_COUNT = Ucm_contractsPackage.IVALUED_FEATURE_COUNT + 1;


	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_deployments.DeploymentModule <em>Deployment Module</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Deployment Module</em>'.
	 * @see ucm_supplement.ucm_deployments.DeploymentModule
	 * @generated
	 */
	EClass getDeploymentModule();

	/**
	 * Returns the meta object for the containment reference '{@link ucm_supplement.ucm_deployments.DeploymentModule#getAssembly <em>Assembly</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Assembly</em>'.
	 * @see ucm_supplement.ucm_deployments.DeploymentModule#getAssembly()
	 * @see #getDeploymentModule()
	 * @generated
	 */
	EReference getDeploymentModule_Assembly();

	/**
	 * Returns the meta object for the containment reference list '{@link ucm_supplement.ucm_deployments.DeploymentModule#getAllocationPlan <em>Allocation Plan</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Allocation Plan</em>'.
	 * @see ucm_supplement.ucm_deployments.DeploymentModule#getAllocationPlan()
	 * @see #getDeploymentModule()
	 * @generated
	 */
	EReference getDeploymentModule_AllocationPlan();

	/**
	 * Returns the meta object for the containment reference list '{@link ucm_supplement.ucm_deployments.DeploymentModule#getArtefact <em>Artefact</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Artefact</em>'.
	 * @see ucm_supplement.ucm_deployments.DeploymentModule#getArtefact()
	 * @see #getDeploymentModule()
	 * @generated
	 */
	EReference getDeploymentModule_Artefact();

	/**
	 * Returns the meta object for the containment reference list '{@link ucm_supplement.ucm_deployments.DeploymentModule#getTestCaseGroup <em>Test Case Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Test Case Group</em>'.
	 * @see ucm_supplement.ucm_deployments.DeploymentModule#getTestCaseGroup()
	 * @see #getDeploymentModule()
	 * @generated
	 */
	EReference getDeploymentModule_TestCaseGroup();

	/**
	 * Returns the meta object for the containment reference list '{@link ucm_supplement.ucm_deployments.DeploymentModule#getComponentInstance <em>Component Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Component Instance</em>'.
	 * @see ucm_supplement.ucm_deployments.DeploymentModule#getComponentInstance()
	 * @see #getDeploymentModule()
	 * @generated
	 */
	EReference getDeploymentModule_ComponentInstance();

	/**
	 * Returns the meta object for the containment reference list '{@link ucm_supplement.ucm_deployments.DeploymentModule#getConnectionInstance <em>Connection Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Connection Instance</em>'.
	 * @see ucm_supplement.ucm_deployments.DeploymentModule#getConnectionInstance()
	 * @see #getDeploymentModule()
	 * @generated
	 */
	EReference getDeploymentModule_ConnectionInstance();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_deployments.AppAssembly <em>App Assembly</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>App Assembly</em>'.
	 * @see ucm_supplement.ucm_deployments.AppAssembly
	 * @generated
	 */
	EClass getAppAssembly();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_deployments.AllocationPlan <em>Allocation Plan</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Allocation Plan</em>'.
	 * @see ucm_supplement.ucm_deployments.AllocationPlan
	 * @generated
	 */
	EClass getAllocationPlan();

	/**
	 * Returns the meta object for the containment reference list '{@link ucm_supplement.ucm_deployments.AllocationPlan#getNode <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Node</em>'.
	 * @see ucm_supplement.ucm_deployments.AllocationPlan#getNode()
	 * @see #getAllocationPlan()
	 * @generated
	 */
	EReference getAllocationPlan_Node();

	/**
	 * Returns the meta object for the containment reference list '{@link ucm_supplement.ucm_deployments.AllocationPlan#getComponentInstanceInitialization <em>Component Instance Initialization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Component Instance Initialization</em>'.
	 * @see ucm_supplement.ucm_deployments.AllocationPlan#getComponentInstanceInitialization()
	 * @see #getAllocationPlan()
	 * @generated
	 */
	EReference getAllocationPlan_ComponentInstanceInitialization();

	/**
	 * Returns the meta object for the containment reference list '{@link ucm_supplement.ucm_deployments.AllocationPlan#getConnectionConfiguration <em>Connection Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Connection Configuration</em>'.
	 * @see ucm_supplement.ucm_deployments.AllocationPlan#getConnectionConfiguration()
	 * @see #getAllocationPlan()
	 * @generated
	 */
	EReference getAllocationPlan_ConnectionConfiguration();

	/**
	 * Returns the meta object for the containment reference list '{@link ucm_supplement.ucm_deployments.AllocationPlan#getTechnicalPolicyConfiguration <em>Technical Policy Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Technical Policy Configuration</em>'.
	 * @see ucm_supplement.ucm_deployments.AllocationPlan#getTechnicalPolicyConfiguration()
	 * @see #getAllocationPlan()
	 * @generated
	 */
	EReference getAllocationPlan_TechnicalPolicyConfiguration();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_deployments.AllocationPlan#getRefines <em>Refines</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Refines</em>'.
	 * @see ucm_supplement.ucm_deployments.AllocationPlan#getRefines()
	 * @see #getAllocationPlan()
	 * @generated
	 */
	EReference getAllocationPlan_Refines();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_deployments.ComponentInstanceInitialization <em>Component Instance Initialization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component Instance Initialization</em>'.
	 * @see ucm_supplement.ucm_deployments.ComponentInstanceInitialization
	 * @generated
	 */
	EClass getComponentInstanceInitialization();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_deployments.ComponentInstanceInitialization#getComponentInstance <em>Component Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Component Instance</em>'.
	 * @see ucm_supplement.ucm_deployments.ComponentInstanceInitialization#getComponentInstance()
	 * @see #getComponentInstanceInitialization()
	 * @generated
	 */
	EReference getComponentInstanceInitialization_ComponentInstance();

	/**
	 * Returns the meta object for the containment reference list '{@link ucm_supplement.ucm_deployments.ComponentInstanceInitialization#getPortInitialization <em>Port Initialization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Port Initialization</em>'.
	 * @see ucm_supplement.ucm_deployments.ComponentInstanceInitialization#getPortInitialization()
	 * @see #getComponentInstanceInitialization()
	 * @generated
	 */
	EReference getComponentInstanceInitialization_PortInitialization();

	/**
	 * Returns the meta object for the containment reference list '{@link ucm_supplement.ucm_deployments.ComponentInstanceInitialization#getPolicyInitialization <em>Policy Initialization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Policy Initialization</em>'.
	 * @see ucm_supplement.ucm_deployments.ComponentInstanceInitialization#getPolicyInitialization()
	 * @see #getComponentInstanceInitialization()
	 * @generated
	 */
	EReference getComponentInstanceInitialization_PolicyInitialization();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_deployments.ComponentInstancePortInitialization <em>Component Instance Port Initialization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component Instance Port Initialization</em>'.
	 * @see ucm_supplement.ucm_deployments.ComponentInstancePortInitialization
	 * @generated
	 */
	EClass getComponentInstancePortInitialization();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_deployments.ComponentInstancePortInitialization#getPort <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Port</em>'.
	 * @see ucm_supplement.ucm_deployments.ComponentInstancePortInitialization#getPort()
	 * @see #getComponentInstancePortInitialization()
	 * @generated
	 */
	EReference getComponentInstancePortInitialization_Port();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_deployments.ComponentInstancePortInitialization#getPortElement <em>Port Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Port Element</em>'.
	 * @see ucm_supplement.ucm_deployments.ComponentInstancePortInitialization#getPortElement()
	 * @see #getComponentInstancePortInitialization()
	 * @generated
	 */
	EReference getComponentInstancePortInitialization_PortElement();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_deployments.ComponentInstancePolicyInitialization <em>Component Instance Policy Initialization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component Instance Policy Initialization</em>'.
	 * @see ucm_supplement.ucm_deployments.ComponentInstancePolicyInitialization
	 * @generated
	 */
	EClass getComponentInstancePolicyInitialization();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_deployments.ComponentInstancePolicyInitialization#getPortElement <em>Port Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Port Element</em>'.
	 * @see ucm_supplement.ucm_deployments.ComponentInstancePolicyInitialization#getPortElement()
	 * @see #getComponentInstancePolicyInitialization()
	 * @generated
	 */
	EReference getComponentInstancePolicyInitialization_PortElement();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_deployments.ComponentInstancePolicyInitialization#getPolicy <em>Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Policy</em>'.
	 * @see ucm_supplement.ucm_deployments.ComponentInstancePolicyInitialization#getPolicy()
	 * @see #getComponentInstancePolicyInitialization()
	 * @generated
	 */
	EReference getComponentInstancePolicyInitialization_Policy();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_deployments.AssemblyPartReference <em>Assembly Part Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Assembly Part Reference</em>'.
	 * @see ucm_supplement.ucm_deployments.AssemblyPartReference
	 * @generated
	 */
	EClass getAssemblyPartReference();

	/**
	 * Returns the meta object for the containment reference '{@link ucm_supplement.ucm_deployments.AssemblyPartReference#getReferenceToSubpart <em>Reference To Subpart</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Reference To Subpart</em>'.
	 * @see ucm_supplement.ucm_deployments.AssemblyPartReference#getReferenceToSubpart()
	 * @see #getAssemblyPartReference()
	 * @generated
	 */
	EReference getAssemblyPartReference_ReferenceToSubpart();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_deployments.AssemblyPartReference#getPart <em>Part</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Part</em>'.
	 * @see ucm_supplement.ucm_deployments.AssemblyPartReference#getPart()
	 * @see #getAssemblyPartReference()
	 * @generated
	 */
	EReference getAssemblyPartReference_Part();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_deployments.ComponentInstance <em>Component Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component Instance</em>'.
	 * @see ucm_supplement.ucm_deployments.ComponentInstance
	 * @generated
	 */
	EClass getComponentInstance();

	/**
	 * Returns the meta object for the containment reference '{@link ucm_supplement.ucm_deployments.ComponentInstance#getPartReference <em>Part Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Part Reference</em>'.
	 * @see ucm_supplement.ucm_deployments.ComponentInstance#getPartReference()
	 * @see #getComponentInstance()
	 * @generated
	 */
	EReference getComponentInstance_PartReference();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_deployments.ConnectionInstance <em>Connection Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Connection Instance</em>'.
	 * @see ucm_supplement.ucm_deployments.ConnectionInstance
	 * @generated
	 */
	EClass getConnectionInstance();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_deployments.ConnectionInstance#getConnection <em>Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Connection</em>'.
	 * @see ucm_supplement.ucm_deployments.ConnectionInstance#getConnection()
	 * @see #getConnectionInstance()
	 * @generated
	 */
	EReference getConnectionInstance_Connection();

	/**
	 * Returns the meta object for the containment reference '{@link ucm_supplement.ucm_deployments.ConnectionInstance#getPartReference <em>Part Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Part Reference</em>'.
	 * @see ucm_supplement.ucm_deployments.ConnectionInstance#getPartReference()
	 * @see #getConnectionInstance()
	 * @generated
	 */
	EReference getConnectionInstance_PartReference();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_deployments.ConnectionConfiguration <em>Connection Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Connection Configuration</em>'.
	 * @see ucm_supplement.ucm_deployments.ConnectionConfiguration
	 * @generated
	 */
	EClass getConnectionConfiguration();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_deployments.ConnectionConfiguration#getConnection <em>Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Connection</em>'.
	 * @see ucm_supplement.ucm_deployments.ConnectionConfiguration#getConnection()
	 * @see #getConnectionConfiguration()
	 * @generated
	 */
	EReference getConnectionConfiguration_Connection();

	/**
	 * Returns the meta object for the containment reference list '{@link ucm_supplement.ucm_deployments.ConnectionConfiguration#getEndConfiguration <em>End Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>End Configuration</em>'.
	 * @see ucm_supplement.ucm_deployments.ConnectionConfiguration#getEndConfiguration()
	 * @see #getConnectionConfiguration()
	 * @generated
	 */
	EReference getConnectionConfiguration_EndConfiguration();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_deployments.ConnectionConfiguration#getRefinedIn <em>Refined In</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Refined In</em>'.
	 * @see ucm_supplement.ucm_deployments.ConnectionConfiguration#getRefinedIn()
	 * @see #getConnectionConfiguration()
	 * @generated
	 */
	EReference getConnectionConfiguration_RefinedIn();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_deployments.ConnectionEndConfiguration <em>Connection End Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Connection End Configuration</em>'.
	 * @see ucm_supplement.ucm_deployments.ConnectionEndConfiguration
	 * @generated
	 */
	EClass getConnectionEndConfiguration();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_deployments.ConnectionEndConfiguration#getComponentInstance <em>Component Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Component Instance</em>'.
	 * @see ucm_supplement.ucm_deployments.ConnectionEndConfiguration#getComponentInstance()
	 * @see #getConnectionEndConfiguration()
	 * @generated
	 */
	EReference getConnectionEndConfiguration_ComponentInstance();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_deployments.ConnectionEndConfiguration#getPort <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Port</em>'.
	 * @see ucm_supplement.ucm_deployments.ConnectionEndConfiguration#getPort()
	 * @see #getConnectionEndConfiguration()
	 * @generated
	 */
	EReference getConnectionEndConfiguration_Port();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_deployments.TechnicalPolicyConfiguration <em>Technical Policy Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Technical Policy Configuration</em>'.
	 * @see ucm_supplement.ucm_deployments.TechnicalPolicyConfiguration
	 * @generated
	 */
	EClass getTechnicalPolicyConfiguration();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_deployments.TechnicalPolicyConfiguration#getPolicy <em>Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Policy</em>'.
	 * @see ucm_supplement.ucm_deployments.TechnicalPolicyConfiguration#getPolicy()
	 * @see #getTechnicalPolicyConfiguration()
	 * @generated
	 */
	EReference getTechnicalPolicyConfiguration_Policy();

	/**
	 * Returns the meta object for the reference list '{@link ucm_supplement.ucm_deployments.TechnicalPolicyConfiguration#getComponentInstance <em>Component Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Component Instance</em>'.
	 * @see ucm_supplement.ucm_deployments.TechnicalPolicyConfiguration#getComponentInstance()
	 * @see #getTechnicalPolicyConfiguration()
	 * @generated
	 */
	EReference getTechnicalPolicyConfiguration_ComponentInstance();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_deployments.TechnicalPolicyConfiguration#getRefinedIn <em>Refined In</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Refined In</em>'.
	 * @see ucm_supplement.ucm_deployments.TechnicalPolicyConfiguration#getRefinedIn()
	 * @see #getTechnicalPolicyConfiguration()
	 * @generated
	 */
	EReference getTechnicalPolicyConfiguration_RefinedIn();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_deployments.Artefact <em>Artefact</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Artefact</em>'.
	 * @see ucm_supplement.ucm_deployments.Artefact
	 * @generated
	 */
	EClass getArtefact();

	/**
	 * Returns the meta object for the reference list '{@link ucm_supplement.ucm_deployments.Artefact#getComponentImplementation <em>Component Implementation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Component Implementation</em>'.
	 * @see ucm_supplement.ucm_deployments.Artefact#getComponentImplementation()
	 * @see #getArtefact()
	 * @generated
	 */
	EReference getArtefact_ComponentImplementation();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_deployments.IComponentNode <em>IComponent Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IComponent Node</em>'.
	 * @see ucm_supplement.ucm_deployments.IComponentNode
	 * @generated
	 */
	EClass getIComponentNode();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_deployments.IComponentNode#getLanguage <em>Language</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Language</em>'.
	 * @see ucm_supplement.ucm_deployments.IComponentNode#getLanguage()
	 * @see #getIComponentNode()
	 * @generated
	 */
	EReference getIComponentNode_Language();

	/**
	 * Returns the meta object for the reference list '{@link ucm_supplement.ucm_deployments.IComponentNode#getComponentInstance <em>Component Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Component Instance</em>'.
	 * @see ucm_supplement.ucm_deployments.IComponentNode#getComponentInstance()
	 * @see #getIComponentNode()
	 * @generated
	 */
	EReference getIComponentNode_ComponentInstance();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_deployments.IHasAttributeInitialization <em>IHas Attribute Initialization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IHas Attribute Initialization</em>'.
	 * @see ucm_supplement.ucm_deployments.IHasAttributeInitialization
	 * @generated
	 */
	EClass getIHasAttributeInitialization();

	/**
	 * Returns the meta object for the containment reference list '{@link ucm_supplement.ucm_deployments.IHasAttributeInitialization#getAttributeInitialization <em>Attribute Initialization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Attribute Initialization</em>'.
	 * @see ucm_supplement.ucm_deployments.IHasAttributeInitialization#getAttributeInitialization()
	 * @see #getIHasAttributeInitialization()
	 * @generated
	 */
	EReference getIHasAttributeInitialization_AttributeInitialization();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_deployments.AttributeValue <em>Attribute Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attribute Value</em>'.
	 * @see ucm_supplement.ucm_deployments.AttributeValue
	 * @generated
	 */
	EClass getAttributeValue();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_deployments.AttributeValue#getAttribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attribute</em>'.
	 * @see ucm_supplement.ucm_deployments.AttributeValue#getAttribute()
	 * @see #getAttributeValue()
	 * @generated
	 */
	EReference getAttributeValue_Attribute();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	Ucm_deploymentsFactory getUcm_deploymentsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_deployments.impl.DeploymentModuleImpl <em>Deployment Module</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_deployments.impl.DeploymentModuleImpl
		 * @see ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl#getDeploymentModule()
		 * @generated
		 */
		EClass DEPLOYMENT_MODULE = eINSTANCE.getDeploymentModule();

		/**
		 * The meta object literal for the '<em><b>Assembly</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPLOYMENT_MODULE__ASSEMBLY = eINSTANCE.getDeploymentModule_Assembly();

		/**
		 * The meta object literal for the '<em><b>Allocation Plan</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPLOYMENT_MODULE__ALLOCATION_PLAN = eINSTANCE.getDeploymentModule_AllocationPlan();

		/**
		 * The meta object literal for the '<em><b>Artefact</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPLOYMENT_MODULE__ARTEFACT = eINSTANCE.getDeploymentModule_Artefact();

		/**
		 * The meta object literal for the '<em><b>Test Case Group</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPLOYMENT_MODULE__TEST_CASE_GROUP = eINSTANCE.getDeploymentModule_TestCaseGroup();

		/**
		 * The meta object literal for the '<em><b>Component Instance</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPLOYMENT_MODULE__COMPONENT_INSTANCE = eINSTANCE.getDeploymentModule_ComponentInstance();

		/**
		 * The meta object literal for the '<em><b>Connection Instance</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPLOYMENT_MODULE__CONNECTION_INSTANCE = eINSTANCE.getDeploymentModule_ConnectionInstance();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_deployments.impl.AppAssemblyImpl <em>App Assembly</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_deployments.impl.AppAssemblyImpl
		 * @see ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl#getAppAssembly()
		 * @generated
		 */
		EClass APP_ASSEMBLY = eINSTANCE.getAppAssembly();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_deployments.impl.AllocationPlanImpl <em>Allocation Plan</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_deployments.impl.AllocationPlanImpl
		 * @see ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl#getAllocationPlan()
		 * @generated
		 */
		EClass ALLOCATION_PLAN = eINSTANCE.getAllocationPlan();

		/**
		 * The meta object literal for the '<em><b>Node</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ALLOCATION_PLAN__NODE = eINSTANCE.getAllocationPlan_Node();

		/**
		 * The meta object literal for the '<em><b>Component Instance Initialization</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ALLOCATION_PLAN__COMPONENT_INSTANCE_INITIALIZATION = eINSTANCE.getAllocationPlan_ComponentInstanceInitialization();

		/**
		 * The meta object literal for the '<em><b>Connection Configuration</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ALLOCATION_PLAN__CONNECTION_CONFIGURATION = eINSTANCE.getAllocationPlan_ConnectionConfiguration();

		/**
		 * The meta object literal for the '<em><b>Technical Policy Configuration</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ALLOCATION_PLAN__TECHNICAL_POLICY_CONFIGURATION = eINSTANCE.getAllocationPlan_TechnicalPolicyConfiguration();

		/**
		 * The meta object literal for the '<em><b>Refines</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ALLOCATION_PLAN__REFINES = eINSTANCE.getAllocationPlan_Refines();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_deployments.impl.ComponentInstanceInitializationImpl <em>Component Instance Initialization</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_deployments.impl.ComponentInstanceInitializationImpl
		 * @see ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl#getComponentInstanceInitialization()
		 * @generated
		 */
		EClass COMPONENT_INSTANCE_INITIALIZATION = eINSTANCE.getComponentInstanceInitialization();

		/**
		 * The meta object literal for the '<em><b>Component Instance</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_INSTANCE_INITIALIZATION__COMPONENT_INSTANCE = eINSTANCE.getComponentInstanceInitialization_ComponentInstance();

		/**
		 * The meta object literal for the '<em><b>Port Initialization</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_INSTANCE_INITIALIZATION__PORT_INITIALIZATION = eINSTANCE.getComponentInstanceInitialization_PortInitialization();

		/**
		 * The meta object literal for the '<em><b>Policy Initialization</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_INSTANCE_INITIALIZATION__POLICY_INITIALIZATION = eINSTANCE.getComponentInstanceInitialization_PolicyInitialization();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_deployments.impl.ComponentInstancePortInitializationImpl <em>Component Instance Port Initialization</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_deployments.impl.ComponentInstancePortInitializationImpl
		 * @see ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl#getComponentInstancePortInitialization()
		 * @generated
		 */
		EClass COMPONENT_INSTANCE_PORT_INITIALIZATION = eINSTANCE.getComponentInstancePortInitialization();

		/**
		 * The meta object literal for the '<em><b>Port</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_INSTANCE_PORT_INITIALIZATION__PORT = eINSTANCE.getComponentInstancePortInitialization_Port();

		/**
		 * The meta object literal for the '<em><b>Port Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_INSTANCE_PORT_INITIALIZATION__PORT_ELEMENT = eINSTANCE.getComponentInstancePortInitialization_PortElement();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_deployments.impl.ComponentInstancePolicyInitializationImpl <em>Component Instance Policy Initialization</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_deployments.impl.ComponentInstancePolicyInitializationImpl
		 * @see ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl#getComponentInstancePolicyInitialization()
		 * @generated
		 */
		EClass COMPONENT_INSTANCE_POLICY_INITIALIZATION = eINSTANCE.getComponentInstancePolicyInitialization();

		/**
		 * The meta object literal for the '<em><b>Port Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_INSTANCE_POLICY_INITIALIZATION__PORT_ELEMENT = eINSTANCE.getComponentInstancePolicyInitialization_PortElement();

		/**
		 * The meta object literal for the '<em><b>Policy</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_INSTANCE_POLICY_INITIALIZATION__POLICY = eINSTANCE.getComponentInstancePolicyInitialization_Policy();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_deployments.impl.AssemblyPartReferenceImpl <em>Assembly Part Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_deployments.impl.AssemblyPartReferenceImpl
		 * @see ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl#getAssemblyPartReference()
		 * @generated
		 */
		EClass ASSEMBLY_PART_REFERENCE = eINSTANCE.getAssemblyPartReference();

		/**
		 * The meta object literal for the '<em><b>Reference To Subpart</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSEMBLY_PART_REFERENCE__REFERENCE_TO_SUBPART = eINSTANCE.getAssemblyPartReference_ReferenceToSubpart();

		/**
		 * The meta object literal for the '<em><b>Part</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSEMBLY_PART_REFERENCE__PART = eINSTANCE.getAssemblyPartReference_Part();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_deployments.impl.ComponentInstanceImpl <em>Component Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_deployments.impl.ComponentInstanceImpl
		 * @see ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl#getComponentInstance()
		 * @generated
		 */
		EClass COMPONENT_INSTANCE = eINSTANCE.getComponentInstance();

		/**
		 * The meta object literal for the '<em><b>Part Reference</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_INSTANCE__PART_REFERENCE = eINSTANCE.getComponentInstance_PartReference();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_deployments.impl.ConnectionInstanceImpl <em>Connection Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_deployments.impl.ConnectionInstanceImpl
		 * @see ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl#getConnectionInstance()
		 * @generated
		 */
		EClass CONNECTION_INSTANCE = eINSTANCE.getConnectionInstance();

		/**
		 * The meta object literal for the '<em><b>Connection</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_INSTANCE__CONNECTION = eINSTANCE.getConnectionInstance_Connection();

		/**
		 * The meta object literal for the '<em><b>Part Reference</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_INSTANCE__PART_REFERENCE = eINSTANCE.getConnectionInstance_PartReference();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_deployments.impl.ConnectionConfigurationImpl <em>Connection Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_deployments.impl.ConnectionConfigurationImpl
		 * @see ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl#getConnectionConfiguration()
		 * @generated
		 */
		EClass CONNECTION_CONFIGURATION = eINSTANCE.getConnectionConfiguration();

		/**
		 * The meta object literal for the '<em><b>Connection</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_CONFIGURATION__CONNECTION = eINSTANCE.getConnectionConfiguration_Connection();

		/**
		 * The meta object literal for the '<em><b>End Configuration</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_CONFIGURATION__END_CONFIGURATION = eINSTANCE.getConnectionConfiguration_EndConfiguration();

		/**
		 * The meta object literal for the '<em><b>Refined In</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_CONFIGURATION__REFINED_IN = eINSTANCE.getConnectionConfiguration_RefinedIn();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_deployments.impl.ConnectionEndConfigurationImpl <em>Connection End Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_deployments.impl.ConnectionEndConfigurationImpl
		 * @see ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl#getConnectionEndConfiguration()
		 * @generated
		 */
		EClass CONNECTION_END_CONFIGURATION = eINSTANCE.getConnectionEndConfiguration();

		/**
		 * The meta object literal for the '<em><b>Component Instance</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_END_CONFIGURATION__COMPONENT_INSTANCE = eINSTANCE.getConnectionEndConfiguration_ComponentInstance();

		/**
		 * The meta object literal for the '<em><b>Port</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_END_CONFIGURATION__PORT = eINSTANCE.getConnectionEndConfiguration_Port();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_deployments.impl.TechnicalPolicyConfigurationImpl <em>Technical Policy Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_deployments.impl.TechnicalPolicyConfigurationImpl
		 * @see ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl#getTechnicalPolicyConfiguration()
		 * @generated
		 */
		EClass TECHNICAL_POLICY_CONFIGURATION = eINSTANCE.getTechnicalPolicyConfiguration();

		/**
		 * The meta object literal for the '<em><b>Policy</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TECHNICAL_POLICY_CONFIGURATION__POLICY = eINSTANCE.getTechnicalPolicyConfiguration_Policy();

		/**
		 * The meta object literal for the '<em><b>Component Instance</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TECHNICAL_POLICY_CONFIGURATION__COMPONENT_INSTANCE = eINSTANCE.getTechnicalPolicyConfiguration_ComponentInstance();

		/**
		 * The meta object literal for the '<em><b>Refined In</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TECHNICAL_POLICY_CONFIGURATION__REFINED_IN = eINSTANCE.getTechnicalPolicyConfiguration_RefinedIn();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_deployments.impl.ArtefactImpl <em>Artefact</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_deployments.impl.ArtefactImpl
		 * @see ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl#getArtefact()
		 * @generated
		 */
		EClass ARTEFACT = eINSTANCE.getArtefact();

		/**
		 * The meta object literal for the '<em><b>Component Implementation</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARTEFACT__COMPONENT_IMPLEMENTATION = eINSTANCE.getArtefact_ComponentImplementation();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_deployments.impl.IComponentNodeImpl <em>IComponent Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_deployments.impl.IComponentNodeImpl
		 * @see ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl#getIComponentNode()
		 * @generated
		 */
		EClass ICOMPONENT_NODE = eINSTANCE.getIComponentNode();

		/**
		 * The meta object literal for the '<em><b>Language</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ICOMPONENT_NODE__LANGUAGE = eINSTANCE.getIComponentNode_Language();

		/**
		 * The meta object literal for the '<em><b>Component Instance</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ICOMPONENT_NODE__COMPONENT_INSTANCE = eINSTANCE.getIComponentNode_ComponentInstance();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_deployments.impl.IHasAttributeInitializationImpl <em>IHas Attribute Initialization</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_deployments.impl.IHasAttributeInitializationImpl
		 * @see ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl#getIHasAttributeInitialization()
		 * @generated
		 */
		EClass IHAS_ATTRIBUTE_INITIALIZATION = eINSTANCE.getIHasAttributeInitialization();

		/**
		 * The meta object literal for the '<em><b>Attribute Initialization</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IHAS_ATTRIBUTE_INITIALIZATION__ATTRIBUTE_INITIALIZATION = eINSTANCE.getIHasAttributeInitialization_AttributeInitialization();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_deployments.impl.AttributeValueImpl <em>Attribute Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_deployments.impl.AttributeValueImpl
		 * @see ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl#getAttributeValue()
		 * @generated
		 */
		EClass ATTRIBUTE_VALUE = eINSTANCE.getAttributeValue();

		/**
		 * The meta object literal for the '<em><b>Attribute</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTRIBUTE_VALUE__ATTRIBUTE = eINSTANCE.getAttributeValue_Attribute();

	}

} //Ucm_deploymentsPackage
