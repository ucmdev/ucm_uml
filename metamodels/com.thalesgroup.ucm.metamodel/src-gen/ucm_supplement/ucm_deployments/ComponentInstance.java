/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_deployments;

import ucm_base.ucm_commons.INamed;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_deployments.ComponentInstance#getPartReference <em>Part Reference</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getComponentInstance()
 * @model
 * @generated
 */
public interface ComponentInstance extends INamed {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Part Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Part Reference</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Part Reference</em>' containment reference.
	 * @see #setPartReference(AssemblyPartReference)
	 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getComponentInstance_PartReference()
	 * @model containment="true" required="true"
	 * @generated
	 */
	AssemblyPartReference getPartReference();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_deployments.ComponentInstance#getPartReference <em>Part Reference</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Part Reference</em>' containment reference.
	 * @see #getPartReference()
	 * @generated
	 */
	void setPartReference(AssemblyPartReference value);

} // ComponentInstance
