/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_deployments;

import java.util.Map;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import ucm_base.ucm_commons.IApplicationModule;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Allocation Plan</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_deployments.AllocationPlan#getNode <em>Node</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.AllocationPlan#getComponentInstanceInitialization <em>Component Instance Initialization</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.AllocationPlan#getConnectionConfiguration <em>Connection Configuration</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.AllocationPlan#getTechnicalPolicyConfiguration <em>Technical Policy Configuration</em>}</li>
 *   <li>{@link ucm_supplement.ucm_deployments.AllocationPlan#getRefines <em>Refines</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getAllocationPlan()
 * @model
 * @generated
 */
public interface AllocationPlan extends IApplicationModule {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Node</b></em>' containment reference list.
	 * The list contents are of type {@link ucm_supplement.ucm_deployments.IComponentNode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Node</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Node</em>' containment reference list.
	 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getAllocationPlan_Node()
	 * @model containment="true"
	 * @generated
	 */
	EList<IComponentNode> getNode();

	/**
	 * Returns the value of the '<em><b>Component Instance Initialization</b></em>' containment reference list.
	 * The list contents are of type {@link ucm_supplement.ucm_deployments.ComponentInstanceInitialization}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Instance Initialization</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Instance Initialization</em>' containment reference list.
	 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getAllocationPlan_ComponentInstanceInitialization()
	 * @model containment="true"
	 * @generated
	 */
	EList<ComponentInstanceInitialization> getComponentInstanceInitialization();

	/**
	 * Returns the value of the '<em><b>Connection Configuration</b></em>' containment reference list.
	 * The list contents are of type {@link ucm_supplement.ucm_deployments.ConnectionConfiguration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connection Configuration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connection Configuration</em>' containment reference list.
	 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getAllocationPlan_ConnectionConfiguration()
	 * @model containment="true"
	 * @generated
	 */
	EList<ConnectionConfiguration> getConnectionConfiguration();

	/**
	 * Returns the value of the '<em><b>Technical Policy Configuration</b></em>' containment reference list.
	 * The list contents are of type {@link ucm_supplement.ucm_deployments.TechnicalPolicyConfiguration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Technical Policy Configuration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Technical Policy Configuration</em>' containment reference list.
	 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getAllocationPlan_TechnicalPolicyConfiguration()
	 * @model containment="true"
	 * @generated
	 */
	EList<TechnicalPolicyConfiguration> getTechnicalPolicyConfiguration();

	/**
	 * Returns the value of the '<em><b>Refines</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Refines</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Refines</em>' reference.
	 * @see #setRefines(AllocationPlan)
	 * @see ucm_supplement.ucm_deployments.Ucm_deploymentsPackage#getAllocationPlan_Refines()
	 * @model
	 * @generated
	 */
	AllocationPlan getRefines();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_deployments.AllocationPlan#getRefines <em>Refines</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Refines</em>' reference.
	 * @see #getRefines()
	 * @generated
	 */
	void setRefines(AllocationPlan value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean configurationIsComplete(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean initializationIsComplete(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean instancesBelongToTheSameDeployment(DiagnosticChain diagnostics, Map<Object, Object> context);

} // AllocationPlan
