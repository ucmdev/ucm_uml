/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_enhanced_portspec;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import ucm_base.ucm_components.InteractionItemBinding;

import ucm_base.ucm_contracts.ucm_datatypes.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interaction Item Binding Set</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_enhanced_portspec.InteractionItemBindingSet#getBinding <em>Binding</em>}</li>
 *   <li>{@link ucm_supplement.ucm_enhanced_portspec.InteractionItemBindingSet#getBindingName <em>Binding Name</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_enhanced_portspec.Ucm_enhanced_portspecPackage#getInteractionItemBindingSet()
 * @model
 * @generated
 */
public interface InteractionItemBindingSet extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Binding</b></em>' containment reference list.
	 * The list contents are of type {@link ucm_base.ucm_components.InteractionItemBinding}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Binding</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Binding</em>' containment reference list.
	 * @see ucm_supplement.ucm_enhanced_portspec.Ucm_enhanced_portspecPackage#getInteractionItemBindingSet_Binding()
	 * @model containment="true"
	 * @generated
	 */
	EList<InteractionItemBinding> getBinding();

	/**
	 * Returns the value of the '<em><b>Binding Name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Binding Name</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Binding Name</em>' reference.
	 * @see #setBindingName(Enumerator)
	 * @see ucm_supplement.ucm_enhanced_portspec.Ucm_enhanced_portspecPackage#getInteractionItemBindingSet_BindingName()
	 * @model
	 * @generated
	 */
	Enumerator getBindingName();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_enhanced_portspec.InteractionItemBindingSet#getBindingName <em>Binding Name</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Binding Name</em>' reference.
	 * @see #getBindingName()
	 * @generated
	 */
	void setBindingName(Enumerator value);

} // InteractionItemBindingSet
