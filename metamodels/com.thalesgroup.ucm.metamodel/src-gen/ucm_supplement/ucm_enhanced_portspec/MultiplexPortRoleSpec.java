/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_enhanced_portspec;

import org.eclipse.emf.common.util.EList;

import ucm_base.ucm_components.IPortSpec;

import ucm_base.ucm_contracts.ucm_datatypes.Enumeration;

import ucm_base.ucm_interactions.InteractionRole;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Multiplex Port Role Spec</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_enhanced_portspec.MultiplexPortRoleSpec#getRole <em>Role</em>}</li>
 *   <li>{@link ucm_supplement.ucm_enhanced_portspec.MultiplexPortRoleSpec#getBindingSet <em>Binding Set</em>}</li>
 *   <li>{@link ucm_supplement.ucm_enhanced_portspec.MultiplexPortRoleSpec#getIndexType <em>Index Type</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_enhanced_portspec.Ucm_enhanced_portspecPackage#getMultiplexPortRoleSpec()
 * @model
 * @generated
 */
public interface MultiplexPortRoleSpec extends IPortSpec {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Role</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role</em>' reference.
	 * @see #setRole(InteractionRole)
	 * @see ucm_supplement.ucm_enhanced_portspec.Ucm_enhanced_portspecPackage#getMultiplexPortRoleSpec_Role()
	 * @model required="true"
	 * @generated
	 */
	InteractionRole getRole();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_enhanced_portspec.MultiplexPortRoleSpec#getRole <em>Role</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Role</em>' reference.
	 * @see #getRole()
	 * @generated
	 */
	void setRole(InteractionRole value);

	/**
	 * Returns the value of the '<em><b>Binding Set</b></em>' containment reference list.
	 * The list contents are of type {@link ucm_supplement.ucm_enhanced_portspec.InteractionItemBindingSet}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Binding Set</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Binding Set</em>' containment reference list.
	 * @see ucm_supplement.ucm_enhanced_portspec.Ucm_enhanced_portspecPackage#getMultiplexPortRoleSpec_BindingSet()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<InteractionItemBindingSet> getBindingSet();

	/**
	 * Returns the value of the '<em><b>Index Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Index Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Index Type</em>' reference.
	 * @see #setIndexType(Enumeration)
	 * @see ucm_supplement.ucm_enhanced_portspec.Ucm_enhanced_portspecPackage#getMultiplexPortRoleSpec_IndexType()
	 * @model required="true"
	 * @generated
	 */
	Enumeration getIndexType();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_enhanced_portspec.MultiplexPortRoleSpec#getIndexType <em>Index Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Index Type</em>' reference.
	 * @see #getIndexType()
	 * @generated
	 */
	void setIndexType(Enumeration value);

} // MultiplexPortRoleSpec
