/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_enhanced_portspec;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see ucm_supplement.ucm_enhanced_portspec.Ucm_enhanced_portspecPackage
 * @generated
 */
public interface Ucm_enhanced_portspecFactory extends EFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Ucm_enhanced_portspecFactory eINSTANCE = ucm_supplement.ucm_enhanced_portspec.impl.Ucm_enhanced_portspecFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Multiplex Port Type Spec</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Multiplex Port Type Spec</em>'.
	 * @generated
	 */
	MultiplexPortTypeSpec createMultiplexPortTypeSpec();

	/**
	 * Returns a new object of class '<em>Abstract Type Binding Set</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Abstract Type Binding Set</em>'.
	 * @generated
	 */
	AbstractTypeBindingSet createAbstractTypeBindingSet();

	/**
	 * Returns a new object of class '<em>Multiplex Port Delegation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Multiplex Port Delegation</em>'.
	 * @generated
	 */
	MultiplexPortDelegation createMultiplexPortDelegation();

	/**
	 * Returns a new object of class '<em>Multiplex Port Role Spec</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Multiplex Port Role Spec</em>'.
	 * @generated
	 */
	MultiplexPortRoleSpec createMultiplexPortRoleSpec();

	/**
	 * Returns a new object of class '<em>Interaction Item Binding Set</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Interaction Item Binding Set</em>'.
	 * @generated
	 */
	InteractionItemBindingSet createInteractionItemBindingSet();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	Ucm_enhanced_portspecPackage getUcm_enhanced_portspecPackage();

} //Ucm_enhanced_portspecFactory
