/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_enhanced_portspec.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import ucm_base.ucm_commons.Ucm_commonsPackage;

import ucm_base.ucm_components.Ucm_componentsPackage;

import ucm_base.ucm_contracts.Ucm_contractsPackage;

import ucm_base.ucm_contracts.ucm_datatypes.Ucm_datatypesPackage;

import ucm_base.ucm_interactions.Ucm_interactionsPackage;

import ucm_base.ucm_technicalpolicies.Ucm_technicalpoliciesPackage;

import ucm_basic_projects.Ucm_basic_projectsPackage;

import ucm_environment.ucm_resources.Ucm_resourcesPackage;

import ucm_environment.ucm_resources.impl.Ucm_resourcesPackageImpl;

import ucm_extended_projects.Ucm_extended_projectsPackage;

import ucm_extended_projects.impl.Ucm_extended_projectsPackageImpl;

import ucm_supplement.ucm_deployments.Ucm_deploymentsPackage;

import ucm_supplement.ucm_deployments.impl.Ucm_deploymentsPackageImpl;

import ucm_supplement.ucm_detailed_components.Ucm_detailed_componentsPackage;

import ucm_supplement.ucm_detailed_components.impl.Ucm_detailed_componentsPackageImpl;

import ucm_supplement.ucm_enhanced_configuration.Ucm_enhanced_configurationPackage;

import ucm_supplement.ucm_enhanced_configuration.impl.Ucm_enhanced_configurationPackageImpl;

import ucm_supplement.ucm_enhanced_portspec.AbstractTypeBindingSet;
import ucm_supplement.ucm_enhanced_portspec.InteractionItemBindingSet;
import ucm_supplement.ucm_enhanced_portspec.MultiplexPortDelegation;
import ucm_supplement.ucm_enhanced_portspec.MultiplexPortRoleSpec;
import ucm_supplement.ucm_enhanced_portspec.MultiplexPortTypeSpec;
import ucm_supplement.ucm_enhanced_portspec.Ucm_enhanced_portspecFactory;
import ucm_supplement.ucm_enhanced_portspec.Ucm_enhanced_portspecPackage;

import ucm_supplement.ucm_test_cases.Ucm_test_casesPackage;

import ucm_supplement.ucm_test_cases.impl.Ucm_test_casesPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Ucm_enhanced_portspecPackageImpl extends EPackageImpl implements Ucm_enhanced_portspecPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass multiplexPortTypeSpecEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractTypeBindingSetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass multiplexPortDelegationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass multiplexPortRoleSpecEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass interactionItemBindingSetEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ucm_supplement.ucm_enhanced_portspec.Ucm_enhanced_portspecPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private Ucm_enhanced_portspecPackageImpl() {
		super(eNS_URI, Ucm_enhanced_portspecFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link Ucm_enhanced_portspecPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static Ucm_enhanced_portspecPackage init() {
		if (isInited) return (Ucm_enhanced_portspecPackage)EPackage.Registry.INSTANCE.getEPackage(Ucm_enhanced_portspecPackage.eNS_URI);

		// Obtain or create and register package
		Ucm_enhanced_portspecPackageImpl theUcm_enhanced_portspecPackage = (Ucm_enhanced_portspecPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof Ucm_enhanced_portspecPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new Ucm_enhanced_portspecPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		Ucm_basic_projectsPackage.eINSTANCE.eClass();
		Ucm_commonsPackage.eINSTANCE.eClass();
		Ucm_technicalpoliciesPackage.eINSTANCE.eClass();
		Ucm_contractsPackage.eINSTANCE.eClass();
		Ucm_interactionsPackage.eINSTANCE.eClass();
		Ucm_componentsPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Ucm_extended_projectsPackageImpl theUcm_extended_projectsPackage = (Ucm_extended_projectsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_extended_projectsPackage.eNS_URI) instanceof Ucm_extended_projectsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_extended_projectsPackage.eNS_URI) : Ucm_extended_projectsPackage.eINSTANCE);
		Ucm_detailed_componentsPackageImpl theUcm_detailed_componentsPackage = (Ucm_detailed_componentsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_detailed_componentsPackage.eNS_URI) instanceof Ucm_detailed_componentsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_detailed_componentsPackage.eNS_URI) : Ucm_detailed_componentsPackage.eINSTANCE);
		Ucm_deploymentsPackageImpl theUcm_deploymentsPackage = (Ucm_deploymentsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_deploymentsPackage.eNS_URI) instanceof Ucm_deploymentsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_deploymentsPackage.eNS_URI) : Ucm_deploymentsPackage.eINSTANCE);
		Ucm_test_casesPackageImpl theUcm_test_casesPackage = (Ucm_test_casesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_test_casesPackage.eNS_URI) instanceof Ucm_test_casesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_test_casesPackage.eNS_URI) : Ucm_test_casesPackage.eINSTANCE);
		Ucm_enhanced_configurationPackageImpl theUcm_enhanced_configurationPackage = (Ucm_enhanced_configurationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_enhanced_configurationPackage.eNS_URI) instanceof Ucm_enhanced_configurationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_enhanced_configurationPackage.eNS_URI) : Ucm_enhanced_configurationPackage.eINSTANCE);
		Ucm_resourcesPackageImpl theUcm_resourcesPackage = (Ucm_resourcesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Ucm_resourcesPackage.eNS_URI) instanceof Ucm_resourcesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Ucm_resourcesPackage.eNS_URI) : Ucm_resourcesPackage.eINSTANCE);

		// Create package meta-data objects
		theUcm_enhanced_portspecPackage.createPackageContents();
		theUcm_extended_projectsPackage.createPackageContents();
		theUcm_detailed_componentsPackage.createPackageContents();
		theUcm_deploymentsPackage.createPackageContents();
		theUcm_test_casesPackage.createPackageContents();
		theUcm_enhanced_configurationPackage.createPackageContents();
		theUcm_resourcesPackage.createPackageContents();

		// Initialize created meta-data
		theUcm_enhanced_portspecPackage.initializePackageContents();
		theUcm_extended_projectsPackage.initializePackageContents();
		theUcm_detailed_componentsPackage.initializePackageContents();
		theUcm_deploymentsPackage.initializePackageContents();
		theUcm_test_casesPackage.initializePackageContents();
		theUcm_enhanced_configurationPackage.initializePackageContents();
		theUcm_resourcesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theUcm_enhanced_portspecPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(Ucm_enhanced_portspecPackage.eNS_URI, theUcm_enhanced_portspecPackage);
		return theUcm_enhanced_portspecPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMultiplexPortTypeSpec() {
		return multiplexPortTypeSpecEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMultiplexPortTypeSpec_Type() {
		return (EReference)multiplexPortTypeSpecEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMultiplexPortTypeSpec_IndexType() {
		return (EReference)multiplexPortTypeSpecEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMultiplexPortTypeSpec_BindingSet() {
		return (EReference)multiplexPortTypeSpecEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractTypeBindingSet() {
		return abstractTypeBindingSetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractTypeBindingSet_Binding() {
		return (EReference)abstractTypeBindingSetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractTypeBindingSet_BindingName() {
		return (EReference)abstractTypeBindingSetEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMultiplexPortDelegation() {
		return multiplexPortDelegationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMultiplexPortDelegation_ExternalBindingName() {
		return (EReference)multiplexPortDelegationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMultiplexPortDelegation_BindingName() {
		return (EReference)multiplexPortDelegationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMultiplexPortRoleSpec() {
		return multiplexPortRoleSpecEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMultiplexPortRoleSpec_Role() {
		return (EReference)multiplexPortRoleSpecEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMultiplexPortRoleSpec_BindingSet() {
		return (EReference)multiplexPortRoleSpecEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMultiplexPortRoleSpec_IndexType() {
		return (EReference)multiplexPortRoleSpecEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInteractionItemBindingSet() {
		return interactionItemBindingSetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInteractionItemBindingSet_Binding() {
		return (EReference)interactionItemBindingSetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInteractionItemBindingSet_BindingName() {
		return (EReference)interactionItemBindingSetEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ucm_enhanced_portspecFactory getUcm_enhanced_portspecFactory() {
		return (Ucm_enhanced_portspecFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		multiplexPortTypeSpecEClass = createEClass(MULTIPLEX_PORT_TYPE_SPEC);
		createEReference(multiplexPortTypeSpecEClass, MULTIPLEX_PORT_TYPE_SPEC__TYPE);
		createEReference(multiplexPortTypeSpecEClass, MULTIPLEX_PORT_TYPE_SPEC__INDEX_TYPE);
		createEReference(multiplexPortTypeSpecEClass, MULTIPLEX_PORT_TYPE_SPEC__BINDING_SET);

		abstractTypeBindingSetEClass = createEClass(ABSTRACT_TYPE_BINDING_SET);
		createEReference(abstractTypeBindingSetEClass, ABSTRACT_TYPE_BINDING_SET__BINDING);
		createEReference(abstractTypeBindingSetEClass, ABSTRACT_TYPE_BINDING_SET__BINDING_NAME);

		multiplexPortDelegationEClass = createEClass(MULTIPLEX_PORT_DELEGATION);
		createEReference(multiplexPortDelegationEClass, MULTIPLEX_PORT_DELEGATION__EXTERNAL_BINDING_NAME);
		createEReference(multiplexPortDelegationEClass, MULTIPLEX_PORT_DELEGATION__BINDING_NAME);

		multiplexPortRoleSpecEClass = createEClass(MULTIPLEX_PORT_ROLE_SPEC);
		createEReference(multiplexPortRoleSpecEClass, MULTIPLEX_PORT_ROLE_SPEC__ROLE);
		createEReference(multiplexPortRoleSpecEClass, MULTIPLEX_PORT_ROLE_SPEC__BINDING_SET);
		createEReference(multiplexPortRoleSpecEClass, MULTIPLEX_PORT_ROLE_SPEC__INDEX_TYPE);

		interactionItemBindingSetEClass = createEClass(INTERACTION_ITEM_BINDING_SET);
		createEReference(interactionItemBindingSetEClass, INTERACTION_ITEM_BINDING_SET__BINDING);
		createEReference(interactionItemBindingSetEClass, INTERACTION_ITEM_BINDING_SET__BINDING_NAME);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		Ucm_componentsPackage theUcm_componentsPackage = (Ucm_componentsPackage)EPackage.Registry.INSTANCE.getEPackage(Ucm_componentsPackage.eNS_URI);
		Ucm_interactionsPackage theUcm_interactionsPackage = (Ucm_interactionsPackage)EPackage.Registry.INSTANCE.getEPackage(Ucm_interactionsPackage.eNS_URI);
		Ucm_datatypesPackage theUcm_datatypesPackage = (Ucm_datatypesPackage)EPackage.Registry.INSTANCE.getEPackage(Ucm_datatypesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		multiplexPortTypeSpecEClass.getESuperTypes().add(theUcm_componentsPackage.getIPortSpec());
		multiplexPortDelegationEClass.getESuperTypes().add(theUcm_componentsPackage.getPortDelegation());
		multiplexPortRoleSpecEClass.getESuperTypes().add(theUcm_componentsPackage.getIPortSpec());

		// Initialize classes and features; add operations and parameters
		initEClass(multiplexPortTypeSpecEClass, MultiplexPortTypeSpec.class, "MultiplexPortTypeSpec", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMultiplexPortTypeSpec_Type(), theUcm_interactionsPackage.getPortType(), null, "type", null, 1, 1, MultiplexPortTypeSpec.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMultiplexPortTypeSpec_IndexType(), theUcm_datatypesPackage.getEnumeration(), null, "indexType", null, 1, 1, MultiplexPortTypeSpec.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMultiplexPortTypeSpec_BindingSet(), this.getAbstractTypeBindingSet(), null, "bindingSet", null, 1, -1, MultiplexPortTypeSpec.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(abstractTypeBindingSetEClass, AbstractTypeBindingSet.class, "AbstractTypeBindingSet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAbstractTypeBindingSet_Binding(), theUcm_componentsPackage.getAbstractTypeBinding(), null, "binding", null, 0, -1, AbstractTypeBindingSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAbstractTypeBindingSet_BindingName(), theUcm_datatypesPackage.getEnumerator(), null, "bindingName", null, 0, 1, AbstractTypeBindingSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(multiplexPortDelegationEClass, MultiplexPortDelegation.class, "MultiplexPortDelegation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMultiplexPortDelegation_ExternalBindingName(), theUcm_datatypesPackage.getEnumerator(), null, "externalBindingName", null, 0, 1, MultiplexPortDelegation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMultiplexPortDelegation_BindingName(), theUcm_datatypesPackage.getEnumerator(), null, "bindingName", null, 0, 1, MultiplexPortDelegation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(multiplexPortRoleSpecEClass, MultiplexPortRoleSpec.class, "MultiplexPortRoleSpec", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMultiplexPortRoleSpec_Role(), theUcm_interactionsPackage.getInteractionRole(), null, "role", null, 1, 1, MultiplexPortRoleSpec.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMultiplexPortRoleSpec_BindingSet(), this.getInteractionItemBindingSet(), null, "bindingSet", null, 1, -1, MultiplexPortRoleSpec.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMultiplexPortRoleSpec_IndexType(), theUcm_datatypesPackage.getEnumeration(), null, "indexType", null, 1, 1, MultiplexPortRoleSpec.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(interactionItemBindingSetEClass, InteractionItemBindingSet.class, "InteractionItemBindingSet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInteractionItemBindingSet_Binding(), theUcm_componentsPackage.getInteractionItemBinding(), null, "binding", null, 0, -1, InteractionItemBindingSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInteractionItemBindingSet_BindingName(), theUcm_datatypesPackage.getEnumerator(), null, "bindingName", null, 0, 1, InteractionItemBindingSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //Ucm_enhanced_portspecPackageImpl
