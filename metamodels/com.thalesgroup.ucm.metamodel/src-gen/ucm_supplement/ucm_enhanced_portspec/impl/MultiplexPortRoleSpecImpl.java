/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_enhanced_portspec.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import ucm_base.ucm_components.impl.IPortSpecImpl;

import ucm_base.ucm_contracts.ucm_datatypes.Enumeration;

import ucm_base.ucm_interactions.InteractionRole;

import ucm_supplement.ucm_enhanced_portspec.InteractionItemBindingSet;
import ucm_supplement.ucm_enhanced_portspec.MultiplexPortRoleSpec;
import ucm_supplement.ucm_enhanced_portspec.Ucm_enhanced_portspecPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Multiplex Port Role Spec</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_enhanced_portspec.impl.MultiplexPortRoleSpecImpl#getRole <em>Role</em>}</li>
 *   <li>{@link ucm_supplement.ucm_enhanced_portspec.impl.MultiplexPortRoleSpecImpl#getBindingSet <em>Binding Set</em>}</li>
 *   <li>{@link ucm_supplement.ucm_enhanced_portspec.impl.MultiplexPortRoleSpecImpl#getIndexType <em>Index Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MultiplexPortRoleSpecImpl extends IPortSpecImpl implements MultiplexPortRoleSpec {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached value of the '{@link #getRole() <em>Role</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRole()
	 * @generated
	 * @ordered
	 */
	protected InteractionRole role;

	/**
	 * The cached value of the '{@link #getBindingSet() <em>Binding Set</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBindingSet()
	 * @generated
	 * @ordered
	 */
	protected EList<InteractionItemBindingSet> bindingSet;

	/**
	 * The cached value of the '{@link #getIndexType() <em>Index Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndexType()
	 * @generated
	 * @ordered
	 */
	protected Enumeration indexType;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MultiplexPortRoleSpecImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_enhanced_portspecPackage.Literals.MULTIPLEX_PORT_ROLE_SPEC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InteractionRole getRole() {
		if (role != null && role.eIsProxy()) {
			InternalEObject oldRole = (InternalEObject)role;
			role = (InteractionRole)eResolveProxy(oldRole);
			if (role != oldRole) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_ROLE_SPEC__ROLE, oldRole, role));
			}
		}
		return role;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InteractionRole basicGetRole() {
		return role;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRole(InteractionRole newRole) {
		InteractionRole oldRole = role;
		role = newRole;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_ROLE_SPEC__ROLE, oldRole, role));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InteractionItemBindingSet> getBindingSet() {
		if (bindingSet == null) {
			bindingSet = new EObjectContainmentEList<InteractionItemBindingSet>(InteractionItemBindingSet.class, this, Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_ROLE_SPEC__BINDING_SET);
		}
		return bindingSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Enumeration getIndexType() {
		if (indexType != null && indexType.eIsProxy()) {
			InternalEObject oldIndexType = (InternalEObject)indexType;
			indexType = (Enumeration)eResolveProxy(oldIndexType);
			if (indexType != oldIndexType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_ROLE_SPEC__INDEX_TYPE, oldIndexType, indexType));
			}
		}
		return indexType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Enumeration basicGetIndexType() {
		return indexType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIndexType(Enumeration newIndexType) {
		Enumeration oldIndexType = indexType;
		indexType = newIndexType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_ROLE_SPEC__INDEX_TYPE, oldIndexType, indexType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_ROLE_SPEC__BINDING_SET:
				return ((InternalEList<?>)getBindingSet()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_ROLE_SPEC__ROLE:
				if (resolve) return getRole();
				return basicGetRole();
			case Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_ROLE_SPEC__BINDING_SET:
				return getBindingSet();
			case Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_ROLE_SPEC__INDEX_TYPE:
				if (resolve) return getIndexType();
				return basicGetIndexType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_ROLE_SPEC__ROLE:
				setRole((InteractionRole)newValue);
				return;
			case Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_ROLE_SPEC__BINDING_SET:
				getBindingSet().clear();
				getBindingSet().addAll((Collection<? extends InteractionItemBindingSet>)newValue);
				return;
			case Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_ROLE_SPEC__INDEX_TYPE:
				setIndexType((Enumeration)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_ROLE_SPEC__ROLE:
				setRole((InteractionRole)null);
				return;
			case Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_ROLE_SPEC__BINDING_SET:
				getBindingSet().clear();
				return;
			case Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_ROLE_SPEC__INDEX_TYPE:
				setIndexType((Enumeration)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_ROLE_SPEC__ROLE:
				return role != null;
			case Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_ROLE_SPEC__BINDING_SET:
				return bindingSet != null && !bindingSet.isEmpty();
			case Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_ROLE_SPEC__INDEX_TYPE:
				return indexType != null;
		}
		return super.eIsSet(featureID);
	}

} //MultiplexPortRoleSpecImpl
