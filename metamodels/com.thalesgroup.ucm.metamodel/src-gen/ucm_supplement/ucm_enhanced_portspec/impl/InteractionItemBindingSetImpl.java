/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_enhanced_portspec.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import ucm_base.ucm_components.InteractionItemBinding;

import ucm_base.ucm_contracts.ucm_datatypes.Enumerator;

import ucm_supplement.ucm_enhanced_portspec.InteractionItemBindingSet;
import ucm_supplement.ucm_enhanced_portspec.Ucm_enhanced_portspecPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Interaction Item Binding Set</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_enhanced_portspec.impl.InteractionItemBindingSetImpl#getBinding <em>Binding</em>}</li>
 *   <li>{@link ucm_supplement.ucm_enhanced_portspec.impl.InteractionItemBindingSetImpl#getBindingName <em>Binding Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class InteractionItemBindingSetImpl extends MinimalEObjectImpl.Container implements InteractionItemBindingSet {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached value of the '{@link #getBinding() <em>Binding</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBinding()
	 * @generated
	 * @ordered
	 */
	protected EList<InteractionItemBinding> binding;

	/**
	 * The cached value of the '{@link #getBindingName() <em>Binding Name</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBindingName()
	 * @generated
	 * @ordered
	 */
	protected Enumerator bindingName;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InteractionItemBindingSetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_enhanced_portspecPackage.Literals.INTERACTION_ITEM_BINDING_SET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InteractionItemBinding> getBinding() {
		if (binding == null) {
			binding = new EObjectContainmentEList<InteractionItemBinding>(InteractionItemBinding.class, this, Ucm_enhanced_portspecPackage.INTERACTION_ITEM_BINDING_SET__BINDING);
		}
		return binding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Enumerator getBindingName() {
		if (bindingName != null && bindingName.eIsProxy()) {
			InternalEObject oldBindingName = (InternalEObject)bindingName;
			bindingName = (Enumerator)eResolveProxy(oldBindingName);
			if (bindingName != oldBindingName) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_enhanced_portspecPackage.INTERACTION_ITEM_BINDING_SET__BINDING_NAME, oldBindingName, bindingName));
			}
		}
		return bindingName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Enumerator basicGetBindingName() {
		return bindingName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBindingName(Enumerator newBindingName) {
		Enumerator oldBindingName = bindingName;
		bindingName = newBindingName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_enhanced_portspecPackage.INTERACTION_ITEM_BINDING_SET__BINDING_NAME, oldBindingName, bindingName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Ucm_enhanced_portspecPackage.INTERACTION_ITEM_BINDING_SET__BINDING:
				return ((InternalEList<?>)getBinding()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_enhanced_portspecPackage.INTERACTION_ITEM_BINDING_SET__BINDING:
				return getBinding();
			case Ucm_enhanced_portspecPackage.INTERACTION_ITEM_BINDING_SET__BINDING_NAME:
				if (resolve) return getBindingName();
				return basicGetBindingName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_enhanced_portspecPackage.INTERACTION_ITEM_BINDING_SET__BINDING:
				getBinding().clear();
				getBinding().addAll((Collection<? extends InteractionItemBinding>)newValue);
				return;
			case Ucm_enhanced_portspecPackage.INTERACTION_ITEM_BINDING_SET__BINDING_NAME:
				setBindingName((Enumerator)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_enhanced_portspecPackage.INTERACTION_ITEM_BINDING_SET__BINDING:
				getBinding().clear();
				return;
			case Ucm_enhanced_portspecPackage.INTERACTION_ITEM_BINDING_SET__BINDING_NAME:
				setBindingName((Enumerator)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_enhanced_portspecPackage.INTERACTION_ITEM_BINDING_SET__BINDING:
				return binding != null && !binding.isEmpty();
			case Ucm_enhanced_portspecPackage.INTERACTION_ITEM_BINDING_SET__BINDING_NAME:
				return bindingName != null;
		}
		return super.eIsSet(featureID);
	}

} //InteractionItemBindingSetImpl
