/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_enhanced_portspec.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import ucm_supplement.ucm_enhanced_portspec.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Ucm_enhanced_portspecFactoryImpl extends EFactoryImpl implements Ucm_enhanced_portspecFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Ucm_enhanced_portspecFactory init() {
		try {
			Ucm_enhanced_portspecFactory theUcm_enhanced_portspecFactory = (Ucm_enhanced_portspecFactory)EPackage.Registry.INSTANCE.getEFactory(Ucm_enhanced_portspecPackage.eNS_URI);
			if (theUcm_enhanced_portspecFactory != null) {
				return theUcm_enhanced_portspecFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new Ucm_enhanced_portspecFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ucm_enhanced_portspecFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_TYPE_SPEC: return createMultiplexPortTypeSpec();
			case Ucm_enhanced_portspecPackage.ABSTRACT_TYPE_BINDING_SET: return createAbstractTypeBindingSet();
			case Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_DELEGATION: return createMultiplexPortDelegation();
			case Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_ROLE_SPEC: return createMultiplexPortRoleSpec();
			case Ucm_enhanced_portspecPackage.INTERACTION_ITEM_BINDING_SET: return createInteractionItemBindingSet();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MultiplexPortTypeSpec createMultiplexPortTypeSpec() {
		MultiplexPortTypeSpecImpl multiplexPortTypeSpec = new MultiplexPortTypeSpecImpl();
		return multiplexPortTypeSpec;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractTypeBindingSet createAbstractTypeBindingSet() {
		AbstractTypeBindingSetImpl abstractTypeBindingSet = new AbstractTypeBindingSetImpl();
		return abstractTypeBindingSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MultiplexPortDelegation createMultiplexPortDelegation() {
		MultiplexPortDelegationImpl multiplexPortDelegation = new MultiplexPortDelegationImpl();
		return multiplexPortDelegation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MultiplexPortRoleSpec createMultiplexPortRoleSpec() {
		MultiplexPortRoleSpecImpl multiplexPortRoleSpec = new MultiplexPortRoleSpecImpl();
		return multiplexPortRoleSpec;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InteractionItemBindingSet createInteractionItemBindingSet() {
		InteractionItemBindingSetImpl interactionItemBindingSet = new InteractionItemBindingSetImpl();
		return interactionItemBindingSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ucm_enhanced_portspecPackage getUcm_enhanced_portspecPackage() {
		return (Ucm_enhanced_portspecPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static Ucm_enhanced_portspecPackage getPackage() {
		return Ucm_enhanced_portspecPackage.eINSTANCE;
	}

} //Ucm_enhanced_portspecFactoryImpl
