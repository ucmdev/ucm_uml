/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_enhanced_portspec.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import ucm_base.ucm_components.impl.IPortSpecImpl;

import ucm_base.ucm_contracts.ucm_datatypes.Enumeration;

import ucm_base.ucm_interactions.PortType;

import ucm_supplement.ucm_enhanced_portspec.AbstractTypeBindingSet;
import ucm_supplement.ucm_enhanced_portspec.MultiplexPortTypeSpec;
import ucm_supplement.ucm_enhanced_portspec.Ucm_enhanced_portspecPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Multiplex Port Type Spec</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_enhanced_portspec.impl.MultiplexPortTypeSpecImpl#getType <em>Type</em>}</li>
 *   <li>{@link ucm_supplement.ucm_enhanced_portspec.impl.MultiplexPortTypeSpecImpl#getIndexType <em>Index Type</em>}</li>
 *   <li>{@link ucm_supplement.ucm_enhanced_portspec.impl.MultiplexPortTypeSpecImpl#getBindingSet <em>Binding Set</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MultiplexPortTypeSpecImpl extends IPortSpecImpl implements MultiplexPortTypeSpec {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected PortType type;

	/**
	 * The cached value of the '{@link #getIndexType() <em>Index Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndexType()
	 * @generated
	 * @ordered
	 */
	protected Enumeration indexType;

	/**
	 * The cached value of the '{@link #getBindingSet() <em>Binding Set</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBindingSet()
	 * @generated
	 * @ordered
	 */
	protected EList<AbstractTypeBindingSet> bindingSet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MultiplexPortTypeSpecImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_enhanced_portspecPackage.Literals.MULTIPLEX_PORT_TYPE_SPEC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortType getType() {
		if (type != null && type.eIsProxy()) {
			InternalEObject oldType = (InternalEObject)type;
			type = (PortType)eResolveProxy(oldType);
			if (type != oldType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_TYPE_SPEC__TYPE, oldType, type));
			}
		}
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortType basicGetType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(PortType newType) {
		PortType oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_TYPE_SPEC__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Enumeration getIndexType() {
		if (indexType != null && indexType.eIsProxy()) {
			InternalEObject oldIndexType = (InternalEObject)indexType;
			indexType = (Enumeration)eResolveProxy(oldIndexType);
			if (indexType != oldIndexType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_TYPE_SPEC__INDEX_TYPE, oldIndexType, indexType));
			}
		}
		return indexType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Enumeration basicGetIndexType() {
		return indexType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIndexType(Enumeration newIndexType) {
		Enumeration oldIndexType = indexType;
		indexType = newIndexType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_TYPE_SPEC__INDEX_TYPE, oldIndexType, indexType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AbstractTypeBindingSet> getBindingSet() {
		if (bindingSet == null) {
			bindingSet = new EObjectContainmentEList<AbstractTypeBindingSet>(AbstractTypeBindingSet.class, this, Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_TYPE_SPEC__BINDING_SET);
		}
		return bindingSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_TYPE_SPEC__BINDING_SET:
				return ((InternalEList<?>)getBindingSet()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_TYPE_SPEC__TYPE:
				if (resolve) return getType();
				return basicGetType();
			case Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_TYPE_SPEC__INDEX_TYPE:
				if (resolve) return getIndexType();
				return basicGetIndexType();
			case Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_TYPE_SPEC__BINDING_SET:
				return getBindingSet();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_TYPE_SPEC__TYPE:
				setType((PortType)newValue);
				return;
			case Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_TYPE_SPEC__INDEX_TYPE:
				setIndexType((Enumeration)newValue);
				return;
			case Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_TYPE_SPEC__BINDING_SET:
				getBindingSet().clear();
				getBindingSet().addAll((Collection<? extends AbstractTypeBindingSet>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_TYPE_SPEC__TYPE:
				setType((PortType)null);
				return;
			case Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_TYPE_SPEC__INDEX_TYPE:
				setIndexType((Enumeration)null);
				return;
			case Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_TYPE_SPEC__BINDING_SET:
				getBindingSet().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_TYPE_SPEC__TYPE:
				return type != null;
			case Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_TYPE_SPEC__INDEX_TYPE:
				return indexType != null;
			case Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_TYPE_SPEC__BINDING_SET:
				return bindingSet != null && !bindingSet.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //MultiplexPortTypeSpecImpl
