/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_enhanced_portspec.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import ucm_base.ucm_components.impl.PortDelegationImpl;

import ucm_base.ucm_contracts.ucm_datatypes.Enumerator;

import ucm_supplement.ucm_enhanced_portspec.MultiplexPortDelegation;
import ucm_supplement.ucm_enhanced_portspec.Ucm_enhanced_portspecPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Multiplex Port Delegation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_enhanced_portspec.impl.MultiplexPortDelegationImpl#getExternalBindingName <em>External Binding Name</em>}</li>
 *   <li>{@link ucm_supplement.ucm_enhanced_portspec.impl.MultiplexPortDelegationImpl#getBindingName <em>Binding Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MultiplexPortDelegationImpl extends PortDelegationImpl implements MultiplexPortDelegation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached value of the '{@link #getExternalBindingName() <em>External Binding Name</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExternalBindingName()
	 * @generated
	 * @ordered
	 */
	protected Enumerator externalBindingName;

	/**
	 * The cached value of the '{@link #getBindingName() <em>Binding Name</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBindingName()
	 * @generated
	 * @ordered
	 */
	protected Enumerator bindingName;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MultiplexPortDelegationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_enhanced_portspecPackage.Literals.MULTIPLEX_PORT_DELEGATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Enumerator getExternalBindingName() {
		if (externalBindingName != null && externalBindingName.eIsProxy()) {
			InternalEObject oldExternalBindingName = (InternalEObject)externalBindingName;
			externalBindingName = (Enumerator)eResolveProxy(oldExternalBindingName);
			if (externalBindingName != oldExternalBindingName) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_DELEGATION__EXTERNAL_BINDING_NAME, oldExternalBindingName, externalBindingName));
			}
		}
		return externalBindingName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Enumerator basicGetExternalBindingName() {
		return externalBindingName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExternalBindingName(Enumerator newExternalBindingName) {
		Enumerator oldExternalBindingName = externalBindingName;
		externalBindingName = newExternalBindingName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_DELEGATION__EXTERNAL_BINDING_NAME, oldExternalBindingName, externalBindingName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Enumerator getBindingName() {
		if (bindingName != null && bindingName.eIsProxy()) {
			InternalEObject oldBindingName = (InternalEObject)bindingName;
			bindingName = (Enumerator)eResolveProxy(oldBindingName);
			if (bindingName != oldBindingName) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_DELEGATION__BINDING_NAME, oldBindingName, bindingName));
			}
		}
		return bindingName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Enumerator basicGetBindingName() {
		return bindingName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBindingName(Enumerator newBindingName) {
		Enumerator oldBindingName = bindingName;
		bindingName = newBindingName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_DELEGATION__BINDING_NAME, oldBindingName, bindingName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_DELEGATION__EXTERNAL_BINDING_NAME:
				if (resolve) return getExternalBindingName();
				return basicGetExternalBindingName();
			case Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_DELEGATION__BINDING_NAME:
				if (resolve) return getBindingName();
				return basicGetBindingName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_DELEGATION__EXTERNAL_BINDING_NAME:
				setExternalBindingName((Enumerator)newValue);
				return;
			case Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_DELEGATION__BINDING_NAME:
				setBindingName((Enumerator)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_DELEGATION__EXTERNAL_BINDING_NAME:
				setExternalBindingName((Enumerator)null);
				return;
			case Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_DELEGATION__BINDING_NAME:
				setBindingName((Enumerator)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_DELEGATION__EXTERNAL_BINDING_NAME:
				return externalBindingName != null;
			case Ucm_enhanced_portspecPackage.MULTIPLEX_PORT_DELEGATION__BINDING_NAME:
				return bindingName != null;
		}
		return super.eIsSet(featureID);
	}

} //MultiplexPortDelegationImpl
