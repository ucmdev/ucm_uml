/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_enhanced_portspec.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import ucm_base.ucm_commons.INamed;

import ucm_base.ucm_components.IPortSpec;
import ucm_base.ucm_components.PortDelegation;

import ucm_supplement.ucm_enhanced_portspec.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see ucm_supplement.ucm_enhanced_portspec.Ucm_enhanced_portspecPackage
 * @generated
 */
public class Ucm_enhanced_portspecAdapterFactory extends AdapterFactoryImpl {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static Ucm_enhanced_portspecPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ucm_enhanced_portspecAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = Ucm_enhanced_portspecPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Ucm_enhanced_portspecSwitch<Adapter> modelSwitch =
		new Ucm_enhanced_portspecSwitch<Adapter>() {
			@Override
			public Adapter caseMultiplexPortTypeSpec(MultiplexPortTypeSpec object) {
				return createMultiplexPortTypeSpecAdapter();
			}
			@Override
			public Adapter caseAbstractTypeBindingSet(AbstractTypeBindingSet object) {
				return createAbstractTypeBindingSetAdapter();
			}
			@Override
			public Adapter caseMultiplexPortDelegation(MultiplexPortDelegation object) {
				return createMultiplexPortDelegationAdapter();
			}
			@Override
			public Adapter caseMultiplexPortRoleSpec(MultiplexPortRoleSpec object) {
				return createMultiplexPortRoleSpecAdapter();
			}
			@Override
			public Adapter caseInteractionItemBindingSet(InteractionItemBindingSet object) {
				return createInteractionItemBindingSetAdapter();
			}
			@Override
			public Adapter caseIPortSpec(IPortSpec object) {
				return createIPortSpecAdapter();
			}
			@Override
			public Adapter caseINamed(INamed object) {
				return createINamedAdapter();
			}
			@Override
			public Adapter casePortDelegation(PortDelegation object) {
				return createPortDelegationAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link ucm_supplement.ucm_enhanced_portspec.MultiplexPortTypeSpec <em>Multiplex Port Type Spec</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_supplement.ucm_enhanced_portspec.MultiplexPortTypeSpec
	 * @generated
	 */
	public Adapter createMultiplexPortTypeSpecAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_supplement.ucm_enhanced_portspec.AbstractTypeBindingSet <em>Abstract Type Binding Set</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_supplement.ucm_enhanced_portspec.AbstractTypeBindingSet
	 * @generated
	 */
	public Adapter createAbstractTypeBindingSetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_supplement.ucm_enhanced_portspec.MultiplexPortDelegation <em>Multiplex Port Delegation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_supplement.ucm_enhanced_portspec.MultiplexPortDelegation
	 * @generated
	 */
	public Adapter createMultiplexPortDelegationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_supplement.ucm_enhanced_portspec.MultiplexPortRoleSpec <em>Multiplex Port Role Spec</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_supplement.ucm_enhanced_portspec.MultiplexPortRoleSpec
	 * @generated
	 */
	public Adapter createMultiplexPortRoleSpecAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_supplement.ucm_enhanced_portspec.InteractionItemBindingSet <em>Interaction Item Binding Set</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_supplement.ucm_enhanced_portspec.InteractionItemBindingSet
	 * @generated
	 */
	public Adapter createInteractionItemBindingSetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_base.ucm_components.IPortSpec <em>IPort Spec</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_base.ucm_components.IPortSpec
	 * @generated
	 */
	public Adapter createIPortSpecAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_base.ucm_commons.INamed <em>INamed</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_base.ucm_commons.INamed
	 * @generated
	 */
	public Adapter createINamedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ucm_base.ucm_components.PortDelegation <em>Port Delegation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ucm_base.ucm_components.PortDelegation
	 * @generated
	 */
	public Adapter createPortDelegationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //Ucm_enhanced_portspecAdapterFactory
