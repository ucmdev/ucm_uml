/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_enhanced_portspec;

import ucm_base.ucm_components.PortDelegation;

import ucm_base.ucm_contracts.ucm_datatypes.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Multiplex Port Delegation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_enhanced_portspec.MultiplexPortDelegation#getExternalBindingName <em>External Binding Name</em>}</li>
 *   <li>{@link ucm_supplement.ucm_enhanced_portspec.MultiplexPortDelegation#getBindingName <em>Binding Name</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_enhanced_portspec.Ucm_enhanced_portspecPackage#getMultiplexPortDelegation()
 * @model
 * @generated
 */
public interface MultiplexPortDelegation extends PortDelegation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>External Binding Name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>External Binding Name</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>External Binding Name</em>' reference.
	 * @see #setExternalBindingName(Enumerator)
	 * @see ucm_supplement.ucm_enhanced_portspec.Ucm_enhanced_portspecPackage#getMultiplexPortDelegation_ExternalBindingName()
	 * @model
	 * @generated
	 */
	Enumerator getExternalBindingName();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_enhanced_portspec.MultiplexPortDelegation#getExternalBindingName <em>External Binding Name</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>External Binding Name</em>' reference.
	 * @see #getExternalBindingName()
	 * @generated
	 */
	void setExternalBindingName(Enumerator value);

	/**
	 * Returns the value of the '<em><b>Binding Name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Binding Name</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Binding Name</em>' reference.
	 * @see #setBindingName(Enumerator)
	 * @see ucm_supplement.ucm_enhanced_portspec.Ucm_enhanced_portspecPackage#getMultiplexPortDelegation_BindingName()
	 * @model
	 * @generated
	 */
	Enumerator getBindingName();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_enhanced_portspec.MultiplexPortDelegation#getBindingName <em>Binding Name</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Binding Name</em>' reference.
	 * @see #getBindingName()
	 * @generated
	 */
	void setBindingName(Enumerator value);

} // MultiplexPortDelegation
