/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_enhanced_portspec;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import ucm_base.ucm_components.Ucm_componentsPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ucm_supplement.ucm_enhanced_portspec.Ucm_enhanced_portspecFactory
 * @model kind="package"
 * @generated
 */
public interface Ucm_enhanced_portspecPackage extends EPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ucm_enhanced_portspec";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://com.thalesgroup.ucm/1.0/metamodels/supplements/portspecs";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ucm_enhanced_portspec";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Ucm_enhanced_portspecPackage eINSTANCE = ucm_supplement.ucm_enhanced_portspec.impl.Ucm_enhanced_portspecPackageImpl.init();

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_enhanced_portspec.impl.MultiplexPortTypeSpecImpl <em>Multiplex Port Type Spec</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_enhanced_portspec.impl.MultiplexPortTypeSpecImpl
	 * @see ucm_supplement.ucm_enhanced_portspec.impl.Ucm_enhanced_portspecPackageImpl#getMultiplexPortTypeSpec()
	 * @generated
	 */
	int MULTIPLEX_PORT_TYPE_SPEC = 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLEX_PORT_TYPE_SPEC__TYPE = Ucm_componentsPackage.IPORT_SPEC_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Index Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLEX_PORT_TYPE_SPEC__INDEX_TYPE = Ucm_componentsPackage.IPORT_SPEC_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Binding Set</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLEX_PORT_TYPE_SPEC__BINDING_SET = Ucm_componentsPackage.IPORT_SPEC_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Multiplex Port Type Spec</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLEX_PORT_TYPE_SPEC_FEATURE_COUNT = Ucm_componentsPackage.IPORT_SPEC_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_enhanced_portspec.impl.AbstractTypeBindingSetImpl <em>Abstract Type Binding Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_enhanced_portspec.impl.AbstractTypeBindingSetImpl
	 * @see ucm_supplement.ucm_enhanced_portspec.impl.Ucm_enhanced_portspecPackageImpl#getAbstractTypeBindingSet()
	 * @generated
	 */
	int ABSTRACT_TYPE_BINDING_SET = 1;

	/**
	 * The feature id for the '<em><b>Binding</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TYPE_BINDING_SET__BINDING = 0;

	/**
	 * The feature id for the '<em><b>Binding Name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TYPE_BINDING_SET__BINDING_NAME = 1;

	/**
	 * The number of structural features of the '<em>Abstract Type Binding Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TYPE_BINDING_SET_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_enhanced_portspec.impl.MultiplexPortDelegationImpl <em>Multiplex Port Delegation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_enhanced_portspec.impl.MultiplexPortDelegationImpl
	 * @see ucm_supplement.ucm_enhanced_portspec.impl.Ucm_enhanced_portspecPackageImpl#getMultiplexPortDelegation()
	 * @generated
	 */
	int MULTIPLEX_PORT_DELEGATION = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLEX_PORT_DELEGATION__NAME = Ucm_componentsPackage.PORT_DELEGATION__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLEX_PORT_DELEGATION__COMMENT = Ucm_componentsPackage.PORT_DELEGATION__COMMENT;

	/**
	 * The feature id for the '<em><b>External Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLEX_PORT_DELEGATION__EXTERNAL_PORT = Ucm_componentsPackage.PORT_DELEGATION__EXTERNAL_PORT;

	/**
	 * The feature id for the '<em><b>Part</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLEX_PORT_DELEGATION__PART = Ucm_componentsPackage.PORT_DELEGATION__PART;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLEX_PORT_DELEGATION__PORT = Ucm_componentsPackage.PORT_DELEGATION__PORT;

	/**
	 * The feature id for the '<em><b>External Binding Name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLEX_PORT_DELEGATION__EXTERNAL_BINDING_NAME = Ucm_componentsPackage.PORT_DELEGATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Binding Name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLEX_PORT_DELEGATION__BINDING_NAME = Ucm_componentsPackage.PORT_DELEGATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Multiplex Port Delegation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLEX_PORT_DELEGATION_FEATURE_COUNT = Ucm_componentsPackage.PORT_DELEGATION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_enhanced_portspec.impl.MultiplexPortRoleSpecImpl <em>Multiplex Port Role Spec</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_enhanced_portspec.impl.MultiplexPortRoleSpecImpl
	 * @see ucm_supplement.ucm_enhanced_portspec.impl.Ucm_enhanced_portspecPackageImpl#getMultiplexPortRoleSpec()
	 * @generated
	 */
	int MULTIPLEX_PORT_ROLE_SPEC = 3;

	/**
	 * The feature id for the '<em><b>Role</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLEX_PORT_ROLE_SPEC__ROLE = Ucm_componentsPackage.IPORT_SPEC_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Binding Set</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLEX_PORT_ROLE_SPEC__BINDING_SET = Ucm_componentsPackage.IPORT_SPEC_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Index Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLEX_PORT_ROLE_SPEC__INDEX_TYPE = Ucm_componentsPackage.IPORT_SPEC_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Multiplex Port Role Spec</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLEX_PORT_ROLE_SPEC_FEATURE_COUNT = Ucm_componentsPackage.IPORT_SPEC_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link ucm_supplement.ucm_enhanced_portspec.impl.InteractionItemBindingSetImpl <em>Interaction Item Binding Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ucm_supplement.ucm_enhanced_portspec.impl.InteractionItemBindingSetImpl
	 * @see ucm_supplement.ucm_enhanced_portspec.impl.Ucm_enhanced_portspecPackageImpl#getInteractionItemBindingSet()
	 * @generated
	 */
	int INTERACTION_ITEM_BINDING_SET = 4;

	/**
	 * The feature id for the '<em><b>Binding</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_ITEM_BINDING_SET__BINDING = 0;

	/**
	 * The feature id for the '<em><b>Binding Name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_ITEM_BINDING_SET__BINDING_NAME = 1;

	/**
	 * The number of structural features of the '<em>Interaction Item Binding Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_ITEM_BINDING_SET_FEATURE_COUNT = 2;


	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_enhanced_portspec.MultiplexPortTypeSpec <em>Multiplex Port Type Spec</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Multiplex Port Type Spec</em>'.
	 * @see ucm_supplement.ucm_enhanced_portspec.MultiplexPortTypeSpec
	 * @generated
	 */
	EClass getMultiplexPortTypeSpec();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_enhanced_portspec.MultiplexPortTypeSpec#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see ucm_supplement.ucm_enhanced_portspec.MultiplexPortTypeSpec#getType()
	 * @see #getMultiplexPortTypeSpec()
	 * @generated
	 */
	EReference getMultiplexPortTypeSpec_Type();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_enhanced_portspec.MultiplexPortTypeSpec#getIndexType <em>Index Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Index Type</em>'.
	 * @see ucm_supplement.ucm_enhanced_portspec.MultiplexPortTypeSpec#getIndexType()
	 * @see #getMultiplexPortTypeSpec()
	 * @generated
	 */
	EReference getMultiplexPortTypeSpec_IndexType();

	/**
	 * Returns the meta object for the containment reference list '{@link ucm_supplement.ucm_enhanced_portspec.MultiplexPortTypeSpec#getBindingSet <em>Binding Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Binding Set</em>'.
	 * @see ucm_supplement.ucm_enhanced_portspec.MultiplexPortTypeSpec#getBindingSet()
	 * @see #getMultiplexPortTypeSpec()
	 * @generated
	 */
	EReference getMultiplexPortTypeSpec_BindingSet();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_enhanced_portspec.AbstractTypeBindingSet <em>Abstract Type Binding Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Type Binding Set</em>'.
	 * @see ucm_supplement.ucm_enhanced_portspec.AbstractTypeBindingSet
	 * @generated
	 */
	EClass getAbstractTypeBindingSet();

	/**
	 * Returns the meta object for the containment reference list '{@link ucm_supplement.ucm_enhanced_portspec.AbstractTypeBindingSet#getBinding <em>Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Binding</em>'.
	 * @see ucm_supplement.ucm_enhanced_portspec.AbstractTypeBindingSet#getBinding()
	 * @see #getAbstractTypeBindingSet()
	 * @generated
	 */
	EReference getAbstractTypeBindingSet_Binding();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_enhanced_portspec.AbstractTypeBindingSet#getBindingName <em>Binding Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Binding Name</em>'.
	 * @see ucm_supplement.ucm_enhanced_portspec.AbstractTypeBindingSet#getBindingName()
	 * @see #getAbstractTypeBindingSet()
	 * @generated
	 */
	EReference getAbstractTypeBindingSet_BindingName();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_enhanced_portspec.MultiplexPortDelegation <em>Multiplex Port Delegation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Multiplex Port Delegation</em>'.
	 * @see ucm_supplement.ucm_enhanced_portspec.MultiplexPortDelegation
	 * @generated
	 */
	EClass getMultiplexPortDelegation();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_enhanced_portspec.MultiplexPortDelegation#getExternalBindingName <em>External Binding Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>External Binding Name</em>'.
	 * @see ucm_supplement.ucm_enhanced_portspec.MultiplexPortDelegation#getExternalBindingName()
	 * @see #getMultiplexPortDelegation()
	 * @generated
	 */
	EReference getMultiplexPortDelegation_ExternalBindingName();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_enhanced_portspec.MultiplexPortDelegation#getBindingName <em>Binding Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Binding Name</em>'.
	 * @see ucm_supplement.ucm_enhanced_portspec.MultiplexPortDelegation#getBindingName()
	 * @see #getMultiplexPortDelegation()
	 * @generated
	 */
	EReference getMultiplexPortDelegation_BindingName();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_enhanced_portspec.MultiplexPortRoleSpec <em>Multiplex Port Role Spec</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Multiplex Port Role Spec</em>'.
	 * @see ucm_supplement.ucm_enhanced_portspec.MultiplexPortRoleSpec
	 * @generated
	 */
	EClass getMultiplexPortRoleSpec();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_enhanced_portspec.MultiplexPortRoleSpec#getRole <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Role</em>'.
	 * @see ucm_supplement.ucm_enhanced_portspec.MultiplexPortRoleSpec#getRole()
	 * @see #getMultiplexPortRoleSpec()
	 * @generated
	 */
	EReference getMultiplexPortRoleSpec_Role();

	/**
	 * Returns the meta object for the containment reference list '{@link ucm_supplement.ucm_enhanced_portspec.MultiplexPortRoleSpec#getBindingSet <em>Binding Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Binding Set</em>'.
	 * @see ucm_supplement.ucm_enhanced_portspec.MultiplexPortRoleSpec#getBindingSet()
	 * @see #getMultiplexPortRoleSpec()
	 * @generated
	 */
	EReference getMultiplexPortRoleSpec_BindingSet();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_enhanced_portspec.MultiplexPortRoleSpec#getIndexType <em>Index Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Index Type</em>'.
	 * @see ucm_supplement.ucm_enhanced_portspec.MultiplexPortRoleSpec#getIndexType()
	 * @see #getMultiplexPortRoleSpec()
	 * @generated
	 */
	EReference getMultiplexPortRoleSpec_IndexType();

	/**
	 * Returns the meta object for class '{@link ucm_supplement.ucm_enhanced_portspec.InteractionItemBindingSet <em>Interaction Item Binding Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Interaction Item Binding Set</em>'.
	 * @see ucm_supplement.ucm_enhanced_portspec.InteractionItemBindingSet
	 * @generated
	 */
	EClass getInteractionItemBindingSet();

	/**
	 * Returns the meta object for the containment reference list '{@link ucm_supplement.ucm_enhanced_portspec.InteractionItemBindingSet#getBinding <em>Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Binding</em>'.
	 * @see ucm_supplement.ucm_enhanced_portspec.InteractionItemBindingSet#getBinding()
	 * @see #getInteractionItemBindingSet()
	 * @generated
	 */
	EReference getInteractionItemBindingSet_Binding();

	/**
	 * Returns the meta object for the reference '{@link ucm_supplement.ucm_enhanced_portspec.InteractionItemBindingSet#getBindingName <em>Binding Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Binding Name</em>'.
	 * @see ucm_supplement.ucm_enhanced_portspec.InteractionItemBindingSet#getBindingName()
	 * @see #getInteractionItemBindingSet()
	 * @generated
	 */
	EReference getInteractionItemBindingSet_BindingName();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	Ucm_enhanced_portspecFactory getUcm_enhanced_portspecFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_enhanced_portspec.impl.MultiplexPortTypeSpecImpl <em>Multiplex Port Type Spec</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_enhanced_portspec.impl.MultiplexPortTypeSpecImpl
		 * @see ucm_supplement.ucm_enhanced_portspec.impl.Ucm_enhanced_portspecPackageImpl#getMultiplexPortTypeSpec()
		 * @generated
		 */
		EClass MULTIPLEX_PORT_TYPE_SPEC = eINSTANCE.getMultiplexPortTypeSpec();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MULTIPLEX_PORT_TYPE_SPEC__TYPE = eINSTANCE.getMultiplexPortTypeSpec_Type();

		/**
		 * The meta object literal for the '<em><b>Index Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MULTIPLEX_PORT_TYPE_SPEC__INDEX_TYPE = eINSTANCE.getMultiplexPortTypeSpec_IndexType();

		/**
		 * The meta object literal for the '<em><b>Binding Set</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MULTIPLEX_PORT_TYPE_SPEC__BINDING_SET = eINSTANCE.getMultiplexPortTypeSpec_BindingSet();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_enhanced_portspec.impl.AbstractTypeBindingSetImpl <em>Abstract Type Binding Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_enhanced_portspec.impl.AbstractTypeBindingSetImpl
		 * @see ucm_supplement.ucm_enhanced_portspec.impl.Ucm_enhanced_portspecPackageImpl#getAbstractTypeBindingSet()
		 * @generated
		 */
		EClass ABSTRACT_TYPE_BINDING_SET = eINSTANCE.getAbstractTypeBindingSet();

		/**
		 * The meta object literal for the '<em><b>Binding</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_TYPE_BINDING_SET__BINDING = eINSTANCE.getAbstractTypeBindingSet_Binding();

		/**
		 * The meta object literal for the '<em><b>Binding Name</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_TYPE_BINDING_SET__BINDING_NAME = eINSTANCE.getAbstractTypeBindingSet_BindingName();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_enhanced_portspec.impl.MultiplexPortDelegationImpl <em>Multiplex Port Delegation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_enhanced_portspec.impl.MultiplexPortDelegationImpl
		 * @see ucm_supplement.ucm_enhanced_portspec.impl.Ucm_enhanced_portspecPackageImpl#getMultiplexPortDelegation()
		 * @generated
		 */
		EClass MULTIPLEX_PORT_DELEGATION = eINSTANCE.getMultiplexPortDelegation();

		/**
		 * The meta object literal for the '<em><b>External Binding Name</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MULTIPLEX_PORT_DELEGATION__EXTERNAL_BINDING_NAME = eINSTANCE.getMultiplexPortDelegation_ExternalBindingName();

		/**
		 * The meta object literal for the '<em><b>Binding Name</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MULTIPLEX_PORT_DELEGATION__BINDING_NAME = eINSTANCE.getMultiplexPortDelegation_BindingName();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_enhanced_portspec.impl.MultiplexPortRoleSpecImpl <em>Multiplex Port Role Spec</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_enhanced_portspec.impl.MultiplexPortRoleSpecImpl
		 * @see ucm_supplement.ucm_enhanced_portspec.impl.Ucm_enhanced_portspecPackageImpl#getMultiplexPortRoleSpec()
		 * @generated
		 */
		EClass MULTIPLEX_PORT_ROLE_SPEC = eINSTANCE.getMultiplexPortRoleSpec();

		/**
		 * The meta object literal for the '<em><b>Role</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MULTIPLEX_PORT_ROLE_SPEC__ROLE = eINSTANCE.getMultiplexPortRoleSpec_Role();

		/**
		 * The meta object literal for the '<em><b>Binding Set</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MULTIPLEX_PORT_ROLE_SPEC__BINDING_SET = eINSTANCE.getMultiplexPortRoleSpec_BindingSet();

		/**
		 * The meta object literal for the '<em><b>Index Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MULTIPLEX_PORT_ROLE_SPEC__INDEX_TYPE = eINSTANCE.getMultiplexPortRoleSpec_IndexType();

		/**
		 * The meta object literal for the '{@link ucm_supplement.ucm_enhanced_portspec.impl.InteractionItemBindingSetImpl <em>Interaction Item Binding Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ucm_supplement.ucm_enhanced_portspec.impl.InteractionItemBindingSetImpl
		 * @see ucm_supplement.ucm_enhanced_portspec.impl.Ucm_enhanced_portspecPackageImpl#getInteractionItemBindingSet()
		 * @generated
		 */
		EClass INTERACTION_ITEM_BINDING_SET = eINSTANCE.getInteractionItemBindingSet();

		/**
		 * The meta object literal for the '<em><b>Binding</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERACTION_ITEM_BINDING_SET__BINDING = eINSTANCE.getInteractionItemBindingSet_Binding();

		/**
		 * The meta object literal for the '<em><b>Binding Name</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERACTION_ITEM_BINDING_SET__BINDING_NAME = eINSTANCE.getInteractionItemBindingSet_BindingName();

	}

} //Ucm_enhanced_portspecPackage
