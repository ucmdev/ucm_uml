/**
 * Copyright © 2015-2016 Thales, PrismTech
 * 
 * Licensed under the EUPL v1.1
 */
package ucm_supplement.ucm_enhanced_portspec;

import org.eclipse.emf.common.util.EList;

import ucm_base.ucm_components.IPortSpec;

import ucm_base.ucm_contracts.ucm_datatypes.Enumeration;

import ucm_base.ucm_interactions.PortType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Multiplex Port Type Spec</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ucm_supplement.ucm_enhanced_portspec.MultiplexPortTypeSpec#getType <em>Type</em>}</li>
 *   <li>{@link ucm_supplement.ucm_enhanced_portspec.MultiplexPortTypeSpec#getIndexType <em>Index Type</em>}</li>
 *   <li>{@link ucm_supplement.ucm_enhanced_portspec.MultiplexPortTypeSpec#getBindingSet <em>Binding Set</em>}</li>
 * </ul>
 *
 * @see ucm_supplement.ucm_enhanced_portspec.Ucm_enhanced_portspecPackage#getMultiplexPortTypeSpec()
 * @model
 * @generated
 */
public interface MultiplexPortTypeSpec extends IPortSpec {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright © 2015-2016 Thales, PrismTech\n\nLicensed under the EUPL v1.1";

	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(PortType)
	 * @see ucm_supplement.ucm_enhanced_portspec.Ucm_enhanced_portspecPackage#getMultiplexPortTypeSpec_Type()
	 * @model required="true"
	 * @generated
	 */
	PortType getType();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_enhanced_portspec.MultiplexPortTypeSpec#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(PortType value);

	/**
	 * Returns the value of the '<em><b>Index Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Index Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Index Type</em>' reference.
	 * @see #setIndexType(Enumeration)
	 * @see ucm_supplement.ucm_enhanced_portspec.Ucm_enhanced_portspecPackage#getMultiplexPortTypeSpec_IndexType()
	 * @model required="true"
	 * @generated
	 */
	Enumeration getIndexType();

	/**
	 * Sets the value of the '{@link ucm_supplement.ucm_enhanced_portspec.MultiplexPortTypeSpec#getIndexType <em>Index Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Index Type</em>' reference.
	 * @see #getIndexType()
	 * @generated
	 */
	void setIndexType(Enumeration value);

	/**
	 * Returns the value of the '<em><b>Binding Set</b></em>' containment reference list.
	 * The list contents are of type {@link ucm_supplement.ucm_enhanced_portspec.AbstractTypeBindingSet}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Binding Set</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Binding Set</em>' containment reference list.
	 * @see ucm_supplement.ucm_enhanced_portspec.Ucm_enhanced_portspecPackage#getMultiplexPortTypeSpec_BindingSet()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<AbstractTypeBindingSet> getBindingSet();

} // MultiplexPortTypeSpec
