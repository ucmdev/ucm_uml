package com.thalesgroup.ucm.metamodel.validation;

import java.util.Hashtable;
import java.util.Map;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.omg.ucm.metamodel.validation.ComponentFeatureListManager;

import ucm_environment.ucm_resources.CommunicationPort;
import ucm_environment.ucm_resources.CommunicationPortDefinition;
import ucm_environment.ucm_resources.CommunicationResource;
import ucm_environment.ucm_resources.CommunicationResourceDefinition;
import ucm_environment.ucm_resources.CommunicationResourcePort;
import ucm_environment.ucm_resources.ComputationResource;
import ucm_environment.ucm_resources.EnvironmentModule;
import ucm_environment.ucm_resources.util.Ucm_resourcesValidator;

public class UcmResourceValidation {
	public boolean portCardinality(CommunicationResource commRsc, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		boolean success = false;
		Hashtable<CommunicationPortDefinition, Integer> portCardinalities = new Hashtable<CommunicationPortDefinition, Integer>();
		CommunicationResourceDefinition def = commRsc.getDefinition();
		for (CommunicationResourcePort commPort : def.getCommunicationPort()) {
			portCardinalities.put(commPort.getDefinition(), new Integer(0));
		}

		/*
		 * For each port of the communication resource, retrieve the
		 * corresponding definition and increment the hash table for this port
		 * definition. If a definition is not found in the hash table, this
		 * means the port definition is not managed by the communication
		 * resource definition.
		 */
		for (CommunicationPort port : commRsc.getCommunicationPort()) {
			CommunicationPortDefinition portDef = port.getComputationPort().getDefinition();
			if (portCardinalities.containsKey(portDef)) {
				portCardinalities.put(portDef, portCardinalities.get(portDef) + 1);
			} else {
				if (diagnostics != null) {
					diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_resourcesValidator.DIAGNOSTIC_SOURCE,
							Ucm_resourcesValidator.COMMUNICATION_RESOURCE__PORT_CARDINALITY, "port " + port.getName()
									+ " is not allowed by communication resource definition " + def.getName(),
							new Object[] { commRsc, port }));
				}
				success = false;
			}
		}

		/*
		 * For each port managed by the communication resource definition, check
		 * the cardinality is correct.
		 */
		for (CommunicationResourcePort commPort : def.getCommunicationPort()) {
			long cardinality = portCardinalities.get(commPort.getDefinition());
			if (cardinality < commPort.getMin() || (cardinality > commPort.getMax() && commPort.getMax() > 0)) {
				if (diagnostics != null) {
					diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_resourcesValidator.DIAGNOSTIC_SOURCE,
							Ucm_resourcesValidator.COMMUNICATION_RESOURCE__PORT_CARDINALITY,
							"incorrect number of communication ports for definition " + commPort.getName(),
							new Object[] { commRsc, commPort }));
				}
				success = false;
			}
		}

		return success;
	}

	public boolean oneConnectionPerPort(EnvironmentModule environment, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		boolean success = true;
		ComponentFeatureListManager<ComputationResource, CommunicationPort> list = new ComponentFeatureListManager<ComputationResource, CommunicationPort>();
		for (CommunicationResource commRsc : environment.getCommunicationResource()) {
			for (CommunicationPort port : commRsc.getCommunicationPort()) {
				ComputationResource compRsc = port.getComputationResource();

				/*
				 * check if the computation resource and the communication
				 * resource are in the same environment module
				 */
				if (compRsc.eContainer() != environment) {
					if (diagnostics != null) {
						diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_resourcesValidator.DIAGNOSTIC_SOURCE,
								Ucm_resourcesValidator.ENVIRONMENT_MODULE__ONE_CONNECTION_PER_PORT,
								"Computation resource " + compRsc.getName() + " and communication resource "
										+ commRsc.getName() + " are in different environment modules",
								new Object[] { commRsc, compRsc }));
					}
					success = false;
				}

				/*
				 * check if the computation port actually belongs to the
				 * computation resource
				 */
				if (!compRsc.getDefinition().getComputationPort().contains(port.getComputationPort())) {
					if (diagnostics != null) {
						diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_resourcesValidator.DIAGNOSTIC_SOURCE,
								Ucm_resourcesValidator.ENVIRONMENT_MODULE__ONE_CONNECTION_PER_PORT,
								"Computation resource " + compRsc.getName() + " has no port "
										+ port.getComputationPort().getName(),
								new Object[] { port, compRsc }));
					}
					success = false;
				}

				/*
				 * memorize the port and the computation resource
				 */
				if (list.alreadyExists(compRsc, port)) {
					if (diagnostics != null) {
						diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_resourcesValidator.DIAGNOSTIC_SOURCE,
								Ucm_resourcesValidator.ENVIRONMENT_MODULE__ONE_CONNECTION_PER_PORT,
								"Port " + port.getName() + " creates a redundant connection to " + compRsc.getName(),
								new Object[] { port, compRsc }));
					}
					success = false;
				}
				list.register(compRsc, port);
			}
		}

		return success;
	}
}
