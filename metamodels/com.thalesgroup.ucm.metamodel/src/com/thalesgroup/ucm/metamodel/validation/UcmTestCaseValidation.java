package com.thalesgroup.ucm.metamodel.validation;

import java.util.Map;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;

import ucm_base.ucm_components.AbstractTypeBinding;
import ucm_base.ucm_components.AssemblyPart;
import ucm_base.ucm_components.ComponentTechnicalPolicy;
import ucm_base.ucm_components.IPortSpec;
import ucm_base.ucm_components.InteractionItemBinding;
import ucm_base.ucm_components.Port;
import ucm_base.ucm_components.PortRoleSpec;
import ucm_base.ucm_components.PortTypeSpec;
import ucm_base.ucm_contracts.AbstractInterface;
import ucm_base.ucm_contracts.IInterface;
import ucm_base.ucm_contracts.Interface;
import ucm_base.ucm_interactions.PortElement;
import ucm_supplement.ucm_deployments.AppAssembly;
import ucm_supplement.ucm_deployments.ComponentInstance;
import ucm_supplement.ucm_deployments.DeploymentModule;
import ucm_supplement.ucm_test_cases.ComponentPortReference;
import ucm_supplement.ucm_test_cases.IComponentInteractionPointReference;
import ucm_supplement.ucm_test_cases.ITestCaseEvent;
import ucm_supplement.ucm_test_cases.TechnicalPolicyReference;
import ucm_supplement.ucm_test_cases.TestCase;
import ucm_supplement.ucm_test_cases.TestCaseGroup;
import ucm_supplement.ucm_test_cases.TestCaseObservation;
import ucm_supplement.ucm_test_cases.util.Ucm_test_casesValidator;

public class UcmTestCaseValidation {

	public boolean componentInstanceLocation(ITestCaseEvent event, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		/*
		 * test whether the component instance is in the same AppAssembly as the
		 * test itself.
		 */
		boolean success = true;
		ComponentInstance instance = event.getComponentInstance();
		try {
			TestCase testCase = (TestCase) event.eContainer();
			TestCaseGroup testCaseGroup = (TestCaseGroup) testCase.eContainer();
			DeploymentModule deployment = (DeploymentModule) testCaseGroup.eContainer();
			AppAssembly testAssembly = deployment.getAssembly();
			AssemblyPart part = instance.getPartReference().getPart();
			AppAssembly instanceAssembly = (AppAssembly) part.eContainer();
			success = instanceAssembly == testAssembly;
		} catch (Exception e) {
			success = false;
		}
		if (!success) {
			if (diagnostics != null) {
				diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_test_casesValidator.DIAGNOSTIC_SOURCE,
						Ucm_test_casesValidator.ITEST_CASE_EVENT__COMPONENT_INSTANCE_LOCATION,
						"Test case event " + event.getName()
								+ " references a component instance that is not in the proper App Assembly",
						new Object[] { event }));
			}
		}
		return success;
	}

	public boolean methodAndPortElementConsistency(ITestCaseEvent event, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		boolean success = true;
		boolean portElementIsCorrect = true;
		boolean interfaceIsCorrect = true;
		boolean methodIsCorrect = true;

		PortElement portElement = event.getPortElement();
		IComponentInteractionPointReference interactionPoint = event.getInteractionPoint();
		/*
		 * If a port element is specified, check if it belongs to the
		 * interaction point (port or policy)
		 */
		if (portElement != null) {
			if (interactionPoint instanceof ComponentPortReference) {
				Port port = ((ComponentPortReference) interactionPoint).getPort();
				IPortSpec spec = port.getSpec();
				if (spec instanceof PortTypeSpec) {
					portElementIsCorrect = ((PortTypeSpec) spec).getType().getPortElement().contains(portElement);
				}
			} else if (interactionPoint instanceof TechnicalPolicyReference) {
				ComponentTechnicalPolicy policy = ((TechnicalPolicyReference) interactionPoint).getTechnicalPolicy();
				portElementIsCorrect = policy.getDefinition().getPortElement().contains(portElement);
			} else {
				portElementIsCorrect = false;
			}
		}

		/*
		 * If there is a port element, retrieve the interface, applying bindings
		 * if need be.
		 */
		Interface actualInterface = null;
		if (portElement != null) {
			IInterface intf = portElement.getInterface();
			if (intf instanceof Interface) {
				actualInterface = (Interface) intf;
			} else if (intf instanceof AbstractInterface) {
				if (interactionPoint instanceof ComponentPortReference) {
					Port port = ((ComponentPortReference) interactionPoint).getPort();
					IPortSpec spec = port.getSpec();
					if (spec instanceof PortTypeSpec) {
						for (AbstractTypeBinding binding : ((PortTypeSpec) spec).getBinding()) {
							if (binding.getAbstractType() == intf && binding.getActualType() instanceof Interface) {
								actualInterface = (Interface) binding.getActualType();
							}
						}
					} else if (spec instanceof PortRoleSpec) {
						for (InteractionItemBinding binding : ((PortRoleSpec) spec).getBinding()) {
							if (binding.getItem() == intf && binding.getActualType() instanceof Interface) {
								actualInterface = (Interface) binding.getActualType();
							}
						}
					}
				} else if (interactionPoint instanceof TechnicalPolicyReference) {
					ComponentTechnicalPolicy policy = ((TechnicalPolicyReference) interactionPoint)
							.getTechnicalPolicy();
					for (AbstractTypeBinding binding : policy.getBinding()) {
						if (binding.getAbstractType() == intf && binding.getActualType() instanceof Interface) {
							actualInterface = (Interface) binding.getActualType();
						}
					}
				}
			}

			interfaceIsCorrect = actualInterface != null;
		}

		/*
		 * check if the method belongs to the interface.
		 */
		if (event.getMethod() == null && event.getPortElement() == null) {
			success = true;
		} else if (event.getMethod() == null && event.getPortElement() != null) {
			success = portElementIsCorrect;
		} else if (event.getMethod() != null && event.getPortElement() == null) {
			methodIsCorrect = false;
			success = false;
		} else {
			methodIsCorrect = actualInterface != null && actualInterface.getMethod().contains(event.getMethod());
			success = portElementIsCorrect && interfaceIsCorrect && methodIsCorrect;
		}
		if (!methodIsCorrect) {
			if (diagnostics != null) {
				diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_test_casesValidator.DIAGNOSTIC_SOURCE,
						Ucm_test_casesValidator.ITEST_CASE_EVENT__METHOD_AND_PORT_ELEMENT_CONSISTENCY,
						"Test case event " + event.getName() + " points to an inconsistent method",
						new Object[] { event, actualInterface, event.getMethod() }));
			}
		}
		if (!interfaceIsCorrect) {
			if (diagnostics != null) {
				diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_test_casesValidator.DIAGNOSTIC_SOURCE,
						Ucm_test_casesValidator.ITEST_CASE_EVENT__METHOD_AND_PORT_ELEMENT_CONSISTENCY,
						"Impossible to retrieve the interface of test case event " + event.getName(),
						new Object[] { event }));
			}
		}
		if (!portElementIsCorrect) {
			if (diagnostics != null) {
				diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_test_casesValidator.DIAGNOSTIC_SOURCE,
						Ucm_test_casesValidator.ITEST_CASE_EVENT__METHOD_AND_PORT_ELEMENT_CONSISTENCY,
						"The port element of " + event.getName() + " is inconsistent",
						new Object[] { event, event.getPortElement() }));
			}
		}
		return success;
	}

	public boolean valueConsistency(TestCaseObservation observation, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		boolean success = true;
		success = observation.getMaxDelay() >= observation.getMinDelay();
		if (!success) {
			if (diagnostics != null) {
				diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_test_casesValidator.DIAGNOSTIC_SOURCE,
						Ucm_test_casesValidator.TEST_CASE_OBSERVATION__VALUE_CONSISTENCY,
						"Test case observation " + observation.getName() + " has inconsistent delays",
						new Object[] { observation }));
			}
		}
		return success;
	}

}
