package com.thalesgroup.ucm.metamodel.validation;

import java.util.LinkedList;
import java.util.Map;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;

import ucm_base.ucm_components.AbstractTypeBinding;
import ucm_base.ucm_components.AssemblyPart;
import ucm_base.ucm_components.AtomicComponentImplementation;
import ucm_base.ucm_components.ComponentTechnicalPolicy;
import ucm_base.ucm_components.ComponentType;
import ucm_base.ucm_components.CompositeComponentImplementation;
import ucm_base.ucm_components.Connection;
import ucm_base.ucm_components.IComponent;
import ucm_base.ucm_components.IComponentImplementation;
import ucm_base.ucm_components.Port;
import ucm_base.ucm_components.PortTypeSpec;
import ucm_base.ucm_contracts.AbstractInterface;
import ucm_base.ucm_contracts.Attribute;
import ucm_base.ucm_contracts.IInterface;
import ucm_base.ucm_contracts.Interface;
import ucm_base.ucm_interactions.PortElement;
import ucm_base.ucm_interactions.PortType;
import ucm_base.ucm_technicalpolicies.TechnicalPolicyDefinition;
import ucm_supplement.ucm_deployments.AllocationPlan;
import ucm_supplement.ucm_deployments.AssemblyPartReference;
import ucm_supplement.ucm_deployments.AttributeValue;
import ucm_supplement.ucm_deployments.ComponentInstance;
import ucm_supplement.ucm_deployments.ComponentInstanceInitialization;
import ucm_supplement.ucm_deployments.ComponentInstancePolicyInitialization;
import ucm_supplement.ucm_deployments.ComponentInstancePortInitialization;
import ucm_supplement.ucm_deployments.ConnectionConfiguration;
import ucm_supplement.ucm_deployments.ConnectionInstance;
import ucm_supplement.ucm_deployments.ConnectionReference;
import ucm_supplement.ucm_deployments.DeploymentModule;
import ucm_supplement.ucm_deployments.TechnicalPolicyConfiguration;
import ucm_supplement.ucm_deployments.util.Ucm_deploymentsValidator;

public class UcmDeploymentValidation {

	public boolean subpartBelongsToPart(AssemblyPartReference partRef, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		boolean success = true;

		AssemblyPartReference subPartRef = partRef.getReferenceToSubpart();
		if (subPartRef != null) {
			AssemblyPart part = partRef.getPart();
			IComponent component = part.getComponentDefinition();
			if (component instanceof CompositeComponentImplementation) {
				AssemblyPart subPart = subPartRef.getPart();
				success = ((CompositeComponentImplementation) component).getPart().contains(subPart);
			}
		}

		if (!success) {
			if (diagnostics != null) {
				diagnostics
						.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_deploymentsValidator.DIAGNOSTIC_SOURCE,
								Ucm_deploymentsValidator.ASSEMBLY_PART_REFERENCE__SUBPART_BELONGS_TO_PART,
								"Part " + subPartRef.getName() + " of " + partRef.getName()
										+ " does not belong to the right component",
								new Object[] { partRef, subPartRef }));
			}
		}
		return success;
	}

	public boolean connectionBelongsToPart(ConnectionReference connectionRef, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		boolean success = true;
		Connection connection = connectionRef.getConnection();
		AssemblyPartReference partRef = connectionRef.getNestingComponentInstance();
		AssemblyPart part = null;
		while (partRef != null) {
			part = partRef.getPart();
			partRef = partRef.getReferenceToSubpart();
		}
		if (part != null) {
			IComponent component = part.getComponentDefinition();
			if (component instanceof CompositeComponentImplementation) {
				success = ((CompositeComponentImplementation) component).getInternalConnection().contains(connection);
			}
		}
		if (!success) {
			if (diagnostics != null) {
				diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_deploymentsValidator.DIAGNOSTIC_SOURCE,
						Ucm_deploymentsValidator.CONNECTION_INSTANCE__CONNECTION_BELONGS_TO_PART,
						connection.getName() + " does not belong to its part", new Object[] { connection }));
			}
		}
		return success;
	}

	public boolean policyManagesComponent(TechnicalPolicyConfiguration policyConfig, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		boolean success = true;
		for (ComponentInstance compInst : policyConfig.getComponentInstance()) {
			AssemblyPartReference partRef = compInst.getPartReference();
			AssemblyPartReference tempPartRef = partRef;
			AssemblyPartReference parentPartRef = partRef;
			while (tempPartRef != null) {
				parentPartRef = tempPartRef;
				tempPartRef = tempPartRef.getReferenceToSubpart();
			}
			AssemblyPart part = parentPartRef.getPart();
			IComponent component = part.getComponentDefinition();
			if (component instanceof AtomicComponentImplementation) {
				ComponentTechnicalPolicy policy = policyConfig.getPolicy();
				if (!((AtomicComponentImplementation) component).getPolicy().contains(policy)) {
					if (diagnostics != null) {
						diagnostics
								.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_deploymentsValidator.DIAGNOSTIC_SOURCE,
										Ucm_deploymentsValidator.TECHNICAL_POLICY_CONFIGURATION__POLICY_MANAGES_COMPONENT,
										"Policy " + policy.getName() + " does not manage " + partRef.getName(),
										new Object[] { policyConfig, policy, partRef }));
					}
					success = false;
				}
			}
		}
		return success;
	}

	public boolean instanceBelongToAppAssembly(TechnicalPolicyConfiguration policyConfig, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		boolean success = true;
		DeploymentModule deployment = null;
		try {
			AllocationPlan allocationPlan = (AllocationPlan) policyConfig.eContainer();
			deployment = (DeploymentModule) allocationPlan.eContainer();
		} catch (Exception e) {
			deployment = null;
		}
		if (deployment != null) {
			for (ComponentInstance compInst : policyConfig.getComponentInstance()) {
				if (!this.instanceBelongToDeployment(compInst, deployment, diagnostics, context)) {
					if (diagnostics != null) {
						diagnostics
								.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_deploymentsValidator.DIAGNOSTIC_SOURCE,
										Ucm_deploymentsValidator.TECHNICAL_POLICY_CONFIGURATION__INSTANCE_BELONG_TO_APP_ASSEMBLY,
										"Component instance " + compInst.getName()
												+ " does not belong to the right deployment module",
										new Object[] { compInst, deployment }));
					}
					success = false;
				}
			}
		}

		return success;
	}

	public boolean instanceBelongToDeployment(ComponentInstance compInst, DeploymentModule deployment,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean success = true;
		DeploymentModule compInstDeployment = (DeploymentModule) compInst.eContainer();
		AssemblyPart part = compInst.getPartReference().getPart();
		success = (part.eContainer() == deployment.getAssembly() && compInstDeployment == deployment);
		return success;
	}

	public boolean instanceBelongToAppAssembly(ComponentInstanceInitialization compInstInit,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean success = true;
		DeploymentModule deployment = null;
		try {
			AllocationPlan allocationPlan = (AllocationPlan) compInstInit.eContainer();
			deployment = (DeploymentModule) allocationPlan.eContainer();
		} catch (Exception e) {
			deployment = null;
		}
		if (deployment != null) {
			ComponentInstance compInst = compInstInit.getComponentInstance();
			if (!this.instanceBelongToDeployment(compInst, deployment, diagnostics, context)) {
				if (diagnostics != null) {
					diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_deploymentsValidator.DIAGNOSTIC_SOURCE,
							Ucm_deploymentsValidator.COMPONENT_INSTANCE_INITIALIZATION__INSTANCE_BELONG_TO_APP_ASSEMBLY,
							"Component instance " + compInst.getName()
									+ " does not belong to the right deployment module",
							new Object[] { compInstInit, compInst }));
				}
				success = false;
			}
		} else {
			success = false;
		}

		return success;
	}

	public boolean instanceBelongToAppAssembly(ConnectionConfiguration connectionConfig, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		boolean success = true;
		DeploymentModule deployment = null;
		try {
			AllocationPlan allocationPlan = (AllocationPlan) connectionConfig.eContainer();
			deployment = (DeploymentModule) allocationPlan.eContainer();
		} catch (Exception e) {
			deployment = null;
		}
		if (deployment != null) {
			ConnectionInstance cntInst = connectionConfig.getConnection();
			AssemblyPartReference partRef = cntInst.getPartReference();
			Connection connection = cntInst.getConnection();
			if (partRef == null) {
				if (connection.eContainer() != deployment.getAssembly()) {
					if (diagnostics != null) {
						diagnostics
								.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_deploymentsValidator.DIAGNOSTIC_SOURCE,
										Ucm_deploymentsValidator.CONNECTION_CONFIGURATION__INSTANCE_BELONG_TO_APP_ASSEMBLY,
										"Connection " + connection.getName() + " is not in the correct Assembly",
										new Object[] { connectionConfig, connection }));
					}
					success = false;
				}
			} else {
				AssemblyPart part = partRef.getPart();
				if (part.eContainer() != deployment.getAssembly()) {
					if (diagnostics != null) {
						diagnostics
								.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_deploymentsValidator.DIAGNOSTIC_SOURCE,
										Ucm_deploymentsValidator.CONNECTION_CONFIGURATION__INSTANCE_BELONG_TO_APP_ASSEMBLY,
										"Part " + part.getName() + " is not in the correct Assembly",
										new Object[] { connectionConfig, part }));
					}
					success = false;
				}
			}
		}
		return success;
	}

	public boolean attributeBelongsToComponent(ComponentInstanceInitialization compInstInit,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean success = false;
		ComponentType compType;
		LinkedList<Attribute> attributeList = new LinkedList<Attribute>();
		/*
		 * Retrieve the list of attributes from the component type and its
		 * ancestors
		 */
		ComponentInstance compInst = compInstInit.getComponentInstance();
		AssemblyPartReference partRef = compInst.getPartReference();
		while (partRef.getReferenceToSubpart() != null) {
			partRef = partRef.getReferenceToSubpart();
		}
		if (partRef.getPart() != null && partRef.getPart().getComponentDefinition() instanceof ComponentType) {
			compType = (ComponentType) partRef.getPart().getComponentDefinition();
			while (compType != null) {
				attributeList.addAll(compType.getAttribute());
				compType = compType.getRefines();
			}
		} else if (partRef.getPart() != null
				&& partRef.getPart().getComponentDefinition() instanceof IComponentImplementation) {
			compType = ((IComponentImplementation) partRef.getPart().getComponentDefinition()).getType();
			while (compType != null) {
				attributeList.addAll(compType.getAttribute());
				compType = compType.getRefines();
			}
		}

		/*
		 * Check every attribute value references an attribute in the list
		 */
		for (AttributeValue attrValue : compInstInit.getAttributeInitialization()) {
			Attribute attr = attrValue.getAttribute();
			if (!attributeList.contains(attr)) {
				if (diagnostics != null) {
					diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_deploymentsValidator.DIAGNOSTIC_SOURCE,
							Ucm_deploymentsValidator.COMPONENT_INSTANCE_INITIALIZATION__ATTRIBUTE_BELONGS_TO_COMPONENT,
							"Attribute " + attr.getName() + " is not associated with component "
									+ compInstInit.getName(),
							new Object[] { compInstInit, attrValue }));
				}
				success = false;
			}
		}

		return success;
	}

	public boolean attributeBelongsToPolicy(ComponentInstancePolicyInitialization policyInit,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean success = false;
		AtomicComponentImplementation atomicImpl = null;
		ComponentInstanceInitialization compInstInit = (ComponentInstanceInitialization) policyInit.eContainer();
		ComponentTechnicalPolicy policy = policyInit.getPolicy();
		TechnicalPolicyDefinition policyDef = policy.getDefinition();
		PortElement portElement = policyInit.getPortElement();

		/*
		 * Ensure the policy is associated with the component instance
		 */
		ComponentInstance compInst = compInstInit.getComponentInstance();
		AssemblyPartReference partRef = compInst.getPartReference();
		while (partRef.getReferenceToSubpart() != null) {
			partRef = partRef.getReferenceToSubpart();
		}
		if (partRef.getPart().getComponentDefinition() instanceof AtomicComponentImplementation) {
			atomicImpl = (AtomicComponentImplementation) partRef.getPart().getComponentDefinition();
		} else {
			if (diagnostics != null) {
				diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_deploymentsValidator.DIAGNOSTIC_SOURCE,
						Ucm_deploymentsValidator.COMPONENT_INSTANCE_POLICY_INITIALIZATION__ATTRIBUTE_BELONGS_TO_POLICY,
						"Component instance " + compInstInit.getName() + " is not an atomic implementation",
						new Object[] { compInstInit }));
			}
			success = false;
		}

		if (atomicImpl != null) {
			if (!policy.getManagedComponent().contains(atomicImpl)) {
				if (diagnostics != null) {
					diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_deploymentsValidator.DIAGNOSTIC_SOURCE,
							Ucm_deploymentsValidator.COMPONENT_INSTANCE_POLICY_INITIALIZATION__ATTRIBUTE_BELONGS_TO_POLICY,
							"Policy " + policyInit.getName() + " is not related with the component instance",
							new Object[] { policyInit }));
				}
				success = false;
			}
		}

		/*
		 * retrieve all the port elements of the policy and check the port
		 * element to initialize is in this list
		 */
		LinkedList<PortElement> policyPortElements = new LinkedList<PortElement>();
		while (policyDef != null) {
			policyPortElements.addAll(policyDef.getPortElement());
			policyDef = policyDef.getExtends();
		}
		if (!policyPortElements.contains(portElement)) {
			if (diagnostics != null) {
				diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_deploymentsValidator.DIAGNOSTIC_SOURCE,
						Ucm_deploymentsValidator.COMPONENT_INSTANCE_POLICY_INITIALIZATION__ATTRIBUTE_BELONGS_TO_POLICY,
						"Policy " + policyInit.getName() + " does not contain port element " + portElement.getName(),
						new Object[] { policyInit, portElement }));
			}
			success = false;
		}

		/*
		 * Retrieve the list of attributes from the interface of the port
		 * element and check the attribute is in the list. To do so, first apply
		 * the abstract type binding if need be to retrieve the actual
		 * interface, then process each attribute value
		 */
		Interface actualInterface = null;
		IInterface intf = portElement.getInterface();
		if (intf instanceof Interface) {
			actualInterface = (Interface) intf;
		} else if (intf instanceof AbstractInterface) {
			for (AbstractTypeBinding binding : policy.getBinding()) {
				if (binding.getAbstractType() == intf) {
					actualInterface = (Interface) binding.getActualType();
				}
			}
		}
		if (actualInterface != null) {
			for (AttributeValue attrValue : policyInit.getAttributeInitialization()) {
				if (!actualInterface.getAttribute().contains(attrValue.getAttribute())) {
					if (diagnostics != null) {
						diagnostics
								.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_deploymentsValidator.DIAGNOSTIC_SOURCE,
										Ucm_deploymentsValidator.COMPONENT_INSTANCE_POLICY_INITIALIZATION__ATTRIBUTE_BELONGS_TO_POLICY,
										"Attribute " + attrValue.getAttribute() + " does not belong to port element "
												+ portElement.getName(),
										new Object[] { policyInit, attrValue, portElement }));
					}
					success = false;
				}
			}
		}

		return success;
	}

	public boolean attributeBelongsToPort(ComponentInstancePortInitialization portInit, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		boolean success = true;
		ComponentType compType;
		LinkedList<AbstractTypeBinding> bindings = new LinkedList<AbstractTypeBinding>();
		LinkedList<Port> ports = new LinkedList<Port>();
		ComponentInstanceInitialization compInstInit = (ComponentInstanceInitialization) portInit.eContainer();
		/*
		 * check the port belongs to the component instance
		 */
		ComponentInstance compInst = compInstInit.getComponentInstance();
		AssemblyPartReference partRef = compInst.getPartReference();
		while (partRef.getReferenceToSubpart() != null) {
			partRef = partRef.getReferenceToSubpart();
		}
		if (partRef.getPart() != null && partRef.getPart().getComponentDefinition() instanceof ComponentType) {
			compType = (ComponentType) partRef.getPart().getComponentDefinition();
			while (compType != null) {
				ports.addAll(compType.getPort());
				compType = compType.getRefines();
			}
		} else if (partRef.getPart() != null
				&& partRef.getPart().getComponentDefinition() instanceof IComponentImplementation) {
			compType = ((IComponentImplementation) partRef.getPart().getComponentDefinition()).getType();
			while (compType != null) {
				ports.addAll(compType.getPort());
				compType = compType.getRefines();
			}
		}

		if (!ports.contains(portInit.getPort())) {
			if (diagnostics != null) {
				diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_deploymentsValidator.DIAGNOSTIC_SOURCE,
						Ucm_deploymentsValidator.COMPONENT_INSTANCE_PORT_INITIALIZATION__ATTRIBUTE_BELONGS_TO_PORT,
						"Port " + portInit.getPort().getName() + " does not belong to component instance "
								+ compInstInit.getName(),
						new Object[] { portInit }));
			}
			success = false;
		}

		/*
		 * check the port elements actually belong to the port
		 */
		if (portInit.getPort().getSpec() instanceof PortTypeSpec) {
			PortType portType = ((PortTypeSpec) portInit.getPort().getSpec()).getType();
			bindings.addAll(((PortTypeSpec) portInit.getPort().getSpec()).getBinding());
			if (!portType.getPortElement().contains(portInit.getPortElement())) {
				if (diagnostics != null) {
					diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_deploymentsValidator.DIAGNOSTIC_SOURCE,
							Ucm_deploymentsValidator.COMPONENT_INSTANCE_PORT_INITIALIZATION__ATTRIBUTE_BELONGS_TO_PORT,
							"Port element " + portInit.getPortElement().getName() + " does not belong to port "
									+ portInit.getPort().getName(),
							new Object[] { portInit, portInit.getPortElement(), portInit.getPort() }));
				}
				success = false;
			}
		} else {
			if (diagnostics != null) {
				diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_deploymentsValidator.DIAGNOSTIC_SOURCE,
						Ucm_deploymentsValidator.COMPONENT_INSTANCE_PORT_INITIALIZATION__ATTRIBUTE_BELONGS_TO_PORT,
						"Port type spec of " + portInit.getPort().getName() + " is not managed ",
						new Object[] { portInit, portInit.getPort() }));
			}
			success = false;
		}

		/*
		 * Retrieve the list of attributes from the interface of the port
		 * element and check the attribute is in the list. To do so, first apply
		 * the abstract type bindings if need be to retrieve the actual
		 * interfaces, then process each attribute value. The can be several
		 * bindings, are the port type specification can be multiplex
		 */
		LinkedList<Interface> actualInterfaces = new LinkedList<Interface>();
		IInterface intf = portInit.getPortElement().getInterface();
		if (intf instanceof Interface) {
			actualInterfaces.add((Interface) intf);
		} else if (intf instanceof AbstractInterface) {
			for (AbstractTypeBinding binding : bindings) {
				if (binding.getAbstractType() == intf) {
					actualInterfaces.add((Interface) binding.getActualType());
				}
			}
		}

		for (AttributeValue attrValue : portInit.getAttributeInitialization()) {
			boolean interfaceContainsAttribute = false;
			for (Interface actualInterface : actualInterfaces) {
				if (actualInterface.getAttribute().contains(attrValue.getAttribute())) {
					interfaceContainsAttribute = true;
				}
			}
			if (!interfaceContainsAttribute) {
				if (diagnostics != null) {
					diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_deploymentsValidator.DIAGNOSTIC_SOURCE,
							Ucm_deploymentsValidator.COMPONENT_INSTANCE_PORT_INITIALIZATION__ATTRIBUTE_BELONGS_TO_PORT,
							"Attribute " + attrValue.getAttribute().getName() + " does not belong to "
									+ portInit.getPortElement().getName(),
							new Object[] { attrValue, portInit.getPortElement().getName() }));
				}
				success = false;
			}
		}
		return success;
	}

	public boolean instancesBelongToTheSameDeployment(AllocationPlan allocationPlan, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		boolean success = true;
		DeploymentModule deployment = null;
		/*
		 * If the allocation plan is nested in a deployment module, this
		 * deployment will be used for reference.
		 */
		if (allocationPlan.eContainer() instanceof DeploymentModule) {
			deployment = (DeploymentModule) allocationPlan.eContainer();
		}
		/*
		 * Check all component instance initializations reference part
		 * references that are in the same deployment. If the deployment is
		 * already set, use it; else use the first deployment we find.
		 */
		for (ComponentInstanceInitialization compInit : allocationPlan.getComponentInstanceInitialization()) {
			if (deployment == null) {
				deployment = (DeploymentModule) compInit.getComponentInstance().eContainer();
			}
			if (!this.instanceBelongToDeployment(compInit.getComponentInstance(), deployment, diagnostics, context)) {
				if (diagnostics != null) {
					diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_deploymentsValidator.DIAGNOSTIC_SOURCE,
							Ucm_deploymentsValidator.ALLOCATION_PLAN__INSTANCES_BELONG_TO_THE_SAME_DEPLOYMENT,
							"Component instance " + compInit.getName() + " does not belong to the right deployment",
							new Object[] { compInit, deployment }));
				}
				success = false;
			}
		}

		/*
		 * Same thing for connection configurations
		 */
		for (ConnectionConfiguration connConf : allocationPlan.getConnectionConfiguration()) {
			if (deployment == null) {
				deployment = (DeploymentModule) connConf.getConnection().eContainer();
			}
			AssemblyPartReference partRef = connConf.getConnection().getPartReference();
			if (!(partRef != null && (partRef.getPart().eContainer() == deployment.getAssembly()))) {
				if (diagnostics != null) {
					diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_deploymentsValidator.DIAGNOSTIC_SOURCE,
							Ucm_deploymentsValidator.ALLOCATION_PLAN__INSTANCES_BELONG_TO_THE_SAME_DEPLOYMENT,
							"Component instance of " + connConf.getName() + " does not belong to the right deployment",
							new Object[] { connConf, deployment }));
				}
				success = false;
			}
		}
		return success;
	}
}
