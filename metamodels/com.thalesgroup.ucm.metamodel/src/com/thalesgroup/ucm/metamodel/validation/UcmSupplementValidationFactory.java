package com.thalesgroup.ucm.metamodel.validation;

import org.omg.ucm.metamodel.validation.UcmBaseValidationFactory;

public class UcmSupplementValidationFactory extends UcmBaseValidationFactory {
	public static UcmSupplementValidationFactory INSTANCE = new UcmSupplementValidationFactory();

	protected UcmTestCaseValidation testCaseValidator = new UcmTestCaseValidation();

	protected UcmDetailedComponentImplementationValidation detailedComponentValidator = new UcmDetailedComponentImplementationValidation();

	protected UcmExtendedContractValidation extendedContractValidator = new UcmExtendedContractValidation();

	protected UcmDeploymentValidation deploymentValidator = new UcmDeploymentValidation();

	protected UcmResourceValidation resourceValidator = new UcmResourceValidation();
	
	public UcmTestCaseValidation getTestCaseValidator() {
		return this.testCaseValidator;
	}

	public UcmExtendedContractValidation getContractValidator() {
		return this.extendedContractValidator;
	}

	public UcmDetailedComponentImplementationValidation getDetailedComponentValidator() {
		return this.detailedComponentValidator;
	}

	public UcmDeploymentValidation getDeploymentValidator() {
		return this.deploymentValidator;
	}

	public UcmResourceValidation getResourceValidator() {
		return this.resourceValidator;
	}
	
}
