package com.thalesgroup.ucm.metamodel.validation;

import java.util.LinkedList;
import java.util.Map;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;

import ucm_base.ucm_components.AbstractTypeBinding;
import ucm_base.ucm_components.ComponentTechnicalPolicy;
import ucm_base.ucm_components.ComponentType;
import ucm_base.ucm_components.Port;
import ucm_base.ucm_components.PortTypeSpec;
import ucm_base.ucm_contracts.IInterface;
import ucm_base.ucm_contracts.Interface;
import ucm_base.ucm_contracts.Method;
import ucm_base.ucm_interactions.PortElement;
import ucm_base.ucm_interactions.PortType;
import ucm_base.ucm_technicalpolicies.TechnicalPolicyDefinition;
import ucm_supplement.ucm_detailed_components.ComponentPortImplementation;
import ucm_supplement.ucm_detailed_components.ComputationStep;
import ucm_supplement.ucm_detailed_components.DetailedComponentImplementation;
import ucm_supplement.ucm_detailed_components.PortAndMethod;
import ucm_supplement.ucm_detailed_components.TechnicalPortImplementation;
import ucm_supplement.ucm_detailed_components.util.Ucm_detailed_componentsValidator;

public class UcmDetailedComponentImplementationValidation {

	public boolean valueConsistency(ComputationStep step, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean success = true;
		success = (step.getEvaluatedBCET() <= step.getEvaluatedWCET())
				&& (step.getMeasuredBCET() <= step.getMeasuredWCET())
				&& (step.getSpecifiedBCET() <= step.getSpecifiedWCET());
		if (!success) {
			if (diagnostics != null) {
				diagnostics
						.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_detailed_componentsValidator.DIAGNOSTIC_SOURCE,
								Ucm_detailed_componentsValidator.COMPUTATION_STEP__VALUE_CONSISTENCY,
								"Step " + step.getName() + " has inconsistent execution times", new Object[] { this }));
			}
		}
		return success;
	}

	public boolean consistency(PortAndMethod portAndMethod, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean success = true;
		Method method = portAndMethod.getMethod();

		/*
		 * Retrieve the interface, applying type bindings if need be
		 */
		IInterface intf = portAndMethod.getPortImplementation().getPortElement().getInterface();
		Interface actualInterface = null;

		if (intf instanceof Interface) {
			actualInterface = (Interface) intf;
		} else {
			if (portAndMethod.getPortImplementation() instanceof ComponentPortImplementation) {
				Port port = ((ComponentPortImplementation) portAndMethod.getPortImplementation()).getPort();
				if (port.getSpec() instanceof PortTypeSpec) {
					for (AbstractTypeBinding binding : ((PortTypeSpec) port.getSpec()).getBinding()) {
						if (binding.getAbstractType() == intf) {
							actualInterface = (Interface) binding.getActualType();
						}
					}
				}
			} else if (portAndMethod.getPortImplementation() instanceof TechnicalPortImplementation) {
				ComponentTechnicalPolicy policy = ((TechnicalPortImplementation) portAndMethod.getPortImplementation())
						.getPolicy();
				for (AbstractTypeBinding binding : policy.getBinding()) {
					if (binding.getAbstractType() == intf) {
						actualInterface = (Interface) binding.getActualType();
					}
				}
			}
		}

		success = actualInterface.getMethod().contains(method);

		if (!success) {
			if (diagnostics != null) {
				diagnostics
						.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_detailed_componentsValidator.DIAGNOSTIC_SOURCE,
								Ucm_detailed_componentsValidator.PORT_AND_METHOD__CONSISTENCY,
								"Method " + method.getName() + " does not belong to port element "
										+ portAndMethod.getPortImplementation().getPortElement().getName(),
								new Object[] { portAndMethod }));
			}
		}
		return success;
	}

	public boolean portImplementationConsistency(TechnicalPortImplementation portImpl, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		boolean success = true;
		/*
		 * check the policy is associated with the component implementation
		 */
		ComponentTechnicalPolicy policy = portImpl.getPolicy();
		DetailedComponentImplementation compImpl = (DetailedComponentImplementation) portImpl.eContainer();
		if (!compImpl.getPolicy().contains(policy)) {
			if (diagnostics != null) {
				diagnostics
						.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_detailed_componentsValidator.DIAGNOSTIC_SOURCE,
								Ucm_detailed_componentsValidator.TECHNICAL_PORT_IMPLEMENTATION__PORT_IMPLEMENTATION_CONSISTENCY,
								"Policy " + policy.getName() + " does not belong to the component ",
								new Object[] { portImpl }));
			}
			success = false;
		}

		/*
		 * Check the port element belongs to the policy
		 */
		LinkedList<PortElement> portElements = new LinkedList<PortElement>();
		TechnicalPolicyDefinition policyDef = policy.getDefinition();
		while (policyDef != null) {
			portElements.addAll(policyDef.getPortElement());
			policyDef = policyDef.getExtends();
		}

		if (!portElements.contains(portImpl.getPortElement())) {
			if (diagnostics != null) {
				diagnostics
						.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_detailed_componentsValidator.DIAGNOSTIC_SOURCE,
								Ucm_detailed_componentsValidator.TECHNICAL_PORT_IMPLEMENTATION__PORT_IMPLEMENTATION_CONSISTENCY,
								"Port element " + portImpl.getPortElement().getName() + " does not belong to policy "
										+ policy.getName(),
								new Object[] { portImpl }));
			}
			success = false;
		}
		return success;
	}

	public boolean portImplementationConsistency(ComponentPortImplementation portImpl, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		boolean success = true;
		/*
		 * check the port belongs to the component type and its ancestors,
		 * ignoring the refined ports
		 */
		Port port = portImpl.getPort();
		LinkedList<Port> ports = new LinkedList<Port>();
		LinkedList<Port> refinedPorts = new LinkedList<Port>();
		LinkedList<PortElement> portElements = new LinkedList<PortElement>();
		DetailedComponentImplementation compImpl = (DetailedComponentImplementation) portImpl.eContainer();
		ComponentType compType = compImpl.getType();
		while (compType != null) {
			for (Port tempPort : compType.getPort()) {
				if (!refinedPorts.contains(tempPort)) {
					ports.add(tempPort);
					if (port.getSpec() instanceof PortTypeSpec) {
						PortTypeSpec spec = (PortTypeSpec) port.getSpec();
						PortType portType = spec.getType();
						portElements.addAll(portType.getPortElement());
					}
				}
				if (tempPort.getRefines() != null) {
					refinedPorts.add(tempPort.getRefines());
				}
			}
			compType = compType.getRefines();
		}

		if (!ports.contains(port)) {
			if (diagnostics != null) {
				diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR,
						Ucm_detailed_componentsValidator.DIAGNOSTIC_SOURCE,
						Ucm_detailed_componentsValidator.COMPONENT_PORT_IMPLEMENTATION__PORT_IMPLEMENTATION_CONSISTENCY,
						"Port " + port.getName() + " does not belong to the component ", new Object[] { portImpl }));
			}
			success = false;
		}

		/*
		 * Check the port element belongs to the port type
		 */
		if (!portElements.contains(portImpl.getPortElement())) {
			if (diagnostics != null) {
				diagnostics
						.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_detailed_componentsValidator.DIAGNOSTIC_SOURCE,
								Ucm_detailed_componentsValidator.COMPONENT_PORT_IMPLEMENTATION__PORT_IMPLEMENTATION_CONSISTENCY,
								"Port element " + portImpl.getPortElement().getName() + " does not belong to port "
										+ port.getName(),
								new Object[] { portImpl }));
			}
			success = false;
		}
		return success;
	}
}
