package com.thalesgroup.ucm.metamodel.validation;

import java.util.LinkedList;
import java.util.Map;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.omg.ucm.metamodel.validation.UcmContractValidation;

import ucm_base.ucm_components.ComponentTechnicalPolicy;
import ucm_base.ucm_components.Connection;
import ucm_base.ucm_components.ConnectionEnd;
import ucm_base.ucm_components.Port;
import ucm_base.ucm_contracts.IConfigurable;
import ucm_base.ucm_contracts.IConfigured;
import ucm_base.ucm_technicalpolicies.TechnicalPolicyDefinition;
import ucm_environment.ucm_resources.CommunicationPort;
import ucm_environment.ucm_resources.CommunicationPortDefinition;
import ucm_environment.ucm_resources.CommunicationResource;
import ucm_environment.ucm_resources.CommunicationResourceDefinition;
import ucm_environment.ucm_resources.CommunicationResourcePort;
import ucm_environment.ucm_resources.ComputationResource;
import ucm_environment.ucm_resources.ComputationResourceDefinition;
import ucm_environment.ucm_resources.ComputationResourcePort;
import ucm_environment.ucm_resources.ExecutionResource;
import ucm_environment.ucm_resources.ExecutionResourceDefinition;
import ucm_environment.ucm_resources.IEnvironmentResource;
import ucm_environment.ucm_resources.IEnvironmentResourceDefinition;
import ucm_environment.ucm_resources.MemoryPartition;
import ucm_environment.ucm_resources.MemoryPartitionDefinition;
import ucm_environment.ucm_resources.ResourceAllocationParameter;
import ucm_environment.ucm_resources.ResourceAllocationParameterValue;
import ucm_environment.ucm_resources.util.Ucm_resourcesValidator;
import ucm_supplement.ucm_deployments.ConnectionConfiguration;
import ucm_supplement.ucm_deployments.ConnectionEndConfiguration;
import ucm_supplement.ucm_deployments.ConnectionInstance;
import ucm_supplement.ucm_deployments.TechnicalPolicyConfiguration;
import ucm_supplement.ucm_enhanced_configuration.PolicyDependencyParameter;
import ucm_supplement.ucm_enhanced_configuration.PolicyDependencyParameterValue;
import ucm_supplement.ucm_enhanced_configuration.util.Ucm_enhanced_configurationValidator;

public class UcmExtendedContractValidation extends UcmContractValidation {

	@Override
	public LinkedList<IConfigurable> getConfigurables(IConfigured configured) {
		LinkedList<IConfigurable> list = super.getConfigurables(configured);

		if (configured instanceof ExecutionResource) {
			ExecutionResourceDefinition def = ((ExecutionResource) configured).getDefinition();
			list.add(def);
		} else if (configured instanceof MemoryPartition) {
			MemoryPartitionDefinition def = ((MemoryPartition) configured).getMemoryPartitionDefinition();
			list.add(def);
		} else if (configured instanceof ComputationResource) {
			ComputationResourceDefinition def = ((ComputationResource) configured).getDefinition();
			list.add(def);
		} else if (configured instanceof CommunicationResource) {
			CommunicationResourceDefinition def = ((CommunicationResource) configured).getDefinition();
			list.add(def);
		} else if (configured instanceof CommunicationPort) {
			ComputationResourcePort computationPort = ((CommunicationPort) configured).getComputationPort();
			CommunicationResource communicationRsc = (CommunicationResource) configured.eContainer();
			CommunicationResourceDefinition communicationRscDef = communicationRsc.getDefinition();
			CommunicationPortDefinition def = computationPort.getDefinition();
			for (CommunicationResourcePort commPort : communicationRscDef.getCommunicationPort()) {
				if (commPort.getDefinition() == def) {
					list.add(commPort.getDefinition());
				}
			}
			list.add(def);
			list.add(computationPort);
		} else if (configured instanceof ConnectionConfiguration) {
			ConnectionInstance connectionRef = ((ConnectionConfiguration) configured).getConnection();
			Connection connection = connectionRef.getConnection();
			list.addAll(super.getConfigurables(connection));
		} else if (configured instanceof TechnicalPolicyConfiguration) {
			ComponentTechnicalPolicy policy = ((TechnicalPolicyConfiguration) configured).getPolicy();
			list.addAll(super.getConfigurables(policy));
		} else if (configured instanceof ConnectionEndConfiguration) {
			ConnectionConfiguration connectionConfig = (ConnectionConfiguration) ((ConnectionEnd) configured)
					.eContainer();
			Connection connection = connectionConfig.getConnection().getConnection();
			Port port = ((ConnectionEndConfiguration) configured).getPort();
			list.addAll(super.getConnectorPortConfigurations(connection, port));
		}
		return list;
	}

	public boolean parameterDefinitionConsistency(ResourceAllocationParameterValue paramValue,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean success = false;
		IConfigured configured = (IConfigured) paramValue.eContainer();
		ResourceAllocationParameter configParam = paramValue.getParameterDefinition();
		if (configured != null) {
			LinkedList<IConfigurable> configurables = this.getConfigurables(configured);
			for (IConfigurable configurable : configurables) {
				if (configurable.getConfigurationParameter().contains(configParam)) {
					success = true;
				}
			}
		}
		if (!success) {
			if (diagnostics != null) {
				diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_resourcesValidator.DIAGNOSTIC_SOURCE,
						Ucm_resourcesValidator.RESOURCE_ALLOCATION_PARAMETER_VALUE__PARAMETER_DEFINITION_CONSISTENCY,
						"The type of configuration parameter value " + paramValue.getResource().getName()
								+ " cannot be found at the proper location",
						new Object[] { paramValue }));
			}
		}
		/*
		 * If we found the definition of the resource allocation parameter, we
		 * have to ensure the resource is consistent with the allowed
		 * definitions
		 */
		else {
			IEnvironmentResource resource = paramValue.getResource();
			IEnvironmentResourceDefinition resourceDefinition = this.getResourceDefinition(resource);
			if (!configParam.getAllowedResourceDefinition().contains(resourceDefinition)) {
				if (diagnostics != null) {
					diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_resourcesValidator.DIAGNOSTIC_SOURCE,
							Ucm_resourcesValidator.RESOURCE_ALLOCATION_PARAMETER_VALUE__PARAMETER_DEFINITION_CONSISTENCY,
							"The type of configuration parameter value " + paramValue.getResource().getName()
									+ " is not allowed by " + configParam.getName(),
							new Object[] { paramValue, configParam }));
				}
				success = false;
			}
		}
		return success;
	}

	protected IEnvironmentResourceDefinition getResourceDefinition(IEnvironmentResource resource) {
		IEnvironmentResourceDefinition def = null;
		if (resource instanceof CommunicationResource) {
			def = ((CommunicationResource) resource).getDefinition();
		} else if (resource instanceof ComputationResource) {
			def = ((ComputationResource) resource).getDefinition();
		} else if (resource instanceof CommunicationPort) {
			def = ((CommunicationPort) resource).getComputationPort().getDefinition();
		} else if (resource instanceof ExecutionResource) {
			def = ((ExecutionResource) resource).getDefinition();
		} else if (resource instanceof MemoryPartition) {
			def = ((MemoryPartition) resource).getMemoryPartitionDefinition();
		}
		return def;
	}

	public boolean parameterDefinitionConsistency(PolicyDependencyParameterValue paramValue,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean success = false;
		IConfigured configured = (IConfigured) paramValue.eContainer();
		PolicyDependencyParameter configParam = paramValue.getParameterDefinition();
		if (configured != null) {
			LinkedList<IConfigurable> configurables = this.getConfigurables(configured);
			for (IConfigurable configurable : configurables) {
				if (configurable.getConfigurationParameter().contains(configParam)) {
					success = true;
				}
			}
		}
		if (!success) {
			if (diagnostics != null) {
				diagnostics.add(
						new BasicDiagnostic(Diagnostic.ERROR, Ucm_enhanced_configurationValidator.DIAGNOSTIC_SOURCE,
								Ucm_enhanced_configurationValidator.POLICY_DEPENDENCY_PARAMETER_VALUE__PARAMETER_DEFINITION_CONSISTENCY,
								"The type of configuration parameter value " + paramValue.getPolicy().getName()
								+ " is not allowed by " + configParam.getName(),
								new Object[] { paramValue, configParam }));
			}
		}
		/*
		 * If we found the definition of the policy dependency parameter, we
		 * have to ensure the resource is consistent with the allowed
		 * definitions
		 */
		else {
			ComponentTechnicalPolicy policy = paramValue.getPolicy();
			TechnicalPolicyDefinition policyDefinition = policy.getDefinition();
			if (!configParam.getAllowedPolicyDefinition().contains(policyDefinition)) {
				if (diagnostics != null) {
					diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, Ucm_resourcesValidator.DIAGNOSTIC_SOURCE,
							Ucm_enhanced_configurationValidator.POLICY_DEPENDENCY_PARAMETER_VALUE__PARAMETER_DEFINITION_CONSISTENCY,
							"The type of configuration parameter value " + paramValue.getParameterDefinition().getName()
									+ " is not allowed by " + configParam.getName(),
							new Object[] { paramValue, configParam }));
				}
				success = false;
			}
		}
		return success;
	}
}
