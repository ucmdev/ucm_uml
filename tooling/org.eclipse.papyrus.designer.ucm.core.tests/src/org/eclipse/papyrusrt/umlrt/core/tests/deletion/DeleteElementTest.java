/*****************************************************************************
 * Copyright (c) 2015 CEA LIST and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Onder Gurcan (CEA LIST) - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrusrt.umlrt.core.tests.deletion;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.UnexecutableCommand;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.RollbackException;
import org.eclipse.emf.transaction.Transaction;
import org.eclipse.emf.transaction.impl.InternalTransactionalEditingDomain;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.IHintedType;
import org.eclipse.gmf.runtime.emf.type.core.commands.CreateElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.GetEditContextRequest;
import org.eclipse.papyrus.infra.core.services.ServiceException;
import org.eclipse.papyrus.infra.services.edit.internal.context.TypeContext;
import org.eclipse.papyrus.infra.services.edit.service.ElementEditServiceUtils;
import org.eclipse.papyrus.infra.services.edit.service.IElementEditService;
import org.eclipse.papyrus.infra.services.edit.utils.IRequestCacheEntries;
import org.eclipse.papyrusrt.umlrt.core.tests.AbstractPapyrusRTCoreTest;
import org.eclipse.papyrusrt.umlrt.core.tests.utils.IDeleteValidationRule;
import org.eclipse.uml2.uml.Element;
import org.junit.Assert;
import org.junit.Test;

/**
 * Test deletion of UML-RT elements (Deletion / Undo / Redo)
 */
@SuppressWarnings("restriction")
public class DeleteElementTest extends AbstractPapyrusRTCoreTest {

	@Test
	public void testDeleteProtocolInModel() throws Exception {
		// runDeletionTest(rootModel, UCMElementTypesEnumerator.INTERACTION_PATTERN, true, DeleteProtocolFromModelValidation.class);
	}

	protected void runDeletionTest(Element owner, IHintedType hintedType, boolean canCreate, Class<? extends IDeleteValidationRule> validationClass) throws Exception {
		// create the element
		CreateElementCommand commandCreate = getCreateChildCommand(owner, hintedType, canCreate);
		commandCreate.execute(null, null);
		Element deletedElement = (Element) commandCreate.getNewElement();
		// the new element should not be null
		Assert.assertNotNull(deletedElement);
		// and it should be in the model
		// Assert.assertTrue(ownerModel.eContents().contains(newElement));

		// delete the created element
		Command commandDelete = getDeleteChildCommand(deletedElement, false);
		if (commandDelete != null && commandDelete.canExecute()) {
			transactionalEditingDomain.getCommandStack().execute(commandDelete);

			IDeleteValidationRule validationRule = validationClass.newInstance();
			Object[] commandResults = commandDelete.getResult().toArray();
			// the element should not be in the model
			validationRule.validatePostEdition(owner, deletedElement, commandResults);

			// undo the delete command
			transactionalEditingDomain.getCommandStack().undo();
			// the element should be in the model
			validationRule.validatePostUndo(owner, deletedElement, commandResults);

			// redo the delete command
			transactionalEditingDomain.getCommandStack().redo();
			// the element should not be in the model
			validationRule.validatePostEdition(owner, deletedElement, commandResults);

			// undo the delete command
			transactionalEditingDomain.getCommandStack().undo();
			// the element should be in the model
			validationRule.validatePostUndo(owner, deletedElement, commandResults);

		} else {
			fail("Delete Command should be executable");
		}

		commandCreate.undo(null, null);
	}

	/**
	 * Creates the element inside the given owner element, undo and redo the
	 * action
	 *
	 * @param owner
	 *            owner of the new element
	 * @param hintedType
	 *            type of the new element
	 * @param canCreate
	 *            <code>true</code> if new element can be created in the
	 *            specified owner
	 */
	protected CreateElementCommand getCreateChildCommand(EObject owner, IHintedType hintedType, boolean canCreate)
			throws Exception {


		CreateElementCommand command = new CreateElementCommand(new CreateElementRequest(owner, hintedType));
		assertNotNull("Command should not be null", command);
		assertTrue("Command should be executable", command.canExecute());
		return command;
	}

	/**
	 * Deletes the element in the given owner element, undo and redo the action
	 *
	 * @param elementToDestroy
	 *            the element to delete
	 * @param canCreate
	 *            <code>true</code> if new element can be created in the
	 *            specified owner
	 */
	protected Command getDeleteChildCommand(EObject elementToDestroy, boolean confirmationRequired) throws Exception {
		GetEditContextRequest editContextRequest = new GetEditContextRequest(transactionalEditingDomain, new DestroyElementRequest(transactionalEditingDomain, elementToDestroy, confirmationRequired), elementToDestroy);

		editContextRequest.setParameter(IRequestCacheEntries.Cache_Maps, new HashMap<>());
		editContextRequest.setEditContext(elementToDestroy);
		try {
			editContextRequest.setClientContext(TypeContext.getContext());
		} catch (ServiceException e) {
			Assert.fail(e.getMessage());
		}

		IElementEditService provider = ElementEditServiceUtils.getCommandProvider(elementToDestroy);
		if (provider == null) {
			Assert.fail("Impossible to get the editing domain provider");
		}

		EObject target = elementToDestroy;
		Object result = null;
		final ICommand getEditContextCommand = provider.getEditCommand(editContextRequest);
		if (getEditContextCommand != null) {
			IStatus status = null;
			try {
				// this command could run in an unprotected transaction, it is not supposed to modify the model
				InternalTransactionalEditingDomain domain = (InternalTransactionalEditingDomain) transactionalEditingDomain;
				Map<String, Object> options = new HashMap<>();
				options.put(Transaction.OPTION_NO_NOTIFICATIONS, true);
				options.put(Transaction.OPTION_NO_VALIDATION, true);
				options.put(Transaction.OPTION_NO_TRIGGERS, true);
				Transaction transaction = domain.startTransaction(false, options);
				try {
					status = getEditContextCommand.execute(null, null);
				} finally {
					transaction.commit();
				}
			} catch (InterruptedException e) {
				Assert.fail(e.getMessage());
			} catch (ExecutionException e) {
				Assert.fail(e.getMessage());
			} catch (RollbackException e) {
				Assert.fail(e.getMessage());
			}
			if (!(status == null || !status.isOK())) {
				result = getEditContextCommand.getCommandResult().getReturnValue();
			}
			if (result instanceof EObject) {
				target = (EObject) result;
			}
		}

		provider = ElementEditServiceUtils.getCommandProvider(target);
		if (provider == null) {
			return UnexecutableCommand.INSTANCE;
		}

		ICommand deleteGMFCommand = provider.getEditCommand(new DestroyElementRequest(transactionalEditingDomain, target, confirmationRequired));
		assertNotNull("Command should not be null", deleteGMFCommand);
		assertTrue("Command should be executable", deleteGMFCommand.canExecute());
		// command is executable, and it was expected to => run the creation
		Command emfCommand = new org.eclipse.papyrus.commands.wrappers.GMFtoEMFCommandWrapper(deleteGMFCommand);
		return emfCommand;
	}

}
