/*****************************************************************************
 * Copyright (c) 2013, 2016 CEA LIST, Christian W. Damus, and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Remi Schnekenburger (CEA LIST) - Initial API and implementation
 *  Christian W. Damus (CEA) - bug 434993
 *  Christian W. Damus - bugs 468071, 492565, 479635, 476984, 493866
 *
 *****************************************************************************/
package org.eclipse.papyrusrt.umlrt.core.tests.creation;

import static org.eclipse.papyrus.junit.matchers.MoreMatchers.greaterThan;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CommandStack;
import org.eclipse.emf.common.command.UnexecutableCommand;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.RollbackException;
import org.eclipse.emf.transaction.Transaction;
import org.eclipse.emf.transaction.impl.InternalTransactionalEditingDomain;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.IHintedType;
import org.eclipse.gmf.runtime.emf.type.core.commands.CreateElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.GetEditContextRequest;
import org.eclipse.papyrus.designer.ucm.core.types.UCMElementTypesEnumerator;
import org.eclipse.papyrus.infra.emf.gmf.command.GMFtoEMFCommandWrapper;
import org.eclipse.papyrus.infra.services.edit.service.ElementEditServiceUtils;
import org.eclipse.papyrus.infra.services.edit.service.IElementEditService;
import org.eclipse.papyrus.infra.services.edit.utils.IRequestCacheEntries;
import org.eclipse.papyrus.junit.utils.rules.UIThreadRule;
import org.eclipse.papyrus.uml.service.types.element.UMLElementTypes;
import org.eclipse.papyrusrt.umlrt.core.tests.AbstractPapyrusRTCoreTest;
import org.eclipse.papyrusrt.umlrt.core.tests.utils.IValidationRule;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Port;
import org.junit.After;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

/**
 * Test creation of UML elements (Creation / Undo / redo)
 */
public class CreateElementTest extends AbstractPapyrusRTCoreTest {
	@Rule
	public final TestRule uiThread = new UIThreadRule(true);

	@Test
	public void testNotCreateEnumerationLiteralInPackage() throws Exception {
		// create a package in the root model
		CreateElementCommand command = new CreateElementCommand(new CreateElementRequest(rootModel, UMLElementTypes.PACKAGE));
		command.execute(null, null);
		EObject newPackage = command.getNewElement();
		// test not creation of EnumerationLiteral in that package
		runNotCreationTest(newPackage, UMLElementTypes.ENUMERATION_LITERAL, true);
	}

	@Test
	public void testNotCreateAttributeInEnumeration() throws Exception {
		// create a package in the root model
		CreateElementCommand command = new CreateElementCommand(new CreateElementRequest(rootModel, UMLElementTypes.ENUMERATION));
		command.execute(null, null);
		EObject newEnumeration = command.getNewElement();
		// test not creation of EnumerationLiteral in that package
		runNotCreationTest(newEnumeration, UMLElementTypes.PROPERTY, true);
	}

	@Test
	public void testNotCreateOperationInEnumeration() throws Exception {
		// create a package in the root model
		CreateElementCommand command = new CreateElementCommand(new CreateElementRequest(rootModel, UMLElementTypes.ENUMERATION));
		command.execute(null, null);
		EObject newEnumeration = command.getNewElement();
		// test not creation of EnumerationLiteral in that package
		runNotCreationTest(newEnumeration, UMLElementTypes.OPERATION, true);
	}

	@Test
	protected void runNotCreationTest(EObject owner, IHintedType hintedType, boolean canCreate) throws Exception {
		Assert.assertTrue("Editor should not be dirty before test", !openPapyrusEditor.isDirty());
		IElementEditService elementEditService = ElementEditServiceUtils.getCommandProvider(owner);
		ICommand command = elementEditService.getEditCommand(new CreateElementRequest(owner, hintedType));
		Assert.assertNull(command);
	}

	@Test
	public void testCreateComponentType() throws Exception {
		runCreationTest(rootModel, UCMElementTypesEnumerator.COMPONENT_TYPE, true);
	}

	protected void runCreationTest(EObject owner, IHintedType hintedType, boolean canCreate) throws Exception {
		Assert.assertTrue("Editor should not be dirty before test", !openPapyrusEditor.isDirty());
		Command command = getCreateChildCommand(owner, hintedType, canCreate);

		// command has been tested when created. Runs the test if it is possible
		if (command != null && canCreate) {
			transactionalEditingDomain.getCommandStack().execute(command);
			transactionalEditingDomain.getCommandStack().undo();
			Assert.assertTrue("Editor should not be dirty after undo", !openPapyrusEditor.isDirty());
			transactionalEditingDomain.getCommandStack().redo();
			transactionalEditingDomain.getCommandStack().undo();
			// assert editor is not dirty
			Assert.assertTrue("Editor should not be dirty after undo", !openPapyrusEditor.isDirty());
		}
	}

	protected void runCreationTestWithGetContext(Element owner, IHintedType hintedType, boolean canCreate, Class<? extends IValidationRule> validationClass) throws Exception {
		runCreationTestWithGetContext(owner, hintedType, canCreate, 1, () -> {
			try {
				return validationClass.newInstance();
			} catch (Exception e) {
				throw new AssertionError(e);
			}
		});
	}

	protected void runCreationTestWithGetContext(Element owner, IHintedType hintedType, boolean canCreate, Supplier<? extends IValidationRule> validationSupplier) throws Exception {
		runCreationTestWithGetContext(owner, hintedType, canCreate, 1, validationSupplier);
	}

	protected void runCreationTestWithGetContext(Element owner, IHintedType hintedType, boolean canCreate, int iterations, Supplier<? extends IValidationRule> validationSupplier) throws Exception {
		Assume.assumeThat("Editor is dirty before test", openPapyrusEditor.isDirty(), is(false));

		List<Command> commands = new ArrayList<>(iterations);
		List<IValidationRule> validations = new ArrayList<>(iterations);
		for (int i = 0; i < iterations; i++) {
			Command command = getCreateChildCommandWithGetEditContext(owner, hintedType, canCreate);
			commands.add(command);

			// command has been tested when created. Runs the test if it is possible
			if (command != null && canCreate) {
				transactionalEditingDomain.getCommandStack().execute(command);
				IValidationRule validationRule = validationSupplier.get();
				validations.add(validationRule);
				Object[] commandResults = command.getResult().toArray();
				validationRule.validatePostEdition(owner, commandResults);
			} else {
				if (canCreate) {
					fail("Command is executable while it should not be");
				}
			}
		}

		// assert editor is dirty
		Assert.assertTrue("Editor should be dirty after execute", openPapyrusEditor.isDirty());

		for (int i = iterations - 1; i >= 0; i--) {
			transactionalEditingDomain.getCommandStack().undo();
			validations.get(i).validatePostUndo(owner, commands.get(i).getResult().toArray());
		}

		// assert editor is not dirty
		Assert.assertTrue("Editor should not be dirty after undo", !openPapyrusEditor.isDirty());

		for (int i = 0; i < iterations; i++) {
			transactionalEditingDomain.getCommandStack().redo();
			validations.get(i).validatePostEdition(owner, commands.get(i).getResult().toArray());
		}

		// assert editor is dirty
		Assert.assertTrue("Editor should be dirty after redo", openPapyrusEditor.isDirty());

		for (int i = iterations - 1; i >= 0; i--) {
			transactionalEditingDomain.getCommandStack().undo();
			validations.get(i).validatePostUndo(owner, commands.get(i).getResult().toArray());
		}

		// assert editor is not dirty
		Assert.assertTrue("Editor should not be dirty after undo", !openPapyrusEditor.isDirty());
	}

	/**
	 * Creates the element in the given owner element, undo and redo the action
	 *
	 * @param owner
	 *            owner of the new element
	 * @param hintedType
	 *            type of the new element
	 * @param canCreate
	 *            <code>true</code> if new element can be created in the specified owner
	 */
	protected Command getCreateChildCommand(EObject owner, IHintedType hintedType, boolean canCreate) throws Exception {
		IElementEditService elementEditService = ElementEditServiceUtils.getCommandProvider(owner);
		ICommand command = elementEditService.getEditCommand(new CreateElementRequest(owner, hintedType));

		if (!canCreate) {
			// command should not be executable: either it should be null or it should be not executable
			if (command != null && command.canExecute()) {
				fail("Creation command is executable but it was expected as not executable");
			}
		} else {
			// command should be executable in this case
			assertNotNull("Command should not be null", command);
			assertTrue("Command should be executable", command.canExecute());
			// command is executable, and it was expected to => run the creation
			Command emfCommand = GMFtoEMFCommandWrapper.wrap(command);
			return emfCommand;
		}
		return null;
	}

	protected Command getCreateChildCommandWithGetEditContext(EObject owner, IHintedType hintedType, boolean canCreate) {
		GetEditContextRequest editContextRequest = new GetEditContextRequest(transactionalEditingDomain, new CreateElementRequest(transactionalEditingDomain, owner, hintedType), owner);

		editContextRequest.setParameter(IRequestCacheEntries.Cache_Maps, new HashMap<>());
		editContextRequest.setEditContext(owner);
		editContextRequest.setClientContext(org.eclipse.papyrus.infra.services.edit.utils.ElementTypeUtils.getEditContext());

		IElementEditService provider = ElementEditServiceUtils.getCommandProvider(owner);
		if (provider == null) {
			Assert.fail("Impossible to get the editing domain provider");
		}

		EObject target = owner;
		Object result = null;
		final ICommand getEditContextCommand = provider.getEditCommand(editContextRequest);
		if (getEditContextCommand != null) {
			IStatus status = null;
			try {
				// this command could run in an unprotected transaction, it is not supposed to modify the model
				InternalTransactionalEditingDomain domain = (InternalTransactionalEditingDomain) transactionalEditingDomain;
				Map<String, Object> options = new HashMap<>();
				options.put(Transaction.OPTION_NO_NOTIFICATIONS, true);
				options.put(Transaction.OPTION_NO_VALIDATION, true);
				options.put(Transaction.OPTION_NO_TRIGGERS, true);
				Transaction transaction = domain.startTransaction(false, options);
				try {
					status = getEditContextCommand.execute(null, null);
				} finally {
					transaction.commit();
				}
			} catch (InterruptedException e) {
				Assert.fail(e.getMessage());
			} catch (ExecutionException e) {
				Assert.fail(e.getMessage());
			} catch (RollbackException e) {
				Assert.fail(e.getMessage());
			}
			if (!(status == null || !status.isOK())) {
				result = getEditContextCommand.getCommandResult().getReturnValue();
			}
			if (result instanceof EObject) {
				target = (EObject) result;
			}
		}

		provider = ElementEditServiceUtils.getCommandProvider(target);
		if (provider == null) {
			return UnexecutableCommand.INSTANCE;
		}

		ICommand createGMFCommand = provider.getEditCommand(new CreateElementRequest(transactionalEditingDomain, target, hintedType));
		if (!canCreate) {
			// command should not be executable: either it should be null or it should be not executable
			if (createGMFCommand != null && createGMFCommand.canExecute()) {
				fail("Creation command is executable but it was expected as not executable");
			}
		} else {
			// command should be executable in this case
			assertNotNull("Command should not be null", createGMFCommand);
			assertTrue("Command should be executable", createGMFCommand.canExecute());
			// command is executable, and it was expected to => run the creation
			Command emfCommand = GMFtoEMFCommandWrapper.wrap(createGMFCommand);
			return emfCommand;
		}
		return null;
	}

	static class NameValidationFactory implements Iterator<IValidationRule> {
		private final Iterator<String> expectedNames;

		/**
		 * Initializes me with an initial sequence of expected names and, optionally,
		 * a seed and operator to generate more (potentially infinite) names.
		 * 
		 * @param initialSequence
		 *            the initial sequence of names to expect
		 * @param seedForRest
		 *            a seed for additional names to be generated from, or {@code null} if no additional names are expected
		 * @param additionalNameGenerator
		 *            an operator on the {@code seedForRest} that generates additional names, or {@code null} if none
		 */
		NameValidationFactory(List<String> initialSequence, String seedForRest, UnaryOperator<String> additionalNameGenerator) {
			super();

			Stream<String> expectedNames = initialSequence.stream();
			if ((seedForRest != null) && (additionalNameGenerator != null)) {
				expectedNames = Stream.concat(expectedNames, Stream.iterate(seedForRest, additionalNameGenerator));
			}

			this.expectedNames = expectedNames.iterator();
		}

		/**
		 * Initializes me with a sequence of expected names.
		 * 
		 * @param sequenceOfNames
		 *            the sequence of names to expect
		 */
		NameValidationFactory(List<String> sequenceOfNames) {
			this(sequenceOfNames, null, null);
		}

		/**
		 * Initializes me with an initial sequence of expected names and, optionally,
		 * additional names based on incrementing the numeric suffix of a given seed.
		 * 
		 * @param initialSequence
		 *            the initial sequence of names to expect
		 * @param seedForRest
		 *            a seed with a numeric suffix for additional names to be generated
		 *            from, or {@code null} if no additional names are expected
		 */
		NameValidationFactory(List<String> initialSequence, String seedForRest) {
			this(initialSequence, seedForRest, incrementer());
		}

		private static UnaryOperator<String> incrementer() {
			Matcher suffix = Pattern.compile("\\d+$").matcher("");

			return previous -> {
				suffix.reset(previous);
				if (!suffix.find()) {
					throw new IllegalStateException("Seed name has no numeric suffix");
				} else {
					int next = Integer.parseInt(suffix.group()) + 1;
					return previous.substring(0, suffix.start()) + next;
				}
			};
		}

		@Override
		public boolean hasNext() {
			return expectedNames.hasNext();
		}

		@Override
		public IValidationRule next() {
			if (!hasNext()) {
				throw new NoSuchElementException();
			}

			String expectedName = expectedNames.next();

			IValidationRule result = new IValidationRule() {
				private NamedElement element;

				@Override
				public void validatePostEdition(Element targetElement, Object[] commandResults) throws Exception {
					assertThat("No element created", commandResults.length, greaterThan(0));
					assertThat("Non-element created", commandResults[0], instanceOf(NamedElement.class));
					element = (NamedElement) commandResults[0];
					assertThat("Wrong element name", element.getName(), is(expectedName));
				}

				@Override
				public void validatePostUndo(Element targetElement, Object[] commandResults) throws Exception {
					assertThat("Element still attached to the model",
							element.eContainer(), nullValue());
				}
			};

			return result;
		}
	}

	/**
	 * After a possibly failed test, unwinds the undo stack as much as possible
	 * to try to return to a clean editor.
	 */
	@After
	public void unwindUndoStack() {
		CommandStack stack = transactionalEditingDomain.getCommandStack();
		while (stack.canUndo()) {
			stack.undo();
		}
	}
}

class RTPortValidationRule implements IValidationRule {
	@Override
	public void validatePostUndo(Element targetElement, Object[] commandResults) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void validatePostEdition(Element targetElement, Object[] commandResults) throws Exception {
		Assert.assertEquals("comand result should not be empty", 1, commandResults.length);
		Object result = commandResults[0];
		Assert.assertTrue("new element should be an EObject", result instanceof EObject);
		// Assert.assertTrue("new element should be an rtPort", ElementTypeUtils.matches((EObject) result, IUCMElementTypes.COMPONENT_TYPE_ID));

		Port newPort = (Port) result;
		assertTrue(newPort.isOrdered());

	}
}
