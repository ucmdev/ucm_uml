/*****************************************************************************
 * Copyright (c) 2014, 2016 CEA LIST, Christian W. Damus, and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *  Christian W. Damus - bugs 479635, 476984, 493866
 *****************************************************************************/
package org.eclipse.papyrusrt.umlrt.core.tests;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.papyrus.designer.ucm.core.types.UCMElementTypesEnumerator;
import org.eclipse.papyrus.infra.core.resource.ModelSet;
import org.eclipse.papyrus.infra.core.resource.ModelUtils;
import org.eclipse.papyrus.infra.core.resource.NotFoundException;
import org.eclipse.papyrus.infra.core.services.ServiceException;
import org.eclipse.papyrus.infra.types.core.registries.ElementTypeSetConfigurationRegistry;
import org.eclipse.papyrus.infra.ui.editor.IMultiDiagramEditor;
import org.eclipse.papyrus.junit.framework.classification.tests.AbstractPapyrusTest;
import org.eclipse.papyrus.junit.utils.PapyrusProjectUtils;
import org.eclipse.papyrus.junit.utils.rules.HouseKeeper;
import org.eclipse.papyrus.uml.tools.model.UmlModel;
import org.eclipse.papyrus.uml.tools.model.UmlUtils;
import org.eclipse.swt.widgets.Display;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.UMLPackage;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.ClassRule;

/**
 * Generic class test for edition tests
 */
public abstract class AbstractPapyrusRTCoreTest extends AbstractPapyrusTest {

	@ClassRule
	public static final HouseKeeper.Static houseKeeper = new HouseKeeper.Static();

	@ClassRule
	// public static final UCMViewpointRule viewpoint = new UCMViewpointRule();

	protected static IProject createProject;

	protected static IFile copyPapyrusModel;

	protected static IMultiDiagramEditor openPapyrusEditor;

	protected static ModelSet modelset;

	protected static UmlModel umlIModel;

	protected static Model rootModel;

	protected static TransactionalEditingDomain transactionalEditingDomain;

	protected static Package protocolContainer;

	protected static Collaboration protocol;

	protected static org.eclipse.uml2.uml.Class capsule;

	protected static Interface messageSetIn;

	protected static Operation inProtocolMessage;

	protected static Interface messageSetOut;

	protected static Operation outProtocolMessage;

	protected static org.eclipse.uml2.uml.Class hasStateMachine;

	protected static StateMachine stateMachine;

	protected static Region region;

	protected static org.eclipse.uml2.uml.Class umlClass;

	protected static Operation umlOperation;

	protected static Parameter umlParameter;

	/**
	 * Init test class
	 */
	@BeforeClass
	public static void initCreateElementTest() throws Exception {

		// create Project
		createProject = houseKeeper.createProject("UMLRTCoreTests");

		// import test model
		try {
			copyPapyrusModel = PapyrusProjectUtils.copyPapyrusModel(createProject, Platform.getBundle("org.eclipse.papyrusrt.umlrt.core.tests"), "/resource/", "TestModel");
		} catch (CoreException e) {
			fail(e.getMessage());
		} catch (IOException e) {
			fail(e.getMessage());
		}

		Display.getDefault().syncExec(new Runnable() {

			@Override
			public void run() {
				// force loading of the state machine plugin, but in UI thread, as the element types require some UI...
				ElementTypeSetConfigurationRegistry registry = ElementTypeSetConfigurationRegistry.getInstance();
				System.err.println(registry.getElementTypeSetConfigurations());

			}
		});

		// open project
		openPapyrusEditor = houseKeeper.openPapyrusEditor(copyPapyrusModel);

		transactionalEditingDomain = openPapyrusEditor.getAdapter(TransactionalEditingDomain.class);
		assertTrue("Impossible to init editing domain", transactionalEditingDomain instanceof TransactionalEditingDomain);

		// retrieve UML model from this editor
		try {
			modelset = ModelUtils.getModelSetChecked(openPapyrusEditor.getServicesRegistry());
			umlIModel = UmlUtils.getUmlModel(modelset);
			rootModel = (Model) umlIModel.lookupRoot();
		} catch (ServiceException e) {
			fail(e.getMessage());
		} catch (NotFoundException e) {
			fail(e.getMessage());
		} catch (ClassCastException e) {
			fail(e.getMessage());
		}
		try {
			initExistingElements();
		} catch (Exception e) {
			fail(e.getMessage());
		}

		// force load of the element type registry. Will need to load in in UI thread because of some advice in communication diag: see org.eclipse.gmf.tooling.runtime.providers.DiagramElementTypeImages
		Display.getDefault().syncExec(new Runnable() {

			@Override
			public void run() {
				ElementTypeSetConfigurationRegistry registry = ElementTypeSetConfigurationRegistry.getInstance();
				Assert.assertNotNull("registry should not be null after init", registry);
				Assert.assertNotNull("element type should not be null", UCMElementTypesEnumerator.COMPONENT_TYPE);
			}
		});
	}

	/**
	 * Init fields corresponding to element in the test model
	 */
	protected static void initExistingElements() throws Exception {
		protocolContainer = (Package) rootModel.getPackagedElement("Protocol0", true, UMLPackage.eINSTANCE.getPackage(), false);
		Assert.assertNotNull(protocolContainer);

		protocol = (Collaboration) protocolContainer.getPackagedElement("Protocol0", true, UMLPackage.eINSTANCE.getCollaboration(), false);
		Assert.assertNotNull(protocol);

		capsule = (org.eclipse.uml2.uml.Class) rootModel.getPackagedElement("Capsule0", true, UMLPackage.eINSTANCE.getClass_(), false);
		Assert.assertNotNull(capsule);

		hasStateMachine = (org.eclipse.uml2.uml.Class) rootModel.getPackagedElement("HasStateMachine", true, UMLPackage.eINSTANCE.getClass_(), false);
		Assert.assertNotNull(hasStateMachine);

		stateMachine = (StateMachine) hasStateMachine.getClassifierBehavior();
		Assert.assertNotNull(stateMachine);

		region = stateMachine.getRegions().isEmpty() ? null : stateMachine.getRegions().get(0);
		Assert.assertNotNull(region);

		umlClass = (org.eclipse.uml2.uml.Class) rootModel.getOwnedType("Thing");
		Assert.assertNotNull(umlClass);

		umlOperation = umlClass.getOwnedOperation("doIt", null, null);
		Assert.assertNotNull(umlOperation);

		Assert.assertFalse(umlOperation.getOwnedParameters().isEmpty());
		umlParameter = umlOperation.getOwnedParameters().get(0);
	}
}
