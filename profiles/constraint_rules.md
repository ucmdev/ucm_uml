# Contraintes pour le profil UML/UCM

Ce fichier fait la liste des contraintes qu'un modèle UML/UCM doit vérifier.

## Base UCM

Cette section regroupe les contraintes qui s'appliquent aux spécifications UCM de base, qui sont standardisées dans la spécification OMG.

### Contrats

* un *ContractModule* est un package UML qui peut contenir seulement:
    * des DataType UML
    * des Interface UML
    * des *ContractModule*
    * des *AnnotationDefinition*
    * des *Exception*

### Composants

* un *ComponentModule* est un package UML qui peut contenir uniquement:
    * des *IComponent* et dérivés
    * des *TechnicalPolicy* et dérivés
    * des *ComponentModule*
    * des *ContractModule*
* un *ComponentType* peut hériter de zéro ou un seul autre *ComponentType* (relation de raffinement)
* un *IComponentImplementation* doit hériter d'un seul *ComponentType* (relation d'implémentation)
* un *CompositeComponentImplementation* peut hériter de zéro ou un seul autre *CompositeComponentImplementation* (relation de raffinement)
* si un *CompositeComponentImplementation* hérite d'un autre, leurs *ComponentType* respectifs doivent être identifiques
* un *AssemblyPart* peut raffiner un autre *AssemblyPart* si:
    * les deux *AssemblyPart* pointent vers des *IComponentImplementation* qui ont le même *ComponentType*
    * ou l'un des *AssemblyPart* pointe vers un *IComponentImplementation* dont le type est celui de l'autre *AssemblyPart*
* une *Connection* peut raffiner une autre *Connection* si le *ConnectorDefinition* de la seconde *Connection* étend ou est le même que le *ConnectorDefinition* de la première *Connection*. Même raisonnement si les *Connection* pointent vers des *InteractionPattern*
* un *AtomicComponentImplementation* ne peut contenir aucun *AssemblyPart*
* un *IComponentImplementation* ne peut pas avoir de port
* tous les ports d'un *ComponentType* doivent avoir pour type un *IPortSpec*

### Annotations

* les commentaires stéréotypés *Annotation* peuvent s'appliquer à:
    * *IComponent*
    * *IConcreteDataType*
    * Interface UML
    * Operation UML
    * Attribut UML
    * *AssemblyPart*

### Paramètres de configuration

* un *ConfigurationParameterValue* doit pointer vers un *ConfigurationParameter* qui appartient à l'élément pointé par l'élément qui possède le *ConfigurationParameterValue*:
    * *ConnectorDefinition* pour *Connection*
    * *ConnectorPortConfiguration* pour *ConnectionLink* ( *ConnectionEnd* dans le méta-modèle)
    * *TechnicalPolicyDefinition* pour *ComponentTechnicalPolicy*
    * *IEnvironmentResourceDefinition* pour *IEnvironmentResource*
    * *AnnotationDefinition* pour *Annotation*
* un *ConnectorPortConfiguration* ne peut reférencer qu'un port du *ConnectorDefinition* qui contient le *ConnectorPortConfiguration* ou l'un des *ConnectorDefinition* ancêtres

### Politiques techniques

* un *NonfunctionalAspectModule* est un package UML qui peut contenir seulement:
    * des *NonfunctionalAspectModule*
    * des *ContractModule*
    * zéro ou une classe *SupportedLanguages*
    * des *TechnicalAspect*
    * des *TechnicalPolicyDefinition*
* une *ComponentTechnicalPolicy* ne peut être lié qu'à un *AtomicComponentImplementation* ou dérivé 
* plusieurs *ComponentTechnicalPolicy* (ou *ComponentPortTechnicalPolicy* ) qui ont le même *TechnicalAspect* peuvent s'appliquer à un *AtomicComponentImplementation* si la cardinalité du *TechnicalAspect* est *AnyNumber* ou *AtLeastOne*
* pour chaque *TechnicalAspect* présent dans le modèle et dont la cardinalité est *ExactlyOne* ou *AtLeastOne*, chaque *AtomicComponentImplementation* doit être lié à une *ComponentTechnicalPolicy* ou *ComponentPortTechnicalPolicy* qui référence ce *TechnicalAspect*
* les éventuels ports d'une *TechnicalPolicyDefinition* doivent être stéréotypés *PortElement*
* dans un package *NonfunctionalAspectModule*, il ne peut y avoir au maximum qu'une seule énumération *SupprtedLanguages*
* dans une classe *SupportedLanguages*, tous les éléments doivent être différents

### Interactions

* un *InteractionModule* est un package UML qui peut contenir seulement:
    * des *InteractionModule*
    * des *ContractModule*
    * des *InteractionPattern*
    * des *ConnectorDefinition*
    * des *PortType*
* tous les ports d'une *ConnectorDefinition* doivent être stéréotypés *ConnectorPort*
* un *ConnectorPort* doit avoir un type stéréotypé *IPortType*
* les ports d'un *IPortType* doivent être stéréotypés *PortElement*
* un *ConnectionLink* doit être relié à une *Connection* et à un port d'un *AssemblyPart*
    * si le port de *l'AssemblyPart* a pour type un *PortTypeSpec*, le *IPortType* correspondant doit être le type d'un des *ConnectorPort* de la *ConnectorDefinition* de la *Connection*
    * si le port de *l'AssemblyPart* a pour type un *PortRoleSpec*, le *IPortRole* correspondant doit appartenir à *l'InteractionPattern* référencé par la *Connection*
* dans une *Connection* et ses ancêtres, pour chaque port du *ConnectorDefinition*, le nombre de *ConnectionLink* pointant vers un port de composant et ayant le *PortType* du *ConnectorPort* (le *PortType* est référencé par le *PortTypeSpec* du port du *ComponentType* ) doit correspondre à la cardinalité du *Role* auquel le *ConnectorPort* est associé

## Supplément UCM

Cette section regroupe les contraintes qui s'appliquent aux définitions étendues d'UCM, qui complètent la spécification OMG en ajoutant l'environnement de déploiement, l'allocation des composants, l'implémentation détaillée des composants et les cas de tests.

Cette section est encore instable.

### Resources d'environnement

* Un *ResourceDefinitionModule* est un package UML qui peut contenir seulement:
    * des *ContractModule*
    * des *ComputationResourceDefinition*
    * des *CommunicationResourceDefinition*
    * des *MemoryPartitionDefinition*
    * des *ExecutionResourceDefinition*
* un *EnvironmentModule* est un package UML qui peut contenir seulement:
    * des *ComputationResource*
    * des *CommunicationResource*

### Déploiement

* Un *DeploymentModule* est un package UML qui peut contenir seulement:
    * un *AppAssembly*
    * des *AllocationPlan*
    * des *TestCaseGroup*
    * des *Artefact*
* un *AppAssembly* est un composant UML qui peut contenir seulement:
    * des *AssemblyPart*
    * des *Connection*
* un *AllocationPlan* est un package UML qui peut contenir seulement:
    * des InstanceSpecification UML
